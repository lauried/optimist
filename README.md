Optimist
========

## Overview

Optimist is a kitchen-sink game engine project. It started in 2011 when I was disappointed that my little coding experiments tended to go to waste once they were done. I found with conventional game engines that I ended up battling the engine itself trying to make it render simple graphics, procedurally generate content, or do custom full-screen post-processing.

I also didn't want to depend on a lot of proprietary code for my own hobby.

Empirically, this worked. I was able to fit the majority of the things I wanted to try out into this framework.

## Building

See compiling.txt and running.txt.

Currently the project needs updating to use OpenGL 3 or higher, and doesn't fully compile at the moment.

## Notable Parts

### Custom Build System

I wrote a custom build system out of dissatisfaction with CMake, which required me to maintain separate lists of files to compile, and was unable to produce a single Code::Blocks project with the builds that I wanted.

The advantages of this system:

* Depends only on the C++ compiler.
* No need to modify a makefile or something to include every file - it traverses the source code to know what to build.
* It can produce a project file with every single target in at once, i.e. numerous different binaries each with debug/profiling/release builds.

Disadvantages:

* You need to structure your project in a specific way.

### Vernacular Scripting Language

I created this as a 64 bit compatible replacement for GameMonkey, but mostly because it was fun. I'd not written any gamecode yet so I didn't need to copy GameMonkey's syntax. I chose to make the language more strict to avoid common bugs of other languages.

Features:

* Uses ':=' for assignment, leaving '=' for comparison.
* Forbids the use of expressions where statements are required, and vice-versa, to prevent assignment-in-condition bugs.
* Values of different types are never equal; they must be explicitly cast before comparison.
* Key order is preserved in objects, letting them work like lists or associative arrays as needed.
* Private and read-only keys of objects can be created, to simulate classes with access levels. (Read-only is useful for class methods.)
* Uses a tri-colour incremental garbage collector.

### Terrain Generation

I'd been working on this for a while but never properly integrated it into a full project.

I generate terrain by blending together random noise at power-of-two frequencies, and use a very simple text language to control this. Each one of these scripts describes a particular type of terrain, e.g. plains, rolling hills, canyons. Chunks of terrain can then be generated, and these are blended together based on a coarse high-level map.

I'd then planned for another modification stage to come after this, allowing procedurally generated vector changes like rivers or cities, or for manual changes to be made to the terrain which would then be saved separately.