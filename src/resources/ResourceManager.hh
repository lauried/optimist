/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <string>
#include <list>
#include <map>
#include <mutex>
#include "filesystem/IFilesystem.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
\page resources Resource Management

\section usage Using Resources

A pointer object to the resource can be returned as follows:
~~~
ResourceManager::ResourcePointer<ConcreteResource> pointer = resourceManager->GetResource<ConcreteResource>("resourcename");
~~~

The object is returned by value and is copyable.

The resource is not guaranteed to load immediately, nor to load successfully at
all. (The former case is if we do asynchronous loading, the latter if the
resource is unavailable)

Every time we want to access the resource, we can then check:
~~~
if (pointer.GetState() == LOADED) ...
~~~

After which point, pointer.GetResource() is guaranteed to return a
ConcreteResource* for the lifetime of the object.

If the resource is no longer going to be used for some period of time, then
pointer.SetRequired(false) can be called. During this time any
ConcreteResource* returned by the pointer cannot be guaranteed to be valid,
even if it was returned before the object was set to not required.

Calling pointer.SetRequired(true) sets the state and behaviour back to that
when the pointer was initially created, e.g. not necessarily loaded, but once
loaded guaranteed to return a valid ConcreteResource*.


\section creating_resources Creating a Resource Type

Create a class ConcreteResource which implements the method:
~~~
bool Load(std::string name, IFilesystem *filesystem);
~~~
This method shall load the resource from the filesystem given the filename,
returning true if the resource was loaded, else false.
The resource system will ensure that each unique filename is only loaded once.

ConcreteResource typically implements a getter method that allows the
actual loaded data to be retrieved.
*/

// TODO: Manage a registered list of resource types so that gamecode is able to
// interact with the resource manager.
// For example, list requested resources, view their state (loaded, unloaded, etc)

/**
 * The main resource manager object.
 * Usage:
 * ResourceManager::ResourcePointer pointer
 * Usage:
 * IResource<ConcreteResource> *resource = resources->GetResource<ConcreteResource>("resourcename");
 */
class ResourceManager {
public:
	/**
	 * Current state of the resource.
	 */
	enum ResourceState {
		NOT_LOADED,
		LOADED,
		LOAD_FAILED
	};

	/**
	 * Flags passed to GetResource.
	 */
	enum ResourceFlags {
		RF_NONE = 0,
		/**
		 * Forces us to immediately try loading the resource rather than loading
		 * asynchronously.
		 */
		RF_FORCE_IMMEDIATE = 1
	};

	ResourceManager(IFilesystem *filesystem) : filesystem(filesystem) {}
	~ResourceManager();

private:
	/**
	 * Base class that we store resources as.
	 * We also use the base for dynamic casts to check for presence of a specific resource type.
	 */
	class IResourceLoader {
	public:
		virtual ~IResourceLoader();
		virtual void Load(std::string name, IFilesystem *filesystem, ResourceManager *manager) = 0;
		virtual int GetRefCount() = 0;
		virtual bool TryUnload() = 0;
	};

	/**
	 * Concrete resource wrapper of a given type.
	 * @todo Make this threadsafe regarding reference counting and loading/unloading.
	 */
	template <class T>
	class ResourceWrapper : public IResourceLoader {
	public:
		ResourceWrapper() : resource(nullptr), state(NOT_LOADED), refCount(0) {}
		~ResourceWrapper() { delete resource; }

		// IResource
		T* GetResource()
		{
			return (state == LOADED) ? resource : nullptr;
		}

		ResourceState GetState()
		{
			return state;
		}

		void IncRefCount()
		{
			refMutex.lock();
			refCount += 1;
			refMutex.unlock();
		}

		void DecRefCount()
		{
			refMutex.lock();
			refCount -= 1;
			refMutex.unlock();
		}

		// IResourceLoader
		void Load(std::string name, IFilesystem *filesystem, ResourceManager *manager)
		{
			resource = new T();
			state = (resource->Load(name, filesystem, manager)) ? LOADED : LOAD_FAILED;
		}

		int GetRefCount()
		{
			refMutex.lock();
			return refCount;
			refMutex.unlock();
		}

		bool TryUnload()
		{
			refMutex.lock();
			if (refCount > 0)
			{
				refMutex.unlock();
				return false;
			}
			state = NOT_LOADED;
			delete resource;
			resource = nullptr;
			refMutex.unlock();
		}

	private:
		T* resource;
		ResourceState state;
		int refCount;
		std::mutex refMutex;
	};

public:
	/**
	 * Wraps a resource pointer and performs reference counting.
	 */
	template <class T>
	class ResourcePointer {
	public:
		ResourcePointer() : required(false), resource(nullptr) {}
		ResourcePointer(ResourceWrapper<T> *resource) : required(true), resource(resource) {}

		ResourcePointer(const ResourcePointer& copy)
		{
			required = true;
			resource = copy.resource;
			resource->IncRefCount();
		}

#if 0
		void operator=(const ResourcePointer &copy)
		{
			// TODO: required should only be true when resource != nullptr, but without the check it segfaults. Fix this.
			if (required && resource != nullptr)
			{
				resource->DecRefCount();
			}
			required = true;
			resource = copy.resource;
			resource->IncRefCount();
		}
#endif

		~ResourcePointer()
		{
			if (required)
			{
				resource->DecRefCount();
			}
		}

		/**
		 * Returns the resource. If the resource is not loaded or if load failed
		 * then this returns nullptr.
		 */
		T* GetResource()
		{
			if (resource == nullptr)
			{
				return nullptr;
			}
			return resource->GetResource();
		}

		T* operator*()
		{
			return GetResource();
		}

		/**
		 * Returns the current state of the resource.
		 */
		ResourceState GetState()
		{
			if (resource == nullptr)
			{
				return ResourceManager::NOT_LOADED;
			}
			return resource->GetState();
		}

		/**
		 * Sets the pointers required flag. While true, the underlying resource is
		 * guaranteed never to be unloaded, but the user code may still have to
		 * wait for it to load. While false, the resource may be unloaded at any
		 * time.
		 */
		void SetRequired(bool required)
		{
			if (resource == nullptr)
			{
				return;
			}
			if (this->required != required)
			{
				if (this->required)
				{
					resource->DecRefCount();
					this->required = false;
				}
				else
				{
					resource->IncRefCount();
					this->required = true;
				}
			}
		}

		/**
		 * Nullifies the pointer.
		 */
		void Nullify()
		{
			required = false;
			resource = nullptr;
		}

	private:
		bool required;
		ResourceWrapper<T> *resource;
	};

	/**
	 * Gets an object of the given resource type.
	 * Attempts to get a resource of a type which can't be loaded will fail at compilation.
	 * Thus we guarantee to return a valid ResourcePointer<T> at runtime.
	 */
	template <class T>
	ResourcePointer<T> GetResource(std::string name, ResourceFlags flags = RF_NONE)
	{
		auto list = GetResourceList(name);
		for (auto it = list->begin(); it != list->end(); ++it)
		{
			ResourceWrapper<T> *resource = dynamic_cast<ResourceWrapper<T>*>(*it);
			if (resource != nullptr)
			{
				return ResourcePointer<T>(resource);
			}
		}
		auto resource = new ResourceWrapper<T>();
		list->push_back(resource);
		LoadResource(name, resource, flags);
		return ResourcePointer<T>(resource);
	}

private:
	IFilesystem *filesystem;
	std::map<std::string, std::list<IResourceLoader*>*> resources;

	/**
	 * Gets or creates the required list.
	 */
	std::list<IResourceLoader*>* GetResourceList(std::string name)
	{
		auto it = resources.find(name);
		if (it != resources.end())
		{
			return it->second;
		}
		auto list = new std::list<IResourceLoader*>();
		resources.insert(std::make_pair(name, list));
		return list;
	}

	/**
	 * Invokes loading of the resource.
	 * The resource is not immediately guaranteed to be of state LOADED.
	 */
	void LoadResource(std::string name, IResourceLoader* loader, ResourceFlags flags);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
