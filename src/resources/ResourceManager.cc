/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "resources/ResourceManager.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

ResourceManager::IResourceLoader::~IResourceLoader()
{
}

ResourceManager::~ResourceManager()
{
	for (auto it = resources.begin(); it != resources.end(); ++it)
	{
		auto list = it->second;
		for (auto it2 = list->begin(); it2 != list->end(); ++it2)
		{
			delete (*it2);
		}
		delete (it->second);
	}
}

void ResourceManager::LoadResource(std::string name, IResourceLoader *loader, ResourceFlags flags)
{
	if (flags & RF_FORCE_IMMEDIATE)
	{
		loader->Load(name, filesystem, this);
		return;
	}

	// TODO: Asynchronously we'd create a work object with the filesystem,
	// filename and loader.

	loader->Load(name, filesystem, this);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
