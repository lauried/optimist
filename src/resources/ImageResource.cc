/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "resources/ImageResource.hh"
#include "filesystem/File.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

ImageResource::~ImageResource()
{
	delete image;
}

bool ImageResource::Load(std::string name, IFilesystem *filesystem, ResourceManager *manager)
{
	File file(filesystem, name);
	if (!file.file->IsOpen())
	{
		return false;
	}
	size_t size = file.file->Size();
	std::vector<unsigned char> data(size);
	size_t read = file.file->ReadBytes((char *)&data[0], size);
	if (read != size)
	{
		return false;
	}
	delete image;
	image = new Image();
	image->Load(&data[0], size);

	if (image->Width() == 0 || image->Height() == 0)
	{
		return false;
	}

	return true;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
