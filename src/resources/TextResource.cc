/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "resources/TextResource.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

TextResource::TextResource()
{
	text = nullptr;
	size = 0;
}

TextResource::~TextResource()
{
}

bool TextResource::Load(std::string name, IFilesystem *filesystem, ResourceManager *manager)
{
	IFile *file = filesystem->Open(name);

	if (!file->IsOpen())
	{
		return false;
	}

	size = file->Size();
	text = new char[size + 1];
	size_t read = file->ReadBytes(text, size);

	if (read != size)
	{
		delete[] text;
		return false;
	}

	return true;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
