/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "resources/FontResource.hh"
#include <vector>
#include "font/StbFont.hh"
#include "font/BitmapFont.hh"
#include "filesystem/File.hh"
#include "library/text/Path.hh"
#include "library/file_types/OptiFontFile.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

FontResource::~FontResource()
{
    if (font != &StbFont::instance)
	{
		delete font;
	}
}

bool FontResource::Load(std::string name, IFilesystem *filesystem, ResourceManager *manager)
{
	Path pathInfo(name, Path::F_Extension);

	if (!pathInfo.extension.compare("csv"))
	{
		// TODO: Errors at any time use the stbfont instead.
		// Also the BitmapFont can wrap the StbFont instance.
		File csvFile(filesystem, name);
		size_t size = csvFile.file->Size();
		std::vector<char> csvData(size + 1);
		if (csvFile.file->ReadBytes(&csvData[0], size) == size)
		{
			csvData[size] = '\0';
			OptiFontFile fontFile(&csvData[0], size);

			// TODO: Here we need the resource manager.
			// It'd be pretty reasonable to pass the resource manager either
			// to this method, or the constructor of the resource object.

			font = new BitmapFont(&fontFile, manager->GetResource<ImageResource>(fontFile.GetImage()));

			return true;
		}
	}

	// Fallback to the default font, to guarantee we'll always be able to
	// render text.
    font = &StbFont::instance;
    return true;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
