/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "resources/SampleResource.hh"
#include <stdexcept>
#include "library/file_types/VorbisFile.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

SampleResource::SampleResource() : sampleFile(nullptr)
{
}

SampleResource::~SampleResource()
{
}

bool SampleResource::Load(std::string name, IFilesystem *filesystem, ResourceManager *manager)
{
	IFile *file = filesystem->Open(name);
	if (!file->IsOpen())
	{
		return false;
	}
	try
	{
		sampleFile = new VorbisFile(file);
	}
	catch (std::runtime_error &e)
	{
		return false;
	}
	return true;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
