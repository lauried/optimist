/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "resources/ModelResource.hh"
#include "library/text/Path.hh"
#include "library/file_types/Q3BspFile.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

static void BuildBspModel(Q3BspFile *bsp, Model *model, float modelScale);

bool ModelResource::Load(std::string name, IFilesystem *filesystem, ResourceManager *manager)
{
	Path pathInfo(name, Path::F_Extension);
	if (!pathInfo.extension.compare("bsp"))
	{
		IFile *file = filesystem->Open(name);
		if (!file->IsOpen())
		{
			return false;
		}
		size_t size = file->Size();
		char *data = new char[size];
		size_t read = file->ReadBytes(data, size);
		if (read != size)
		{
			delete[] data;
			return false;
		}

		// The Q3BspFile doesn't copy data when constructed in this way.
		// TODO: See if we can override placement new to do this instead, as
		// the syntax would then match the semantics of what we're doing.
		Q3BspFile *bspFile = new Q3BspFile((uint8_t *)data, size);
		if (!bspFile->IsValid())
		{
			delete[] data;
			return false;
		}
		model = new Model();
		BuildBspModel(bspFile, model, 1.0f);
		delete bspFile;
		delete[] data;
		return true;
	}
	// The file specified was not a recognized model format.
	return false;
}

static void BuildBspModel(Q3BspFile *bsp, Model *model, float modelScale)
{
	if (!bsp->IsValid())
		return;

	float scale = 1.0f / 128.0f;
	scale *= modelScale;

	model->SetScale(scale);

	// Parse entity data.
	QEntityFile entities(bsp->entities, bsp->entitiesSize);
	// Build an index from model index to entity index.
	// So that we can find the entity data for a given model.
	std::vector<int> entityForModel;
	entityForModel.resize(bsp->numModels, -1);
	for (int iEntity = 0; iEntity < entities.NumEntities(); iEntity += 1)
	{
		std::string c = entities.GetEntity(iEntity).GetString("classname");
		if (!c.compare("worldspawn"))
		{
			scale = entities.GetEntity(iEntity).GetFloat("scale");
			if (scale == 0.0f)
				scale = 128.0f;
			scale = 1.0f / scale;
		}

		std::string s = entities.GetEntity(iEntity).GetString("model");
		if (s.length() < 1 || s[0] != '*')
			continue;
		int modelNum = std::atoi(&s.c_str()[1]) - 1;
		if (modelNum >= 0 && modelNum < bsp->numModels)
			entityForModel[modelNum] = iEntity;
	}

	for (int iModel = 0; iModel < bsp->numModels; ++iModel)
	{
		int numVerts = 0;

		// For each face, if it's type 1, count the meshverts.
		for (int iFace = bsp->models[iModel].face
			; iFace <= bsp->models[iModel].face + bsp->models[iModel].numFaces
			; ++iFace)
		{
			if (bsp->faces[iFace].type != 1)
				continue;
			numVerts += bsp->faces[iFace].numMeshVerts;
		}

		// Allocate for that many triangles.
		int numTris = (numVerts / 3);
		int modelPart = 0;
		Vector3F partOrigin;
		if (entityForModel[iModel] >= 0)
		{
			std::string partName = entities.GetEntity(entityForModel[iModel]).GetString("part");
			modelPart = model->AddPart(partName);
			partOrigin = entities.GetEntity(entityForModel[iModel]).GetVector("origin") * scale;
			model->GetPart(modelPart)->origin = partOrigin;
		}
		Model::MeshData *md = model->AddMesh();
		md->part = modelPart;
		MeshBuilder *mesh = md->mesh;
		mesh->AllocateTris(numTris);

		// Add the verts.
		for (int iFace = bsp->models[iModel].face
			; iFace < bsp->models[iModel].face + bsp->models[iModel].numFaces
			; ++iFace)
		{
			if (bsp->faces[iFace].type != 1)
				continue;

			Color color;
			color.Set(bsp->textures[bsp->faces[iFace].texture].name, 64);

			for (int iMeshVert = bsp->faces[iFace].meshVert
				; iMeshVert < bsp->faces[iFace].meshVert + bsp->faces[iFace].numMeshVerts
				; ++iMeshVert)
			{
				int iVertex = bsp->meshVerts[iMeshVert] + bsp->faces[iFace].vertex;
				Q3BspFile::Vertex *vert = &(bsp->vertexes[iVertex]);

				float v[3] = {
					vert->position[0] * scale,
					vert->position[1] * scale,
					vert->position[2] * scale
				};

				mesh->AddVertex(v, vert->normal, color.rgba, 0);
			}
		}
	}

	// Set bounds.
	model->Bounds().mins.Set( std::numeric_limits<float>::max());
	model->Bounds().maxs.Set(-std::numeric_limits<float>::max());
	for (int i = 0; i < model->NumMeshes(); ++i)
	{
		MeshBuilder *mesh = model->GetMesh(i);
		for (int axis = 0; axis < 3; ++axis)
		{
			if (mesh->bounds.mins[axis] < model->Bounds().mins[axis])
				model->Bounds().mins[axis] = mesh->bounds.mins[axis];
			if (mesh->bounds.maxs[axis] > model->Bounds().maxs[axis])
				model->Bounds().maxs[axis] = mesh->bounds.maxs[axis];
		}
	}

	// Add non-model point entities to model.
	for (int iEntity = 0; iEntity < entities.NumEntities(); iEntity += 1)
	{
		std::string s = entities.GetEntity(iEntity).GetString("model");
		if (s.length() < 1 || s[0] != '*')
			continue;
		entities.RemoveEntity(iEntity);
		iEntity -= 1;
	}
	entities.MoveEntitiesTo(model->PointData());

}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
