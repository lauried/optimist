/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "events/EventsAdapter.hh"
#include <iostream>
#include "events/GamecodeTimerEvent.hh"
#include "gamecode/IGamecode.hh"
#include "gamecode/library/GamecodeOrientation.hh"
#include "gamecode/library/GamecodeVector.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

EventsAdapter::~EventsAdapter()
{
	// Delete the vectors.
	for (auto it = handlers.begin(); it != handlers.end(); ++it)
	{
		delete it->second;
	}
}

bool EventsAdapter::TriggerEvent(const std::string &name, std::vector<GamecodeValue> &parameters)
{
	auto itHandlers = handlers.find(name);
	if (itHandlers == handlers.end())
	{
		return false;
	}
	CallbackArray *callbacks = itHandlers->second;
	for (auto itCallbacks = callbacks->begin(); itCallbacks != callbacks->end(); ++itCallbacks)
	{
		IGamecodeFunction *callback = (*itCallbacks);
		GamecodeValue result = gamecode->CallFunction(callback, parameters, GamecodeValue());
		if (result.IsBool() && result.GetBool())
		{
			return true;
		}
	}

	return false;
}

bool EventsAdapter::TriggerEvent(const std::string &name, IEventTranslator *event)
{
	IGamecodeTable *table = event->GetGamecodeTable(gamecode);
	std::vector<GamecodeValue> params;
	params.push_back(GamecodeValue(table));
	return TriggerEvent(name, params);
}

std::map<std::string, IGamecodeAdapter::AdapterMethod> EventsAdapter::GetGlobalFunctions()
{
	return {
		// addListener(eventName, function)
		{ "addListener", std::bind(&EventsAdapter::AddListener, this, std::placeholders::_1, std::placeholders::_2) },

		// removeListener(eventName, function)
		{ "removeListener", std::bind(&EventsAdapter::RemoveListener, this, std::placeholders::_1, std::placeholders::_2) },

		// registerTimer(delay, function, [repeat])
		{ "registerTimer", std::bind(&EventsAdapter::RegisterTimer, this, std::placeholders::_1, std::placeholders::_2) },

		// getTimers()
		{ "getTimers", std::bind(&EventsAdapter::GetTimersGamecode, this, std::placeholders::_1, std::placeholders::_2) },
	};
}

void EventsAdapter::OnRegister(IGamecode *gamecode)
{
	this->gamecode = gamecode;

	// Note: no constructor for this object - we should only ever create it here.
	gamecode->RegisterEngineType("TimerEvent", gamecode->CreateEngineType<GamecodeTimerEvent>()
		->AddMethod("cancelTimer", &GamecodeTimerEvent::CancelTimerGamecode)
		->AddMethod("isCancelled", &GamecodeTimerEvent::IsCancelledGamecode)
	);
}

void EventsAdapter::TriggerEvents()
{
	uint64_t currentTime = timeProvider->GetTime();

	for (auto itTimers = timers.begin(); itTimers != timers.end();)
	{
		GamecodeTimerEvent *event = *itTimers;

		if (event->cancelled)
		{
			itTimers = timers.erase(itTimers);
			continue;
		}

		int64_t dueTime = (int64_t)event->triggerTime - (int64_t)currentTime;
		if (dueTime > 0)
		{
			++itTimers;
			continue;
		}

		std::vector<GamecodeValue> parameters;
		gamecode->CallFunction(GamecodeValue(event->callback), parameters, GamecodeValue());
		if (event->repeat)
		{
			// We take off the amount that we've overshot this delay, so that
			// repeating events occur somewhat regularly.
			event->triggerTime = currentTime + dueTime + event->delay;
			++itTimers;
		}
		else
		{
			itTimers = timers.erase(itTimers);
		}
	}
}

void EventsAdapter::GetTimers(std::vector<ITimerEvent*> *events)
{
	for (auto itTimers = timers.begin(); itTimers != timers.end(); ++itTimers)
	{
		events->push_back(*itTimers);
	}
}

uint64_t EventsAdapter::TimeUntilNextEvent()
{
	const uint64_t MAX_TIME = 0xffffffffffff;// 8000 years
	uint64_t currentTime = timeProvider->GetTime();
	uint64_t longestTime = MAX_TIME;

	for (auto itTimers = timers.begin(); itTimers != timers.end(); ++itTimers)
	{
		uint64_t timeUntilTrigger = (*itTimers)->triggerTime - currentTime;
		if (timeUntilTrigger < longestTime)
		{
			longestTime = timeUntilTrigger;
		}
	}
	if (longestTime < MAX_TIME)
	{
		return longestTime;
	}
	return 0;
}

void EventsAdapter::GetChildObjects(std::vector<GamecodeValue> *objects)
{
	for (auto itHandlers = handlers.begin(); itHandlers != handlers.end(); ++itHandlers)
	{
		CallbackArray *callbacks = itHandlers->second;
		for (auto itCallbacks = callbacks->begin(); itCallbacks != callbacks->end(); ++itCallbacks)
		{
			objects->push_back(GamecodeValue(*itCallbacks));
		}
	}

	for (auto itTimers = timers.begin(); itTimers != timers.end(); ++itTimers)
	{
		GamecodeTimerEvent *timer = *itTimers;
		objects->push_back(GamecodeValue((timer->callback)));
	}
}

GamecodeValue EventsAdapter::AddListener(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(2);
	std::string eventName(values->RequireType(0, GamecodeValue::T_String).GetString());
	IGamecodeFunction *callback = values->RequireType(1, GamecodeValue::T_Function).GetFunction();

	auto itHandlers = handlers.find(eventName);
	if (itHandlers == handlers.end())
	{
		itHandlers = handlers.insert(std::make_pair(eventName, new CallbackArray())).first;
	}
	CallbackArray *callbacks = itHandlers->second;
	callbacks->push_back(callback);

	return GamecodeValue();
}

GamecodeValue EventsAdapter::RemoveListener(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(2);
	std::string eventName(values->RequireType(0, GamecodeValue::T_String).GetString());
	IGamecodeFunction *callback = values->RequireType(1, GamecodeValue::T_Function).GetFunction();

	auto itHandlers = handlers.find(eventName);
	if (itHandlers == handlers.end())
	{
		return GamecodeValue();
	}

	CallbackArray *callbacks = itHandlers->second;
	for (auto itCallbacks = callbacks->begin(); itCallbacks != callbacks->end();)
	{
		if (*itCallbacks == callback)
		{
			itCallbacks = callbacks->erase(itCallbacks);
		}
		else
		{
			++itCallbacks;
		}
	}

	return GamecodeValue();
}

GamecodeValue EventsAdapter::RegisterTimer(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(2, 3);
	int delay = values->RequireType(0, GamecodeValue::T_Number).GetInt();
	IGamecodeFunction *callback = values->RequireType(1, GamecodeValue::T_Function).GetFunction();
	bool repeat = false;
	if (values->size() == 3)
	{
		repeat = values->RequireType(2, GamecodeValue::T_Bool).GetBool();
	}

	double triggerTime = timeProvider->GetTime() + delay;

	GamecodeTimerEvent *event = new GamecodeTimerEvent(triggerTime, delay, callback, repeat);
	timers.push_back(event);

	return GamecodeValue(event);
}

GamecodeValue EventsAdapter::GetTimersGamecode(GamecodeParameterList *values, IGamecode* gamecode)
{
	GamecodeValue tableValue = gamecode->CreateTable();
	IGamecodeTable *table = tableValue.GetTable();

	int index = 0;
	for (auto itTimers = timers.begin(); itTimers != timers.end(); ++itTimers, index += 1)
	{
		table->Set(GamecodeValue(index), GamecodeValue(*itTimers));
	}

	return tableValue;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
