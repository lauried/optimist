/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <vector>
#include "events/ITimerEvent.hh"
#include "gamecode/IGamecodeFunction.hh"
#include "gamecode/IGamecodeEngineObject.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class GamecodeTimerEvent : public IGamecodeEngineObject, public ITimerEvent {
public:
	bool cancelled;
	uint64_t triggerTime;
	int delay;
	IGamecodeFunction *callback;
	bool repeat;

	GamecodeTimerEvent(uint64_t triggerTime, int delay, IGamecodeFunction *callback, bool repeat);

	// ITimerEvent
	virtual void CancelTimer() { cancelled = true; }
	virtual bool IsCancelled() { return cancelled; }

	// IGamecodeEngineObject
	GamecodeValue CancelTimerGamecode(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue IsCancelledGamecode(GamecodeParameterList *parameters, IGamecode *gamecode);
	virtual std::vector<IGamecodeObject*>* GetChildObjects() { return &childObjects; }

private:
	std::vector<IGamecodeObject*> childObjects;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

