/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "library/EventQueue.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Listens for events of type DATA and uses TRANSLATOR to translate them into
 * gamecode tables to pass to the events adapter.
 *
 * Translator must typedef DataType, and construct from one.
 */
template <class TRANSLATOR, typename DATA>
class EventListener : public EventQueue<DATA>::IListener {
public:
	EventListener(std::string eventName, EventsAdapter *adapter)
		: eventName(eventName)
		, adapter(adapter)
		{}

	typedef typename EventQueue<DATA>::IEventState EventState;

	// EventQueue<DATA>::IListener
	virtual void HandleEvent(DATA &data, EventState &state)
	{
		TRANSLATOR translator(data);
		bool result = adapter->TriggerEvent(eventName, &translator);
		if (result)
		{
			state.Finish();
		}
	}

private:
	std::string eventName;
	EventsAdapter *adapter;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

