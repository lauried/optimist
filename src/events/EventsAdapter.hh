/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <map>
#include <list>
#include "events/IEventTranslator.hh"
#include "events/ITimerEvent.hh"
#include "events/GamecodeTimerEvent.hh"
#include "gamecode/IGamecodeAdapter.hh"
#include "platform/ITimeProvider.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Listens to platform events, processes them and passes them to gamecode.
 */
class EventsAdapter : public IGamecodeAdapter {
public:
	EventsAdapter(ITimeProvider *timeProvider) : timeProvider(timeProvider), name("Events"), gamecode(nullptr) {}
	~EventsAdapter();

	/**
	 * Triggers the named event.
	 * Gamecode functions are called in order with the given parameters until
	 * one returns true, then stops.
	 * Returns true if a gamecode function returned true, else false.
	 */
	bool TriggerEvent(const std::string &name, std::vector<GamecodeValue> &parameters);

	/**
	 * Triggers the event using an event translator.
	 */
	bool TriggerEvent(const std::string &name, IEventTranslator *event);

	// IGamecodeAdapter
	virtual const std::string& GetName() { return name; }
	virtual std::map<std::string, IGamecodeAdapter::AdapterMethod> GetGlobalFunctions();
	virtual void OnRegister(IGamecode *gamecode);
	virtual void GetChildObjects(std::vector<GamecodeValue> *objects);

	// Events
	void TriggerEvents();
	void GetTimers(std::vector<ITimerEvent*> *events);
	uint64_t TimeUntilNextEvent();

	// Adapter methods
	GamecodeValue AddListener(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue RemoveListener(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue RegisterTimer(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue GetTimersGamecode(GamecodeParameterList *values, IGamecode* gamecode);

private:
	typedef std::vector<IGamecodeFunction *> CallbackArray;

	ITimeProvider *timeProvider;
	std::string name;
	IGamecode *gamecode;
	std::map<std::string, CallbackArray*> handlers;
	std::list<GamecodeTimerEvent*> timers;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

