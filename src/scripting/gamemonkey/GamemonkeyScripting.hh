/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#ifndef _SCRIPTING_GAMEMONKEY_GAMEMONKEYSCRIPTING_HH_
#define _SCRIPTING_GAMEMONKEY_GAMEMONKEYSCRIPTING_HH_
//-----------------------------------------------------------------------------
#include "scripting/ScriptingInterface.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class GamemonkeyScripting : public ScriptingInterface {
public:
	// Initialize loading files from somewhere.
	// For now, we have full filesystem access but we eventually want to
	// request a filesystem wrapper and do all our access through that. This
	// would allow individual systems to each have their own wrapper enforcing
	// permissions suitable for that access.
	GamemonkeyScripting();
	~GamemonkeyScripting();
	void Init();

	// Implement ScriptingInterface

private:

};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
#endif //_SCRIPTING_GAMEMONKEY_GAMEMONKEYSCRIPTING_HH_
//-----------------------------------------------------------------------------

