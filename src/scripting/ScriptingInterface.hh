/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#ifndef _SCRIPTING_SCRIPTINGINTERFACE_HH_
#define _SCRIPTING_SCRIPTINGINTERFACE_HH_
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// TODO: Rename scripting to something else, i.e. a proper name for the object,
// because "a scripting" doesn't make sense.
// Machine might make sense.

// Calling it Gamecode would kind of make sense, except that it seems
// inappropriate for the framework to decide that that shall be the use of
// scripting.
// But then we really are talking about the interface between what runs the
// game and the different parts that the engine provides to the scripting.
// Behind this interface we could still be using C++ code.

class ScriptingInterface {
public:
	// Table interface, for encapsulating data
	// Call Function
	// Global Isset, Get, Set
	// Register external system to scripting
	//   this will provide a list of function names and their callbacks
	//   it can also set globals.
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
#endif //_SCRIPTING_SCRIPTINGINTERFACE_HH_
//-----------------------------------------------------------------------------

