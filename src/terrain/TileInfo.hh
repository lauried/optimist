/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "library/geometry/Vector3.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Information about a tile of terrain (as opposed to heightmap points)
 */
class TileInfo {
public:
	bool isWater[2];
	int diagonality; ///< 0 = divided positive to negative, 1 = pn to np
	int terrainType; ///< Index of a TerrainType in a TerrainTypeData.

	TileInfo() : diagonality(0), terrainType(0)
	{
		isWater[0] = false;
		isWater[1] = false;
	}

	// Other types would combine land type and vegetation, though each land
	// type might have its own vegetation models.
	// Thus the least significant hex digit can determine vegetation type.
	// bare/short grass, sparse+small, sparse trees, forest.
	// The basic terrain type then just describes land type:
	// temperate/grass, sandy desert, soily desert, snow
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
