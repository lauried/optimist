/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <string>
#include <vector>
#include "library/geometry/Vector2.hh"
#include "terrain/ITerrainSource.hh"
#include "terrain/TerrainCache.hh"
#include "terrain/TerrainCacheFactory.hh"
#include "gamecode/IGamecodeEngineObject.hh"
#include "resources/ResourceManager.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Represents a unique terrain.
 * Contains a TerrainSource and a cache of its chunks.
 * Is used by terrain rendering and physics objects.
 * Here we can also do things like query tile type, gradient and height at
 * specific coordinates.
 */
class GamecodeTerrain : public IGamecodeEngineObject {
public:
	GamecodeTerrain(ResourceManager *resources, TerrainCacheFactory *cacheFactory, int chunkSize = 16);
	~GamecodeTerrain();

	// IGamecodeEngineObject
	void SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode);
	GamecodeValue GetProperty(std::string key, IGamecode *gamecode);
	void SetIndex(int i, GamecodeValue val, IGamecode *gamecode);
	GamecodeValue GetIndex(int i, IGamecode *gamecode);
	void Destruct();

	// Methods for engine objects
	// TODO: Get chunk at given coordinates and resolution.
	// Consider ChunkSize when we're retrieving chunks.
	// Use coordinates in Chunks. Lower resolution chunks would still have the
	// same size, but they'd exist on power-of-2 chunk boundaries, rather than
	// just single ones.

	TerrainChunk *GetChunk(int x, int y);
	void FreeChunk(TerrainChunk *data);

	// Methods for both engine and gamecode
	// TODO: TileType, TerrainNormal, TerrainHeight.
	// Provide the option to query whether a chunk is cached, and then let
	// the query-point functions take a parameter whether to force the chunk to
	// load or just to return zero if it's not cached.

	/**
	 * Translate from world units to being in terms of numbers
	 * of tiles. Fractional values within tile still occur.
	 * The actual integer tile coordinate is the rounded-down (towards minus
	 * infinity) value, so whole numbers lie on tile boundaries.
	 * Tiles themselves are addressed by their axis-minimum coordinates.
	 */
	Vector2F WorldCoordsToTile(Vector2F world)
	{
		return world * (1.0f / source->HorizScale());
	}

	/**
	 * Translate coordinates from numbers of tiles to actual world units.
	 */
	Vector2F TileCoordsToWorld(Vector2F tile)
	{
		return tile * source->HorizScale();
	}

	/**
	 * Translate from world units to a position in terms of numbers of chunks.
	 * Fractions still occur, but whole numbers lie on chunk boundaries.
	 * Chunks themselves are addressed by their axis-minimum coordinates.
	 */
	Vector2F WorldCoordsToChunk(Vector2F world)
	{
		return world * (1.0f / (source->HorizScale() * (float)chunkSize));
	}

	/**
	 * Returns the world coordinates of the minimum corner of the chunk.
	 */
	Vector2F ChunkCoordsToWorld(Vector2F chunk)
	{
		return chunk * source->HorizScale() * (float)chunkSize;
	}

	/**
	 * Return the size of a tile in world coordinates.
	 */
	Vector2F WorldTileSize()
	{
		return Vector2F(source->HorizScale(), source->HorizScale());
	}

	/**
	 * Return the size of a chunk in world coordinates.
	 */
	Vector2F WorldChunkSize()
	{
		return Vector2F((float)chunkSize, (float)chunkSize);
	}

	// TODO: Setting source is probably better done by engine objects that the
	// gamecode can use to configure how terrain is loaded and saved.
	// Or we use some factory to get the source.

	void SetImageSource(std::string image);
	GamecodeValue SetImageSource(GamecodeParameterList *parameters, IGamecode *gamecode);

	void SetProceduralSource();
	GamecodeValue SetProceduralSource(GamecodeParameterList *parameters, IGamecode *gamecode);

	/**
	 * Returns terrain height under the given coordinates.
	 * Everything is in world coordinates.
	 */
	float GetHeightAtPoint(Vector2F worldCoords);
	GamecodeValue GetHeightAtPoint(GamecodeParameterList *parameters, IGamecode *gamecode);

	// TODO: Implement Multi-Image and Procedural.
	// TODO: Let us set up Terrain Type data.

private:
	ResourceManager *resources;
	TerrainCacheFactory *cacheFactory;
	int chunkSize;
	ITerrainSource *source;
	TerrainCache *cache;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
