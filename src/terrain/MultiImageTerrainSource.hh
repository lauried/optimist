/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <vector>
#include "library/containers/Array2D.hh"
#include "library/containers/PagedAllocator.hh"
#include "library/graphics/Image.hh"
#include "terrain/ITerrainSource.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class MultiImageTerrainSource : public ITerrainSource {
public:
	MultiImageTerrainSource(float waterDepth = 4.0f);
	~MultiImageTerrainSource();

	// WaterLevel is in the same units as the scaled height.
	void AddHeightmap(Image *image, int x, int y, float waterLevel, float vertScale);
	// TODO: Add type data

	// ITerrainSource
	TerrainChunk *GetPiece(int x, int y, int w, int h);
	void FreePiece(TerrainChunk *data);
	float HorizScale();

private:
	struct HeightMap {
		int x, y;
		// Altitude of zero values in heightmap, relative to waterlevel.
		// This should be negative.
		float floor;
		Array2D<float> heights;
	};
	struct HeightMapFloorComparer {
		bool operator()(const HeightMap *a, const HeightMap *b) const
		{ return a->floor < b->floor; }
	};

	PagedAllocator<HeightMap> heightMaps;
	float waterDepth;
	float horizScale;

	void BlendHeights(int destX, int destY, Array2D<float> *dest);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
