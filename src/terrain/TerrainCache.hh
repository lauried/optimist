/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <map>
#include <deque>
#include "library/geometry/Box2.hh"
#include "terrain/ITerrainSource.hh"
#include "terrain/TerrainChunk.hh"
#include "platform/ITimeProvider.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class TerrainCache : public ITerrainSource {
public:
	TerrainCache(ITerrainSource *source, ITimeProvider *time) :
		source(source),
		time(time),
		checkInterval(512),
		upperThreshold(4096),
		lowerThreshold(1024),
		cacheDeletionAge(2000),
		cacheSize(0),
		lastCheckTime(0)
		{}

	~TerrainCache();

	// ITerrainSource
	TerrainChunk* GetPiece(int x, int y, int w, int h);
	void FreePiece(TerrainChunk *data);
	float HorizScale() { return source->HorizScale(); }

private:
	ITerrainSource *source;
	ITimeProvider *time;

	struct CacheEntryDynamic {
		TerrainChunk *data;
		int64_t lastAccess; // Uses platform's GetTime.
		int references; // Reference count.
	};

	std::map<Box2I, CacheEntryDynamic> cache;

	// The map lets us find the object on deletion.
	std::map<TerrainChunk*, Box2I> cacheByItem;

	// The queue lets us delete most recent entries in an order somewhat based
	// on age.
	// We needn't delete from this when we remove the object, we can just clear
	// out the empty entries when freeing up cache.
	std::deque<Box2I> cacheEntriesByCreation;

	int checkInterval; // check after this many ticks
	int upperThreshold; // clear out cache if it exceeds this many items
	int lowerThreshold; // delete cache down to this many items
	uint64_t cacheDeletionAge; // age below which entries will not be removed
	// from cache.

	int cacheSize; // In items. TODO: do size in bytes
	int lastCheckTime; // counter for checkInterval
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
