/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "terrain/ImageTerrainSource.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

ImageTerrainSource::ImageTerrainSource(Image *image, int tx, int ty,
	float waterLevel, float waterDepth, float vertScale)
{
	this->waterLevel = waterLevel;
	this->waterDepth = waterDepth;
	this->vertScale  = vertScale;

	horizScale = 16.0f;

	this->tx = tx;
	this->ty = ty;

	heights.SetSize(image->Width(), image->Height());
	uint8_t *data = image->Data();
	int size = image->Width() * image->Height();
	for (int i=0; i<size; ++i)
	{
		heights.at(i) = *data * vertScale;
		data += image->Bpp();
	}

	// Adjust for water depth and terrain height.
	for (int i = 0; i < heights.Size()[0] * heights.Size()[1]; ++i)
	{
		if (heights.at(i) == 0)
			heights.at(i) = -waterDepth;
		heights.at(i) -= waterLevel;
		if (heights.at(i) < -waterDepth)
			heights.at(i) = -waterDepth;
	}

	// Raise bits so the coastline has a solid edge.
	for (int y = 0; y < heights.Size()[1]; ++y)
	{
		for (int x = 0; x < heights.Size()[0]; ++x)
		{
			if (heights.at(x, y) >= 0)
				continue;

			if (   (heights.at_clamped(x+0, y+1) > 0)
				|| (heights.at_clamped(x-1, y+0) > 0)
				|| (heights.at_clamped(x+0, y-1) > 0)
				|| (heights.at_clamped(x+1, y+0) > 0))
			{
				heights.at(x, y) = 0;
			}
		}
	}
}

ImageTerrainSource::~ImageTerrainSource()
{
}

static void CopyPart(float *src, int srcW, float *dst, int dstW, int srcX, int srcY, int dstX, int dstY, int w, int h)
{
	for (int y = 0; y < h; ++y)
	{
		for (int x = 0; x < w; ++x)
		{
			dst[(dstY+y)*dstW + dstX+x] = src[(srcY+y)*srcW + srcX+x];
		}
	}
}

// Must return water if the position is out of range.
TerrainChunk *ImageTerrainSource::GetPiece(int x, int y, int w, int h)
{
	// We need an extra row of heights to get this many tiles.
	w += 1;
	h += 1;

	// Offset the source data by the translation it was constructed with.
	x -= tx;
	y -= ty;

	// Init to waterdepth.
	Array2D<float> data(w, h, -waterDepth);

	// Copy one to the other with an offset, automatically clipping the size:
	// TODO: this could be moved to Array2D
	if (x + w > 0 && y + h > 0 && x <= heights.Width() && y <= heights.Height())
	{
		int srcX = x,  srcY = y;
		int dstX = 0,  dstY = 0;
		int copyW = w, copyH = h;

		if (srcX < 0)
		{
			dstX = -srcX;
			copyW += srcX;
			srcX = 0;
		}
		if (srcY < 0)
		{
			dstY = -srcY;
			copyH += srcY;
			srcY = 0;
		}
		if (srcX + copyW > heights.Width())
		{
			copyW = heights.Width() - srcX;
		}
		if (srcY + copyH > heights.Height())
		{
			copyH = heights.Height() - srcY;
		}
		CopyPart(&heights.at(0), heights.Width(), &data.at(0), w, srcX, srcY, dstX, dstY, copyW, copyH);
	}

	return new TerrainChunk(&data, x + tx, y + ty, horizScale, 1);
}

void ImageTerrainSource::FreePiece(TerrainChunk *data)
{
	delete data;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

