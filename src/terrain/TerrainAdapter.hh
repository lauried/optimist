/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gamecode/IGamecode.hh"
#include "platform/ITimeProvider.hh"
#include "terrain/TerrainCacheFactory.hh"
#include "resources/ResourceManager.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Lets us instantiate terrain objects.
 */
class TerrainAdapter : public IGamecodeAdapter {
public:
	TerrainAdapter(ResourceManager *resources, ITimeProvider *time) : resources(resources), cacheFactory(time) {}

	// IGamecodeAdapter
	virtual const std::string& GetName();
	virtual std::map<std::string, IGamecodeAdapter::AdapterMethod> GetGlobalFunctions();
	virtual void OnRegister(IGamecode *gamecode);

private:
	ResourceManager *resources;
	TerrainCacheFactory cacheFactory;

	IGamecodeEngineObject* ConstructTerrain(GamecodeParameterList *parameters, IGamecode *gamecode);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
