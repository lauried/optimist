/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <map>
#include "terrain/TerrainChunk.hh"
#include "renderer/Model.hh"
#include "library/geometry/Orientation.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

struct TerrainColour {
	uint32_t colour[2];
	TerrainColour() { colour[0] = colour[1] = 0x00880088; }
	TerrainColour(uint32_t a, uint32_t b) { colour[0] = a; colour[1] = b; }
};

class TerrainChunkModel {
public:
	// The terrain data is not required once we're constructed.
	TerrainChunkModel(TerrainChunk *data, std::map<int, TerrainColour> *terrainColours);
	~TerrainChunkModel();

	Model* GetLand() { return land; }
	Model* GetUnderwater() { return underwater; }
	Model* GetWaterSurface() { return waterSurface; }

	OrientationF orientation;
	Box3F bounds;

private:
	Model *land;
	Model *underwater;
	Model *waterSurface;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
