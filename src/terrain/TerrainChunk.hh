/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "library/graphics/Image.hh"
#include "library/containers/Array2D.hh"
#include "library/geometry/Box3.hh"
#include "terrain/TileInfo.hh"
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * This is the plain data on a piece of terrain. Height and surface-type
 * information can be queried.
 */
class TerrainChunk {
public:
	typedef uint16_t TileTypeValue;

	/**
	 * Constructs empty.
	 */
	TerrainChunk(int sx, int sy, int tx, int ty, float hScale, float vScale);

	/**
	 * Constructs from an Image. The data is copied, leaving the image intact.
	 */
	TerrainChunk(Image *image, int tx, int ty, float hScale, float vScale);

	/**
	 * Constructs from a 2D array of height data.
	 * The original array is destroyed and will no longer contain any data,
	 * with ownership transfered internally.
	 */
	TerrainChunk(Array2D<float> *data, int tx, int ty, float hScale, float vScale);

	/**
	 * Constructs from 2D arrays of height data and tile type.
	 * The original arrays are destroyed and will no longer contain any data,
	 * with ownership transfered internally.
	 */
	TerrainChunk(Array2D<float> *data, Array2D<TileTypeValue> *tileTypes, int tx, int ty, float hScale, float vScale);
	~TerrainChunk();

	void UpdateBoundsAndDiagonals() { SetDiagonals(); SetBounds(); }

	/**
	 * Retrieves triangle vertices, its terrain type and diagonality.
	 * Vertices are speicfied with the right-angled vertex first, and then in
	 * counter-clockwise order.
	 * @param x Internal coordinates of the tile within the chunk.
	 * @param y Internal coordinates of the tile within the chunk.
	 * @param triangle The triangle number, must be 0 or 1.
	 * @param vertData Pointer to the buffer to which vertex data is written.
	 * @param vertStep Offset between the start of each vertex.
	 * @param type Pointer to an integer which will be set to the terrain type
	 * of the triangle.
	 * @param diagonality Pointer to an integer which will be set to the
	 * diagonality of the tile. Diagonality is the direction in which the tile
	 * is split - 0 for y=x and 1 for y=1-x. This line is relative to the
	 * minimum coordinate of the tile.
	 */
	void GetTriangle(int x, int y, int triangle, float *vertData, int vertStep, int *type, int *diagonality);

	/**
	 * See GetTriangle with diagonality. This version simply discards the
	 * diagonality information automatically.
	 * @todo Remove shorthand version.
	 */
	void GetTriangle(int x, int y, int triangle, float *vertData, int vertStep, int *type)
	{
		int tmp;
		GetTriangle(x, y, triangle, vertData, vertStep, type, &tmp);
	}

	/**
	 * Size of the chunk in tiles.
	 */
	const Vector2I& SizeTiles() const { return tiles.Size(); }

	/**
	 * Returns a TileInfo structure with information on the given tile.
	 * @param x Internal coordinates of the tile within the chunk.
	 * @param y Internal coordinates of the tile within the chunk.
	 */
	TileInfo GetTileInfo(int x, int y) { return tiles.at(x, y); }

	/**
	 * Returns the chunks world position in tile coordinates.
	 */
	const Vector2I& TileOrigin() const { return tileOrigin; }

	/**
	 * Returns the height of the terrain at a given point.
	 * @param x The coordinate relative to the chunk.
	 * @param y The coordinate relative to the chunk.
	 */
	float GetTerrainHeight(float x, float y);

	/**
	 * Returns the bounding box of the chunk relative to the chunk's minimum
	 * coordinate at zero altitude.
	 */
	const Box3F & GetBounds() { return bounds; }

	/**
	 Returns the chunk's minimum coordinate at zero altitude.
	 */
	const Vector3F & GetOrigin() { return origin; }

	/**
	 * Horizontal scale is the horizontal size of a tile in world coordinates.
	 */
	float HorizScale() { return horizScale; }

	/**
	 * Access to the heights.
	 */
	const Array2D<float> *GetHeights() const { return &heights; }

	/**
	 * Access to tile info.
	 */
	const Array2D<TileInfo> *GetTileInfo() const { return &tiles; }

private:
	Array2D<float> heights;
	Array2D<TileInfo> tiles;
	Vector2I tileOrigin;
	float horizScale, vertScale;
	Box3F bounds;
	Vector3F origin;

	void SetDiagonals();
	void SetTileTypes(Array2D<TileTypeValue> *tileTypes);
	void SetBounds();
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
