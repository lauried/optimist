/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "terrain/MultiImageTerrainSource.hh"
#include <vector>
#include <algorithm>
#include <iostream>
#include "library/maths/Maths.hh"
#include "library/maths/RandomField.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

MultiImageTerrainSource::MultiImageTerrainSource(float waterDepth)
{
	this->waterDepth = waterDepth;
	horizScale = 16.0f;
}

MultiImageTerrainSource::~MultiImageTerrainSource()
{
}

void MultiImageTerrainSource::AddHeightmap(Image *image, int x, int y, float waterLevel, float vertScale)
{
	HeightMap *heightMap = &heightMaps[heightMaps.Allocate()];
	heightMap->x = x;
	heightMap->y = y;
	heightMap->heights.SetSize(image->Width(), image->Height());

	// If the underwater terrain is deeper than the sea bed, then lower its
	// values and clamp it to the bottom.
	heightMap->floor = -waterLevel;
	float offset = 0;
	if (waterLevel > waterDepth)
	{
		heightMap->floor = -waterDepth;
		offset = waterLevel - waterDepth;
	}

	// Copy image data.
	uint8_t *data = image->Data();
	int size = image->Width() * image->Height();
	for (int i=0; i<size; ++i)
	{
		heightMap->heights.at(i) = *data * vertScale;
		heightMap->heights.at(i) -= offset;
		if (heightMap->heights.at(i) < 0) heightMap->heights.at(i) = 0;
		data += image->Bpp();
	}
}

void MultiImageTerrainSource::BlendHeights(int destX, int destY, Array2D<float> *dest)
{
	// Get an array of all intersecting heightmaps and sort them from deepest
	// to shallowest floor.
	std::vector<HeightMap*> intersectingMaps;
	intersectingMaps.reserve(4);
	for (int i = 0; i < heightMaps.CurrentCapacity(); ++i)
	{
		if (   (heightMaps[i].x + heightMaps[i].heights.Width() < destX)
		    || (heightMaps[i].y + heightMaps[i].heights.Height() < destY)
		    || (heightMaps[i].x > destX + dest->Width())
			|| (heightMaps[i].y > destY + dest->Height())
			)
			continue;
		intersectingMaps.push_back(&heightMaps[i]);
	}
	HeightMapFloorComparer comparer;
	std::sort(intersectingMaps.begin(), intersectingMaps.end(), comparer);

	// Added copy each source into the dest:
	// If the height is currently less than our floor, we set it to the floor.
	// Add our value to the current height.
	for (int i = 0; i < intersectingMaps.size(); ++i)
	{
		// position of source relative to dest
		int sourceOfsX = intersectingMaps[i]->x - destX;
		int sourceOfsY = intersectingMaps[i]->y - destY;
		Array2D<float> *source = &intersectingMaps[i]->heights;

		int xMin = std::max(0, sourceOfsX);
		int yMin = std::max(0, sourceOfsY);
		int xMax = std::min(sourceOfsX + source->Size()[0], dest->Size()[0]);
		int yMax = std::min(sourceOfsY + source->Size()[1], dest->Size()[1]);

		for (int y = yMin; y < yMax; ++y)
		{
			int sourceY = y - sourceOfsY;
			for (int x = xMin; x < xMax; ++x)
			{
				int sourceX = x - sourceOfsX;
				if (source->at(sourceX, sourceY) == 0)
					continue;
				if (dest->at(x, y) < intersectingMaps[i]->floor)
					dest->at(x, y) = intersectingMaps[i]->floor;
				dest->at(x, y) += source->at(sourceX, sourceY);
			}
		}
	}
}

TerrainChunk *MultiImageTerrainSource::GetPiece(int x, int y, int w, int h)
{
	// Note: w and h are in tiles, but we're generating a grid of points
	// at the corners of those tiles.
	w += 1;
	h += 1;

	Array2D<float> bordered(w + 2, h + 2, -waterDepth);
	BlendHeights(x - 1, y - 1, &bordered);

	Array2D<float> heights(w, h);
	for (int by = 1; by < bordered.Size()[1] - 1; ++by)
	{
		int ym1 = by - 1;
		for (int bx = 1; bx < bordered.Size()[0] - 1; ++bx)
		{
			int xm1 = bx - 1;
			if (
				(bordered.at(bx, by) < 0)
				&& (
					(bordered.at(bx+0, by+1) > 0)
					|| (bordered.at(bx-1, by+0) > 0)
					|| (bordered.at(bx+0, by-1) > 0)
					|| (bordered.at(bx+1, by+0) > 0)
				)
			)
			{
				heights.at(xm1, ym1) = 0;
			}
			else
			{
				heights.at(xm1, ym1) = bordered.at(bx, by);
			}
		}
	}

	// For now the tile type rules are hard-coded.
	Array2D<TerrainChunk::TileTypeValue> tileTypes(w - 1, h - 1);
	for (int ty = 0; ty < tileTypes.Size()[1]; ty += 1)
	{
		for (int tx = 0; tx < tileTypes.Size()[0]; tx += 1)
		{
			//    a
			//   0 1 b
			// d 2 3
			//    c
			float h0 = heights.at(tx,     ty);
			float h1 = heights.at(tx + 1, ty);
			float h2 = heights.at(tx,     ty + 1);
			float h3 = heights.at(tx + 1, ty + 1);
			float scl = 1.0f / horizScale;
			float gA = (h1 - h0) * scl;
			float gB = (h3 - h1) * scl;
			float gC = (h2 - h3) * scl;
			float gD = (h2 - h0) * scl;
			if (gA < 0) gA *= -1;
			if (gB < 0) gB *= -1;
			if (gC < 0) gC *= -1;
			if (gD < 0) gD *= -1;
			float gradient = std::atan((gA + gB + gC + gD) * 0.25) / (M_PI * 0.5);
			float height = (h0 + h1 + h2 + h3) * 0.25;
			float minHeight = std::min(h0, std::min(h1, std::min(h2, h3)));
			float maxHeight = std::max(h0, std::max(h1, std::max(h2, h3)));
			float random1 = RandomField::CalculateF(x + tx, y + ty, 1);
			float random2 = RandomField::CalculateF(x + tx, y + ty, 2);

			if (maxHeight < 0.5)
			{
				tileTypes.at(tx, ty) = 2;
			}
			else if (height > 8.0 && height < 64 && gradient > 0.2)
			{
				tileTypes.at(tx, ty) = 3;
			}
			else
			{
				tileTypes.at(tx, ty) = 1;
			}
		}
	}

	return new TerrainChunk(&heights, &tileTypes, x, y, horizScale, 1);
}

void MultiImageTerrainSource::FreePiece(TerrainChunk *data)
{
	delete data;
}

float MultiImageTerrainSource::HorizScale()
{
	return horizScale;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
