/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "terrain/TerrainAdapter.hh"
#include <functional>
#include "terrain/GamecodeTerrain.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// TerrainAdapter
//-----------------------------------------------------------------------------

const std::string& TerrainAdapter::GetName()
{
	static std::string name("Terrain");
	return name;
}

std::map<std::string, IGamecodeAdapter::AdapterMethod> TerrainAdapter::GetGlobalFunctions()
{
	return {};
}

void TerrainAdapter::OnRegister(IGamecode *gamecode)
{
	gamecode->RegisterEngineType("Terrain", gamecode->CreateEngineType<GamecodeTerrain>()
		->Constructor(std::bind(&TerrainAdapter::ConstructTerrain, this, std::placeholders::_1, std::placeholders::_2))
		->AddMethod("setImageSource", &GamecodeTerrain::SetImageSource)
		->AddMethod("setProceduralSource", &GamecodeTerrain::SetProceduralSource)
		->AddMethod("getHeightAtPoint", &GamecodeTerrain::GetHeightAtPoint)
	);
}

IGamecodeEngineObject* TerrainAdapter::ConstructTerrain(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	return new GamecodeTerrain(resources, &cacheFactory);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
