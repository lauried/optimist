/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <map>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * A type of terrain. Each tile of terrain has only one type.
 * (Technically two if one triangle is water).
 */
class TileType {
public:
	// TODO: Colour and alternate colour.

private:
};

/**
 * The main data structure to deal with the properties of the surface of
 * terrain.
 */
class TerrainTypeData {
public:
	void SetTileType(int index, TileType &type);
	TileType* GetTileType(int index);

private:
	std::map<int, TileType> tileTypes;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
