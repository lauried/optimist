/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include <cassert>
#include <cmath>
#include <iostream>
#include "terrain/TerrainChunk.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

void TerrainChunk::SetBounds()
{
	float minHeight = heights.at(0);
	float maxHeight = heights.at(0);
	for (int i=0; i<heights.Width()*heights.Height(); ++i)
	{
		if (heights.at(i) < minHeight)
		{
			minHeight = heights.at(i);
		}
		if (heights.at(i) > maxHeight)
		{
			maxHeight = heights.at(i);
		}
	}

	bounds.mins[0] = 0;
	bounds.mins[1] = 0;
	bounds.mins[2] = minHeight;

	bounds.maxs[0] = tiles.Width() * horizScale;
	bounds.maxs[1] = tiles.Height() * horizScale;
	bounds.maxs[2] = (maxHeight > 0.0f) ? maxHeight : 0.0f;

	origin[0] = (float)tileOrigin[0] * horizScale;
	origin[1] = (float)tileOrigin[1] * horizScale;
	origin[2] = 0;

	for (int i = 0; i < 3; ++i)
	{
		assert(bounds.maxs[i] >= bounds.mins[i]);
	}
}

void TerrainChunk::SetDiagonals()
{
	tiles.SetSize(heights.Width() - 1, heights.Height() - 1);
	for (int y=0; y<tiles.Height(); ++y)
	{
		for (int x=0; x<tiles.Width(); ++x)
		{
			TileInfo *info = &tiles.at(x, y);

			// Diagonality
			//   tl    tr    tl    tr
			// 0 -------   1 -------
			//   | \   |     |   / |
			//   |   \ |     | /   |
			//   -------     -------
			//   bl    br    bl    br

			// Get top left, top right, bottom left, bottom right
			float tl = heights.at(x,   y);
			float tr = heights.at(x+1, y);
			float bl = heights.at(x,   y+1);
			float br = heights.at(x+1, y+1);

			// The closest pair of opposite heights determine which way we
			// split.
			float tlbr = tl - br; if (tlbr < 0) tlbr *= -1;
			float trbl = tr - bl; if (trbl < 0) trbl *= -1;
			info->diagonality = (tlbr > trbl) ? 1 : 0;

			// Calculate water tiles.
			if (info->diagonality == 0)
			{
				info->isWater[0] = (tl < 0 || tr < 0 || br < 0);
				info->isWater[1] = (bl < 0 || br < 0 || tl < 0);
			}
			else
			{
				info->isWater[0] = (tl < 0 || tr < 0 || bl < 0);
				info->isWater[1] = (bl < 0 || br < 0 || tr < 0);
			}

			// The first triangle is the one with two verts on the top row.
			// Terrain type by mean height.
			float meanHeight = (tl + tr + bl + br) * 0.25;
			info->terrainType = 1;
			if (meanHeight > 128)
			{
				info->terrainType = 2;
			}
		}
	}
}

void TerrainChunk::SetTileTypes(Array2D<TileTypeValue> *tileTypes)
{
	for (int y = 0; y < tiles.Size()[1]; y += 1)
	{
		for (int x = 0; x < tiles.Size()[0]; x += 1)
		{
			tiles.at(x, y).terrainType = tileTypes->at(x, y);
		}
	}
}

TerrainChunk::TerrainChunk(int sizeX, int sizeY, int posX, int posY, float hScale, float vScale)
{
	heights.SetSize(sizeX, sizeY, 0.0f);
	horizScale = hScale;
	vertScale = vScale;
	SetDiagonals();
	SetBounds();
}

TerrainChunk::TerrainChunk(Image *image, int tx, int ty, float hScale, float vScale)
{
	heights.SetSize(image->Width(), image->Height());

	horizScale = hScale;
	vertScale = vScale;

	uint8_t *data = image->Data();
	int size = image->Width() * image->Height();
	for (int i=0; i<size; ++i)
	{
		heights.at(i) = *data * vertScale;
		data += image->Bpp();
	}
	tileOrigin.Set(tx, ty);

	SetDiagonals();
	SetBounds();
}

TerrainChunk::TerrainChunk(Array2D<float> *data, int tx, int ty, float hScale, float vScale)
{
	horizScale = hScale;
	vertScale = vScale;

	data->MoveTo(heights);
	int size = heights.Width() * heights.Height();
	for (int i = 0; i < size; ++i)
	{
		heights.at(i) *= vertScale;
	}
	tileOrigin.Set(tx, ty);

	SetDiagonals();
	SetBounds();
}

TerrainChunk::TerrainChunk(Array2D<float> *data, Array2D<TileTypeValue> *tileTypes, int tx, int ty, float hScale, float vScale)
{
	horizScale = hScale;
	vertScale = vScale;

	data->MoveTo(heights);
	int size = heights.Width() * heights.Height();
	for (int i = 0; i < size; ++i)
	{
		heights.at(i) *= vertScale;
	}
	tileOrigin.Set(tx, ty);

	SetDiagonals();
	SetTileTypes(tileTypes);
	SetBounds();
}


TerrainChunk::~TerrainChunk()
{
}

void TerrainChunk::GetTriangle(int x, int y, int triangle, float *vertData, int vertStep, int *type, int *diagonality)
{
	TileInfo *info = &tiles.at(x, y);
	int x1, x2, x3, y1, y2, y3;
	*diagonality = info->diagonality;
	// The order is always CCW with the right-angled vertex first.
	if (info->diagonality == 0)
	{
		if (triangle == 0)
		{
			x1 = 1; y1 = 0;
			x2 = 0; y2 = 0;
			x3 = 1; y3 = 1;
		}
		else
		{
			x1 = 0; y1 = 1;
			x2 = 1; y2 = 1;
			x3 = 0; y3 = 0;
		}
	}
	else
	{
		if (triangle == 0)
		{
			x1 = 0; y1 = 0;
			x2 = 0; y2 = 1;
			x3 = 1; y3 = 0;
		}
		else
		{
			x1 = 1; y1 = 1;
			x2 = 1; y2 = 0;
			x3 = 0; y3 = 1;
		}
	}

	float *vertPtr = vertData;
	vertPtr[0] = (x + x1) * horizScale;
	vertPtr[1] = (y + y1) * horizScale;
	vertPtr[2] = heights.at(x + x1, y + y1);
	vertPtr += vertStep;
	vertPtr[0] = (x + x2) * horizScale;
	vertPtr[1] = (y + y2) * horizScale;
	vertPtr[2] = heights.at(x + x2, y + y2);
	vertPtr += vertStep;
	vertPtr[0] = (x + x3) * horizScale;
	vertPtr[1] = (y + y3) * horizScale;
	vertPtr[2] = heights.at(x + x3, y + y3);

	if (info->isWater[triangle])
		*type = 0;
	else
		*type = info->terrainType;
}

inline int MyModf(float value, float *fraction)
{
	float ival = floor((double)value);
	*fraction = value - ival;
	return (int)ival;
}

inline float Lerp(float baseHeight, float frac1, float height1, float frac2, float height2)
{
	return baseHeight + (height1 - baseHeight) * frac1 + (height2 - baseHeight) * frac2;
}

float TerrainChunk::GetTerrainHeight(float x, float y)
{
	float fracX, fracY;
	int tileX = MyModf(x / horizScale, &fracX);
	int tileY = MyModf(y / horizScale, &fracY);

	if (tileX == tiles.Width() && fracX == 0.0)
	{
		--tileX;
		fracX = 1.0;
	}
	if (tileY == tiles.Height() && fracY == 0.0)
	{
		--tileY;
		fracY = 1.0;
	}

	if (tileX < 0 || tileX >= tiles.Width() || tileY < 0 || tileY >= tiles.Height())
	{
		// This shouldn't be reached if we check first with ContainsPoint,
		// so we can warn here that we got the wrong height.
		std::cout << "Internal error warning: TerrainChunk::GetTerrainHeight - ";
		std::cout << "box " << origin[0] << "," << origin[1]
			<< " mins " << bounds.mins[0] << "," << bounds.mins[1]
			<< " maxs " << bounds.maxs[0] << "," << bounds.maxs[1]
			<< " doesn't contain " << x << "," << y << std::endl;
		return -10;
	}

	// Diagonality
	//   tl    tr    tl    tr
	// 0 -------   1 -------
	//   | \   |     |   / |
	//   |   \ |     | /   |
	//   -------     -------
	//   bl    br    bl    br
	float tl = heights.at(tileX,   tileY);
	float tr = heights.at(tileX+1, tileY);
	float bl = heights.at(tileX,   tileY+1);
	float br = heights.at(tileX+1, tileY+1);

	// Diagonality of tile determines how we determine what triangle we're in.
	// Then it's just a case of how we handle the fractions.
	if (tiles.at(tileX, tileY).diagonality == 0)
	{
		if ((1-fracX) + fracY < 1.0)
			return Lerp(tr, 1-fracX, tl, fracY, br);
		else
			return Lerp(bl, fracX, br, 1-fracY, tl);
	}
	else
	{
		if (fracX + fracY < 1.0)
			return Lerp(tl, fracX, tr, fracY, bl);
		else
			return Lerp(br, 1-fracX, bl, 1-fracY, tr);
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
