/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "terrain/TerrainChunk.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Represents a near-infinite plane of terrain from which pieces can be
 * retrieved in the form of TerrainChunk.
 * This implementation takes an image as the source.
 */
class ITerrainSource {
public:
	virtual ~ITerrainSource() { };

	/**
	 * This must always return some data even if it's outside the range of
	 * whatever the source data is. Using images, if it's out of range we
	 * return water.
	 * Coordinates are in terrain tiles.
	 */
	virtual TerrainChunk *GetPiece(int x, int y, int w, int h) = 0;

	/**
	 * Frees a piece of data that we retrieved from GetPiece.
	 * Since we created it, we're responsible for deleting it in an appropriate
	 * way.
	 */
	virtual void FreePiece(TerrainChunk *data) = 0;

	/**
	 * The horizontal scale of tiles in world units.
	 */
	virtual float HorizScale() = 0;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
