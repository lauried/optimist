/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "terrain/ITerrainSource.hh"
#include "terrain/procedural/ProceduralTerrainType.hh"
#include "terrain/procedural/IProceduralTerrainTypeMap.hh"
#include "terrain/procedural/TestProceduralTerrainMap.hh"
#include "terrain/TerrainChunk.hh"
#include "terrain/TerrainData.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class ProceduralTerrainSource : public ITerrainSource {
public:
	ProceduralTerrainSource();

	// ITerrainSource
	TerrainChunk *GetPiece(int x, int y, int w, int h);
	void FreePiece(TerrainChunk *data);
	float HorizScale();

private:
	struct BoundInfo {
		Vector2I coordMin, coordMax;
		Vector2F fracMin, fracMax;
	};

	float horizScale, vertScale;

	// For now we just load a single terrain type.
	ProceduralTerrainType defaultTerrain;
	TestProceduralTerrainMap defaultMap;

	void GenerateMultiTerrainType(IProceduralTerrainTypeMap *map, int x, int y, TerrainData *data);
	void GenerateSingleTerrainType(ProceduralTerrainType *type, int x, int y, TerrainData *data);
	void FlattenWaterBoundary(TerrainData *data);
	void RenderTypes(const Array2D<ProceduralTerrainType*> &types, int x, int y, int w, int h, Array2D<TerrainData*> *data, std::vector<TerrainData*> *uniqueData);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
