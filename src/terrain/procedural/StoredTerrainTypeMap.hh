/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/
//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "terrain/procedural/IEditableTerrainTypeMap.hh"
#include "filesystem/IFilesystem.hh"
#include "log/ILogTarget.hh"
#include "library/containers/Array2D.hh"
#include "library/Exceptions.hh"
#include <map>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class StoredTerrainTypeMap : public IEditableTerrainTypeMap {
public:
	StoredTerrainTypeMap();
	~StoredTerrainTypeMap();

	void Load(IFile *file);
	void LoadText(IFile *file);
	void Save(IFile *file);

	// IEditableTerrainSurfaceTypeMap
	virtual int GetGridSizeLog2() { return gridSize; }
	virtual void SetGridSizeLog2(int size) { gridSize = size; }
    virtual void GetValues(int x, int y, Array2D<int> *types, Array2D<float> *heights);
    virtual void SetValue(int x, int y, int type, float height);

private:
	const int CHUNK_SIZE_LOG_2 = 6;
	const int CHUNK_SIZE = 1 << CHUNK_SIZE_LOG_2;
	const int CHUNK_COORD_MASK = CUNK_SIZE - 1;

	struct Chunk {
		int x, y;
		Array2D<int> *types;
		Array2D<float> *heights;
	};

	int gridSize;
	int defaultType;
	float defaultHeight;
	std::map<unit32_t, Chunk> chunks;

	uint32_t IndexForCoordinates(int x, int y);
	///< Returns nullptr if it can't find the chunk.
	Chunk* FindChunk(int x, int y);
	///< Creates the chunk if it doesn't already exist.
	Chunk* FindOrCreateChunk(int x, int y);

	void Clear();
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

