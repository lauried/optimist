/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/
#include "terrain/procedural/ProceduralTerrainTypePicker.hh"
#include <vector>
#include "library/text/String.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

ProceduralTerrainTypePicker::ProceduralTerrainTypePicker(IFilesystem *filesystem)
	: filesystem(filesystem)
{
	Refresh();
}

void ProceduralTerrainTypePicker::Refresh()
{
	types.clear();

	std::vector<std::string> files;
	filesystem->GetFilesInDirectory("", &files);
	for (int i = 0; i < files.size(); i += 1)
	{
		std::vector<std::string> tokens;
		String::Split(files[i], &tokens, "-", 2);
		if (tokens.size() > 1 && IsNumeric(tokens[0]))
		{
            types.insert(std::make_pair(String::ToInt(tokens[0]), files[i]));
		}
	}
}

IFile *ProceduralTerrainTypePicker::OpenType(int index)
{
	auto it = types.find(index);
	if (it == types.end())
	{
		return nullptr;
	}
	return filesystem->Open(it->second);
}

std::string ProceduralTerrainTypePicker::GetType(int index)
{
	auto it = types.find(index);
	if (it == types.end())
	{
		return nullTypeInfo;
	}
	return it->second;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
