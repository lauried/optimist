/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/
#include "terrain/procedural/ProceduralTerrainTypeManager.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

ProceduralTerrainTypeManager::ProceduralTerrainTypeManager(ProceduralTerrainTypePicker *picker, IEditableTerrainTypeMap *map)
	: picker(picker)
	, map(map)
{
	// TODO: Initialise default terrain type.
	Refresh();
}

void ProceduralTerrainTypeManager::Refresh()
{
	picker->Refresh();
	auto pickerTypes = picker->GetTypes();
	for (auto it = pickerTypes.begin(); it != pickerTypes.end(); ++it)
	{
		File *file = picker->OpenType(it->first);
		// TODO: Load the file and then create a terrain type with it.
		// TODO: If we can't load it then load some default file.
	}
}

int ProceduralTerrainTypeManager::GetGridSizeLog2()
{
	return map->GetGridSizeLog2();
}

void ProceduralTerrainTypeManager::GetSurfaceType(int x, int y, Array2D<ProceduralTerrainType*> *destTypes, Array2D<float> *destHeights)
{
	Array2D<int> types(destTypes->Width(), destTypes->Height());
	map->GetValues(x, y, &types, destHeights);

	for (int i = 0; i < types.NumElements(); i += 1)
	{
		// TODO: Get the appropriate object and put it into the heights.
		// TODO: If we don't have the type then use the default one.
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
