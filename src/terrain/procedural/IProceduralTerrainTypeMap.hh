/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "terrain/procedural/ProceduralTerrainType.hh"
#include "library/containers/Array2D.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Provides a high scale map of what surface type to use at what coordinates,
 * as well as an extra altitude that can be added to the terrain.
 */
class IProceduralTerrainTypeMap {
public:
	/**
	 * Returns the intended number of terrain tiles per map cell, log 2.
	 * The actual size of the grid will be 1 << GetGridSizeLog2().
	 */
	virtual int GetGridSizeLog2() = 0;

	/**
	 * Writes the arrays with the specified types.
	 * The sizes of the arrays specify the size of the region to query.
	 */
	virtual void GetSurfaceType(int x, int y, Array2D<ProceduralTerrainType*> *destTypes, Array2D<float> *destHeights) = 0;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

