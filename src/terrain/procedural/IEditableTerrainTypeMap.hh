/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/
//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "library/containers/Array2D.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Interface for editing a surface type map.
 */
class IEditableTerrainTypeMap {
public:
	/**
	 * Gets the grid size.
	 */
	virtual int GetGridSizeLog2() = 0;

	/**
	 * Sets the grid size.
	 */
	virtual void SetGridSizeLog2(int size) = 0;

    /**
     * Gets a region of cells.
     */
	virtual void GetValues(int x, int y, Array2D<int> *types, Array2D<float> *heights) = 0;

    /**
     * Sets a single cell.
     */
    virtual void SetValue(int x, int y, int type, float height) = 0;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

