/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "terrain/procedural/ProceduralTerrainSource.hh"
#include "terrain/procedural/TerrainBlender.hh"
#include "library/maths/GridSplit.hh"
#include <iostream>
#include <cmath>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ITerrainSource
//-----------------------------------------------------------------------------

#define MULTI_TERRAIN 1

ProceduralTerrainSource::ProceduralTerrainSource()
{
	horizScale = 8.0f;
	vertScale = 8.0f;

	// Formerly "game/test/terrain/test.terrain"
	char data[] =
#if MULTI_TERRAIN
		"terrain noise 64 1.0 100\n"
		"terrain shape 0.50 0.52 0.54 0.56 1.19 1.21 1.23 1.25\n"
		"texture 1\n"
		"texture 3 gradient 0.05 to 0.2 probability 0.01 1 400\n"
		"texture 3 gradient above 0.4 probability 0.95 1 400\n";
#else
		"# generate ordinary noise\n"
		"terrain noise  2 0.5 100\n"
		"terrain noise  4 1.0 101\n"
		"terrain noise  8 2.0 102\n"
		"terrain noise 16 4.0 103\n"
		"terrain noise 32 8.0 104\n"
		"# give it a very steep edge, and also change the range so it's entirely positive.\n"
		"terrain shape 0.50 0.52 0.54 0.56 1.19 1.21 1.23 1.25\n"
		"# multiply it by a single low frequency, to give the cliffs varying heights\n"
		"buffer new\n"
		"terrain noise 64 16.0 200\n"
		"buffer merge mul\n"
		"# add on some ordinary noise\n"
		"terrain noise  2 0.250 300\n"
		"terrain noise  4 0.500 301\n"
		"terrain noise  8 1.000 302\n"
		"terrain noise 16 2.000 303\n"
		"# texture it simply\n"
		"# texture indexes correspond to tile types in terrain_types.txt\n"
		"texture 1\n"
		"texture 3 gradient 0.05 to 0.2 probability 0.01 1 400\n"
		"texture 3 gradient above 0.4 probability 0.95 1 400\n";
#endif

	defaultTerrain.LoadData(data, sizeof(data) / sizeof(char), "embedded");
}

/**
 * This must always return some data even if it's outside the range of
 * whatever the source data is.
 */
TerrainChunk *ProceduralTerrainSource::GetPiece(int x, int y, int w, int h)
{
	// First we need an extra row and column, because the width and height are
	// in tiles, not points.
	w += 1;
	h += 1;

	// Next, we need to generate terrain with a border because flattening water
	// boundaries requires it.
	TerrainData bordered(w + 2, h + 2);
#if MULTI_TERRAIN
	GenerateMultiTerrainType(&defaultMap, x - 1, y - 1, &bordered);
#else
	GenerateSingleTerrainType(&defaultTerrain, x - 1, y - 1, &bordered);
#endif
	FlattenWaterBoundary(&bordered);
	// Finally we extract the bordered region.
	TerrainData data(bordered, 1, 1, w, h);
	return new TerrainChunk(&data.heights, &data.types, x, y, horizScale, vertScale);
}

/**
 * Frees a piece of data that we retrieved from GetPiece.
 * Since we created it, we're responsible for deleting it in an appropriate
 * way.
 */
void ProceduralTerrainSource::FreePiece(TerrainChunk *data)
{
	delete data;
}

/**
 * The horizontal scale of tiles in world units.
 */
float ProceduralTerrainSource::HorizScale()
{
	return horizScale;
}

/**
 * Generates a chunk of a single terrain type.
 */
void ProceduralTerrainSource::GenerateSingleTerrainType(ProceduralTerrainType *type, int x, int y, TerrainData *data)
{
	int w = data->Width();
	int h = data->Height();

	ProceduralTerrainTypeBuildParameters parms;
	parms.x = x;
	parms.y = y;
	parms.width = w;
	parms.height = h;
	parms.scale = 1;
	parms.terrain = data->heights.RawElements();
	parms.textures = data->types.RawElements();

	type->BuildChunk(&parms);
}

/**
 * Creates TerrainData for the array of types.
 * Used as part of GenerateMultiTerrainType.
 * Unique types are de-duped, and we populate a list of unique ones so they can
 * be freed.
 */
void ProceduralTerrainSource::RenderTypes(const Array2D<ProceduralTerrainType*> &types, int x, int y, int w, int h, Array2D<TerrainData*> *data, std::vector<TerrainData*> *uniqueData)
{
	int size = types.Width() * types.Height();
	for (int make = 0; make < size; make += 1)
	{
		ProceduralTerrainType *type = types.at(make);

		// If we previously made this type, copy its pointer.
		bool copied = false;
		for (int look = 0; look < make; look += 1)
		{
			if (types.at(look) == type)
			{
				data->at(make) = data->at(look);
				copied = true;
			}
		}
		// If we copied, skip making it.
		if (copied)
		{
			continue;
		}

		// Make it and add it to the unique list.
		TerrainData *newData = new TerrainData(w, h);
		if (type != nullptr)
		{
			GenerateSingleTerrainType(type, x, y, newData);
		}
		data->at(make) = newData;
		uniqueData->push_back(newData);
	}

	if (uniqueData->size() < 1)
	{
		throw std::logic_error("No unique data rendered - probably some error in ProceduralTerrainSource::RenderTypes");
	}
}

/**
 * Generates a chunk of multiple types blended together.
 */
void ProceduralTerrainSource::GenerateMultiTerrainType(IProceduralTerrainTypeMap *map, int x, int y, TerrainData *data)
{
	int gridPower = map->GetGridSizeLog2();

	Vector2I dataWorldCoord(x, y);

	int w = data->Width();
	int h = data->Height();

	GridSplit gridSplit;
	TerrainBlender blender;

	std::vector<GridSplit::BoundInfo> bounds;
	gridSplit.GenerateBounds(x, y, x + w - 1, y + h - 1, 1 << gridPower, &bounds);

	for (int i = 0; i < bounds.size(); i += 1)
	{
		Array2D<ProceduralTerrainType*> types(2, 2);
		Array2D<float> heights(2, 2);

		int mapX = bounds[i].coordMin[0] >> gridPower;
		int mapY = bounds[i].coordMin[1] >> gridPower;
		map->GetSurfaceType(mapX, mapY, &types, &heights);

		std::vector<TerrainData*> uniqueData;
		Array2D<TerrainData*> gridData(2, 2);

		Vector2I subChunkSize = bounds[i].coordMax - bounds[i].coordMin;
		RenderTypes(types, bounds[i].coordMin[0], bounds[i].coordMin[1], subChunkSize[0], subChunkSize[1], &gridData, &uniqueData);

		Vector2I subChunkOffset = bounds[i].coordMin - dataWorldCoord;

		if (uniqueData.size() == 1)
		{
			data->SetRegion(*uniqueData[0], subChunkOffset[0], subChunkOffset[1]);
		}
		else
		{
			TerrainBlender::TerrainBlendInfo info;
			info.data[0] = gridData.at(0, 0);
			info.data[1] = gridData.at(1, 0);
			info.data[2] = gridData.at(0, 1);
			info.data[3] = gridData.at(1, 1);

			info.fracMin = bounds[i].fracMin;
			info.fracMax = bounds[i].fracMax;

			info.gridSize = 1 << gridPower;

			blender.BlendTerrain(info, bounds[i].coordMin, data, subChunkOffset);
		}

		blender.AddHeight(heights.at(0), heights.at(1), heights.at(2), heights.at(3), data);

		// Tidy up the unique data we created.
		for (int j = 0; j < uniqueData.size(); j += 1)
		{
			delete uniqueData.at(j);
		}
	}
}

/**
 * Flattens the boundary with water so that it occurs on triangle boundaries.
 */
void ProceduralTerrainSource::FlattenWaterBoundary(TerrainData *data)
{
	int w = data->Width();
	int h = data->Height();
	Array2D<float> *heights = &data->heights;

	for (int by = 1; by < h - 1; by += 1)
	{
		for (int bx = 1; bx < w - 1; bx += 1)
		{
			if (heights->at(bx, by) < 0
				&& (
					   heights->at(bx+1, by) > 0
					|| heights->at(bx-1, by) > 0
					|| heights->at(bx, by+1) > 0
					|| heights->at(bx, by-1) > 0
					)
				)
			{
				heights->at(bx, by) = 0;
			}
		}
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

