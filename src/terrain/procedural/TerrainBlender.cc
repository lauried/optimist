/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "terrain/procedural/TerrainBlender.hh"
#include "library/maths/RandomField.hh"
#include <iostream>
#include <cmath>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

inline Vector4F LerpWeights(float fracX, float fracY)
{
	static Vector4F weight0(1, 0, 0, 0);
	static Vector4F weight1(0, 1, 0, 0);
	static Vector4F weight2(0, 0, 1, 0);
	static Vector4F weight3(0, 0, 0, 1);

	float iFracX = 1.0f - fracX;
	Vector4F blend0 = weight0 * fracX + weight1 * iFracX;
	Vector4F blend1 = weight2 * fracX + weight3 * iFracX;

	float iFracY = 1.0f - fracY;
	return blend0 * fracY + blend1 * iFracY;
}

inline int BestTileTypeIndex(int x, int y, Vector4F weights)
{
	weights[0] += RandomField::CalculateF(x, y, 0);
	weights[1] += RandomField::CalculateF(x, y, 1);
	weights[2] += RandomField::CalculateF(x, y, 2);
	weights[3] += RandomField::CalculateF(x, y, 3);

	if (weights[0] > weights[1] && weights[0] > weights[2] && weights[0] > weights[3])
	{
		return 0;
	}
	if (weights[1] > weights[2] && weights[1] > weights[3])
	{
		return 1;
	}
	if (weights[2] > weights[3])
	{
		return 2;
	}
	return 3;
}

inline float SumVec4(const Vector4F &v)
{
	return v[0] + v[1] + v[2] + v[3];
}

#if 0
#define BLENDMAP(x) x
#else
#define BLENDMAP(x) (0.5 - 0.5 * std::cos(x * M_PI))
#endif

void TerrainBlender::BlendTerrain(const TerrainBlender::TerrainBlendInfo &source, Vector2I worldCoords, TerrainData *destination, Vector2I offsetInDest)
{
	Vector2I size = source.data[0]->heights.Size();

	Array2D<Vector4F> weights(2, 2);
	weights.at(0, 0) = LerpWeights(source.fracMin[0], source.fracMin[1]);
	weights.at(1, 0) = LerpWeights(source.fracMax[0], source.fracMin[1]);
	weights.at(0, 1) = LerpWeights(source.fracMin[0], source.fracMax[1]);
	weights.at(1, 1) = LerpWeights(source.fracMax[0], source.fracMax[1]);

	Array2D<float> *heights[] = {
		&(source.data[0]->heights),
		&(source.data[1]->heights),
		&(source.data[2]->heights),
		&(source.data[3]->heights),
	};
	Array2D<float> *destinationHeights = &(destination->heights);

	Array2D<uint16_t> *types[] = {
		&(source.data[0]->types),
		&(source.data[1]->types),
		&(source.data[2]->types),
		&(source.data[3]->types),
	};
	Array2D<uint16_t> *destinationTypes = &(destination->types);

	float xStep = 1.0f / (float)size[0];
	float yStep = 1.0f / (float)size[1];

	Vector4F stepMinX = (weights.at(0, 1) - weights.at(0, 0)) * yStep;
	Vector4F stepMaxX = (weights.at(1, 1) - weights.at(1, 0)) * yStep;
	Vector4F weightMinX = weights.at(0, 0);
	Vector4F weightMaxX = weights.at(1, 0);

	for (int sourceY = 0; sourceY < size[1]; sourceY += 1)
	{
		int destY = sourceY + offsetInDest[1];
		int absY = sourceY + worldCoords[1];
		Vector4F weightStep = (weightMaxX - weightMinX) * xStep;
		Vector4F weight = weightMinX;
		for (int sourceX = 0; sourceX < size[0]; sourceX += 1)
		{
			int destX = sourceX + offsetInDest[0];
			int absX = sourceX + worldCoords[0];

			destinationHeights->at(destX, destY)
				= heights[0]->at(sourceX, sourceY) * BLENDMAP(weight[0])
				+ heights[1]->at(sourceX, sourceY) * BLENDMAP(weight[1])
				+ heights[2]->at(sourceX, sourceY) * BLENDMAP(weight[2])
				+ heights[3]->at(sourceX, sourceY) * BLENDMAP(weight[3]);

			destinationTypes->at(destX, destY) = types[BestTileTypeIndex(absX, absY, weight)]->at(sourceX, sourceY);

			weight += weightStep;
		}
		weightMinX += stepMinX;
		weightMaxX += stepMaxX;
	}
}

void TerrainBlender::AddHeight(float height1, float height2, float height3, float height4, TerrainData *destination)
{
	Vector2I size = destination->heights.Size();
	Array2D<float> *destinationHeights = &(destination->heights);

#if 0
	// Linear

	float xStep = 1.0f / (float)size[0];
	float yStep = 1.0f / (float)size[1];

	float stepMinX = (height3 - height1) * yStep;
	float heightMinX = height1;

	float stepMaxX = (height4 - height2) * yStep;
	float heightMaxX = height2;

	for (int y = 0; y < size[1]; y +=1 )
	{
		float step = (heightMaxX - heightMinX) * xStep;
		float height = heightMinX;

		for (int x = 0; x < size[0]; x += 1)
		{
			destinationHeights->at(x, y) += height;
			height += step;
		}
		heightMinX += stepMinX;
		heightMaxX += stepMaxX;
	}
#else
	// Cosine

	float xStep = 1.0f / (float)size[0];
	float yStep = 1.0f / (float)size[1];
	float xBlend = 0;
	float yBlend = 0;
	for (int y = 0; y < size[1]; y += 1)
	{
		xBlend = 0;

		float yFactor = 0.5 + 0.5 * std::cos(yBlend * M_PI);
		float yInvFactor = 1.0 - yFactor;

		for (int x = 0; x < size[0]; x += 1)
		{
			float xFactor = 0.5 + 0.5 * std::cos(xBlend * M_PI);
			float xInvFactor = 1.0 - xFactor;

			float height = height1 * xFactor * yFactor
				+ height2 * xInvFactor * yFactor
				+ height3 * xFactor * yInvFactor
				+ height4 * xInvFactor * yInvFactor;

			destinationHeights->at(x, y) += height;

			xBlend += xStep;
		}
		yBlend += yStep;
	}
#endif
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
