/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "terrain/procedural/TestProceduralTerrainMap.hh"
#include <string>
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

TestProceduralTerrainMap::TestProceduralTerrainMap()
{
	std::string type0 = "terrain noise 2 1.0 100\n"
		"terrain shape 0.50 0.52 0.54 0.56 1.19 1.21 1.23 1.25\n"
		"texture 1\n"
		"texture 3 gradient 0.05 to 0.2 probability 0.01 1 400\n"
		"texture 3 gradient above 0.4 probability 0.95 1 400\n";

	std::string type1 = "terrain noise 64 4.0 101\n"
		"terrain shape 0.50 0.52 0.54 0.56 1.19 1.21 1.23 1.25\n"
		"texture 1\n"
		"texture 3 gradient 0.05 to 0.2 probability 0.01 1 400\n"
		"texture 3 gradient above 0.4 probability 0.95 1 400\n";

	std::string type2 = "terrain noise 8 1.0 102\n"
		"terrain shape 0.50 0.52 0.54 0.56 1.19 1.21 1.23 1.25\n"
		"texture 1\n"
		"texture 3 gradient 0.05 to 0.2 probability 0.01 1 400\n"
		"texture 3 gradient above 0.4 probability 0.95 1 400\n";

	std::string type3 = //"terrain noise  16 1.0 103\n"
		//"terrain shape 0.50 0.52 0.54 0.56 1.19 1.21 1.23 1.25\n"
		"texture 1\n"
		"texture 3 gradient 0.05 to 0.2 probability 0.01 1 400\n"
		"texture 3 gradient above 0.4 probability 0.95 1 400\n";

	types[0].LoadData(type0.c_str(), type0.size(), "embedded");
	types[1].LoadData(type1.c_str(), type1.size(), "embedded");
	types[2].LoadData(type2.c_str(), type2.size(), "embedded");
	types[3].LoadData(type3.c_str(), type3.size(), "embedded");
}

TestProceduralTerrainMap::~TestProceduralTerrainMap()
{
}

int TestProceduralTerrainMap::GetGridSizeLog2()
{
	return 5;
}

void TestProceduralTerrainMap::GetSurfaceType(int x, int y, Array2D<ProceduralTerrainType*> *destTypes, Array2D<float> *destHeights)
{
	if (destTypes->Width() != destHeights->Width() || destTypes->Height() != destHeights->Height())
	{
		throw new std::logic_error("Types and Heights arrays must be the same size");
	}

	int width = destTypes->Width();
	int height = destTypes->Height();

	for (int destY = 0; destY < height; destY += 1)
	{
		int absY = destY + y;

		for (int destX = 0; destX < width; destX += 1)
		{
			int absX = destX + x;
			int index = ((absY & 1) << 1) + (absX & 1);
			destTypes->at(destX, destY) = &types[index & 3];
			destHeights->at(destX, destY) = 0;
		}
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
