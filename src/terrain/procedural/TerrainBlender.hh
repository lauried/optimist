/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "terrain/TerrainData.hh"
#include "library/geometry/Vector4.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Blends terrain data together at low frequencies.
 */
class TerrainBlender {
public:
	struct TerrainBlendInfo {
		/**
		 * Each object must have the same dimensions.
		 */
		TerrainData *data[4];

		/**
		 * Position of the data as fraction through the grid tile.
		 */
		Vector2F fracMin, fracMax;

		/**
		 * Grid size. This makes it easier to set the step.
		 */
		int gridSize;
	};

	/**
	 * Blends four individual pieces of terrain together using their lerped weights.
	 * The source tiles must fit inside the destination.
	 */
	void BlendTerrain(const TerrainBlender::TerrainBlendInfo &source, Vector2I worldCoords, TerrainData *destination, Vector2I offsetInDest);

	/**
	 * Lerps the four heights and adds them to the destination terrain.
	 */
	void AddHeight(float height1, float height2, float height3, float height4, TerrainData *destination);

private:
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

