/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/
//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <map>
#include "terrain/procedural/IEditableTerrainTypeMap.hh"
#include "terrain/procedural/ProceduralTerrainTypePicker.hh"
#include "terrain/procedrual/IProceduralTerrainTypeMap.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class ProceduralTerrainTypeManager : public IProceduralTerrainTypeMap {
public:
	ProceduralTerrainTypeManager(ProceduralTerrainTypePicker *picker, IEditableTerrainTypeMap *map);

	void Refresh();

	// IProceduralTerrainTypeMap
	virtual int GetGridSizeLog2();
	virtual void GetSurfaceType(int x, int y, Array2D<ProceduralTerrainType*> *destTypes, Array2D<float> *destHeights);

private:
	ProceduralTerrainTypePicker *picker;
	IEditableTerrainTypeMap *map;
	std::map<int, ProceduralTerrainType*> types;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

