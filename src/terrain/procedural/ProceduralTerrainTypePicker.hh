/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/
//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <map>
#include "filesystem/IFilesystem.hh"
#include "terrain/procedural/ProceduralTerrainType.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class ProceduralTerrainTypePicker {
public:
	/**
	 * Construct with a filesystem whose current working directory contains all
	 * of the available terrain types.
	 */
	ProceduralTerrainTypePicker(IFilesystem *filesystem);

	IFile *OpenType(int index);
	std::string GetType(int index);
	const std::map<int, std::string> &GetTypes() { return types; }

	void Refresh();

private:
	IFilesystem *filesystem;
	std::map<int, std::string> types;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

