/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/
#include "terrain/procedural/StoredTerrainTypeMap.hh"
#include <vector>
#include "library/maths/GridSplit.hh"
#include "minecraft/NbtFile.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

StoredTerrainTypeMap::StoredTerrainTypeMap()
{
	defaultValue = TerrainValue(0, -128.0f);
	gridSize = 8;
}

StoredTerrainTypeMap::~StoredTerrainTypeMap()
{
	for (auto it = chunks.begin(); it != chunks.end(); ++it)
	{
		delete it->second.heights;
		delete it->second.types;
	}
}

/*
Nbt structure:

map COMPOUND {
	defaults COMPOUND {
		type INT
		height FLOAT
	}
	chunks LIST(COMPOUND) [
		{
			x INT
			y INT
			heights LIST(FLOAT) [ ... ]
			types LIST(FLOAT) [ ... ]
		}
	]
}

*/

void StoredTerrainTypeMap::Load(IFile *file)
{
	Clear();

	// TODO: Check if it's the text format and load that way instead if so.
	// Alternately different methods for loading and saving, and the factory
	// determines what the type is.

	NbtFile nbt;
	nbt.Load(file);

	NbtFile::NbtTag *gridSizeTag = nbt.GetTag("map/gridSizeLog2");

	NbtFile::NbtTag *defaultTypeTag = nbt.GetTag("map/defaults/type");
	if (defaultTypeTag != nullptr)
	{
		defaultType = defaultTypeTag->GetInt();
	}

	NbtFile::NbtTag *defaultHeightTag = nbt.GetTag("map/defaults/height");
	if (defaultHeightTag != nullptr)
	{
		defaultHeight = defaultHeightTag->GetDouble();
	}

	NbtFile::NbtTag *nbtChunks = nbt.GetTag("map/chunks");
	if (nbtChunks == nullptr)
	{
		return;
	}
	if (nbtChunks->Type() != NbtFile::Tag_List && nbtChunks->ListType() != Tag_Compound)
	{
		Clear();
		throw file_load_error("chunks node is not Tag_Compound");
	}

	for (int chunkIndex = 0; chunkIndex < nbtChunks->NumValues(); chunkIndex += 1)
	{
		int x = 0;
		int y = 0;

		NbtFile::NbtTag &chunk = nbtChunks->Child(chunkIndex);
		NbtFile::NbtTag *chunkX = chunk->FindChild("x");
		if (chunkX != nullptr)
		{
			x = chunkX->GetInt();
		}
		NbtFile::NbtTag *chunkY = chunk->FindChild("y");
		if (chunkY != nullptr)
		{
			y = chunkY->GetInt();
		}
		NbtFile::NbtTag *chunkHeights = chunk->findChild("heights");
		if (chunkHeights == nullptr
			|| chunkHeights->Type() != Tag_List
			|| chunkHeights->ListType() != Tag_Float
			|| chunkHeights->NumValues() != CHUNK_SIZE * CHUNK_SIZE)
		{
			throw file_load_error("missing or invalid type or size for chunks/" + chunkIndex + "/heights");
		}
		NbtFile::NbtTag *chunkTypes = chunk->findChild("types");
		if (chunkTypes == nullptr
			|| chunkTypes->Type() != Tag_List
			|| chunkTypes->ListType() != Tag_Float
			|| chunkTypes->NumValues() != CHUNK_SIZE * CHUNK_SIZE)
		{
			throw file_load_error("missing or invalid type or size for chunks/" + chunkIndex + "/heights");
		}

		Chunk *chunk = FindOrCreateChunk(IndexForCoordinates(x, y));
		int *types = chunkTypes->GetInt32Data();
		int *destTypes = chunk->types->RawElements();
		for (int tileIndex = 0; tileIndex < CHUNK_SIZE * CHUNK_SIZE; tileIndex += 1)
		{
			destTypes[i] = types[i];
		}

		float *heights = chunkHeights->GetFloatData();
		float *destHeights = chunkHeights->RawElements();
		for (int tileIndex = 0; tileIndex < CHUNK_SIZE * CHUNK_SIZE; tileIndex += 1)
		{
			destHeights[i] = heights[i];
		}
	}
}

void StoredTerrainTypeMap::LoadText(IFile *file)
{
	Clear();

	// TODO: See if it's worth interpreting opti's multi-csv as a file type.
	// We'd have named sections each with an array of array of string.

	// TODO: Read terrain types.
	// TODO: Read parameters.
	// TODO: Read and interpret map. Offset it so that 0,0 is the centre.
}

void StoredTerrainTypeMap::Save(IFile *file)
{
	NbtFile nbt;

	NbtTag *chunks = nbt.CreateTag()->SetType(NbtFile::Tag_List);

	for (auto it = chunks.begin(); it != chunks.end(); ++it)
	{
		Array2D<int> types = it->second->types;
		Array2D<float> heights = it->second->heights;
		chunks.Append(
			nbt.CreateTag()->SetType(NbtFile::Tag_Compound)
				->Set("heights", nbt.CreateTag(NbtFile::Tag_List)->Set(heights.RawElements(), heights.NumElements()))
				->Set("types", nbt.CreateTag(NbtFile::Tag_List)->Set(types.RawElements(), types.NumElements()))
		);
	}

	nbt.Root()
		->SetType(NbtFile::Tag_Compound)
		->Set("map", nbt.CreateTag()->SetType(NbtFile::Tag_Compound)
			->Set("gridSizeLog2", nbt.CreateTag()->SetType(NbtFile::Tag_Int)->Set(gridSize))
			->Set("defaults", nbt.CreateTag()->SetType(NbtFile::Tag_Compound)
				->Set("type", nbt.CreateTag()->SetType(NbtFile::Tag_Int)->Set(defaultType))
				->Set("height", nbt.CreateTag()->SetType(NbtFile::Tag_Float)->Set(defaultHeight))
			)
			->Set("chunks", chunks)
		);

	nbt.Save(file);
}

void StoredTerrainTypeMap::Clear()
{
	chunks.clear();
	defaultValue = TerrainValue();
}

//-----------------------------------------------------------------------------
// IEditableTerrainSurfaceTypeMap
//-----------------------------------------------------------------------------

void StoredTerrainTypeMap::GetValues(int x, int y, Array2D<int> *types, Array2D<float> *heights)
{
	GridSplit gridSplit;
	std::vector<GridSplit::BoundInfo> bounds;
	gridSplit.GenerateBounds(x, y, x + values->Width(), y + values->Height(), &bounds);

	for (auto it = bounds.begin(); it != bounds.end(); ++it)
	{
		GridSplit::BoundInfo &boundInfo = *it;
		// size of the bounds region
		Vector2I coordSize = boundInfo.coordMax - boundInfo.coordMin;
		// coordinates of the bounds region within the destination.
		int toX = boundInfo.coordMin[0] - x;
		int toY = boundInfo.coordMin[1] - y;

		Chunk *chunk = FindChunk(boundInfo.coordMin[0], boundInfo.coordMin[1]);
		if (chunk != nullptr)
		{
			int fromX = boundInfo.coordMin[0] & CHUNK_MASK;
			int fromY = boundInfo.coordMin[1] & CHUNK_MASK;
			types->SetRegion(*(chunk->types), toX, toY, fromX, fromY, coordSize[0], coordSize[1]);
			heights->SetRegion(*(chunk->heights), toX, toY, fromX, fromY, coordSize[0], coordSize[1]);
		}
		else
		{
			types->SetRegion(toX, toY, coordSize[0], coordSize[1], defaultType);
			heights->SetRegion(toX, toY, coordSize[0], coordSize[1], defaultHeight);
		}
	}
}

void StoredTerrainTypeMap::SetValue(int x, int y, int type, float height)
{
	Array2D<TerrainValue>* chunk = FindOrCreateChunk(x, y);
	chunk->types.at(x & CHUNK_MASK, y & CHUNK_MASK) = type;
	chunk->heights.at(x & CHUNK_MASK, y & CHUNK_MASK) = height;
}

//-----------------------------------------------------------------------------
// internal
//-----------------------------------------------------------------------------

uint32_t StoredTerrainTypeMap::IndexForCoordinates(int x, int y)
{
	int chunkX = x >> CHUNK_SIZE_LOG_2;
	int chunkY = y >> CHUNK_SIZE_LOG_2;
	return (((chunkY + 32768) & 0xffff) << 16) + ((chunkY + 32768) & 0xffff);
}

StoredTerrainTypeMap::Chunk* StoredTerrainTypeMap::FindChunk(int x, int y)
{
	uint32_t index = IndexForCoordinates(x, y);
	auto it = chunks.find(index);
	if (it == chunks.end())
	{
		return nullptr;
	}
	return it->second;
}

StoredTerrainTypeMap::Chunk* StoredTerrainTypeMap::FindOrCreateChunk(int x, int y)
{
	uint32_t index = IndexForCoordinates(x, y);
	auto it = chunks.find(index);
	if (it == nullptr)
	{
		Chunk chunk;
		chunk.x = x;
		chunk.y = y;
		chunk.types = new Array2D<float>(CHUNK_SIZE, CHUNK_SIZE, defaultType);
		chunk.heights = new Array2D<float>(CHUNK_SIZE, CHUNK_SIZE, defaultHeight);
		chunks.insert(std::make_pair(index, chunk));
	}
	return &it->second;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
