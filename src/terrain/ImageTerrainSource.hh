/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "terrain/ITerrainSource.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Terrain source using a single image as heightmap data.
 */
class ImageTerrainSource : public ITerrainSource {
public:
	ImageTerrainSource(Image *image, int tx, int ty,
		float waterLevel = 0.0f, float waterDepth = 4.0f, float vertScale = 0.25f);
	~ImageTerrainSource();

	// ITerrainSource
	TerrainChunk *GetPiece(int x, int y, int w, int h);
	void FreePiece(TerrainChunk *data);
	float HorizScale() { return horizScale; }

private:
	float horizScale, vertScale;
	float waterLevel;
	float waterDepth;
	int tx, ty;
	Array2D<float> heights;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
