/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <iostream>
#include "terrain/ITerrainSource.hh"
#include "library/containers/Array2D.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

template <class T>
class ITerrainDataProvider {
public:
	/**
	 * This method should create a T from data and set the pointer-pointer to it.
	 */
	virtual T* GetData(int x, int y) = 0;

	virtual void FreeData(T* data) = 0;
};

/**
 * Stores a set of rendered terrain chunks centred around a moving point.
 * When the point updates, the clipmap gets new chunks from the source and uses
 * them to construct T.
 * A provider interface is used to provide the T.
 */
template <class T>
class TerrainClipmap {
public:
	TerrainClipmap(int radius, ITerrainDataProvider<T> *provider)
	{
		this->provider = provider;
		this->radius = radius;

		gridSize = (radius * 2) + 1;
		grid.SetSize(gridSize, gridSize, nullptr);
	}

	~TerrainClipmap()
	{
		ClearGrid();
	}

	/**
	 * Position in chunks.
	 */
	void SetCentrePosition(Vector2I centre)
	{
		Vector2I translationInGrid = centrePosition - centre;

		// If the movement is bigger than the grid size, we can just clear the grid.
		if (translationInGrid[0] == 0 && translationInGrid[1] == 0)
		{
			return;
		}
		centrePosition = centre;

		int gridSize = (radius * 2) + 1;
		if (translationInGrid[0] >= gridSize || translationInGrid[0] <= -gridSize || translationInGrid[1] >= gridSize || translationInGrid[1] <= -gridSize)
		{
			ClearGrid();
			return;
		}

		// The direction of iteration in each axis is opposite to the direction of
		// translation.
		int xStart = 0, xEnd = gridSize, xDir = 1;
		if (translationInGrid[0] > 0)
		{
			xStart = gridSize - 1;
			xEnd = -1;
			xDir = -1;
		}
		int yStart = 0, yEnd = gridSize, yDir = 1;
		if (translationInGrid[1] > 0)
		{
			yStart = gridSize - 1;
			yEnd = -1;
			yDir = -1;
		}

		// We move existing values, but new ones just get set to nullptr and we
		// lazy-load them later.
		// Any that get moved off the chunk have to be freed with the object
		// that provides them.
		for (int y = yStart, yc = 0; y != yEnd; y += yDir, yc += 1)
		{
			int oldY = y - translationInGrid[1];
			for (int x = xStart, xc = 0; x != xEnd; x += xDir, xc += 1)
			{
				int oldX = x - translationInGrid[0];
				//grid.at(x, y) = grid.at_bordered(x - move[0], y - move[1], nullptr);

				// We're iterating in the opposite order to the translation.
				// When we move an item we null its old cell, so that when we're
				// writing to a non-null cell we know it's being moved off the
				// edge and thus we can free it.
				if (grid.at(x, y) != nullptr)
				{
					provider->FreeData(grid.at(x, y));
				}
				if (grid.oob(oldX, oldY))
				{
					grid.at(x, y) = nullptr;
				}
				else
				{
					grid.at(x, y) = grid.at(oldX, oldY);
					grid.at(oldX, oldY) = nullptr;
				}
			}
		}
	}

	Vector2I GetMins()
	{
		return centrePosition - Vector2I(radius, radius);
	}

	Vector2I GetMaxs()
	{
		return centrePosition + Vector2I(radius, radius);
	}

	/**
	 * Position supplied in chunks, relative to the grid centre.
	 */
	T* GetChunkRelative(int x, int y)
	{
		int xAbsolute = x + radius;
		if (xAbsolute < 0 || xAbsolute >= gridSize)
		{
			throw std::out_of_range("TerrainClipmap::GetChunkRelative: access out of bounds");
		}

		int yAbsolute = y + radius;
		if (yAbsolute < 0 || yAbsolute >= gridSize)
		{
			throw std::out_of_range("TerrainClipmap::GetChunkRelative: access out of bounds");
		}

		if (grid.at(xAbsolute, yAbsolute) == nullptr)
		{
			grid.at(xAbsolute, yAbsolute) = provider->GetData(x + centrePosition[0], y + centrePosition[1]);
		}
		return grid.at(xAbsolute, yAbsolute);
	}

	/**
	 * Position supplied in chunks.
	 */
	T* GetChunkAbsolute(int x, int y)
	{
		return GetChunkRelative(x - centrePosition[0], y - centrePosition[1]);
	}

	/**
	 * Radius means the number of extra chunks around the centre chunk.
	 */
	int GetRadius()
	{
		return radius;
	}

private:
	int radius;
	int gridSize;
	ITerrainDataProvider<T> *provider;
	Vector2I centrePosition;
	Array2D<T*> grid;

	void ClearGrid()
	{
		for (int i = 0; i < grid.NumElements(); i += 1)
		{
			if (grid.at(i) != nullptr)
			{
				provider->FreeData(grid.at(i));
				grid.at(i) = nullptr;
			}
		}
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
