/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "terrain/GamecodeTerrain.hh"

#include "gamecode/GamecodeException.hh"
#include "gamecode/library/GamecodeVector.hh"
#include "resources/ImageResource.hh"
#include "terrain/ImageTerrainSource.hh"
#include "terrain/procedural/ProceduralTerrainSource.hh"
#include "terrain/NullTerrainSource.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

GamecodeTerrain::GamecodeTerrain(ResourceManager *resources, TerrainCacheFactory *cacheFactory, int chunkSize) :
	resources(resources),
	cacheFactory(cacheFactory),
	chunkSize(chunkSize),
	source(nullptr),
	cache(nullptr)
{
	source = new NullTerrainSource();
	cache = cacheFactory->CreateCache(source);
}

GamecodeTerrain::~GamecodeTerrain()
{
	cacheFactory->DeleteCache(cache);
	delete source;
}

//-----------------------------------------------------------------------------
// IGamecodeEngineObject
//-----------------------------------------------------------------------------

void GamecodeTerrain::SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode)
{
}

GamecodeValue GamecodeTerrain::GetProperty(std::string key, IGamecode *gamecode)
{
	return GamecodeValue();
}

void GamecodeTerrain::SetIndex(int i, GamecodeValue val, IGamecode *gamecode)
{
}

GamecodeValue GamecodeTerrain::GetIndex(int i, IGamecode *gamecode)
{
	return GamecodeValue();
}

void GamecodeTerrain::Destruct()
{
	delete this;
}

//-----------------------------------------------------------------------------
// Engine-only Methods
//-----------------------------------------------------------------------------

TerrainChunk *GamecodeTerrain::GetChunk(int x, int y)
{
	if (cache == nullptr)
	{
		return nullptr;
	}
	return cache->GetPiece(x * chunkSize, y * chunkSize, chunkSize, chunkSize);
}

void GamecodeTerrain::FreeChunk(TerrainChunk *data)
{
	cache->FreePiece(data);
}

//-----------------------------------------------------------------------------
// Shared Methods
//-----------------------------------------------------------------------------

void GamecodeTerrain::SetImageSource(std::string image)
{
	cacheFactory->DeleteCache(cache);
	delete source;
	ResourceManager::ResourcePointer<ImageResource> resource = resources->GetResource<ImageResource>(image, ResourceManager::RF_FORCE_IMMEDIATE);
	if (resource.GetState() != ResourceManager::LOADED)
	{
		return;
	}
	Image* img = resource.GetResource()->GetImage();
	source = new ImageTerrainSource(img, -(img->Width() >> 1), -(img->Height() >> 1));
	cache = cacheFactory->CreateCache(source);
}

GamecodeValue GamecodeTerrain::SetImageSource(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	SetImageSource(parameters->RequireType(0, GamecodeValue::T_String).GetString());

	return GamecodeValue();
}

void GamecodeTerrain::SetProceduralSource()
{
	cacheFactory->DeleteCache(cache);
	delete source;
	source = new ProceduralTerrainSource();
	cache = cacheFactory->CreateCache(source);
}

GamecodeValue GamecodeTerrain::SetProceduralSource(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(0);
	SetProceduralSource();
	return GamecodeValue();
}

float GamecodeTerrain::GetHeightAtPoint(Vector2F worldCoords)
{
	if (cache == nullptr)
	{
		return 0;
	}
    Vector2F chunk = WorldCoordsToChunk(worldCoords);
    TerrainChunk *data = GetChunk((int)std::floor(chunk[0]), (int)std::floor(chunk[1]));
	Vector3F origin = data->GetOrigin();
	worldCoords[0] -= origin[0];
	worldCoords[1] -= origin[1];
	return data->GetTerrainHeight(worldCoords[0], worldCoords[1]);
}

GamecodeValue GamecodeTerrain::GetHeightAtPoint(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	GamecodeVector *vw = parameters->RequireEngineType<GamecodeVector>(0, "Vector");
	Vector3F *vec = vw->GetObject();
	return GamecodeValue(GetHeightAtPoint(Vector2F((*vec)[0], (*vec)[1])));
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
