/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "library/containers/Array2D.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class TerrainData {
public:
	// TODO: Perhaps make the size immutable.

	Array2D<float> heights;
	Array2D<uint16_t> types;

	TerrainData() {}

	TerrainData(int w, int h)
		: heights(w, h, 0.0f)
		, types(w, h, 1)
		{}

	TerrainData(const TerrainData &other, int x, int y, int w, int h)
		: heights(other.heights, x, y, w, h)
		, types(other.types, x, y, w, h)
		{}

	void SetRegion(const TerrainData &other, int x, int y)
	{
		heights.SetRegion(other.heights, x, y);
		types.SetRegion(other.types, x, y);
	}

	size_t Width() const { return heights.Width(); }
	size_t Height() const { return heights.Height(); }
	size_t Size() const { return heights.Width() * heights.Height(); }
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

