/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "terrain/TerrainCache.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

TerrainCache::~TerrainCache()
{
	for (auto it = cache.begin(); it != cache.end(); ++it)
	{
		source->FreePiece((*it).second.data);
	}
}

TerrainChunk* TerrainCache::GetPiece(int x, int y, int w, int h)
{
	uint64_t currentTime = time->GetTime();

	Box2I coords(x, y, w, h);
	auto it = cache.find(coords);
	if (it != cache.end())
	{
		it->second.lastAccess = currentTime;
		it->second.references += 1;
		return it->second.data;
	}

    CacheEntryDynamic dynamicData;
    dynamicData.data = source->GetPiece(x, y, w, h),
    dynamicData.lastAccess = currentTime;
    dynamicData.references = 1;
    cache.insert(std::make_pair(coords, dynamicData));
    cacheByItem.insert(std::make_pair(dynamicData.data, coords));
    cacheEntriesByCreation.push_back(coords);
    cacheSize += 1; // TODO: would prefer this to be in bytes

    if (cacheSize > upperThreshold && (currentTime - checkInterval >= lastCheckTime))
	{
		uint64_t timeThreshold = currentTime - cacheDeletionAge;
		for (auto itQ = cacheEntriesByCreation.rbegin(); itQ != cacheEntriesByCreation.rend() && cacheSize > upperThreshold; ++itQ)
		{
			auto itM = cache.find(*itQ);
			if (itM != cache.end() && itM->second.lastAccess < timeThreshold && itM->second.references < 1)
			{
				source->FreePiece(itM->second.data);
				cacheSize -= 1; // TODO: change to use bytes
				cache.erase(itM);
			}
			//cacheEntriesByCreation.erase(itQ); // for normal iterator
			cacheEntriesByCreation.erase((++itQ).base()); // for reverse iterator
		}
		lastCheckTime = currentTime;
	}

	return dynamicData.data;
}

void TerrainCache::FreePiece(TerrainChunk *data)
{
	auto it1 = cacheByItem.find(data);
	if (it1 == cacheByItem.end())
	{
		throw std::logic_error("Tried to free piece which wasn't in cache");
	}
	auto it2 = cache.find(it1->second);
	if (it2 == cache.end())
	{
		throw std::logic_error("Internal error");
	}
	it2->second.references -= 1;
	// We don't even need to delete it here, we can hold it until the GC method
	// frees it.
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
