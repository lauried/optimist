/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "terrain/TerrainChunkModel.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

static Model* BuildModel(TerrainChunk *data, bool waterMatch, bool flatten, std::map<int, TerrainColour> *terrainColours);

TerrainChunkModel::TerrainChunkModel(TerrainChunk *data, std::map<int, TerrainColour> *terrainColours)
{
	land = BuildModel(data, false, false, terrainColours);
	underwater = BuildModel(data, true, false, terrainColours);
	waterSurface = BuildModel(data, true, true, terrainColours);

	orientation.origin = data->GetOrigin();

	bounds = data->GetBounds();
	bounds.mins += orientation.origin;
	bounds.maxs += orientation.origin;
}

TerrainChunkModel::~TerrainChunkModel()
{
	delete land;
	delete underwater;
	delete waterSurface;
}

static void ConvertTerrainColour(uint32_t source, float *dest)
{
    dest[0] = (float)((source & 0x00ff0000) >> 16) / 256.0f;
    dest[1] = (float)((source & 0x0000ff00) >>  8) / 256.0f;
    dest[2] = (float)((source & 0x000000ff)      ) / 256.0f;
}

static Model* BuildModel(TerrainChunk *data, bool waterMatch, bool flatten, std::map<int, TerrainColour> *terrainColours)
{
	Model *model = new Model();

	// Count and allocate triangles where the isWater == waterMatch.
	int triangleCount = 0;
	for (int y = 0; y < data->SizeTiles()[1]; ++y)
	{
		for (int x = 0; x < data->SizeTiles()[0]; ++x)
		{
			TileInfo info = data->GetTileInfo(x, y);
			if (info.isWater[0] == waterMatch) ++triangleCount;
			if (info.isWater[1] == waterMatch) ++triangleCount;
		}
	}

	if (triangleCount == 0)
	{
		return model;
	}

	MeshBuilder *mesh = model->AddMesh()->mesh;
	mesh->AllocateTris(triangleCount);

	// If we're doing the water surface, set the heights to zero as we build.
	for (int y = 0; y < data->SizeTiles()[1]; ++y)
	{
		for (int x = 0; x < data->SizeTiles()[0]; ++x)
		{
			TileInfo info = data->GetTileInfo(x, y);
			for (int tri = 0; tri < 2; ++tri)
			{
				if (info.isWater[tri] == !waterMatch)
				{
					continue;
				}

				int type = 0;
				float vertData[9];
				data->GetTriangle(x, y, tri, vertData, 3, &type);

				if (flatten)
				{
					vertData[2] = 0;
					vertData[5] = 0;
					vertData[8] = 0;
					type = 0; // water
				}

				float u[3], v[3], normal[3];
				Vector3F::Sub(&vertData[3], vertData, v);
				Vector3F::Sub(&vertData[6], vertData, u);
				Vector3F::CrossProduct(u, v, normal);
				Vector3F::Normalize(normal);

				float colour[3] = { 1, 0, 0 };
				auto it = terrainColours->find(type);
				if (it != terrainColours->end())
				{
					ConvertTerrainColour(it->second.colour[(x + y) & 1], colour);
				}

				mesh->AddVertex(&vertData[0], normal, colour, 0);
				mesh->AddVertex(&vertData[3], normal, colour, 0);
				mesh->AddVertex(&vertData[6], normal, colour, 0);
			}
		}
	}

	return model;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
