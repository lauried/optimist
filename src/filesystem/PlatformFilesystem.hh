/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "filesystem/IFilesystem.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Implements the platform's filesystem directly. No path translation occurs.
 */
class PlatformFilesystem : public IFilesystem {
public:
	~PlatformFilesystem();

	bool FileExists(const std::string &filename);
	bool DirectoryExists(const std::string &path);
	bool CreateDirectory(const std::string &path);
	bool GetFilesInDirectory(const std::string &path, std::vector<std::string> *files);
	IFile* Open(const std::string &filename, IFilesystem::OpenType type = T_Read);
	void Close(IFile *file);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

