/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "filesystem/DummyFilesystem.hh"
#include <stdexcept>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

DummyFilesystem::~DummyFilesystem()
{
}

class DummyFile : public IFile {
public:
	void Seek(size_t pos, SeekType type = T_Start) {}
	size_t Tell(SeekType type = T_Start) { return 0; }
	size_t Size() { return 0; }
	size_t ReadBytes(char* buffer, size_t maxBytes) { return 0; }
	void ReadString(char* buffer, size_t maxBytes) {}
	std::string ReadString(size_t maxLength = 0) { return std::string(""); }
	size_t WriteBytes(const void *buffer, size_t bufferSize) { return 0; }
	size_t WriteString(std::string &str) { return 0; }
	void Flush() {}
	bool Eof() { return true; }
	bool IsOpen() { return false; }
	void Close() {}
};

bool DummyFilesystem::FileExists(const std::string &filename)
{
	return false;
}

bool DummyFilesystem::DirectoryExists(const std::string &path)
{
	return false;
}

bool DummyFilesystem::CreateDirectory(const std::string &path)
{
	return false;
}

bool DummyFilesystem::GetFilesInDirectory(const std::string &path, std::vector<std::string> *files)
{
	return false;
}

IFile* DummyFilesystem::Open(const std::string &filename, OpenType type)
{
	return new DummyFile();
}

void DummyFilesystem::Close(IFile *file)
{
	DummyFile *df = dynamic_cast<DummyFile*>(file);
if (df == nullptr)
	{
		throw std::invalid_argument("IFile must be of type DummyFile");
	}
	delete df;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
