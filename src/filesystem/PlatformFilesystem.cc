/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "filesystem/PlatformFilesystem.hh"
#include <fstream>
#include <stdexcept>
#include <iostream>
#include "library/extern/tinydir.h"

#ifdef _MSC_VER
#include <direct.h>
#define PLATFORMFILESYSTEM_CREATE_DIR(x) _mkdir(x);
#undef CreateDirectory
#else
#define PLATFORMFILESYSTEM_CREATE_DIR(x) mkdir(x, S_IRWXU | S_IRWXG | S_IROTH);
#endif
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

PlatformFilesystem::~PlatformFilesystem()
{
}

struct PlatformFile : public IFile {
	FILE *f;

	PlatformFile(const std::string &filename, const IFilesystem::OpenType type)
	{
		f = nullptr;
		switch (type)
		{
		case IFilesystem::T_Read:
			f = fopen(filename.c_str(), "rb");
			return;
		case IFilesystem::T_Write:
			f = fopen(filename.c_str(), "wb");
			return;
		case IFilesystem::T_Append:
			f = fopen(filename.c_str(), "ab");
			return;
		}
		throw std::invalid_argument("Invalid file-open type");
		if (f == nullptr)
		{
			throw std::runtime_error("Could not open file " + filename);
		}
	}

	~PlatformFile()
	{
		if (f != nullptr)
		{
			fclose(f);
		}
	}

	void Seek(size_t pos, IFile::SeekType type = IFile::T_Start)
	{
		int origin = SEEK_SET;
		switch (type)
		{
		case IFile::T_Start: origin = SEEK_SET; break;
		case IFile::T_End: origin = SEEK_END; break;
		case IFile::T_Relative: origin = SEEK_CUR; break;
		}

		if (fseek(f, pos, origin) != 0)
		{
			throw std::range_error("Error seeking file position");
		}
	}

	size_t Tell(IFile::SeekType type = IFile::T_Start)
	{
		switch (type)
		{
		case IFile::T_Start: return ftell(f);
		case IFile::T_End: return Size() - ftell(f);
		}
		return 0;
	}

	size_t Size()
	{
		size_t oldPos = Tell();
		Seek(0, IFile::T_End);
		size_t endPos = Tell();
		Seek(oldPos);
		return endPos;
	}

	size_t ReadBytes(char* buffer, size_t maxBytes)
	{
		return fread(buffer, 1, maxBytes, f);
	}

	void ReadString(char* buffer, size_t maxBytes)
	{
		if (fgets(buffer, maxBytes, f) != buffer)
		{
			buffer[0] = '\n';
		}
	}

	std::string ReadString(size_t maxLength = 1024)
	{
		std::vector<char> buffer(maxLength);
		ReadString(&buffer[0], maxLength);
		return std::string(&buffer[0]);
	}

	size_t WriteBytes(const void *buffer, size_t bufferSize)
	{
		return fwrite(buffer, 1, bufferSize, f);
	}

	size_t WriteString(std::string &str)
	{
		fputs(str.c_str(), f);
		return str.length();
	}

	void Flush()
	{
		fflush(f);
	}

	bool Eof()
	{
		return feof(f);
	}

	bool IsOpen()
	{
		return (f != nullptr);
	}

	void Close()
	{
		fclose(f);
		f = nullptr;
	}
};

//-----------------------------------------------------------------------------
// Fileysystem
//-----------------------------------------------------------------------------

bool PlatformFilesystem::FileExists(const std::string &filename)
{
	std::ifstream ifile(filename.c_str());
	return (bool)ifile;
}

bool PlatformFilesystem::DirectoryExists(const std::string &path)
{
	tinydir_dir dir;
	int result = tinydir_open(&dir, path.c_str());
	tinydir_close(&dir);
	return result != -1;
}

bool PlatformFilesystem::CreateDirectory(const std::string &path)
{
	if (DirectoryExists(path))
	{
		return true;
	}

	size_t lastSlash = path.find_last_of('/');
	if (lastSlash != std::string::npos)
	{
		std::string base = path.substr(0, lastSlash);
		CreateDirectory(base);
	}

	PLATFORMFILESYSTEM_CREATE_DIR(path.c_str())

	return false;
}

#undef PLATFORMFILESYSTEM_CREATE_DIR

bool PlatformFilesystem::GetFilesInDirectory(const std::string &path, std::vector<std::string> *files)
{
	tinydir_dir dir;
	if (tinydir_open(&dir, path.c_str()) == -1)
	{
		tinydir_close(&dir);
		return false;
	}

	while (dir.has_next)
	{
		tinydir_file file;
		if (tinydir_readfile(&dir, &file) != -1)
		{
			std::string filename(file.name);
			if (filename.compare(".") && filename.compare(".."))
			{
				files->push_back(std::string(file.name));
			}
		}
		tinydir_next(&dir);
	}

	tinydir_close(&dir);
	return true;
}

IFile* PlatformFilesystem::Open(const std::string &filename, IFilesystem::OpenType type)
{
	return new PlatformFile(filename, type);
}

void PlatformFilesystem::Close(IFile *file)
{
	PlatformFile *df = dynamic_cast<PlatformFile*>(file);
	if (df == nullptr)
	{
		throw std::invalid_argument("IFile must be of type PlatformFile");
	}
	delete df;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
