/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "filesystem/OverlayFilesystem.hh"
#include <iostream>
#include <stdexcept>
#include <cstring>
#include "library/text/Path.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

OverlayFilesystem::~OverlayFilesystem()
{
}

void OverlayFilesystem::AddFilesystem(IFilesystem *filesystem, int priority, const std::string &mountPoint, bool writable)
{
	int flags = Path::F_Path | Path::F_PathParts | Path::F_Filename | Path::F_NormalizePath;
	Path pathInfo(mountPoint, flags);
	filesystems.push_front(Mount(filesystem, priority, Path::Join({ pathInfo.path, pathInfo.filename }), writable));
	filesystems.sort(Mount::Compare);
}

//-----------------------------------------------------------------------------
// Helpers
//-----------------------------------------------------------------------------

bool OverlayFilesystem::Mount::Compare(const Mount& first, const Mount& second)
{
	return first.priority < second.priority;
}

class NullFile : public IFile {
public:
	void Seek(size_t pos, SeekType type = T_Start) {}
	size_t Tell(SeekType type = T_Start) { return 0; }
	size_t Size() { return 0; }
	size_t ReadBytes(char* buffer, size_t maxBytes) { return 0; }
	void ReadString(char* buffer, size_t maxBytes) {}
	std::string ReadString(size_t maxLength = 0) { return ""; }
	size_t WriteBytes(const void *buffer, size_t bufferSize) { return 0; }
	size_t WriteString(std::string &str) { return 0; }
	void Flush() {}
	bool Eof() { return true; }
	bool IsOpen() { return false; }
	void Close() {}
};

NullFile gNullFile;

//-----------------------------------------------------------------------------
// IFilesystem
//-----------------------------------------------------------------------------

bool OverlayFilesystem::FileExists(const std::string &filename)
{
	for (auto it = filesystems.begin(); it != filesystems.end(); ++it)
	{
		const Mount *mount = &(*it);
		std::string realFilename = mount->GetInternalPath(filename);
		if (realFilename.length() > 0)
		{
			if (mount->filesystem->FileExists(realFilename))
			{
				return true;
			}
		}
	}
	return false;
}

bool OverlayFilesystem::DirectoryExists(const std::string &path)
{
	for (auto it = filesystems.begin(); it != filesystems.end(); ++it)
	{
		const Mount *mount = &(*it);
		std::string realPath = mount->GetInternalPath(path);
		if (realPath.length() > 0)
		{
			if (mount->filesystem->DirectoryExists(realPath))
			{
				return true;
			}
		}
	}
	return false;
}

bool OverlayFilesystem::CreateDirectory(const std::string &path)
{
	for (auto it = filesystems.begin(); it != filesystems.end(); ++it)
	{
		Mount *mount = &(*it);
		if (!mount->writable)
		{
			continue;
		}
		std::string realPath = mount->GetInternalPath(path);
		if (realPath.length() == 0)
		{
			continue;
		}
		if (mount->filesystem->CreateDirectory(realPath))
		{
			return true;
		}
	}
	return false;
}

bool OverlayFilesystem::GetFilesInDirectory(const std::string &path, std::vector<std::string> *files)
{
	// We have to call this for all filesystems where the directory exists.
	// We return true if the directory exists in any filesystem, else false.
	bool found = false;
	for (auto it = filesystems.begin(); it != filesystems.end(); ++it)
	{
		const Mount *mount = &(*it);
		std::string realPath = mount->GetInternalPath(path);
		if (realPath.length() > 0 && mount->filesystem->DirectoryExists(realPath))
		{
			found = true;
			mount->filesystem->GetFilesInDirectory(realPath, files);
		}
	}
	return found;
}

IFile* OverlayFilesystem::Open(const std::string &filename, OpenType type)
{
	if (type == IFilesystem::T_Read)
	{
		return OpenRead(filename, type);
	}
	return OpenWrite(filename, type);
}

IFile* OverlayFilesystem::OpenRead(const std::string &filename, OpenType type)
{
	// For read, we find the first filesystem with that file and open it.
	for (auto it = filesystems.begin(); it != filesystems.end(); ++it)
	{
		Mount *mount = &(*it);
		std::string realFilename = mount->GetInternalPath(filename);
		if (realFilename.length() > 0)
		{
			if (mount->filesystem->FileExists(realFilename))
			{
				IFile *file = mount->filesystem->Open(realFilename, type);
				openFiles.insert(std::make_pair(file, mount->filesystem));
				return file;
			}
		}
	}
	return &gNullFile;
}

IFile* OverlayFilesystem::OpenWrite(const std::string &filename, OpenType type)
{
	// For write, we find the first writable filesystem whose mountpoint our
	// path points inside of (starts with).
	for (auto it = filesystems.begin(); it != filesystems.end(); ++it)
	{
		Mount *mount = &(*it);
		if (!mount->writable)
		{
			continue;
		}
		std::string realFilename = mount->GetInternalPath(filename);
		if (realFilename.length() > 0)
		{
			IFile *file = mount->filesystem->Open(realFilename, type);
			openFiles.insert(std::make_pair(file, mount->filesystem));
			return file;
		}
	}
	return &gNullFile;
}

void OverlayFilesystem::Close(IFile *file)
{
	// If it's the null file object, we don't delete it because it's not a heap object.
	NullFile *nullFile = dynamic_cast<NullFile*>(file);
	if (nullFile != nullptr)
	{
		return;
	}

	auto it = openFiles.find(file);
	if (it == openFiles.end())
	{
		throw std::logic_error("Tried to close file which does not belong to this filesystem");
	}
	it->second->Close(file);
	openFiles.erase(it);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
