/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "filesystem/ChrootFilesystem.hh"
#include <stdexcept>
#include "library/text/Path.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

ChrootFilesystem::ChrootFilesystem(IFilesystem *pFilesystem, const std::string &pBaseDir)
{
	filesystem = pFilesystem;
	baseDir = pBaseDir;
}

ChrootFilesystem::~ChrootFilesystem()
{
}

//-----------------------------------------------------------------------------
// Helpers
//-----------------------------------------------------------------------------

std::string ChrootFilesystem::TranslatePath(const std::string &filename) const
{
	Path path(filename, Path::F_Path | Path::F_PathParts | Path::F_Filename | Path::F_NormalizePath);
	if (path.pathParts.size() > 0 && !path.pathParts[0].compare(".."))
	{
		return std::string("");
	}
	return Path::Join({ baseDir, path.path, path.filename });
}

//-----------------------------------------------------------------------------
// IFilesystem
//-----------------------------------------------------------------------------

bool ChrootFilesystem::FileExists(const std::string &filename)
{
	std::string translatedFilename = TranslatePath(filename);
	return filesystem->FileExists(translatedFilename);
}

bool ChrootFilesystem::DirectoryExists(const std::string &path)
{
	std::string translatedPath = TranslatePath(path);
	return filesystem->DirectoryExists(translatedPath);
}

bool ChrootFilesystem::CreateDirectory(const std::string &path)
{
	std::string translatedPath = TranslatePath(path);
	return filesystem->CreateDirectory(translatedPath);
}

bool ChrootFilesystem::GetFilesInDirectory(const std::string &path, std::vector<std::string> *files)
{
	std::string translatedPath = TranslatePath(path);
	return filesystem->GetFilesInDirectory(translatedPath, files);
}

IFile* ChrootFilesystem::Open(const std::string &filename, OpenType type)
{
	std::string translatedFilename = TranslatePath(filename);
	return filesystem->Open(translatedFilename, type);
}

void ChrootFilesystem::Close(IFile *file)
{
	return filesystem->Close(file);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
