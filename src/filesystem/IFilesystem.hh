/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <cstddef>
#include <string>
#include <vector>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * File wrapper object.
 * It should be copyable, but should only be closed once.
 */
class IFile {
public:
	virtual ~IFile() {}

	enum SeekType {
		T_Start,
		T_End,
		T_Relative,
	};

	/**
	 * Sets the file pointer.
	 */
	virtual void Seek(size_t pos, SeekType type = T_Start) = 0;

	/**
	 * Returns the current position of the file pointer.
	 */
	virtual size_t Tell(SeekType type = T_Start) = 0;

	/**
	 * Returns the size of the entire file, in bytes.
	 */
	virtual size_t Size() = 0;

	/**
	 * Reads bytes into buffer until either the end of the buffer or the end of
	 * the file. Returns the actual number of bytes read.
	 *
	 * @todo This should really use the uint8_t type, and so should everything
	 * that's not dealing with strings.
	 */
	virtual size_t ReadBytes(char* buffer, size_t maxBytes) = 0;

	/**
	 * Reads bytes into buffer until either the end of the buffer, the end of
	 * the file, or a '\0' or '\n' is reached in the data.
	 */
	virtual void ReadString(char* buffer, size_t maxBytes) = 0;

	/**
	 * Reads bytes into buffer until either the end of the buffer, the end of
	 * the file, or a '\0' or '\n' is reached in the data. Returns the data as a
	 * std::string object.
	 */
	virtual std::string ReadString(size_t maxLength = 0) = 0;

	/**
	 * Writes the buffer to file.
	 */
	virtual size_t WriteBytes(const void *buffer, size_t bufferSize) = 0;

	/**
	 * Writes the string to file, including a null terminator.
	 */
	virtual size_t WriteString(std::string &str) = 0;

	virtual void Flush() = 0;

	virtual bool Eof() = 0;
	virtual bool IsOpen() = 0;
	virtual void Close() = 0;
};

/**
 * Interface to access files.
 * @todo Add directory scanning.
 */
class IFilesystem {
public:
	enum OpenType {
		T_Read,
		T_Write,
		T_Append,
	};

	virtual ~IFilesystem();

	/**
	 * Returns true if the specified string is the path and filename of a file
	 * that exists.
	 */
	virtual bool FileExists(const std::string &filename) = 0;

	/**
	 * Returns true if the specified string is a path that exist.
	 */
	virtual bool DirectoryExists(const std::string &path) = 0;

	/**
	 * Creates the given directory and any required subdirectories.
	 * Returns true if it could be created, else false.
	 */
	virtual bool CreateDirectory(const std::string &path) = 0;

	/**
	 * If the specified path is a directory, add all the files in the directory
	 * to the given vector and return true.
	 * Otherwise, return false.
	 * This allows the check for the existence of the directory and the retrieval
	 * of its contents to be performed in one step.
	 * This MUST NOT return '.' and '..'
	 */
	virtual bool GetFilesInDirectory(const std::string &path, std::vector<std::string> *files) = 0;

	/**
	 * Opens the named file for the given type of access.
	 * @todo Ought to throw exceptions if there was an error.
	 */
	virtual IFile* Open(const std::string &filename, OpenType type = T_Read) = 0;

	/**
	 * Closes and destroys a file object we created.
	 */
	virtual void Close(IFile *file) = 0;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
