/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "filesystem/IFilesystem.hh"
#include <list>
#include <map>
#include <functional>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * OverlayFilesystem combines multiple separate filesystems together into a
 * single virtual filesystem by joining their roots to itself at given mount
 * points. Combined with ChrootFilesystem this lets us merge multiple separate
 * locations into one virtual filesystem.
 */
class OverlayFilesystem : public IFilesystem {
public:
	virtual ~OverlayFilesystem();

	/**
	 * Adds the given filesystem.
	 *
	 * The filesystem is added at a given mount point, i.e. all files within it
	 * will appear to have the mount point as a base path.
	 *
	 * If any two filesystems provide files which map to the same external path,
	 * then the filesystem with the lower priority will take precedence.
	 *
	 * Behaviour is undefined if two filesystems to have the same priority.
	 */
	void AddFilesystem(IFilesystem *filesystem, int priority, const std::string &mountPoint, bool writable);

	/**
	 * Returns true if the path points to a file in any mounted filesystem.
	 * The search order is unimportant because as long as the file exists in
	 * at least one filesystem, this will return true.
	 */
	bool FileExists(const std::string &filename);
	/**
	 * Returns true if the path is a valid directory in any mounted filesystem.
	 */
	bool DirectoryExists(const std::string &path);
	/**
	 * Searches writable filesystems in ascending order of priority for the
	 * first filesystem in which the given path can exist - that means that the
	 * path has to point inside the mount point of the given filesystem.
	 * Creates the entire path in the filesystem it was found and returns true,
	 * or returns false if no suitable filesystem was found or the creation of
	 * the directory failed.
	 */
	bool CreateDirectory(const std::string &path);
	/**
	 * Returns all files from all filesystems in the given virtual path.
	 */
	bool GetFilesInDirectory(const std::string &path, std::vector<std::string> *files);
	/**
	 * Searches all filesystems (or in the case of write mode, only writeable
	 * filesystems) in ascending order of priority. The first filesystem which
	 * provides the given path (that is, the path points inside their mount
	 * point), will be used to open the file.
	 */
	IFile* Open(const std::string &filename, const IFilesystem::OpenType type = T_Read);
	/**
	 * Closes a file opened with Open().
	 */
	void Close(IFile *file);

private:
    class Mount {
    public:
    	IFilesystem *filesystem;
    	int priority;
    	std::string mountPoint;
    	bool writable;

    	Mount(IFilesystem *filesystem, int priority, std::string mountPoint, bool writable) :
    		filesystem(filesystem),
    		priority(priority),
    		mountPoint(mountPoint),
    		writable(writable)
    		{}

    	bool operator==(const Mount &other) const
    	{
    		return filesystem == other.filesystem
				&& priority == other.priority
				&& mountPoint == other.mountPoint
				&& writable == other.writable;
    	}

    	/**
    	 * Returns true if the given path is inside our mount point.
    	 */
    	bool IsInternalPath(std::string path) const
    	{
    		if (path[path.length() - 1] != '/')
			{
				path = path + "/";
			}
    		if (mountPoint.length() < 1)
			{
				return true;
			}
			if (!mountPoint.compare(path.substr(0, mountPoint.length())))
			{
				return true;
			}
			return false;
    	}

    	/**
    	 * Takes an external path and removes our mount point.
    	 * Returns a blank string if the given path is not internal.
    	 */
    	std::string GetInternalPath(const std::string &path) const
    	{
    		if (!IsInternalPath(path))
			{
				return "";
			}
			if (mountPoint.length() == 0)
			{
				return path;
			}
			return path.substr(mountPoint.length() + 1);
    	}

    	static bool Compare(const Mount& first, const Mount& second);
    };

	std::list<Mount> filesystems;
	std::map<IFile*, IFilesystem*> openFiles;

	IFile* OpenRead(const std::string &filename, IFilesystem::OpenType type);
	IFile* OpenWrite(const std::string &filename, IFilesystem::OpenType type);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

