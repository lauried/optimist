/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <vector>
#include <sstream>
#include "gamecode/GamecodeValue.hh"
#include "gamecode/GamecodeException.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * A container for passing gamecode values as function parameters. The methods
 * are guaranteed either to return the requested value or throw an exception.
 */
class GamecodeParameterList : public std::vector<GamecodeValue> {
public:
	~GamecodeParameterList();

	/**
	 * Throw an exception if the number of parameters is not in the given range.
	 * If only one parameter is given, check for matching the first parameter exactly.
	 * No data is returned, but after calling if an exception is not thrown the
	 * parameter count is guaranteed to be within the given range.
	 * @param min  The minimum size for an accepted parameter count.
	 * @param max  The maximum size for an accepted parameter count. If max is not specified, then the parameter count must equal min.
	 */
	void RequireParmCount(int min, int max = -1)
	{
		if (max == -1)
		{
			max = min;
		}
		if (size() < min || size() > max)
		{
			std::stringstream message;
			message << "Invalid number of parameters: Expected between " << min << " and " << max << " (inclusive), got " << size();
			throw gamecode_exception(message.str().c_str());
		}
	}

	/**
	 * Throw an exception if the number of parameters is in the given range.
	 * If only one parameter is given, check for matching the first parameter exactly.
	 * No data is returned, but after calling if an exception is not thrown the
	 * parameter count is guaranteed not to be in the given range.
	 * @param min  The minimum size for an excluded parameter count.
	 * @param max  The maximum size for an excluded parameter count. If max is not specified, then the parameter count must equal min.
	 */
	void RequireNotParmCount(int min, int max = -1)
	{
		if (max == -1)
		{
			max = min;
		}
		if (size() >= min && size() <= max)
		{
			std::stringstream message;
			message << "Invalid number of parameters: Cannot have between " << min << " and " << max << " (inclusive), have " << size();
			throw gamecode_exception(message.str().c_str());
		}
	}

	/**
	 * Throw an exception if the given parameter is not one of the corresponding types.
	 * The returned value is guaranteed to be one of the specified types.
	 * @param index  The index of the parameter in the list.
	 * @param types  A value formed by adding zero or more of GamecodeValue::Type.
	 */
	GamecodeValue RequireType(int index, int types)
	{
		if (index < 0 || index >= size())
		{
			std::stringstream message;
			message << "There was no parameter at index " << index;
			throw gamecode_exception(message.str().c_str());
		}
		GamecodeValue::Type type = at(index).GetType();
		if (!(type & types))
		{
			std::stringstream message;
			message << "Parameter " << index << " not of the required type, must be one of: ";
			if (types & GamecodeValue::T_Null) { message << "null, "; }
			if (types & GamecodeValue::T_Bool) { message << "bool, "; }
			if (types & GamecodeValue::T_Number) { message << "number, "; }
			if (types & GamecodeValue::T_String) { message << "string, "; }
			if (types & GamecodeValue::T_Table) { message << "table, "; }
			if (types & GamecodeValue::T_Function) { message << "function, "; }
			if (types & GamecodeValue::T_Engine) { message << "EngineObject, "; }
			throw gamecode_exception(message.str().c_str());
		}
		return at(index);
	}

	/**
	 * Throws an exception if the specified parameter is not an engine object of
	 * the type specified by the template parameter.
	 * @param index  The index of the parameter in the list.
	 * @param name  The name of the object type being required, used for error reporting.
	 */
	template <class T>
	T* RequireEngineType(int index, const char *name)
	{
		RequireType(index, GamecodeValue::T_Engine);
		T *object = dynamic_cast<T*>(at(index).GetEngine());
		if (object == nullptr)
		{
			std::stringstream message;
			message << "Parameter " << index << " must be an engine object of type " << name;
			throw gamecode_exception(message.str().c_str());
		}
		return object;
	}

	template <class T>
	T* RequireEngineType(int index, std::string name)
	{
		return RequireEngineType<T>(index, name.c_str());
	}

	/**
	 * Returns true if a set of parameters match.
	 * Example usage:
	 *   if (params->MatchParams(1, { GamecodeValue::T_Float, GamecodeValue::T_Float, GamecodeValue::T_Float }) ...
	 * This would match three floats starting at the second parameter.
	 *
	 * @param offset The offset at which we start matching parameters.
	 * @param types A vector of types.
	 * @return True if all the parameters match, else false.
	 */
	bool Match(int offset, std::vector<int> types)
	{
		if (offset + types.size() > size())
		{
			return false;
		}
		for (int i = 0; i < types.size(); i += 1)
		{
			if (types[i] & at(i + offset).GetType() == 0)
			{
				return false;
			}
		}
		return true;
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
