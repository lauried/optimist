/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <vector>
#include <string>
#include <functional>
#include "gamecode/IGamecodeObject.hh"
#include "gamecode/GamecodeValue.hh"
#include "gamecode/GamecodeParameterList.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Engine objects.
 * Engine code derives from these to create its own object types.
 * These types are created by an adapter register function that can create the
 * user object and return it.
 *
 * An engine object may register some methods.
 * To override operators, the object simply registers methods with specific
 * names. This means that where the gamecode implementation doesn't have the
 * operators, it can simply call the methods by name.
 * The methods are as follows:
 *   Binary methods - these take one parameter, and return this_object OP param.
 *     _OpAdd, _OpSub, _OpMul, _OpDiv, _OpMod (modulo)
 *     (Bitwise:)
 *     _OpBitOr, _OpBitAnd, _OpBitXor, _OpBitShiftLeft, _OpBitShiftRight.
 *   Unary methods - these return a value that represents the transformed value of this object:
 *     _OpBitNot, _OpNegate, _OpBoolNot, _OpBool.
 *   Compound assignment, e.g. += - these take one parameter and perform the modification directly to this object:
 *     _OpAddBy, _OpSubBy, _OpMulBy, _OpDivBy, _OpModBy, _OpBitOrBy, _OpBitAndBy, _OpBitXorBy
 *   Comparison - this takes one parameter and returns <0 if this < param, >0 if this > param and 0 if this == param:
 *     _OpCompare
 *
 * The implementation should transform all the operators it has into these
 * methods where possible.
 *
 * If the operator has another name, e.g. vector scale or crossproduct, then
 * the object can also register methods with those names.
 */
class IGamecodeEngineObject : public IGamecodeObject {
public:
	class ImplementationData {
	public:
		virtual ~ImplementationData();
	};

	ImplementationData *implementationData;

	IGamecodeEngineObject() : implementationData(nullptr) {}
	virtual ~IGamecodeEngineObject();

	// Property and index access.
	virtual void SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode) { throw gamecode_exception("No such property"); }
	virtual GamecodeValue GetProperty(std::string key, IGamecode *gamecode) { throw gamecode_exception("No such property"); }
	virtual void SetIndex(int i, GamecodeValue val, IGamecode *gamecode) { throw gamecode_exception("Invalid index"); }
	virtual GamecodeValue GetIndex(int i, IGamecode *gamecode) { throw gamecode_exception("Invalid index"); }

	/**
	 * Returns pointer to a vector of pointers to all this object's child objects.
	 * May return null only if the object has no children.
	 */
	virtual std::vector<IGamecodeObject*>* GetChildObjects() { return nullptr; }

	/**
	 * This method only called by gamecode to trigger wrapper objects to unwrap
	 * when their parent object has been destroyed.
	 */
	virtual void OnParentObjectDestruct() {}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
