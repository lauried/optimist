/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <stdexcept>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Exception which adapter callbacks can throw, and the gamecode implementation
 * can catch and pass back to its gamecode.
 * Represents invalid use of an adapter by gamecode, rather than anything that
 * would be fatal within the engine itself.
 */
class gamecode_exception : public std::runtime_error {
public:
	explicit gamecode_exception(const std::string& what_arg) : std::runtime_error(what_arg) {}
	explicit gamecode_exception(const char* what_arg) : std::runtime_error(what_arg) {}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
