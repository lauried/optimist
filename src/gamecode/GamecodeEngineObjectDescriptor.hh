/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gamecode/GamecodeValue.hh"
#include <string>
#include <map>
#include <functional>
#include "library/geometry/Vector3.hh"
#include "library/geometry/Orientation.hh"
#include "gamecode/IGamecodeEngineObject.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class IGamecode;

/**
 * This is the virtual base. We use this and an instance of the
 * template GamecodeEngineObjectDescriptor.
 * Referred to as GEOD as shorthand here.
 */
class IGamecodeEngineObjectDescriptor {
public:
	virtual ~IGamecodeEngineObjectDescriptor();

	virtual void SetName(std::string name) = 0;
	virtual std::string GetName() = 0;

	virtual bool IsObjectThisType(IGamecodeEngineObject *object) = 0;

	virtual bool HasProperty(const char *key) = 0;
	virtual void SetProperty(IGamecodeEngineObject* object, const char *key, GamecodeValue val) = 0;
	virtual GamecodeValue GetProperty(IGamecodeEngineObject* object, const char *key) = 0;
	virtual GamecodeValue GetProperty(IGamecodeEngineObject* object, std::string key) = 0;

	virtual std::vector<std::string>* GetMethods() = 0;
	virtual bool HasMethod(const char *key) = 0;
	virtual int MethodIndex(const char *key) = 0;
	virtual int MethodIndex(std::string key) = 0;
	virtual GamecodeValue CallMethod(IGamecodeEngineObject* object, const char *key, GamecodeParameterList *parameters, IGamecode *gamecode) = 0;
	virtual GamecodeValue CallMethod(IGamecodeEngineObject* object, int index, GamecodeParameterList *parameters, IGamecode *gamecode) = 0;
	virtual void RenameMethod(int methodIndex, std::string newName) = 0;

	virtual void GetChildObjects(IGamecodeEngineObject* object, std::vector<IGamecodeEngineObject*>* children) = 0;

	virtual IGamecodeEngineObject* Construct(GamecodeParameterList *parms, IGamecode *gamecode) = 0;
	virtual void Destruct(IGamecodeEngineObject *object) = 0;
};

/**
 * Template for an GEOD for a specific type of IGamecodeEngineObject.
 * These objects would be owned by the gamecode implementation, created and
 * returned a template method RegisterEngineType<T>(const char *name);
 *
 * Overall syntax would be something like:
 *   gamecode->RegisterEngineType("Foo", gamecode->CreateEngineType<Foo>()
 *     ->AddInt("myInt", &Foo::myInt)
 *     ->AddFloat("myFloat", &Foo::myFloat)
 *     ->AddMethod("bar", &Foo::Bar)
 *   );
 */
template <class T>
class GamecodeEngineObjectDescriptor : public IGamecodeEngineObjectDescriptor {
public:
	// Note: Template methods defined after the private templates that they use internally.

	GamecodeEngineObjectDescriptor() : constructor(nullptr), destructor(nullptr) {}

	~GamecodeEngineObjectDescriptor()
	{
		for (auto it = properties.begin(); it != properties.end(); ++it)
		{
			delete it->second;
		}
		for (auto it = methods.begin(); it != methods.end(); ++it)
		{
			delete (*it);
		}
	}

	void SetName(std::string name)
	{
		this->name = name;
	}

	std::string GetName()
	{
		return name;
	}

	// These all implement IGamecodeEngineObjectDescriptor
	bool IsObjectThisType(IGamecodeEngineObject *object)
	{
		T *type = dynamic_cast<T*>(object);
		return (type != nullptr);
	}

	bool HasProperty(const char *key)
	{
		return properties.find(std::string(key)) != properties.end();
	}

	void SetProperty(IGamecodeEngineObject* gameObject, const char *key, GamecodeValue val)
	{
		T *object = dynamic_cast<T*>(gameObject);
		auto it = properties.find(std::string(key));
		if (it != properties.end())
		{
			it->second->Set(object, val);
		}
	}

	GamecodeValue GetProperty(IGamecodeEngineObject* gameObject, const char *key)
	{
		return GetProperty(gameObject, std::string(key));
	}

	GamecodeValue GetProperty(IGamecodeEngineObject* gameObject, std::string key)
	{
		T *object = dynamic_cast<T*>(gameObject);
		auto it = properties.find(key);
		if (it == properties.end())
		{
			return GamecodeValue();
		}
		return it->second->Get(object);
	}

	std::vector<std::string>* GetMethods()
	{
		return &methodNames;
	}

	bool HasMethod(const char *key)
	{
		return methodsByName.find(std::string(key)) != methodsByName.end();
	}

	int MethodIndex(const char *key)
	{
		return MethodIndex(std::string(key));
	}

	int MethodIndex(std::string key)
	{
		auto it = methodsByName.find(key);
		if (it == methodsByName.end())
		{
			return -1;
		}
		return it->second;
	}

	GamecodeValue CallMethod(IGamecodeEngineObject* gameObject, const char *key, GamecodeParameterList *parameters, IGamecode *gamecode)
	{
		T *object = dynamic_cast<T*>(gameObject);
		int index = methodsByName.find(std::string(key))->second;
		return methods[index]->Call(object, parameters, gamecode);
	}

	GamecodeValue CallMethod(IGamecodeEngineObject* gameObject, int index, GamecodeParameterList *parameters, IGamecode *gamecode)
	{
		T *object = dynamic_cast<T*>(gameObject);
		return methods[index]->Call(object, parameters, gamecode);
	}

	void RenameMethod(int methodIndex, std::string newName)
	{
		methodsByName.erase(methodsByName.find(methodNames[methodIndex]));
		methodsByName.insert(std::make_pair(newName, methodIndex));
		methodNames[methodIndex] = newName;
	}

	void GetChildObjects(IGamecodeEngineObject* gameObject, std::vector<IGamecodeEngineObject*>* children)
	{
		T *object = dynamic_cast<T*>(gameObject);
		for (auto it = properties.begin(); it != properties.end(); ++it)
		{
			IGamecodeEngineObject *child = it->second->GetObject(object);
			if (child != nullptr)
			{
				children->push_back(child);
			}
		}
	}

	IGamecodeEngineObject* Construct(GamecodeParameterList *parms, IGamecode *gamecode)
	{
		return constructor(parms, gamecode);
	}

	void Destruct(IGamecodeEngineObject *object)
	{
		if (destructor != nullptr)
		{
			destructor(object);
		}
		delete object;
	}

private:
	typedef GamecodeValue (T::*GamecodeMethod)(GamecodeParameterList *parameters, IGamecode *gamecode);
	typedef std::function<IGamecodeEngineObject*(GamecodeParameterList*, IGamecode*)> GamecodeConstructor;
	typedef void(*GamecodeDestructor)(IGamecodeEngineObject *object);

	/**
	 * Base class for properties.
	 */
	template <class U>
	class IGamecodeEngineObjectProperty {
	public:
		virtual void Set(U *object, GamecodeValue val) = 0;
		virtual GamecodeValue Get(U *object) = 0;
		virtual IGamecodeEngineObject *GetObject(U* object) { return nullptr; }
	};

	/**
	 * Implementation for a type supported by GamecodeValue.
	 */
	template <class U, typename V>
	class GamecodeEngineObjectPropertyLiteral : public IGamecodeEngineObjectProperty<U> {
	public:
		V U::*value;

		GamecodeEngineObjectPropertyLiteral(V U::*value) : value(value) {}

		void Set(U *object, GamecodeValue val)
		{
			val.Get(&object->*value);
		}
		GamecodeValue Get(U *object)
		{
			return GamecodeValue(object->*value);
		}
	};

	/**
	 * Implementation for an engine object.
	 */
	template <class U, class V>
	class GamecodeEngineObjectPropertyEngine : public IGamecodeEngineObjectProperty<U> {
	public:
		V U::*value;

		GamecodeEngineObjectPropertyEngine(V U::*value) : value(value) {}

		void Set(U *object, GamecodeValue val)
		{
			object->*value = dynamic_cast<U*>(val.GetEngine());
		}
		GamecodeValue Get(U *object)
		{
			return GamecodeValue((IGamecodeEngineObject*)object->*value);
		}
	};

	/**
	 * Implementation for a wrapper type.
	 */
	template <class U, class V, class W>
	class GamecodeEngineObjectPropertyEngineWrapper : public IGamecodeEngineObjectProperty<U> {
	public:
		V U::*value;
		W* U::*wrapper;

		GamecodeEngineObjectPropertyEngineWrapper(V U::*value, W* U::*wrapper) : value(value), wrapper(wrapper) {}

		void Set(U *object, GamecodeValue val)
		{
			throw gamecode_exception("Cannot set wrapper type on engine object");
		}
		GamecodeValue Get(U *object)
		{
			if (object->*wrapper == nullptr)
			{
				object->*wrapper = new W(&(object->*value));
			}
			return GamecodeValue(object->*wrapper);
		}
	};

	/**
	 * Implementation for a type that calls IGamecodeEngineObject::SetProperty and IGamecodeEngineObject::GetProperty.
	 */
	template <class U>
	class GamecodeEngineObjectPropertyCallback : public IGamecodeEngineObjectProperty<U> {
	public:
		std::string key;

		GamecodeEngineObjectPropertyCallback(std::string key) : key(key) {}

		void Set(U *object, GamecodeValue val)
		{
			object->SetProperty(key, val, nullptr);
		}

		GamecodeValue Get(U *object)
		{
			return object->GetProperty(key, nullptr);
		}
	};

	/**
	 * The only class for methods.
	 */
	template <class U>
	class GamecodeEngineObjectMethod {
	public:
		GamecodeMethod method;

		GamecodeEngineObjectMethod(GamecodeMethod method) : method(method) {}

		GamecodeValue Call(IGamecodeEngineObject* gamecodeObject, GamecodeParameterList *parameters, IGamecode *gamecode)
		{
			U *object = dynamic_cast<U*>(gamecodeObject);
			return (object->*method)(parameters, gamecode);
		}
	};

public:
	GamecodeEngineObjectDescriptor* AddInt(const char *name, int T::*ptr)
	{
		properties.insert(std::make_pair(std::string(name), new GamecodeEngineObjectPropertyLiteral<T, int>(ptr)));
		return this;
	}

	GamecodeEngineObjectDescriptor* AddFloat(const char *name, float T::*ptr)
	{
		properties.insert(std::make_pair(std::string(name), new GamecodeEngineObjectPropertyLiteral<T, float>(ptr)));
		return this;
	}

	GamecodeEngineObjectDescriptor* AddString(const char *name, std::string T::*ptr)
	{
		properties.insert(std::make_pair(std::string(name), new GamecodeEngineObjectPropertyLiteral<T, std::string>(ptr)));
		return this;
	}

	template <class U>
	GamecodeEngineObjectDescriptor* AddObject(const char *name, U T::*ptr)
	{
		properties.insert(std::make_pair(std::string(name), new GamecodeEngineObjectPropertyEngine<T, U>(ptr)));
		return this;
	}

	template <class U, class V>
	GamecodeEngineObjectDescriptor* AddObject(const char *name, U T::*val, V* T::*wrapper)
	{
		properties.insert(std::make_pair(std::string(name), new GamecodeEngineObjectPropertyEngineWrapper<T, U, V>(val, wrapper)));
		return this;
	}

	GamecodeEngineObjectDescriptor* AddProperty(const char *name)
	{
		std::string key(name);
		properties.insert(std::make_pair(name, new GamecodeEngineObjectPropertyCallback<T>(name)));
		return this;
	}

	GamecodeEngineObjectDescriptor* AddMethod(const char *name, GamecodeMethod ptr)
	{
		methods.push_back(new GamecodeEngineObjectMethod<T>(ptr));
		methodNames.push_back(name);
		methodsByName.insert(std::make_pair(std::string(name), methods.size() - 1));
		return this;
	}

	GamecodeEngineObjectDescriptor* Constructor(GamecodeConstructor c)
	{
		constructor = c;
		return this;
	}

	GamecodeEngineObjectDescriptor* Destructor(GamecodeDestructor d)
	{
		destructor = d;
		return this;
	}

private:
	std::string name;
	GamecodeConstructor constructor;
	GamecodeDestructor destructor;
	std::map<std::string, IGamecodeEngineObjectProperty<T>*> properties;
	std::vector<std::string> methodNames;
	std::vector<GamecodeEngineObjectMethod<T>*> methods;
	std::map<std::string, int> methodsByName;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
