/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gamecode/IGamecode.hh"
#include "filesystem/IFilesystem.hh"
#include "gamecode/javascript/duktape/duktape.h"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class DuktapeGamecode : public IGamecode {
public:
	// Construction, etc.
	DuktapeGamecode(IFilesystem *fs);
	~DuktapeGamecode();
	void Init(std::string rootScriptFilename);

	// IGamecode
	GamecodeValue CreateString(const char* s);
	GamecodeValue CreateTable();
	GamecodeValue CallFunction(std::string &name, std::vector<GamecodeValue> &values, GamecodeValue thisObject);
	GamecodeValue GetFunction(std::string &name);
	void PreserveTable(GamecodeValue table, IGamecodeAdapter *adapter);
	void ReleaseTable(GamecodeValue table, IGamecodeAdapter *adapter);
	void RegisterAdapter(IGamecodeAdapter *adapter);
	void RegisterEngineClass(std::type_index index, std::string name, GamecodeEngineObjectConstructor constructor, GamecodeEngineObjectMethodList *methods);

private:
	IFilesystem *filesystem;
	duk_context *context;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

