/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/javascript/DuktapeGamecode.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// TODO: Creating a function callback:
// duk_idx_t global = duk_push_global_object(ctx);
// duk_idx_t func = duk_push_c_function(ctx, callback, DUK_VARARGS);
// duk_push_int(ctx, some_index); // pushes an index which tells us what function call this is, or some other user data
// duk_put_prop_string(ctx, func, "index"); // sets our index as a property of the function
// duk_put_prop_string(ctx, global, funcname); // makes our function a property of the global object
// duk_pop(); // pop the global object

// TODO: Inside the callback:
// duk_idx_t func = duc_push_current_function(ctx);
// duk_get_prop_string(ctx, func, "index");
// duk_int_t index = duk_get_int(ctx, func + 1);
// duk_pop(); // pop the int
// duk_pop(); // pop the c function

// Note: we might use more metadata:
// - what gamecode instance
// - what global function
// - what object
// - what method on object
// We can use pointers for objects, letting us pass and retrieve void*
// We can use a couple of different callbacks for the different types:
// global, object constructor, object method

// It seems OK to pass around actual pointers to our internal objects.

DuktapeGamecode::DuktapeGamecode(IFilesystem *fs)
{
	filesystem = fs;
	context = duk_create_heap_default();
}

DuktapeGamecode::~DuktapeGamecode()
{
	duk_destroy_heap(context);
}

void DuktapeGamecode::Init(std::string rootScriptFilename)
{
	// TODO: Load the javascript from the root.
	// Use the module system for including other files, by implementing the/
	// callback that duk requires.
}

//-----------------------------------------------------------------------------
// IGamecode
//-----------------------------------------------------------------------------

GamecodeValue DuktapeGamecode::CreateString(const char* s)
{
	return GamecodeValue();
}

GamecodeValue DuktapeGamecode::CreateTable()
{
	return GamecodeValue();
}

GamecodeValue DuktapeGamecode::CallFunction(std::string &name, std::vector<GamecodeValue> &values, GamecodeValue thisObject)
{
	return GamecodeValue();
}

GamecodeValue DuktapeGamecode::GetFunction(std::string &name)
{
	return GamecodeValue();
}

void DuktapeGamecode::PreserveTable(GamecodeValue table, IGamecodeAdapter *adapter)
{
}

void DuktapeGamecode::ReleaseTable(GamecodeValue table, IGamecodeAdapter *adapter)
{
}

void DuktapeGamecode::RegisterAdapter(IGamecodeAdapter *adapter)
{
	// add global functions
	// call onregister
}

void DuktapeGamecode::RegisterEngineClass(std::type_index index, std::string name, GamecodeEngineObjectConstructor constructor, GamecodeEngineObjectMethodList *methods)
{
	// add constructor to duk
	// add methods to duk, also taking the object handle as a parameter
	// add destructor method (no automatic gc)
}

//-----------------------------------------------------------------------------
// Function callback
//-----------------------------------------------------------------------------

// TODO: Callbacks for different types.

/**
 * Callback for global functions.
 */
static int function_callback(duk_context *ctx)
{
	return 0; // returns the number of return values left on the stack
}

/**
 * Callback for engine object constructors.
 */
static int constructor_callback(duk_context *ctx)
{
	// TODO: Depending on implementation we either return a duk pointer or an object that wraps it and has the required methods.
	return 0;
}

/**
 * Callback for a method on an engine object. Requires a pointer passed as the first parameter.
 */
static int method_callback(duk_context *ctx)
{
	return 0;
}

//-----------------------------------------------------------------------------
// Data Conversion
//-----------------------------------------------------------------------------

// TODO: Back and forth between gamecode and duktape.
// Conversion into duktape might have to be done carefully, as things are put
// on to a stack. (Meaning it seems like a mistake would have knock-on effects)

// We also need to check whether we can take references to global objects or
// not.

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
