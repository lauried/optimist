/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/vernacular/interpreter/VernacularStack.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

VernacularStack::VernacularStack(int size)
{
	this->size = size;
	this->addresses = new VernacularValue[size];
	top = 0;
}

VernacularStack::~VernacularStack()
{
	delete[] addresses;
}

VernacularValue *VernacularStack::Allocate(int size, int parameters)
{
	top = top + (size - parameters);
	return &addresses[top - size];
}

void VernacularStack::Free(int size, int parameters)
{
	top = top - (size - parameters);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
