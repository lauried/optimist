/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/vernacular/interpreter/VernacularGarbageCollector.hh"
#include <stdexcept>
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

VernacularGarbageCollector::~VernacularGarbageCollector()
{
}

// Tri-colour garbage collection.
// To begin a cycle, we put all objects into the white set, then every
// object accessible from root is moved into the grey set.
// Then we're able to iteratively perform the other steps while still being
// able to create new objects.
// We iteratively pick an object from the grey set, move it to the black
// set, and move all its child objects into the grey set.
// We continue this until there are no objects in the grey set, and then
// we're allowed to delete all objects that remain in the white set.

void VernacularGarbageCollector::StartCollection()
{
	if (collectionState != READY)
	{
		return;
	}
	if (objects.size() <= 0)
	{
		return;
	}

	collectionState = IN_PROGRESS;
	numNew = numWhite = numGrey = numBlack = 0;
	for (auto it = objects.begin(); it != objects.end(); ++it)
	{
		(*it)->set = IVernacularCollectable::WHITE;
		numWhite += 1;
	}

	std::vector<IVernacularCollectable*> children;
	for (auto it = rootObjects.begin(); it != rootObjects.end(); ++it)
	{
		children.resize(0);
		(*it)->GetChildren(&children);
		for (auto itChild = children.begin(); itChild != children.end(); ++itChild)
		{
			// These are now all white when we get here.
			if ((*itChild)->set != IVernacularCollectable::WHITE)
			{
				// This happens when an object is in the a GC root but not added
				// to the garbage collector.
				throw std::logic_error("All objects should be in white set at this point");
			}
			(*itChild)->set = IVernacularCollectable::GREY;
			numWhite -= 1;
			numGrey += 1;
		}
	}

	scanOffset = 0;
}

void VernacularGarbageCollector::CollectSome(int maxWork)
{
	if (collectionState == READY)
	{
		return;
	}

	int workDone = 0;
	std::vector<IVernacularCollectable*> children;
	while (workDone < maxWork)
	{
		if (numGrey <= 0)
		{
			FinishCollection();
			return;
		}

		// Loop through to the next grey object.
		int startScan = scanOffset;
		int i;
		for (i = 0; i < objects.size(); i += 1)
		{
			scanOffset += 1;
			if (scanOffset >= objects.size())
			{
				scanOffset = 0;
			}
			if (objects[scanOffset]->set == IVernacularCollectable::GREY)
			{
				break;
			}
		}
		if (i >= objects.size())
		{
			// If we reach here, then something must have given us a child
			// object that hasn't been added to the garbage collector.
			// But the object already has to have been white to be added to
			// the grey set, and unregistered objects will always be NEW.
			throw std::logic_error("Grey set reportedly not empty, but we iterated all objects and couldn't find any grey ones.");
		}

		// Blacken the grey, and greyen white children of it.
		objects[scanOffset]->set = IVernacularCollectable::BLACK;
		numGrey -= 1;
		numBlack += 1;
		workDone += 1;

		children.resize(0);
		objects[scanOffset]->GetChildren(&children);
		for (auto itChild = children.begin(); itChild != children.end(); ++itChild)
		{
			// A child might be NEW if it's been added since the last
			// garbage collection.

			// We require that all child objects are already registered
			// with the garbage collector.
			// If we forget, then they'll not be marked white here
			// and will just leak.
			// But if they're added as the child of another object,
			// then we'll get the grey-set-not-empty exception.
			if ((*itChild)->set == IVernacularCollectable::WHITE)
			{
				(*itChild)->set = IVernacularCollectable::GREY;
				numWhite -= 1;
				numGrey += 1;
				workDone += 1;
			}
		}
	}
}

void VernacularGarbageCollector::FinishCollection()
{
	lastNumUncollected = numBlack;

	for (int i = 0; i < objects.size(); i += 1)
	{
		if (objects[i]->set == IVernacularCollectable::WHITE)
		{
			objects[i]->listener->OnCollect(objects[i]);
		}
	}

	for (int i = 0; i < objects.size(); i += 1)
	{
		if (objects[i]->set == IVernacularCollectable::WHITE)
		{
			objects[i]->listener->Collect(objects[i]);
			objects[i] = objects[objects.size() - 1];
			objects.resize(objects.size() - 1);
			i -= 1;
		}
	}
	collectionState = READY;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
