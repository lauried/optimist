/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gamecode/vernacular/types/VernacularValue.hh"
#include "gamecode/GamecodeValue.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class VernacularFunctionFactory;
class VernacularObjectFactory;
class VernacularStringFactory;
class VernacularUserObjectFactory;
class VernacularGlobals;
class IGamecode;
class ILogTarget;

/**
 * Interface to a runtime environment, providing access to factories and
 * conversion methods.
 */
class IVernacularEnvironment {
public:
	virtual IGamecode* GetGamecode() = 0;
	virtual VernacularGlobals* GetGlobals() = 0;
	virtual VernacularFunctionFactory* GetFunctionFactory() = 0;
	virtual VernacularObjectFactory* GetObjectFactory() = 0;
	virtual VernacularStringFactory* GetStringFactory() = 0;
	virtual VernacularUserObjectFactory* GetUserObjectFactory() = 0;

	virtual bool IncludeFile(std::string filename) = 0;

	virtual void ThrowException(std::string message) = 0;
	virtual void ThrowWarning(std::string message) = 0;
	virtual void Print(std::string message) = 0;

	virtual ILogTarget *GetLog() = 0;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
