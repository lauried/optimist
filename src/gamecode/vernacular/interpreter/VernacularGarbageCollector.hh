/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <vector>
#include <set>
#include <iostream>
#include "gamecode/vernacular/interpreter/IVernacularCollectable.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * An object which acts as a root container.
 */
class IVernacularGCRoot {
public:
	virtual void GetChildren(std::vector<IVernacularCollectable*> *objects) = 0;
};

/**
 * An object which listens to garbage collection events.
 */
class IVernacularGCListener {
public:
	/**
	 * This will be called in the delete phase, before any objects have
	 * actually been deleted. This means all child elements of the object will
	 * still be present.
	 */
	virtual void OnCollect(IVernacularCollectable* object) = 0;

	/**
	 * Called to delete the object. The GC listener is responsible for
	 * performing deletion. This allows for object pools and other things.
	 */
	virtual void Collect(IVernacularCollectable* object) = 0;
};

class VernacularGarbageCollector {
public:
	VernacularGarbageCollector() :
		collectionState(READY),
		numNew(0),
		numWhite(0),
		numGrey(0),
		numBlack(0),
		lastNumUncollected(0)
		{}
	~VernacularGarbageCollector();

	void AddRootObject(IVernacularGCRoot *object) { rootObjects.insert(object); }
	void RemoveRootObject(IVernacularGCRoot *object) { auto it = rootObjects.find(object); if (it != rootObjects.end()) { rootObjects.erase(it); } }

	void AddListener(IVernacularGCListener *object) { listeners.insert(object); }
	void RemoveListener(IVernacularGCListener *object) { auto it = listeners.find(object); if (it != listeners.end()) { listeners.erase(it); } }

	/**
	 * Registers an object with the collector.
	 * This must be done for every object that gets created.
	 */
	void AddObject(IVernacularCollectable *object, IVernacularGCListener *listener)
	{
		object->listener = listener;
		object->set = IVernacularCollectable::NEW;
		objects.push_back(object);
	}

	bool IsReady() { return collectionState == READY; }
	void StartCollection();
	void CollectSome(int maxWork);
	int LastAmountUncollected() { return lastNumUncollected; }
	int CurrentNumObjects() { return objects.size(); }

private:
	std::set<IVernacularGCRoot*> rootObjects;
	std::set<IVernacularGCListener*> listeners;

	enum { READY, IN_PROGRESS } collectionState;
	int scanOffset; // lets the iterative work function keep track of where it was looking in the list
	int numNew, numWhite, numGrey, numBlack;
	std::vector<IVernacularCollectable*> objects;

	int lastNumUncollected;

	void FinishCollection();
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
