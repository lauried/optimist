/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/vernacular/interpreter/VernacularGlobals.hh"
#include "gamecode/vernacular/interpreter/IVernacularCollectable.hh"
#include "gamecode/vernacular/types/VernacularString.hh"
#include "gamecode/vernacular/types/VernacularFunction.hh"
#include "gamecode/vernacular/types/VernacularObject.hh"
#include "gamecode/vernacular/types/VernacularUserObject.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

VernacularGlobals::VernacularGlobals()
{
	for (int i = 0; i < 4; i += 1)
	{
		values.push_back(VernacularValue());
	}
	values[2].SetBool(true);
	values[3].SetBool(false);
}

int VernacularGlobals::Create(std::string name)
{
	auto it = names.find(name);
	if (it != names.end())
	{
		return it->second;
	}
	int val = values.size();
	names.insert(std::make_pair(name, val));
	values.resize(values.size() + 1);
	return val;
}

int VernacularGlobals::FindAddress(std::string name)
{
	auto it = names.find(name);
	if (it != names.end())
	{
		return it->second;
	}
	return -1;
}

void VernacularGlobals::GetChildren(std::vector<IVernacularCollectable*> *objects)
{
	for (int i = 0; i < values.size(); i += 1)
	{
		IVernacularCollectable *collectable = nullptr;
		switch (values[i].GetType())
		{
		case GamecodeValue::T_String:
			collectable = values[i].GetVernacularString();
			break;
		case GamecodeValue::T_Function:
			collectable = values[i].GetVernacularFunction();
			break;
		case GamecodeValue::T_Table:
			collectable = values[i].GetVernacularObject();
			break;
		case GamecodeValue::T_Engine:
			collectable = values[i].GetVernacularUserObject();
			break;
		}
		if (collectable != nullptr)
		{
			objects->push_back(collectable);
		}
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
