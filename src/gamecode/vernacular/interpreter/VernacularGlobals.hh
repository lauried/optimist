/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <map>
#include <vector>
#include <string>
#include "gamecode/vernacular/types/VernacularValue.hh"
#include "gamecode/vernacular/interpreter/VernacularBytecode.hh"
#include "gamecode/vernacular/interpreter/VernacularGarbageCollector.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * A VernacularArray which also maintains a list of names so we can wire up
 * functions to it dynamically.
 */
class VernacularGlobals : public IVernacularGCRoot {
public:
	VernacularGlobals();

	/**
	 * Creates an index by name, or returns the existing index.
	 */
	int Create(std::string name);

	/**
	 * Returns the index of the variable with the given name, or -1 if there
	 * was no such name.
	 */
	int FindAddress(std::string name);

	/**
	 * Returns an array of values.
	 * If Create is called then the result is no longer valid.
	 */
	VernacularValue *GetArray() { return &values[0]; }

	/**
	 * Returns the size of the globals array.
	 */
	size_t Size() { return values.size(); }

	// IVernacularGCRoot
	// Adds the collectible objects to the array.
	void GetChildren(std::vector<IVernacularCollectable*> *objects);

	VernacularBytecode ThisAddress() { return VernacularBytecode(VernacularBytecode::T_GLOBAL, 0); }
	VernacularBytecode NullAddress() { return VernacularBytecode(VernacularBytecode::T_GLOBAL, 1); }
	VernacularBytecode TrueAddress() { return VernacularBytecode(VernacularBytecode::T_GLOBAL, 2); }
	VernacularBytecode FalseAddress() { return VernacularBytecode(VernacularBytecode::T_GLOBAL, 3); }

private:
	std::vector<VernacularValue> values;
	std::map<std::string, int> names;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
