/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <cstdint>
#include "library/text/String.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * This wraps some integer type, and lets us store a type and a value in one
 * object. Thus we can represent instructions or different address types.
 * Address type is freeform, letting us use type to index an array of pointers
 * to address ranges.
 *
 * We use this to store both addresses and instructions.
 * Storing the type as part of the data allows us to easily remap addresses in
 * a function without checking the instruction for how many parameters it has.
 */
struct VernacularBytecode {
	typedef uint32_t DataType;
	const int dataBits = 28;
	const DataType typeMask  = 0xf0000000;
	const DataType valueMask = 0x0fffffff; // also the maximum possible value we can store

	// The address being written to MUST be a later parameter than the addresses
	// being read from.
	enum Opcode {
		// This means that if we accidentally execute zeroed memory we'll
		// get an exception rather than just executing a bunch of nulls
		// until we hit something.
		// In practice in most environments this makes us halt if we go out
		// of bounds.
		OP_EXCEPTION = 0,

		OP_NULL,

		OP_CALL, // CALL <func_object> <bytecode_addr> : Perform a function call. The bytecode address specifies the address of the first function parameter.
		OP_RETURN, // RETURN : Returns from a function call. The return value is expected to be passed to instruction zero.

		OP_JMP, // JMP <bytecode_addr> : Unconditional jump to the specified address in bytecode.
		OP_JFALSE, // JFALSE <mem_addr> <bytecode_addr> : If the value of the memory address is false, jump to the specified address in bytecode.
		OP_JTRUE, // JTRUE <mem_addr> <bytecode_addr> : If the value of the memory address is true, jump to the specified address in bytecode.

		OP_ITSTART, // ITSTART <object> <iterator_index> : Starts iterating an object using the given iterator index.
		OP_ITNEXTJ, // ITNEXTJ <iterator_index> <address_jump_if_more> : Increments the given iterator index and jumps to the specified address if there's another item.
		OP_ITKEY, // ITKEY <iterator_index> <key_addr>
		OP_ITVALUE, // ITVALUE <iterator_index> <value_addr>
		OP_ITKEYVAL, // ITKV <iterator_index> <key_addr> <value_addr>

		OP_NEWOBJ, // NEWOBJ <target> : Creates a new object and assigns it to the target address.
		OP_DOTSET, // DOTSET <object> <key> <value>
		OP_DOTGET, // DOTGET <object> <key> <result>
		OP_PUSHTHIS, // PUSHTHIS <object> : Pushes a new object onto the 'this' stack.
		OP_POPTHIS, // POPTHIS : Pops the top of the 'this' stack.

		OP_DOTSET_META, // DOTSET_READONLY <object> <key> <value> <meta>

		OP_CPY, // CPY <from> <to>
		OP_COPY_ON_WRITE, // COPY_ON_WRITE <from> <to>
		OP_NULLIFY, // NULLIFY <mem_addr> : Set the value of the specified memory address to null.
		OP_TRUE, // TRUE <mem_addr> : Set the value of the specified memory address to boolean true.
		OP_FALSE, // FALSE <mem_addr> : Set the value of the specified memory address to boolean false.

		// binary *OP* <left> <right> <result>
		OP_POWER,
		OP_MULT,
		OP_LOGAND,
		OP_BITAND,
		OP_LOGOR,
		OP_BITOR,
		OP_NEQUAL,
		OP_GEQUAL,
		OP_LEQUAL,
		OP_EQUAL,
		OP_BITRIGHT,
		OP_BITLEFT,
		OP_GREATER,
		OP_LESS,
		OP_BITXOR,
		OP_MOD,
		OP_DIV,
		OP_ADD,
		OP_SUB,
		OP_CONCAT,

		// unary *OP* <operand> <result>
		OP_LOGNOT,
		OP_BITNOT,
		OP_NEGATE,
	};

	enum Type {
		T_GLOBAL = 0,
		T_FUNCTION = 1, // literals
		T_LOCAL = 2,
		T_INSTRUCTION_ADDRESS = 3, // used for jumps, refers to an index in a functions bytecode
		T_INTEGER = 4, // a literal integer, used by function calls
		T_INSTRUCTION = 5, // opcodes
		T_UNMAPPED_GLOBAL = 6, // indexes a list of names in the function object.
		T_UNMAPPED_LOCAL = 7, // Prior to local variables being compressed by usage. Indexes a data array in the bytecode generator.
		T_UNMAPPED_FUNCTION_PARAMETER = 8, // Prior to mapping of function parameters. This is an offset subtracted from the highest local address.
		// Note: Unmapped function call parameters can be created as special
		// types of unmapped locals. The unmapped local list can include data
		// specifying the parameter index out of a total number of parameters.
		// Then the remap at the end can put these in the right position.
	};

	DataType data;

	VernacularBytecode() : data(0) {}

	VernacularBytecode(const VernacularBytecode &other)
	{
		data = other.data;
	}

	VernacularBytecode(Opcode code)
	{
		Set(T_INSTRUCTION, code);
	}

	VernacularBytecode(int type, int value)
	{
		Set(type, value);
	}

	void operator=(const VernacularBytecode &other)
	{
		data = other.data;
	}

	bool operator==(const VernacularBytecode &other) const
	{
		return data == other.data;
	}

	int GetType()
	{
		return (data & typeMask) >> dataBits;
	}

	int GetValue()
	{
		return (data & valueMask);
	}

	void Set(VernacularBytecode other)
	{
		data = other.data;
	}

	void Set(int type, int value)
	{
		data = ((type << dataBits) & typeMask) | (value & valueMask);
	}

	bool Equals(VernacularBytecode &other) const
	{
		return data == other.data;
	}

	// Mostly useful for debug
	std::string ToString()
	{
		std::string str;
		switch (GetType())
		{
		case T_GLOBAL:
			str = "(global) ";
			break;
		case T_FUNCTION:
			str = "(function) ";
			break;
		case T_LOCAL:
			str = "(local) ";
			break;
		case T_INSTRUCTION_ADDRESS:
			str = "(instruction address) ";
			break;
		case T_INTEGER:
			str = "(integer) ";
			break;
		case T_INSTRUCTION:
			str = "(operator) ";
			break;
		case T_UNMAPPED_GLOBAL:
			str = "(unmapped global) ";
			break;
		case T_UNMAPPED_LOCAL:
			str = "(unmapped local) ";
			break;
		case T_UNMAPPED_FUNCTION_PARAMETER:
			str = "(unmapped function parameter) ";
			break;
		default:
			str = "(unknown) ";
		}
		if (GetType() != T_INSTRUCTION)
		{
			return str + String::ToString(GetValue());
		}

#define CASE(v) case v: return str + #v;
		switch (GetValue())
		{
			CASE(OP_EXCEPTION)
			CASE(OP_NULL)
			CASE(OP_CALL)
			CASE(OP_RETURN)
			CASE(OP_JMP)
			CASE(OP_JFALSE)
			CASE(OP_JTRUE)
			CASE(OP_ITSTART)
			CASE(OP_ITNEXTJ)
			CASE(OP_ITKEY)
			CASE(OP_ITVALUE)
			CASE(OP_ITKEYVAL)
			CASE(OP_NEWOBJ)
			CASE(OP_DOTSET)
			CASE(OP_DOTSET_META)
			CASE(OP_DOTGET)
			CASE(OP_PUSHTHIS)
			CASE(OP_POPTHIS)
			CASE(OP_CPY)
			CASE(OP_COPY_ON_WRITE)
			CASE(OP_NULLIFY)
			CASE(OP_TRUE)
			CASE(OP_FALSE)
			CASE(OP_POWER)
			CASE(OP_MULT)
			CASE(OP_LOGAND)
			CASE(OP_BITAND)
			CASE(OP_LOGOR)
			CASE(OP_BITOR)
			CASE(OP_NEQUAL)
			CASE(OP_GEQUAL)
			CASE(OP_LEQUAL)
			CASE(OP_EQUAL)
			CASE(OP_BITRIGHT)
			CASE(OP_BITLEFT)
			CASE(OP_GREATER)
			CASE(OP_LESS)
			CASE(OP_BITXOR)
			CASE(OP_MOD)
			CASE(OP_DIV)
			CASE(OP_ADD)
			CASE(OP_SUB)
			CASE(OP_CONCAT)
			CASE(OP_LOGNOT)
			CASE(OP_BITNOT)
			CASE(OP_NEGATE)
		}
#undef CASE
		return str + "?" + String::ToString(GetValue());
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
