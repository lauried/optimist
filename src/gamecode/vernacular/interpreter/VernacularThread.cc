/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/vernacular/interpreter/VernacularThread.hh"
#include <stdexcept>
#include "library/text/String.hh"
#include "gamecode/vernacular/interpreter/VernacularInterpreter.hh"
#include "gamecode/IGamecode.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// TODO: Track stack size so that the VernacularGamecode can adjust its initial
// stack size.

#ifdef VERNACULAR_BOUNDS_CHECK
#define READ_CHECK(x) if (!ReadCheck(x)) { DumpStack(log); ThrowException("Read-Bounds checking failed on " + x.ToString()); return; }
#define WRITE_CHECK(x) if (!WriteCheck(x)) { DumpStack(log); ThrowException("Write-Bounds checking failed on " + x.ToString()); return; }
#define TYPE_CHECK(x, y) if (x.GetType() != VernacularBytecode::y) { DumpStack(log); ThrowException("Type checking failed on " + x.ToString()); return; }
#else
#define READ_CHECK(x)
#define WRITE_CHECK(x)
#define TYPE_CHECK(x, y)
#endif

VernacularThread::VernacularThread(VernacularFunction *function, IVernacularEnvironment *environment, int initialStackSize)
{
	globals = environment->GetGlobals();

	callState.function = function;
	this->environment = environment;
	this->initialStackSize = initialStackSize;

	PushCallstate(function, 0, nullptr);
	PopThis();

	runStatus = S_READY;
	stackSize = peakStackSize = function->localAddressSpace;
}

VernacularThread::~VernacularThread()
{
	while (callStack.size())
	{
		PopCallstate();
	}
}

//-----------------------------------------------------------------------------
// VernacularThread bytecode interpreter
//-----------------------------------------------------------------------------

VernacularThread::RunResult VernacularThread::Run(int instructionLimit, ILogTarget *log)
{
	this->log = log;

	if (runStatus & S_EXCEPTION)
	{
		return R_EXCEPTION;
	}
	if (runStatus & S_FINISHED)
	{
		return R_RETURNED;
	}

	runStatus = S_RUNNING;

	if (!callState.function->MapGlobals(environment->GetGlobals(), log))
	{
		// TODO: We need to find out the variables that could not be mapped,
		// and say "Unknown global variable V at L, C in F"
		ThrowException("Unknown variable names found when attempting to run function");
		return R_EXCEPTION;
	}
	SetAddressTable();

	bool exception = false;

	try
	{
		for (int count = 0; count < instructionLimit; count += 1)
		{
			RunInstruction(log);

			if (runStatus == S_EXCEPTION)
			{
				return R_EXCEPTION;
			}
			if (runStatus == S_FINISHED)
			{
				return R_RETURNED;
			}
		}
	}
	catch (gamecode_exception &e)
	{
		ThrowException(std::string("Exception thrown by engine object: ") + e.what());
		return R_EXCEPTION;
	}

	return R_REACHED_LIMIT;
}

inline void VernacularThread::RunInstruction(ILogTarget *log)
{
	int instruction = ReadInstruction();

	// Must be guaranteed never to change.
	VernacularBytecode thisAddr = environment->GetGlobals()->ThisAddress();

	switch (instruction)
	{
	case VernacularBytecode::OP_EXCEPTION:
		callState.function->Dump(&std::cout);
		ThrowException("OP_EXCEPTION opcode encountered");
		return;

	case VernacularBytecode::OP_NULL:
		break;

	// CALL <func_object> <bytecode_addr> : Perform a function call. The bytecode address specifies the address of the first function parameter.
	case VernacularBytecode::OP_CALL:
		{
			VernacularBytecode funcAddr = ReadAddress();
			READ_CHECK(funcAddr)
			VernacularFunction *func = LookupAddress(funcAddr).GetVernacularFunction();
			if (func == nullptr)
			{
				ThrowException("Attempted to call a non-function");
				return;
			}

			// Bound functions act outwardly like a non-method function, but
			// actually call a class method, i.e. they wrap a table and a
			// function and call the function with 'this' set to the table.
			if (func->IsBound())
			{
				callState.boundCall = true;
				PushThis(VernacularValue(func->boundObject));
				func = func->boundMethod;
				if (!func->IsBytecode())
				{
					ThrowException("Tried to call a bound non-bytecode method");
					return;
				}
			}

#if 0
			// Bound classes are where a copy of the table is taken prior to
			// calling the constructor on it.
			// These were only added because we wanted quicker table literals.
			// Now that COW exists, it's no longer needed.
			if (func->IsBoundClass())
			{
				if (LookupAddress(thisAddr).GetType() != GamecodeValue::T_Table)
				{
					ThrowException("Tried to call a bound class when 'this' is not a table");
					return;
				}
				VernacularObject *thisObject = LookupAddress(thisAddr).GetVernacularObject();
				VernacularObject::Iterator it = func->boundObject->GetIterator(true); //it(func->boundObject->index, true);
				while (!it.End())
				{
					thisObject->Set(it.Key(), it.Value(), this, true);
					thisObject->SetMetaFlags(it.Key(), (VernacularObject::Index::Flags)it.Flags());
					it.Next();
				}

				func = func->boundMethod;
				if (!func->IsBytecode())
				{
					ThrowException("Tried to call a bound non-bytecode method");
					return;
				}
			}
#endif

			VernacularBytecode paramsAddr = ReadAddress();
			WRITE_CHECK(paramsAddr)
			TYPE_CHECK(paramsAddr, T_LOCAL)
			int paramsOfs = paramsAddr.GetValue();
			int overlap = callState.function->localAddressSpace - paramsOfs;
			if (overlap < 1)
			{
				overlap = 1;
			}

			int numParams = ReadInteger();

			if (func->IsObjectMethod())
			{
				if (func->IsObjectConstructor())
				{
					LookupAddress(paramsAddr) = CallEngineObjectConstructor(func, &LookupAddress(paramsAddr), numParams);
					DBP("user constructor returned " << LookupAddress(paramsAddr).ToString())
				}
				else
				{
					VernacularValue object = LookupAddress(VernacularBytecode(environment->GetGlobals()->ThisAddress()));

					if (!object.IsEngine())
					{
						ThrowException("Object method called on non-object");
						return;
					}
					CheckEngineObject(object);
					VernacularUserObject *userObj = object.GetVernacularUserObject();
					if (userObj == nullptr)
					{
						throw std::logic_error("Vernacular encountered an engine object without implementation data");
					}
					try
					{
						LookupAddress(paramsAddr) = CallEngineObjectMethod(userObj, func, &LookupAddress(paramsAddr), numParams);
					}
					catch (gamecode_exception &e)
					{
						ThrowException(e.what());
						return;
					}
					DBP("user method returned " << LookupAddress(paramsAddr).ToString())
				}
			}
			else if (func->IsAdapterMethod())
			{
				LookupAddress(paramsAddr) = CallAdapterMethod(func, &LookupAddress(paramsAddr), numParams);
				DBP("adapter method returned " << LookupAddress(paramsAddr).ToString())
			}
			else if (func->IsCallback())
			{
				LookupAddress(paramsAddr) = func->GetCallback()(&LookupAddress(paramsAddr), numParams, this);
				DBP("callback function returned " << LookupAddress(paramsAddr).ToString())
				// TODO: Only need to do this if the globals were modified by include() or some other method.
				SetAddressTable();
			}
			else
			{
				if (!func->MapGlobals(environment->GetGlobals(), log))
				{
					ThrowException("Unknown variable names found when attempting to run function");
					return;
				}
				PushCallstate(func, overlap, &LookupAddress(paramsAddr));
			}
		}
		break;

	// RETURN : Returns from a function call. The return value is expected to be passed to instruction zero.
	case VernacularBytecode::OP_RETURN:
		{
			if (callStack.size() == 0)
			{
				runStatus = S_FINISHED;
				return;
			}
			PopCallstate();
			// Returning from a bound method has to pop the 'this' object.
			if (callState.boundCall)
			{
				PopThis();
				callState.boundCall = false;
			}
		}
		break;

	// JMP <bytecode_addr> : Unconditional jump to the specified address in bytecode.
	case VernacularBytecode::OP_JMP:
		{
			callState.programCounter = ReadBytecodeAddress();
		}
		break;

	// JFALSE <mem_addr> <bytecode_addr> : If the value of the memory address is false, jump to the specified address in bytecode.
	case VernacularBytecode::OP_JFALSE:
		{
			VernacularBytecode conditionAddr = ReadAddress();
			READ_CHECK(conditionAddr)
			int jumpAddr = ReadBytecodeAddress();
			if (!LookupAddress(conditionAddr).GetAsBool())
			{
				callState.programCounter = jumpAddr;
			}
		}
		break;

	// JTRUE <mem_addr> <bytecode_addr> : If the value of the memory address is true, jump to the specified address in bytecode.
	case VernacularBytecode::OP_JTRUE:
		{
			VernacularBytecode conditionAddr = ReadAddress();
			READ_CHECK(conditionAddr)
			int jumpAddr = ReadBytecodeAddress();
			if (LookupAddress(conditionAddr).GetAsBool())
			{
				callState.programCounter = jumpAddr;
			}
		}
		break;

	// ITSTART <object> <iterator_index> : Starts iterating an object using the given iterator index.
	case VernacularBytecode::OP_ITSTART:
		{
			VernacularBytecode objAddr = ReadAddress();
			READ_CHECK(objAddr)
			if (LookupAddress(objAddr).GetType() != GamecodeValue::T_Table)
			{
				ThrowException("Tried to iterate a non-iterable type");
				return;
			}
			int index = ReadInteger();
			delete callState.iterators[index];
			//callState.iterators[index] = new VernacularObject::Iterator(
			//	LookupAddress(objAddr).GetVernacularObject()->index,
			//	LookupAddress(thisAddr).Equals(LookupAddress(objAddr))
			//);
			callState.iterators[index] = LookupAddress(objAddr).GetVernacularObject()->GetIteratorPointer(LookupAddress(thisAddr).Equals(LookupAddress(objAddr)));
		}
		break;

	// ITNEXTJ <iterator_index> <address_jump_if_more> : Increments the given iterator index and jumps to the specified address if there's another item.
	case VernacularBytecode::OP_ITNEXTJ:
		{
			int index = ReadInteger();
			int jumpAddr = ReadBytecodeAddress();
			if (!callState.iterators[index]->End())
			{
				callState.programCounter = jumpAddr;
			}
		}
		break;

	// ITKEY <iterator_index> <key_addr>
	case VernacularBytecode::OP_ITKEY:
		{
			int index = ReadInteger();
			VernacularBytecode keyAddr = ReadAddress();
			WRITE_CHECK(keyAddr)
			LookupAddress(keyAddr) = callState.iterators[index]->Key();
			callState.iterators[index]->Next();
		}
		break;

	// ITVALUE <iterator_index> <value_addr>
	case VernacularBytecode::OP_ITVALUE:
		{
			int index = ReadInteger();
			VernacularBytecode valueAddr = ReadAddress();
			WRITE_CHECK(valueAddr)
			LookupAddress(valueAddr) = callState.iterators[index]->Value();
			callState.iterators[index]->Next();
		}
		break;

	// ITKEYVAL <iterator_index> <key_addr> <value_addr>
	case VernacularBytecode::OP_ITKEYVAL:
		{
			int index = ReadInteger();
			VernacularBytecode keyAddr = ReadAddress();
			WRITE_CHECK(keyAddr)
			LookupAddress(keyAddr) = callState.iterators[index]->Key();
			VernacularBytecode valueAddr = ReadAddress();
			WRITE_CHECK(valueAddr)
			LookupAddress(valueAddr) = callState.iterators[index]->Value();
			callState.iterators[index]->Next();
		}
		break;

	// NEWOBJ <target> : Creates a new object and assigns it to the target address.
	case VernacularBytecode::OP_NEWOBJ:
		{
			VernacularBytecode targAddr = ReadAddress();
			WRITE_CHECK(targAddr)
			LookupAddress(targAddr).SetObject(environment->GetObjectFactory()->Allocate());
		}
		break;

	// DOTSET <object> <key> <value>
	case VernacularBytecode::OP_DOTSET:
		{
			VernacularBytecode objAddr = ReadAddress();
			READ_CHECK(objAddr)
			VernacularBytecode keyAddr = ReadAddress();
			READ_CHECK(keyAddr)
			VernacularBytecode valueAddr = ReadAddress();
			READ_CHECK(valueAddr)
			bool isThis = LookupAddress(thisAddr).Equals(LookupAddress(objAddr));
			LookupAddress(objAddr).OpDotSet(LookupAddress(keyAddr), LookupAddress(valueAddr), this, isThis);
		}
		break;

	// DOTSET_META <object> <key> <value> <meta>
	case VernacularBytecode::OP_DOTSET_META:
		{
			VernacularBytecode objAddr = ReadAddress();
			READ_CHECK(objAddr)
			VernacularBytecode keyAddr = ReadAddress();
			READ_CHECK(keyAddr)
			VernacularBytecode valueAddr = ReadAddress();
			READ_CHECK(valueAddr)
			bool isThis = LookupAddress(thisAddr).Equals(LookupAddress(objAddr));
			int meta = ReadInteger();
			LookupAddress(objAddr).OpDotSetMeta(LookupAddress(keyAddr), LookupAddress(valueAddr), meta, this, isThis);
		}
		break;

	// DOTGET <object> <key> <result>
	case VernacularBytecode::OP_DOTGET:
		{
			VernacularBytecode objAddr = ReadAddress();
			READ_CHECK(objAddr)
			VernacularBytecode keyAddr = ReadAddress();
			READ_CHECK(keyAddr)
			VernacularBytecode targAddr = ReadAddress();
			WRITE_CHECK(targAddr)
			CheckEngineObject(LookupAddress(objAddr));
			bool isThis = LookupAddress(thisAddr).Equals(LookupAddress(objAddr));
			LookupAddress(targAddr) = LookupAddress(objAddr).OpDotGet(LookupAddress(keyAddr), this, isThis);
		}
		break;

	// PUSHTHIS <object> : Pushes a new object onto the 'this' stack.
	case VernacularBytecode::OP_PUSHTHIS:
		{
			VernacularBytecode objAddr = ReadAddress();
			READ_CHECK(objAddr)
			GamecodeValue::Type type = LookupAddress(objAddr).GetType();
			if (type != GamecodeValue::T_Table && type != GamecodeValue::T_Engine)
			{
				ThrowException("Tried to call a method on a non-object");
				return;
			}
			PushThis(LookupAddress(objAddr));
		}
		break;

	// POPTHIS : Pops the top of the 'this' stack.
	case VernacularBytecode::OP_POPTHIS:
		{
			PopThis();
		}
		break;

	// CPY <from> <to>
	case VernacularBytecode::OP_CPY:
		{
			VernacularBytecode fromAddr = ReadAddress();
			READ_CHECK(fromAddr)
			VernacularBytecode toAddr = ReadAddress();
			WRITE_CHECK(toAddr)
			LookupAddress(toAddr) = LookupAddress(fromAddr);
		}
		break;

	// COPY_ON_WRITE <from> <to>
	case VernacularBytecode::OP_COPY_ON_WRITE:
		{
			VernacularBytecode fromAddr = ReadAddress();
			READ_CHECK(fromAddr)
			VernacularBytecode toAddr = ReadAddress();
			WRITE_CHECK(toAddr)
            LookupAddress(toAddr) = LookupAddress(fromAddr).OpCopyOnWrite(this);
		}
		break;

	// NULLIFY <mem_addr> : Set the value of the specified memory address to null.
	case VernacularBytecode::OP_NULLIFY:
		{
			VernacularBytecode addr = ReadAddress();
			WRITE_CHECK(addr)
			LookupAddress(addr).SetNull();
		}
		break;

	// TRUE <mem_addr> : Set the value of the specified memory address to boolean true.
	case VernacularBytecode::OP_TRUE:
		{
			VernacularBytecode addr = ReadAddress();
			WRITE_CHECK(addr)
			LookupAddress(addr).SetTrue();
		}
		break;

	// FALSE <mem_addr> : Set the value of the specified memory address to boolean false.
	case VernacularBytecode::OP_FALSE:
		{
			VernacularBytecode addr = ReadAddress();
			WRITE_CHECK(addr)
			LookupAddress(addr).SetFalse();
		}
		break;

	// binary *OP* <left> <right> <result>
	case VernacularBytecode::OP_POWER:
		{
			VernacularBytecode leftAddr = ReadAddress();
			READ_CHECK(leftAddr)
			VernacularBytecode rightAddr = ReadAddress();
			READ_CHECK(rightAddr)
			VernacularBytecode resultAddr = ReadAddress();
			WRITE_CHECK(resultAddr)
			LookupAddress(resultAddr) = LookupAddress(leftAddr).OpPower(LookupAddress(rightAddr), this);
		}
		break;
	case VernacularBytecode::OP_MULT:
		{
			VernacularBytecode leftAddr = ReadAddress();
			READ_CHECK(leftAddr)
			VernacularBytecode rightAddr = ReadAddress();
			READ_CHECK(rightAddr)
			VernacularBytecode resultAddr = ReadAddress();
			WRITE_CHECK(resultAddr)
			LookupAddress(resultAddr) = LookupAddress(leftAddr).OpMult(LookupAddress(rightAddr), this);
		}
		break;
	case VernacularBytecode::OP_LOGAND:
		{
			VernacularBytecode leftAddr = ReadAddress();
			READ_CHECK(leftAddr)
			VernacularBytecode rightAddr = ReadAddress();
			READ_CHECK(rightAddr)
			VernacularBytecode resultAddr = ReadAddress();
			WRITE_CHECK(resultAddr)
			LookupAddress(resultAddr) = LookupAddress(leftAddr).OpLogAnd(LookupAddress(rightAddr), this);
		}
		break;
	case VernacularBytecode::OP_BITAND:
		{
			VernacularBytecode leftAddr = ReadAddress();
			READ_CHECK(leftAddr)
			VernacularBytecode rightAddr = ReadAddress();
			READ_CHECK(rightAddr)
			VernacularBytecode resultAddr = ReadAddress();
			WRITE_CHECK(resultAddr)
			LookupAddress(resultAddr) = LookupAddress(leftAddr).OpBitAnd(LookupAddress(rightAddr), this);
		}
		break;
	case VernacularBytecode::OP_LOGOR:
		{
			VernacularBytecode leftAddr = ReadAddress();
			READ_CHECK(leftAddr)
			VernacularBytecode rightAddr = ReadAddress();
			READ_CHECK(rightAddr)
			VernacularBytecode resultAddr = ReadAddress();
			WRITE_CHECK(resultAddr)
			LookupAddress(resultAddr) = LookupAddress(leftAddr).OpLogOr(LookupAddress(rightAddr), this);
		}
		break;
	case VernacularBytecode::OP_BITOR:
		{
			VernacularBytecode leftAddr = ReadAddress();
			READ_CHECK(leftAddr)
			VernacularBytecode rightAddr = ReadAddress();
			READ_CHECK(rightAddr)
			VernacularBytecode resultAddr = ReadAddress();
			WRITE_CHECK(resultAddr)
			LookupAddress(resultAddr) = LookupAddress(leftAddr).OpBitOr(LookupAddress(rightAddr), this);
		}
		break;
	case VernacularBytecode::OP_NEQUAL:
		{
			VernacularBytecode leftAddr = ReadAddress();
			READ_CHECK(leftAddr)
			VernacularBytecode rightAddr = ReadAddress();
			READ_CHECK(rightAddr)
			VernacularBytecode resultAddr = ReadAddress();
			WRITE_CHECK(resultAddr)
			LookupAddress(resultAddr) = LookupAddress(leftAddr).OpNotEqual(LookupAddress(rightAddr), this);
		}
		break;
	case VernacularBytecode::OP_GEQUAL:
		{
			VernacularBytecode leftAddr = ReadAddress();
			READ_CHECK(leftAddr)
			VernacularBytecode rightAddr = ReadAddress();
			READ_CHECK(rightAddr)
			VernacularBytecode resultAddr = ReadAddress();
			WRITE_CHECK(resultAddr)
			LookupAddress(resultAddr) = LookupAddress(leftAddr).OpGreaterEqual(LookupAddress(rightAddr), this);
		}
		break;
	case VernacularBytecode::OP_LEQUAL:
		{
			VernacularBytecode leftAddr = ReadAddress();
			READ_CHECK(leftAddr)
			VernacularBytecode rightAddr = ReadAddress();
			READ_CHECK(rightAddr)
			VernacularBytecode resultAddr = ReadAddress();
			WRITE_CHECK(resultAddr)
			LookupAddress(resultAddr) = LookupAddress(leftAddr).OpLessEqual(LookupAddress(rightAddr), this);
		}
		break;
	case VernacularBytecode::OP_EQUAL:
		{
			VernacularBytecode leftAddr = ReadAddress();
			READ_CHECK(leftAddr)
			VernacularBytecode rightAddr = ReadAddress();
			READ_CHECK(rightAddr)
			VernacularBytecode resultAddr = ReadAddress();
			WRITE_CHECK(resultAddr)
			LookupAddress(resultAddr) = LookupAddress(leftAddr).OpEqual(LookupAddress(rightAddr), this);
		}
		break;
	case VernacularBytecode::OP_BITRIGHT:
		{
			VernacularBytecode leftAddr = ReadAddress();
			READ_CHECK(leftAddr)
			VernacularBytecode rightAddr = ReadAddress();
			READ_CHECK(rightAddr)
			VernacularBytecode resultAddr = ReadAddress();
			WRITE_CHECK(resultAddr)
			LookupAddress(resultAddr) = LookupAddress(leftAddr).OpBitRight(LookupAddress(rightAddr), this);
		}
		break;
	case VernacularBytecode::OP_BITLEFT:
		{
			VernacularBytecode leftAddr = ReadAddress();
			READ_CHECK(leftAddr)
			VernacularBytecode rightAddr = ReadAddress();
			READ_CHECK(rightAddr)
			VernacularBytecode resultAddr = ReadAddress();
			WRITE_CHECK(resultAddr)
			LookupAddress(resultAddr) = LookupAddress(leftAddr).OpBitLeft(LookupAddress(rightAddr), this);
		}
		break;
	case VernacularBytecode::OP_GREATER:
		{
			VernacularBytecode leftAddr = ReadAddress();
			READ_CHECK(leftAddr)
			VernacularBytecode rightAddr = ReadAddress();
			READ_CHECK(rightAddr)
			VernacularBytecode resultAddr = ReadAddress();
			WRITE_CHECK(resultAddr)
			LookupAddress(resultAddr) = LookupAddress(leftAddr).OpGreater(LookupAddress(rightAddr), this);
		}
		break;
	case VernacularBytecode::OP_LESS:
		{
			VernacularBytecode leftAddr = ReadAddress();
			READ_CHECK(leftAddr)
			VernacularBytecode rightAddr = ReadAddress();
			READ_CHECK(rightAddr)
			VernacularBytecode resultAddr = ReadAddress();
			WRITE_CHECK(resultAddr)
			LookupAddress(resultAddr) = LookupAddress(leftAddr).OpLess(LookupAddress(rightAddr), this);
		}
		break;
	case VernacularBytecode::OP_BITXOR:
		{
			VernacularBytecode leftAddr = ReadAddress();
			READ_CHECK(leftAddr)
			VernacularBytecode rightAddr = ReadAddress();
			READ_CHECK(rightAddr)
			VernacularBytecode resultAddr = ReadAddress();
			WRITE_CHECK(resultAddr)
			LookupAddress(resultAddr) = LookupAddress(leftAddr).OpBitXor(LookupAddress(rightAddr), this);
		}
		break;
	case VernacularBytecode::OP_MOD:
		{
			VernacularBytecode leftAddr = ReadAddress();
			READ_CHECK(leftAddr)
			VernacularBytecode rightAddr = ReadAddress();
			READ_CHECK(rightAddr)
			VernacularBytecode resultAddr = ReadAddress();
			WRITE_CHECK(resultAddr)
			LookupAddress(resultAddr) = LookupAddress(leftAddr).OpMod(LookupAddress(rightAddr), this);
		}
		break;
	case VernacularBytecode::OP_DIV:
		{
			VernacularBytecode leftAddr = ReadAddress();
			READ_CHECK(leftAddr)
			VernacularBytecode rightAddr = ReadAddress();
			READ_CHECK(rightAddr)
			VernacularBytecode resultAddr = ReadAddress();
			WRITE_CHECK(resultAddr)
			LookupAddress(resultAddr) = LookupAddress(leftAddr).OpDiv(LookupAddress(rightAddr), this);
		}
		break;
	case VernacularBytecode::OP_ADD:
		{
			VernacularBytecode leftAddr = ReadAddress();
			READ_CHECK(leftAddr)
			VernacularBytecode rightAddr = ReadAddress();
			READ_CHECK(rightAddr)
			VernacularBytecode resultAddr = ReadAddress();
			WRITE_CHECK(resultAddr)
			LookupAddress(resultAddr) = LookupAddress(leftAddr).OpAdd(LookupAddress(rightAddr), this);
		}
		break;
	case VernacularBytecode::OP_SUB:
		{
			VernacularBytecode leftAddr = ReadAddress();
			READ_CHECK(leftAddr)
			VernacularBytecode rightAddr = ReadAddress();
			READ_CHECK(rightAddr)
			VernacularBytecode resultAddr = ReadAddress();
			WRITE_CHECK(resultAddr)
			LookupAddress(resultAddr) = LookupAddress(leftAddr).OpSub(LookupAddress(rightAddr), this);
		}
		break;
	case VernacularBytecode::OP_CONCAT:
		{
			VernacularBytecode leftAddr = ReadAddress();
			READ_CHECK(leftAddr)
			VernacularBytecode rightAddr = ReadAddress();
			READ_CHECK(rightAddr)
			VernacularBytecode resultAddr = ReadAddress();
			WRITE_CHECK(resultAddr)
			VernacularValue left = LookupAddress(leftAddr).ConvertToString(this);
			VernacularValue right = LookupAddress(rightAddr).ConvertToString(this);
			LookupAddress(resultAddr).SetString(environment->GetStringFactory()->Concatenate(left.GetVernacularString(), right.GetVernacularString()));
		}
		break;

	// unary *OP* <operand> <result>
	case VernacularBytecode::OP_LOGNOT:
		{
			VernacularBytecode operandAddr = ReadAddress();
			READ_CHECK(operandAddr)
			VernacularBytecode resultAddr = ReadAddress();
			WRITE_CHECK(resultAddr)
			LookupAddress(resultAddr) = LookupAddress(operandAddr).OpLogNot(this);
		}
		break;
	case VernacularBytecode::OP_BITNOT:
		{
			VernacularBytecode operandAddr = ReadAddress();
			READ_CHECK(operandAddr)
			VernacularBytecode resultAddr = ReadAddress();
			WRITE_CHECK(resultAddr)
			LookupAddress(resultAddr) = LookupAddress(operandAddr).OpBitNot(this);
		}
		break;
	case VernacularBytecode::OP_NEGATE:
		{
			VernacularBytecode operandAddr = ReadAddress();
			READ_CHECK(operandAddr)
			VernacularBytecode resultAddr = ReadAddress();
			WRITE_CHECK(resultAddr)
			LookupAddress(resultAddr) = LookupAddress(operandAddr).OpNegate(this);
		}
		break;

	default:
		ThrowException("Unknown instruction");
		return;
	}
}

//-----------------------------------------------------------------------------
// VernacularThread setters and getters
//-----------------------------------------------------------------------------

VernacularValue VernacularThread::GetReturnValue()
{
	return addressTable[VernacularBytecode::T_LOCAL][0];
}

int VernacularThread::NumParameters()
{
	return callState.function->numParameters;
}

void VernacularThread::SetParameter(int index, VernacularValue parameter)
{
	if (runStatus != S_READY)
	{
		throw std::logic_error("Can't set parameter, already running.");
	}
	if (index < 0 || index >= callState.function->numParameters || index >= initialStackSize)
	{
		return;
	}
	addressTable[VernacularBytecode::T_LOCAL][index] = parameter;
}

void VernacularThread::SetThis(VernacularValue object)
{
	if (runStatus != S_READY)
	{
		throw std::logic_error("Can't set 'this', already running.");
	}
	VernacularBytecode thisAddr = environment->GetGlobals()->ThisAddress();
	LookupAddress(thisAddr) = object;
}

int VernacularThread::GetPeakStackSize()
{
	return peakStackSize;
}

void VernacularThread::SetAddressTable()
{
	addressTable[VernacularBytecode::T_GLOBAL] = environment->GetGlobals()->GetArray();
	addressTable[VernacularBytecode::T_FUNCTION] = callState.function->literals.GetArray();
	addressTable[VernacularBytecode::T_LOCAL] = &(callState.stack->at(callState.stackIndex));
}

void VernacularThread::ThrowException(std::string message)
{
	exceptionMessage = message;
	runStatus = S_EXCEPTION;
}

void VernacularThread::ThrowWarning(std::string message)
{
	environment->ThrowWarning(message);
}

void VernacularThread::Print(std::string message)
{
	environment->Print(message);
}

//-----------------------------------------------------------------------------
// VernacularThread stack management
//-----------------------------------------------------------------------------

void VernacularThread::PushCallstate(VernacularFunction *function, int overlap, VernacularValue *returnParameter)
{
	callState.returnParameter = returnParameter;
	if (callState.stack == nullptr)
	{
		// Un-initialized first callstate, so create a new locals stack.
		callState.stack = new std::vector<VernacularValue>();
		callState.stack->resize(initialStackSize);
		callState.stackIndex = 0;
	}
	else
	{
		callStack.push(callState);
		if (callState.stackIndex + callState.function->localAddressSpace + function->localAddressSpace - overlap > callState.stack->size())
		{
			// Ran out of space on the previous stack, so allocate a new one.
			// Then we need to copy parameters across.
			callState.stack = new std::vector<VernacularValue>();
			callState.stack->resize(initialStackSize);
			callState.stackIndex = 0;
			std::vector<VernacularValue> &newStack = *(callState.stack);
			for (size_t i = 0; i < overlap; i += 1)
			{
				newStack[i] = returnParameter[i];
			}
		}
		else
		{
			callState.stackIndex += callState.function->localAddressSpace - overlap;
			// Nullify the variables which haven't been passed but which the new function expects.
			for (int i = overlap; i < function->numParameters; i += 1)
			{
				callState.stack->at(i).SetNull();
			}
		}
	}
	callState.function = function;
	callState.iterators.resize(0);
	callState.iterators.resize(function->numIterators, nullptr);
	callState.programCounter = 0;
	SetAddressTable();
	DBP("CALLSTACK PUSHED " << callStack.size())
}

void VernacularThread::PopCallstate()
{
	if (callStack.size() < 1)
	{
		throw std::logic_error("Popped the call stack too many times");
	}

	for (size_t i = 0; i < callState.iterators.size(); i += 1)
	{
		delete callState.iterators[i];
		callState.iterators[i] = nullptr;
	}

	if (callStack.top().stack != callState.stack)
	{
		// If we created another locals stack when the old one ran out,
		// then we copy the return value back and delete it.
		std::vector<VernacularValue> &oldStack = *(callState.stack);
		callStack.top().returnParameter[0] = oldStack[0];
		delete callState.stack;
	}

	callState = callStack.top();
	callStack.pop();

	SetAddressTable();
	DBP("CALLSTACK POPPED " << callStack.size() << " returning to " << callState.programCounter)
}

//-----------------------------------------------------------------------------
// VernacularThread function calls
//-----------------------------------------------------------------------------

VernacularValue VernacularThread::CallEngineObjectConstructor(VernacularFunction *function, const VernacularValue *parameters, int numParameters)
{
	GamecodeParameterList parameterList;
	for (int i = 0; i < numParameters; i += 1)
	{
		parameterList.push_back((GamecodeValue)parameters[i]);
	}

	IGamecodeEngineObject *result = function->GetObjectMethodType()->Construct(&parameterList, environment->GetGamecode());
	environment->GetUserObjectFactory()->Allocate(result);
	return ConvertGamecodeValue(result);
}

VernacularValue VernacularThread::CallEngineObjectMethod(VernacularUserObject *object, VernacularFunction *function, const VernacularValue *parameters, int numParameters)
{
	GamecodeParameterList parameterList;
	for (int i = 0; i < numParameters; i += 1)
	{
		parameterList.push_back((GamecodeValue)parameters[i]);
	}

	GamecodeValue result = function->GetObjectMethodType()->CallMethod(object->GetEngineObject(), function->GetObjectMethodIndex(), &parameterList, environment->GetGamecode());
	return ConvertGamecodeValue(result);
}

VernacularValue VernacularThread::CallAdapterMethod(VernacularFunction *function, const VernacularValue *parameters, int numParameters)
{
	throw std::logic_error("This method should no longer be called");
	return VernacularValue();
}

VernacularValue VernacularThread::ConvertGamecodeValue(GamecodeValue gamecodeValue)
{
	VernacularValue result = (VernacularValue)gamecodeValue;
	CheckEngineObject(result);
	return result;
}

void VernacularThread::CheckEngineObject(VernacularValue &value)
{
	if (value.GetType() == GamecodeValue::T_Engine)
	{
		VernacularUserObject *user = dynamic_cast<VernacularUserObject*>(value.GetEngine()->implementationData);
		if (user == nullptr)
		{
			environment->GetUserObjectFactory()->Allocate(value.GetEngine());
		}
	}
}

//-----------------------------------------------------------------------------
// IVernacularGCRoot
//-----------------------------------------------------------------------------

void VernacularThread::GetChildren(std::vector<IVernacularCollectable*> *objects)
{
	// Traverse the call stack.
	// For each locals stack, add all the objects.
	std::vector<VernacularValue> *stack = nullptr;
	for (int i = 0; i < callStack.size(); i += 1)
	{
		if (callStack.at_top(i).stack != stack)
		{
			stack = callStack.at_top(i).stack;
			for (int j = 0; j < stack->size(); j += 1)
			{
				IVernacularCollectable *collectable = (*stack)[j].GetVernacularCollectable();
				if (collectable != nullptr)
				{
					objects->push_back(collectable);
				}
			}
		}
	}
}

//-----------------------------------------------------------------------------
// Debug stuff
//-----------------------------------------------------------------------------

static void DumpState(ILogTarget *log, VernacularFunction *function, int programCounter, int level)
{
	log->Start(ILogTarget::NOTIFICATION)
		->Write(String::ToString(level))
		->Write(" ")
		->Write(function->filename)
		->Write(":")
		->Write(String::ToString(function->debugInfo[programCounter].row))
		->Write(",")
		->Write(String::ToString(function->debugInfo[programCounter].col))
		->Write(" (")
		->Write(VernacularValue(function).ToString())
		->Write(")")
		->End();
}

void VernacularThread::DumpStack(ILogTarget *log)
{
	// It's required that we don't modify the stack.
	// We allow calling this in between subsequent calls to Run.

	// Current running function:
	DumpState(log, callState.function, callState.programCounter, 0);

	// All stored states:
	for (int level = 0; level < callStack.size(); level += 1)
	{
		DumpState(log, callStack.at_top(level).function, callStack.at_top(level).programCounter, level + 1);
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
