/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <stack>
#include "gamecode/vernacular/types/VernacularValue.hh"
#include "gamecode/vernacular/types/VernacularFunction.hh"
#include "gamecode/vernacular/types/VernacularObject.hh"
#include "gamecode/vernacular/types/VernacularUserObject.hh"
#include "gamecode/vernacular/interpreter/VernacularStack.hh"
#include "gamecode/vernacular/interpreter/VernacularInterpreter.hh"
#include "gamecode/vernacular/interpreter/IVernacularEnvironment.hh"
#include "gamecode/vernacular/interpreter/VernacularGarbageCollector.hh"
#include "library/containers/InspectableStack.hh"
#include "log/ILogTarget.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

#ifdef VERNACULAR_THREAD_DEBUG
#define DBP(x) std::cout << x << std::endl;
#else
#define DBP(x)
#endif

/**
 * Represents a single call to gamecode, tracking the function and local stack
 * until it returns.
 */
class VernacularThread : public IVernacularEnvironment, public IVernacularGCRoot {
public:
	enum RunResult {
		R_RETURNED,
		R_REACHED_LIMIT,
		R_EXCEPTION,
	};

	// TODO: Use some struct to represent the environment - the factories plus the garbage collector.
	// It's a bit better than passing it around everywhere.
	VernacularThread(VernacularFunction *function, IVernacularEnvironment *environment, int initialStackSize);

	~VernacularThread();

	/**
	 * Runs or resumes the function.
	 */
	RunResult Run(int instructionLimit, ILogTarget *log);

	/**
	 * Gets the return value. Only valid once the result of Run is RETURNED.
	 */
	VernacularValue GetReturnValue();

	/**
	 * Number of parameters the function accepts.
	 */
	int NumParameters();

	/**
	 * Sets a given parameter. Bounds checked.
	 */
	void SetParameter(int index, VernacularValue parameter);

	/**
	 * Sets the 'this' object. 'private' and 'readonly' will also be dealt with
	 * automatically. This method will throw an exception if called during or
	 * after running the thread.
	 */
	void SetThis(VernacularValue object);

	/**
	 * Returns the highest number of locals that were ever on the stack at
	 * once. This ignores that we internally might allocate another stack object;
	 * as far as this method is concerned it's all one big stack.
	 */
	int GetPeakStackSize();

	/**
	 * Returns the runtime environment.
	 */
	IVernacularEnvironment *GetEnvironment() { return environment; }

	/**
	 * Dump the entire stack to a log.
	 * This is guaranteed not to interfere with execution state, if Run is
	 * called multiple times.
	 */
	void DumpStack(ILogTarget *log);

	// IVernacularEnvironment
	IGamecode* GetGamecode() { return environment->GetGamecode(); }
	VernacularGlobals* GetGlobals() { return environment->GetGlobals(); }
	VernacularFunctionFactory* GetFunctionFactory() { return environment->GetFunctionFactory(); }
	VernacularObjectFactory* GetObjectFactory() { return environment->GetObjectFactory(); }
	VernacularStringFactory* GetStringFactory() { return environment->GetStringFactory(); }
	VernacularUserObjectFactory* GetUserObjectFactory() { return environment->GetUserObjectFactory(); }
	bool IncludeFile(std::string filename) { return environment->IncludeFile(filename); }
	void ThrowException(std::string message);
	void ThrowWarning(std::string message);
	void Print(std::string message);
	ILogTarget *GetLog() { return log; }

	// IVernacularGCRoot
	void GetChildren(std::vector<IVernacularCollectable*> *objects);

	std::string GetExceptionMessage() { return exceptionMessage; }

private:
	ILogTarget *log;

	enum RunStatus {
		S_READY = 0,
		S_RUNNING = 1,
		S_EXCEPTION = 2,
		S_FINISHED = 3, // this is a bitflag, so we can check S_FINISHED for both ordinary finished an exception
	};

	// We push this onto a stack.
	// It's the state for the currently running function.
	struct CallState {
		int stackIndex; // index within stack of current locals
		int programCounter;
		bool boundCall;
		VernacularValue *returnParameter;
		VernacularFunction *function;
		std::vector<VernacularValue> *stack;
		std::vector<VernacularObject::Iterator*> iterators;

		CallState() : stackIndex(0), programCounter(0), boundCall(false), returnParameter(nullptr), function(nullptr), stack(nullptr) {}

	};

	std::string exceptionMessage;

	VernacularGlobals *globals;
	IVernacularEnvironment *environment;
	VernacularValue *addressTable[3];

	CallState callState;

	RunStatus runStatus;

	/**
	 * Number of locals addresses to reserve initially.
	 * Also used for subsequent stack allocations when the first one runs out.
	 */
	int initialStackSize;

	/**
	 * Keep track of current stack size while running.
	 */
	int stackSize;

	/**
	 * Keeps track of the peak stack size while running. This is the sum total
	 * of all local variables that would be used in a given call.
	 */
	int peakStackSize;

	InspectableStack<CallState> callStack;
	std::stack<VernacularValue> thisStack;

	inline void RunInstruction(ILogTarget *log);

	void PushCallstate(VernacularFunction *function, int overlap, VernacularValue *returnParameter);
	void PopCallstate();

	/**
	 * Sets the address table from the call state.
	 */
	void SetAddressTable();

	/**
	 * Reads the next instruction and increments the program counter.
	 * If #defined, validates that the bytecode really is an instruction.
	 */
	inline int ReadInstruction()
	{
		DBP("ReadInstruction " << callState.programCounter
			<< " (" << callState.function->debugInfo[callState.programCounter].row << "," << callState.function->debugInfo[callState.programCounter].col << "): "
			<< callState.function->bytecode[callState.programCounter].ToString())
		int instruction = callState.function->bytecode[callState.programCounter].GetValue();
		callState.programCounter += 1;
		return instruction;
	}

	/**
	 * Reads an address pointing to bytecode, and increments the program counter.
	 * If #defined, validates that the bytecode really is an address of this type.
	 */
	inline int ReadBytecodeAddress()
	{
		DBP("ReadBytecodeAddress " << callState.programCounter
			<< " (" << callState.function->debugInfo[callState.programCounter].row << "," << callState.function->debugInfo[callState.programCounter].col << "): "
			<< callState.function->bytecode[callState.programCounter].ToString())
		int offset = callState.function->bytecode[callState.programCounter].GetValue();
		callState.programCounter += 1;
		return offset;
	}

	/**
	 * Reads an integer operand (for iterators and parameter counts)
	 * If #defined, validates that the bytecode really is an integer.
	 */
	inline int ReadInteger()
	{
		DBP("ReadInteger " << callState.programCounter
			<< " (" << callState.function->debugInfo[callState.programCounter].row << "," << callState.function->debugInfo[callState.programCounter].col << "): "
			<< callState.function->bytecode[callState.programCounter].ToString())
		int value = callState.function->bytecode[callState.programCounter].GetValue();
		callState.programCounter += 1;
		return value;
	}

	/**
	 * Reads an address that uses the address tables, and increments the program counter.
	 * If #defined, validates that the bytecode really is an address of this type.
	 */
	inline VernacularBytecode ReadAddress()
	{
		DBP("ReadAddress " << callState.programCounter
			<< " (" << callState.function->debugInfo[callState.programCounter].row << "," << callState.function->debugInfo[callState.programCounter].col << "): "
			<< callState.function->bytecode[callState.programCounter].ToString() << " -> " << LookupAddress(callState.function->bytecode[callState.programCounter]).ToString())
		VernacularBytecode bytecode = callState.function->bytecode[callState.programCounter];
		callState.programCounter += 1;
		return bytecode;
	}

	/**
	 * Returns a reference to the VernacularValue addressed by the VernacularBytecode.
	 * If #defined, will also attempt bounds-checking of the address.
	 */
	inline VernacularValue& LookupAddress(VernacularBytecode bytecode)
	{
		return addressTable[bytecode.GetType()][bytecode.GetValue()];
	}

	VernacularValue CallEngineObjectConstructor(VernacularFunction *function, const VernacularValue *parameters, int numParameters);
	VernacularValue CallEngineObjectMethod(VernacularUserObject *object, VernacularFunction *function, const VernacularValue *parameters, int numParameters);
	VernacularValue CallAdapterMethod(VernacularFunction *function, const VernacularValue *parameters, int numParameters);
	VernacularValue ConvertGamecodeValue(GamecodeValue gamecodeValue);
	void CheckEngineObject(VernacularValue &value);

	/**
	 * Pushes a 'this' object, storing the old one on a stack.
	 */
	inline void PushThis(VernacularValue value)
	{
		VernacularBytecode thisAddr = environment->GetGlobals()->ThisAddress();
		VernacularValue &thisValue = LookupAddress(thisAddr);
		thisStack.push(thisValue);
		thisValue = value;
	}

	/**
	 * Restores the next most recent old 'this' object from the stack.
	 */
	inline void PopThis()
	{
		VernacularBytecode thisAddr = environment->GetGlobals()->ThisAddress();
		if (thisStack.size() > 0)
		{
			LookupAddress(thisAddr) = thisStack.top();
			thisStack.pop();
		}
		else
		{
			LookupAddress(thisAddr) = VernacularValue();
		}
	}

	/**
	 * Returns true if a bytecode refers to a valid readable address, else
	 * false.
	 */
	inline bool ReadCheck(VernacularBytecode address)
	{
		int value = address.GetValue();
		switch (address.GetType())
		{
		case VernacularBytecode::T_GLOBAL:
			if (value >= 0 && value < globals->Size())
			{
				return true;
			}
			break;
		case VernacularBytecode::T_FUNCTION:
			if (value >= 0 && value < callState.function->literals.values.size())
			{
				return true;
			}
			break;
		case VernacularBytecode::T_LOCAL:
			if (value >= 0 && value < callState.function->localAddressSpace)
			{
				return true;
			}
			break;
		case VernacularBytecode::T_INSTRUCTION:
			if (value >= 0 && value < callState.function->bytecode.size())
			{
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * Returns true if a bytecode refers to a valid writable address, else
	 * false.
	 */
	inline bool WriteCheck(VernacularBytecode address)
	{
		int value = address.GetValue();
		switch (address.GetType())
		{
		case VernacularBytecode::T_GLOBAL:
			if (value >= 0 && value < globals->Size())
			{
				return true;
			}
			break;
		case VernacularBytecode::T_LOCAL:
			if (value >= 0 && value < callState.function->localAddressSpace)
			{
				return true;
			}
			break;
		}
		return false;
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
