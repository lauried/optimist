/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gamecode/vernacular/types/VernacularValue.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Local variable stack for vernacular.
 */
class VernacularStack {
public:
	/**
	 * Constructs a stack with the given number of addresses.
	 */
	VernacularStack(int size);

	~VernacularStack();

	/**
	 * Reserves local space for a function and returns a pointer to the first
	 * address.
	 *
	 * @param int size The total number of local variables used by the function.
	 * @param int parameters How many addresses at the start are used to pass
	 *            parameters to the function. This has to be the number being
	 *            given, not the number expected.
	 *            This is basically the number of parameters to overlap from
	 *            the calling function's address space.
	 */
	VernacularValue *Allocate(int size, int parameters);

	/**
	 * Frees the given space on the stack. The parameters are the same as those
	 * passed to Allocate.
	 */
	void Free(int size, int parameters);

	/**
	 * Returns the number of addresses remaining in the stack.
	 */
	int SpaceRemaining() { return size - top; }

	/**
	 * Total free space in stack.
	 */
	int Size() { return size; }

	/**
	 * Index of next free space.
	 */
	int Top() { return top; }

private:
	VernacularValue *addresses;
	int size;
	int top;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
