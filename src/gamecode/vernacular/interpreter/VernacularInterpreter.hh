/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gamecode/vernacular/interpreter/VernacularBytecode.hh"
#include "gamecode/vernacular/types/VernacularValue.hh"
#include "gamecode/vernacular/types/VernacularUserObjectFactory.hh"
#include "gamecode/vernacular/types/VernacularFunctionFactory.hh"
#include "gamecode/vernacular/types/VernacularObjectFactory.hh"
#include "gamecode/vernacular/types/VernacularStringFactory.hh"
#include "gamecode/vernacular/interpreter/VernacularGarbageCollector.hh"
#include "gamecode/vernacular/interpreter/VernacularGlobals.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// TODO: Redesign this.
// We take the GC and factories as parameters, as they're owned by the concrete Gamecode object.

// We have a function we want to run, and a locals stack, and a globals array.
// When we first run a function, we try to map its globals. Failure to do so prevents the function from running.

// We maintain an address table which stops being valid when we call another
// function, as the globals might be reallocated.
// We can check this case when running a function for the first time,
// and update it accordingly.

// Function calls on the local stack normally work by overlapping adddress
// spaces. But if we run out of stack space, we'll have to allocate another
// locals object, making that particular function call much slower.
// We'll want to avoid that call happening in the middle of a loop, which we'll
// have to do fairly clever stuff to avoid:
// - Reserve some space at the top of the stack for calls that need to be fast:
//   - Normally function calls won't use that space, if the space required exceeds
//     the limit they'll make a slow call.
//   - However, if we detect that we need to (or are) calling this function
//     rapidly and repeatedly, then we can use the reserved space instead.
// - Keep track of the typical size required for subsequent function calls.
//   We update how much space is initially reserved for a call.
// For now we can have the code behave unintelligently, and look at these ideas
// again when we get to it.

// For debug to produce a stack trace, we'd place function call objects on a stack.
// Therefore we can write it such that we can run some number of instructions,
// return, then resume later.

// The object on the stack can store state information for the current call,
// including the locals stack.

/**
 * Represents a scripting environment's state.
 * Uses VernacularThread to represent an instance of running some code.
 */
class VernacularInterpreter {
public:
	struct Environment {
		VernacularGarbageCollector *gc;
		VernacularStringFactory *stringFactory;
		VernacularObjectFactory *objectFactory;
		VernacularFunctionFactory *functionFactory;
		VernacularUserObjectFactory *userObjectFactory;
		VernacularGlobals *globals;
		IGamecode *gamecode;
	};

	VernacularInterpreter(IGamecode *gamecode) :
		stringFactory(&gc),
		objectFactory(&gc),
		functionFactory(&gc),
		userObjectFactory(&gc),
		environment({&gc, &stringFactory, &objectFactory, &functionFactory, &userObjectFactory, &globals, gamecode})
		{}

	// Inspect globals - get a vernacularvalue by name.

	// Method to create a thread for a given function.

	Environment *GetEnvironment() { return &environment; }

private:
	VernacularGarbageCollector gc;
	VernacularStringFactory stringFactory;
	VernacularObjectFactory objectFactory;
	VernacularFunctionFactory functionFactory;
	VernacularUserObjectFactory userObjectFactory;
	VernacularGlobals globals;

	Environment environment;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
