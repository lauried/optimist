/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/vernacular/compiler/VernacularLocalsData.hh"
#include <stdexcept>
#include <iostream>
#include "gamecode/vernacular/compiler/VernacularCompileException.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

#if 0
#define VERNACULAR_LOCALSDATA_DEBUG
#endif

VernacularBytecode VernacularLocalsData::AddVariable()
{
	// Just add another variable on the list.
	addresses.push_back(LocalsData(VernacularBytecode(VernacularBytecode::T_UNMAPPED_LOCAL, addresses.size())));
	return addresses[addresses.size() - 1].address;
}

VernacularBytecode VernacularLocalsData::AddVariable(Token token, int blockDepth, bool canRemap)
{
	std::string name = token.str();
	// Throw an exception of the name already exists.
	for (auto it = addresses.begin(); it != addresses.end(); ++it)
	{
		if (!it->name.compare(name))
		{
			throw VernacularCompileException(token.line, token.column, "Variable redeclaration.");
		}
	}
	addresses.push_back(LocalsData(VernacularBytecode(VernacularBytecode::T_UNMAPPED_LOCAL, addresses.size()), blockDepth, name));
	if (!canRemap)
	{
		addresses[addresses.size() - 1].remapTo = addresses.size() - 1;
	}
	return addresses[addresses.size() - 1].address;
}

VernacularBytecode VernacularLocalsData::FindVariable(std::string name)
{
	for (auto it = addresses.begin(); it != addresses.end(); ++it)
	{
		if (!it->name.compare(name))
		{
			return it->address;
		}
	}
	// Return a global address if the local doesn't exist.
	return VernacularBytecode();
}

void VernacularLocalsData::ClearScopeDepth(int blockDepth)
{
	for (auto it = addresses.begin(); it != addresses.end(); ++it)
	{
		if (it->blockDepth == blockDepth)
		{
			it->name = "";
		}
	}
}

void VernacularLocalsData::AddVariableReference(VernacularBytecode variable, VernacularBytecode bytecodeOffset)
{
	// Find the given address and update its first and last use.
	// We know that an uninitialized VernacularBytecode is type 0 value 0.
	for (auto it = addresses.begin(); it != addresses.end(); ++it)
	{
		if (it->address.Equals(variable))
		{
			if (it->firstUse.GetType() != VernacularBytecode::T_INSTRUCTION_ADDRESS || it->firstUse.GetValue() > bytecodeOffset.GetValue())
			{
				it->firstUse.Set(bytecodeOffset);
			}
			if (it->lastUse.GetValue() < bytecodeOffset.GetValue())
			{
				it->lastUse.Set(bytecodeOffset);
			}
			return;
		}
	}
	// The compiler is using this object incorrectly if this happens.
	throw std::logic_error("No such address");
}

void VernacularLocalsData::AddVariableLoopReference(VernacularBytecode variable, VernacularBytecode loopStart, VernacularBytecode loopEnd)
{
	for (auto it = addresses.begin(); it != addresses.end(); ++it)
	{
		if (it->address.Equals(variable) && it->firstUse.GetValue() < loopStart.GetValue() && it->lastUse.GetValue() < loopEnd.GetValue())
		{
			it->lastUse.Set(loopEnd);
		}
	}
}

int VernacularLocalsData::RemapAddresses(std::vector<VernacularBytecode> &bytecode)
{
	// Not sure what the best algorithm for this is, but even a relatively
	// naive approach will have a significant effect.
	// For each unmapped address in order of first use,
	//   Repeatedly...
	//     find the unmapped address first used soonest after our last use
	//     map it to our address
	//     update our last use to that of the unmapped address
	//   ...until we can't find an address.
	int lastAddress = 0;
	for (int i = 0; i < addresses.size(); i += 1)
	{
#ifdef VERNACULAR_LOCALSDATA_DEBUG
		std::cout << "Local " << i
			<< " f " << addresses[i].firstUse.GetValue()
			<< " l " << addresses[i].lastUse.GetValue()
			<< " rt " << addresses[i].remapTo
			<< std::endl;
#endif

		if (addresses[i].remapTo != -1)
		{
			lastAddress += 1;
			continue;
		}
		addresses[i].remapTo = lastAddress;

#ifdef VERNACULAR_LOCALSDATA_DEBUG
		std::cout << "remap " << i << " -> " << lastAddress << std::endl;
#endif

		int start = addresses[i].firstUse.GetValue();
		int end = addresses[i].lastUse.GetValue();
		for (int j = i + 1; j < addresses.size(); j += 1)
		{
			if (addresses[j].firstUse.GetValue() > end)
			{
				addresses[j].remapTo = lastAddress;
				end = addresses[j].lastUse.GetValue();

#ifdef VERNACULAR_LOCALSDATA_DEBUG
				std::cout << "remap " << j << " -> " << lastAddress << std::endl;
#endif
			}
		}
		addresses[i].lastUse.Set(VernacularBytecode::T_UNMAPPED_LOCAL, end);
		lastAddress += 1;
	}
	for (int i = 0; i < bytecode.size(); i += 1)
	{
		if (bytecode[i].GetType() != VernacularBytecode::T_UNMAPPED_LOCAL)
		{
			continue;
		}
		for (int j = 0; j < addresses.size(); j += 1)
		{
			if (bytecode[i].Equals(addresses[j].address))
			{
				bytecode[i].Set(VernacularBytecode::T_LOCAL, addresses[j].remapTo);
			}
		}
	}

	// Function parameters stay in order and sit at the top of the address space.
	int maxFuncParameter = 0;
	for (int i = 0; i < bytecode.size(); i += 1)
	{
		if (bytecode[i].GetType() == VernacularBytecode::T_UNMAPPED_FUNCTION_PARAMETER)
		{
			int value = bytecode[i].GetValue();

#ifdef VERNACULAR_LOCALSDATA_DEBUG
			std::cout << "funcparm " << value << std::endl;
#endif

			if (value > maxFuncParameter)
			{
				maxFuncParameter = value;
			}
			bytecode[i] = VernacularBytecode(VernacularBytecode::T_LOCAL, value + lastAddress);
		}
	}

#ifdef VERNACULAR_LOCALSDATA_DEBUG
	std::cout << "maxfunc " << maxFuncParameter << std::endl;
#endif

	return lastAddress + maxFuncParameter + 1;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
