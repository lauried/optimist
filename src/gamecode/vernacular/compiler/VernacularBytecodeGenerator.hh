/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <stack>
#include <list>
#include <iostream>
#include "gamecode/vernacular/interpreter/VernacularBytecode.hh"
#include "gamecode/vernacular/compiler/VernacularAST.hh"
#include "gamecode/vernacular/compiler/VernacularLocalsData.hh"
#include "gamecode/vernacular/compiler/VernacularCompileException.hh"
#include "gamecode/vernacular/types/VernacularFunctionFactory.hh"
#include "gamecode/vernacular/types/VernacularObjectFactory.hh"
#include "gamecode/vernacular/types/VernacularStringFactory.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * A bytecode generator takes an Abstract Syntax Tree and writes the bytecode
 * for a function.
 *
 * The alternative to this class would've been a VernacularAST derived object
 * which also contained code to compile to bytecode.
 */
class VernacularBytecodeGenerator {
public:
	VernacularBytecodeGenerator(
		VernacularFunctionFactory *funcs,
		VernacularObjectFactory *objects,
		VernacularStringFactory *strings,
		ILogTarget *log,
		int dumpLevel = 0
	) :
		functionFactory(funcs),
		objectFactory(objects),
		stringFactory(strings),
		log(log),
		nextIterator(0),
		numFunctionParameters(0),
		dumpLevel(dumpLevel)
	{
		blockDepth = 0;
	}

	/**
	 * Returns a VernacularFunction created using the supplied VernacularFunctionFactory,
	 * thus registered with the garbage collector that the factory is configured to use.
	 *
	 * @param VernacularAST* syntax The Abstract Syntax Tree to compile a function from.
	 * @param bool root Whether or not we're compiling a root function - a flat file, as opposed to a function literal.
	 */
	VernacularFunction* CompileFunction(VernacularAST *syntax);

private:
	struct BreakInfo {
		VernacularBytecode loopStart;
		VernacularBytecode jumpAddress;
		BreakInfo();
		BreakInfo(VernacularBytecode loopStart, VernacularBytecode jumpAddress) : loopStart(loopStart), jumpAddress(jumpAddress) {}
	};

	template<class T>
	class AddressableStack : public std::vector<T> {
	public:
		T& top(size_t index) { return this->at(this->size() - 1); }
		const T& top(size_t index) const { return this->at(this->size() - 1); }
		void push(T& val) { this->push_back(val); }
		void pop() { this->pop_back(); }
	};

	bool globalScope;
	VernacularFunctionFactory *functionFactory;
	VernacularObjectFactory *objectFactory;
	VernacularStringFactory *stringFactory;
	ILogTarget *log;

	std::string filename;
	VernacularFunction *function;
	VernacularLocalsData locals;
	Token lastToken; // used for writing debug data
	int blockDepth; // keeps track of how deeply into a block we are, to implement block scope

	int nextIterator;
	AddressableStack<VernacularBytecode> loopStack;
	std::list<BreakInfo> breakList;

	// Count the number of function parameters we use.
	// These go up as we make nested function calls while building the outer
	// function call.
	int numFunctionParameters;

	int dumpLevel;

	VernacularFunction* CompileFunction(VernacularFunctionLiteral *func);

	void CompileBlock(VernacularBlock *block);
	void CompileStatement(VernacularStatement *statement);

	void CompileVariableDeclarationList(VernacularVariableDeclarationList *vars);
	void CompileIf(VernacularIf *vif);
	void CompileFor(VernacularFor *vfor);
	void CompileForeach(VernacularForeach *vforeach);
	void CompileReturn(VernacularReturn *vreturn);
	void CompileBreak(VernacularBreak *vbreak);

	void CompileExpression(VernacularExpression *expression);
	void CompileEvaluatingExpression(VernacularExpression *expression, VernacularBytecode targetAddress, bool reuseTarget = true);
	VernacularBytecode AddressForExpression(VernacularExpression *expression, VernacularBytecode targetAddress);
	VernacularBytecode AddressForExpression(VernacularExpression *expression);
	int CompileFunctionCall(VernacularFunctionCall *funccall, bool isNew = false);
	VernacularBytecode CompileEvaluatingFunctionCall(VernacularFunctionCall *funccall, bool isNew = false);
	void CompileBooleanLiteral(VernacularBooleanLiteral *blit, VernacularBytecode targetAddress);
	void CompileNullLiteral(VernacularNullLiteral *nlit, VernacularBytecode targetAddress);
	void CompileObjectLiteral(VernacularObjectLiteral *oblit, VernacularBytecode targetAddress, VernacularBytecode tempAddress);

	VernacularBytecode MakeLiteral(int i);
	VernacularBytecode MakeLiteral(VernacularLiteral *vlit);
	VernacularValue MakeLiteralValue(VernacularLiteral *vlit);
	VernacularBytecode MakeLiteral(VernacularIdentifier *vid);
	VernacularValue MakeLiteralValue(VernacularIdentifier *vid);
	VernacularBytecode AddressForIdentifier(std::string identifier);
	VernacularBytecode AddressForIdentifier(VernacularIdentifier *vid) { return AddressForIdentifier(vid->name.str()); }
	void RemapBreaks(VernacularBytecode loopStart, VernacularBytecode afterLoop);
	void AdjustLoopLocals(VernacularBytecode loopStart, VernacularBytecode loopEnd);

	// Returns a literal that we've created.
	VernacularValue &GetLiteral(VernacularBytecode address) { return function->literals.values[address.GetValue()]; }

	void Emit(VernacularBytecode code, Token token)
	{
		function->bytecode.push_back(code);
		if (code.GetType() == VernacularBytecode::T_INSTRUCTION && code.GetValue() == VernacularBytecode::OP_RETURN)
		{
			locals.AddVariableReference(VernacularBytecode(VernacularBytecode::T_UNMAPPED_LOCAL, 0), LastAddress());
		}
		if (code.GetType() == VernacularBytecode::T_UNMAPPED_LOCAL)
		{
			locals.AddVariableReference(code, LastAddress());
		}
		function->debugInfo.push_back(VernacularFunction::DebugInfo(token.line, token.column));
		lastToken = token;
#ifdef VERNACULAR_BYTECODE_GENERATOR_DEBUG
		std::cout << std::string(dumpLevel * 2, ' ') << "Emit " << code.ToString()
			<< " " << token.line << "," << token.column
			<< " @ " << (function->bytecode.size() - 1) << std::endl;
#endif
	}
	void Emit(VernacularBytecode code)
	{
		Emit(code, lastToken);
	}
	void EmitOpcode(VernacularBytecode::Opcode code, Token token) { Emit(VernacularBytecode(VernacularBytecode::T_INSTRUCTION, code), token); }
	void EmitInteger(int i) { Emit(VernacularBytecode(VernacularBytecode::T_INTEGER, i)); }
	VernacularBytecode LastAddress() { return VernacularBytecode(VernacularBytecode::T_INSTRUCTION_ADDRESS, function->bytecode.size() - 1); }
	VernacularBytecode NextAddress() { return VernacularBytecode(VernacularBytecode::T_INSTRUCTION_ADDRESS, function->bytecode.size()); }
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
