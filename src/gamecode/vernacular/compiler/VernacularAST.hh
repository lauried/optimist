/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <vector>
#include <list>
#include <ostream>
#include "library/text/ITokenizer.hh"
#include "log/ILogTarget.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// TODO: These all need constructors for default values.
// We also need to make sure that RTTI is present to enable us to deal with the
// tree.

struct VernacularSyntaxObject {
	// abstract
	Token token;
	virtual ~VernacularSyntaxObject();

	/**
	 * Returns true if this object and all child objects are valid.
	 * Errors are logged to console.
	 */
	virtual bool Validate(ILogTarget *log) = 0;
	virtual void Dump(std::ostream *ss, int level) = 0;

	/**
	 * Returns true if this object is of the given type.
	 */
	template <class T>
	bool Is()
	{
		T* ptr = dynamic_cast<T*>(this);
		return ptr != nullptr;
	}

	template <class T>
	T* As()
	{
		return dynamic_cast<T*>(this);
	}

protected:
	std::string TabLevel(int i) { return std::string(i, '\t'); }
};

struct VernacularStatement : public virtual VernacularSyntaxObject {
	// abstract
	virtual bool IsAction() = 0;
	virtual bool IsValue() = 0;
	virtual bool IsFlow() = 0;
};

struct VernacularBlock : public virtual VernacularSyntaxObject {
	std::vector<VernacularStatement*> statements;
	~VernacularBlock();
	bool Validate(ILogTarget *log);
	void Dump(std::ostream *ss, int level);
};

// Expressions

struct VernacularExpression : public virtual VernacularStatement {
	// abstract
};

struct VernacularIdentifier : public VernacularExpression {
	Token name;
	~VernacularIdentifier();
	bool Validate(ILogTarget *log);
	void Dump(std::ostream *ss, int level);
	bool IsAction() { return false; }
	bool IsValue() { return true; }
	bool IsFlow() { return false; }
};

// Expressions - Literals

struct VernacularLiteral : public VernacularExpression {
	// abstract
};

struct VernacularNullLiteral : public VernacularLiteral {
	Token value;
	bool Validate(ILogTarget *log) { return true; }
	void Dump(std::ostream *ss, int level);
	bool IsAction() { return false; }
	bool IsValue() { return true; }
	bool IsFlow() { return false; }
};

struct VernacularBooleanLiteral : public VernacularLiteral {
	Token value;
	bool Validate(ILogTarget *log) { return true; }
	void Dump(std::ostream *ss, int level);
	bool IsAction() { return false; }
	bool IsValue() { return true; }
	bool IsFlow() { return false; }
	// specific to boolean:
	bool Value() { return value.str().compare("false"); }
};

struct VernacularNumberLiteral : public VernacularLiteral {
	Token value;
	bool Validate(ILogTarget *log);
	void Dump(std::ostream *ss, int level);
	bool IsAction() { return false; }
	bool IsValue() { return true; }
	bool IsFlow() { return false; }
};

struct VernacularStringLiteral : public VernacularLiteral {
	Token value;
	bool Validate(ILogTarget *log);
	void Dump(std::ostream *ss, int level);
	bool IsAction() { return false; }
	bool IsValue() { return true; }
	bool IsFlow() { return false; }
};

struct VernacularFunctionLiteral : public VernacularLiteral {
	std::vector<Token> parameters; // TODO: Make these identifiers
	VernacularBlock *block;

	VernacularFunctionLiteral() : block(nullptr) {}
	bool Validate(ILogTarget *log);
	void Dump(std::ostream *ss, int level);
	bool IsAction() { return false; }
	bool IsValue() { return true; }
	bool IsFlow() { return false; }
};

struct VernacularObjectLiteral : public VernacularLiteral {
	std::vector<VernacularExpression*> values;
	bool Validate(ILogTarget *log);
	void Dump(std::ostream *ss, int level);
	bool IsAction() { return false; }
	bool IsValue() { return true; }
	bool IsFlow() { return false; }
	bool IsSimple(); // true if all values are literals and not tables
};

// Expressions - Operators

struct VernacularOperator : public VernacularExpression {
	// abstract
	int precedence;
	bool rightAssoc;

	VernacularOperator() : precedence(0), rightAssoc(false) {}
};

struct VernacularBinaryOperator : public VernacularOperator {
	Token op;
	VernacularExpression *left, *right;

	VernacularBinaryOperator() : left(nullptr), right(nullptr) {}
	bool Validate(ILogTarget *log);
	void Dump(std::ostream *ss, int level);

	bool IsAction() { return false; }
	bool IsValue() { return true; }
	bool IsFlow() { return false; }

	// These are just for binary operator, to check specific cases:
	virtual bool IsAssign() { return false; }
	virtual bool IsDot() { return !op.str().compare("."); }
};

struct VernacularAssignmentOperator : public VernacularBinaryOperator {
	enum MetaType {
		NONE,
		READONLY,
		PRIVATE,
		RESET
	};
	bool isObjectLiteral;
	MetaType metaType;
	VernacularAssignmentOperator() : isObjectLiteral(false), metaType(NONE) {}
	bool Validate(ILogTarget *log);
	bool IsAction() { return true; }
	bool IsValue() { return false; }
	bool IsFlow() { return false; }
	bool IsAssign() { return true; }
	bool IsDot() { return false; }
};

struct VernacularUnaryOperator : public VernacularOperator {
	Token op;
	// Note: Function calls and index access actually have their expression on
	// the left.
	VernacularExpression *right;

	VernacularUnaryOperator() : right(nullptr) {}
	bool Validate(ILogTarget *log);
	void Dump(std::ostream *ss, int level);
	bool IsAction() { return false; }
	bool IsValue() { return true; }
	bool IsFlow() { return false; }

	// These are just for the unary operator, to check specific cases:
	bool IsNew() { return !op.str().compare("new"); }
};

struct VernacularFunctionCall : public VernacularUnaryOperator {
	// The function being called is the unary operator's 'RHS'.
	std::vector<VernacularExpression*> parameters;

	VernacularFunctionCall() {}
	bool Validate(ILogTarget *log);
	void Dump(std::ostream *ss, int level);
	bool IsAction() { return true; }
	bool IsValue() { return true; }
	bool IsFlow() { return false; }
};

struct VernacularIndexAccess : public VernacularUnaryOperator {
	// The object being accessed is the unary operator's 'RHS'.
	VernacularExpression *key;

	VernacularIndexAccess() : key(nullptr) {}
	bool Validate(ILogTarget *log);
	void Dump(std::ostream *ss, int level);
	bool IsAction() { return false; }
	bool IsValue() { return true; }
	bool IsFlow() { return false; }
};

// This is used for building an expression.
struct VernacularGroupingOperator : public VernacularOperator {
	bool Validate(ILogTarget *log);
	void Dump(std::ostream *ss, int level);
	bool IsAction() { return false; }
	bool IsValue() { return false; }
	bool IsFlow() { return false; }
};

// Non-expression statements (mostly flow control):

struct VernacularVariableDeclaration : public VernacularSyntaxObject {
	Token variableName;
	VernacularExpression *expression;

	VernacularVariableDeclaration() : expression(nullptr) {}
	bool Validate(ILogTarget *log);
	void Dump(std::ostream *ss, int level);
	bool IsAction() { return true; }
	bool IsValue() { return false; }
	bool IsFlow() { return false; }
};

struct VernacularVariableDeclarationList : public VernacularStatement {
	std::vector<VernacularVariableDeclaration*> declarations;
	bool Validate(ILogTarget *log);
	void Dump(std::ostream *ss, int level);
	bool IsAction() { return true; }
	bool IsValue() { return false; }
	bool IsFlow() { return false; }
};

struct VernacularIf : public VernacularStatement {
	VernacularExpression *condition;
	VernacularBlock *ifBlock;
	VernacularBlock *elseBlock;

	VernacularIf() : condition(nullptr), ifBlock(nullptr), elseBlock(nullptr) {}
	bool Validate(ILogTarget *log);
	void Dump(std::ostream *ss, int level);
	bool IsAction() { return false; }
	bool IsValue() { return false; }
	bool IsFlow() { return true; }
};

struct VernacularFor : public VernacularStatement {
	VernacularStatement *initializer;
	VernacularExpression *condition;
	VernacularStatement *updater;
	VernacularBlock *block;

	VernacularFor() : initializer(nullptr), condition(nullptr), updater(nullptr) {}
	bool Validate(ILogTarget *log);
	void Dump(std::ostream *ss, int level);
	bool IsAction() { return false; }
	bool IsValue() { return false; }
	bool IsFlow() { return true; }
};

struct VernacularForeach : public VernacularStatement {
	VernacularExpression *object;
	Token key;
	Token value;
	VernacularBlock *block;

	VernacularForeach() : object(nullptr), block(nullptr) {}
	bool Validate(ILogTarget *log);
	void Dump(std::ostream *ss, int level);
	bool IsAction() { return false; }
	bool IsValue() { return false; }
	bool IsFlow() { return true; }
};

struct VernacularReturn : public VernacularStatement {
	VernacularExpression *returnValue;

	VernacularReturn() : returnValue(nullptr) {}
	bool Validate(ILogTarget *log);
	void Dump(std::ostream *ss, int level);
	bool IsAction() { return false; }
	bool IsValue() { return false; }
	bool IsFlow() { return true; }
};

struct VernacularBreak : public VernacularStatement {
	Token token;
	int levels;

	VernacularBreak() : levels(1) {}
	bool Validate(ILogTarget *log) { return true; }
	void Dump(std::ostream *ss, int level);
	bool IsAction() { return false; }
	bool IsValue() { return false; }
	bool IsFlow() { return true; }
};

/**
 * Maintains ownership of objects created when managing an AST.
 */
class VernacularAST {
public:
	std::string filename;

	VernacularBlock *root;

	template<class T>
	T* Allocate()
	{
		T* t = new T();
		objects.push_back(t);
		return t;
	}

	~VernacularAST()
	{
		for (auto it = objects.begin(); it != objects.end(); ++it)
		{
			delete *it;
		}
		objects.clear();
	}

	/**
	 * Returns true if the tree is valid, else false.
	 * Should output validation errors to console.
	 */
	bool Validate(ILogTarget *log);

	void Dump(std::ostream *ss)
	{
		root->Dump(ss, 0);
		std::cout << std::endl << std::endl;
	}

private:
	std::list<VernacularSyntaxObject*> objects;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
