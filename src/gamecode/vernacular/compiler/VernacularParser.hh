/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "library/text/ITokenizer.hh"
#include "library/text/TokenParser.hh"
#include "gamecode/vernacular/compiler/VernacularAST.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Tokenizes and parses Vernacular scripting language into an abstract syntax
 * tree (AST).
 * The AST represents what parseable syntax is saying, but its mere construction
 * doesn't guarantee that it's valid use of that syntax. For example, we could
 * write:
 *   new 2;
 * Which would parse as an expression: a new operator with a numeric literal as
 * its right hand side. This would be caught later when the tree is validated,
 * because new requires its right hand side to be a function call operator.
 */
class VernacularParser {
public:
	VernacularParser() : parser(nullptr), tree(nullptr) {}
	~VernacularParser();

	VernacularAST* Parse(const char *data, int dataLength);

private:
	TokenParser *parser;
	VernacularAST* tree;

	VernacularBlock* ParseBlock();
	VernacularStatement* ParseStatement();
	VernacularBlock* ParseBlockOrStatement();
	VernacularExpression* ParseExpression();
	VernacularFunctionCall* ParseFunctionCall();
	VernacularVariableDeclarationList* ParseVariableDeclarationList();
	VernacularVariableDeclaration* ParseVariableDeclaration();
	VernacularObjectLiteral* ParseObjectLiteral();
	VernacularFunctionLiteral* ParseFunctionLiteral();
	VernacularIndexAccess* ParseIndexAccess();
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
