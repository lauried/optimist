/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/vernacular/compiler/VernacularParser.hh"
#include <stdexcept>
#include <iostream>
#include <stack>
#include <map>
#include "library/text/String.hh"
#include "library/text/Tokenizer.hh"
#include "library/text/TokenParser.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

#ifdef VERNACULAR_PARSER_DEBUG
#define DBG(a) std::cout << a << " next='" << parser->PeekTokenSafe()->str() << "'" << std::endl;
#define CDBG(x) x;
#else
#define DBG(a)
#define CDBG(x)
#endif

// Matches a string literal
struct StringLiteralTokenType : public TokenType {
	int TokenLength(const char *buffer, int maxLength)
	{
		if (maxLength < 2)
		{
			return 0;
		}
		if (buffer[0] != '"')
		{
			return 0;
		}
		bool escape = false;
		for (int len = 1; len < maxLength; len += 1)
		{
			if (escape)
			{
				escape = false;
				continue;
			}
			if (buffer[len] == '\\')
			{
				escape = true;
				continue;
			}
			if (buffer[len] != '"')
			{
				continue;
			}
			return len + 1;
		}
		return 0;
	}
};

// Match comment types
struct CommentTokenType : public TokenType {
	int TokenLength(const char *buffer, int maxLength)
	{
		if (maxLength < 2)
		{
			return 0;
		}
		if (buffer[0] != '/')
		{
			return 0;
		}
		if (buffer[1] == '/')
		{
			for (int len = 2; len < maxLength; len += 1)
			{
				if (buffer[len] == '\n' || buffer[len] == '\r')
				{
					return len;
				}
			}
			return maxLength;
		}
		else if (buffer[1] == '*' && maxLength > 3)
		{
			for (int len = 2; len < maxLength; len += 1)
			{
				if (buffer[len - 1] == '*' && buffer[len] == '/')
				{
					return len + 1;
				}
			}
			return maxLength;
		}
		return 0;
	}
};

//-----------------------------------------------------------------------------
// VernacularParser
//-----------------------------------------------------------------------------

VernacularParser::~VernacularParser()
{
}

VernacularAST* VernacularParser::Parse(const char *data, int dataLength)
{
	// The token types are scanned in order so they need to be ordered such
	// that simpler token types aren't caught by more broad types.
	Tokenizer tokenizer;
	tokenizer.AddTokenType("comment", new CommentTokenType(), Tokenizer::SKIP);
	tokenizer.AddTokenType("nullliteral", new StringMatchTokenType("null"));
	tokenizer.AddTokenType("booleanliteral", new StringMatchTokenType(std::vector<std::string>(
		{ "true", "false" }
	)));
	tokenizer.AddTokenType("numberliteral", new RegexTokenType("-?0x[0-9abcdefABCDEF]+"));
	tokenizer.AddTokenType("numberliteral", new RegexTokenType("-?0b[01]+"));
	tokenizer.AddTokenType("numberliteral", new RegexTokenType("-?[0-9]*\\.[0-9]+"));
	tokenizer.AddTokenType("numberliteral", new RegexTokenType("-?[0-9]+"));
	// All operators are called "operator" because we hardly care about their
	// individual type until emitting bytecode. We do need to tell if they're
	// unary operators at this point though.
	tokenizer.AddTokenType("op_binary", new StringMatchTokenType(std::vector<std::string>(
		{
			"**", "*", "&&", "&", "||", "|",
			":=", ":+=", ":-=", ":*=", ":/=", ":**=", ":%=", ":$=", ":^=", ":<<=", ":>>=", ":|=", ":&=",
			"!=", ">=", "<=", "=", ">>", "<<", ">", "<", "^", "%", "/", "+", ".", "$"
		}
	)));
	tokenizer.AddTokenType("op_unary", new StringMatchTokenType(std::vector<std::string>(
		{ "!", "~", "new" }
	)));
	tokenizer.AddTokenType("op_both", new StringMatchTokenType("-"));
	tokenizer.AddTokenType("brace_open", new StringMatchTokenType("{"));
	tokenizer.AddTokenType("brace_close", new StringMatchTokenType("}"));
	tokenizer.AddTokenType("bracket_open", new StringMatchTokenType("["));
	tokenizer.AddTokenType("bracket_close", new StringMatchTokenType("]"));
	tokenizer.AddTokenType("paren_open", new StringMatchTokenType("("));
	tokenizer.AddTokenType("paren_close", new StringMatchTokenType(")"));
	tokenizer.AddTokenType("semicolon", new StringMatchTokenType(";"));
	tokenizer.AddTokenType("comma", new StringMatchTokenType(","));
	tokenizer.AddTokenType("stringliteral", new StringLiteralTokenType());
	// Keywords mostly just prevent us from reading keywords as identifiers.
	// TODO: Add a whole-word-only option for string match token type.
	// It will read one character ahead of the actual token and make sure that
	// it's not alphanumeric.
	tokenizer.AddTokenType("keyword", new StringMatchTokenType(std::vector<std::string>(
		{ "if", "else", "foreach", "key", "value", "in", "for", "return", "var", "function", "break", "@private", "@readonly", "@reset" }
	), StringMatchTokenType::WHOLE_WORDS_ONLY));
	tokenizer.AddTokenType("identifier", new RegexTokenType("[a-zA-Z_][a-zA-Z0-9_]*"));
	// The SKIP flag means the tokenizer will hide the whitespace tokens from us.
	tokenizer.AddTokenType("whitespace", new RegexTokenType("[\\r\\n\\t ]+"), Tokenizer::SKIP);

	TokenParser tp(data, dataLength, &tokenizer);
	parser = &tp;
	tree = new VernacularAST();

	try
	{
		tree->root = ParseBlock();
		parser->ExpectEof();
	}
	catch (ParseException &ex)
	{
		delete tree;
		tree = nullptr;
		throw;
	}
	return tree;
}

VernacularBlock* VernacularParser::ParseBlock()
{
	DBG("start block");
	VernacularBlock *block = new VernacularBlock();
	if (!parser->Eof())
	{
		block->token = *(parser->PeekToken());
	}
	while (!parser->Eof() && parser->PeekToken()->str().compare("}"))
	{
		block->statements.push_back(ParseStatement());
	}
	DBG("end block (" << block->statements.size() << "stmts)");
	return block;
}

VernacularStatement* VernacularParser::ParseStatement()
{
	DBG("start statement");
	// Either a valid statement keyword, otherwise we parse a variable followed
	// by a function call or an assignment operator.
	std::string token = parser->PeekToken()->str();

	if (!token.compare("if"))
	{
		VernacularIf *vif = tree->Allocate<VernacularIf>();
		vif->token = *(parser->PeekToken());

		parser->ExpectToken("if");
		parser->ExpectToken("(");
		vif->condition = ParseExpression();
		parser->ExpectToken(")");
		vif->ifBlock = ParseBlockOrStatement();
		if (!parser->Eof() && !parser->PeekToken()->str().compare("else"))
		{
			parser->ExpectToken("else");
			vif->elseBlock = ParseBlockOrStatement();
		}
		DBG("end statement (if)");
		return vif;
	}
	else if (!token.compare("for"))
	{
		VernacularFor *vfor = tree->Allocate<VernacularFor>();
		vfor->token = *(parser->PeekToken());

		parser->ExpectToken("for");
		parser->ExpectToken("(");
		if (parser->PeekToken()->str().compare(";"))
		{
			vfor->initializer = ParseStatement();
		}
		else
		{
			parser->ExpectToken(";");
		}
		if (parser->PeekToken()->str().compare(";"))
		{
			vfor->condition = ParseExpression();
			parser->ExpectToken(";");
		}
		else
		{
			parser->ExpectToken(";");
		}
		if (parser->PeekToken()->str().compare(")"))
		{
			vfor->updater = ParseExpression();
		}
		parser->ExpectToken(")");
		vfor->block = ParseBlockOrStatement();
		DBG("end statement (for)");
		return vfor;
	}
	else if (!token.compare("foreach"))
	{
		VernacularForeach *vforeach = tree->Allocate<VernacularForeach>();
		vforeach->token = *(parser->PeekToken());

		parser->ExpectToken("foreach");
		parser->ExpectToken("(");
		bool expectValue = true;
		if (!parser->PeekToken()->str().compare("key"))
		{
			parser->ExpectToken("key");
			vforeach->key = parser->ExpectTokenType("identifier");
			if (parser->PeekToken()->str().compare(","))
			{
				expectValue = false;
			}
			else
			{
				parser->ExpectToken(",");
			}
		}
		if (expectValue && !parser->PeekToken()->str().compare("value"))
		{
			parser->ExpectToken("value");
			vforeach->value = parser->ExpectTokenType("identifier");
		}
		parser->ExpectToken("in");
		vforeach->object = ParseExpression();
		parser->ExpectToken(")");
		vforeach->block = ParseBlockOrStatement();
		DBG("end statement (foreach)");
		return vforeach;
	}
	else if (!token.compare("return"))
	{
		VernacularReturn *vreturn = tree->Allocate<VernacularReturn>();
		vreturn->token = *(parser->PeekToken());

		parser->ExpectToken("return");
		if (parser->PeekToken()->str().compare(";"))
		{
			vreturn->returnValue = ParseExpression();
		}
		parser->ExpectToken(";");
		DBG("end statement (return)");
		return vreturn;
	}
	else if (!token.compare("break"))
	{
		VernacularBreak *vbreak = tree->Allocate<VernacularBreak>();
		vbreak->token = parser->ExpectToken("break");
		std::string str = parser->PeekToken()->str();
		if (String::IsNumeric(str) && str[0] != '0')
		{
			vbreak->levels = String::ToInt(str);
		}
		parser->ExpectToken(";");
		return vbreak;
	}
	else if (!token.compare("var"))
	{
		VernacularVariableDeclarationList *vdecl = ParseVariableDeclarationList();
		parser->ExpectToken(";");
		DBG("end statement (var)");
		return vdecl;
	}
	else
	{
		VernacularExpression *expression = ParseExpression();
		DBG("start expect token");
		parser->ExpectToken(";"); // TODO: We're expecting this token, and we can peek it, but we never reach it.
		DBG("end expect token");
		DBG("end statement (expression)");
		return expression;
	}
}

VernacularBlock* VernacularParser::ParseBlockOrStatement()
{
	DBG("start block or statement");
	if (!parser->PeekToken()->str().compare("{"))
	{
		parser->ExpectToken("{");
		VernacularBlock *block = ParseBlock();
		parser->ExpectToken("}");
		DBG("end block or statement (block)");
		return block;
	}

	VernacularBlock *block = tree->Allocate<VernacularBlock>();
	block->token = *(parser->PeekToken());
	block->statements.push_back(ParseStatement());
	DBG("end block or statement (statement)");
	return block;
}

// Helpers for ParseExpression

struct OperatorPrecedenceInfo {
	int precedence; // lower takes precedence.
	bool rightAssoc;
};

static void SetOperatorPrecedenceEtc(VernacularBinaryOperator *op)
{
	// "**", "*", "&&", "&", "||", "|", ":=", "!=", ">=", "<=", "=", ">>", "<<", ">", "<", "^", "%", "/", "+", "."
	// "-"
	static std::map<std::string, OperatorPrecedenceInfo> opInfo = {
		{ ".",  { 0,  false }},
		{ "**", { 4,  true }},
		{ "*",  { 5,  false }},
		{ "/",  { 5,  false }},
		{ "%",  { 5,  false }},
		{ "+",  { 6,  false }},
		{ "$",  { 6,  false }},
		{ "-",  { 6,  false }},
		{ "<<", { 7,  false }},
		{ ">>", { 7,  false }},
		{ "&",  { 8,  false }},
		{ "^",  { 9,  false }},
		{ "|",  { 10, false }},
		{ "<",  { 11, false }},
		{ ">",  { 11, false }},
		{ "<=", { 11, false }},
		{ ">=", { 11, false }},
		{ "=",  { 12, false }},
		{ "!=", { 12, false }},
		{ "&&", { 13, false }},
		{ "||", { 14, false }},
		{ ":=", { 15, true }},
		{ ":+=", { 15, true }},
		{ ":-=", { 15, true }},
		{ ":*=", { 15, true }},
		{ ":/=", { 15, true }},
		{ ":**=", { 15, true }},
		{ ":$=", { 15, true }},
		{ ":%=", { 15, true }},
		{ ":^=", { 15, true }},
		{ ":<<=", { 15, true }},
		{ ":>>=", { 15, true }},
		{ ":|=", { 15, true }},
		{ ":&=", { 15, true }},
	};
	auto it = opInfo.find(op->op.str());
	if (it == opInfo.end())
	{
		throw std::logic_error("Unknown binary operator token");
	}
	op->precedence = it->second.precedence;
	op->rightAssoc = it->second.rightAssoc;
}

static void SetOperatorPrecedenceEtc(VernacularUnaryOperator *op)
{
	// function call, index access
	// "!", "~"
	// "-"
	static std::map<std::string, OperatorPrecedenceInfo> opInfo = {
		{ "[",   { 0, false }},
		{ "(",   { 0, false }},
		{ "new", { 3, true }},
		{ "-",   { 3, true }},
		{ "!",   { 4, true }},
		{ "~",   { 4, true }},
	};
	auto it = opInfo.find(op->op.str());
	if (it == opInfo.end())
	{
		throw std::logic_error("Unknown unary operator token");
	}
	op->precedence = it->second.precedence;
	op->rightAssoc = it->second.rightAssoc;
}

static void PushOutput(std::stack<VernacularExpression*> *output, VernacularExpression *item)
{
	auto bi = dynamic_cast<VernacularBinaryOperator*>(item);
	if (bi != nullptr)
	{
		if (output->size() < 2)
		{
			throw ParseException(bi->op.line, bi->op.column, "Error parsing expression. Missing closing parenthesis?");
		}

		bi->right = output->top();
		output->pop();
		bi->left = output->top();
		output->pop();
		output->push(bi);
		return;
	}

	auto un = dynamic_cast<VernacularUnaryOperator*>(item);
	if (un != nullptr)
	{
		if (output->size() < 1)
		{
			throw ParseException(un->op.line, un->op.column, "Error parsing expression. Missing closing parenthesis?");
		}

		un->right = output->top();
		output->pop();
		output->push(un);
		return;
	}

	// It's not an operator; just push it.
	output->push(item);
}

VernacularExpression* VernacularParser::ParseExpression()
{
	DBG("start expression");
	std::stack<VernacularExpression*> output;
	std::stack<VernacularOperator*> operators;
	int groupingParentheses = 0;

	enum {
		OPERAND,
		OPERATOR
	} expects = OPERAND;

	do
	{
		VernacularOperator *op = nullptr;
		VernacularExpression *operand = nullptr;
		bool endGroup = false;
		int row = 0, col = 0;

		if (expects == OPERAND)
		{
			DBG("operand...");
			expects = OPERATOR;
			if (!parser->PeekToken()->typeId.compare("identifier"))
			{
				DBG("... identifier");
				auto a = tree->Allocate<VernacularIdentifier>();
				a->token = *(parser->PeekToken());
				a->name = parser->GetToken();
				operand = a;
			}
			else if (!parser->PeekToken()->typeId.compare("nullliteral"))
			{
				DBG("... null literal");
				auto a = tree->Allocate<VernacularNullLiteral>();
				a->token = *(parser->PeekToken());
				a->value = parser->GetToken();
				operand = a;
			}
			else if (!parser->PeekToken()->typeId.compare("booleanliteral"))
			{
				DBG("... boolean literal");
				auto a = tree->Allocate<VernacularBooleanLiteral>();
				a->token = *(parser->PeekToken());
				a->value = parser->GetToken();
				operand = a;
			}
			else if (!parser->PeekToken()->typeId.compare("numberliteral"))
			{
				DBG("... number literal");
				auto a = tree->Allocate<VernacularNumberLiteral>();
				a->token = *(parser->PeekToken());
				a->value = parser->GetToken();
				operand = a;
			}
			else if (!parser->PeekToken()->typeId.compare("stringliteral"))
			{
				DBG("... string literal");
				auto a = tree->Allocate<VernacularStringLiteral>();
				a->token = *(parser->PeekToken());
				a->value = parser->GetToken();
				operand = a;
			}
			else if (!parser->PeekToken()->str().compare("{"))
			{
				DBG("... object literal");
				auto a = ParseObjectLiteral();
				operand = a;
			}
			else if (!parser->PeekToken()->str().compare("function"))
			{
				DBG("... function literal");
				auto a = ParseFunctionLiteral();
				operand = a;
			}
			else if (!parser->PeekToken()->typeId.compare("op_unary") || !parser->PeekToken()->typeId.compare("op_both"))
			{
				DBG("... unary operator");
				expects = OPERAND;
				auto a = tree->Allocate<VernacularUnaryOperator>();
				a->token = *(parser->PeekToken());
				a->op = parser->GetToken();
				SetOperatorPrecedenceEtc(a);
				op = a;
			}
			else if (!parser->PeekToken()->str().compare("("))
			{
				DBG("... grouping paren");
				expects = OPERAND;
				groupingParentheses += 1;
                auto a = tree->Allocate<VernacularGroupingOperator>();
				a->token = parser->GetToken();
				op = a;
			}
			else
			{
				DBG("... end exp");
				// It's the end of the expression. We leave the token in the
				// parser for outside code to handle.
				break;
			}
		}
		else
		{
			DBG("operator...");
			expects = OPERAND;
			if (!parser->PeekToken()->typeId.compare("op_binary") || !parser->PeekToken()->typeId.compare("op_both"))
			{
				DBG("... binary operator");
				VernacularBinaryOperator *a;
				if (
					!parser->PeekToken()->str().compare(":=")
					|| !parser->PeekToken()->str().compare(":+=")
					|| !parser->PeekToken()->str().compare(":-=")
					|| !parser->PeekToken()->str().compare(":*=")
					|| !parser->PeekToken()->str().compare(":/=")
					|| !parser->PeekToken()->str().compare(":**=")
					|| !parser->PeekToken()->str().compare(":$=")
					|| !parser->PeekToken()->str().compare(":%=")
					|| !parser->PeekToken()->str().compare(":^=")
					|| !parser->PeekToken()->str().compare(":<<=")
					|| !parser->PeekToken()->str().compare(":>>=")
					|| !parser->PeekToken()->str().compare(":|=")
					|| !parser->PeekToken()->str().compare(":&=")
				)
				{
					auto assign = tree->Allocate<VernacularAssignmentOperator>();
					a = assign;
					a->token = *(parser->PeekToken());
					a->op = parser->GetToken();
					if (!parser->PeekToken()->str().compare("@private"))
					{
						assign->metaType = VernacularAssignmentOperator::PRIVATE;
						parser->GetToken();
					}
					else if (!parser->PeekToken()->str().compare("@readonly"))
					{
						assign->metaType = VernacularAssignmentOperator::READONLY;
						parser->GetToken();
					}
					else if (!parser->PeekToken()->str().compare("@reset"))
					{
						assign->metaType = VernacularAssignmentOperator::RESET;
						parser->GetToken();
					}
				}
				else
				{
					a = tree->Allocate<VernacularBinaryOperator>();
					a->token = *(parser->PeekToken());
					a->op = parser->GetToken();
				}
				SetOperatorPrecedenceEtc(a);
				op = a;
			}
			else if (!parser->PeekToken()->str().compare("("))
			{
				DBG("... function call");
				expects = OPERATOR;
				auto a = ParseFunctionCall();
				SetOperatorPrecedenceEtc(a);
				op = a;
			}
			else if (!parser->PeekToken()->str().compare("["))
			{
				DBG("... index access");
				expects = OPERATOR;
				auto a = ParseIndexAccess();
				SetOperatorPrecedenceEtc(a);
				op = a;
			}
			else if (!parser->PeekToken()->str().compare(")"))
			{
				DBG("... endgroup");
				// In a function call, this marks the end of the parameter list
				// so we have to keep track of parentheses and end the expression if it's unbalanced.
				// Unbalanced parentheses are caught when we look for whatever follows the expression.
				if (groupingParentheses == 0)
				{
					break;
				}
				expects = OPERATOR;
				groupingParentheses -= 1;
				Token token = parser->GetToken();
				row = token.line;
				col = token.column;
				endGroup = true;
			}
			else
			{
				DBG("... end");
				// It's the end of the expression. We leave the token in the
				// parser for outside code to handle.
				break;
			}
		}
		if (operand != nullptr)
		{
			CDBG(std::cout << "out " << operand->token.str() << std::endl)
			output.push(operand);
			continue;
		}
		if (op != nullptr)
		{
			// If it's not a grouping start operator,
			// we pop other operators with higher precedence.
			// Here lower precedence means performed first, so we pop operators
			// with higher precedence.
			if (dynamic_cast<VernacularGroupingOperator*>(op) == nullptr)
			{
				while (
					operators.size() > 0 &&
					dynamic_cast<VernacularGroupingOperator*>(operators.top()) == nullptr &&
					(
						(op->rightAssoc == false && op->precedence >= operators.top()->precedence) ||
						(op->rightAssoc == true && op->precedence > operators.top()->precedence)
					)
				)
				{
					CDBG(std::cout << "pop1 " << operators.top()->token.str() << std::endl)
					PushOutput(&output, operators.top());
					operators.pop();
				}
			}
			CDBG(std::cout << "push " << op->token.str() << std::endl)
			operators.push(op);
			continue;
		}
		if (endGroup)
		{
			while (operators.size() > 0 && dynamic_cast<VernacularGroupingOperator*>(operators.top()) == nullptr)
			{
				CDBG(std::cout << "pop2 " << operators.top()->token.str() << std::endl)
				PushOutput(&output, operators.top());
				operators.pop();
			}
			if (operators.size() == 0)
			{
				// We shouldn't actually reach here, because an extra closing parenthesis ends the expression just in case it's inside a function call.
				throw ParseException(row, col, "Unexpected closing parenthesis in expression.");
			}
			CDBG(std::cout << "pop3 " << operators.top()->token.str() << std::endl)
			operators.pop(); // pops the startgroup corresponding to the endgroup.
			continue;
		}

		throw std::logic_error("No operand, operator or endgroup set");
	}
	while (true);

	// Push the last of the operators onto the output stack.
	while (operators.size() > 0)
	{
		PushOutput(&output, operators.top());
		operators.pop();
	}

	if (output.size() == 0)
	{
		int row = parser->PeekToken()->line;
		int col = parser->PeekToken()->column;
		throw ParseException(row, col, "Expected expression, found '" + parser->PeekToken()->str() + "'");
	}
	if (output.size() > 1)
	{
		int row = parser->PeekToken()->line;
		int col = parser->PeekToken()->column;
		throw ParseException(row, col, "Something mismatched in expression.");
	}
	DBG("end expression");
	return output.top();
}

VernacularFunctionCall* VernacularParser::ParseFunctionCall()
{
	DBG("start function call");
	VernacularFunctionCall *call = tree->Allocate<VernacularFunctionCall>();
	call->token = *(parser->PeekToken());
	call->op = parser->ExpectToken("(");
	while(parser->PeekToken()->str().compare(")"))
	{
		call->parameters.push_back(ParseExpression());
		if (parser->PeekToken()->str().compare(","))
		{
			break;
		}
		parser->ExpectToken(",");
	}
	parser->ExpectToken(")");
	DBG("end function call");
	return call;
}

VernacularVariableDeclarationList* VernacularParser::ParseVariableDeclarationList()
{
	DBG("start variable declaration list");
	VernacularVariableDeclarationList *vdecl = tree->Allocate<VernacularVariableDeclarationList>();
	vdecl->token = *(parser->PeekToken());
	parser->ExpectToken("var");

	for (;;)
	{
		VernacularVariableDeclaration *vdec = ParseVariableDeclaration();
		vdecl->declarations.push_back(vdec);
		if (parser->PeekToken()->str().compare(","))
		{
			break;
		}
		parser->ExpectToken(",");
	}
	DBG("end variable declaration list");
	return vdecl;
}

VernacularVariableDeclaration* VernacularParser::ParseVariableDeclaration()
{
	DBG("start variable declaration");
	VernacularVariableDeclaration *vdec = tree->Allocate<VernacularVariableDeclaration>();
	vdec->token = *(parser->PeekToken());
	vdec->variableName = parser->ExpectTokenType("identifier");
	if (!parser->PeekToken()->str().compare(":="))
	{
		parser->ExpectToken(":=");
		vdec->expression = ParseExpression();
	}
	DBG("end variable declaration");
	return vdec;
}

VernacularObjectLiteral* VernacularParser::ParseObjectLiteral()
{
	DBG("start object literal");
	VernacularObjectLiteral *objl = tree->Allocate<VernacularObjectLiteral>();
	objl->token = *(parser->PeekToken());
	parser->ExpectToken("{");
	for (bool first = true; parser->PeekToken()->str().compare("}"); first = false)
	{
		if (!first)
		{
			parser->ExpectToken(",");
		}
		VernacularExpression *exp = ParseExpression();
		if (exp->Is<VernacularAssignmentOperator>())
		{
			exp->As<VernacularAssignmentOperator>()->isObjectLiteral = true;
		}
		objl->values.push_back(exp);
	}
	parser->ExpectToken("}");
	DBG("end object literal");
	return objl;
}

VernacularFunctionLiteral* VernacularParser::ParseFunctionLiteral()
{
	DBG("start function literal");
	VernacularFunctionLiteral *func = tree->Allocate<VernacularFunctionLiteral>();
	func->token = *(parser->PeekToken());
	parser->ExpectToken("function");
	parser->ExpectToken("(");
	for (bool first = true; parser->PeekToken()->str().compare(")"); first = false)
	{
		if (!first)
		{
			parser->ExpectToken(",");
		}
		func->parameters.push_back(parser->ExpectTokenType("identifier"));
	}
    parser->ExpectToken(")");
    parser->ExpectToken("{");
    func->block = ParseBlock();
    parser->ExpectToken("}");
	DBG("end function literal");
    return func;
}

VernacularIndexAccess* VernacularParser::ParseIndexAccess()
{
	DBG("start index access");
	VernacularIndexAccess *ia = tree->Allocate<VernacularIndexAccess>();
	ia->token = *(parser->PeekToken());
	ia->op = parser->ExpectToken("[");
	ia->key = ParseExpression();
	parser->ExpectToken("]");
	DBG("end index access");
	return ia;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
