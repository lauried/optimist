/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <string>
#include <vector>
#include "gamecode/vernacular/interpreter/VernacularBytecode.hh"
#include "library/text/ITokenizer.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/*
Typical usage:

Named:
When we encounter a 'var' statement, we call AddVariable(name).

Whenever the varaible is referred to in some expression (including its
initialization, we call AddVariableReference(varAddr, bytecodeOffset) passing
the address of the variable and its offset in bytecode.

Anonymous:
Whenever we need a temporary variable, we call AddVariable().
Whenever that temporary variable is referred to in some expression, we call
AddVariableReference(varAddr, bytecodeOffset).

When the entire function is written, we call RemapAddresses with our bytecode,
to map everything down to a smaller address space where usage overlaps.

*/

/**
 * Stores information on local variables during the compilation of a function.
 */
class VernacularLocalsData {
public:
/**
	 * Creates an anonymous variable.
	 * Returns the local address reserved for it.
	 */
	VernacularBytecode AddVariable();

	/**
	 * Creates a named local variable.
	 * Returns the local address reserved for it.
	 */
	VernacularBytecode AddVariable(Token token, int blockDepth, bool canRemap = true);

	/**
	 * Finds an existing named variable which is still in scope.
	 * If we can't find the variable, return global:0
	 */
	VernacularBytecode FindVariable(std::string name);

	/**
	 * Removes variables at the given block level from scope, preventing them
	 * from being found by name and causing new ones to be created.
	 */
	void ClearScopeDepth(int blockDepth);

	/**
	 * Updates the first and last use of a vernacular address.
	 */
	void AddVariableReference(VernacularBytecode variable, VernacularBytecode bytecodeOfsfet);

	/**
	 * If the variable's first use is before the start of the loop, it will
	 * update its last use to at least the end of the loop.
	 */
	void AddVariableLoopReference(VernacularBytecode variable, VernacularBytecode loopStart, VernacularBytecode loopEnd);

	/**
	 * Remaps all variable references to reuse address space.
	 * This must be performed at the very end.
	 * It returns the number of addresses used.
	 */
	int RemapAddresses(std::vector<VernacularBytecode> &bytecode);

private:
	struct LocalsData {
		std::string name;
		VernacularBytecode address;
		VernacularBytecode firstUse;
		VernacularBytecode lastUse;
		int remapTo;
		int blockDepth;

		LocalsData() : remapTo(-1), blockDepth(0) {}
		LocalsData(VernacularBytecode address) : address(address), remapTo(-1), blockDepth(0) {}
		LocalsData(VernacularBytecode address, int blockDepth, std::string name) : address(address), name(name), remapTo(-1), blockDepth(blockDepth) {}
	};
	std::vector<LocalsData> addresses;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
