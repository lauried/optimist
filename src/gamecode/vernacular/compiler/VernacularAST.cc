/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/vernacular/compiler/VernacularAST.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

#if 0
#define DBG(a) std::cout << a << " " << token.str() << std::endl;
#else
#define DBG(a)
#endif

//-----------------------------------------------------------------------------
// VernacularSyntaxObject
//-----------------------------------------------------------------------------

VernacularSyntaxObject::~VernacularSyntaxObject()
{
}

//-----------------------------------------------------------------------------

VernacularBlock::~VernacularBlock()
{
}

VernacularIdentifier::~VernacularIdentifier()
{
}

//-----------------------------------------------------------------------------
// Validation
//-----------------------------------------------------------------------------

bool VernacularAST::Validate(ILogTarget *log)
{
	Token token;
	DBG("VernacularAST validate");
	return root->Validate(log);
}

bool VernacularBlock::Validate(ILogTarget *log)
{
	DBG("VernacularBlock validate");
	bool valid = true;
	for (auto it = statements.begin(); it != statements.end(); ++it)
	{
		VernacularStatement *statement = *it;
		if (!statement->IsAction() && !statement->IsFlow())
		{
			valid = false;
			log->Start(ILogTarget::ERROR)
				->Write("@")->Write(std::to_string(statement->token.line))->Write(",")->Write(std::to_string(statement->token.column))
				->Write(": Statement does not perform an action")->End();
		}
		if (!(*it)->Validate(log))
		{
			valid = false;
		}
	}
	return valid;
}

bool VernacularIdentifier::Validate(ILogTarget *log)
{
	DBG("VernacularIdentifier validate");
	return true;
}

bool VernacularNumberLiteral::Validate(ILogTarget *log)
{
	DBG("VernacularNumberLiteral validate");
	return true;
}

bool VernacularStringLiteral::Validate(ILogTarget *log)
{
	DBG("VernacularStringLiteral validate");
	return true;
}

bool VernacularFunctionLiteral::Validate(ILogTarget *log)
{
	// The parser guarantees that all our parameters are identifiers.
	// They're tokens, so we can't check now.
	return block->Validate(log);
}

bool VernacularObjectLiteral::Validate(ILogTarget *log)
{
	DBG("VernacularObjectLiteral validate");
	bool valid = true;
	for (auto it = values.begin(); it != values.end(); ++it)
	{
		VernacularExpression *exp = *it;
		if (!exp->Validate(log))
		{
			valid = false;
		}
		auto op = dynamic_cast<VernacularAssignmentOperator*>(exp);
        if (op != nullptr)
        {
			auto left = dynamic_cast<VernacularIdentifier*>(op->left);
			if (left == nullptr)
			{
				valid = false;
				log->Start(ILogTarget::ERROR)
					->Write("@")->Write(std::to_string(exp->token.line))->Write(",")->Write(std::to_string(exp->token.column))
					->Write(": Assignment in object literal must assign to a single identifier")->End();
				continue;
			}
		}
	}
	return valid;
}

// A simple table literal contains only non-table literals.
bool VernacularObjectLiteral::IsSimple()
{
	for (auto it = values.begin(); it != values.end(); ++it)
	{
		VernacularExpression *exp = *it;
		if (exp->Is<VernacularLiteral>() && !exp->Is<VernacularObjectLiteral>())
		{
			continue;
		}
		if (exp->Is<VernacularAssignmentOperator>())
		{
			auto assign = exp->As<VernacularAssignmentOperator>();
			// Literal, not an object literal, and has no meta type.
			if (assign->right->Is<VernacularLiteral>() && !assign->right->Is<VernacularObjectLiteral>() && assign->metaType == VernacularAssignmentOperator::NONE)
			{
				continue;
			}
		}
		return false;
	}
	return true;
}

bool VernacularBinaryOperator::Validate(ILogTarget *log)
{
	DBG("VernacularBinaryOperator validate");
	bool valid = true;
	if (!left->IsValue())
	{
		valid = false;
		log->Start(ILogTarget::ERROR)
			->Write("@")->Write(std::to_string(left->token.line))->Write(",")->Write(std::to_string(left->token.column))
			->Write(": Expresion must evaluate to a value")->End();
	}
	if (!right->IsValue())
	{
		valid = false;
		log->Start(ILogTarget::ERROR)
			->Write("@")->Write(std::to_string(right->token.line))->Write(",")->Write(std::to_string(right->token.column))
			->Write(": Expresion must evaluate to a value")->End();
	}
	// If we're a dot, the right must be an identifier.
	if (IsDot())
	{
		if (!right->Is<VernacularIdentifier>())
		{
			valid = false;
			log->Start(ILogTarget::ERROR)
				->Write("@")->Write(std::to_string(right->token.line))->Write(",")->Write(std::to_string(right->token.column))
				->Write(": Right of dot operator must be identifier")->End();
		}
	}
	if (!left->Validate(log))
	{
		valid = false;
	}
	if (!right->Validate(log))
	{
		valid = false;
	}
	return valid;
}

bool VernacularAssignmentOperator::Validate(ILogTarget *log)
{
	DBG("VernacularAssignmentOperator validate");
	bool valid = VernacularBinaryOperator::Validate(log);
	if (op.str().compare(":="))
	{
		if (metaType != NONE)
		{
			valid = false;
			log->Start(ILogTarget::ERROR)
				->Write("@")->Write(std::to_string(right->token.line))->Write(",")->Write(std::to_string(right->token.column))
				->Write(": Meta modifiers cannot be used with assignment+operation, e.g. :+= etc.")->End();
		}
		if (left->Is<VernacularUnaryOperator>())
		{
			auto tmpLeft = left->As<VernacularUnaryOperator>();
			if (tmpLeft->IsNew())
			{
				valid = false;
				log->Start(ILogTarget::ERROR)
					->Write("@")->Write(std::to_string(right->token.line))->Write(",")->Write(std::to_string(right->token.column))
					->Write(": Assignment+operation, e.g. :+= etc. cannot be used with 'new' operator")->End();
			}
		}
	}
	if (metaType != NONE && !isObjectLiteral)
	{
		if (!left->Is<VernacularBinaryOperator>() || !left->As<VernacularBinaryOperator>()->IsDot())
		{
			valid = false;
			log->Start(ILogTarget::ERROR)
				->Write("@")->Write(std::to_string(right->token.line))->Write(",")->Write(std::to_string(right->token.column))
				->Write(": Left of assignment with meta modifier must be dot operator")->End();
		}
		else
		{
			auto tmpLeft = left->As<VernacularBinaryOperator>();
			if (!tmpLeft->left->Is<VernacularIdentifier>() || tmpLeft->left->As<VernacularIdentifier>()->token.str().compare("this"))
			{
				valid = false;
				log->Start(ILogTarget::ERROR)
					->Write("@")->Write(std::to_string(right->token.line))->Write(",")->Write(std::to_string(right->token.column))
					->Write(": Meta modifiers can only be used on assignment to 'this', or inside an object literal")->End();
			}
		}
	}
	return valid;
}

bool VernacularUnaryOperator::Validate(ILogTarget *log)
{
	DBG("VernacularUnaryOperator validate");
	bool valid = true;
	if (!right->IsValue())
	{
		valid = false;
		log->Start(ILogTarget::ERROR)
			->Write("@")->Write(std::to_string(right->token.line))->Write(",")->Write(std::to_string(right->token.column))
			->Write(": Expresion must evaluate to a value")->End();
	}
	if (IsNew())
	{
		if (!right->Is<VernacularFunctionCall>())
		{
			valid = false;
			log->Start(ILogTarget::ERROR)
				->Write("@")->Write(std::to_string(right->token.line))->Write(",")->Write(std::to_string(right->token.column))
				->Write(": Right of new operator must evaluate to a function call")->End();
		}
		else
		{
			VernacularFunctionCall *funccall = right->As<VernacularFunctionCall>();
			if ( (funccall->right->Is<VernacularBinaryOperator>() && funccall->right->As<VernacularBinaryOperator>()->IsDot())
				|| funccall->right->Is<VernacularIndexAccess>())
			{
				valid = false;
				log->Start(ILogTarget::ERROR)
					->Write("@")->Write(std::to_string(right->token.line))->Write(",")->Write(std::to_string(right->token.column))
					->Write(": Cannot apply new operator to object method call")->End();
			}
		}
	}
	if (!right->Validate(log))
	{
		valid = false;
	}
	return valid;
}

bool VernacularFunctionCall::Validate(ILogTarget *log)
{
	DBG("VernacularFunctionCall validate");
	bool valid = true;
	if (!VernacularUnaryOperator::Validate(log))
	{
		valid = false;
	}
	for (auto it = parameters.begin(); it != parameters.end(); ++it)
	{
		VernacularExpression *exp = *it;
		if (!exp->IsValue())
		{
			valid = false;
			log->Start(ILogTarget::ERROR)
				->Write("@")->Write(std::to_string(exp->token.line))->Write(",")->Write(std::to_string(exp->token.column))
				->Write(": Expresion must evaluate to a value")->End();
		}
		if (!exp->Validate(log))
		{
			valid = false;
		}
	}
	return valid;
}

bool VernacularIndexAccess::Validate(ILogTarget *log)
{
	DBG("VernacularIndexAccess validate");
	bool valid = true;
	if (!VernacularUnaryOperator::Validate(log))
	{
		valid = false;
	}
	if (!key->IsValue())
	{
		valid = false;
		log->Start(ILogTarget::ERROR)
			->Write("@")->Write(std::to_string(key->token.line))->Write(",")->Write(std::to_string(key->token.column))
			->Write(": Expresion must evaluate to a value")->End();
	}
	if (!key->Validate(log))
	{
		valid = false;
	}
	return valid;
}

bool VernacularGroupingOperator::Validate(ILogTarget *log)
{
	DBG("VernacularGroupingOperator validate");
	// TODO: It's probably a logic error for this to make its way into the tree.
	log->Start(ILogTarget::ERROR)
		->Write("@")->Write(std::to_string(token.line))->Write(",")->Write(std::to_string(token.column))
		->Write(": Grouping operator found its way into the Abstract Syntax Tree (error in compiler)")->End();
	return false;
}

bool VernacularVariableDeclaration::Validate(ILogTarget *log)
{
	DBG("VernacularVariableDeclaration validate");
	bool valid = true;
	// token name guaranteed to be an identifier
	if (expression != nullptr)
	{
		if (!expression->IsValue())
		{
			log->Start(ILogTarget::ERROR)
				->Write("@")->Write(std::to_string(expression->token.line))->Write(",")->Write(std::to_string(expression->token.column))
				->Write(": Expresion must evaluate to a value")->End();
			valid = false;
		}
		if (!expression->Validate(log))
		{
			valid = false;
		}
	}
	return valid;
}

bool VernacularVariableDeclarationList::Validate(ILogTarget *log)
{
	DBG("VernacularVariableDeclarationList validate");
	bool valid = true;
	for (auto it = declarations.begin(); it != declarations.end(); ++it)
	{
		if (!(*it)->Validate(log))
		{
			valid = false;
		}
	}
	return valid;
}

bool VernacularIf::Validate(ILogTarget *log)
{
	DBG("VernacularIf validate");
	bool valid = true;
	if (condition != nullptr)
	{
		if (!condition->IsValue())
		{
			log->Start(ILogTarget::ERROR)
				->Write("@")->Write(std::to_string(condition->token.line))->Write(",")->Write(std::to_string(condition->token.column))
				->Write(": Expresion must evaluate to a value")->End();
			valid = false;
		}
		if (!condition->Validate(log))
		{
			valid = false;
		}
	}
	if (ifBlock != nullptr && !ifBlock->Validate(log))
	{
		valid = false;
	}
	if (elseBlock != nullptr && !elseBlock->Validate(log))
	{
		valid = false;
	}
	return valid;
}

bool VernacularFor::Validate(ILogTarget *log)
{
	DBG("VernacularFor validate");
	bool valid = true;
	if (initializer != nullptr)
	{
		// TODO: Check that the initializer is an expression or variable declaration.
		if (!initializer->IsAction())
		{
			log->Start(ILogTarget::ERROR)
				->Write("@")->Write(std::to_string(initializer->token.line))->Write(",")->Write(std::to_string(initializer->token.column))
				->Write(": Expresion must perform an action")->End();
			valid = false;
		}
		if (!initializer->Validate(log))
		{
			valid = false;
		}
	}
	if (condition != nullptr)
	{
		if (!condition->IsValue())
		{
			log->Start(ILogTarget::ERROR)
				->Write("@")->Write(std::to_string(condition->token.line))->Write(",")->Write(std::to_string(condition->token.column))
				->Write(": Expresion must evaluate to a value")->End();
			valid = false;
		}
		if (!condition->Validate(log))
		{
			valid = false;
		}
	}
	if (updater != nullptr)
	{
		if (!updater->IsAction())
		{
			log->Start(ILogTarget::ERROR)
				->Write("@")->Write(std::to_string(updater->token.line))->Write(",")->Write(std::to_string(updater->token.column))
				->Write(": Expresion must perform an action")->End();
			valid = false;
		}
		if (!updater->Validate(log))
		{
			valid = false;
		}
	}
	if (!block->Validate(log))
	{
		valid = false;
	}
	return valid;
}

bool VernacularForeach::Validate(ILogTarget *log)
{
	DBG("VernacularForeach validate");
	bool valid = true;
	if (!object->IsValue())
	{
		log->Start(ILogTarget::ERROR)
			->Write("@")->Write(std::to_string(object->token.line))->Write(",")->Write(std::to_string(object->token.column))
			->Write(": Expresion must evaluate to a value")->End();
		valid = false;
	}
	if (!object->Validate(log))
	{
		valid = false;
	}
	if (!block->Validate(log))
	{
		valid = false;
	}
	return valid;
}

bool VernacularReturn::Validate(ILogTarget *log)
{
	DBG("VernacularReturn validate");
	bool valid = true;
	if (returnValue != nullptr)
	{
		if (!returnValue->IsValue())
		{
			log->Start(ILogTarget::ERROR)
				->Write("@")->Write(std::to_string(returnValue->token.line))->Write(",")->Write(std::to_string(returnValue->token.column))
				->Write(": Expresion must evaluate to a value")->End();
			valid = false;
		}
		if (!returnValue->Validate(log))
		{
			valid = false;
		}
	}
	return valid;
}

//-----------------------------------------------------------------------------
// Dump
//-----------------------------------------------------------------------------

// Dump expects the first line to be indented already.

void VernacularBlock::Dump(std::ostream *ss, int level)
{
	(*ss) << "VernacularBlock {" << std::endl;
	for (int i = 0; i < statements.size(); i += 1)
	{
		(*ss) << TabLevel(level + 1) << "statement = ";
		statements[i]->Dump(ss, level + 1);
		(*ss) << std::endl;
	}
	(*ss) << TabLevel(level) << "}";
}

void VernacularIdentifier::Dump(std::ostream *ss, int level)
{
	(*ss) << "VernacularIdentifier{" << name.str() << "}";
}

void VernacularNullLiteral::Dump(std::ostream *ss, int level)
{
	(*ss) << "VernacularNullLiteral{" << value.str() << "}";
}

void VernacularBooleanLiteral::Dump(std::ostream *ss, int level)
{
	(*ss) << "VernacularBooleanLiteral{" << value.str() << "}";
}

void VernacularNumberLiteral::Dump(std::ostream *ss, int level)
{
	(*ss) << "VernacularNumberLiteral{" << value.str() << "}";
}

void VernacularStringLiteral::Dump(std::ostream *ss, int level)
{
	(*ss) << "VernacularStringLiteral{" << value.str() << "}";
}

void VernacularFunctionLiteral::Dump(std::ostream *ss, int level)
{
	(*ss) << "VernacularFunctionLiteral {" << std::endl;
	for (int i = 0; i < parameters.size(); i += 1)
	{
		(*ss) << TabLevel(level + 1) << "parameter = " << parameters[i].str() << std::endl;
	}
	(*ss) << TabLevel(level + 1) << "block = ";
	block->Dump(ss, level + 1);
	(*ss) << std::endl;
	(*ss) << TabLevel(level) << "}";
}

void VernacularObjectLiteral::Dump(std::ostream *ss, int level)
{
	(*ss) << "VernacularObjectLiteral {" << std::endl;
	for (int i = 0; i < values.size(); i += 1)
	{
		(*ss) << TabLevel(level + 1) << "statement = ";
		values[i]->Dump(ss, level + 1);
		(*ss) << std::endl;
	}
	(*ss) << TabLevel(level) << "}";
}

void VernacularBinaryOperator::Dump(std::ostream *ss, int level)
{
	(*ss) << "VernacularBinaryOperator( " << op.str() << " ) {" << std::endl;
	(*ss) << TabLevel(level + 1) << "left = ";
	left->Dump(ss, level + 1);
	(*ss) << std::endl;
	(*ss) << TabLevel(level + 1) << "right = ";
	right->Dump(ss, level + 1);
	(*ss) << std::endl;
	(*ss) << TabLevel(level) << "}";
}

void VernacularUnaryOperator::Dump(std::ostream *ss, int level)
{
	(*ss) << "VernacularUnaryOperator( " << op.str() << " ) {" << std::endl;
	(*ss) << TabLevel(level + 1) << "operand = ";
	right->Dump(ss, level + 1);
	(*ss) << std::endl;
	(*ss) << TabLevel(level) << "}";
}

void VernacularFunctionCall::Dump(std::ostream *ss, int level)
{
	(*ss) << "VernacularFunctionCall {" << std::endl;
	if (right != nullptr)
	{
		(*ss) << TabLevel(level + 1) << "right = ";
		right->Dump(ss, level + 1);
		(*ss) << std::endl;
	}
	for (int i = 0; i < parameters.size(); i += 1)
	{
		(*ss) << TabLevel(level + 1) << "parameter = ";
		parameters[i]->Dump(ss, level + 1);
		(*ss) << std::endl;
	}
	(*ss) << TabLevel(level) << "}";
}

void VernacularIndexAccess::Dump(std::ostream *ss, int level)
{
	(*ss) << "VernacularIndexAccess {" << std::endl;
	(*ss) << TabLevel(level + 1) << "object = ";
	right->Dump(ss, level + 1);
	(*ss) << std::endl;
	(*ss) << TabLevel(level + 1) << "key = ";
	key->Dump(ss, level + 1);
	(*ss) << std::endl;
	(*ss) << TabLevel(level) << "}";
}

void VernacularGroupingOperator::Dump(std::ostream *ss, int level)
{
	(*ss) << "VernacularGroupingOperator{!!!!!}";
}

void VernacularVariableDeclaration::Dump(std::ostream *ss, int level)
{
	(*ss) << "VernacularVariableDeclaration {" << std::endl;
	(*ss) << TabLevel(level + 1) << "name = " << variableName.str() << std::endl;
	if (expression != nullptr)
	{
		(*ss) << TabLevel(level + 1) << "value = ";
		expression->Dump(ss, level + 1);
		(*ss) << std::endl;
	}
	(*ss) << TabLevel(level) << "}";
}

void VernacularVariableDeclarationList::Dump(std::ostream *ss, int level)
{
	(*ss) << "VernacularVariableDeclarationList {" << std::endl;
	for (int i = 0; i < declarations.size(); i += 1)
	{
		(*ss) << TabLevel(level + 1) << "var = ";
		declarations[i]->Dump(ss, level + 1);
		(*ss) << std::endl;
	}
	(*ss) << TabLevel(level) << "}";
}

void VernacularIf::Dump(std::ostream *ss, int level)
{
	(*ss) << "VernacularIf {" << std::endl;
	if (condition != nullptr)
	{
		(*ss) << TabLevel(level + 1) << "condition = ";
		condition->Dump(ss, level + 1);
		(*ss) << std::endl;
	}
	if (ifBlock)
	{
		(*ss) << TabLevel(level + 1) << "ifBlock = ";
		ifBlock->Dump(ss, level + 1);
		(*ss) << std::endl;
	}
	if (elseBlock)
	{
		(*ss) << TabLevel(level + 1) << "elseBlock = ";
		elseBlock->Dump(ss, level + 1);
		(*ss) << std::endl;
	}
	(*ss) << TabLevel(level) << "}";
}

void VernacularFor::Dump(std::ostream *ss, int level)
{
	(*ss) << "VernacularFor {" << std::endl;
	if (initializer != nullptr)
	{
		(*ss) << TabLevel(level + 1) << "initializer = ";
		initializer->Dump(ss, level + 1);
		(*ss) << std::endl;
	}
	if (condition != nullptr)
	{
		(*ss) << TabLevel(level + 1) << "condition = ";
		condition->Dump(ss, level + 1);
		(*ss) << std::endl;
	}
	if (updater != nullptr)
	{
		(*ss) << TabLevel(level + 1) << "updater = ";
		updater->Dump(ss, level + 1);
		(*ss) << std::endl;
	}
	if (block != nullptr)
	{
		(*ss) << TabLevel(level + 1) << "block = ";
		block->Dump(ss, level + 1);
		(*ss) << std::endl;
	}
	(*ss) << TabLevel(level) << "}";
}

void VernacularForeach::Dump(std::ostream *ss, int level)
{
	(*ss) << "VernacularForeach {" << std::endl;
	(*ss) << TabLevel(level + 1) << "key = " << key.str() << std::endl;
	(*ss) << TabLevel(level + 1) << "value = " << value.str() << std::endl;
	if (object != nullptr)
	{
		(*ss) << TabLevel(level + 1) << "object = ";
		object->Dump(ss, level + 1);
		(*ss) << std::endl;
	}
	if (block != nullptr)
	{
		(*ss) << TabLevel(level + 1) << "block = ";
		block->Dump(ss, level + 1);
		(*ss) << std::endl;
	}
	(*ss) << TabLevel(level) << "}";
}

void VernacularReturn::Dump(std::ostream *ss, int level)
{
	(*ss) << "VernacularReturn {" << std::endl;
	if (returnValue != nullptr)
	{
		(*ss) << TabLevel(level + 1) << "expression = ";
		returnValue->Dump(ss, level + 1);
		(*ss) << std::endl;
	}
	(*ss) << TabLevel(level) << "}";
}

void VernacularBreak::Dump(std::ostream *ss, int level)
{
	(*ss) << "VernacularBreak{}";
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
