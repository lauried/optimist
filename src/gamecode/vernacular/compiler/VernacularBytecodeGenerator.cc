/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include <sstream>
#include "gamecode/vernacular/compiler/VernacularBytecodeGenerator.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// We use assert syntax for convenience.
// But we throw an exception so that compiler bugs don't cause the entire game
// to crash, allowing modders to work around a bug if one occurs.
// This ought to be moved to some library somewhere.
#define ASSERTX(EX) if (!(EX)) { throw std::logic_error("Assert failure: " #EX); }

#ifdef VERNACULAR_BYTECODE_GENERATOR_DEBUG
#define DBG(a) std::cout << std::string(dumpLevel * 2, ' ') <<  a << std::endl;
#define DBGST(a) std::cout << std::string(dumpLevel * 2, ' ') << a << std::endl; dumpLevel += 1;
#define DBGEN(a) dumpLevel -= 1; std::cout << std::string(dumpLevel * 2, ' ') << a << std::endl;
#else
#define DBG(a)
#define DBGST(a)
#define DBGEN(a)
#endif

#if 0
#define VERNACULAR_BYTECODE_GENERATOR_DEBUG_DUMP_UNMAPPED_FUNCTIONS
#endif

//-----------------------------------------------------------------------------
// Lookup opcodes
//-----------------------------------------------------------------------------

struct OpcodeLookup {
	VernacularBytecode::Opcode opcode;
	std::string symbol;
};

static std::vector<OpcodeLookup> gBinaryOpcodes = {
	{ VernacularBytecode::OP_DOTGET, "." },
	{ VernacularBytecode::OP_POWER, "**" },
	{ VernacularBytecode::OP_MULT, "*" },
	{ VernacularBytecode::OP_LOGAND, "&&" },
	{ VernacularBytecode::OP_BITAND, "&" },
	{ VernacularBytecode::OP_LOGOR, "||" },
	{ VernacularBytecode::OP_BITOR, "|" },
	{ VernacularBytecode::OP_NEQUAL, "!=" },
	{ VernacularBytecode::OP_GEQUAL, ">=" },
	{ VernacularBytecode::OP_LEQUAL, "<=" },
	{ VernacularBytecode::OP_EQUAL, "=" },
	{ VernacularBytecode::OP_BITRIGHT, ">>" },
	{ VernacularBytecode::OP_BITLEFT, "<<" },
	{ VernacularBytecode::OP_GREATER, ">" },
	{ VernacularBytecode::OP_LESS, "<" },
	{ VernacularBytecode::OP_BITXOR, "^" },
	{ VernacularBytecode::OP_MOD, "%" },
	{ VernacularBytecode::OP_DIV, "/" },
	{ VernacularBytecode::OP_ADD, "+" },
	{ VernacularBytecode::OP_SUB, "-" },
	{ VernacularBytecode::OP_CONCAT, "$" },
};

static std::vector<OpcodeLookup> gUnaryOpcodes = {
	{ VernacularBytecode::OP_LOGNOT, "!" },
	{ VernacularBytecode::OP_BITNOT, "~" },
	{ VernacularBytecode::OP_NEGATE, "-" },
};

static VernacularBytecode::Opcode OpcodeForSymbol(std::string symbol, std::vector<OpcodeLookup> *table)
{
	for (int i = 0; i < table->size(); i += 1)
	{
		if (!symbol.compare(table->at(i).symbol))
		{
			return table->at(i).opcode;
		}
	}
	throw std::logic_error("Could not find operator for symbol " + symbol);
}

//-----------------------------------------------------------------------------
// VernacularBytecodeGenerator
//-----------------------------------------------------------------------------

VernacularFunction* VernacularBytecodeGenerator::CompileFunction(VernacularAST *syntax)
{
	DBGST("VernacularBytecodeGenerator::CompileFunction(VernacularAST)")

	// Address spaces:
	// Globals are just created as references in function->globalReferences
	// Literals are stored in function->literals
	// Locals are dealt with in this->locals. (VernacularLocalsData)

	// Traverse the tree emitting the bytecode and updating state for all
	// the nodes. Note that when we're doing the root function, we handle
	// locals and globals slightly differently:
	// In a normal function, var declares local variables, and undeclared
	// variables are kept to wire up to globals.
	// In the root function, var declares global variables. We can still delay
	// undeclared variables to wire up to existing globals.

	// For function literals we create a new instance of BytecodeGenerator
	// with access to our factories and the global array.

	// TODO: Handle root block differently from function literals.
	// This is done the same for the whole bytecode generator instance, because
	// we create a new one for compiling function literals.

	// Actually, we could just forbid 'var' in global scope.
	// Then when we go to run a global scope function, instead of seeking
	// globals we map them.

	filename = syntax->filename;
	globalScope = true;
	nextIterator = 0;
	function = functionFactory->Allocate();
	function->filename = filename;
	ASSERTX(syntax->root != nullptr);
	locals.AddVariable();
	CompileBlock(syntax->root);
	VernacularBytecode opReturn(VernacularBytecode::OP_RETURN);
	EmitOpcode(VernacularBytecode::OP_RETURN, lastToken);
#ifdef VERNACULAR_BYTECODE_GENERATOR_DEBUG_DUMP_UNMAPPED_FUNCTIONS
	function->Dump(&std::cout);
#endif
	function->localAddressSpace = locals.RemapAddresses(function->bytecode);

	DBGEN("End")

	return function;
}

VernacularFunction* VernacularBytecodeGenerator::CompileFunction(VernacularFunctionLiteral *func)
{
	DBGST("VernacularBytecodeGenerator::CompileFunction(VernacularFunctionLiteral)")

	globalScope = false;
	nextIterator = 0;
	function = functionFactory->Allocate();
	function->filename = filename;
	function->numParameters = func->parameters.size();
	if (func->parameters.size() < 1)
	{
		locals.AddVariable();
	}
	for (auto it = func->parameters.begin(); it != func->parameters.end(); ++it)
	{
		VernacularBytecode variable = locals.AddVariable(*it, blockDepth, false);
		locals.AddVariableReference(variable, VernacularBytecode(VernacularBytecode::T_INSTRUCTION_ADDRESS, 0));
	}
	ASSERTX(func->block != nullptr);
	CompileBlock(func->block);
	VernacularBytecode opReturn(VernacularBytecode::OP_RETURN);
	EmitOpcode(VernacularBytecode::OP_RETURN, lastToken);
#ifdef VERNACULAR_BYTECODE_GENERATOR_DEBUG_DUMP_UNMAPPED_FUNCTIONS
	function->Dump(&std::cout);
#endif
	function->localAddressSpace = locals.RemapAddresses(function->bytecode);

	DBGEN("End")

	return function;
}

void VernacularBytecodeGenerator::CompileBlock(VernacularBlock *block)
{
	DBGST("VernacularBytecodeGenerator::CompileBlock")
	blockDepth += 1;
	for (auto it = block->statements.begin(); it != block->statements.end(); ++it)
	{
		CompileStatement(*it);
	}
	locals.ClearScopeDepth(blockDepth);
	blockDepth -= 1;
	DBGEN("End")
}

void VernacularBytecodeGenerator::CompileStatement(VernacularStatement *statement)
{
	DBGST("VernacularBytecodeGenerator::CompileStatement")

	if (statement->Is<VernacularExpression>())
	{
		CompileExpression(statement->As<VernacularExpression>());
	}
	else if (statement->Is<VernacularVariableDeclarationList>())
	{
		CompileVariableDeclarationList(statement->As<VernacularVariableDeclarationList>());
	}
	else if (statement->Is<VernacularIf>())
	{
		CompileIf(statement->As<VernacularIf>());
	}
	else if (statement->Is<VernacularFor>())
	{
		VernacularBytecode loopStart = NextAddress();
		loopStack.push(loopStart);
		CompileFor(statement->As<VernacularFor>());
		RemapBreaks(loopStart, NextAddress());
		loopStack.pop();
		AdjustLoopLocals(loopStart, LastAddress());

	}
	else if (statement->Is<VernacularForeach>())
	{
		VernacularBytecode loopStart = NextAddress();
		loopStack.push(loopStart);
		CompileForeach(statement->As<VernacularForeach>());
		VernacularBytecode loopEnd = LastAddress();
		RemapBreaks(loopStart, NextAddress());
		loopStack.pop();
		AdjustLoopLocals(loopStart, LastAddress());
	}
	else if (statement->Is<VernacularReturn>())
	{
		CompileReturn(statement->As<VernacularReturn>());
	}
	else if (statement->Is<VernacularBreak>())
	{
		CompileBreak(statement->As<VernacularBreak>());
	}
	else
	{
		throw std::logic_error("Unhandled statement type encountered.");
	}

	DBGEN("End")
}

//-----------------------------------------------------------------------------
// Simple block compilation
//-----------------------------------------------------------------------------

void VernacularBytecodeGenerator::CompileVariableDeclarationList(VernacularVariableDeclarationList *vars)
{
	DBGST("VernacularBytecodeGenerator::CompileVariableDeclarationList")

	// All variables are either initialized by their expression or implicitly
	// initialized to null.
	for (int i = 0; i < vars->declarations.size(); i += 1)
	{
		VernacularBytecode variable = locals.AddVariable(vars->declarations[i]->variableName, blockDepth);
		if (vars->declarations[i]->expression)
		{
			CompileEvaluatingExpression(vars->declarations[i]->expression, variable);
		}
		else
		{
			EmitOpcode(VernacularBytecode::OP_NULLIFY, vars->declarations[i]->variableName);
			Emit(variable);
		}
	}

	DBGEN("End")
}

void VernacularBytecodeGenerator::CompileIf(VernacularIf *vif)
{
	DBGST("VernacularBytecodeGenerator::CompileIf")

	VernacularBytecode tempAddr = locals.AddVariable();
	CompileEvaluatingExpression(vif->condition, tempAddr);
	EmitOpcode(VernacularBytecode::OP_JFALSE, vif->token);
	Emit(tempAddr);
	Emit(VernacularBytecode()); // Dummy address to be rewritten with the end of the if block.
	VernacularBytecode ifJumpTarget = LastAddress();
	ASSERTX(vif->ifBlock != nullptr);
	CompileBlock(vif->ifBlock);

	VernacularBytecode elseJumpTarget;
	if (vif->elseBlock != nullptr)
	{
		// If there's an else block then the end of the if block has to jump over it.
		EmitOpcode(VernacularBytecode::OP_JMP, vif->token);
		Emit(VernacularBytecode()); // Dummy address to be rewritten the end of the else block.
		elseJumpTarget.Set(LastAddress());
	}

	// Update the address we jump to to jump over the whole if block when the condition is false.
	function->bytecode[ifJumpTarget.GetValue()].Set(NextAddress());

	if (vif->elseBlock != nullptr)
	{
		CompileBlock(vif->elseBlock);
		// Update the jump over the else block from the end of the if block.
		function->bytecode[elseJumpTarget.GetValue()].Set(NextAddress());
	}

	DBGEN("End")
}

void VernacularBytecodeGenerator::CompileFor(VernacularFor *vfor)
{
	DBGST("VernacularBytecodeGenerator::CompileFor")

	blockDepth += 1;

	if (vfor->initializer != nullptr)
	{
		CompileStatement(vfor->initializer);
	}
	EmitOpcode(VernacularBytecode::OP_JMP, vfor->token);
	Emit(VernacularBytecode());
	VernacularBytecode startJumpTarget = LastAddress();
	VernacularBytecode loopStart = NextAddress();
	ASSERTX(vfor->block != nullptr);
	CompileBlock(vfor->block);
	if (vfor->updater != nullptr)
	{
		CompileStatement(vfor->updater);
	}
	function->bytecode[startJumpTarget.GetValue()].Set(NextAddress());
	if (vfor->condition != nullptr)
	{
		VernacularBytecode temp = locals.AddVariable();
		CompileEvaluatingExpression(vfor->condition, temp);
		EmitOpcode(VernacularBytecode::OP_JTRUE, vfor->token);
		Emit(temp);
		Emit(loopStart);
	}
	else
	{
		EmitOpcode(VernacularBytecode::OP_JMP, vfor->token);
		Emit(loopStart);
	}

	locals.ClearScopeDepth(blockDepth);
	blockDepth -= 1;

	DBGEN("End")
}

void VernacularBytecodeGenerator::CompileForeach(VernacularForeach *vforeach)
{
	DBGST("VernacularBytecodeGenerator::CompileForeach")

	blockDepth += 1;

	int iterator = nextIterator;
	nextIterator += 1;
	if (function->numIterators < nextIterator)
	{
		function->numIterators = nextIterator;
	}

	VernacularBytecode objAddr = AddressForExpression(vforeach->object);
	EmitOpcode(VernacularBytecode::OP_ITSTART, vforeach->token);
	Emit(objAddr);
	Emit(VernacularBytecode(VernacularBytecode::T_INTEGER, iterator));
	EmitOpcode(VernacularBytecode::OP_JMP, vforeach->token); // jump to the end of the block, where the iteration check is performed
	Emit(VernacularBytecode());
	VernacularBytecode jumpAddr = LastAddress();
	VernacularBytecode loopStart = NextAddress();
	if (vforeach->key.size() > 0)
	{
		if (vforeach->value.size() > 0)
		{
			EmitOpcode(VernacularBytecode::OP_ITKEYVAL, vforeach->key);
			Emit(VernacularBytecode(VernacularBytecode::T_INTEGER, iterator));
			Emit(locals.AddVariable(vforeach->key, blockDepth + 1));
			Emit(locals.AddVariable(vforeach->value, blockDepth + 1));
		}
		else
		{
			EmitOpcode(VernacularBytecode::OP_ITKEY, vforeach->key);
			Emit(VernacularBytecode(VernacularBytecode::T_INTEGER, iterator));
			Emit(locals.AddVariable(vforeach->key, blockDepth + 1));
		}
	}
	else if (vforeach->value.size() > 0)
	{
		EmitOpcode(VernacularBytecode::OP_ITVALUE, vforeach->value);
		Emit(VernacularBytecode(VernacularBytecode::T_INTEGER, iterator));
		Emit(locals.AddVariable(vforeach->value, blockDepth + 1));
	}
	ASSERTX(vforeach->block != nullptr);
	CompileBlock(vforeach->block);
	function->bytecode[jumpAddr.GetValue()].Set(NextAddress()); // update the jump at the start
	EmitOpcode(VernacularBytecode::OP_ITNEXTJ, vforeach->token);
	Emit(VernacularBytecode(VernacularBytecode::T_INTEGER, iterator));
	Emit(loopStart);
	nextIterator -= 1;

	locals.ClearScopeDepth(blockDepth);
	blockDepth -= 1;

	DBGEN("End")
}

void VernacularBytecodeGenerator::CompileReturn(VernacularReturn *vreturn)
{
	DBGST("VernacularBytecodeGenerator::CompileReturn")

	if (vreturn->returnValue != nullptr)
	{
		// We tell the evaluating expression not to use local 0 as a temporary, because we might need it as a parameter.
		CompileEvaluatingExpression(vreturn->returnValue, VernacularBytecode(VernacularBytecode::T_LOCAL, 0), false);
	}
	else
	{
		EmitOpcode(VernacularBytecode::OP_NULLIFY, vreturn->token);
		Emit(VernacularBytecode(VernacularBytecode::T_LOCAL, 0));
	}
	EmitOpcode(VernacularBytecode::OP_RETURN, vreturn->token);

	DBGEN("End")
}

void VernacularBytecodeGenerator::CompileBreak(VernacularBreak *vbreak)
{
	DBGST("VernacularBytecodeGenerator::CompileBreak")

	if (vbreak->levels > loopStack.size())
	{
		throw VernacularCompileException(vbreak->token.line, vbreak->token.column, "Break specifies more levels than there are loops for it to be nested within.");
	}

	EmitOpcode(VernacularBytecode::OP_JMP, vbreak->token);
	Emit(VernacularBytecode()); // dummy address
	breakList.push_back(BreakInfo(loopStack[loopStack.size() - vbreak->levels], LastAddress()));
	// After the loop is parsed, the breakList is checked for breaks from that
	// loop, and the address will be updated.

	DBGEN("End")
}

//-----------------------------------------------------------------------------
// Expression compilation (also literals)
//-----------------------------------------------------------------------------

static VernacularBytecode::Opcode GetAssignmentOperator(std::string op)
{
	//:+= :-= :*= :/= :**= :%= :$= :^= :<<= :>>= :|= :&=
	if (!op.compare(":+="))
	{
		return VernacularBytecode::OP_ADD;
	}
	if (!op.compare(":-="))
	{
		return VernacularBytecode::OP_SUB;
	}
	if (!op.compare(":*="))
	{
		return VernacularBytecode::OP_MULT;
	}
	if (!op.compare(":/="))
	{
		return VernacularBytecode::OP_DIV;
	}
	if (!op.compare(":**="))
	{
		return VernacularBytecode::OP_POWER;
	}
	if (!op.compare(":%="))
	{
		return VernacularBytecode::OP_MOD;
	}
	if (!op.compare(":$="))
	{
		return VernacularBytecode::OP_CONCAT;
	}
	if (!op.compare(":^="))
	{
		return VernacularBytecode::OP_BITXOR;
	}
	if (!op.compare(":<<="))
	{
		return VernacularBytecode::OP_BITLEFT;
	}
	if (!op.compare(":>>="))
	{
		return VernacularBytecode::OP_BITRIGHT;
	}
	if (!op.compare(":|="))
	{
		return VernacularBytecode::OP_BITOR;
	}
	if (!op.compare(":&="))
	{
		return VernacularBytecode::OP_BITAND;
	}
	return VernacularBytecode::OP_NULL;
}

void VernacularBytecodeGenerator::CompileExpression(VernacularExpression *expression)
{
	DBGST("VernacularBytecodeGenerator::CompileExpression")

	// Note: Except where necessary, statements should always be evaluated left-to-right.
	// (Or rather, in language reading order, which will also be token-order.)
	// This more closely meets the user's expectations.

	ASSERTX(expression->IsAction());

	// Check if it's a function call unary.
	if (expression->Is<VernacularFunctionCall>())
	{
		CompileFunctionCall(expression->As<VernacularFunctionCall>());
		DBGEN("End")
		return;
	}

	if (expression->Is<VernacularAssignmentOperator>())
	{
		VernacularAssignmentOperator *assign = expression->As<VernacularAssignmentOperator>();
		ASSERTX(assign->left != nullptr);
		ASSERTX(assign->right != nullptr);
		if (assign->left->Is<VernacularIdentifier>())
		{
			VernacularBytecode::Opcode opcode = GetAssignmentOperator(assign->op.str());
			if (opcode != VernacularBytecode::OP_NULL)
			{
				VernacularBytecode value = AddressForIdentifier(assign->left->As<VernacularIdentifier>()->name.str());
				VernacularBytecode temp = locals.AddVariable();
				CompileEvaluatingExpression(assign->right, temp);
				EmitOpcode(opcode, assign->token);
				Emit(value);
				Emit(temp);
				Emit(value);
			}
			else
			{
				// TODO: Check if we're self-referential before deciding to use a temp address.
				// This makes the code relaly messy though.
#if 1
				VernacularBytecode value = AddressForIdentifier(assign->left->As<VernacularIdentifier>()->name.str());
				VernacularBytecode temp = locals.AddVariable();
				CompileEvaluatingExpression(assign->right, temp);
				EmitOpcode(VernacularBytecode::OP_CPY, assign->token);
				Emit(temp);
				Emit(value);
#else
				VernacularBytecode value = AddressForIdentifier(assign->left->As<VernacularIdentifier>()->name.str());
				EmitOpcode(VernacularBytecode::OP_CPY, assign->token);
#endif
			}
		}
		else if (assign->left->Is<VernacularBinaryOperator>() && assign->left->As<VernacularBinaryOperator>()->IsDot())
		{
			VernacularBinaryOperator *dot = assign->left->As<VernacularBinaryOperator>();
			VernacularIdentifier *identifier = dot->right->As<VernacularIdentifier>();
			if (identifier == nullptr)
			{
				throw VernacularCompileException(assign->op.line, assign->op.column, "Right of dot operator must be an identifier.");
			}
			VernacularBytecode object = locals.AddVariable();
			CompileEvaluatingExpression(dot->left, object);
			VernacularBytecode value = locals.AddVariable();
			CompileEvaluatingExpression(assign->right, value);

			// If the assignment also has an operator:
			VernacularBytecode::Opcode opcode = GetAssignmentOperator(assign->op.str());
			if (opcode != VernacularBytecode::OP_NULL)
			{
				VernacularBytecode temp = locals.AddVariable();
				// Read current state into temp
				EmitOpcode(VernacularBytecode::OP_DOTGET, assign->token);
				Emit(object);
				Emit(MakeLiteral(identifier));
				Emit(temp);
				// Do operator(temp, value) -> value
				EmitOpcode(opcode, assign->token);
				Emit(temp);
				Emit(value);
				Emit(value);
			}

			if (assign->metaType == VernacularAssignmentOperator::NONE)
			{
				EmitOpcode(VernacularBytecode::OP_DOTSET, assign->token);
			}
			else
			{
				EmitOpcode(VernacularBytecode::OP_DOTSET_META, assign->token);
			}
			Emit(object);
			Emit(MakeLiteral(identifier));
			Emit(value);
			switch (assign->metaType)
			{
			case VernacularAssignmentOperator::RESET:
				EmitInteger(0);
				break;
			case VernacularAssignmentOperator::PRIVATE:
				EmitInteger(1);
				break;
			case VernacularAssignmentOperator::READONLY:
				EmitInteger(2);
				break;
			default:
				break;
			}
		}
		else if (assign->left->Is<VernacularIndexAccess>())
		{
			VernacularIndexAccess *index = assign->left->As<VernacularIndexAccess>();
			VernacularBytecode object = AddressForExpression(index->right);
			VernacularBytecode key = AddressForExpression(index->key);
			VernacularBytecode value = AddressForExpression(assign->right);

			// If the assignment has an operator:
			VernacularBytecode::Opcode opcode = GetAssignmentOperator(assign->op.str());
			if (opcode != VernacularBytecode::OP_NULL)
			{
				VernacularBytecode temp = locals.AddVariable();
				// Read current state into temp
				EmitOpcode(VernacularBytecode::OP_DOTGET, assign->token);
				Emit(object);
				Emit(key);
				Emit(temp);
				// Do operator(temp, value) -> value
				EmitOpcode(opcode, assign->token);
				Emit(temp);
				Emit(value);
				Emit(value);
			}

			EmitOpcode(VernacularBytecode::OP_DOTSET, assign->token);
			Emit(object);
			Emit(key);
			Emit(value);
		}
		else
		{
			throw VernacularCompileException(assign->op.line, assign->op.column, "Tried to assign to a non-assignable.");
		}
		DBGEN("End")
		return;
	}

	throw VernacularCompileException(expression->token.line, expression->token.column, "(INTERNAL ERROR) Unhandled non-evaluating expression encountered.");
}

// This turns out the simplest way to evaluate expressions, without handling
// all the different cases of whether an address was available.
// Note that we don't have to care too much about allocating extra temporary
// variables as they'll always be flattened down.
// We just have to try to avoid literal copies.

void VernacularBytecodeGenerator::CompileEvaluatingExpression(VernacularExpression *expression, VernacularBytecode targetAddress, bool reuseTarget)
{
	DBGST("VernacularBytecodeGenerator::CompileEvaluatingExpression > " << targetAddress.ToString())

	VernacularBytecode tempAddress = targetAddress;

	// reuseTarget is true if we're allowed to reuse the target address as an
	// intermediate temporary address. This is not the case for return statements
	// for reasons I forget.
	if (!reuseTarget)
	{
		tempAddress = locals.AddVariable();
	}

	if (expression->Is<VernacularFunctionCall>())
	{
		int size = CompileFunctionCall(expression->As<VernacularFunctionCall>());
		EmitOpcode(VernacularBytecode::OP_CPY, expression->token);
		Emit(VernacularBytecode(VernacularBytecode::T_UNMAPPED_FUNCTION_PARAMETER, size));
		Emit(targetAddress);
	}
	else if (expression->Is<VernacularIndexAccess>())
	{
		VernacularIndexAccess *index = expression->As<VernacularIndexAccess>();
		ASSERTX(index->right != nullptr);
		ASSERTX(index->key != nullptr);
		VernacularBytecode object = AddressForExpression(index->right, tempAddress);
		if (object.Equals(tempAddress))
		{
			tempAddress.Set(locals.AddVariable());
		}
		VernacularBytecode key = AddressForExpression(index->key, tempAddress);
		EmitOpcode(VernacularBytecode::OP_DOTGET, index->token);
		Emit(object);
		Emit(key);
		Emit(targetAddress);
	}
	else if (expression->Is<VernacularUnaryOperator>())
	{
		VernacularUnaryOperator *unary = expression->As<VernacularUnaryOperator>();
		ASSERTX(unary->right != nullptr);
		if (unary->IsNew())
		{
			ASSERTX(unary->right->Is<VernacularFunctionCall>());
			EmitOpcode(VernacularBytecode::OP_NEWOBJ, unary->token);
			Emit(tempAddress);
			EmitOpcode(VernacularBytecode::OP_PUSHTHIS, unary->token);
			Emit(tempAddress);
			CompileEvaluatingFunctionCall(unary->right->As<VernacularFunctionCall>(), true);
			EmitOpcode(VernacularBytecode::OP_POPTHIS, unary->token);
			if (!tempAddress.Equals(targetAddress))
			{
				EmitOpcode(VernacularBytecode::OP_DOTSET, unary->token);
				Emit(tempAddress);
				Emit(targetAddress);
			}
		}
		else
		{
			VernacularBytecode source = AddressForExpression(unary->right, tempAddress);
			EmitOpcode(OpcodeForSymbol(unary->op.str(), &gUnaryOpcodes), unary->token);
			Emit(source);
			Emit(targetAddress);
		}
	}
	else if (expression->Is<VernacularBinaryOperator>())
	{
		VernacularBinaryOperator *binary = expression->As<VernacularBinaryOperator>();
		ASSERTX(binary->left != nullptr);
		ASSERTX(binary->right != nullptr);
		if (binary->IsDot())
		{
			ASSERTX(binary->right->Is<VernacularIdentifier>());
			VernacularBytecode leftAddress = AddressForExpression(binary->left, tempAddress);
			EmitOpcode(VernacularBytecode::OP_DOTGET, binary->token);
			Emit(leftAddress);
			Emit(MakeLiteral(binary->right->As<VernacularIdentifier>()));
			Emit(targetAddress);
		}
		else
		{
			VernacularBytecode leftAddress, rightAddress;
			leftAddress = AddressForExpression(binary->left, tempAddress);
			if (leftAddress.Equals(tempAddress))
			{
				// Sometimes we don't need the temp address.
				// It will get discarded later as having no usage.
				DBG("Old temp: " << tempAddress.ToString());
				tempAddress = locals.AddVariable();
				DBG("New temp: " << tempAddress.ToString());
			}
			rightAddress = AddressForExpression(binary->right, tempAddress);
			EmitOpcode(OpcodeForSymbol(binary->op.str(), &gBinaryOpcodes), binary->token);
			Emit(leftAddress);
			Emit(rightAddress);
			Emit(targetAddress);
		}
	}
	else if (expression->Is<VernacularObjectLiteral>())
	{
		VernacularObjectLiteral *oblit = expression->As<VernacularObjectLiteral>();
		CompileObjectLiteral(oblit, targetAddress, tempAddress);
	}
	else if (expression->Is<VernacularBooleanLiteral>())
	{
        if (expression->As<VernacularBooleanLiteral>()->Value())
        {
			EmitOpcode(VernacularBytecode::OP_TRUE, expression->token);
			Emit(targetAddress);
        }
        else
        {
			EmitOpcode(VernacularBytecode::OP_FALSE, expression->token);
			Emit(targetAddress);
        }
	}
	else if (expression->Is<VernacularLiteral>())
	{
		// TODO: Because we have a target address here, for boolean null, true
		// and false we can instead emit OP_NULLIFY, OP_TRUE, or OP_FALSE
		// followed by the target address.

		EmitOpcode(VernacularBytecode::OP_CPY, expression->token);
		Emit(MakeLiteral(expression->As<VernacularLiteral>()));
		Emit(targetAddress);
	}
	else if (expression->Is<VernacularIdentifier>())
	{
		EmitOpcode(VernacularBytecode::OP_CPY, expression->token);
		Emit(AddressForIdentifier(expression->As<VernacularIdentifier>()));
		Emit(targetAddress);
	}
	else
	{
		throw VernacularCompileException(expression->token.line, expression->token.column, "(INTERNAL ERROR) Unhandled evaluating expression encountered.");
	}

	DBGEN("End")
}

// Either returns the address of a literal or identifier,
// or evaluates into the one specified.
VernacularBytecode VernacularBytecodeGenerator::AddressForExpression(VernacularExpression *expression, VernacularBytecode targetAddress)
{
	DBGST("VernacularBytecodeGenerator::AddressForExpression(VernacularExpression, VernacularBytecode)")

	VernacularBytecode address;

	if (expression->Is<VernacularNullLiteral>())
	{
		// TODO: When we're allowed to return the target address, we should
		// return reserved globals.
		CompileNullLiteral(expression->As<VernacularNullLiteral>(), targetAddress);
		address = targetAddress;
	}
	else if (expression->Is<VernacularBooleanLiteral>())
	{
		CompileBooleanLiteral(expression->As<VernacularBooleanLiteral>(), targetAddress);
		address = targetAddress;
	}
	else if (expression->Is<VernacularLiteral>() && !expression->Is<VernacularObjectLiteral>())
	{
		address = MakeLiteral(expression->As<VernacularLiteral>());
	}
	else if (expression->Is<VernacularIdentifier>())
	{
		address = AddressForIdentifier(expression->As<VernacularIdentifier>());
	}
	else if (expression->Is<VernacularFunctionCall>())
	{
		address = CompileEvaluatingFunctionCall(expression->As<VernacularFunctionCall>());
	}
	else
	{
		CompileEvaluatingExpression(expression, targetAddress);
		address = targetAddress;
	}

	DBGEN("End")
	return address;
}

// Either returns the address of a literal or identifier,
// or allocates a new address and evaluates into that.
VernacularBytecode VernacularBytecodeGenerator::AddressForExpression(VernacularExpression *expression)
{
	DBGST("VernacularBytecodeGenerator::AddressForExpression(VernacularExpression)")

	VernacularBytecode address;

	if (expression->Is<VernacularLiteral>() && !expression->Is<VernacularObjectLiteral>())
	{
		address = MakeLiteral(expression->As<VernacularLiteral>());
	}
	else if (expression->Is<VernacularIdentifier>())
	{
		address = AddressForIdentifier(expression->As<VernacularIdentifier>());
	}
	else if (expression->Is<VernacularFunctionCall>())
	{
		address = CompileEvaluatingFunctionCall(expression->As<VernacularFunctionCall>());
	}
	else
	{
		VernacularBytecode targetAddress = locals.AddVariable();
		CompileEvaluatingExpression(expression, targetAddress);
		address = targetAddress;
	}

	DBGEN("End")
	return address;
}

VernacularBytecode VernacularBytecodeGenerator::CompileEvaluatingFunctionCall(VernacularFunctionCall *funccall, bool isNew)
{
	DBGST("VernacularBytecodeGenerator::CompileEvaluatingFunctionCall")

	int size = CompileFunctionCall(funccall, isNew);
	VernacularBytecode funcAddress = VernacularBytecode(VernacularBytecode::T_UNMAPPED_FUNCTION_PARAMETER, size);
	VernacularBytecode targetAddress = locals.AddVariable();

	EmitOpcode(VernacularBytecode::OP_CPY, funccall->token);
	Emit(funcAddress);
	Emit(targetAddress);

	DBGEN("End")

	return targetAddress;
}

// Returns the number of parameters.
// This is also the address of the return parameter.
int VernacularBytecodeGenerator::CompileFunctionCall(VernacularFunctionCall *funccall, bool isNew)
{
	DBGST("VernacularBytecodeGenerator::CompileFunctionCall")

	// Note: Index access also calls a method because it adds extra functionality,
	// i.e. that of being able to call a variable method.
	// We still can call a function that's stored in an object without it being
	// a method call:
	//   var func = object.method;
	//   func();
	// Which will compile to the same bytecode as index access.

	// TODO: New function call method.
	// We specify the address of the first parameter, but also the number of
	// parameters we're actually passing, because empty parameters are required
	// to be null.

	VernacularBytecode funcobj;
	VernacularBytecode object;
	bool isMethod = false;

	int prevFunctionParameters = numFunctionParameters;
	VernacularBytecode baseAddress(VernacularBytecode::T_UNMAPPED_FUNCTION_PARAMETER, numFunctionParameters);

	if (funccall->right->Is<VernacularIdentifier>())
	{
		funcobj.Set(AddressForIdentifier(funccall->right->As<VernacularIdentifier>()));
	}
	else if (funccall->right->Is<VernacularBinaryOperator>() && funccall->right->As<VernacularBinaryOperator>()->IsDot() && !isNew)
	{
		// Dot operator makes this function call a method call.
		isMethod = true;
		VernacularBinaryOperator *binary = funccall->right->As<VernacularBinaryOperator>();
		ASSERTX(binary->left != nullptr);
		ASSERTX(binary->right != nullptr);
		ASSERTX(binary->right->Is<VernacularIdentifier>());
		object = AddressForExpression(binary->left);
		funcobj.Set(locals.AddVariable());
		EmitOpcode(VernacularBytecode::OP_DOTGET, binary->token);
		Emit(object);
		Emit(MakeLiteral(binary->right->As<VernacularIdentifier>()));
		Emit(funcobj);
	}
	else if (funccall->right->Is<VernacularIndexAccess>() && !isNew)
	{
		// Index access makes this function call a method call.
		isMethod = true;
		VernacularIndexAccess *index = funccall->right->As<VernacularIndexAccess>();
		ASSERTX(index->right != nullptr);
		ASSERTX(index->key != nullptr);
		object = AddressForExpression(index->right);
		funcobj.Set(locals.AddVariable());
		EmitOpcode(VernacularBytecode::OP_DOTGET, index->token);
		Emit(object);
		Emit(AddressForExpression(index->key, funcobj));
		Emit(funcobj);
	}
	else
	{
		funcobj.Set(locals.AddVariable());
		CompileEvaluatingExpression(funccall->right, funcobj);
	}

	if (funccall->parameters.size())
	{
		for (int i = 0; i < funccall->parameters.size(); i += 1)
		{
			// Increment this first, so that a nested function call starts
			// from the next address.
			numFunctionParameters += 1;
			VernacularBytecode param(VernacularBytecode::T_UNMAPPED_FUNCTION_PARAMETER, prevFunctionParameters + i);
			CompileEvaluatingExpression(funccall->parameters[i], param);
		}
	}
	else
	{
		// Note: a function call with zero parameters has to pass a single null
		// parameter so that the return value can be passed. Therefore a function
		// that accepts zero parameters must leave space for a single null value.
		EmitOpcode(VernacularBytecode::OP_NULLIFY, funccall->token);
		Emit(VernacularBytecode(VernacularBytecode::T_UNMAPPED_FUNCTION_PARAMETER, 1));
	}
	int size = funccall->parameters.size();

	if (isMethod)
	{
		EmitOpcode(VernacularBytecode::OP_PUSHTHIS, funccall->token);
		Emit(object);
	}

	EmitOpcode(VernacularBytecode::OP_CALL, funccall->token);
	Emit(funcobj);
	Emit(baseAddress);
	Emit(VernacularBytecode(VernacularBytecode::T_INTEGER, size));

	if (isMethod)
	{
		EmitOpcode(VernacularBytecode::OP_POPTHIS, funccall->token);
	}

	numFunctionParameters = prevFunctionParameters;

	DBGEN("End")

	return baseAddress.GetValue();
}

//-----------------------------------------------------------------------------
// Literals
//-----------------------------------------------------------------------------

VernacularBytecode VernacularBytecodeGenerator::MakeLiteral(int i)
{
	DBGST("VernacularBytecodeGenerator::MakeLiteral(int)")
	DBGEN("End")
	return VernacularBytecode(VernacularBytecode::T_FUNCTION, function->literals.Create(VernacularValue(i)));
}

// Note: We're not allowed to emit code here.
VernacularBytecode VernacularBytecodeGenerator::MakeLiteral(VernacularLiteral *vlit)
{
	DBGST("VernacularBytecodeGenerator::MakeLiteral(VernacularLiteral)")

	VernacularValue value;

	if (vlit->Is<VernacularNullLiteral>())
	{
		DBGEN("End")
		return VernacularBytecode(VernacularBytecode::T_GLOBAL, 1);
	}
	else if (vlit->Is<VernacularBooleanLiteral>())
	{
		if (vlit->As<VernacularBooleanLiteral>()->Value())
		{
			DBGEN("End")
			return VernacularBytecode(VernacularBytecode::T_GLOBAL, 2);
		}
		else
		{
			DBGEN("End")
			return VernacularBytecode(VernacularBytecode::T_GLOBAL, 3);
		}
	}

	value = MakeLiteralValue(vlit);

	DBGEN("End")
	return VernacularBytecode(VernacularBytecode::T_FUNCTION, function->literals.Create(value));
}

VernacularValue VernacularBytecodeGenerator::MakeLiteralValue(VernacularLiteral *vlit)
{
	DBGST("VernacularBytecodeGenerator::MakeLiteralValue(VernacularLiteral)")

	VernacularValue value;

	if (vlit->Is<VernacularNullLiteral>())
	{
		// it defaulted to null
	}
	else if (vlit->Is<VernacularBooleanLiteral>())
	{
		if (vlit->As<VernacularBooleanLiteral>()->Value())
		{
			value.SetTrue();
		}
		else
		{
			value.SetFalse();
		}
	}
	else if (vlit->Is<VernacularNumberLiteral>())
	{
		VernacularNumberLiteral *vnl = vlit->As<VernacularNumberLiteral>();
		value.SetNumeric(vnl->value.str());
	}
	else if (vlit->Is<VernacularStringLiteral>())
	{
		VernacularStringLiteral *vsl = vlit->As<VernacularStringLiteral>();
		std::string sval = vsl->value.str();

		if (sval.length() >= 2 && sval[0] == '"' && sval[sval.length() - 1] == '"')
		{
			sval = sval.substr(1, sval.length() - 2);
		}

		std::stringstream sb;
		bool escape = false;
		for (int i = 0; i < sval.length(); i += 1)
		{
			if (sval[i] == '\\')
			{
				escape = true;
			}
			else if (escape == true)
			{
				if (sval[i] == 'n')
				{
					sb << '\n';
				}
				else if (sval[i] == 't')
				{
					sb << '\t';
				}
				else if (sval[i] == '"')
				{
					sb << '"';
				}
				else if (sval[i] == '\\')
				{
					sb << '\\';
				}
				else
				{
					sb << sval[i];
				}
				escape = false;
			}
			else
			{
				sb << sval[i];
			}
		}
		value = GamecodeValue(stringFactory->Allocate(sb.str()));
	}
	else if (vlit->Is<VernacularFunctionLiteral>())
	{
		VernacularFunctionLiteral *vfl = vlit->As<VernacularFunctionLiteral>();
		VernacularBytecodeGenerator generator(functionFactory, objectFactory, stringFactory, log, dumpLevel);
		generator.filename = filename;
		value = GamecodeValue(generator.CompileFunction(vfl));
	}

	DBGEN("End")

	return value;
}

VernacularBytecode VernacularBytecodeGenerator::MakeLiteral(VernacularIdentifier *vid)
{
	DBGST("VernacularBytecodeGenerator::MakeLiteral(VernacularIdentifier)")

	VernacularValue value = MakeLiteralValue(vid);
	VernacularBytecode addr = VernacularBytecode(VernacularBytecode::T_FUNCTION, function->literals.Create(value));
	DBG(addr.ToString())
	DBGEN("End")
	return addr;
}

VernacularValue VernacularBytecodeGenerator::MakeLiteralValue(VernacularIdentifier *vid)
{
	DBGST("VernacularBytecodeGenerator::MakeLiteralValue(VernacularIdentifier)")

	// It is a compilation error if the identifier is 'this', 'readonly' or 'private'.
	// It means the code is something like:
	//   object = {};
	//   object.this := 12;
	// Which is invalid syntax.
	// Note that for the equivalent function (and identical bytecode), we'd do
	// something like:
	//   object := {};
	//   object["this"] := 12;

	std::string name = vid->name.str();
	DBG("name = " << name)
	if (!name.compare("this") || !name.compare("readonly") || !name.compare("private"))
	{
		throw VernacularCompileException(vid->name.line, vid->name.column, "The identifier " + name + " cannot be used in this context.");
	}

	VernacularValue value(GamecodeValue(stringFactory->Allocate(name)));

	DBGEN("End")

	return value;
}

void VernacularBytecodeGenerator::CompileBooleanLiteral(VernacularBooleanLiteral *blit, VernacularBytecode targetAddress)
{
	DBGST("VernacularBytecodeGenerator::CompileBooleanLiteral")

	if (blit->Value())
	{
		EmitOpcode(VernacularBytecode::OP_TRUE, blit->token);
		Emit(targetAddress);
	}
	else
	{
		EmitOpcode(VernacularBytecode::OP_FALSE, blit->token);
		Emit(targetAddress);
	}

	DBGEN("End")
}

void VernacularBytecodeGenerator::CompileNullLiteral(VernacularNullLiteral *nlit, VernacularBytecode targetAddress)
{
	DBGST("VernacularBytecodeGenerator::CompileNullLiteral")

	EmitOpcode(VernacularBytecode::OP_NULLIFY, nlit->token);
	Emit(targetAddress);

	DBGEN("End")
}

void VernacularBytecodeGenerator::CompileObjectLiteral(VernacularObjectLiteral *oblit, VernacularBytecode targetAddress, VernacularBytecode tempAddress)
{
	DBGST("VernacularBytecodeGenerator::CompileObjectLiteral")

#if 1
	if (oblit->IsSimple())
	{
		VernacularObject *object = objectFactory->Allocate();

		for (int i = 0; i < oblit->values.size(); i += 1)
		{
			if (oblit->values[i]->Is<VernacularBinaryOperator>())
			{
				VernacularValue key, value;

				VernacularAssignmentOperator *subexp = oblit->values[i]->As<VernacularAssignmentOperator>();
				ASSERTX(subexp != nullptr);
				ASSERTX(subexp->left != nullptr);
				ASSERTX(subexp->right != nullptr);

				VernacularIdentifier *keyid = subexp->left->As<VernacularIdentifier>();
				ASSERTX(keyid != nullptr);
				key = MakeLiteralValue(keyid);

				VernacularLiteral *valueexp = subexp->right->As<VernacularLiteral>();
				ASSERTX(valueexp != nullptr);
				value = MakeLiteralValue(valueexp);

				object->PushBack(key, value);
			}
			else
			{
				VernacularValue value;

				VernacularLiteral *valueexp = oblit->values[i]->As<VernacularLiteral>();
				ASSERTX(valueexp != nullptr);

				value = MakeLiteralValue(valueexp);

				object->PushBack(value);
			}
		}

		object->BecomeLiteral();

		VernacularValue objectValue(object);
		VernacularBytecode literalAddress = VernacularBytecode(VernacularBytecode::T_FUNCTION, function->literals.Create(objectValue));

		EmitOpcode(VernacularBytecode::OP_COPY_ON_WRITE, oblit->token);
		Emit(literalAddress);
		Emit(targetAddress);

		DBGEN("End")

		return;
	}
#endif

	EmitOpcode(VernacularBytecode::OP_NEWOBJ, oblit->token);
	Emit(tempAddress);
	VernacularBytecode value;
	if (oblit->values.size() > 0)
	{
		value.Set(locals.AddVariable());
	}
	int nextIndex = 0;
	for (int i = 0; i < oblit->values.size(); i += 1)
	{
		if (oblit->values[i]->Is<VernacularBinaryOperator>())
		{
			VernacularAssignmentOperator *subexp = oblit->values[i]->As<VernacularAssignmentOperator>();
			ASSERTX(subexp != nullptr);
			ASSERTX(subexp->left != nullptr);
			ASSERTX(subexp->right != nullptr);
			VernacularIdentifier *keyid = subexp->left->As<VernacularIdentifier>();
			ASSERTX(keyid != nullptr);
			VernacularBytecode key = MakeLiteral(keyid);
			// If the key is numeric, then the next auto-generated key is
			// one higher.
			VernacularValue &literal = GetLiteral(key);
			if (literal.IsNumber())
			{
				int index = literal.GetInt();
				if (nextIndex <= index)
				{
					nextIndex = index + 1;
				}
			}
			CompileEvaluatingExpression(subexp->right, value);
			if (subexp->metaType == VernacularAssignmentOperator::NONE)
			{
				EmitOpcode(VernacularBytecode::OP_DOTSET, subexp->token);
			}
			else
			{
				EmitOpcode(VernacularBytecode::OP_DOTSET_META, subexp->token);
			}
			Emit(tempAddress);
			Emit(key);
			Emit(value);
			switch (subexp->metaType)
			{
			case VernacularAssignmentOperator::RESET:
				EmitInteger(0);
				break;
			case VernacularAssignmentOperator::PRIVATE:
				EmitInteger(1);
				break;
			case VernacularAssignmentOperator::READONLY:
				EmitInteger(2);
				break;
			default:
				break;
			}
		}
		else
		{
			VernacularExpression *subexp = oblit->values[i];
			ASSERTX(subexp->IsValue());
			VernacularBytecode key = MakeLiteral(nextIndex);
			nextIndex += 1;
			CompileEvaluatingExpression(subexp, value);
			EmitOpcode(VernacularBytecode::OP_DOTSET, subexp->token);
			Emit(tempAddress);
			Emit(key);
			Emit(value);
		}
	}
	if (!tempAddress.Equals(targetAddress))
	{
		EmitOpcode(VernacularBytecode::OP_DOTSET, oblit->token);
		Emit(tempAddress);
		Emit(targetAddress);
	}

	DBGEN("End")
}

//-----------------------------------------------------------------------------
// Helper stuff
//-----------------------------------------------------------------------------

VernacularBytecode VernacularBytecodeGenerator::AddressForIdentifier(std::string identifier)
{
	DBGST("VernacularBytecodeGenerator::AddressForIdentifier(" << identifier << ")")

	if (!identifier.compare("this"))
	{
		VernacularBytecode addr = VernacularBytecode(VernacularBytecode::T_GLOBAL, 0);
		DBG(addr.ToString())
		DBGEN("End");
		return addr;
	}

	VernacularBytecode local = locals.FindVariable(identifier);
	if (local.GetType() == VernacularBytecode::T_UNMAPPED_LOCAL)
	{
		DBG(local.ToString())
		DBGEN("End")
		return local;
	}
	auto it = function->globalReferences.find(identifier);
	if (it != function->globalReferences.end())
	{
		DBG(it->second.ToString())
		DBGEN("End")
		return it->second;
	}
	VernacularBytecode uglobal(VernacularBytecode::T_UNMAPPED_GLOBAL, function->globalReferences.size());
	function->globalReferences.insert(std::make_pair(identifier, uglobal));
	DBG(uglobal.ToString())
	DBGEN("End")
	return uglobal;
}

void VernacularBytecodeGenerator::RemapBreaks(VernacularBytecode loopStart, VernacularBytecode afterLoop)
{
	// updates break statement placeholder addresses to point to the address after the loop.
	for (auto it = breakList.begin(); it != breakList.end(); ++it)
	{
		if ((*it).loopStart.Equals(loopStart))
		{
			function->bytecode[(*it).jumpAddress.GetValue()].Set(afterLoop);
			it = breakList.erase(it);
		}
	}
}

void VernacularBytecodeGenerator::AdjustLoopLocals(VernacularBytecode loopStart, VernacularBytecode loopEnd)
{
	// If a variable used in a loop is first used before the start of the loop,
	// then update its last use to be after the end of the loop.
	for (int pos = loopStart.GetValue(); pos <= loopEnd.GetValue(); pos += 1)
	{
		if (function->bytecode[pos].GetType() == VernacularBytecode::T_UNMAPPED_LOCAL)
		{
			locals.AddVariableLoopReference(VernacularBytecode(VernacularBytecode::T_INSTRUCTION_ADDRESS, pos), loopStart, loopEnd);
		}
	}
	// Note: Currently it's more efficient every time you need a variable to
	// create a new one. It's also semantically clearer, doing so should be
	// encouraged and variable reuse can be left inefficient.
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
