/**
\page vernacular_script Vernacular Scripting Language

Vernacular is a scripting language created partly as an exercise, and partly as
a tidy 64 bit compatible replacement for GameMonkey.

\section introduction Introduction

Back when I was just beginning to program computers, modding games helped me
greatly strengthen my programming skills and broaden my experience. Today, the
games I enjoy most are ones with strong modding communities. User content means
your game lasts longer and can be tailored to suit the way you like to play.

Vernacular is intended to be an ideal modding language: simple enough to learn
all the details of its behaviour quickly, while providing enough features to
let its users implement any kind of game behaviour they can imagine.

A more complete list of considerations is as follows:
- The core features of the language should be limited in number, but not so
  limited that complex usage needs to be understood in order to implement
  basic things.
- The language should behave in a way which is easy to understand and predict.
- The programmer should be alerted to errors that have been made as soon as
  possible.
- Error descriptions should be as clear, precise and accurate as possible.
- Nonsensical, null or bug-prone code should be a syntax error.
  (For example, assignment in an expression which is expected to evaluate)

\section files Files

A Vernacular script file is composed of zero or more statements.
A statement in Vernacular is an assignment, a function call or flow control.
Note that expressions themselves are not considered statements. Vernacular
distinguishes between statements, which perform actions, and expressions, which
evaluate to a value.

\section scope Scope

A variable's scope describes the parts of code in which it can be accessed
using its name.

Outside of any function, simply assigning to an identifier will create a global
variable. This variable can then be accessed from within any function. The
global variable doesn't need to exist when the function is created, but it does
need to when the function is run.

Inside a function, a variable must be declared using the 'var' keyword, and
it can be accessed until the end of the block in which it was declared.

Variables declared in a for or foreach loop can be accessed until the end of
the loop's block.

Function parameters can be accessed within the whole of the function.

\code
x := 1; // global variable

// This function is itself a global variable.
// Also note that the
myFunc := function(a, b) {
	var c := 2;
	if (a = b) {
		// locals a, b, c and global x are accessible here.
		var d := c + a + x;
	} // d is no longer accessible after the end of the block
	// a, b c and x are still accessible

	for (var i := 0; i < 3; i := i + 1) {
		// i is accessible in the parenthesized section of this loop
		b := b + i;
	} // i is no longer accessible

	var table := { 1, 2, 3 };
	foreach (value v in table) {
		// v is accessible here
	} // v is no longer accessible
};
\endcode


\section assignment Assignment

An assignment takes the following form:
\code
<identifier> := <expression>;
\endcode

There are other forms for assignment which will be explained later as more
of the language structures are explained:
\code
<table_expression>.<identifier> := <expression>;
<table_expression>[<expression>] := <expression>;
\endcode
In the above, <table_expression> is an expression that must evaluate to a table.
At its simplest, it can be the identifier of a varaible that contains a table.

An identifier is a string starting with an ascii letter or an underscore, which
is followed by zero or more ascii letters, underscores or digits.
\code
// Valid identifiers
david
gr8_m8
_cheesecake_2000
_12

// Invalid identifiers
1abc_def // starts with a digit
large-hat_please // contains an invalid character '-'
\endcode

As an example, the following code would search for the variable named 'x' and
assign it the numerical value 12. In global scope, it would create the variable
if it did not exist.
\code
x := 12;
\endcode

In function scope, the following code would create a local variable named 'x'
with the value 13, then reassign it the value 14. The global variable named 'x'
will not be affected.
\code
var x := 13;
x := 14;
\endcode

There are other assignment operators, which perform an operation on the current
value rather than simply overwriting it. The assignment version of an operator
is always the same as the evaluating operator preceded by : and followed by =
\code
var num := 1;
num :+= 3; // num is 4 - equivalent to num := num + 3
num :/= 2; // num is 2 - equvalent to num := num / 2
num :*= 3; // num is 6 - equivalent to num := num * 3
num :**= 2; // num is 36 - equivalent to num := num ** 2
num :%= 10; // num is 6 - equivalent to num := num % 10

var str := "Hello";
str :$= " World :)"; // str is "Hello World :)"

var bits := 0b000101;
bits :<<= 2; // bits is 0b010100
bits :>>= 1; // bits is 0b001010
bits :|= 0b000100; // bits is 0b001110
bits :&= 0b000110; // bits is 0b000010
\endcode


\section types_and_literals Types and Literals

There are several types of value which can be assigned to a varaible.

\subsection null Null

The null type has only one possible value: null. Although it is technically a
type and a value, it is to be treated semantically as neither. That is, it
should never be used to represent information, only the absence of it.
\code
x := null;
\endcode

\subsection boolean Boolean

A boolean value has only the values 'true' and 'false'.
\code
x := true;
x := false;
\endcode

The logical and comparison operators will always evaluate to boolean.
The operands will automatically be converted to boolean before the operation.
\code
x := true || false; // true
x := true && false; // false
x := (3 = 4); // false
x := (3 != 4); // true
x := 3 < 4; // true
x := 3 > 4; // false
x := 3 >= 4; // true
x := 3 <= 4; // false
x := !false; // true
\endcode

The core library provides a method to cast other types to boolean using the
same method as automatic casting.
\code
x := toBool(null); // false
x := toBool(0); // false
x := toBool(1); // true
x := toBool(-1); // true
x := toBool(5); // true
x := toBool(-3.5); // true
x := toBool("any string"); // true
x := toBool(""); // true
x := toBool({}); // true
x := toBool(function(){}); // true
\endcode

\subsection number Number

A number value can represent whole numbers or fractions.
\code
// decimal
x := 12;
x := 3.14;
x := -1.6;

// hexadecimal
x := 0xffff;
x := -0xe;
x := 0xDEADBEEF;

// binary
x := 0b1010;
x := -0b11110011;
\endcode

The mathematical and bitwise operators act upon numbers.
The operands will be automatically cast to numbers before the operation is performed.
\code
x := 1 + 2; // 3
x := 1 - 2; // -1
x := 3 * 4; // 12
x := 12 / 3; // 4
x := 13 % 3; // 1 (modulo)
x := 0x823 & 0xff; // 0x23
x := 0b0011 ^ 0b0101; // 0b0110; (xor)
x := -(3 + 4); // -7 (the - operator is a unary sign negation as well as binary subtraction)
x := 10 ** 3; // 1000 (power operator)
\endcode

The core library provides a method to cast to number.
\code
x := toNumber(null); // 0
x := toNumber(false); // 0
x := toNumber(true); // 1
x := toNumber("1234"); // 1234 - any valid literal is converted, all other strings become zero
x := toNumber("34.56"); // 34.56
x := toNumber("0xff"); // 255
x := toNumber("0b1010"); // 10
x := toNumber("vernacular"); // 0
x := toNumber({}); // 0
x := toNumber(function(){}); // 0
\endcode


\subsection string String

The string type contains a string of characters. It is enclosed in double quotes
which are not part of its value. It can contain a double quote by prefixing it
with the backslash character.
\code
x := "Hello World";
x := "The above string says \"Hello World\"";
\endcode

Strings can be concatenated with the $ operator. Other types will be automatically
converted to string before the operation.
\code
x := "Hello";
y := "World";
z := x $ " " $ y; // "Hello World"
\endcode

Strings in Vernacular are immutable, meaning that applying an operator to a
string creates a new string while leaving all variables that were assigned the
old value intact. This also means that it doesn't matter if strings behind the
scenes are passed by reference or value.
\code
x := "String";
y := x;
y := y $ " Concatenation";
// Now y is equal to "String Concatenation" but x is still equal to "String".
\endcode

The core library provides a method to convert other types to string.
This function converts strings in the same way as the concatenation operator.
\code
x := toString(null); // "null"
x := toString(false); // "false"
x := toString(true); // "true"
x := toString(1234); // "1234" - numbers always decimal expansion
x := toString(-12.34); // "-12.34"
\endcode

Converting a table to string results in "table:" followed by an implementation-defined
string of characters. The whole string is guaranteed to uniquely identify that table object.
The same is true for function and user objects.
\code
x := toString({}); // "table:82378"
x := toString(function(){}); // "function:763225"
x := toString(anEngineObject); // "user:982347"
\endcode

The core library also provides some other methods to operate upon strings.
Vernacular treats strings as arrays of unicode characters rather than as bytes.

_stringLength_\n
Returns the length of a string.
\code
length := stringLength("123123123"); // 9
\endcode

_stringPart_\n
Returns part of a string.
The second parameter is offset and the third is the length. If either parameter
is negative it is treated as being that many characters before the end.
\code
part := stringPart("abcdef", 1, 3); // "bcd"
\endcode

_stringSplit_\n
Splits a string by a separator. The result is a table.
\code
table := stringSplit("the,cat,sat,on,the,mat", ","); // table is now { "the", "cat", "sat", "on", "the", "mat" }
\endcode
Splitting also accepts a third paramter which limits the number of parts the string will be split into.
\code
table := stringSplit("the,cat,sat,on,the,mat", ",", 2); // table is now { "the", "cat,sat,on,the,mat" }
\endcode

_stringJoin_\n
Joins a string with a delimiter.
\code
parts := { "the", "cat", "sat", "on", "the", "mat" };
newstr := stringJoin(parts, "."); // "the.cat.sat.on.the.mat"
\endcode

_stringFromUnicode_\n
Creates a string from a single unicode, or array of unicode values.
\code
str := stringFromUnicode(33); // "!"
str := stringFromUnicode({ 72, 101, 108, 108, 111 }); // "Hello"
\endcode

_stringToUnicode_\n
Splits a string into unicode.
The second parameter is the offset, and an optional third parameter is the length,
which will then output a table rather than single code.
Offset and length work as in stringPart.
\code
code := stringToUnicode("A", 0); // 65
codes := stringToUnicode("A", 0, 1); // { 65 }
codes := stringToUnicode("Oh, hello", 4, 5); // { 104, 101, 108, 108, 111 }
\endcode


\subsection table Table

The table type (also referred to as object type) is a combined hashmap and
linked list. It is a set of keys which map to values, and the order of the
key-value pairs is maintained, with newly added keys being appended to the end
of the list.

The table literal takes the form of a comma separated list of assignments to
single identifiers:
\code
{
	<identifier> := <expression>,
	<identifier> := <expression>
}
\endcode
Following are examples of valid table literals:
\code
myTable := {}; // empty table

myTable := {
	a := "hello",
	b := 12,
	c := {} // nested empty table
};

myTable := {
	// non-constant expressions are evaluated when the table is assigned
	// here a function is called and the result assigned to the key 'd'.
	d := uppercase("hello")
}
\endcode

The elements within a table which are string identifiers can then be accessed
using the dot operator, including setting new ones.

\code
myTable := { a := 12, b := 13 }
myTable.a := myTable.a + 1;
myTable.c := myTable.a + myTable.b;
// myTable.c is now 26
\endcode

It is possible to use any value type except for null as a table key using the
element access operator []

\code
myTable := {}
anotherTable := { x := 12 }
myTable[anotherTable] := true; // the key is a table, and the value is true
\endcode

When we use identifiers as table keys, we are actually using their string
representation:

\code
myTable := { fred := "barney" }
x := myTable["fred"]; // "barney"
\endcode

We can remove a key by setting its value to null.

\code
myTable := { fred := "barney" }
myTable["fred"] := null;
// myTable is now empty again, because its only key, 'fred', was removed.
\endcode

Trying to write an element that doesn't exist will create it, but trying to
read one that doesn't exist will return null. The null value is also handled as
a special case: when trying to perform dot-read on a null value, rather than
throwing an exception, null is returned. This allows us to check whether a
nested table key exists in a single line, rather than having to perform a check
at every level.

\code
myTable := {
	a := {
		b := {
			c := "Hi"
		}
	}
};
print(myTable.a.b.c); // Hi
print(myTable.d.e.f); // null
\endcode

The core library provides some useful methods for operating on tables.

_tablePushBack(table, value)_\n
_tablePushBack(table, key, value)_\n
Adds a key value pair to the end of the table. This is identical to using the [] operator.
If the key is omitted, an incrementing value is used, starting at 0.
If the table already contains numeric keys, then the next highest integer is used.

_tablePopBack(table)_\n
Removes the last value from the table and returns it.

_tableBack(table)_\n
Returns the key at the end of the table, without removing it.

_tablePushFront(table, value)_\n
_tablePushFront(table, key, value)_\n
Adds a key value pair to the start of the table. A subsequent foreach will iterate
this key before the rest.
If the key is omitted, an incrementing value is used, starting at 0.
If the table already contains numeric keys, then the next lowest integer is used.

_tablePopFront(table)_\n
Removes the first value from the table and returns it.

_tableFront(table)_\n
Returns the key at the start of the table, without removing it.

_tableFindValue(table, value)_\n
If the table has a key with the given value, then that key is returned.
Otherwise, the function returns null.

_tableGetKeys(table)_\n
Returns a new table containing as values the keys of the table passed as a
parameter. The new table's values are in the same order as the original table's
keys. The new table has incrementing integer keys starting from 0.

_tableReverse(table)_\n
Reverses the order of the passed table.

_tableSort(table, func)_\n
Sorts the passed table using the passed function, for example:
\code
myTable := { 6, 3, 2, 7, 1, 5, 9, 4, 0 }
tableSort(myTable, function(a, b) {
	if (a > b) {
		return 1;
	} else if (a < b) {
		return -1;
	}
	return 0;
});
\endcode

_tableReIndex(table)_\n
_tableReIndex(table, start)_\n
_tableReIndex(table, start, step)_\n
Maintaining order, changes the keys to start at the start value and increment
by step. The start and step parameters must be number type. The default start
is 0 and the default step is 1.

_tableCount(table)_\n
Returns the number of items in the table.

_tableShallowCopy(table)_\n
Returns a new table with the same keys and values as the one passed.
If the old table contains tables or functions, the new table will contain the
exact same tables and functions.


\subsection function Function

The only way to create a function in Vernacular is to write a function literal
and assign it to a variable.
\code
add := function(x, y) {
	var r := x + y;
	return r;
};
value := add(1, 2); // the number 3 will be assigned to value.
\endcode
Note that the paramters specified in the function literal become local, function
-scope variables the same as though they were declared with the var keyword.

Here is an example to help clarify variable scope:
\code
x := "global";

updateGlobal := function() {
	// updates the global x
	x := "global updated";
};

useLocal := function(val) {
	// creates the local x and sets it to val
	// the local is discarded when the function ends
	var x;
	x := val;
};

updateGlobal();
useLocal("local");
// the global variable x now contains the string "global updated"
\endcode

The 'var' keyword can be given a comma separated list of variables to initialize.
\code
myFunc := function(a, b) {
	var d := 1,
		e := 2;
};
\endcode

It has already been shown, but a function is called using the function call
operator:
\code
myFunc := function(str) {
	print("Hello " + str);
};
myFunc("World"); // prints "Hello World"
\endcode

A function may return another function and called immediately. This is done
by chaining the two function calls:

\code
outer := function() {
	return function(x, y) {
		return x + y;
	};
};
result := outer()(4, 2);
print(result); // prints 6
\endcode


\section flow_control Flow Control

\subsection conditons Conditions

Conditional branches use the 'if' and 'else' keywords.
\code
if (x := y) {
	match := true;
}

if (someTest()) {
	doSomething();
} else {
	doSomethingElse();
}

if (a = 1) {
	name := "Cat";
} else if (a = 2) {
	name := "Dog";
} else if (a = 3) {
	name := "Guinea Pig";
} else {
	name := "Rabbit";
}
\endcode

\subsection foreach Foreach

The 'foreach' keyword can be used to iterate the keys and/or values in a table.

We explicitly state which is the key and which is the value for clarity,
as opposed to just a comma separated list, or an 'and'.
The keywords 'key' and 'value' also act as variable declarations.
We don't use 'var' because the foreach syntax becomes too wordy, and any
redeclarations become obvious on compilation.

\code
animals := {
	cats := 12,
	dogs := 8
};

foreach (key k, value v in animals) {
	print(k + " = " + v);
}
\endcode

Either the key or the value may be omitted, but never both.
\code
foreach (key type in animals) {
	print("there are " + type);
}
// there are cats
// there are dogs
\endcode
\code
total := 0;
foreach (value count in animals) {
	total := total + count;
}
// total is now 20
\endcode

Note that foreach will iterate the state of the table as it was when the foreach
keyword was reached. Modifications within the loop will not affect what is being
iterated.

Items are iterated in the order to which their key was added to the table.
Removing a key by setting it to null and then re-adding it will place it at the
end of the table.

\subsection for For

The for loop is a general purpose loop. It takes the form:
\code
for (<statement>; <expression>; <statement>) {
}
\endcode

The first statement is run before the loop begins.
The expression is evaluated before every iteration of the loop, and if it
is false then the program jumps to the next statement after the end of the
for loop. The second statement is run after every iteration of the loop.

The typical example is as follows:
\code
// Print the numbers 0 to 9
for (var num := 0; num < 10; num := num + 1) {
	print(toString(num));
}
\endcode

The break statement can be used to break out of any loop.
\code
// This loop will break during the 10th iteration.
for (var num := 1; num < 15; num := num + 1) {
	if (num = 10) {
		break;
	}
}
\endcode

An integer can be specified after a break statement to break out of nested loops:
\code
for (var x := 1; x <= 99; x := x + 1) {
	for (var y := 1; y <= 99; y := y + 1) {
		if (coordMatch(x, y)) {
			foundCoord();
			break 2;
		}
	}
}
\endcode


\section classes Making Classes

There are a couple of ways to create classes in Vernacular:

A method can create and return an object.
\code
MyClass := function() {
	var instance := {};
	instance.aProperty := "Hello";
	instance.aMethod := function() {
		return this.aProperty;
	};
	return instance;
};
myInstance := MyClass(); // create an instance of the class
\endcode
Note the use of the 'this' keyword. When a function on a table is called via
one of the two ways:
\code
myInstance.aMethod();
myInstance["aMethod"]();
\endcode
Then the 'this' variable is set to the object which owned the method that was
called. Thus 'this.aProperty' is able to access the 'aProperty' key for the
table on which the method was called.

Both object.method() and object["method"]() count as method calls.
The second syntax is considered useful for calling variable methods.
To avoid calling a function in an object as a method, the following syntax
will work:
\code
var func = object.method;
func();
\endcode
This is as efficient as retrieving a function from an object can be.

The 'function-that-creates-an-object' way to create classes makes it easy to
implement a kind of inheritance:
\code
BaseClass := function() {
	var instance := {};
	instance.x := 0;
	instance.y := 0;
	instance.length := function() {
		return this.x*this.x + this.y*this.y;
	};
};
DerivedClass := function() {
	instance := BaseClass();
	instance.z := 0;
	instance.length := function() {
		return this.x*this.x + this.y*this.y + this.z*this.z;
	};
};
\endcode

\subsection classes_new Operator New

The second way to create a class is with the 'new' operator.
\code
MyNewClass := function() {
	this.a := 1;
	this.b := 2;
	this.c := function(x) {
		return (this.a + this.b) * x;
	};
};
myNewInstance = new MyNewClass();
\endcode

The 'new' operator creates an object and calls the following function on it
as a method. It evaluates to the newly created object. So:
\code
var thing := new Blah();
\endcode

The above code will create an object, run Blah() on it as a method, and then
assign the object to the variable 'thing'.
The 'new' operator is forbidden from being used where it would conflict with
a method call.
To store constructors in an object, we must use the following code:
\code
var constructor := object.method;
var result := new constructor();
\endcode

Use of the 'new' keyword is the prefered way to create classes, as it allows
creation of private and readonly properties via table metadata.
\code
var MyClass := function() {
	this.pubProp := "You can see this";
	this.privProp := @private "You can't see this";
	this.method := @readonly function() {
		return this.privProp $ " until now";
	};
};
var instance := new MyClass();
print("Items in table: " $ tableCount(instance));
foreach (key k, value v in instance) {
	print(k $ " := " $ v);
}
print("table.privProp: " $ instance.privProp);
print("method() returns: " $ instance.method());
\endcode

This will output:
\code
Items in table: 1
pubProp := You can see this
table.privProp: null
method() returns: You can't see this until now
\endcode

\@private and \@readonly are modifiers applied to the assignment operator,
and they apply metadata to the key which is assigned.

Private properties cannot be read from outside the object, i.e. when the
'this' identifier does not refer to the object which contains the property.
The key will show up in a foreach loop, but the value will always be null.

Readonly properties cannot be written from outside the object. This is useful
for allowing outside code to run methods on the object without being able to
change them accidentally. Readonly keys and values will be iterated in a
foreach loop.

The meta modifiers can be removed with the \@reset modifier.

Methods are available to read the meta information of table keys:

bool tableIsReadonly(table, key)

bool tableIsPrivate(table, key)


\section library Other Library Functions

Vernacular's core library provides a few other general purpose functions:
\code
getType(null); // "null"
getType(true); // "boolean"
getType(false); // "boolean"
getType(0); // "number"
getType(12); // "number"
getType("hello"); // "string"
getType({}); // "table"
getType(function(){}); // "function"
// Also engine objects will return "user"
\endcode

\code
getUserType(aUserObject);
// Returns the name with which the user object type was registered, or false if
// the given value is not a user object.
\endcode

\section precedence Operator Precedence

Precedence | Operator | Associativity | Description
-----------|----------|---------------|-------------
0          | .        | Left          | Dot operator, for accessing elements in tables by an identifier.
0          | []       | Left          | Bracket operator, for accessing elements in tables by a variable.
0          | ()       | Left          | Function call.
3          | new      | Right         | New operator, for constructing instances of classes.
3          | -        | Right         | Unary sign negation.
4          | !        | Right         | Logical negation.
4          | ~        | Right         | Bitwise negation.
4          | **       | Right         | Raising to a power.
5          | *        | Left          | Numerical multiplication.
5          | /        | Left          | Numerical division.
5          | %        | Left          | Modulo.
6          | +        | Left          | Addition.
6          | $        | Left          | String concatenation.
6          | -        | Left          | Binary numeric subtraction.
7          | <<       | Left          | Bitwise shift left.
7          | >>       | Left          | Bitwise shift right.
8          | &        | Left          | Bitwise AND (arithmetic).
9          | ^        | Left          | Bitwise XOR (arithmetic).
10         | \|       | Left          | Bitwise OR (arithmetic).
11         | <        | Left          | Less than (comparison).
11         | >        | Left          | Greater than (comparison).
11         | <=       | Left          | Less than or equal (comparison).
11         | >=       | Left          | Greater than or equal (comparison).
12         | =        | Left          | Equals (comparison).
12         | !=       | Left          | Is not equal to (comparison).
13         | &&       | Left          | Logical AND.
14         | \|\|     | Left          | Logical XOR.
15         | :=       | Left          | Assignment.
15         | :+=      | Left          | Assignment with addition.
15         | :-=      | Left          | Assignment with subtraction.
15         | :*=      | Left          | Assignment with multiplication.
15         | :/=      | Left          | Assignment with division.
15         | :**=     | Left          | Assignment with power.
15         | :$=      | Left          | Assignment with string concatenation.
15         | :%=      | Left          | Assignment with modulus.
15         | :^=      | Left          | Assignment with bitwise XOR.
15         | :<<=     | Left          | Assignment with bitwise left shift.
15         | :>>=     | Left          | Assignment with bitwise right shift.
15         | :\|=     | Left          | Assignment with bitwise OR.
15         | :&=      | Left          | Assignment with bitwise AND.

\section proposed Proposed Additions / Changes

Should dot-read on any type other than a table return null?

This would allow code to check for the existence of nested keys without the
risk of crashing when one of the expected keys is actually a value.
But would this cause problems with too much silent failure?
Possible situations:
- Passing any other type when a table is expected.
  - Reading would act like an empty table, writing would still fail.
 What behaviour should writing have in this situation?
 What happens when a readonly property is written?
  - Calling methods would still fail as an attempt to call a non-function.
    This does seem like it'd be catastrophically bad to fail silently.
Maybe reading any non-object should throw an error, and we should instead
provide a built-in function to read a nested key with full handling.
That way the dot operator works symmetrically, and the function works fully.

Exception handling.

This would probably be the usual try/catch stuff, except that without types
we'd need a way to determine the type of an exception.
Internally, we might have a catch-address stack for each function call.
Entering a try block would push the address of the catch block onto the stack,
and leaving would pop it.
We'd probably do something to put all the catch blocks at the end of the
bytecode.
We'd also have to implement exiting a function because an exception was
unhandled, and re-entering a function with a thrown exception.

Separation from Optimist.

Without being embeddable it doesn't quite make it a GameMonkey replacement.
We only need to be able to pull it out of Optimist and drop it somewhere else.

*/
