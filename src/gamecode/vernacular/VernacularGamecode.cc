/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/vernacular/VernacularGamecode.hh"
#include "gamecode/vernacular/documentation.h"
#include "filesystem/File.hh"
#include "library/text/String.hh"
#include "gamecode/GamecodeException.hh"
#include "gamecode/vernacular/compiler/VernacularParser.hh"
#include "gamecode/vernacular/compiler/VernacularBytecodeGenerator.hh"
#include "gamecode/vernacular/interpreter/VernacularThread.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// VernacularGamecode
//-----------------------------------------------------------------------------

VernacularGamecode::~VernacularGamecode()
{
}

bool VernacularGamecode::LoadFile(std::string filename)
{
	// If it's a directory, load it recursively.
	std::vector<std::string> files;
	if (filesystem->GetFilesInDirectory(filename, &files))
	{
		log->Start(ILogTarget::NOTIFICATION)->Write("Loading directory ")->Write(filename)->End();
		bool loaded = false;
		for (auto it = files.begin(); it != files.end(); ++it)
		{
			// TODO: There ought to be a filesystem utility or a string wrapper
			// which represents a filename and provides methods to concatenate
			// properly, get the path and extension and so on.
			std::string newFile(*it);
			if (filename[filename.length() - 1] != '/')
			{
				newFile = filename + "/" + newFile;
			}
			else
			{
				newFile = filename + newFile;
			}
			if (LoadFile(newFile))
			{
				loaded = true;
			}
		}
		return loaded;
	}

	File scriptFile(filesystem, filename);
	if (!scriptFile.file->IsOpen())
	{
		log->Start(ILogTarget::ERROR)->Write("Vernacular could not open script ")->Write(filename)->End();
		return false;
	}
	size_t size = scriptFile.file->Size();
	std::vector<char> buffer;
	buffer.resize(size + 1);
	scriptFile.file->ReadBytes(&buffer[0], size);
	buffer[size] = '\0';
	scriptFile.file->Close();
	return RunBuffer(&buffer[0], size, filename);
}

bool VernacularGamecode::Eval(std::string code)
{
	return RunBuffer(code.c_str(), code.length(), "evaluated code");
}

bool VernacularGamecode::RunBuffer(const char *buffer, size_t size, std::string filename)
{
	// parse to tree
	VernacularAST *tree;
	try
	{
		VernacularParser parser;
		tree = parser.Parse(buffer, size);
	}
	catch (ParseException &e)
	{
		log->Start(ILogTarget::ERROR)
			->Write(filename)->Write(":")
			->Write(std::to_string(e.line))->Write(",")->Write(std::to_string(e.token))->Write(": ")
			->Write(e.what())->End();
		return false;
	}
	tree->filename = filename;

	if (astDumpOnCompile != nullptr)
	{
		tree->Dump(astDumpOnCompile);
	}

	// validate the tree
	if (!tree->Validate(log))
	{
		log->Start(ILogTarget::ERROR)->Write("There were errors after parsing script '")->Write(filename)->Write("'")->End();
		delete tree;
		return false;
	}

	// TODO: optimize the tree
	// The AST needs its own methods to self-optimize.
	// Alternately the compiler can do it, because it has access to the
	// interpreter and thus the ability to perform operations on values.

	VernacularFunction *function = nullptr;
	try
	{
		// If this fails, it'll be tidy because globals are only created when
		// a successfully compiled function runs.
		// The rest of the mess will get garbage collected.
		VernacularBytecodeGenerator codegen(&functionFactory, &objectFactory, &stringFactory, log);
		function = codegen.CompileFunction(tree);

		if (functionDumpOnCompile != nullptr)
		{
			function->Dump(functionDumpOnCompile, true);
		}
	}
	catch (VernacularCompileException &e)
	{
		log->Start(ILogTarget::ERROR)
			->Write(filename)->Write(":")
			->Write(std::to_string(e.line))->Write(",")->Write(std::to_string(e.token))->Write(": ")
			->Write(e.what())->End();
		delete tree;
		return false;
	}

	log->Start(ILogTarget::NOTIFICATION)->Write("Loaded script '")->Write(filename)->Write("'")->End();
	delete tree;

	// Because this is a root file, any globals referenced get created.
	function->CreateAndMapGlobals(&globals);
	VernacularThread thread(function, this, CalculateStackSize(function));

	FunctionGCWrapper wrapper(function);
	gc.AddRootObject(&wrapper);
	try
	{
		RunThread(&thread);
	}
	catch (std::exception &e)
	{
		gc.RemoveRootObject(&wrapper);
		throw;
	}
	gc.RemoveRootObject(&wrapper);

	return true;
}

void VernacularGamecode::RunThread(VernacularThread *thread)
{
	gc.AddRootObject(thread);

	// Garbage collect some old stuff.
	// Note that we can never start collection in the middle of execution
	// because we don't know if all the objects created have been made
	// accessible yet.
	if (gc.IsReady() && gc.LastAmountUncollected() > gc.CurrentNumObjects() * 2)
	{
		gc.StartCollection();
	}
	if (!gc.IsReady())
	{
		gc.CollectSome(gc.LastAmountUncollected() / 8);
	}

	VernacularThread::RunResult result;
	try
	{
		result = thread->Run(instructionLimit, log);
	}
	catch (std::exception &e)
	{
		gc.RemoveRootObject(thread);
		throw;
	}
	if (result == VernacularThread::R_EXCEPTION)
	{
		log->Start(ILogTarget::ERROR)->Write(thread->GetExceptionMessage())->End();
		thread->DumpStack(log);
		gc.RemoveRootObject(thread);
		throw gamecode_exception(std::string("Exception in vernacular language:") + thread->GetExceptionMessage());
	}
	else if (result == VernacularThread::R_REACHED_LIMIT)
	{
		log->Start(ILogTarget::ERROR)->Write("Instruction limit reached. Possible runaway loop error.")->End();
		gc.RemoveRootObject(thread);
		throw gamecode_exception("Maximum instruction count reached");
	}

	gc.RemoveRootObject(thread);
}

int VernacularGamecode::CalculateStackSize(VernacularFunction *function)
{
	// TODO: Fancy calculation.
	return 4096;
}

VernacularValue VernacularGamecode::GetGlobal(std::string name)
{
	int offset = globals.FindAddress(name);
	if (offset < 0)
	{
		throw gamecode_exception("GetGlobal: No such global variable '" + name + "'");
	}
	VernacularValue &funcValue = globals.GetArray()[offset];
	if (!funcValue.IsFunction())
	{
		throw gamecode_exception("CallFunction: Global variable '" + name + "' is not a function");
	}
	return funcValue;
}

GamecodeValue VernacularGamecode::CallFunction(std::string &name, std::vector<GamecodeValue> &parameters)
{
	int offset = globals.FindAddress(name);
	if (offset < 0)
	{
		throw gamecode_exception("CallFunction: No such global variable '" + name + "'");
	}
	VernacularValue &funcValue = globals.GetArray()[offset];
	if (!funcValue.IsFunction())
	{
		throw gamecode_exception("CallFunction: Global variable '" + name + "' is not a function");
	}
	VernacularFunction *function = funcValue.GetVernacularFunction();
	VernacularThread thread(function, this, CalculateStackSize(function));

	for (int i = 0; i < thread.NumParameters(); i += 1)
	{
		if (i < parameters.size())
		{
			thread.SetParameter(i, parameters[i]);
		}
		else
		{
			thread.SetParameter(i, VernacularValue());
		}
	}

	RunThread(&thread);

	return thread.GetReturnValue();
}

void VernacularGamecode::DumpFunction(std::string &name, std::ostream *ss, bool recursive)
{
	int offset = globals.FindAddress(name);
	if (offset < 0)
	{
		throw gamecode_exception("DumpFunction: No such global variable");
	}
	VernacularValue &funcValue = globals.GetArray()[offset];
	if (!funcValue.IsFunction())
	{
		throw gamecode_exception("DumpFunction: Global variable is not a function");
	}
	VernacularFunction *function = funcValue.GetVernacularFunction();
	function->Dump(ss, recursive);
}

GamecodeValue VernacularGamecode::CallFunction(GamecodeValue func, std::vector<GamecodeValue> &parameters, GamecodeValue thisObject)
{
	VernacularValue funcValue = func;
	if (!funcValue.IsFunction())
	{
		throw std::invalid_argument("CallFunction: Expected a function object to be passed, but the value was another type");
	}
	VernacularFunction *function = funcValue.GetVernacularFunction();
	VernacularThread thread(function, this, CalculateStackSize(function));

	for (int i = 0; i < thread.NumParameters(); i += 1)
	{
		if (i < parameters.size())
		{
			thread.SetParameter(i, parameters[i]);
		}
		else
		{
			thread.SetParameter(i, VernacularValue());
		}
	}

	if (!thisObject.IsNull())
	{
		if (!thisObject.IsTable())
		{
			throw std::invalid_argument("CallFunction: Non-object and non-null passed for 'this'");
		}
		thread.SetThis(thisObject);
	}

	RunThread(&thread);
	return thread.GetReturnValue();
}

GamecodeValue VernacularGamecode::GetFunction(std::string &name)
{
	int offset = globals.FindAddress(name);
	if (offset < 0)
	{
		return GamecodeValue();
	}
	VernacularValue &funcValue = globals.GetArray()[offset];
	if (!funcValue.IsFunction())
	{
		return GamecodeValue();
	}
	return funcValue;
}

void VernacularGamecode::RegisterAdapter(IGamecodeAdapter *adapter)
{
	VernacularObject *object = objectFactory.Allocate();
	std::string name = String::TransformCamelCase(adapter->GetName(), true, true);
	int globalIndex = globals.Create(String::TransformCamelCase(adapter->GetName(), true, true));
	globals.GetArray()[globalIndex].SetObject(object);

	auto map = adapter->GetGlobalFunctions();
	for (auto it = map.begin(); it != map.end(); ++it)
	{
		VernacularString *key = stringFactory.Allocate(String::TransformCamelCase(it->first, false, true));
		auto method = it->second;
		VernacularFunction *func = functionFactory.Allocate(
			[method](VernacularValue* parameters, int numParameters, IVernacularEnvironment *environment) -> VernacularValue {
				GamecodeParameterList parameterList;
				for (int i = 0; i < numParameters; i += 1)
				{
					parameterList.push_back((GamecodeValue)parameters[i]);
				}
				return (VernacularValue)method(&parameterList, environment->GetGamecode());
			}
		);
		VernacularValue keyVal, funcVal;
		keyVal.SetString(key);
		funcVal.SetFunction(func);
		object->Set(keyVal, funcVal);
	}

	adapters.push_back(adapter);
	adapter->OnRegister(this);
}

void VernacularGamecode::RegisterEngineType(std::string name, IGamecodeEngineObjectDescriptor *type)
{
	name = String::TransformCamelCase(name, true, true);
	engineObjectTypes.push_back(type);
	VernacularFunction *func = functionFactory.Allocate(type);
	int globalIndex = globals.Create(name);
	globals.GetArray()[globalIndex].SetFunction(func);
	userObjectFactory.AddType(type);
}

GamecodeValue VernacularGamecode::CreateString(const char* s)
{
	VernacularString *string = stringFactory.Allocate(s);
	VernacularValue value;
	value.SetString(string);
	return (GamecodeValue)value;
}

GamecodeValue VernacularGamecode::CreateTable()
{
	VernacularObject *object = objectFactory.Allocate();
	VernacularValue value;
	value.SetObject(object);
	return (GamecodeValue)value;
}

void VernacularGamecode::ThrowException(std::string message)
{
	log->Start(ILogTarget::ERROR)->Write(message)->End();
}

void VernacularGamecode::ThrowWarning(std::string message)
{
	log->Start(ILogTarget::WARNING)->Write(message)->End();
}

void VernacularGamecode::Print(std::string message)
{
	log->Start(ILogTarget::NOTIFICATION)->Write(message)->End();
}

void VernacularGamecode::GetChildren(std::vector<IVernacularCollectable*> *objects)
{
	std::vector<GamecodeValue> childObjects;
	for (int i = 0; i < adapters.size(); i += 1)
	{
		adapters[i]->GetChildObjects(&childObjects);
	}
	for (int i = 0; i < childObjects.size(); i += 1)
	{
		GamecodeValue child = childObjects[i];
		IVernacularCollectable *collectable;
		switch (childObjects[i].GetType())
		{
		case GamecodeValue::T_String:
			collectable = dynamic_cast<IVernacularCollectable*>(child.GetStringObject());
			break;
		case GamecodeValue::T_Table:
			collectable = dynamic_cast<IVernacularCollectable*>(child.GetTable());
			break;
		case GamecodeValue::T_Function:
			collectable = dynamic_cast<IVernacularCollectable*>(child.GetFunction());
			break;
		case GamecodeValue::T_Engine:
			collectable = dynamic_cast<IVernacularCollectable*>(child.GetEngine());
			break;
		}
		if (collectable != nullptr)
		{
			objects->push_back(collectable);
		}
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
