/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gamecode/IGamecode.hh"
#include <sstream>
#include "filesystem/IFilesystem.hh"
#include "log/ILogTarget.hh"
#include "gamecode/vernacular/VernacularCoreLibrary.hh"
#include "gamecode/vernacular/types/VernacularFunctionFactory.hh"
#include "gamecode/vernacular/types/VernacularObjectFactory.hh"
#include "gamecode/vernacular/types/VernacularStringFactory.hh"
#include "gamecode/vernacular/interpreter/VernacularInterpreter.hh"
#include "gamecode/vernacular/interpreter/VernacularGarbageCollector.hh"
#include "gamecode/vernacular/interpreter/IVernacularEnvironment.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class VernacularGamecode : public IGamecode, public IVernacularEnvironment, public IVernacularGCRoot {
public:
	// Construct
	VernacularGamecode(IFilesystem *filesystem, ILogTarget *log) :
		astDumpOnCompile(nullptr),
		functionDumpOnCompile(nullptr),
		instructionLimit(256*1024*1024),
		log(log),
		filesystem(filesystem),
		stringFactory(&gc),
		objectFactory(&gc),
		functionFactory(&gc),
		userObjectFactory(&gc)
	{
		coreLibrary.Register(this);
		gc.AddRootObject(&globals);
		gc.AddRootObject(&stringFactory);
		gc.AddRootObject(&userObjectFactory);
		gc.AddRootObject(this);
	}

	// Load file
	bool LoadFile(std::string filename);
	bool Eval(std::string code);

	void SetInstructionLimit(int limit) { instructionLimit = limit; }

	// IGamecode
	~VernacularGamecode();
	GamecodeValue CallFunction(std::string &name, std::vector<GamecodeValue> &parameters);
	GamecodeValue CallFunction(GamecodeValue function, std::vector<GamecodeValue> &parameters, GamecodeValue thisObject);
	GamecodeValue GetFunction(std::string &name);
	void PreserveTable(GamecodeValue table, IGamecodeAdapter *adapter);
	void ReleaseTable(GamecodeValue table, IGamecodeAdapter *adapter);
	void RegisterAdapter(IGamecodeAdapter *adapter);
	void RegisterEngineType(std::string name, IGamecodeEngineObjectDescriptor *type);
	GamecodeValue CreateString(const char* s);
	GamecodeValue CreateTable();

	// Debug stuff
	void DumpFunction(std::string &name, std::ostream *ss, bool recursive);
	void DumpASTOnCompile(std::ostream *ss) { astDumpOnCompile = ss; }
	void DumpFunctionOnCompile(std::ostream *ss) { functionDumpOnCompile = ss; }
	VernacularValue GetGlobal(std::string name);

	// IVernacularEnvironment
	IGamecode* GetGamecode() { return this; }
	VernacularGlobals* GetGlobals() { return &globals; }
	VernacularFunctionFactory* GetFunctionFactory() { return &functionFactory; }
	VernacularObjectFactory* GetObjectFactory() { return &objectFactory; }
	VernacularStringFactory* GetStringFactory() { return &stringFactory; }
	VernacularUserObjectFactory* GetUserObjectFactory() { return &userObjectFactory; }
	bool IncludeFile(std::string filename) { return LoadFile(filename); }
	void ThrowException(std::string message);
	void ThrowWarning(std::string message);
	void Print(std::string message);
	ILogTarget *GetLog() { return log; }

	// IVernacularGCRoot
	virtual void GetChildren(std::vector<IVernacularCollectable*> *objects);

private:
	// We use this struct to wrap a the function from evaluated or loaded code.
	struct FunctionGCWrapper : public IVernacularGCRoot {
		VernacularFunction *function;
		FunctionGCWrapper(VernacularFunction *func) : function(func) {}
		void GetChildren(std::vector<IVernacularCollectable*> *objects) { objects->push_back(function); }
	};

	VernacularCoreLibrary coreLibrary;

	std::ostream *astDumpOnCompile;
	std::ostream *functionDumpOnCompile;

	int instructionLimit;
	ILogTarget *log;
	IFilesystem *filesystem;

	VernacularGlobals globals;
	VernacularGarbageCollector gc;
	VernacularStringFactory stringFactory;
	VernacularObjectFactory objectFactory;
	VernacularFunctionFactory functionFactory;
	VernacularUserObjectFactory userObjectFactory;

	std::vector<IGamecodeAdapter*> adapters;
	std::vector<IGamecodeEngineObjectDescriptor*> engineObjectTypes;

	bool RunBuffer(const char *buffer, size_t size, std::string filename);
	void RunThread(VernacularThread *thread);
	int CalculateStackSize(VernacularFunction *function);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
