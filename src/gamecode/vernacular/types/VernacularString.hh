/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <sstream>
#include "gamecode/GamecodeValue.hh"
#include "gamecode/vernacular/interpreter/IVernacularCollectable.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class VernacularString : public IGamecodeString, public IVernacularCollectable {
public:
	/**
	 * The length below which we de-dupe, and above which we lazy-concatenate.
	 * De-duping is done by the string manager itself.
	 */
	static const int key_size = 32;

	VernacularString() : length(0), start(nullptr), end(nullptr), data(nullptr) {}
	VernacularString(const char *str);
	VernacularString(std::string str);
	/**
	 * Construct by concatenating two other strings.
	 * The other two strings must also be managed by the same string manager.
	 */
	VernacularString(VernacularString *start, VernacularString *end);
	~VernacularString();

	/**
	 * Implements IGamecodeString::Get
	 */
	const char* Get();

	/**
	 * Implements IVernacularCollectable::GetChildren
	 */
	void GetChildren(std::vector<IVernacularCollectable*> *objects);

	int Length() { return length; }

	int UnicodeLength() { return unicodeLength; }

	/**
	 * Returns true if this string is identical, else false.
	 * Requires that the other string be owned by the same string manager.
	 */
	bool Equals(VernacularString *other);

	/**
	 * Returns zero if it's the same string, negative if this string sorts
	 * before the other string, positive if it sorts after.
	 */
	int CompareTo(VernacularString *other);

	/**
	 * Writes the substring of this string starting at the given offset and of
	 * the given length, to the buffer. The buffer can then be added as a new
	 * string by the string manager, if that's what we're doing.
	 */
	void SubstringToBuffer(char *buffer, int offset, int length);

	/**
	 * Writes a substring of this string starting at the given character and
	 * of the given length in characters, to the stream.
	 * Returns the number of characters written to the stream.
	 */
	int UnicodeSubstringToStream(std::stringstream *stream, int offset, int length);

	/**
	 * Efficiently copy our contents to a buffer.
	 * This is used when combining so that we don't cause all child parts to
	 * needlessly form intermediate buffers.
	 */
	void CopyToBuffer(char* buffer);

private:
	int length;
	int unicodeLength;
	VernacularString *start;
	VernacularString *end;
	char *data;

	/**
	 * If we're a composite string, combine our child parts into a single
	 * buffer.
	 */
	void Combine();
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
