/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gamecode/vernacular/types/VernacularValue.hh"
#include "gamecode/vernacular/types/VernacularUserObject.hh"
#include "gamecode/vernacular/types/VernacularEngineObjectDescriptor.hh"
#include "gamecode/vernacular/interpreter/VernacularGarbageCollector.hh"
#include "gamecode/IGamecodeEngineObject.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class VernacularUserObjectFactory : public IVernacularGCListener, public IVernacularGCRoot {
public:
	VernacularUserObjectFactory(VernacularGarbageCollector *gc);
	VernacularUserObject *Allocate(IGamecodeEngineObject *object, VernacularEngineObjectDescriptor *type = nullptr);
	VernacularUserObject *Allocate(VernacularEngineObjectDescriptor *type, VernacularValue *parameters, int numParameters);

	// TODO: We could take complete charge of engine object descriptors, but
	// we'd also have to be given their name.
	void AddType(IGamecodeEngineObjectDescriptor *type) { types.push_back(new VernacularEngineObjectDescriptor(type)); }

	// IVernacularGCListener
	void OnCollect(IVernacularCollectable* object);
	void Collect(IVernacularCollectable* object) { delete object; }

	// IVernacularGCRoot (for const strings)
	void GetChildren(std::vector<IVernacularCollectable*> *objects);

private:
	VernacularGarbageCollector *gc;
	std::vector<VernacularEngineObjectDescriptor*> types;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
