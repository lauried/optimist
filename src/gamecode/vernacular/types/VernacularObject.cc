/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/vernacular/types/VernacularObject.hh"
#include <iostream>
#include "gamecode/vernacular/types/VernacularString.hh"
#include "gamecode/vernacular/types/VernacularFunction.hh"
#include "gamecode/vernacular/types/VernacularUserObject.hh"
#include "gamecode/vernacular/interpreter/IVernacularEnvironment.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

VernacularObject::VernacularObject(IndexPoolType *pool)
	: flags(0)
	, copyOnWrite(nullptr)
{
	index = new Index();
}

VernacularObject::VernacularObject(VernacularObject *copyOnWrite)
	: index(copyOnWrite->index)
	, flags(FL_COPY_ON_WRITE)
	, copyOnWrite(copyOnWrite)
{
	if (!copyOnWrite->flags & FL_LITERAL)
	{
		throw std::logic_error("Cannot create a copy-on-write copy of a non-literal");
	}
}

VernacularObject::~VernacularObject()
{
	// In case we get garbage collected while the original index is being
	// iterated. This should only happen if we garbage collect in the middle
	// of running a thread.
	// We currently don't run in the middle of a thread and we might need to do
	// other things to support it.
	DeleteOrDisownIndex();
}

void VernacularObject::BecomeLiteral()
{
	if (flags)
	{
		throw std::logic_error("Cannot become literal when flags are already set");
	}
	flags = FL_LITERAL;
}

void VernacularObject::Set(VernacularValue key, VernacularValue value, IVernacularEnvironment *environment, bool isThis, int flags)
{
	PerformWriteChecks();
	if (value.IsNull())
	{
		index->Unset(key, environment, isThis, flags);
		return;
	}
	index->Set(key, value, environment, isThis, flags);
}

VernacularValue VernacularObject::Get(VernacularValue key, IVernacularEnvironment *environment, bool isThis)
{
	return index->Get(key, environment, isThis);
}

VernacularValue VernacularObject::Back()
{
	PerformWriteChecks();
	return index->Back();
}

void VernacularObject::PushBack(VernacularValue key, VernacularValue value)
{
	PerformWriteChecks();
	index->PushBack(key, value);
}

void VernacularObject::PushBack(VernacularValue value)
{
	PerformWriteChecks();
	index->PushBack(value);
}

VernacularValue VernacularObject::PopBack()
{
	PerformWriteChecks();
	return index->PopBack();
}

VernacularValue VernacularObject::Front()
{
	return index->Front();
}

void VernacularObject::PushFront(VernacularValue key, VernacularValue value)
{
	PerformWriteChecks();
	index->PushFront(key, value);
}

void VernacularObject::PushFront(VernacularValue value)
{
	PerformWriteChecks();
	index->PushFront(value);
}

VernacularValue VernacularObject::PopFront()
{
	// TODO: As a language feature, Pop should error on a table with metadata
	// set. Perhaps we should create a whole separate type for classes.
	PerformWriteChecks();
	return index->PopFront();
}

// ^^^ Working from here
//---------------------------------------------------------------

VernacularValue VernacularObject::KeyForValue(VernacularValue &value)
{
	for (Iterator iterator = Iterator(index, false); !iterator.End(); iterator.Next())
	{
		if (iterator.Value().Equals(value))
		{
			return iterator.Key();
		}
	}
	static VernacularValue nullValue;
	return nullValue;
}

int VernacularObject::Count()
{
	return index->Count();
}

void VernacularObject::Copy(VernacularObject *object)
{
	PerformNewIndexChecks();
	DeleteOrDisownIndex();
	index = CopyIndex(object->index);
}

void VernacularObject::CopyKeys(VernacularObject *object)
{
	PerformNewIndexChecks();
	Index *newIndex = new Index();
	for (Iterator iterator = Iterator(index, false); !iterator.End(); iterator.Next())
	{
		newIndex->PushBack(iterator.Key());
	}
	DeleteOrDisownIndex();
	index = newIndex;
}

void VernacularObject::Reverse()
{
	PerformWriteChecks();
	index->Reverse();
}

void VernacularObject::Reindex(double start, double step)
{
	PerformNewIndexChecks();
	Index *newIndex = new Index();
	double j = start;
	for (Iterator iterator = Iterator(index, true); !iterator.End(); iterator.Next())
	{
		newIndex->Set(VernacularValue(j), iterator.Value(), nullptr, true, iterator.Flags());
		j += step;
	}
	DeleteOrDisownIndex();
	index = newIndex;
}

//-----------------------------------------------------------------------------
// IGamecodeTable
//-----------------------------------------------------------------------------

GamecodeValue VernacularObject::Get(GamecodeValue key)
{
	VernacularValue vernacularKey = key;
	return Get(vernacularKey);
}

int VernacularObject::NumKeys()
{
	return 0;
}

GamecodeValue VernacularObject::GetKey(int i)
{
	return GamecodeValue();
}

GamecodeValue VernacularObject::GetValue(int i)
{
	return GamecodeValue();
}

void VernacularObject::Set(GamecodeValue key, GamecodeValue value)
{
	VernacularValue vernacularKey = key;
	VernacularValue vernacularValue = value;
	Set(vernacularKey, vernacularValue);
}

//-----------------------------------------------------------------------------
// IVernacularCollectable
//-----------------------------------------------------------------------------

void ValueToList(VernacularValue value, std::vector<IVernacularCollectable*> *objects)
{
	IVernacularCollectable *collectable = value.GetVernacularCollectable();
	if (collectable != nullptr)
	{
		objects->push_back(collectable);
	}
}

void VernacularObject::GetChildren(std::vector<IVernacularCollectable*> *objects)
{
	for (Iterator iterator = Iterator(index, true); !iterator.End(); iterator.Next())
	{
		ValueToList(iterator.Key(), objects);
		ValueToList(iterator.Value(), objects);
	}
	if (copyOnWrite != nullptr)
	{
		objects->push_back(copyOnWrite);
	}
}

//-----------------------------------------------------------------------------
// VernacularObject::Iterator
//-----------------------------------------------------------------------------

VernacularObject::Iterator::Iterator(VernacularObject::Index *index, bool privateAccess)
	: index(index)
	, privateAccess(privateAccess)
{
	index->iteratorCount += 1;
	iterator = index->IteratorFront();
	Next();
}

bool VernacularObject::Iterator::Next()
{
	if (privateAccess)
	{
		iterator.NextAll();
	}
	else
	{
		iterator.Next();
	}
	if (iterator.IsBack())
	{
		Finished();
		return false;
	}
	return true;
}

bool VernacularObject::Iterator::End()
{
	return index == nullptr;
}

VernacularValue VernacularObject::Iterator::Key()
{
	return iterator.Key();
}

int VernacularObject::Iterator::Flags()
{
	return iterator.Flags();
}

VernacularValue VernacularObject::Iterator::Value()
{
	return iterator.Value();
}

void VernacularObject::Iterator::Finished()
{
	if (index == nullptr)
	{
		return;
	}
	index->iteratorCount -= 1;
	if (!index->tableOwned && index->iteratorCount == 0)
	{
		delete index;
	}
	index = nullptr;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
