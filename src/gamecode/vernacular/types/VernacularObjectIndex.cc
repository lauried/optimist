/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "gamecode/vernacular/types/VernacularObjectIndex.hh"
#include <iostream>
#include "gamecode/vernacular/interpreter/IVernacularEnvironment.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

VernacularObjectIndex::VernacularObjectIndex()
{
	front.next = &back;
	back.prev = &front;
}

void VernacularObjectIndex::Set(VernacularValue key, VernacularValue value, IVernacularEnvironment *environment, bool allowPrivate, int flags)
{
	Node *node = FindNode(key);
	if (node == nullptr)
	{
		node = PushNodeBack(key, value);
		node->flags = flags;
		return;
	}
	if (!allowPrivate && (node->flags & (FL_PRIVATE | FL_READONLY)))
	{
		if (environment != nullptr)
		{
			environment->ThrowException("Unable to set private or readonly key");
		}
		return;
	}
	node->value = value;
	node->flags = flags;
}

void VernacularObjectIndex::Unset(VernacularValue key, IVernacularEnvironment *environment, bool allowPrivate, int flags)
{
    NodeIterator iterator = FindNodeIterator(key);
    if (iterator == nodes.end())
    {
		return;
    }
    Node *node = &(iterator->second);
	if (!allowPrivate && node->flags & (FL_PRIVATE | FL_READONLY))
	{
		if (environment != nullptr)
		{
			environment->ThrowException("Unable to set private or readonly key");
		}
		return;
	}
	EraseNode(iterator);
}

VernacularValue VernacularObjectIndex::Get(VernacularValue key, IVernacularEnvironment *environment, bool allowPrivate)
{
	Node *node = FindNode(key);
	if (node == nullptr || (!allowPrivate && (node->flags & FL_PRIVATE)))
	{
		return VernacularValue();
	}
	return node->value;
}


VernacularValue VernacularObjectIndex::Back()
{
	if (back.prev == &front)
	{
		return VernacularValue();
	}
	return back.prev->key;
}

void VernacularObjectIndex::PushBack(VernacularValue key, VernacularValue value)
{
	NodeIterator iterator = FindNodeIterator(key);
	if (iterator != nodes.end())
	{
		EraseNode(iterator);
	}
	PushNodeBack(key, value);
}

VernacularValue VernacularObjectIndex::PopBack()
{
	VernacularValue key;
	if (back.prev != &front)
	{
		key = back.prev->key;
		NodeIterator iterator = FindNodeIterator(key);
		EraseNode(iterator);
	}
	return key;
}

VernacularValue VernacularObjectIndex::Front()
{
	if (front.next == &back)
	{
		return VernacularValue();
	}
	return front.next->key;
}

void VernacularObjectIndex::PushFront(VernacularValue key, VernacularValue value)
{
	NodeIterator iterator = FindNodeIterator(key);
	if (iterator != nodes.end())
	{
		EraseNode(iterator);
	}
	PushNodeFront(key, value);
}

VernacularValue VernacularObjectIndex::PopFront()
{
	VernacularValue key;
	if (front.next != &back)
	{
		key = front.next->key;
		NodeIterator iterator = FindNodeIterator(key);
		EraseNode(iterator);
	}
	return key;
}

VernacularValue VernacularObjectIndex::KeyForValue(VernacularValue &value)
{
	for (auto it = nodes.begin(); it != nodes.end(); it++)
	{
		if (it->second.value.Equals(value))
		{
			return it->second.key;
		}
	}
	return VernacularValue();
}

int VernacularObjectIndex::Count()
{
	return nodes.size();
}

void VernacularObjectIndex::VernacularObjectIndex::Reverse()
{
	// Note we iterate forward using the prev pointer, because it's really the
	// old forward pointer that we're in the process of swapping and we're
	// iterating in the old forward direction.
	for (Node *node = front.next; node != &back; node = node->prev)
	{
		Node *tmp = node->prev;
		node->prev = node->next;
		node->next = node->prev;
	}
	Node *tmp = front.next;
	front.next = back.prev;
	back.prev = tmp;
}

//-----------------------------------------------------------------------------
// Internals/helpers
//-----------------------------------------------------------------------------

void VernacularObjectIndex::PushNodeFront(Node *node)
{
	// front > currentfront
	// front < currentfront
	// BECOMES
	// front > node > currentfront
	// front < node < currentfront
	node->next = front.next;
	node->prev = &front;
	front.next->prev = node;
	front.next = node;
}

void VernacularObjectIndex::PushNodeBack(Node *node)
{
	// currentback > back
	// currentback < back
	// BECOMES
	// currentback > node > back
	// currentback < node < back
	node->prev = back.prev;
	node->next = &back;
	back.prev->next = node;
	back.prev = node;
}

void VernacularObjectIndex::UnlinkNode(Node *node)
{
	// prev > node > next
	// prev < node < next
	// BECOMES
	// prev > next
	// prev < next
	node->prev->next = node->next;
	node->next->prev = node->prev;
	// We don't care about the node's pointers because it's being deleted
}

VernacularObjectIndex::Node *VernacularObjectIndex::FindNode(VernacularValue key)
{
	NodeIterator iterator = FindNodeIterator(key);
	if (iterator == nodes.end())
	{
		return nullptr;
	}
	return &(iterator->second);
}

VernacularObjectIndex::NodeIterator VernacularObjectIndex::FindNodeIterator(VernacularValue key)
{
	return nodes.find(key);
}

VernacularObjectIndex::Node* VernacularObjectIndex::PushNodeFront(VernacularValue key, VernacularValue value)
{
	auto pair = nodes.insert(std::make_pair(key, Node(key, value)));
	Node *node = &(pair.first->second);
	PushNodeFront(node);
	return node;
}

VernacularObjectIndex::Node* VernacularObjectIndex::PushNodeBack(VernacularValue key, VernacularValue value)
{
	auto pair = nodes.insert(std::make_pair(key, Node(key, value)));
	Node *node = &(pair.first->second);
	PushNodeBack(node);
	return node;
}

void VernacularObjectIndex::EraseNode(NodeIterator iterator)
{
	UnlinkNode(&(iterator->second));
	nodes.erase(iterator);
}

//-----------------------------------------------------------------------------
// Iterator
//-----------------------------------------------------------------------------

void VernacularObjectIndex::Iterator::Next()
{
	// Re-do the NextAll while flags are nonzero.
	do
	{
		NextAll();
	}
	while (current->flags & FL_PRIVATE != 0);
}

void VernacularObjectIndex::Iterator::NextAll()
{
	if (current->next != nullptr)
	{
		current = current->next;
	}
}

void VernacularObjectIndex::Iterator::Prev()
{
	do
	{
		PrevAll();
	}
	while (current->flags & FL_PRIVATE != 0);
}

void VernacularObjectIndex::Iterator::PrevAll()
{
	if (current->prev != nullptr)
	{
		current = current->prev;
	}
}

bool VernacularObjectIndex::Iterator::IsFront()
{
	return current->prev == nullptr;
}

bool VernacularObjectIndex::Iterator::IsBack()
{
	return current->next == nullptr;
}

VernacularValue &VernacularObjectIndex::Iterator::Key()
{
	return current->key;
}

VernacularValue &VernacularObjectIndex::Iterator::Value()
{
	return current->value;
}

int VernacularObjectIndex::Iterator::Flags()
{
	return current->flags;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
