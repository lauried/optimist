/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gamecode/vernacular/interpreter/VernacularGarbageCollector.hh"
#include <vector>
#include "gamecode/vernacular/types/VernacularEngineObjectDescriptor.hh"
#include "gamecode/IGamecodeEngineObject.hh"
#include "gamecode/GamecodeEngineObjectDescriptor.hh"
#include "gamecode/vernacular/types/VernacularValue.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class VernacularUserObject : public IVernacularCollectable, public IGamecodeEngineObject::ImplementationData {
public:
	VernacularUserObject(IGamecodeEngineObject *object, VernacularEngineObjectDescriptor *type) :
		IVernacularCollectable(),
		object(object),
		type(type)
	{
		object->implementationData = this;
	}

	~VernacularUserObject();

	// IVernacularCollectable
	void GetChildren(std::vector<IVernacularCollectable*> *objects);

	IGamecodeEngineObject *GetEngineObject() { return object; }

	bool SetKey(VernacularValue key, VernacularValue value);
	VernacularValue GetKey(VernacularValue key, IVernacularEnvironment *environment);

	std::string GetTypeName() { return type->GetName(); }

private:
	IGamecodeEngineObject *object;
	VernacularEngineObjectDescriptor *type;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
