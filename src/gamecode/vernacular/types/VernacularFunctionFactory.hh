/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gamecode/vernacular/types/VernacularFunction.hh"
#include <iostream>
#include "gamecode/vernacular/interpreter/VernacularGarbageCollector.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class IVernacularEnvironment;

class VernacularFunctionFactory : public IVernacularGCListener {
public:
	VernacularFunctionFactory(VernacularGarbageCollector *gc) : gc(gc) {}

	VernacularFunction *Allocate();
	VernacularFunction *Allocate(IGamecodeEngineObjectDescriptor *engineObjectDescriptor, int index = -1);
	VernacularFunction *Allocate(IGamecodeAdapter *gamecodeAdapter, int index);
	VernacularFunction *Allocate(VernacularFunction::Callback callback);
	VernacularFunction *Allocate(VernacularObject *object, VernacularFunction *method);

	void OnCollect(IVernacularCollectable* object) { }
	void Collect(IVernacularCollectable* object) { delete object; }

private:
	VernacularGarbageCollector *gc;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
