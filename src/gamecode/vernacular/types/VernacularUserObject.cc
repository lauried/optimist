/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/vernacular/types/VernacularUserObject.hh"
#include "gamecode/vernacular/types/VernacularFunctionFactory.hh"
#include "gamecode/vernacular/interpreter/IVernacularEnvironment.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

VernacularUserObject::~VernacularUserObject()
{
	type->Destruct(object);
}

void VernacularUserObject::GetChildren(std::vector<IVernacularCollectable*> *objects)
{
	std::vector<IGamecodeObject*> *childObjects = object->GetChildObjects();
	if (childObjects == nullptr)
	{
		return;
	}
	for (auto it = childObjects->begin(); it != childObjects->end(); ++it)
	{
		IGamecodeEngineObject *engineObject = dynamic_cast<IGamecodeEngineObject*>(*it);
		if (engineObject != nullptr)
		{
			VernacularUserObject *userObject = dynamic_cast<VernacularUserObject*>(engineObject->implementationData);
			if (userObject != nullptr)
			{
				objects->push_back(userObject);
			}
		}
		// An IGamecodeEngineObject won't be an IVernacularCollectable.
		IVernacularCollectable *collectable = dynamic_cast<IVernacularCollectable*>(*it);
		if (collectable != nullptr)
		{
			objects->push_back(collectable);
		}
	}
}

bool VernacularUserObject::SetKey(VernacularValue key, VernacularValue value)
{
	if (key.GetType() != GamecodeValue::T_String)
	{
		return false;
	}
	type->SetProperty(object, key.GetString(), (GamecodeValue)value);
	return true;
}

VernacularValue VernacularUserObject::GetKey(VernacularValue key, IVernacularEnvironment *environment)
{
	std::string strKey = key.ToString();
	int methodIndex = type->MethodIndex(strKey);
	if (methodIndex != -1)
	{
		return VernacularValue(type->GetFunction(methodIndex, environment->GetFunctionFactory()));
	}
	GamecodeValue gamecodeValue = type->GetProperty(object, strKey);

	return (VernacularValue)gamecodeValue;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
