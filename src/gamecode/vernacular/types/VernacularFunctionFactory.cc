/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/vernacular/types/VernacularFunctionFactory.hh"
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

VernacularFunction *VernacularFunctionFactory::Allocate()
{
	VernacularFunction *function = new VernacularFunction();
	gc->AddObject(function, this);
	return function;
}

VernacularFunction *VernacularFunctionFactory::Allocate(IGamecodeEngineObjectDescriptor *engineObjectDescriptor, int index)
{
	VernacularFunction *function = new VernacularFunction(engineObjectDescriptor, index);
	gc->AddObject(function, this);
	return function;
}

VernacularFunction *VernacularFunctionFactory::Allocate(IGamecodeAdapter *gamecodeAdapter, int index)
{
	VernacularFunction *function = new VernacularFunction(gamecodeAdapter, index);
	gc->AddObject(function, this);
	return function;
}

VernacularFunction *VernacularFunctionFactory::Allocate(VernacularFunction::Callback callback)
{
	VernacularFunction *function = new VernacularFunction(callback);
	gc->AddObject(function, this);
	return function;
}

VernacularFunction *VernacularFunctionFactory::Allocate(VernacularObject *object, VernacularFunction *method)
{
	VernacularFunction *function = new VernacularFunction(object, method);
	gc->AddObject(function, this);
	return function;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
