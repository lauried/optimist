/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/vernacular/types/VernacularUserObjectFactory.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

VernacularUserObjectFactory::VernacularUserObjectFactory(VernacularGarbageCollector *gc)
{
	this->gc = gc;
	gc->AddListener(this);
}

VernacularUserObject *VernacularUserObjectFactory::Allocate(IGamecodeEngineObject *object, VernacularEngineObjectDescriptor *type)
{
	if (type == nullptr)
	{
		for (int i = 0; i < types.size(); i += 1)
		{
			if (types[i]->IsObjectThisType(object))
			{
				type = types[i];
				break;
			}
		}
		if (type == nullptr)
		{
			throw std::logic_error("VernacularUserObjectFactory unable to deduce user object type");
		}
	}

	VernacularUserObject *userObject = dynamic_cast<VernacularUserObject*>(object->implementationData);
	if (userObject == nullptr)
	{
		userObject = new VernacularUserObject(object, type);
		gc->AddObject(userObject, this);
	}
	return userObject;
}

VernacularUserObject *VernacularUserObjectFactory::Allocate(VernacularEngineObjectDescriptor *type, VernacularValue *parameters, int numParameters)
{
	GamecodeParameterList gamecodeParameters;
	for (int i = 0; i < numParameters; i += 1)
	{
		gamecodeParameters.push_back(parameters[i]);
	}
	// TODO: Deal with the second parameter (expecting an IGamecode) being null (because we don't have access to it here).
	// Check if anything needs to know the gamecode in its constructor.
	IGamecodeEngineObject *object = type->Construct(&gamecodeParameters, nullptr);
	return Allocate(object, type);
}

void VernacularUserObjectFactory::OnCollect(IVernacularCollectable* object)
{
	// Do nothing. We're not responsible for deleting the object, and we don't
	// need to know anything here.
}

void VernacularUserObjectFactory::GetChildren(std::vector<IVernacularCollectable*> *objects)
{
	for (int i = 0; i < types.size(); i += 1)
	{
		types[i]->GetChildren(objects);
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
