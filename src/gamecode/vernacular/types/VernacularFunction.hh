/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <map>
#include <vector>
#include <sstream>
#include <iostream>
#include <functional>
#include "gamecode/GamecodeValue.hh"
#include "gamecode/vernacular/types/VernacularValue.hh"
#include "gamecode/vernacular/interpreter/VernacularBytecode.hh"
#include "gamecode/vernacular/interpreter/VernacularGarbageCollector.hh"
#include "gamecode/vernacular/interpreter/VernacularLiterals.hh"
#include "log/ILogTarget.hh"
#include "gamecode/GamecodeEngineObjectDescriptor.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class VernacularGlobals;
class IVernacularEnvironment;
class VernacularThread;

// The function represents the code that the VernacularInterpreter will run.
// Its data is only accessed inside Vernacular.
// Thus, it's safe for it to have its data fairly exposed.

class VernacularFunction : public IGamecodeFunction, public IVernacularCollectable {
public:
	typedef std::function<VernacularValue(VernacularValue*, int, IVernacularEnvironment*)> Callback;

	struct DebugInfo {
		int row, col;
		DebugInfo() : row(-1), col(-1) {}
		DebugInfo(int row, int col) : row(row), col(col) {}
	};

	VernacularFunction() :
		boundObject(nullptr),
		boundMethod(nullptr),
		objectMethodType(nullptr),
		objectMethodIndex(0),
		globalReferencesMapped(false),
		currentGlobalMapTarget(nullptr),
		localAddressSpace(0),
		numParameters(0),
		numIterators(0)
	{
		type = T_BYTECODE;
	}

	VernacularFunction(IGamecodeEngineObjectDescriptor *engineObjectDescriptor, int index = -1)
		: VernacularFunction()
	{
		type = T_OBJECT_METHOD;
		objectMethodType = engineObjectDescriptor;
		objectMethodIndex = index;
	}

	VernacularFunction(IGamecodeAdapter *gamecodeAdapter, int index)
		: VernacularFunction()
	{
		type = T_ADAPTER_METHOD;
		adapterMethodType = gamecodeAdapter;
		adapterMethodIndex = index;
	}

	VernacularFunction(Callback function)
		: VernacularFunction()
	{
		type = T_CALLBACK;
		functionCallback = function;
	}

	VernacularFunction(VernacularObject *object, VernacularFunction *method)
		: VernacularFunction()
	{
		type = T_BOUND;
		boundObject = object;
		boundMethod = method;
	}

	~VernacularFunction();

	// The rest of the state refers to an interpreted bytecode function.
	std::string name; // when assigned to an identifier
	std::string filename;
	std::vector<VernacularBytecode> bytecode;
	std::vector<DebugInfo> debugInfo; // must match bytecode in length

	// Store global references.
	// We hang on to global references even after they've been mapped, so that
	// To delete (unset) a global variable, we remove it and then set all
	// functions to have unmapped global references.
	std::map<std::string, VernacularBytecode> globalReferences;
	bool globalReferencesMapped;
	VernacularGlobals *currentGlobalMapTarget; // globals object we're currently mapped to.

	// Store function globals (literals).
	VernacularLiterals literals;

	int localAddressSpace;
	int numParameters; // number of parameters the function expects
	int numIterators; // number of simultaneous iterators the function uses

	/**
	 * Maps the functions addresses to globals. Mapping must be successful
	 * before the function can be run. Returns true if mapping was successful,
	 * else false.
	 */
	bool MapGlobals(VernacularGlobals *globals, ILogTarget *log = nullptr);

	/**
	 * Creates and maps globals for all our global references.
	 * This should only be done for the root function loaded from a file.
	 */
	void CreateAndMapGlobals(VernacularGlobals *globals);

	// IGamecodeFunction.
	// TODO: Investigate removing this method from the interface.
	// Instead, we'd pass a function object to a run method on the gamecode
	// implementation.
	// We'd also want to set the 'this' object.
	GamecodeValue CallFunction(std::string &name, std::vector<GamecodeValue> &values, IGamecode* gamecode, GamecodeValue thisObject);

	// IVernacularCollectable
	void GetChildren(std::vector<IVernacularCollectable*> *objects);

	// Debug help
	void Dump(std::ostream *ss, bool recursive = false);
	void DumpLiterals(std::ostream *ss);

	// Query the function type.

	bool IsObjectMethod() const { return type == T_OBJECT_METHOD; }
	bool IsObjectConstructor() const { return objectMethodIndex == -1; }
	bool IsAdapterMethod() const { return type == T_ADAPTER_METHOD; }
	bool IsCallback() const { return type == T_CALLBACK; }
	bool IsBytecode() const { return type == T_BYTECODE; }
	bool IsBound() const { return type == T_BOUND; }

	IGamecodeEngineObjectDescriptor* GetObjectMethodType() const { return objectMethodType; }
	int GetObjectMethodIndex() const { return objectMethodIndex; }

	IGamecodeAdapter* GetAdapterMethodType() const { return adapterMethodType; }
	int GetAdapterMethodIndex() const { return adapterMethodIndex; }

	Callback GetCallback() const { return functionCallback; }

	/// If set, then the function binds an object and method.
	VernacularObject *boundObject;
	VernacularFunction *boundMethod;

private:
	enum Type {
		T_BOUND,
		T_BOUND_CLASS,
		T_OBJECT_METHOD,
		T_ADAPTER_METHOD,
		T_CALLBACK,
		T_BYTECODE
	};

	Type type;

	void RemapAddress(VernacularBytecode from, VernacularBytecode to);

	/// If set, then the function represents an engine object method.
	IGamecodeEngineObjectDescriptor *objectMethodType;
	int objectMethodIndex; ///< -1 is the constructor.

	/// If set, then the function represents an adapter method.
	/// There's no real need for a 'this' object to be involved.
	IGamecodeAdapter *adapterMethodType;
	int adapterMethodIndex;

	/// If not empty, then the function represents a callback.
	/// Check by doing something like if (func->functionCallback) to invoke
	/// conversion to bool.
	Callback functionCallback;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
