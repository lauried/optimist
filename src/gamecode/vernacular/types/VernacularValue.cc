/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/vernacular/types/VernacularValue.hh"
#include <cmath>
#include <functional>
#include <cstring>
#include "gamecode/vernacular/types/VernacularString.hh"
#include "gamecode/vernacular/types/VernacularStringFactory.hh"
#include "gamecode/vernacular/types/VernacularObject.hh"
#include "gamecode/vernacular/types/VernacularFunction.hh"
#include "gamecode/vernacular/types/VernacularUserObject.hh"
#include "gamecode/vernacular/interpreter/IVernacularEnvironment.hh"
#include "gamecode/vernacular/interpreter/IVernacularCollectable.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

static double StringToNumber(const char *number)
{
	size_t length = strlen(number);
	if (length > 2 && number[0] == '0')
	{
		if (number[1] == 'x')
		{
			int value = 0;
			for (int i = 2; i < length; i += 1)
			{
				value = value << 4;
				if (number[i] >= '0' && number[i] <= '9')
				{
					value += number[i] - '0';
				}
				else if (number[i] >= 'a' && number[i] <= 'f')
				{
					value += 10 + number[i] - 'a';
				}
				else if (number[i] >= 'A' && number[i] <= 'F')
				{
					value += 10 + number[i] - 'A';
				}
				else
				{
					return 0;
				}
			}
			return (double)value;
		}
		else if (number[1] == 'b')
		{
			int value = 0;
			for (int i = 2; i < length; i += 1)
			{
				value = value << 1;
				if (number[i] == '1')
				{
					value += 1;
				}
				else if (number[i] != '0')
				{
					return 0;
				}
			}
			return (double)value;
		}
	}
	try
	{
		return std::stod(number);
	}
	catch (std::invalid_argument &e)
	{
	}
	return 0;
}


bool VernacularValue::Equals(const VernacularValue &other) const
{
	if (other.type != type)
	{
		return false;
	}

	switch (type)
	{
	case GamecodeValue::T_Null:
		return true;
	case GamecodeValue::T_Bool:
		return value.v_double == other.value.v_double;
	case GamecodeValue::T_Number:
		return value.v_double == other.value.v_double;
	case GamecodeValue::T_String:
		{
			VernacularString *vsthis = dynamic_cast<VernacularString*>(value.v_string);
			VernacularString *vsother = dynamic_cast<VernacularString*>(other.value.v_string);
			if (vsthis != nullptr && vsother != nullptr)
			{
				return vsthis->Equals(vsother);
			}
		}
		break;
	case GamecodeValue::T_Table:
		return value.v_table == other.value.v_table;
	case GamecodeValue::T_Function:
		return value.v_function == other.value.v_function;
	case GamecodeValue::T_Engine:
		return value.v_engine == other.value.v_engine;
	}
	return false;
}

VernacularString* VernacularValue::GetVernacularString()
{
	if (type != GamecodeValue::T_String)
	{
		return nullptr;
	}
	return dynamic_cast<VernacularString*>(value.v_string);
}

VernacularObject* VernacularValue::GetVernacularObject()
{
	if (type != GamecodeValue::T_Table)
	{
		return nullptr;
	}
	return dynamic_cast<VernacularObject*>(value.v_table);
}

VernacularFunction* VernacularValue::GetVernacularFunction()
{
	if (type != GamecodeValue::T_Function)
	{
		return nullptr;
	}
	return dynamic_cast<VernacularFunction*>(value.v_function);
}

VernacularUserObject* VernacularValue::GetVernacularUserObject()
{
	if (type != GamecodeValue::T_Engine)
	{
		return nullptr;
	}
	IGamecodeEngineObject *engine = dynamic_cast<IGamecodeEngineObject*>(value.v_engine);
	if (engine == nullptr)
	{
		return nullptr;
	}
	return dynamic_cast<VernacularUserObject*>(engine->implementationData);
}

IVernacularCollectable* VernacularValue::GetVernacularCollectable()
{
	switch (GetType())
	{
	case GamecodeValue::T_String:
		return GetVernacularString();
	case GamecodeValue::T_Function:
		return GetVernacularFunction();
	case GamecodeValue::T_Table:
		return GetVernacularObject();
	case GamecodeValue::T_Engine:
		return GetVernacularUserObject();
	}
	return nullptr;
}

bool VernacularValue::GetAsBool()
{
	switch (type)
	{
	case T_Null:
		return false;
	case T_Bool:
	case T_Number:
		return (value.v_double != 0);
	case T_String:
	case T_Table:
	case T_Function:
	case T_Engine:
		return true;
	}
	throw std::logic_error("Forgot to handle a data type");
}

double VernacularValue::GetAsNumber()
{
	switch (type)
	{
	case T_Null:
		return 0;
	case T_Bool:
	case T_Number:
		return value.v_double;
	case T_Table:
	case T_Function:
	case T_Engine:
		return 0;
	}
	return StringToNumber(GetVernacularString()->Get());
}

std::string VernacularValue::ToString()
{
	switch (type)
	{
	case T_Null:
		return "null";
	case GamecodeValue::T_Bool:
		if (GetBool())
		{
			return "true";
		}
		else
		{
			return "false";
		}
	case GamecodeValue::T_Number:
		return String::ToString(GetDouble());
	case GamecodeValue::T_String:
		return GetVernacularString()->Get();
	case GamecodeValue::T_Table:
		return std::string("table:") + String::ToString(GetVernacularObject());
	case GamecodeValue::T_Function:
		return std::string("function:") + String::ToString(GetVernacularFunction());
	case GamecodeValue::T_Engine:
		return std::string("user:") + String::ToString(GetVernacularUserObject());
	}
	return std::string("");
}

VernacularValue VernacularValue::ConvertToString(IVernacularEnvironment *environment)
{
	VernacularValue result;
	switch (GetType())
	{
	case GamecodeValue::T_Null:
		result.SetString(environment->GetStringFactory()->GetConstNull());
		break;
	case GamecodeValue::T_Bool:
		if (GetBool())
		{
			result.SetString(environment->GetStringFactory()->GetConstTrue());
		}
		else
		{
			result.SetString(environment->GetStringFactory()->GetConstFalse());
		}
		break;
	case GamecodeValue::T_Number:
		result.SetString(environment->GetStringFactory()->Allocate(String::ToString(GetDouble())));
		break;
	case GamecodeValue::T_String:
		result = *this;
		break;
	case GamecodeValue::T_Table:
		result.SetString(environment->GetStringFactory()->Allocate(std::string("table:") + String::ToString(GetVernacularObject())));
		break;
	case GamecodeValue::T_Function:
		result.SetString(environment->GetStringFactory()->Allocate(std::string("function:") + String::ToString(GetVernacularFunction())));
		break;
	case GamecodeValue::T_Engine:
		result.SetString(environment->GetStringFactory()->Allocate(std::string("user:") + String::ToString(GetVernacularUserObject())));
		break;
	}
	return result;
}

void VernacularValue::SetNull()
{
	type = T_Null;
}

void VernacularValue::SetTrue()
{
	type = T_Bool;
	value.v_double = 1;
}

void VernacularValue::SetFalse()
{
	type = T_Bool;
	value.v_double = 0;
}

void VernacularValue::SetBool(bool val)
{
	type = T_Bool;
	value.v_double = val ? 1 : 0;
}

void VernacularValue::SetNumeric(std::string number)
{
	SetDouble(StringToNumber(number.c_str()));
}

void VernacularValue::SetString(VernacularString *stringObj)
{
	type = GamecodeValue::T_String;
	value.v_string = stringObj;
}

void VernacularValue::SetFunction(VernacularFunction *funcObj)
{
	type = GamecodeValue::T_Function;
	value.v_function = funcObj;
}

void VernacularValue::SetObject(VernacularObject *object)
{
	type = GamecodeValue::T_Table;
	value.v_table = object;
}

size_t VernacularValue::Hash() const
{
	size_t hashVal = 0;
	switch (type)
	{
		case T_Bool:
		case T_Number:
			hashVal = *(size_t*)&(value.v_double);
			break;
		case T_String:
			hashVal = (size_t)value.v_string;
			break;
		case T_Table:
			hashVal = (size_t)value.v_table;
			break;
		case T_Function:
			hashVal = (size_t)value.v_function;
			break;
		case T_Engine:
			hashVal = (size_t)value.v_engine;
			break;
	}

	if (sizeof(size_t) >= sizeof(uint64_t))
	{
		hashVal = (hashVal * 6364136223846793005 + 1442695040888963407) & 0xffffffffffffffff;
	}
	else
	{
		hashVal = (hashVal * 1103515245 + 12345) & 0xffffffff;
	}

	return hashVal;
}

//-----------------------------------------------------------------------------
// Operators
//-----------------------------------------------------------------------------

void VernacularValue::OpDotSet(VernacularValue &key, VernacularValue &value, IVernacularEnvironment *environment, bool isThis)
{
	switch (type)
	{
	case T_Table:
		GetVernacularObject()->Set(key, value, environment, isThis);
		return;
	case T_Engine:
		GetVernacularUserObject()->SetKey(key, value);
		return;
	}
	environment->ThrowException("Left of dot operator must be object type");
}

void VernacularValue::OpDotSetMeta(VernacularValue &key, VernacularValue &value, int meta, IVernacularEnvironment *environment, bool isThis)
{
	switch (type)
	{
	case T_Table:
		if (meta == 0)
		{
			GetVernacularObject()->Set(key, value, environment, isThis, VernacularObjectIndex::FL_NONE);
		}
		if (meta == 1)
		{
			GetVernacularObject()->Set(key, value, environment, isThis, VernacularObjectIndex::FL_PRIVATE);
		}
		else if (meta == 2)
		{
			GetVernacularObject()->Set(key, value, environment, isThis, VernacularObjectIndex::FL_READONLY);
		}
		return;
	}
	environment->ThrowException("Left of dot operator must be table type when using assignment modifiers");
}

VernacularValue VernacularValue::OpDotGet(VernacularValue &key, IVernacularEnvironment *environment, bool isThis)
{
	switch (type)
	{
	case T_Null:
		return VernacularValue();
	case T_Table:
		return GetVernacularObject()->Get(key, environment, isThis);
	case T_Engine:
		return GetVernacularUserObject()->GetKey(key, environment);
	}
	environment->ThrowException("Left of dot operator must be object type");
	return VernacularValue();
}

VernacularValue VernacularValue::OpCopyOnWrite(IVernacularEnvironment *environment)
{
	switch (type)
	{
		case T_Table:
			return VernacularValue(GetVernacularObject()->CreateCopyOnWrite());
	}
	environment->ThrowException("Copy on write operator can only be performed on a table");
	return VernacularValue();
}

VernacularValue& VernacularValue::OpPower(VernacularValue &rhs, IVernacularEnvironment *environment)
{
	static VernacularValue result;
	result.type = T_Number;
	result.value.v_double = std::pow(GetAsNumber(), rhs.GetAsNumber());
	return result;
}

VernacularValue& VernacularValue::OpMult(VernacularValue &rhs, IVernacularEnvironment *environment)
{
	static VernacularValue result;
	result.type = T_Number;
	result.value.v_double = GetAsNumber() * rhs.GetAsNumber();
	return result;
}

VernacularValue& VernacularValue::OpLogAnd(VernacularValue &rhs, IVernacularEnvironment *environment)
{
	static VernacularValue result;
	result.type = T_Bool;
	result.value.v_double = (GetAsBool() && rhs.GetAsBool()) ? 1 : 0;
	return result;
}

VernacularValue& VernacularValue::OpBitAnd(VernacularValue &rhs, IVernacularEnvironment *environment)
{
	static VernacularValue result;
	result.type = T_Number;
	result.value.v_double = (double)(((uint64_t)GetAsNumber() & (uint64_t)rhs.GetAsNumber()) & NUMBER_BITMASK);
	return result;
}

VernacularValue& VernacularValue::OpLogOr(VernacularValue &rhs, IVernacularEnvironment *environment)
{
	static VernacularValue result;
	result.type = T_Bool;
	result.value.v_double = (GetAsBool() || rhs.GetAsBool()) ? 1 : 0;
	return result;
}

VernacularValue& VernacularValue::OpBitOr(VernacularValue &rhs, IVernacularEnvironment *environment)
{
	static VernacularValue result;
	result.type = T_Number;
	result.value.v_double = (double)(((uint64_t)GetAsNumber() | (uint64_t)rhs.GetAsNumber()) & NUMBER_BITMASK);
	return result;
}

VernacularValue& VernacularValue::OpEqual(VernacularValue &rhs, IVernacularEnvironment *environment)
{
	static VernacularValue result;
	result.type = T_Bool;
	result.value.v_double = Equals(rhs) ? 1 : 0;
	return result;
}

VernacularValue& VernacularValue::OpNotEqual(VernacularValue &rhs, IVernacularEnvironment *environment)
{
	static VernacularValue result;
	result.type = T_Bool;
	result.value.v_double = Equals(rhs) ? 0 : 1;
	return result;
}

template <class T>
VernacularValue& VernacularValue::OpCompare(VernacularValue &rhs, IVernacularEnvironment *environment)
{
	static VernacularValue result;
	static T comp;
	if (type != rhs.type)
	{
		environment->ThrowException("Left and right of comparison operators must be of the same type");
		result.type = T_Null;
		return result;
	}
	switch (type)
	{
	case T_Number:
		result.type = T_Bool;
		result.value.v_double = (comp(value.v_double, rhs.value.v_double)) ? 1 : 0;
		return result;
		break;
	case T_String:
		result.type = T_Bool;
		result.value.v_double = (comp(GetVernacularString()->CompareTo(rhs.GetVernacularString()), 0)) ? 1 : 0;
		return result;
		break;
	}
	result.type = T_Null;
	environment->ThrowException("Left and right of comparison operators must be comparable (number, string)");
	return result;
}

VernacularValue& VernacularValue::OpGreaterEqual(VernacularValue &rhs, IVernacularEnvironment *environment)
{
	return OpCompare<std::greater_equal<double>>(rhs, environment);
}

VernacularValue& VernacularValue::OpLessEqual(VernacularValue &rhs, IVernacularEnvironment *environment)
{
	return OpCompare<std::less_equal<double>>(rhs, environment);
}

VernacularValue& VernacularValue::OpGreater(VernacularValue &rhs, IVernacularEnvironment *environment)
{
	return OpCompare<std::greater<double>>(rhs, environment);
}

VernacularValue& VernacularValue::OpLess(VernacularValue &rhs, IVernacularEnvironment *environment)
{
	return OpCompare<std::less<double>>(rhs, environment);
}

VernacularValue& VernacularValue::OpBitRight(VernacularValue &rhs, IVernacularEnvironment *environment)
{
	static VernacularValue result;
	result.type = T_Number;
	result.value.v_double = (double)(((uint64_t)GetAsNumber() >> (uint64_t)rhs.GetAsNumber()) & NUMBER_BITMASK);
	return result;
}

VernacularValue& VernacularValue::OpBitLeft(VernacularValue &rhs, IVernacularEnvironment *environment)
{
	static VernacularValue result;
	result.type = T_Number;
	result.value.v_double = (double)(((uint64_t)GetAsNumber() << (uint64_t)rhs.GetAsNumber()) & NUMBER_BITMASK);
	return result;
}

VernacularValue& VernacularValue::OpBitXor(VernacularValue &rhs, IVernacularEnvironment *environment)
{
	static VernacularValue result;
	result.type = T_Number;
	result.value.v_double = (double)(((uint64_t)GetAsNumber() ^ (uint64_t)rhs.GetAsNumber()) & NUMBER_BITMASK);
	return result;
}

VernacularValue& VernacularValue::OpMod(VernacularValue &rhs, IVernacularEnvironment *environment)
{
	static VernacularValue result;
	result.type = T_Number;
	result.value.v_double = std::fmod(GetAsNumber(), rhs.GetAsNumber());
	return result;
}

VernacularValue& VernacularValue::OpDiv(VernacularValue &rhs, IVernacularEnvironment *environment)
{
	static VernacularValue result;
	result.type = T_Number;
	result.value.v_double = GetAsNumber() / rhs.GetAsNumber();
	return result;
}

VernacularValue& VernacularValue::OpAdd(VernacularValue &rhs, IVernacularEnvironment *environment)
{
	static VernacularValue result;
	result.type = T_Number;
	result.value.v_double = GetAsNumber() + rhs.GetAsNumber();
	return result;
}

VernacularValue& VernacularValue::OpSub(VernacularValue &rhs, IVernacularEnvironment *environment)
{
	static VernacularValue result;
	result.type = T_Number;
	result.value.v_double = GetAsNumber() - rhs.GetAsNumber();
	return result;
}

VernacularValue& VernacularValue::OpLogNot(IVernacularEnvironment *environment)
{
	static VernacularValue result;
	result.type = T_Bool;
	result.value.v_double = (GetAsBool()) ? 0 : 1;
	return result;
}

VernacularValue& VernacularValue::OpBitNot(IVernacularEnvironment *environment)
{
	static VernacularValue result;
	result.type = T_Number;
	result.value.v_double = (double)(~(uint64_t)GetAsNumber()  & NUMBER_BITMASK);
	return result;
}

VernacularValue& VernacularValue::OpNegate(IVernacularEnvironment *environment)
{
	static VernacularValue result;
	result.type = T_Number;
	result.value.v_double = -GetAsNumber();
	return result;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
