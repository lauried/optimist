/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <map>
#include <string>
#include "gamecode/vernacular/types/VernacularString.hh"
#include "gamecode/vernacular/interpreter/VernacularGarbageCollector.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// All data objects will be instantiated through a factory.

// String factory keeps track of de-duped strings, so we have to pass all string
// creation throuhg it so it can check whether a given string already exists and
// return that one instead.

// It needs to listen to the garbage collector so it can remove strings that
// are about to be deleted by GC.

// It also needs to know the GC in order to add new strings to it.

class VernacularStringFactory : public IVernacularGCListener, public IVernacularGCRoot {
public:
	VernacularStringFactory(VernacularGarbageCollector *gc);
	~VernacularStringFactory();

	VernacularString *Allocate(std::string str);
	VernacularString *Allocate(const char *str);
	VernacularString *Concatenate(VernacularString *start, VernacularString *end);

	// IVernacularGCListener
	void OnCollect(IVernacularCollectable* object);
	void Collect(IVernacularCollectable* object) { delete object; }

	// IVernacularGCRoot (for const strings)
	void GetChildren(std::vector<IVernacularCollectable*> *objects);

	// Constants we keep track of:
	VernacularString *GetConstNull() { return constNull; }
	VernacularString *GetConstTrue() { return constTrue; }
	VernacularString *GetConstFalse() { return constFalse; }
	VernacularString *GetConstBoolean() { return constBoolean; }
	VernacularString *GetConstNumber() { return constNumber; }
	VernacularString *GetConstString() { return constString; }
	VernacularString *GetConstTable() { return constTable; }
	VernacularString *GetConstFunction() { return constFunction; }
	VernacularString *GetConstUser() { return constUser; }
	VernacularString *GetConstReadonly() { return constReadonly; }
	VernacularString *GetConstPrivate() { return constPrivate; }

private:
	VernacularGarbageCollector *gc;
	std::map<std::string, VernacularString*> strings;

	VernacularString
		*constNull,
		*constTrue,
		*constFalse,
		*constBoolean,
		*constNumber,
		*constString,
		*constTable,
		*constFunction,
		*constUser,
		*constReadonly,
		*constPrivate;

	VernacularString *SetupConst(std::string value);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
