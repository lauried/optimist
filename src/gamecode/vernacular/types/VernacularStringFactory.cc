/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/vernacular/types/VernacularStringFactory.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

VernacularStringFactory::VernacularStringFactory(VernacularGarbageCollector *gc)
{
	this->gc = gc;
	gc->AddListener(this);

	constNull = SetupConst("null");
	constTrue = SetupConst("true");
	constFalse = SetupConst("false");
	constBoolean = SetupConst("boolean");
	constNumber = SetupConst("number");
	constString = SetupConst("string");
	constTable = SetupConst("table");
	constFunction = SetupConst("function");
	constUser = SetupConst("user");
	constReadonly = SetupConst("readonly");
	constPrivate = SetupConst("constprivate");
}

VernacularStringFactory::~VernacularStringFactory()
{
	delete constNull;
	delete constTrue;
	delete constFalse;
	delete constBoolean;
	delete constNumber;
	delete constString;
	delete constTable;
	delete constFunction;
	delete constUser;
	delete constReadonly;
	delete constPrivate;
}

VernacularString* VernacularStringFactory::SetupConst(std::string str)
{
	VernacularString *string = new VernacularString(str);
	strings.insert(std::make_pair(str, string));
	gc->AddObject(string, this);
	return string;
}

VernacularString *VernacularStringFactory::Allocate(std::string str)
{
	if (str.length() <= VernacularString::key_size)
	{
		auto it = strings.find(str);
		if (it != strings.end())
		{
			return it->second;
		}
		VernacularString *string = new VernacularString(str);
		strings.insert(std::make_pair(str, string));
		gc->AddObject(string, this);
		return string;
	}

	VernacularString *string = new VernacularString(str);
	gc->AddObject(string, this);
	return string;
}

VernacularString *VernacularStringFactory::Allocate(const char *str)
{
	return Allocate(std::string(str));
}

VernacularString *VernacularStringFactory::Concatenate(VernacularString *start, VernacularString *end)
{
	int combinedLength = start->Length() + end->Length();
	if (combinedLength <= VernacularString::key_size)
	{
		std::vector<char> buffer(combinedLength + 1);
		start->CopyToBuffer(&buffer[0]);
		end->CopyToBuffer(&buffer[0] + start->Length());
		buffer[combinedLength] = '\0';
		return Allocate(&buffer[0]);
	}

	VernacularString *string = new VernacularString(start, end);
	gc->AddObject(string, this);
	return string;
}

void VernacularStringFactory::OnCollect(IVernacularCollectable* object)
{
	VernacularString *string = dynamic_cast<VernacularString*>(object);
	if (string == nullptr)
	{
		throw std::logic_error("Tried to collect object which wasn't a vernacular string");
	}
	if (string->Length() <= VernacularString::key_size)
	{
		strings.erase(strings.find(std::string(string->Get())));
	}
}

void VernacularStringFactory::GetChildren(std::vector<IVernacularCollectable*> *objects)
{
	objects->push_back(constNull);
	objects->push_back(constTrue);
	objects->push_back(constFalse);
	objects->push_back(constBoolean);
	objects->push_back(constNumber);
	objects->push_back(constString);
	objects->push_back(constTable);
	objects->push_back(constFunction);
	objects->push_back(constUser);
	objects->push_back(constReadonly);
	objects->push_back(constPrivate);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
