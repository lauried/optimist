/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/vernacular/types/VernacularFunction.hh"
#include "gamecode/vernacular/interpreter/VernacularGlobals.hh"
#include "gamecode/vernacular/types/VernacularObject.hh"
#include "gamecode/vernacular/types/VernacularString.hh"
#include "gamecode/vernacular/types/VernacularUserObject.hh"
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

VernacularFunction::~VernacularFunction()
{
}

GamecodeValue VernacularFunction::CallFunction(std::string &name, std::vector<GamecodeValue> &values, IGamecode* gamecode, GamecodeValue thisObject)
{
	return GamecodeValue();
}

void VernacularFunction::GetChildren(std::vector<IVernacularCollectable*> *objects)
{
	for (int i = 0; i < literals.values.size(); i += 1)
	{
		IVernacularCollectable *collectable = literals.values[i].GetVernacularCollectable();
		if (collectable != nullptr)
		{
			objects->push_back(collectable);
		}
	}

	if (type == T_BOUND || type == T_BOUND_CLASS)
	{
		objects->push_back(boundObject);
		objects->push_back(boundMethod);
	}
}

bool VernacularFunction::MapGlobals(VernacularGlobals *globals, ILogTarget *log)
{
	if (globals == currentGlobalMapTarget)
	{
		return true;
	}
	currentGlobalMapTarget = nullptr;
	for (auto it = globalReferences.begin(); it != globalReferences.end(); ++it)
	{
		int address = globals->FindAddress(it->first);
		if (address >= 0)
		{
			VernacularBytecode bytecode(VernacularBytecode::T_GLOBAL, address);
			RemapAddress(it->second, bytecode);
			it->second.Set(bytecode);
		}
		else
		{
			if (log != nullptr)
			{
				log->Start(ILogTarget::ERROR)
					->Write("Unable to map global named '")
					->Write(it->first)
					->Write("'")
					->End();
			}
			return false;
		}
	}
	currentGlobalMapTarget = globals;
	return true;
}

void VernacularFunction::CreateAndMapGlobals(VernacularGlobals *globals)
{
	for (auto it = globalReferences.begin(); it != globalReferences.end(); ++it)
	{
		int address = globals->Create(it->first);
		VernacularBytecode bytecode(VernacularBytecode::T_GLOBAL, address);
		RemapAddress(it->second, bytecode);
		it->second.Set(bytecode);
	}
	currentGlobalMapTarget = globals;
}

void VernacularFunction::RemapAddress(VernacularBytecode from, VernacularBytecode to)
{
	for (int i = 0; i < bytecode.size(); i += 1)
	{
		if (bytecode[i].Equals(from))
		{
			bytecode[i].Set(to);
		}
	}
}

void VernacularFunction::Dump(std::ostream *ss, bool recursive)
{
	*ss << "func " << this << ", " << bytecode.size() << " instructions, "
		<< numParameters << " parameters, "
		<< localAddressSpace << " locals:"
		<< std::endl;
	for (int i = 0; i < bytecode.size(); i += 1)
	{
		*ss << i << " (" << debugInfo[i].row << "," << debugInfo[i].col << "): "
			<< bytecode[i].ToString();
		if (bytecode[i].GetType() == VernacularBytecode::T_FUNCTION && bytecode[i].GetValue() >= 0 && bytecode[i].GetValue() < literals.values.size())
		{
			*ss << " (" << literals.values[bytecode[i].GetValue()].ToString() << ")";
		}
		*ss << std::endl;
	}

	if (recursive)
	{
		for (int i = 0; i < literals.values.size(); i += 1)
		{
			if (literals.values[i].GetType() == GamecodeValue::T_Function)
			{
				literals.values[i].GetVernacularFunction()->Dump(ss, true);
			}
		}
	}
}

void VernacularFunction::DumpLiterals(std::ostream *ss)
{
	*ss << "func " << this << " literals:" << std::endl;
	for (int i = 0; i < literals.values.size(); i += 1)
	{
		*ss << i << ": " << literals.values[i].ToString() << std::endl;
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
