/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/vernacular/types/VernacularString.hh"
#include <cstring>
#include <iostream>
#include "library/text/String.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

VernacularString::VernacularString(const char *str)
{
	length = strlen(str);
	unicodeLength = String::UTF8Length(str, length);

	data = new char[length + 1];
	strncpy(data, str, length);
	data[length] = '\0';

	start = nullptr;
	end = nullptr;
}

VernacularString::VernacularString(std::string str)
{
	length = str.length();

	data = new char[length + 1];
	strncpy(data, str.c_str(), length);
	data[length] = '\0';

	unicodeLength = String::UTF8Length(data, length);

	start = nullptr;
	end = nullptr;
}

VernacularString::VernacularString(VernacularString *start, VernacularString *end)
{
	length = start->length + end->length;
	unicodeLength = start->unicodeLength + end->unicodeLength;
	if (length > key_size)
	{
		// If we're longer than the maximum de-duping length, then we can
		// become a composite string.
		this->start = start;
		this->end = end;
		data = nullptr;
	}
	else
	{
		// If we're too short to be composite, then so are our child strings.
		data = new char[length + 1];
		strncpy(data, start->data, start->length);
		strncpy(data + start->length, end->data, end->length);
		data[length] = '\0';
		this->start = nullptr;
		this->end = nullptr;
	}
}

VernacularString::~VernacularString()
{
	//Combine();
	//std::cout << "Deleting '" << data << "'" << std::endl;
}

//-----------------------------------------------------------------------------
// IGamecode
//-----------------------------------------------------------------------------

const char* VernacularString::Get()
{
	Combine();
	return data;
}

//-----------------------------------------------------------------------------
// IVernacularCollectable
//-----------------------------------------------------------------------------

void VernacularString::GetChildren(std::vector<IVernacularCollectable*> *objects)
{
	if (start != nullptr)
	{
		objects->push_back(start);
	}
	if (end != nullptr)
	{
		objects->push_back(end);
	}
}

//-----------------------------------------------------------------------------
// String specific
//-----------------------------------------------------------------------------

bool VernacularString::Equals(VernacularString *other)
{
	if (other->length != length)
	{
		return false;
	}
	if (length <= key_size)
	{
		return this == other;
	}
	Combine();
	other->Combine();
	return strncmp(data, other->data, length) == 0;
}

int VernacularString::CompareTo(VernacularString *other)
{
	Combine();
	other->Combine();
	return strncmp(data, other->data, length);
}

void VernacularString::SubstringToBuffer(char *buffer, int offset, int length)
{
	if (offset < 0)
	{
		length += offset;
		offset = 0;
	}
	if (offset + length > this->length)
	{
		length = this->length - offset;
	}
	if (length < 1)
	{
		return;
	}

	if (data != nullptr)
	{
		strncpy(buffer, data + offset, length);
		return;
	}
	if (offset < start->length)
	{
		start->SubstringToBuffer(buffer, offset, length);
	}
	if (offset + length >= start->length)
	{
		end->SubstringToBuffer(buffer + start->length, offset - start->length, length);
	}
}

int VernacularString::UnicodeSubstringToStream(std::stringstream *stream, int offset, int length)
{
	if (offset < 0)
	{
		length += offset;
		offset = 0;
	}
	if (offset + length > unicodeLength)
	{
		length = unicodeLength - offset;
	}
	if (length < 1)
	{
		return 0;
	}

	if (data != nullptr)
	{
		// first iterate unicode characters until we meet offset
		int count = 0;
		int byteOffset = 0;
		while (byteOffset < this->length && count < offset)
		{
			if (data[byteOffset] & 0xc0 == 0x80)
			{
				count += 1;
			}
			byteOffset += 1;
		}
		// next, iterate length, outputting characters
		count = 0;
		while (byteOffset < this->length)
		{
			if (data[byteOffset] & 0xc0 == 0x80)
			{
				count += 1;
			}
			if (count >= length)
			{
				break;
			}
			(*stream) << data[byteOffset];
			byteOffset += 1;
		}
	}

	int count = 0;
	if (offset < start->length)
	{
		count = start->UnicodeSubstringToStream(stream, offset, length);
	}
	length -= count;
	offset = 0;
	if (length > 0)
	{
		count += end->UnicodeSubstringToStream(stream, offset, length);
	}
	return count;
}

//-----------------------------------------------------------------------------
// Private
//-----------------------------------------------------------------------------

void VernacularString::Combine()
{
	if (data != nullptr)
	{
		return;
	}
	data = new char[length + 1];
	start->CopyToBuffer(data);
	end->CopyToBuffer(data + start->length);
	data[length] = '\0';
	start = nullptr;
	end = nullptr;
}

void VernacularString::CopyToBuffer(char *buffer)
{
	if (data != nullptr)
	{
		strncpy(buffer, data, length);
		return;
	}
	start->CopyToBuffer(buffer);
	end->CopyToBuffer(buffer + start->length);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
