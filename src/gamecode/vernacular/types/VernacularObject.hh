/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <unordered_map>
#include <vector>
#include "library/containers/LinkedList.hh"
#include "gamecode/vernacular/types/VernacularValue.hh"
#include "gamecode/GamecodeValue.hh"
#include "gamecode/vernacular/interpreter/VernacularGarbageCollector.hh"
#include "gamecode/vernacular/types/VernacularObjectIndex.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// TODO:
// Back up old class (for reference while working) and reimplement from interface.
// Wrap Index in our own class that adds iterator reference counts.
// Behaviours:
// - Iterator is forward and its construction chooses whether it's private access or not.
//   Check whether VernacularObject's iterator
// - Copy on write object can only be constructed from a literal.
//   The literal is a GC child so that the index never gets deleted.
// - Copy on write can become a normal instance by copying the index.
//   This is done on every write attempt.

/**
 * A vernacular object is an associative array with fast random access by key
 * but also maintaining the order in which keys were added when iterating.
 */
class VernacularObject : public IGamecodeTable, public IVernacularCollectable {
public:
	/**
	 * The linked list of key/value pairs.
	 * We do lookup using a separate map which is part of the table.
	 * This is safe because iterators never perform this kind of lookup.
	 */
	class Index : public VernacularObjectIndex {
	public:
		int iteratorCount; // number of iterators currently iterating
		int nextIncrement, nextDecrement;
		bool tableOwned; // whether or not it's owned by a table
		Index() : iteratorCount(0), tableOwned(true), nextIncrement(0), nextDecrement(0) {}
		inline void PushBack(VernacularValue value)
		{
			VernacularObjectIndex::PushBack(VernacularValue(nextIncrement++), value);
		}
		inline void PushBack(VernacularValue key, VernacularValue value)
		{
			VernacularObjectIndex::PushBack(key, value);
		}
		inline void PushFront(VernacularValue value)
		{
			VernacularObjectIndex::PushFront(VernacularValue(nextDecrement--), value);
		}
		inline void PushFront(VernacularValue key, VernacularValue value)
		{
			VernacularObjectIndex::PushFront(key, value);
		}
		inline void Set(VernacularValue key, VernacularValue value, IVernacularEnvironment *environment = nullptr, bool allowPrivate = false, int flags = 0)
		{
			if (key.IsNumber())
			{
				nextIncrement = key.GetInt() + 1;
			}
			VernacularObjectIndex::Set(key, value, environment, allowPrivate, flags);
		}
	};

	/**
	 * Iterates the index.
	 * This is purely readonly, and the index is required never to be modified.
	 * This is guaranteed behaviour of VernacularObject, which will disown and
	 * create a new index if the old one is in use when a write is required.
	 */
	class Iterator {
	public:
		/**
		 * Constructs with Key and Value pointing to the first node, which, if
		 * the list is empty, will be END. Use End() to check if this is the
		 * case.
		 */
		Iterator(Index *index, bool privateAccess = false);
		~Iterator() { Finished(); }

		/**
		 * Moves to the next key value pair.
		 * Returns false if the end has been reached, at which time Key() and
		 * Value() can no longer be used.
		 */
		bool Next();

		/**
		 * Returns true if we're at the end, else false.
		 */
		bool End();

		VernacularValue Key();
		VernacularValue Value();
		int Flags();

		/**
		 * Signals that this iterator no longer needs the index it is using,
		 * and will remove itself from the count, deleting it if it's the last
		 * iterator and the object it came from no longer owns it.
		 * This is called by Next(), or when exiting a function in the middle
		 * of iterating.
		 */
		void Finished();

	private:
		Index *index;
		VernacularObjectIndex::Iterator iterator;
		bool privateAccess;
	};

	typedef int IndexPoolType; // TODO: Later remove this from the factory class

	VernacularObject(IndexPoolType *pool);
	~VernacularObject();

	void BecomeLiteral();

	void Set(VernacularValue key, VernacularValue value, IVernacularEnvironment *environment = nullptr, bool isThis = false, int flags = 0);
	VernacularValue Get(VernacularValue key, IVernacularEnvironment *environment = nullptr, bool isThis = false);
	bool SetMetaFlags(VernacularValue key, Index::Flags flags);

	VernacularValue Back();
	void PushBack(VernacularValue key, VernacularValue value);
	void PushBack(VernacularValue value);
	VernacularValue PopBack();
	VernacularValue Front();
	void PushFront(VernacularValue key, VernacularValue value);
	void PushFront(VernacularValue value);
	VernacularValue PopFront();

	VernacularValue KeyForValue(VernacularValue &value);
	int Count();
	// Copies the keys and values from another object.
	void Copy(VernacularObject *object);
	// Copies the keys from another object as our values.
	void CopyKeys(VernacularObject *object);
	void Reverse();
	void Reindex(double start, double step);

	// TODO: Review IGamecodeTable interface.

	// IVernacularCollectable
	/**
	 * Implements IVernacularCollectable::GetChildren
	 */
	void GetChildren(std::vector<IVernacularCollectable*> *objects);

	// IGamecodeTable.
	/** /name
	 * Attempts to retrieve a non-existent key must return a null value.
	 * This can serve as a check for whether a key exists.
	 */
	//@{
	GamecodeValue Get(GamecodeValue key);
	//@}

	int NumKeys();
	GamecodeValue GetKey(int i);
	GamecodeValue GetValue(int i);

	void Set(GamecodeValue key, GamecodeValue value);

	/** /name
	 * Return true if the underlying implementation supports the relevant
	 * properties.
	 */
	//@{
	virtual bool AllowStringKey() { return true; }
	//@}

	inline Iterator GetIterator(bool privateAccess = false) { return Iterator(index, privateAccess); }
	inline Iterator * GetIteratorPointer(bool privateAccess = false) { return new Iterator(index, privateAccess); }

	inline VernacularObject* CreateCopyOnWrite() { return new VernacularObject(this); }

private:
	enum Flags {
		FL_LITERAL = 1, // we can never be written to
		FL_COPY_ON_WRITE = 2, // means that we are a copy
	};

	int flags;
	VernacularObject *copyOnWrite;
	Index *index;

	// copy-on-write constructor
	// If we need to do anything fancy, then make this a generic constructor.
	VernacularObject(VernacularObject *copyOnWrite);

	/**
	 * Checks if we need to copy the index to be able to write to it, and does
	 * so if we need to.
	 */
	inline void PerformWriteChecks()
	{
		if (flags & FL_LITERAL)
		{
			ThrowLiteralWriteException();
		}
		else if (flags & FL_COPY_ON_WRITE)
		{
			CopyOnWrite();
		}
		else if (index->iteratorCount > 0)
		{
			CopyIteratedIndex();
		}
	}

	inline void PerformNewIndexChecks()
	{
		if (flags & FL_LITERAL)
		{
			ThrowLiteralWriteException();
		}
	}

	void ThrowLiteralWriteException()
	{
		// Literals are never allowed to be written.
		throw std::logic_error("Attempted to write to a literal object");
	}

	void CopyOnWrite()
	{
		// Copy on write needs to make us a new index and clear our copy-on-write status.
		// Our previous index was owned by copyOnWrite, so it's safe to just replace it.
		index = CopyIndex(index);
		flags = flags - (flags & FL_COPY_ON_WRITE);
		copyOnWrite = nullptr;
	}

	void CopyIteratedIndex()
	{
		// When iterators are iterating our index and we need to write, disown
		// our current and use a copy of it.
		index->tableOwned = false;
		index = CopyIndex(index);
	}

	Index* CopyIndex(Index *original)
	{
		Index *newIndex = new Index();
		for (Iterator iterator = Iterator(original, true); !iterator.End(); iterator.Next())
		{
			newIndex->Set(iterator.Key(), iterator.Value(), nullptr, true, iterator.Flags());
		}
		newIndex->nextIncrement = original->nextIncrement;
		newIndex->nextDecrement = original->nextDecrement;
		return newIndex;
	}

	/**
	 * If there are iterators, disown the index, else delete it.
	 */
	void DeleteOrDisownIndex()
	{
		if (flags & FL_COPY_ON_WRITE)
		{
			return;
		}
		if (index->iteratorCount)
		{
			index->tableOwned = false;
		}
		else
		{
			delete index;
		}
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
