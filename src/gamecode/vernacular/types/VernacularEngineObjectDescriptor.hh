/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <map>
#include "gamecode/GamecodeEngineObjectDescriptor.hh"
#include "gamecode/vernacular/types/VernacularFunction.hh"
#include "gamecode/vernacular/types/VernacularFunctionFactory.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Decorator class for IGamecodeEngineObjectDescriptor
 */
class VernacularEngineObjectDescriptor : public IGamecodeEngineObjectDescriptor {
public:
	VernacularEngineObjectDescriptor(IGamecodeEngineObjectDescriptor *descriptor)
		: descriptor(descriptor)
		{}

	~VernacularEngineObjectDescriptor();

	void SetName(std::string name);
	std::string GetName();

	bool IsObjectThisType(IGamecodeEngineObject *object);

	bool HasProperty(const char *key);
	void SetProperty(IGamecodeEngineObject* object, const char *key, GamecodeValue val);
	GamecodeValue GetProperty(IGamecodeEngineObject* object, const char *key);
	GamecodeValue GetProperty(IGamecodeEngineObject* object, std::string key);

	std::vector<std::string>* GetMethods();
	bool HasMethod(const char *key);
	int MethodIndex(const char *key);
	int MethodIndex(std::string key);
	GamecodeValue CallMethod(IGamecodeEngineObject* object, const char *key, GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue CallMethod(IGamecodeEngineObject* object, int index, GamecodeParameterList *parameters, IGamecode *gamecode);
	void RenameMethod(int methodIndex, std::string newName);

	void GetChildObjects(IGamecodeEngineObject* object, std::vector<IGamecodeEngineObject*>* children);

	IGamecodeEngineObject* Construct(GamecodeParameterList *parms, IGamecode *gamecode);
	void Destruct(IGamecodeEngineObject *object);

	void GetChildren(std::vector<IVernacularCollectable*> *objects);

	// own methods
	IGamecodeEngineObjectDescriptor *GetDescriptor() { return descriptor; }

	VernacularFunction *GetFunction(int index, VernacularFunctionFactory *functionFactory);

private:
	IGamecodeEngineObjectDescriptor *descriptor;
	std::map<int, VernacularFunction*> methodObjects;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
