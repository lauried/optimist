/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <unordered_map>
#include "gamecode/vernacular/types/VernacularValue.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * An ordered associative array of vernacular values.
 * Here we implement the storage itself, without dealing with anything else.
 */
class VernacularObjectIndex {
public:
	enum Flags {
		FL_NONE = 0,
		FL_PRIVATE = 1,
		FL_READONLY = 2,
	};

	struct Node {
		VernacularValue value;
		VernacularValue key;
		Node *next;
		Node *prev;
		int flags;

		Node() : next(nullptr), prev(nullptr), flags(0) {}
		Node(VernacularValue key, VernacularValue value) : value(value), key(key), next(nullptr), prev(nullptr), flags(0) {}
	};

	// Doesn't do any kind of reference counting.
	// The object itself is responsible for that sort of thing.
	class Iterator {
	public:
		Iterator() : current(nullptr) {}
		Iterator(Node *current) : current(current) {}
		void Next();
		void NextAll();
		void Prev();
		void PrevAll();
		bool IsFront();
		bool IsBack();
		VernacularValue &Key();
		VernacularValue &Value();
		int Flags();

	private:
		Node *current;
	};

	VernacularObjectIndex();

	void Set(VernacularValue key, VernacularValue value, IVernacularEnvironment *environment = nullptr, bool allowPrivate = false, int flags = 0);
	void Unset(VernacularValue key, IVernacularEnvironment *environment = nullptr, bool allowPrivate = false, int flags = 0);
	VernacularValue Get(VernacularValue key, IVernacularEnvironment *environment = nullptr, bool allowPrivate = false);

	VernacularValue Back();
	void PushBack(VernacularValue key, VernacularValue value);
	VernacularValue PopBack();
	VernacularValue Front();
	void PushFront(VernacularValue key, VernacularValue value);
	VernacularValue PopFront();

	VernacularValue KeyForValue(VernacularValue &value);
	int Count();
	void Reverse();

	Iterator IteratorFront() { return Iterator(&front); }
	Iterator IteratorBack() { return Iterator(&back); }

private:
	// The standard guarantees that pointers to the objects in a map are safe
	// until the object is removed. So our linked list uses pointers between
	// the nodes in a map.
	// We must ensure that nodes are only linked and unlinked while they remain
	// inside the map.

	typedef std::unordered_map<VernacularValue, Node, VernacularValue::hash> NodeContainer;
	typedef NodeContainer::iterator NodeIterator;

	NodeContainer nodes;
	Node front;
	Node back;

	void PushNodeFront(Node *node);
	void PushNodeBack(Node *node);
	void UnlinkNode(Node *node);

	Node *FindNode(VernacularValue key);
	NodeIterator FindNodeIterator(VernacularValue key);
	Node* PushNodeFront(VernacularValue key, VernacularValue value);
	Node* PushNodeBack(VernacularValue key, VernacularValue value);
	void EraseNode(NodeIterator iterator);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

