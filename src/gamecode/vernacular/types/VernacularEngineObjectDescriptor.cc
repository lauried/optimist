/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/vernacular/types/VernacularEngineObjectDescriptor.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

VernacularEngineObjectDescriptor::~VernacularEngineObjectDescriptor()
{
}

void VernacularEngineObjectDescriptor::SetName(std::string name)
{
	descriptor->SetName(name);
}

std::string VernacularEngineObjectDescriptor::GetName()
{
	return descriptor->GetName();
}

bool VernacularEngineObjectDescriptor::IsObjectThisType(IGamecodeEngineObject *object)
{
	return descriptor->IsObjectThisType(object);
}

bool VernacularEngineObjectDescriptor::HasProperty(const char *key)
{
	return descriptor->HasProperty(key);
}

void VernacularEngineObjectDescriptor::SetProperty(IGamecodeEngineObject* object, const char *key, GamecodeValue val)
{
	descriptor->SetProperty(object, key, val);
}

GamecodeValue VernacularEngineObjectDescriptor::GetProperty(IGamecodeEngineObject* object, const char *key)
{
	return descriptor->GetProperty(object, key);
}

GamecodeValue VernacularEngineObjectDescriptor::GetProperty(IGamecodeEngineObject* object, std::string key)
{
	return descriptor->GetProperty(object, key);
}

std::vector<std::string>* VernacularEngineObjectDescriptor::GetMethods()
{
	return descriptor->GetMethods();
}

bool VernacularEngineObjectDescriptor::HasMethod(const char *key)
{
	return descriptor->HasMethod(key);
}

int VernacularEngineObjectDescriptor::MethodIndex(const char *key)
{
	return descriptor->MethodIndex(key);
}

int VernacularEngineObjectDescriptor::MethodIndex(std::string key)
{
	return descriptor->MethodIndex(key);
}

GamecodeValue VernacularEngineObjectDescriptor::CallMethod(IGamecodeEngineObject* object, const char *key, GamecodeParameterList *parameters, IGamecode *gamecode)
{
	return descriptor->CallMethod(object, key, parameters, gamecode);
}

GamecodeValue VernacularEngineObjectDescriptor::CallMethod(IGamecodeEngineObject* object, int index, GamecodeParameterList *parameters, IGamecode *gamecode)
{
	return descriptor->CallMethod(object, index, parameters, gamecode);
}

void VernacularEngineObjectDescriptor::RenameMethod(int methodIndex, std::string newName)
{
	descriptor->RenameMethod(methodIndex, newName);
}

void VernacularEngineObjectDescriptor::GetChildObjects(IGamecodeEngineObject* object, std::vector<IGamecodeEngineObject*>* children)
{
	descriptor->GetChildObjects(object, children);
}

IGamecodeEngineObject* VernacularEngineObjectDescriptor::Construct(GamecodeParameterList *parms, IGamecode *gamecode)
{
	return descriptor->Construct(parms, gamecode);
}

void VernacularEngineObjectDescriptor::Destruct(IGamecodeEngineObject *object)
{
	descriptor->Destruct(object);
}

void VernacularEngineObjectDescriptor::GetChildren(std::vector<IVernacularCollectable*> *objects)
{
	for (auto it = methodObjects.begin(); it != methodObjects.end(); ++it)
	{
		objects->push_back(it->second);
	}
}

VernacularFunction* VernacularEngineObjectDescriptor::GetFunction(int index, VernacularFunctionFactory *functionFactory)
{
	auto it = methodObjects.find(index);
	if (it != methodObjects.end())
	{
		return it->second;
	}
	VernacularFunction *function = functionFactory->Allocate(this, index);
	methodObjects.insert(std::make_pair(index, function));
	return function;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
