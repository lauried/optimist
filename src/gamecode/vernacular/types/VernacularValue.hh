/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gamecode/GamecodeValue.hh"
#include <ostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class VernacularString;
class VernacularObject;
class VernacularFunction;
class VernacularUserObject;
class IVernacularEnvironment;
class IVernacularCollectable;

/**
 * This is Vernacular's internal data type. It extends GamecodeValue.
 * For now, it can also use the same float/int for numerical storage.
 * Later we might abstract the numerical representation away from GCV, allowing
 * us to swap out float/int for double, or any other degree of precision.
 * Or we might update GCV to use doubles internally instead of Float+Int.
 */
class VernacularValue : public GamecodeValue {
public:
	VernacularValue() : GamecodeValue() {}
	VernacularValue(int i) : GamecodeValue(i) {}
	VernacularValue(double d) : GamecodeValue(d) {}
	VernacularValue(const GamecodeValue& other) : GamecodeValue(other) {}
	VernacularValue(const VernacularValue& other) : GamecodeValue(other) {}

	bool Equals(const VernacularValue &other) const;

	VernacularString* GetVernacularString();
	VernacularObject* GetVernacularObject();
	VernacularFunction* GetVernacularFunction();
	VernacularUserObject* GetVernacularUserObject();

	IVernacularCollectable* GetVernacularCollectable();

	bool GetAsBool(); ///< Performs a cast to boolean.
	double GetAsNumber(); ///< Performs a cast to number.
	std::string ToString(); ///< Standard language string conversion.

	VernacularValue ConvertToString(IVernacularEnvironment *environment);

	void SetNull();
	void SetTrue();
	void SetFalse();
	void SetBool(bool val);
	void SetNumeric(std::string number);
	void SetString(VernacularString *stringObj);
	void SetFunction(VernacularFunction *funcObj);
	void SetObject(VernacularObject *object);

	// Vernacular operator related methods.
	// TODO: All these should take an environment parameter in case the result
	// of the operation needs to be allocated.
	void OpDotSet(VernacularValue &key, VernacularValue &value, IVernacularEnvironment *environment, bool isThis);
	void OpDotSetMeta(VernacularValue &key, VernacularValue &value, int meta, IVernacularEnvironment *environment, bool isThis);
	VernacularValue OpDotGet(VernacularValue &key, IVernacularEnvironment *environment, bool isThis);

	VernacularValue OpCopyOnWrite(IVernacularEnvironment *environment);

	VernacularValue& OpPower(VernacularValue &rhs, IVernacularEnvironment *environment);
	VernacularValue& OpMult(VernacularValue &rhs, IVernacularEnvironment *environment);
	VernacularValue& OpLogAnd(VernacularValue &rhs, IVernacularEnvironment *environment);
	VernacularValue& OpBitAnd(VernacularValue &rhs, IVernacularEnvironment *environment);
	VernacularValue& OpLogOr(VernacularValue &rhs, IVernacularEnvironment *environment);
	VernacularValue& OpBitOr(VernacularValue &rhs, IVernacularEnvironment *environment);
	VernacularValue& OpEqual(VernacularValue &rhs, IVernacularEnvironment *environment);
	VernacularValue& OpNotEqual(VernacularValue &rhs, IVernacularEnvironment *environment);
	VernacularValue& OpGreaterEqual(VernacularValue &rhs, IVernacularEnvironment *environment);
	VernacularValue& OpLessEqual(VernacularValue &rhs, IVernacularEnvironment *environment);
	VernacularValue& OpGreater(VernacularValue &rhs, IVernacularEnvironment *environment);
	VernacularValue& OpLess(VernacularValue &rhs, IVernacularEnvironment *environment);
	VernacularValue& OpBitRight(VernacularValue &rhs, IVernacularEnvironment *environment);
	VernacularValue& OpBitLeft(VernacularValue &rhs, IVernacularEnvironment *environment);
	VernacularValue& OpBitXor(VernacularValue &rhs, IVernacularEnvironment *environment);
	VernacularValue& OpMod(VernacularValue &rhs, IVernacularEnvironment *environment);
	VernacularValue& OpDiv(VernacularValue &rhs, IVernacularEnvironment *environment);
	VernacularValue& OpAdd(VernacularValue &rhs, IVernacularEnvironment *environment);
	VernacularValue& OpSub(VernacularValue &rhs, IVernacularEnvironment *environment);

	VernacularValue& OpLogNot(IVernacularEnvironment *environment);
	VernacularValue& OpBitNot(IVernacularEnvironment *environment);
	VernacularValue& OpNegate(IVernacularEnvironment *environment);

	// Template used internally by the comparison operators.
	// Not for external use.
	template <class T>
	VernacularValue& OpCompare(VernacularValue &rhs, IVernacularEnvironment *environment);

	bool operator== (const VernacularValue &other) const { return Equals(other); }

	size_t Hash() const;

	struct hash {
		size_t operator() (const VernacularValue &value) const { return value.Hash(); }
	};
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
