/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "gamecode/vernacular/VernacularCli.hh"
#include "log/FileLog.hh"
#include "log/DummyLog.hh"
#include "platform/posix/PosixPlatform.hh"
#include "platform/TimeAdapter.hh"
#include "filesystem/PlatformFilesystem.hh"
#include "terminal/ArgsAdapter.hh"
#include "terminal/AnsiTerminalAdapter.hh"
#include "events/EventsAdapter.hh"
#ifndef _WIN32
#include "terminal/NcursesAdapter.hh"
#endif
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

int VernacularCli::Run(int argc, char *argv[])
{
	if (argc < 1)
	{
		printf("Error: No command line arguments at all\n");
		return -1;
	}
	if (argc < 2)
	{
		printf("%s <filename>\n", argv[0]);
		return 0;
	}

	PlatformFilesystem filesystem;
	DummyLog log("vernacular");
	VernacularGamecode vernacular(&filesystem, &log);

	PosixPlatform posix;

	TimeAdapter time(&posix);
	vernacular.RegisterAdapter(&time);

	EventsAdapter events(&posix);
	vernacular.RegisterAdapter(&events);

	AnsiTerminalAdapter ansi;
	vernacular.RegisterAdapter(&ansi);

#ifndef _WIN32
	NcursesAdapter ncurses;
	vernacular.RegisterAdapter(&ncurses);
#endif

	ArgsAdapter args(argc, argv);
	vernacular.RegisterAdapter(&args);

	// TODO: Some way to tell the app to quit.

	// TODO: A means to load and save game data.

	// TODO: Wire up the log to a log file instead of the terminal,
	// because the terminal is the primary display to the user.

	// TODO: Support garbage collection in the middle of running vernacular
	// code, to support long-running vernacular scripts that don't just use
	// timers.
	// - Add the stack and input parameters as extra root objects, so that
	//   that sort of thing doesn't get collected.
	// - Ensure that the GC call order bug doesn't resurface. It might be fixed
	//   by the above item, along with also trying not to collect returned
	//   values.

	// TODO: See if we can use Ncurses to generate input events and use those
	// instead of wiring up ncurses here.
	// Maybe build a terminal game engine which outputs text instead of using
	// SDL for graphics.

	// We'd make an Ncurses platform that implements IPlatformInput and
	// IPlatformTextDisplay (which we would create to abstract rendering text),
	// and generates the input events that we're able to.

	// Run the file specified.
	vernacular.LoadFile(argv[1]);

	// Once the file has loaded, if it set up any events then we keep running
	// those events.
	for (uint64_t nextEvent = events.TimeUntilNextEvent(); nextEvent != 0; nextEvent = events.TimeUntilNextEvent())
	{
		posix.Delay(nextEvent);
		events.TriggerEvents();
	}

	return 0;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

int main(int argc, char *argv[])
{
	opti::VernacularCli vernacularCli;
	return vernacularCli.Run(argc, argv);
}
