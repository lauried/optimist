/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/vernacular/VernacularCoreLibrary.hh"
#include <sstream>
#include <cmath>
#include <random>
#include "library/text/String.hh"
#include "gamecode/vernacular/interpreter/IVernacularEnvironment.hh"
#include "gamecode/vernacular/interpreter/VernacularGlobals.hh"
#include "gamecode/vernacular/interpreter/VernacularThread.hh"
#include "gamecode/vernacular/types/VernacularFunctionFactory.hh"
#include "gamecode/vernacular/types/VernacularObject.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SYSTEM STUFF
//-----------------------------------------------------------------------------

// include(string) -- loads another file
static VernacularValue Include(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1 || !parameters[0].IsString())
	{
		environment->ThrowException("include() requires a single string parameter");
		return VernacularValue();
	}
	VernacularValue result;
	result.SetBool(environment->IncludeFile(parameters[0].ToString()));
	return result;
}

// print(mixed) -- prints to the console
static VernacularValue Print(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	for (int i = 0; i < numParameters; i += 1)
	{
		environment->Print(parameters[i].ToString());
	}
	return VernacularValue();
}

// exception(mixed) -- throws exception which halts execution
static VernacularValue Exception(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1)
	{
		environment->ThrowException("exception() requires a single parameter");
		return VernacularValue();
	}
	environment->ThrowException(parameters[0].ToString());
	return VernacularValue();
}

// warning(mixed) -- throws a language warning
static VernacularValue Warning(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1)
	{
		environment->ThrowException("warning() requires a single parameter");
		return VernacularValue();
	}
	environment->ThrowWarning(parameters[0].ToString());
	return VernacularValue();
}

// dumpFunction(function) -- dumps a function's output
static VernacularValue DumpFunction(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1 || !parameters[0].IsFunction())
	{
		environment->ThrowException("Incorrect parameter types for dumpFunction(function)");
		return VernacularValue();
	}
	parameters[0].GetVernacularFunction()->Dump(&std::cout);
	return VernacularValue();
}

// bind(table, function) -- binds a method on a table into a single function
static VernacularValue Bind(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 2 || !parameters[0].IsTable() || !parameters[1].IsFunction())
	{
		environment->ThrowException("Incorrect parameter types for bind(table, function)");
		return VernacularValue();
	}
	VernacularFunction *function = parameters[1].GetVernacularFunction();
	if (!function->IsBytecode())
	{
		environment->ThrowWarning("Unable to bind a non-bytecode function");
		return VernacularValue();
	}
	return VernacularValue(environment->GetFunctionFactory()->Allocate(parameters[0].GetVernacularObject(), function));
}

#if 0
// class(table, function) -- creates a class constructor out of the given table and function
static VernacularValue Class(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 2 || !parameters[0].IsTable() || !parameters[1].IsFunction())
	{
		environment->ThrowException("Incorrect parameter types for class(table, function)");
		return VernacularValue();
	}
	VernacularFunction *function = parameters[1].GetVernacularFunction();
	if (!function->IsBytecode())
	{
		environment->ThrowWarning("Unable to bind a non-bytecode function");
		return VernacularValue();
	}
	return VernacularValue(environment->GetFunctionFactory()->Allocate(parameters[0].GetVernacularObject(), function, true));
}
#endif

//-----------------------------------------------------------------------------
// TABLES
//-----------------------------------------------------------------------------

// tableSetClassname(table, classname)
static VernacularValue TableSetClassname(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	// TODO: Requires implementation in VernacularObject.
	environment->ThrowException("tableSetClassname not implemented");
	return VernacularValue();
}

// tableGetClassname(table)
static VernacularValue TableGetClassname(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	// TODO: Requires implementation in VernacularObject.
	environment->ThrowException("tableGetClassname not implemented");
	return VernacularValue();
}

// tableSetDestructor(table, func)
static VernacularValue TableSetDestructor(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	// TODO: Requires implementation in VernacularObject.
	environment->ThrowException("tableSetDestructor not implemented");
	return VernacularValue();
}

// tableGetDestructor(func)
static VernacularValue TableGetDestructor(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	// TODO: Requires implementation in VernacularObject.
	environment->ThrowException("tableGetDestructor not implemented");
	return VernacularValue();
}

#if 0
inline void SetClearTableFlag(VernacularValue *parameters, VernacularObject::Index::Flags flag, IVernacularEnvironment *environment)
{
	bool done;
	if (parameters[2].GetBool())
	{
		done = parameters[0].GetVernacularObject()->SetMetaFlag(parameters[1], flag);
	}
	else
	{
		done = parameters[0].GetVernacularObject()->ClearMetaFlag(parameters[1], flag);
	}
	environment->ThrowWarning("Table metadata could not be set for key");
}

// tableSetReadonly(table, key, readonly)
static VernacularValue TableSetReadonly(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 3 || !parameters[0].IsTable() || !parameters[2].IsBool())
	{
		environment->ThrowException("tableSetReadonly requires parameters(table, mixed, bool)");
		return VernacularValue();
	}
	SetClearTableFlag(parameters, VernacularObject::Index::FL_READONLY, environment);
	return VernacularValue();
}

// tableIsReadonly(table, key)
static VernacularValue TableIsReadonly(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 2 || !parameters[0].IsTable())
	{
		environment->ThrowException("tableIsReadonly requires parameters(table, mixed)");
		return VernacularValue();
	}
	int flags = 0;
	parameters[0].GetVernacularObject()->GetMetaFlags(parameters[1], &flags);
    VernacularValue result;
    result.SetBool(flags & VernacularObject::Index::FL_READONLY);
    return VernacularValue();
}

// tableSetPrivate(table, key, private)
static VernacularValue TableSetPrivate(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 3 || !parameters[0].IsTable() || !parameters[2].IsBool())
	{
		environment->ThrowException("tableSetReadonly requires parameters(table, mixed, bool)");
		return VernacularValue();
	}
	SetClearTableFlag(parameters, VernacularObject::Index::FL_PRIVATE, environment);
	return VernacularValue();
}

// tableIsPrivate(table, key)
static VernacularValue TableIsPrivate(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 2 || !parameters[0].IsTable())
	{
		environment->ThrowException("tableIsPrivate requires parameters(table, mixed)");
		return VernacularValue();
	}
	int flags = 0;
	parameters[0].GetVernacularObject()->GetMetaFlags(parameters[1], &flags);
    VernacularValue result;
    result.SetBool(flags & VernacularObject::Index::FL_READONLY);
    return VernacularValue();
}
#endif

// tablePushBack(table, [key,] value)
static VernacularValue TablePushBack(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters < 2 || numParameters > 3 || !parameters[0].IsTable())
	{
		environment->ThrowException("tablePushBack requires parameters(table, [mixed,] value)");
		return VernacularValue();
	}
	if (numParameters == 2)
	{
		parameters[0].GetVernacularObject()->PushBack(parameters[1]);
	}
	else
	{
		parameters[0].GetVernacularObject()->PushBack(parameters[1], parameters[2]);
	}
	return VernacularValue();
}

// tablePopBack(table)
static VernacularValue TablePopBack(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1 || !parameters[0].IsTable())
	{
		environment->ThrowException("tablePopBack requires parameters(table)");
		return VernacularValue();
	}
	return parameters[0].GetVernacularObject()->PopBack();
}

// tableBack(table)
static VernacularValue TableBack(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1 || !parameters[0].IsTable())
	{
		environment->ThrowException("tableBack requires parameters(table)");
		return VernacularValue();
	}
	return parameters[0].GetVernacularObject()->Back();
}

// tablePushFront(table, [key,] value)
static VernacularValue TablePushFront(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters < 2 || numParameters > 3 || !parameters[0].IsTable())
	{
		environment->ThrowException("tablePushFront requires parameters(table, [mixed,] value)");
		return VernacularValue();
	}
	if (numParameters == 2)
	{
		parameters[0].GetVernacularObject()->PushFront(parameters[1]);
	}
	else
	{
		parameters[0].GetVernacularObject()->PushFront(parameters[1], parameters[2]);
	}
	return VernacularValue();
}

// tablePopFront(table)
static VernacularValue TablePopFront(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1 || !parameters[0].IsTable())
	{
		environment->ThrowException("tablePopFront requires parameters(table)");
		return VernacularValue();
	}
	return parameters[0].GetVernacularObject()->PopFront();
}

// tableFront(table)
static VernacularValue TableFront(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1 || !parameters[0].IsTable())
	{
		environment->ThrowException("tablePopFront requires parameters(table)");
		return VernacularValue();
	}
	return parameters[0].GetVernacularObject()->Front();
}

// tableFindValue(table, value) -- returns the key
static VernacularValue TableFindValue(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
    if (numParameters != 2 || !parameters[0].IsTable())
    {
		environment->ThrowException("tableFindValue requires parameters(table, key)");
		return VernacularValue();
    }
    return parameters[0].GetVernacularObject()->KeyForValue(parameters[1]);
}

// tableGetKeys(table) -- returns a new table with keys as values
static VernacularValue TableGetKeys(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1 || !parameters[0].IsTable())
	{
		environment->ThrowException("tableGetKeys requires a single table parameter");
		return VernacularValue();
	}
	VernacularValue result;
	VernacularObject *object = environment->GetObjectFactory()->Allocate();
	object->CopyKeys(parameters[0].GetVernacularObject());
	result.SetObject(object);
	return result;
}

// tableReverse(table) -- reverses the order of the given table
static VernacularValue TableReverse(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1 || !parameters[0].IsTable())
	{
		environment->ThrowException("tableReverse requires a single table parameter");
		return VernacularValue();
	}
	parameters[0].GetVernacularObject()->Reverse();
	return VernacularValue();
}

// tableSort(table, function) -- sorts table by function, preserving keys
static VernacularValue TableSort(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	environment->ThrowException("tableSort not implemented");
	return VernacularValue();
}

// tableReindex(table[, start[, step]]) -- maintains order, changes keys to be numeric
static VernacularValue TableReindex(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 3 || !parameters[0].IsTable() || !parameters[1].IsNumber() || !parameters[2].IsNumber())
	{
		environment->ThrowException("tableReindex requires parameters(table, number, number)");
		return VernacularValue();
	}
	parameters[0].GetVernacularObject()->Reindex(parameters[1].GetDouble(), parameters[2].GetDouble());
	return VernacularValue();
}

// tableCount(table) -- returns number of keys
static VernacularValue TableCount(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1 || !parameters[0].IsTable())
	{
		environment->ThrowException("tableFindValue requires a single table parameter");
		return VernacularValue();
	}
	return VernacularValue(parameters[0].GetVernacularObject()->Count());
}

// tableShallowCopy(table) -- returns a shallow copy of the given table
static VernacularValue TableShallowCopy(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1 || !parameters[0].IsTable())
	{
		environment->ThrowException("tableShallowCopy requires a single table parameter");
		return VernacularValue();
	}
	VernacularValue result;
	VernacularObject *object = environment->GetObjectFactory()->Allocate();
	object->Copy(parameters[0].GetVernacularObject());
	result.SetObject(object);
	return result;
}

//-----------------------------------------------------------------------------
// STRING
//-----------------------------------------------------------------------------

// stringLength(string) -- returns the length of a string
static VernacularValue StringLength(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1 || !parameters[0].IsString())
	{
		environment->ThrowException("Invalid parameters for stringLength(string)");
		return VernacularValue();
	}
	return VernacularValue(parameters[0].GetVernacularString()->Length());
}

// stringPart(string, offset, length) -- returns part of a string, starting from the given offset with the given length
static VernacularValue StringPart(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters < 2 || numParameters > 3 || !parameters[0].IsString() || !parameters[1].IsNumber())
	{
		environment->ThrowException("Invalid parameters for stringPart(string, number offset[ , number length])");
		return VernacularValue();
	}
	VernacularString *string = parameters[0].GetVernacularString();
	int offset = parameters[1].GetInt();
	if (offset < 0)
	{
		offset = string->UnicodeLength() - offset;
	}
	int length;
	if (numParameters == 3)
	{
		if (!parameters[2].IsNumber())
		{
			environment->ThrowException("Invalid parameters for stringPart(string, number offset[ , number length])");
			return VernacularValue();
		}
		length = parameters[2].GetInt();
	}
	else
	{
		length = string->UnicodeLength() - offset;
		if (length < 0)
		{
			length = (string->UnicodeLength() - length) - offset;
		}
	}
	std::stringstream stream;
	string->UnicodeSubstringToStream(&stream, offset, length);
	return VernacularValue(environment->GetStringFactory()->Allocate(stream.str()));
}

// stringSplit(string, separator) -- returns table of parts
static VernacularValue StringSplit(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters < 2 || numParameters > 3 || !parameters[0].IsString() || !parameters[1].IsString())
	{
		environment->ThrowException("Invalid parameters for stringSplit(string, string separator[, number maxTokens])");
		return VernacularValue();
	}
	int maxTokens = 0;
	if (numParameters == 3)
	{
		if (!parameters[2].IsNumber())
		{
			environment->ThrowException("Invalid parameters for stringSplit(string, string separator[, number maxTokens])");
			return VernacularValue();
		}
		maxTokens = parameters[2].GetInt();
	}
	std::string subject(parameters[0].GetVernacularString()->Get());
	std::string delimiter(parameters[1].GetVernacularString()->Get());
	std::vector<std::string> tokens;
	String::Split(subject, &tokens, delimiter, maxTokens);

	VernacularObject *object = environment->GetObjectFactory()->Allocate();
	for (int i = 0; i < tokens.size(); i += 1)
	{
		VernacularValue token(environment->GetStringFactory()->Allocate(tokens[i]));
		object->PushBack(token);
	}
	return VernacularValue(object);
}

// strignJoin(table, delimiter) -- returns single string
static VernacularValue StringJoin(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 2 || !parameters[0].IsTable() || !parameters[1].IsString())
	{
		environment->ThrowException("Invalid parameters for stringFromUnicode(table, string delimiter)");
		return VernacularValue();
	}
	VernacularString *result = nullptr;
	VernacularString *delimiter = parameters[1].GetVernacularString();
	VernacularObject::Iterator iterator = parameters[0].GetVernacularObject()->GetIterator(); //(parameters[0].GetVernacularObject()->index);
	for (; !iterator.End(); iterator.Next())
	{
		VernacularValue value = iterator.Value();
		if (!value.IsString())
		{
			value = value.ConvertToString(environment);
		}
		if (result == nullptr)
		{
			result = value.GetVernacularString();
		}
		else
		{
			result = environment->GetStringFactory()->Concatenate(result, delimiter);
			result = environment->GetStringFactory()->Concatenate(result, value.GetVernacularString());
		}
	}
	return VernacularValue(result);
}

// stringFromCode(mixed) -- converts a single numeric code or a table of codes into a string
static VernacularValue StringFromUnicode(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1 || (!parameters[0].IsNumber() && !parameters[0].IsTable()))
	{
		environment->ThrowException("Invalid parameters for stringFromUnicode(table | number)");
		return VernacularValue();
	}
	if (parameters[0].IsNumber())
	{
		char unicode[7];
		int chars = String::UnicodeToUTF8(parameters[0].GetInt(), unicode);
		return VernacularValue(environment->GetStringFactory()->Allocate(unicode));
	}
	else
	{
		char unicode[7];
		int chars;
		std::stringstream ss;
		VernacularObject::Iterator iterator = parameters[0].GetVernacularObject()->GetIterator();//(parameters[0].GetVernacularObject()->index);
		for (; !iterator.End(); iterator.Next())
		{
			VernacularValue value = iterator.Value();
			if (value.IsNumber())
			{
				chars = String::UnicodeToUTF8(value.GetInt(), unicode);
				ss << unicode;
			}
		}
		return VernacularValue(environment->GetStringFactory()->Allocate(ss.str()));
	}
}

// stringToUnicode(string, offset, length) -- if no length is specified, returns a single unicode, otherwise returns an array of unicodes
static VernacularValue StringToUnicode(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters < 2 || numParameters > 3 || !parameters[0].IsString() || !parameters[1].IsNumber())
	{
		environment->ThrowException("Invalid parameters for stringToUnicode(string, number offset[ , number length])");
		return VernacularValue();
	}
	VernacularString *string = parameters[0].GetVernacularString();
	int offset = parameters[1].GetInt();
	if (offset < 0)
	{
		offset = string->UnicodeLength() - offset;
	}
	int length;
	if (numParameters == 3)
	{
		if (!parameters[2].IsNumber())
		{
			environment->ThrowException("Invalid parameters for stringToUnicode(string, number offset[ , number length])");
			return VernacularValue();
		}
		length = parameters[2].GetInt();
	}
	else
	{
		length = string->UnicodeLength() - offset;
		if (length < 0)
		{
			length = (string->UnicodeLength() - length) - offset;
		}
	}
	std::stringstream stream;
	string->UnicodeSubstringToStream(&stream, offset, length);
	int unicode;
	std::string str = stream.str();
	if (numParameters == 2)
	{
		String::UTF8ToUnicode(str, 0, &unicode);
		return VernacularValue(unicode);
	}
	VernacularObject *object = environment->GetObjectFactory()->Allocate();
	int bufferOffset = 0;
	int read;
	for (int read = String::UTF8ToUnicode(str, bufferOffset, &unicode); read > 0; read = String::UTF8ToUnicode(str, bufferOffset, &unicode))
	{
		bufferOffset += read;
		VernacularValue ucValue(unicode);
		object->PushBack(ucValue);
	}
	return VernacularValue(object);
}

//-----------------------------------------------------------------------------
// CONVERSION AND TYPE
//-----------------------------------------------------------------------------

// toBool(value)
// - null: false
// - number: zero -> false, nonzero -> true
// - string: true
// - object: true
// - function: true
// - user: true
static VernacularValue ToBool(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1)
	{
		environment->ThrowException("toBool requires exactly one parameter");
		return VernacularValue();
	}
	VernacularValue result;
	switch (parameters[0].GetType())
	{
	case GamecodeValue::T_Null:
		result.SetFalse();
		break;
	case GamecodeValue::T_Bool:
		result = parameters[0];
		break;
	case GamecodeValue::T_Number:
		result.SetBool(parameters[0].GetDouble() != 0);
		break;
	default:
		result.SetTrue();
	}
	return result;
}

// toNumber(value)
// - null: 0
// - boolean: false -> 0, true -> 1
// - string: any valid string literal is converted, anything else becomes zero
// - object: 0
// - function: 0
// - user: 0
static VernacularValue ToNumber(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1)
	{
		environment->ThrowException("toNumber requires exactly one parameter");
		return VernacularValue();
	}
	VernacularValue result;
	switch (parameters[0].GetType())
	{
	case GamecodeValue::T_Null:
		result.SetDouble(0);
		break;
	case GamecodeValue::T_Bool:
		result.SetDouble(result.GetBool() ? 1 : 0);
		break;
	case GamecodeValue::T_Number:
		result = parameters[0];
		break;
	default:
		result.SetDouble(0);
	}
	return result;
}

// toString(value)
// - null: "null"
// - boolean: false -> "false", true -> "true"
// - number: convert to decimal - a proper number formatting method is preferred
// - object: "object:" + unique identifier for object
// - function: "function:" + unique identifier for function
// - user: "user:" + unique identifier for user object
static VernacularValue ToString(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1)
	{
		environment->ThrowException("toString requires exactly one parameter");
		return VernacularValue();
	}

	return parameters[0].ConvertToString(environment);
}

// getType(mixed) -- returns "null", "boolean", "number", "string", "table", "function" or "user".
static VernacularValue GetType(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1)
	{
		environment->ThrowException("getType requires exactly one parameter");
		return VernacularValue();
	}
	VernacularValue result;
	switch (parameters[0].GetType())
	{
	case GamecodeValue::T_Null:
		result.SetString(environment->GetStringFactory()->GetConstNull());
		break;
	case GamecodeValue::T_Bool:
		result.SetString(environment->GetStringFactory()->GetConstBoolean());
		break;
	case GamecodeValue::T_Number:
		result.SetString(environment->GetStringFactory()->GetConstNumber());
		break;
	case GamecodeValue::T_String:
		result.SetString(environment->GetStringFactory()->GetConstString());
		break;
	case GamecodeValue::T_Table:
		result.SetString(environment->GetStringFactory()->GetConstTable());
		break;
	case GamecodeValue::T_Function:
		result.SetString(environment->GetStringFactory()->GetConstFunction());
		break;
	case GamecodeValue::T_Engine:
		result.SetString(environment->GetStringFactory()->GetConstUser());
		break;
	}
	return result;
}

// getUserType(mixed) -- returns the name of the user type, or false if the value is not a user object.
static VernacularValue GetUserType(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1)
	{
		environment->ThrowException("getUserType requires exactly one parameter");
		return VernacularValue();
	}
	if (!parameters[0].IsEngine())
	{
		VernacularValue result;
		result.SetFalse();
		return result;
	}
	VernacularValue result;
	result.SetString(environment->GetStringFactory()->Allocate(parameters[0].GetVernacularUserObject()->GetTypeName()));
	return result;
}

//-----------------------------------------------------------------------------
// MATHS
//-----------------------------------------------------------------------------

static VernacularValue Cos(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1)
	{
		environment->ThrowException("cos requires exactly one parameter");
		return VernacularValue();
	}
	return VernacularValue(std::cos(parameters[0].GetAsNumber()));
}

static VernacularValue Sin(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1)
	{
		environment->ThrowException("sin requires exactly one parameter");
		return VernacularValue();
	}
	return VernacularValue(std::sin(parameters[0].GetAsNumber()));
}

static VernacularValue Tan(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1)
	{
		environment->ThrowException("tan requires exactly one parameter");
		return VernacularValue();
	}
	return VernacularValue(std::tan(parameters[0].GetAsNumber()));
}

static VernacularValue Acos(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1)
	{
		environment->ThrowException("acos requires exactly one parameter");
		return VernacularValue();
	}
	return VernacularValue(std::acos(parameters[0].GetAsNumber()));
}

static VernacularValue Asin(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1)
	{
		environment->ThrowException("asin requires exactly one parameter");
		return VernacularValue();
	}
	return VernacularValue(std::acos(parameters[0].GetAsNumber()));
}

static VernacularValue Atan(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters == 1)
	{
		return VernacularValue(std::atan(parameters[0].GetAsNumber()));
	}
	if (numParameters == 2)
	{
		return VernacularValue(std::atan2(parameters[0].GetAsNumber(), parameters[1].GetAsNumber()));
	}
	environment->ThrowException("atan requires one or two parameters");
	return VernacularValue();
}

static VernacularValue Log(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1)
	{
		environment->ThrowException("log requires exactly one parameter");
		return VernacularValue();
	}
	return VernacularValue(std::log(parameters[0].GetAsNumber()));
}

static VernacularValue Log10(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1)
	{
		environment->ThrowException("log10 requires exactly one parameter");
		return VernacularValue();
	}
	return VernacularValue(std::log10(parameters[0].GetAsNumber()));
}

static VernacularValue Sqrt(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1)
	{
		environment->ThrowException("sqrt requires exactly one parameter");
		return VernacularValue();
	}
	return VernacularValue(std::sqrt(parameters[0].GetAsNumber()));
}

static VernacularValue Ceil(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1)
	{
		environment->ThrowException("ceil requires exactly one parameter");
		return VernacularValue();
	}
	return VernacularValue(std::ceil(parameters[0].GetAsNumber()));
}

static VernacularValue Floor(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1)
	{
		environment->ThrowException("floor requires exactly one parameter");
		return VernacularValue();
	}
	return VernacularValue(std::floor(parameters[0].GetAsNumber()));
}

static VernacularValue Round(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1)
	{
		environment->ThrowException("round requires exactly one parameter");
		return VernacularValue();
	}
	return VernacularValue(std::rint(parameters[0].GetAsNumber()));
}

static VernacularValue Abs(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1)
	{
		environment->ThrowException("abs requires exactly one parameter");
		return VernacularValue();
	}
	return VernacularValue(std::abs(parameters[0].GetAsNumber()));
}

//-----------------------------------------------------------------------------
// RANDOM
//-----------------------------------------------------------------------------

static int RandomInt(int min, int max)
{
	static std::mt19937 rng;
	static bool init = false;
	if (!init)
	{
		rng.seed(std::random_device()());
		init = true;
	}
	std::uniform_int_distribution<std::mt19937::result_type> dist(min, max);
	return dist(rng);
}

static VernacularValue Random(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 2)
	{
		environment->ThrowException("random requires exactly two parameters");
		return VernacularValue();
	}
	return VernacularValue(RandomInt(parameters[0].GetAsNumber(), parameters[1].GetAsNumber()));
}

static VernacularValue TableRandomKey(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1 || !parameters[0].IsTable())
	{
		environment->ThrowException("tableRandomKey requires parameters(table)");
		return VernacularValue();
	}
	VernacularObject *object = parameters[0].GetVernacularObject();
	int count = object->Count();
	if (count == 0)
	{
		return VernacularValue();
	}
	int index = RandomInt(0, count - 1);
	auto iterator = object->GetIterator();
	for (int i = 0; i < index; i += 1)
	{
		iterator.Next();
	}
	return iterator.Key();
}

static VernacularValue TableRandomValue(VernacularValue *parameters, int numParameters, IVernacularEnvironment *environment)
{
	if (numParameters != 1 || !parameters[0].IsTable())
	{
		environment->ThrowException("tableRandomValue requires parameters(table)");
		return VernacularValue();
	}
	VernacularObject *object = parameters[0].GetVernacularObject();
	int count = object->Count();
	if (count == 0)
	{
		return VernacularValue();
	}
	int index = RandomInt(0, count - 1);
	auto iterator = object->GetIterator();
	for (int i = 0; i < index; i += 1)
	{
		iterator.Next();
	}
	return iterator.Value();
}

//-----------------------------------------------------------------------------
// SETUP
//-----------------------------------------------------------------------------

static void RegisterFunction(IVernacularEnvironment *environment, std::string name, VernacularFunction::Callback function)
{
	VernacularGlobals *globals = environment->GetGlobals();
	int index = globals->Create(name);
	globals->GetArray()[index].SetFunction(environment->GetFunctionFactory()->Allocate(function));
}

void VernacularCoreLibrary::Register(IVernacularEnvironment *environment)
{
	RegisterFunction(environment, "include", Include);
	RegisterFunction(environment, "print", Print);
	RegisterFunction(environment, "exception", Exception);
	RegisterFunction(environment, "warning", Warning);
	RegisterFunction(environment, "dumpFunction", DumpFunction);
	RegisterFunction(environment, "bind", Bind);
	//RegisterFunction(environment, "class", Class);

	//RegisterFunction(environment, "tableSetClassname", TableSetClassname);
	//RegisterFunction(environment, "tableGetClassname", TableGetClassname);
	//RegisterFunction(environment, "tableSetDestructor", TableSetDestructor);
	//RegisterFunction(environment, "tableGetDestructor", TableGetDestructor);
	//RegisterFunction(environment, "tableSetReadonly", TableSetReadonly);
	//RegisterFunction(environment, "tableIsReadonly", TableIsReadonly);
	//RegisterFunction(environment, "tableSetPrivate", TableSetPrivate);
	//RegisterFunction(environment, "tableIsPrivate", TableIsPrivate);

	RegisterFunction(environment, "tableBack", TableBack);
	RegisterFunction(environment, "tablePushBack", TablePushBack);
	RegisterFunction(environment, "tablePopBack", TablePopBack);
	RegisterFunction(environment, "tableFront", TableFront);
	RegisterFunction(environment, "tablePushFront", TablePushFront);
	RegisterFunction(environment, "tablePopFront", TablePopFront);

	RegisterFunction(environment, "tableFindValue", TableFindValue);
	RegisterFunction(environment, "tableGetKeys", TableGetKeys);
	RegisterFunction(environment, "tableFindValue", TableFindValue);
	RegisterFunction(environment, "tableReverse", TableReverse);
	RegisterFunction(environment, "tableSort", TableSort);
	RegisterFunction(environment, "tableReindex", TableReindex);
	RegisterFunction(environment, "tableCount", TableCount);
	RegisterFunction(environment, "tableShallowCopy", TableShallowCopy);

	RegisterFunction(environment, "stringLength", StringLength);
	RegisterFunction(environment, "stringPart", StringPart);
	RegisterFunction(environment, "stringSplit", StringSplit);
	RegisterFunction(environment, "stringFromUnicode", StringFromUnicode);
	RegisterFunction(environment, "stringToUnicode", StringToUnicode);

	RegisterFunction(environment, "toBool", ToBool);
	RegisterFunction(environment, "toNumber", ToNumber);
	RegisterFunction(environment, "toString", ToString);

	RegisterFunction(environment, "getType", GetType);
	RegisterFunction(environment, "getUserType", GetUserType);

	RegisterFunction(environment, "cos", Cos);
	RegisterFunction(environment, "sin", Sin);
	RegisterFunction(environment, "tan", Tan);
	RegisterFunction(environment, "acos", Acos);
	RegisterFunction(environment, "asin", Asin);
	RegisterFunction(environment, "atan", Atan);
	RegisterFunction(environment, "log", Log);
	RegisterFunction(environment, "log10", Log10);
	RegisterFunction(environment, "sqrt", Sqrt);
	RegisterFunction(environment, "ceil", Ceil);
	RegisterFunction(environment, "floor", Floor);
	RegisterFunction(environment, "round", Round);
	RegisterFunction(environment, "abs", Abs);

	RegisterFunction(environment, "random", Random);
	RegisterFunction(environment, "tableRandomKey", TableRandomKey);
	RegisterFunction(environment, "tableRandomValue", TableRandomValue);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
