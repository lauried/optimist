/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <functional>
#include <vector>
#include <utility>
#include <string>
#include <map>
#include "gamecode/GamecodeParameterList.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * This interface represents a module which provides functions and object types
 * to the gamecode.
 */
class IGamecodeAdapter {
public:
	typedef std::function<GamecodeValue(GamecodeParameterList*, IGamecode*)> AdapterMethod;
	typedef std::vector<std::pair<const char*, AdapterMethod>> AdapterMethodList;

	/**
	 * Returns the name by which gamecode can check if this adapter is present.
	 * This name must be unique among adapters and should be the name of the
	 * class, minus the suffix 'Adapter' which is added as part of the naming
	 * convention.
	 */
	virtual const std::string& GetName() = 0;

	/**
	 * The adapter must return a list of all functions which it wants to be
	 * bound by gamecode. The gamecode is responsible for placing the functions
	 * in a namespace according to the adapter name.
	 */
	virtual std::map<std::string, AdapterMethod> GetGlobalFunctions() = 0;

	/**
	 * Called by the gamecode implementation when the adapter has been
	 * registered with it (using RegisterAdapter).
	 *
	 * This is where engine object types are registered, either by direct calls
	 * to IGamecode::RegisterEngineType, or by using ModularEngineTypes.
	 *
	 * @param gamecode  The gamecode implementation on which this
	 * adapter is being registered.
	 */
	virtual void OnRegister(IGamecode *gamecode) = 0;

	/**
	 * The adapter must return a list of all gamecode objects that it wishes
	 * to prevent being garbage collected.
	 * This is for things like registering event callbacks.
	 */
	virtual void GetChildObjects(std::vector<GamecodeValue> *objects) {}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
