/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gamecode/IGamecodeObject.hh"
#include "gamecode/GamecodeValue.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * A wrapper to a gamecode table object, a generic data structure
 * within the gamecode.
 */
class IGamecodeTable : public IGamecodeObject {
public:
	~IGamecodeTable();

	/** /name
	 * Attempts to retrieve a non-existent key must return a null value.
	 * This can serve as a check for whether a key exists.
	 */
	//@{
	virtual GamecodeValue Get(GamecodeValue key) = 0;
	//@}

	virtual int NumKeys() = 0;
	virtual GamecodeValue GetKey(int i) = 0;
	virtual GamecodeValue GetValue(int i) = 0;

	virtual void Set(GamecodeValue key, GamecodeValue value) = 0;

	/** /name
	 * Return true if the underlying implementation supports the relevant
	 * properties.
	 */
	//@{
	virtual bool AllowStringKey() = 0;
	//@}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
