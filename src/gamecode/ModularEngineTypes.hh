/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gamecode/IGamecode.hh"
#include "resources/ResourceManager.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * This system allows an adapter to support unknown engine types without having
 * to modify the adapter's code.
 *
 * In the engine type source, we create a static function which registers the
 * type. Then we register the callback with a define:
 *   MODULAR_ENGINE_TYPE(TheAdapter, RegisterCallback)
 *
 * Then the only code needed in the adapter's onregister is:
 *   ModularEngineTypes::Instance()->Register<TheAdapter>(igamecodepointer);
 */

class BaseModularEngineType {
public:
	virtual void Register(IGamecode *gamecode, ResourceManager *resources) = 0;
};

class ModularEngineTypes {
public:
	template <class T>
	class ModularEngineType : public BaseModularEngineType {
		friend class ModularEngineTypes;
	public:
		typedef void (*RegisterCallback)(IGamecode *gamecode, ResourceManager *resources);

		ModularEngineType(RegisterCallback rcb)
		{
			callback = rcb;
			ModularEngineTypes::Instance()->DeclareType(this);
		}

	private:
		RegisterCallback callback;

		void Register(IGamecode *gamecode, ResourceManager *resources)
		{
			callback(gamecode, resources);
		}
	};

	std::vector<BaseModularEngineType*> types;

	// Only ModularEngineType should call this.
	void DeclareType(BaseModularEngineType* type)
	{
		types.push_back(type);
	}

	template <class T>
	void Register(IGamecode *gamecode, ResourceManager *resources)
	{
		// TODO: Here we might write to the gamecode's log so that we can tell
		// which objects have been registered with it.
		for (int i = 0; i < types.size(); i += 1)
		{
			ModularEngineType<T> *type = dynamic_cast<ModularEngineType<T>*>(types[i]);
			if (type != nullptr)
			{
				type->Register(gamecode, resources);
			}
		}
	}

	static ModularEngineTypes* Instance();

private:
	ModularEngineTypes() {}
};

#define MODULAR_ENGINE_TYPE(ADAPTER, CALLBACK) static ModularEngineTypes::ModularEngineType<ADAPTER> modularEngineType(&CALLBACK);

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
