/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gamecode/IGamecodeEngineObject.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

template <class T>
class WrapperGamecodeEngineObject : public virtual IGamecodeEngineObject {
public:
	/**
	 * Construct with its own wrapped object.
	 */
	WrapperGamecodeEngineObject()
	{
		object = CreateObject();
		ownsObject = true;
	}

	~WrapperGamecodeEngineObject()
	{
		if (ownsObject)
		{
			DeleteObject();
		}
	}

	/**
	 * Construct with a wrapped object which it does not own.
	 */
	WrapperGamecodeEngineObject(T *objectToWrap)
	{
		object = objectToWrap;
		ownsObject = false;
	}

	/**
	 * Convert from pointing to a non-owned wrapped object, to its own copy.
	 * This must only be called by the gamecode implementation when the parent
	 * of this object has its destructor called.
	 */
	void OnParentObjectDestruct()
	{
		if (!ownsObject)
		{
			object = CreateObject();
			ownsObject = true;
		}
	}

	virtual T* CreateObject()
	{
		return new T();
	}

	virtual void DeleteObject()
	{
		delete object;
	}

	T* GetObject()
	{
		return object;
	}
protected:
	bool ownsObject; // true if we created and are responsible for deletion of the object.
	T *object;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
