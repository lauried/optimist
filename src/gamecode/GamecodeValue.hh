/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <vector>
#include <string>
#include <stdexcept>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class IGamecodeVector;
class IGamecodeString;
class IGamecodeTable;
class IGamecodeFunction;
class IGamecodeEngineObject;

class IGamecodeAdapter;
class IGamecode;

// TODO: Transition from float+int to double.
// - Remove IsFloat, IsInt, IsFloatOrInt and update external code that used these.
// - Change v_float and v_int to v_double, and update getters/setters that address these.
// - Remove T_Int and T_Float and replace with T_Number, and update external code.

/**
 * Representation of a data element retrieved from or to be passed to gamecode.
 *
 * This is a concrete class that's passed by value.
 * Int and Float values are guaranteed always to exist, while object types
 * are no longer guaranteed to exist as soon as execution returns to gamecode:
 * - Parameters created by the user and passed to gamecode are invalid as soon
 *   as a gamecode function is called.
 * - Parameters returned to user code from a gamecode function call are cleared
 *   the next time a function call is made.
 * - Parameters passed to an adapter function callback are invalid when that
 *   function returns to gamecode.
 * - Parameters returned from an adapter function are invalid on return to
 *   gamecode.
 *
 * Null vectors are all zero.
 * Null strings are zero length.
 * Null tables are empty and cannot have new values added.
 * Null functions do nothing when called.
 * Null engine objects do not dynamic_cast to any real object, their get and
 * set functions do nothing.
 */
class GamecodeValue {
public:
	enum Type {
		T_Null      = 0,
		T_Bool      = 1,
		T_Number	= 2,
		T_String    = 4,
		T_Table     = 8,
		T_Function  = 16,
		T_Engine    = 32,
	};

	GamecodeValue();
	GamecodeValue(const GamecodeValue &other);

	GamecodeValue(bool b);
	GamecodeValue(int i);
	GamecodeValue(float f);
	GamecodeValue(double d);

	/** /name
	 * The pointer returned is no longer valid if more gamecode is executed.
	 */
	//@{
	GamecodeValue(IGamecodeString* string);
	GamecodeValue(IGamecodeTable* table);
	GamecodeValue(IGamecodeFunction* function);
	GamecodeValue(IGamecodeEngineObject* engine);
	~GamecodeValue();
	//@}

	void operator=(const GamecodeValue &val);

	GamecodeValue::Type GetType() { return type; }

	bool IsNull() { return (GetType() == T_Null); }
	bool IsBool() { return (GetType() == T_Bool); }
	bool IsNumber() { return (GetType() == T_Number); }
	bool IsString() { return (GetType() == T_String); }
	bool IsTable() { return (GetType() == T_Table); }
	bool IsFunction() { return (GetType() == T_Function); }
	bool IsEngine() { return (GetType() == T_Engine); }

	/** /name
	 * The only conversion performed here is between numeric and boolean types.
	 */
	//@{
	bool GetBool();
	int GetInt();
	float GetFloat();
	double GetDouble();
	const char* GetString();
	IGamecodeTable* GetTable();
	IGamecodeFunction* GetFunction();
	IGamecodeEngineObject* GetEngine();
	//@}

	void Get(int *out) { *out = GetInt(); }
	void Get(float *out) { *out = GetFloat(); }
	void Get(std::string *out) { *out = GetString(); }

	void SetBool(bool b) { type = T_Bool; value.v_double = b ? 1 : 0; }
	void SetInt(int i) { type = T_Number; value.v_double = i; }
	void SetFloat(float f) { type = T_Number; value.v_double = f; }
	void SetDouble(double d) { type = T_Number; value.v_double = d; }

	/** /name
	 * Will return nullptr if the value is not of this type.
	 */
	//@{
	IGamecodeString* GetStringObject();
	//@}

	bool operator==(const GamecodeValue& other);
	bool operator!=(const GamecodeValue& other) { return !(*this == other); }

protected:
	Type type;

	union {
		double v_double;
		IGamecodeString* v_string;
		IGamecodeTable* v_table;
		IGamecodeFunction* v_function;
		IGamecodeEngineObject* v_engine;
	} value;

	static const uint64_t NUMBER_BITMASK = 0x1fffffffffffff;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
#include "gamecode/IGamecodeFunction.hh"
#include "gamecode/IGamecodeString.hh"
#include "gamecode/IGamecodeTable.hh"
//-----------------------------------------------------------------------------
