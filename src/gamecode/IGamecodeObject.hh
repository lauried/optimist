/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Interface that all representations of objects in gamecode implement.
 * They could be wrappers, or gamecode objects could descend directly from
 * them.
 *
 * If not returned as a child of an engine object, this object is not guaranteed
 * to exist after the engine method to which it was passed as a parameter
 * returns.
 */
class IGamecodeObject {
public:
	virtual ~IGamecodeObject();
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

