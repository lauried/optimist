/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <typeinfo>
#include <typeindex>
#include <string>
#include <vector>
#include <functional>
#include <stdexcept>
#include "gamecode/IGamecodeAdapter.hh"
#include "gamecode/GamecodeValue.hh"
#include "gamecode/IGamecodeEngineObject.hh"
#include "gamecode/GamecodeParameterList.hh"
#include "gamecode/GamecodeEngineObjectDescriptor.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// http://www.stack.nl/~dimitri/doxygen/manual/markdown.html
/**
\page gamecode Gamecode Interface

The goal of the gamecode interface is two-fold: to keep the gamecode
implementation from knowing the internal details of attached modules, and to
keep modules from having to know details of the specific implementation of
gamecode being used. This is known as dependency inversion principle.

A new scripting language can be made available to the game engine by
implementing [IGamecode](@ref opti::IGamecode) and the wrapper classes used to
pass data around.

A new module can be made available to gamecode by implementing
[IGamecodeAdapter](@ref opti::IGamecodeAdapter) and
[IGamecodeEngineObject](@ref opti::IGamecodeEngineObject).


\section modules Creating Modules

Modules available to gamecode in Optimist are called adapters, and they are
created by implementing IGamecodeAdapter. The virtual functions that must be
implemented are described in the documentation for that interface.

Before use, the adapter must be registered with the gamecode with a call to
IGamecode::RegisterAdapter. This will invoke a call back to
IGamecodeAdapter::OnRegister.

Adapters are currently registered in Engine.


\section preserved_tables preserved Tables

TODO: Overview of preserved table use. It may be deprecated as dynamics
collision callbacks can be implemented using GetChildObjects.

TODO:
Put example code and explanations and overall expectations from the
interface, but also expect and inform the reader to look at the class
documentation for interface guarantees and requirements.

* Creating the adapter, implementing OnRegister callback.
* How global function calls are added to the adapter - ListGlobalFunctions and CallFunction, the index parameter.
* TableGarbageCollected and its relation to PreserveTable.

* Creating an engine object, deriving from IGamecodeEngineObject.
* Using GamecodeEngineObjectDescriptor interface to wire up most methods and properties.
* Using GetProperty and SetProperty callbacks for more complex behaviour.
* Operator methods.
* Use of GetChildObjects, and how it's not required for properties added with AddObject.

* Use of ModularEngineTypes, using Scenegraph as an example.
  Mention how this can be appropriate any time an object isn't an integral part of the module.


\section implementations Creating An Implementation

TODO:
There's not that much point providing example code for this, but we must
provide all the details of what needs to be done at each stage, and we can do
this using GamemonkeyGamecode as a reference (but not necessarily mentioning
it as an example).


\section naming Naming Conventions

A class which implements IGamecodeAdapter should have the suffix Adapter, e.g.
PlatformAdapter, DynamicsAdapter.

A class which implements IGamecodeEngineObject should have the prefix Gamecode,
e.g. GamecodeDynamics, GamecodeTerrain.

A class which implements IGamecode should have the suffix Gamecode, e.g.
GamemonkeyGamecode.

*/

/**
 * Scripted gamecode implementation.
 */
class IGamecode {
public:

	virtual ~IGamecode();

	virtual GamecodeValue CreateString(const char* s) = 0;
	virtual GamecodeValue CreateTable() = 0;

	/**
	 * Calls the named function with the given parameters.
	 * Throws a gamecode_exception if there was an exception running the
	 * gamecode caused by an error in scripted language.
	 * Throws std::invalid_argument_error on invalid parameters.
	 */
	virtual GamecodeValue CallFunction(std::string &name, std::vector<GamecodeValue> &parameters) = 0;

	/**
	 * Calls the given function object with the given parameters.
	 * Throws a gamecode_exception if there was an exception running the
	 * gamecode caused by an error in scripted language.
	 * Throws std::invalid_argument_error on invalid parameters.
	 */
	virtual GamecodeValue CallFunction(GamecodeValue function, std::vector<GamecodeValue> &parameters, GamecodeValue thisObject) = 0;

	/**
	 * Returns the named function, or null if no function exists by that name.
	 */
	virtual GamecodeValue GetFunction(std::string &name) = 0;

	/**
	 * Registers an adapter with the gamecode.
	 * The adapter's global functions will be made available to gamecode.
	 * The implementation must call adapter->OnRegister(this) when the adapter
	 * has been set up.
	 */
	virtual void RegisterAdapter(IGamecodeAdapter *adapter) = 0;

	/**
	 * Creates an engine object descriptor which can then have properties and
	 * methods added. The type must then be registered with RegisterEngineType.
	 */
	template<class T>
	GamecodeEngineObjectDescriptor<T>* CreateEngineType()
	{
		return new GamecodeEngineObjectDescriptor<T>();
	}

	/**
	 * Registers an engine type with the gamecode.
	 */
	virtual void RegisterEngineType(std::string name, IGamecodeEngineObjectDescriptor *type) = 0;

protected:
	/**
	 * Deletes an engine object descriptor created with CreateEngineType.
	 * This is provided so that we delete the object in a way consistent with
	 * how it was created in CreateEngineType.
	 */
	void DeleteEngineType(IGamecodeEngineObjectDescriptor *type) { delete type; }
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
