/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gamecode/WrapperGamecodeEngineObject.hh"
#include "library/geometry/Vector3.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class GamecodeVector : public WrapperGamecodeEngineObject<Vector3F> {
public:
	GamecodeVector() : WrapperGamecodeEngineObject<Vector3F>() {}
	GamecodeVector(Vector3F* vec) : WrapperGamecodeEngineObject<Vector3F>(vec) {}
	GamecodeVector(const GamecodeVector *other);

	// IGamecodeEngineObject
	void SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode);
	GamecodeValue GetProperty(std::string key, IGamecode *gamecode);
	void SetIndex(int i, GamecodeValue val, IGamecode *gamecode);
	GamecodeValue GetIndex(int i, IGamecode *gamecode);

	// Engine Methods
	GamecodeValue OpAdd(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue OpSub(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue OpMul(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue OpAddBy(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue OpSubBy(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue OpMulBy(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue Set(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue Scale(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue DotProduct(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue Length(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue Normalize(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue GetNormalized(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue CrossProduct(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue RotateAroundVector(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue Bound(GamecodeParameterList *parameters, IGamecode *gamecode);

	// Static data
	static IGamecodeEngineObject* Construct(GamecodeParameterList *parameters, IGamecode *gamecode);

	static GamecodeVector* Create(float x, float y, float z)
	{
		GamecodeVector *vec = new GamecodeVector();
		vec->GetObject()->Set(x, y, z);
		return vec;
	}

	static GamecodeVector* Create(float *v)
	{
		GamecodeVector *vec = new GamecodeVector();
		vec->GetObject()->Set(v[0], v[1], v[2]);
		return vec;
	}

	static GamecodeVector* Create(Vector3F v)
	{
		GamecodeVector *vec = new GamecodeVector();
		vec->GetObject()->Set(v);
		return vec;
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
