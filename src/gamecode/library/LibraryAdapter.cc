/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/library/LibraryAdapter.hh"
#include "gamecode/library/GamecodeVector.hh"
#include "gamecode/library/GamecodeOrientation.hh"
#include "gamecode/GamecodeException.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// IGamecodeAdapter
//-----------------------------------------------------------------------------

const std::string& LibraryAdapter::GetName()
{
	static std::string name("library");
	return name;
}

std::map<std::string, IGamecodeAdapter::AdapterMethod> LibraryAdapter::GetGlobalFunctions()
{
	return {};
}

void LibraryAdapter::OnRegister(IGamecode *gamecode)
{
	gamecode->RegisterEngineType("Vector", gamecode->CreateEngineType<GamecodeVector>()
		->Constructor(&GamecodeVector::Construct)
		->AddMethod("_OpAdd", &GamecodeVector::OpAdd)
		->AddMethod("_OpSub", &GamecodeVector::OpSub)
		->AddMethod("_OpMul", &GamecodeVector::OpMul)
		->AddMethod("_OpAddBy", &GamecodeVector::OpAddBy)
		->AddMethod("_OpSubBy", &GamecodeVector::OpSubBy)
		->AddMethod("_OpMulBy", &GamecodeVector::OpMulBy)
		->AddMethod("set", &GamecodeVector::Set)
		->AddMethod("scale", &GamecodeVector::Scale)
		->AddMethod("dotProduct", &GamecodeVector::DotProduct)
		->AddMethod("length", &GamecodeVector::Length)
		->AddMethod("normalize", &GamecodeVector::Normalize)
		->AddMethod("getNormalized", &GamecodeVector::GetNormalized)
		->AddMethod("crossProduct", &GamecodeVector::CrossProduct)
		->AddMethod("rotateAroundVector", &GamecodeVector::RotateAroundVector)
		->AddMethod("bound", &GamecodeVector::Bound)
		->AddProperty("x")
		->AddProperty("y")
		->AddProperty("z")
	);
	gamecode->RegisterEngineType("Orientation", gamecode->CreateEngineType<GamecodeOrientation>()
		->Constructor(&GamecodeOrientation::Construct)
		->AddMethod("directionToInternalSpace", &GamecodeOrientation::DirectionToInternalSpace)
		->AddMethod("directionToExternalSpace", &GamecodeOrientation::DirectionToExternalSpace)
		->AddMethod("pointToInternalSpace", &GamecodeOrientation::PointToInternalSpace)
		->AddMethod("pointToExternalSpace", &GamecodeOrientation::PointToExternalSpace)
		->AddMethod("toInternalSpaceOf", &GamecodeOrientation::ToInternalSpaceOf)
		->AddMethod("toExternalSpaceOf", &GamecodeOrientation::ToExternalSpaceOf)
		->AddMethod("rotate", &GamecodeOrientation::Rotate)
		->AddMethod("set", &GamecodeOrientation::Set)
		->AddMethod("setAngles", &GamecodeOrientation::SetAngles)
		->AddProperty("origin")
		->AddProperty("forward")
		->AddProperty("right")
		->AddProperty("up")
	);
}

//-----------------------------------------------------------------------------
// Callbacks
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
