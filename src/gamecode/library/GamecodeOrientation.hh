/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gamecode/WrapperGamecodeEngineObject.hh"
#include "gamecode/ParentGamecodeEngineObject.hh"
#include "gamecode/library/GamecodeVector.hh"
#include "library/geometry/Orientation.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class GamecodeOrientation : public WrapperGamecodeEngineObject<OrientationF>, public ParentGamecodeEngineObject {
public:
	GamecodeOrientation() :
		WrapperGamecodeEngineObject<OrientationF>(),
		forward(nullptr),
		right(nullptr),
		up(nullptr),
		origin(nullptr)
		{}

	GamecodeOrientation(OrientationF* ori) :
		WrapperGamecodeEngineObject<OrientationF>(ori),
		forward(nullptr),
		right(nullptr),
		up(nullptr),
		origin(nullptr)
		{}

	// IGamecodeEngineObject
	void SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode);
	GamecodeValue GetProperty(std::string key, IGamecode *gamecode);

	// Engine Methods
	GamecodeValue DirectionToInternalSpace(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue DirectionToExternalSpace(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue PointToInternalSpace(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue PointToExternalSpace(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue ToInternalSpaceOf(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue ToExternalSpaceOf(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue Rotate(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue Set(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue SetAngles(GamecodeParameterList *parameters, IGamecode *gamecode);

	// Static data
	static IGamecodeEngineObject* Construct(GamecodeParameterList *parameters, IGamecode *gamecode);

private:
	GamecodeVector *forward;
	GamecodeVector *right;
	GamecodeVector *up;
	GamecodeVector *origin;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
