/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#ifndef _GEOMETRY_GEOMETRYADAPTER_HH_
#define _GEOMETRY_GEOMETRYADAPTER_HH_
//-----------------------------------------------------------------------------
#include "gamecode/IGamecode.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Provides a set of wrappers around library classes,
 * e.g. Vector3F, OrientationF.
 */
class GeometryAdapter : public IGamecodeAdapter {
public:
	// IGamecodeAdapter
	const std::string& GetName();
	void ListGlobalFunctions(std::vector<const char*>* functions);
	void OnRegister(IGamecode *gamecode, GamecodeValue adapterTable);
	GamecodeValue CallFunction(int functionIndex, GamecodeParameterList *values, IGamecode* gamecode);
	void TableGarbageCollected(GamecodeValue table, IGamecode* gamecode) {}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
#endif // _GEOMETRY_GEOMETRYADAPTER_HH_
//-----------------------------------------------------------------------------

