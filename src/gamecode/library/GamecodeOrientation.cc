/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/library/GamecodeOrientation.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Static
//-----------------------------------------------------------------------------

IGamecodeEngineObject* GamecodeOrientation::Construct(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	// TODO: Support constructing in different ways.
	// This would require us being able to control construction of the object.
	return new GamecodeOrientation();
}

//-----------------------------------------------------------------------------
// Engine Methods
//-----------------------------------------------------------------------------

GamecodeValue GamecodeOrientation::DirectionToInternalSpace(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	GamecodeVector *result = new GamecodeVector();
	result->GetObject()->Set(object->DirectionToInternalSpace(*(parameters->RequireEngineType<GamecodeVector>(0, "Vector")->GetObject())));
	return GamecodeValue(result);
}

GamecodeValue GamecodeOrientation::DirectionToExternalSpace(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	GamecodeVector *result = new GamecodeVector();
	result->GetObject()->Set(object->DirectionToExternalSpace(*(parameters->RequireEngineType<GamecodeVector>(0, "Vector")->GetObject())));
	return GamecodeValue(result);
}

GamecodeValue GamecodeOrientation::PointToInternalSpace(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	GamecodeVector *result = new GamecodeVector();
	result->GetObject()->Set(object->PointToInternalSpace(*(parameters->RequireEngineType<GamecodeVector>(0, "Vector")->GetObject())));
	return GamecodeValue(result);
}

GamecodeValue GamecodeOrientation::PointToExternalSpace(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	GamecodeVector *result = new GamecodeVector();
	result->GetObject()->Set(object->PointToExternalSpace(*(parameters->RequireEngineType<GamecodeVector>(0, "Vector")->GetObject())));
	return GamecodeValue(result);
}

GamecodeValue GamecodeOrientation::ToInternalSpaceOf(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	object->ToInternalSpaceOf(*(parameters->RequireEngineType<GamecodeOrientation>(0, "GamecodeOrientation")->GetObject()));
	return GamecodeValue(this);
}

GamecodeValue GamecodeOrientation::ToExternalSpaceOf(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	object->ToExternalSpaceOf(*(parameters->RequireEngineType<GamecodeOrientation>(0, "GamecodeOrientation")->GetObject()));
	return GamecodeValue(this);
}

GamecodeValue GamecodeOrientation::Rotate(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1, 2);
	if (parameters->size() == 1)
	{
		object->Rotate(*(parameters->RequireEngineType<GamecodeVector>(0, "Vector")->GetObject()));
	}
	else
	{
		int axisNum = parameters->RequireType(0, GamecodeValue::T_Number).GetInt();
		OrientationF::Axis axis
			= (axisNum == 0) ? OrientationF::X
			: (axisNum == 1) ? OrientationF::Y
			: OrientationF::Z;
		object->Rotate(axis, parameters->RequireType(1, GamecodeValue::T_Number).GetFloat());
	}
	return GamecodeValue(this);
}

GamecodeValue GamecodeOrientation::Set(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	object->Set(*(parameters->RequireEngineType<GamecodeOrientation>(0, "Orientation")->GetObject()));
	return GamecodeValue(this);
}

GamecodeValue GamecodeOrientation::SetAngles(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	IGamecodeEngineObject *eo = parameters->RequireType(0, GamecodeValue::T_Engine).GetEngine();
	GamecodeVector *vec = dynamic_cast<GamecodeVector*>(eo);
	if (vec != nullptr)
	{
		object->SetAngles(*(vec->GetObject()));
		return GamecodeValue(this);
	}
	GamecodeOrientation *ori = dynamic_cast<GamecodeOrientation*>(eo);
	if (ori != nullptr)
	{
		object->SetAngles(*(ori->GetObject()));
		return GamecodeValue(this);
	}
	throw gamecode_exception("Parameter 0 must be Vector or Orientation");
}

//-----------------------------------------------------------------------------
// IGamecodeEngineObject
//-----------------------------------------------------------------------------

void GamecodeOrientation::SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode)
{
	// TODO: We would always have to copy the new value into our existing value,
	// which may break from expectations.
	// Call .forward.Set() instead.
	throw gamecode_exception("This key is read-only");
}

GamecodeValue GamecodeOrientation::GetProperty(std::string key, IGamecode *gamecode)
{
	// forward, right up
	if (!key.compare("forward"))
	{
		if (forward == nullptr)
		{
			forward = new GamecodeVector(&(object->v[0]));
			AddChildObject(forward);
		}
		return GamecodeValue(forward);
	}
	if (!key.compare("right"))
	{
		if (right == nullptr)
		{
			right = new GamecodeVector(&(object->v[1]));
			AddChildObject(right);
		}
		return GamecodeValue(right);
	}
	if (!key.compare("up"))
	{
		if (up == nullptr)
		{
			up = new GamecodeVector(&(object->v[2]));
			AddChildObject(up);
		}
		return GamecodeValue(up);
	}
	if (!key.compare("origin"))
	{
		if (origin == nullptr)
		{
			origin = new GamecodeVector(&(object->origin));
			AddChildObject(origin);
		}
		return GamecodeValue(origin);
	}

	throw gamecode_exception("Unknown property");
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
