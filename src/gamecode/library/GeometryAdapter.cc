/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/library/GeometryAdapter.hh"
#include "gamecode/library/GamecodeVector.hh"
#include "gamecode/library/GamecodeOrientation.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

const std::string& GeometryAdapter::GetName()
{
	static std::string name("geometry");
	return name;
}

void GeometryAdapter::ListGlobalFunctions(std::vector<const char*>* functions)
{
}

void GeometryAdapter::OnRegister(IGamecode *gamecode, GamecodeValue adapterTable)
{
	gamecode->RegisterEngineClass<VectorWrapper>();
	gamecode->RegisterEngineClass<OrientationWrapper>();
}

GamecodeValue GeometryAdapter::CallFunction(int functionIndex, GamecodeParameterList *values, IGamecode* gamecode)
{
	return GamecodeValue();
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
