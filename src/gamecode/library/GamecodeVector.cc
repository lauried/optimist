/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/library/GamecodeVector.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Construct
//-----------------------------------------------------------------------------

GamecodeVector::GamecodeVector(const GamecodeVector *other) : WrapperGamecodeEngineObject<Vector3F>()
{
	object->Set(*(other->object));
}

//-----------------------------------------------------------------------------
// Static stuff to get methods and name.
//-----------------------------------------------------------------------------

IGamecodeEngineObject* GamecodeVector::Construct(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	if (parameters->size() == 0)
	{
		return new GamecodeVector();
	}
	else if (parameters->size() == 1)
	{
		return GamecodeVector::Create(parameters->RequireEngineType<GamecodeVector>(0, "Vector")->GetObject()->v);
	}
	else if (parameters->size() == 3)
	{
		return GamecodeVector::Create(
			parameters->RequireType(0, GamecodeValue::T_Number).GetFloat(),
			parameters->RequireType(1, GamecodeValue::T_Number).GetFloat(),
			parameters->RequireType(2, GamecodeValue::T_Number).GetFloat()
		);
	}
	else
	{
		throw gamecode_exception("Vector constructor requires 0, 1 or 3 parameters");
	}
}

//-----------------------------------------------------------------------------
// IGamecodeEngineObject
//-----------------------------------------------------------------------------

void GamecodeVector::SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode)
{
	if (!key.compare("x"))
	{
		object->v[0] = val.GetFloat();
	}
	else if (!key.compare("y"))
	{
		object->v[1] = val.GetFloat();
	}
	else if (!key.compare("z"))
	{
		object->v[2] = val.GetFloat();
	}
	else
	{
		throw gamecode_exception("Unknown property");
	}
}

GamecodeValue GamecodeVector::GetProperty(std::string key, IGamecode *gamecode)
{
	if (!key.compare("x"))
	{
		return GamecodeValue(object->v[0]);
	}
	if (!key.compare("y"))
	{
		return GamecodeValue(object->v[1]);
	}
	if (!key.compare("z"))
	{
		return GamecodeValue(object->v[2]);
	}
	throw gamecode_exception("Unknown property");
}

void GamecodeVector::SetIndex(int i, GamecodeValue val, IGamecode *gamecode)
{
	if (i < 0 || i > 2)
	{
		throw gamecode_exception("Vector index out of range");
	}
	object->v[i] = val.GetFloat();
}

GamecodeValue GamecodeVector::GetIndex(int i, IGamecode *gamecode)
{
	if (i < 0 || i > 2)
	{
		throw gamecode_exception("Vector index out of range");
	}
	return GamecodeValue(object->v[i]);
}

//-----------------------------------------------------------------------------
// Engine Methods
//-----------------------------------------------------------------------------

// vector OpAdd(vector)
GamecodeValue GamecodeVector::OpAdd(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	GamecodeVector *nw = new GamecodeVector(this);
	*(nw->object) += *(parameters->RequireEngineType<GamecodeVector>(0, "Vector")->object);
	return GamecodeValue(nw);
}

// vector OpSub(vector)
GamecodeValue GamecodeVector::OpSub(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	GamecodeVector *nw = new GamecodeVector(this);
	*(nw->object) -= *(parameters->RequireEngineType<GamecodeVector>(0, "Vector")->object);
	return GamecodeValue(nw);
}

// vector OpMul(float)
GamecodeValue GamecodeVector::OpMul(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	GamecodeVector *nw = new GamecodeVector(this);
	*(nw->object) *= parameters->RequireType(0, GamecodeValue::T_Number).GetFloat();
	return GamecodeValue(nw);
}

// void OpAddBy(vector)
GamecodeValue GamecodeVector::OpAddBy(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	*object += *(parameters->RequireEngineType<GamecodeVector>(0, "Vector")->object);
	return GamecodeValue(this);
}

// void OpSubBy(vector)
GamecodeValue GamecodeVector::OpSubBy(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	*object -= *(parameters->RequireEngineType<GamecodeVector>(0, "Vector")->object);
	return GamecodeValue(this);
}

// void OpMulBy(float)
GamecodeValue GamecodeVector::OpMulBy(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	*object *= parameters->RequireType(0, GamecodeValue::T_Number).GetFloat();
	return GamecodeValue(this);
}

// void Set(float, float, float)
// void Set(vector)
GamecodeValue GamecodeVector::Set(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	if (parameters->size() == 3)
	{
		object->Set(
			parameters->RequireType(0, GamecodeValue::T_Number).GetFloat(),
			parameters->RequireType(1, GamecodeValue::T_Number).GetFloat(),
			parameters->RequireType(2, GamecodeValue::T_Number).GetFloat()
		);
		return GamecodeValue(this);
	}
	parameters->RequireParmCount(1);
	object->Set(*(parameters->RequireEngineType<GamecodeVector>(0, "Vector")->object));
	return GamecodeValue(this);
}

// void Scale(float)
GamecodeValue GamecodeVector::Scale(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	*object *= parameters->RequireType(0, GamecodeValue::T_Number).GetFloat();
	return GamecodeValue(this); // differs from mulby in that it's chainable
}

// float Dotproduct(vector)
GamecodeValue GamecodeVector::DotProduct(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	return GamecodeValue(object->DotProduct(*parameters->RequireEngineType<GamecodeVector>(0, "Vector")->object));
}

// float Length()
GamecodeValue GamecodeVector::Length(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(0);
	return GamecodeValue((float)object->Length());
}

// void Normalize()
GamecodeValue GamecodeVector::Normalize(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(0);
	object->Normalize();
	return GamecodeValue(this);
}

// vector GetNormalized()
GamecodeValue GamecodeVector::GetNormalized(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(0);
	GamecodeVector *nw = new GamecodeVector(this);
	nw->object->Normalize();
	return GamecodeValue(nw);
}

// vector CrossProduct(vector)
GamecodeValue GamecodeVector::CrossProduct(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	GamecodeVector *nw = new GamecodeVector();
	nw->object->Set(object->CrossProduct(*(parameters->RequireEngineType<GamecodeVector>(0, "Vector")->object)));
	return GamecodeValue(nw);
}

// vector RotateAroundVector(vector, float)
GamecodeValue GamecodeVector::RotateAroundVector(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(2);
	GamecodeVector *nw = new GamecodeVector();
	nw->object->Set(object->RotateAroundVector(
		*(parameters->RequireEngineType<GamecodeVector>(0, "Vector")->object),
		parameters->RequireType(1, GamecodeValue::T_Number).GetFloat()
	));
	return GamecodeValue(nw);
}

// void Bound(float, float)
GamecodeValue GamecodeVector::Bound(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(2);
	object->Bound(
		parameters->RequireType(0, GamecodeValue::T_Number).GetFloat(),
		parameters->RequireType(1, GamecodeValue::T_Number).GetFloat()
	);
	return GamecodeValue(this);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
