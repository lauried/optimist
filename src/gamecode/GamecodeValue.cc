/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/GamecodeValue.hh"
#include "gamecode/IGamecodeEngineObject.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class NullTable : public IGamecodeTable {
public:
	GamecodeValue Get(GamecodeValue key) { return GamecodeValue(); }

	int NumKeys() { return 0; }
	GamecodeValue GetKey(int i) { return GamecodeValue(); }
	GamecodeValue GetValue(int i) { return GamecodeValue(); }

	void Set(GamecodeValue key, GamecodeValue value) {}

	bool AllowStringKey() { return true; }
};

class NullFunction : public IGamecodeFunction {
public:
	GamecodeValue CallFunction(std::string &name, std::vector<GamecodeValue> &values, IGamecode* gamecode, GamecodeValue thisObject) { return GamecodeValue(); }
};

class NullEngineObject : public IGamecodeEngineObject {
public:
	void SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode) {}
	GamecodeValue GetProperty(std::string key, IGamecode *gamecode) { return GamecodeValue(); }
	void SetIndex(int i, GamecodeValue val, IGamecode *gamecode) {}
	GamecodeValue GetIndex(int i, IGamecode *gamecode) { return GamecodeValue(); }
	GamecodeValue CallMethod(int methodIndex, std::vector<GamecodeValue> &values, IGamecode *gamecode) { return GamecodeValue(); }
	void Destruct() {}
	~NullEngineObject() {}
};

//-----------------------------------------------------------------------------
// GamecodeValue
//-----------------------------------------------------------------------------

GamecodeValue::GamecodeValue()
{
	type = T_Null;
}

GamecodeValue::GamecodeValue(const GamecodeValue &other)
{
	type = other.type;
	switch (type)
	{
	case T_Bool:
	case T_Number:
		value.v_double = other.value.v_double;
		break;
	case T_String:
		value.v_string = other.value.v_string;
		break;
	case T_Table:
		value.v_table = other.value.v_table;
		break;
	case T_Function:
		value.v_function = other.value.v_function;
		break;
	case T_Engine:
		value.v_engine = other.value.v_engine;
	}
}

GamecodeValue::GamecodeValue(bool b)
{
	type = T_Bool;
	value.v_double = b ? 1 : 0;
}

GamecodeValue::GamecodeValue(int i)
{
	type = T_Number;
	value.v_double = i;
}

GamecodeValue::GamecodeValue(float f)
{
	type = T_Number;
	value.v_double = f;
}

GamecodeValue::GamecodeValue(double d)
{
	type = T_Number;
	value.v_double = d;
}

GamecodeValue::GamecodeValue(IGamecodeString* string)
{
	type = T_String;
	value.v_string = string;
}

GamecodeValue::GamecodeValue(IGamecodeTable* table)
{
	type = T_Table;
	value.v_table = table;
}

GamecodeValue::GamecodeValue(IGamecodeFunction* function)
{
	type = T_Function;
	value.v_function = function;
}

GamecodeValue::GamecodeValue(IGamecodeEngineObject* engine)
{
	type = T_Engine;
	value.v_engine = engine;
}

GamecodeValue::~GamecodeValue()
{
}

void GamecodeValue::operator=(const GamecodeValue &val)
{
	type = val.type;
	switch(type)
	{
	case T_Bool:
	case T_Number:
		value.v_double = val.value.v_double;
		break;
	case T_String:
		value.v_string = val.value.v_string;
		break;
	case T_Table:
		value.v_table = val.value.v_table;
		break;
	case T_Function:
		value.v_function = val.value.v_function;
		break;
	case T_Engine:
		value.v_engine = val.value.v_engine;
	}
}

bool GamecodeValue::GetBool()
{
	switch (GetType())
	{
	case T_Bool:
	case T_Number:
		return value.v_double != 0;
	default:
		return false;
	}
}

int GamecodeValue::GetInt()
{
	switch (GetType())
	{
	case T_Bool:
	case T_Number:
		return value.v_double;
	default:
		return 0;
	}
}

float GamecodeValue::GetFloat()
{
	switch (GetType())
	{
	case T_Bool:
	case T_Number:
		return value.v_double;
	default:
		return 0;
	}
}

double GamecodeValue::GetDouble()
{
	switch(GetType())
	{
	case T_Bool:
	case T_Number:
		return value.v_double;
	default:
		return 0;
	}
}

const char* GamecodeValue::GetString()
{
	static const char* nullString = "";
	switch (GetType())
	{
	case T_String:
		return value.v_string->Get();
	default:
		return nullString;
	}
}

IGamecodeTable* GamecodeValue::GetTable()
{
	static NullTable nullTable;
	switch (GetType())
	{
	case T_Table:
		return value.v_table;
	default:
		return &nullTable;
	}
}

IGamecodeFunction* GamecodeValue::GetFunction()
{
	static NullFunction nullFunction;
	switch (GetType())
	{
	case T_Function:
		return value.v_function;
	default:
		return &nullFunction;
	}
}

IGamecodeEngineObject* GamecodeValue::GetEngine()
{
	static NullEngineObject nullEngineObject;
	switch (GetType())
	{
	case T_Engine:
		return value.v_engine;
	default:
		return &nullEngineObject;
	}
}

IGamecodeString* GamecodeValue::GetStringObject()
{
	switch (GetType())
	{
	case T_String:
		return value.v_string;
	default:
		return 0;
	}
}

bool GamecodeValue::operator==(const GamecodeValue& other)
{
	if (type != other.type)
	{
		return false;
	}
	switch (type)
	{
	case T_Null:
		return true;
	case T_Bool:
	case T_Number:
		return value.v_double == other.value.v_double;
	case T_String:
		return value.v_string == other.value.v_string;
	case T_Table:
		return value.v_table == other.value.v_table;
	case T_Function:
		return value.v_function == other.value.v_function;
	case T_Engine:
		return value.v_engine == other.value.v_engine;
	}
	return false;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
