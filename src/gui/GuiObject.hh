/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gui/document/GuiDocumentNode.hh"
#include <vector>
#include "library/text/String.hh"
#include "library/geometry/Box2.hh"
#include "library/geometry/Vector2.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class GuiObject : public GuiDocumentNode {
public:
	struct PositionInfo {
		float offset;
		float fraction;
		bool set;

		PositionInfo() : offset(0), fraction(0), set(false) {}
		void Set(float ofs, float frac = 0) { offset = ofs; fraction = frac; set = true; }
		void Set(std::string value);
		float Get(float size) { return offset + size * fraction; }
	};

	enum EdgeFlags {
		E_NONE   = 0,
		E_TOP    = 1,
		E_LEFT   = 2,
		E_BOTTOM = 4,
		E_RIGHT  = 8,
		E_ALL    = 15,
	};

	enum LayoutType {
		L_ABSOLUTE = 0,
		L_GRID = 1,
		L_TOOLBAR = 2,
		L_SCROLL = 3,
	};

	enum AxisFlags {
		A_NONE = 0,
		A_HORIZONTAL = 1,
		A_VERTICAL = 2,
		A_BOTH = 3,
	};

	enum Direction {
		D_LEFT = 0,
		D_DOWN = 1,
		D_RIGHT = 2,
		D_UP = 3,
	};

	LayoutType layout;
	PositionInfo top, left, bottom, right, width, height;
	Box2F relativePosition;

	GuiObject() : layout(L_ABSOLUTE) {}
	~GuiObject();

	/**
	 * Update this object's properties from the document node part.
	 */
	void UpdateFromDocumentNode();

	/**
	 * Set our position relative to our parent, while also doing the same for
	 * all our child elements.
	 * We may ignore the parent dimensions and bounds and the parent may
	 * override what we've seet.
	 *
	 * @param Vector2F dimensions The dimensions of the parent, or the
	 *                            dimensions that the parent wants to fit the
	 *                            child element into.
	 * @param int variableEdges The edges of the parent bounds that the
	 *                          child element is allowed to exceed.
	 *                          Composed from EdgeFlags.
	 */
	void RecursiveResize(Vector2F parentDimensions, int variableEdges);

private:
	/**
	 * Reads top/left/bottom/right/width/height/variableEdges and sets our
	 * relative position.
	 */
	void SetRelativePosition(Vector2F parentDimensions, int variableEdges);
	void GetChildObjects(std::vector<GuiObject*> *objects);
	int GetAxes(std::string attribute);
	int GetEdge(std::string attribute, int defaultValue);
	float GetFloat(std::string attribute, float defaultValue = 0.0f);
	int GetDirection(std::string attribute, int defaultValue);

	int EdgeForDirection(int direction)
	{
		switch (direction)
		{
			case D_LEFT: return E_LEFT;
			case D_DOWN: return E_BOTTOM;
			case D_RIGHT: return E_RIGHT;
			case D_UP: return E_TOP;
		}
		return E_NONE;
	}

	int EdgesForAxes(int axes)
	{
		switch (axes)
		{
			case A_NONE: return E_NONE;
			case A_HORIZONTAL: return E_LEFT | E_RIGHT;
			case A_VERTICAL: return E_TOP | E_BOTTOM;
			case A_BOTH: return E_ALL;
		}
		return E_NONE;
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

