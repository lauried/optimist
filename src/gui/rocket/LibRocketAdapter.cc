/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "gui/rocket/LibRocketAdapter.hh"
#include "Rocket/Core/Core.h"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

LibRocketAdapter::LibRocketAdapter(RocketInterface *interface, IPlatform *platform)
	: name("Rocket")
	, platform(platform)
	, context(nullptr)
{
	Rocket::Core::String contextName("default");
	context = new Rocket::Core::Context(contextName);
	Vector2I screenSize = platform->GetScreenSize();
	context->SetDimensions({ screenSize[0], screenSize[1] });
}

LibRocketAdapter::~LibRocketAdapter()
{
	context->RemoveReference();
}

// Apparently Rocket has its own document stack but I've no idea what effect
// it has for documents to be stacked.
// We could probably make a gamecode document class that wraps its methods,
// and give gamecode a fair bit of control.

// TODO:
// Get single-document-mode to work.
// Get rendering and fonts to work, maybe creating our own font wrapper.
// Handle events.
// Figure out how to detect events in rocket and let gamecode set up callbacks for them.
// Set up document object for full control by gamecode, remove the basic methods and just let gamecode manage documents.

void LibRocketAdapter::HandleEvent(PlatformEvent &event, IPlatform::EventState &state)
{
	if (event.type == PlatformEvent::PE_WindowResize)
	{
		Vector2I screenSize = platform->GetScreenSize();
		context->SetDimensions({ screenSize[0], screenSize[1] });
		return;
	}
	// TODO: Intercept input events if there's a gui taking input
	// TODO: Always allow release events to pass through
}

void LibRocketAdapter::Draw()
{
	if (documents.size() > 0)
	{
		context->Update();
		context->Render();
	}
}

std::map<std::string, IGamecodeAdapter::AdapterMethod> LibRocketAdapter::GetGlobalFunctions()
{
	return {
		// bool loadDocument(filename)
		{ "pushDocument", std::bind(&LibRocketAdapter::PushDocument, this, std::placeholders::_1, std::placeholders::_2) },

		// bool popDocument()
		{ "popDocument", std::bind(&LibRocketAdapter::PopDocument, this, std::placeholders::_1, std::placeholders::_2) },
	};
}

void LibRocketAdapter::OnRegister(IGamecode *gamecode)
{
}

GamecodeValue LibRocketAdapter::PushDocument(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(1);
	std::string filename(values->RequireType(0, GamecodeValue::T_String).GetString());
	Rocket::Core::ElementDocument* document = context->LoadDocument(filename.c_str());
	if (document == nullptr)
	{
		return GamecodeValue(false);
	}
	if (documents.size() > 0)
	{
		documents.top()->Hide();
	}
	document->Show();
	documents.push(document);
	return GamecodeValue(true);
}

GamecodeValue LibRocketAdapter::PopDocument(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(0);
	if (documents.size() < 1)
	{
		return GamecodeValue(false);
	}
	Rocket::Core::ElementDocument* document = documents.top();
	document->GetContext()->UnloadDocument(document);
	documents.pop();
	if (documents.size() > 0)
	{
		documents.top()->Show();
	}
	return GamecodeValue(true);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
