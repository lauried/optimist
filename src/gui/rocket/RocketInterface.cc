/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "gui/rocket/RocketInterface.hh"
#include "library/SingleInstanceException.hh"
#include "Rocket/Core/Core.h"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

static RocketInterface *instance = nullptr;

RocketInterface::RocketInterface(IPlatform *platform, ILogTarget *log, IFilesystem *filesystem, IRenderCore *renderer, ResourceManager *resources)
	: system(platform, log)
	, file(filesystem)
	, render(renderer, resources)
{
	RequireSingleInstance();

	Rocket::Core::SetSystemInterface(GetSystem());
	Rocket::Core::SetFileInterface(GetFile());
	Rocket::Core::SetRenderInterface(GetRender());
	Rocket::Core::Initialise();
}

RocketInterface::~RocketInterface()
{
	Rocket::Core::Shutdown();
}

void RocketInterface::RequireSingleInstance()
{
	if (instance != nullptr)
	{
		throw std::logic_error("Only a single instance of RocketInterface may exist");
	}
	instance = this;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
