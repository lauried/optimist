/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "filesystem/IFilesystem.hh"
#include "Rocket/Core/FileInterface.h"
//-----------------------------------------------------------------------------
namespace opti {
	using Rocket::Core::FileHandle;
	using Rocket::Core::String;
//-----------------------------------------------------------------------------

class RocketFile : public Rocket::Core::FileInterface {
public:
	RocketFile(IFilesystem *filesystem) : filesystem(filesystem) {}

	/// Opens a file.
	/// @param file The file handle to write to.
	/// @return A valid file handle, or NULL on failure
	virtual FileHandle Open(const String& path);

	/// Closes a previously opened file.
	/// @param file The file handle previously opened through Open().
	virtual void Close(FileHandle file);

	/// Reads data from a previously opened file.
	/// @param buffer The buffer to be read into.
	/// @param size The number of bytes to read into the buffer.
	/// @param file The handle of the file.
	/// @return The total number of bytes read into the buffer.
	virtual size_t Read(void* buffer, size_t size, FileHandle file);

	/// Seeks to a point in a previously opened file.
	/// @param file The handle of the file to seek.
	/// @param offset The number of bytes to seek.
	/// @param origin One of either SEEK_SET (seek from the beginning of the file), SEEK_END (seek from the end of the file) or SEEK_CUR (seek from the current file position).
	/// @return True if the operation completed successfully, false otherwise.
	virtual bool Seek(FileHandle file, long offset, int origin);

	/// Returns the current position of the file pointer.
	/// @param file The handle of the file to be queried.
	/// @return The number of bytes from the origin of the file.
	virtual size_t Tell(FileHandle file);

	/// Returns the length of the file.
	/// The default implementation uses Seek & Tell.
	/// @param file The handle of the file to be queried.
	/// @return The length of the file in bytes.
	virtual size_t Length(FileHandle file);

	/// Called when this file interface is released.
	virtual void Release();

private:
	IFilesystem *filesystem;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

