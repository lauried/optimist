/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include <string>
#include <iostream>
#include "gui/rocket/RocketRender.hh"
#include "library/graphics/Image.hh"
#include "renderer/MeshData.hh"
#include "resources/ImageResource.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

void RocketRender::RenderGeometry(Vertex* vertices, int num_vertices, int* indices, int num_indices, TextureHandle texture, const Vector2f& translation)
{
std::cout << "rendergeometry" << std::endl;
	size_t step = sizeof(Vertex) / sizeof(float);

	MeshData mesh;
	mesh.verts.data.f = &(vertices[0].position[0]);
	mesh.verts.components = 2;
	mesh.verts.step = step;
	mesh.colours.type = MeshData::UCHAR;
	mesh.colours.data.c = &(vertices[0].colour[0]);
	mesh.colours.components = 4;
	mesh.colours.step = step;
	mesh.texcoords.data.f = &(vertices[0].tex_coord[0]);
	mesh.texcoords.components = 2;
	mesh.texcoords.step = step;
	mesh.indices = indices;
	mesh.numIndices = num_indices;

	Material material;

	ITexture *realTexture = (ITexture*)texture;
	if (realTexture != nullptr)
	{
		material.texInfo.texture = (ITexture*)texture;
	}

	// TODO: Nicer to have a facade class to help with 2D rendering.
	OrientationF originalMV;
	OrientationF newMV;
	originalMV.Set(renderCore->GetModelview());
	newMV.Set(originalMV);
	newMV.origin += Vector3F(translation[0], translation[1], 0);

	renderCore->SetModelview(newMV);
	renderCore->SetMaterial(material);
	renderCore->DrawMesh(mesh);
	renderCore->SetModelview(originalMV);
}

void RocketRender::EnableScissorRegion(bool enable)
{
	return;
	if (enable)
	{
		renderCore->GetViewport(&viewportTopLeft, &viewportBottomRight);
	}
	else
	{
		renderCore->SetViewport(viewportTopLeft, viewportBottomRight);
	}
}

void RocketRender::SetScissorRegion(int x, int y, int width, int height)
{
	return;

	// Note: top, left, width, height
	// Presumably these coordinates use the same coordinate system
	// as Rocket::Core::Context::SetDimensions().
	Vector2I topLeft(x, y);
	Vector2I bottomRight(width, height);
	bottomRight += topLeft;
	renderCore->SetViewport(topLeft, bottomRight);
}

bool RocketRender::LoadTexture(TextureHandle& texture_handle, Vector2i& texture_dimensions, const String& source)
{
	// Note: We can't load asynchronously because we need to know the dimensions.
	ResourceManager::ResourcePointer<ImageResource> imagePtr = resources->GetResource<ImageResource>(std::string(source.CString()), ResourceManager::RF_FORCE_IMMEDIATE);
	Image *image = imagePtr.GetResource()->GetImage();
	texture_dimensions[0] = image->Width();
	texture_dimensions[1] = image->Height();

	ITexture *texture = renderCore->CreateTexture();
	texture->SetImage(image);
	texture_handle = (TextureHandle)texture;

	return true;
}

bool RocketRender::GenerateTexture(TextureHandle& texture_handle, const byte* source, const Vector2i& source_dimensions)
{
	// Note: source is always RGBA 1 byte per channel.
	Image image;
	image.Set(source_dimensions[0], source_dimensions[1], (uint8_t*)source, 4);

	ITexture *texture = renderCore->CreateTexture();
	texture->SetImage(&image);

	texture_handle = (TextureHandle)texture;

	return true;
}

void RocketRender::ReleaseTexture(TextureHandle texture)
{
	renderCore->FreeTexture((ITexture*)texture);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
