/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "gui/rocket/RocketSystem.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

float RocketSystem::GetElapsedTime()
{
	return (float)((double)platform->GetTime() / 1024.0);
}

bool RocketSystem::LogMessage(Log::Type type, const String& message)
{
	ILogTarget::MessageType messageType = ILogTarget::NOTIFICATION;
	switch (type) {
	case Log::LT_ERROR:
	case Log::LT_ASSERT:
		messageType = ILogTarget::ERROR;
		break;
	case Log::LT_WARNING:
		messageType = ILogTarget::WARNING;
		break;
	}
	log->Start(messageType)->Code(type)->Write(message.CString())->End();
	return true;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
