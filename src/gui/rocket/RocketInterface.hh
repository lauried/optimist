/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gui/rocket/RocketSystem.hh"
#include "gui/rocket/RocketFile.hh"
#include "gui/rocket/RocketRender.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * LibRocket only allows for its interfaces to be applied globally.
 * We can't be sure if internally things will be muddled if we switch them
 * for different contexts, so only one instance of this class can be created.
 */
class RocketInterface {
public:
	RocketInterface(IPlatform *platform, ILogTarget *log, IFilesystem *filesystem, IRenderCore *renderer, ResourceManager *resources);
	~RocketInterface();

	RocketSystem *GetSystem() { return &system; }
	RocketFile *GetFile() { return &file; }
	RocketRender *GetRender() { return &render; }

private:
	RocketSystem system;
	RocketFile file;
	RocketRender render;

	void RequireSingleInstance();
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

