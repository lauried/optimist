/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------

//## add_source_dirs src/gui/rocket/librocket src/gui/rocket/librocket/Include

#include <stack>
#include "gamecode/IGamecodeAdapter.hh"
#include "gui/rocket/RocketInterface.hh"
#include "Rocket/Core/Context.h"
#include "platform/IPlatform.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class LibRocketAdapter : public IGamecodeAdapter, public IPlatform::EventListener {
public:
	LibRocketAdapter(RocketInterface *interface, IPlatform *platform);
	~LibRocketAdapter();

	// IPlatformEventListener
	virtual void HandleEvent(PlatformEvent &event, IPlatform::EventState &state);

	// IGamecodeAdapter
	virtual const std::string& GetName() { return name; }
	virtual std::map<std::string, IGamecodeAdapter::AdapterMethod> GetGlobalFunctions();
	virtual void OnRegister(IGamecode *gamecode);

	// Gamecode methods
	GamecodeValue PushDocument(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue PopDocument(GamecodeParameterList *values, IGamecode* gamecode);

	void Draw();

private:
	std::string name;
	IPlatform *platform;
	Rocket::Core::Context *context;
	std::stack<Rocket::Core::ElementDocument*> documents;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

