/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "Rocket/Core/SystemInterface.h"
#include "platform/IPlatform.hh"
#include "log/ILogTarget.hh"
//-----------------------------------------------------------------------------
namespace opti {
	using Rocket::Core::Log;
	using Rocket::Core::String;
//-----------------------------------------------------------------------------

class RocketSystem : public Rocket::Core::SystemInterface {
public:
	RocketSystem(IPlatform *platform, ILogTarget *log) : platform(platform), log(log) {}

	/// Get the number of seconds elapsed since the start of the application.
	/// @return Elapsed time, in seconds.
	virtual float GetElapsedTime();

	/// Log the specified message.
	/// @param[in] type Type of log message, ERROR, WARNING, etc.
	/// @param[in] message Message to log.
	/// @return True to continue execution, false to break into the debugger.
	virtual bool LogMessage(Log::Type type, const String& message);

private:
	IPlatform *platform;
	ILogTarget *log;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

