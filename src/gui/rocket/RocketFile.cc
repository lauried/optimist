/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "gui/rocket/RocketFile.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

FileHandle RocketFile::Open(const String& path)
{
	return (FileHandle)filesystem->Open(path.CString());
}

void RocketFile::Close(FileHandle file)
{
	filesystem->Close((IFile*)file);
}

size_t RocketFile::Read(void* buffer, size_t size, FileHandle file)
{
	return ((IFile*)file)->ReadBytes((char*)buffer, size);
}

bool RocketFile::Seek(FileHandle file, long offset, int origin)
{
	IFile::SeekType type = IFile::T_Start;
	switch (origin) {
	case SEEK_END:
		type = IFile::T_End;
		break;
	case SEEK_CUR:
		type = IFile::T_Relative;
		break;
	}
	((IFile*)file)->Seek(offset, type);
	return true;
}

size_t RocketFile::Tell(FileHandle file)
{
	return ((IFile*)file)->Tell(IFile::T_Start);
}

size_t RocketFile::Length(FileHandle file)
{
	return ((IFile*)file)->Size();
}

void RocketFile::Release()
{
	// We don't need to do anything here.
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
