/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "gui/document/XmlDocumentParser.hh"
#include <cstring>
#include <string>
#include <stack>
#include <iostream>
#include "library/text/Tokenizer.hh"
#include "library/text/TokenParser.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

struct EscapedTokenType : public TokenType {
	std::string start;
	std::string end;

	EscapedTokenType(std::string start, std::string end) : start(start), end(end) {}

	int TokenLength(const char *buffer, int maxLength)
	{
		if (maxLength < 7 || std::strncmp(buffer, start.c_str(), start.length()) != 0)
		{
			return 0;
		}
		const char *pos = std::strstr(&buffer[start.length()], end.c_str());
		if (pos == nullptr)
		{
			return 0;
		}
		return (pos - buffer) + end.length();
	}
};

void XmlDocumentParser::ParseDocument(GuiDocument *document, const char *data, int dataLength)
{
	Tokenizer tokenizer;
	// State 0: text
	tokenizer.AddTokenType(0, 0, "escaped", new EscapedTokenType("<?", "?>"), Tokenizer::SKIP);
	tokenizer.AddTokenType(0, 0, "comment", new EscapedTokenType("<!--", "-->"), Tokenizer::SKIP);
	tokenizer.AddTokenType(0, 1, "startTag", new RegexTokenType("<[a-zA-Z_][a-zA-Z0-9_]+"));
	tokenizer.AddTokenType(0, 0, "closeTag", new RegexTokenType("</[a-zA-Z_][a-zA-Z0-9_]+>"));
	tokenizer.AddTokenType(0, 0, "entity", new RegexTokenType("&[^;<>&]+;"));
	tokenizer.AddTokenType(0, 0, "text", new RegexTokenType("[^<&]+"));
	tokenizer.AddTokenType(0, 0, "unknown", new RegexTokenType("."), Tokenizer::SKIP); // or not skip and make it text, to handle things gracefully.
	// State 1: tag
	tokenizer.AddTokenType(1, 1, "identifier", new RegexTokenType("[a-zA-Z_][a-zA-Z0-9_-]+"));
	tokenizer.AddTokenType(1, 1, "quotedString", new RegexTokenType("\"[^\"]+\""));
	tokenizer.AddTokenType(1, 1, "equals", new StringMatchTokenType("="));
	tokenizer.AddTokenType(1, 0, "selfClose", new StringMatchTokenType("/>"));
	tokenizer.AddTokenType(1, 0, "endTag", new StringMatchTokenType(">"));
	tokenizer.AddTokenType(1, 1, "whitespace", new RegexTokenType("[\\r\\n\\t ]+"), Tokenizer::SKIP);
	tokenizer.AddTokenType(1, 1, "unknown", new RegexTokenType("."), Tokenizer::SKIP);

	TokenParser parser(data, dataLength, &tokenizer);
	std::stack<GuiDocumentNode*> stack;
	GuiDocumentNode* currentNode = document->CreateNode();
	GuiDocumentNode* initialContainer = currentNode;

	while (!parser.Eof())
	{
		Token token = parser.GetToken();
		if (!token.typeId.compare("text"))
		{
			// Append text node to current node, creating dummy node if needed.
		}
		else if (!token.typeId.compare("entity"))
		{
			// Append entity node to current node, creating dummy node if needed.
		}
		else if (!token.typeId.compare("startTag"))
		{
			// Create a node and append to current node if present.
			GuiDocumentNode* newNode = document->CreateNode();
			currentNode->AppendChild(newNode);
			// Get name from the start tag token.
			newNode->SetType(token.str().substr(1));
			// Expect attributes until selfClose or endTag.
			for (token = parser.GetToken(); !token.typeId.compare("identifier"); token = parser.GetToken())
			{
				std::string key = token.str();
				if (!parser.PeekToken()->typeId.compare("equals"))
				{
					parser.ExpectTokenType("equals");
					std::string value = parser.ExpectTokenType("quotedString").str();
					newNode->SetAttribute(key, value);
				}
				else
				{
					newNode->SetAttribute(key, key);
				}
			}
			// If there was an endtag, then push the currentnode and put this node there instead.
			// Then until a closing tag we'll be appending to this node instead of the parent.
			if (!token.typeId.compare("endTag"))
			{
				stack.push(currentNode);
				currentNode = newNode;
			}

			if (!token.typeId.compare("selfClose"))
			{
			}
		}
		else if (!token.typeId.compare("closeTag"))
		{
			// Get name from the close tag token.
			std::string name = token.str();
			name = name.substr(2, name.length() - 3);
			// If the name matches current node, then pop the stack.
			if (stack.size() > 0 && !name.compare(currentNode->GetType()))
			{
				currentNode = stack.top();
				stack.pop();
			}
		}
		// Skip it if we didn't recognise it.
	}

	// If our temporary wrapper just contained a single node, unwrap it.
	if (initialContainer->GetChildren().size() == 1)
	{
		GuiDocumentNode *newRoot = initialContainer->GetChildren().front();
		document->SetRoot(newRoot);
		// TODO: Should we make document setroot automatically do this?
		initialContainer->RemoveChild(newRoot);
		document->DeleteNode(initialContainer);
	}
	else
	{
		document->SetRoot(initialContainer);
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
