/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <string>
#include <list>
#include <set>
#include <map>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class GuiDocumentNode {
public:
	GuiDocumentNode() : parent(nullptr), type("") {}
	virtual ~GuiDocumentNode();

	void SetType(std::string &name) { type = name; }
	void SetType(std::string name) { type = name; }
	std::string& GetType() { return type; }

	void AddClass(std::string &name);
	bool HasClass(std::string &name);
	void RemoveClass(std::string &name);

	void SetAttribute(std::string &name, std::string &value);
	bool HasAttribute(std::string &name);
	const std::string& GetAttribute(std::string name);

	void AppendChild(GuiDocumentNode *element);
	void RemoveChild(GuiDocumentNode *element);
	std::list<GuiDocumentNode*>& GetChildren() { return children; }

protected:
	GuiDocumentNode* parent;
	std::list<GuiDocumentNode*> children;

private:
	std::string type;
	std::set<std::string> classes;
	std::map<std::string, std::string> attributes;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

