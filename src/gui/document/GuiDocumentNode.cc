/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "gui/document/GuiDocumentNode.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

GuiDocumentNode::~GuiDocumentNode()
{
}

void GuiDocumentNode::AddClass(std::string &name)
{
	classes.insert(name);
}

bool GuiDocumentNode::HasClass(std::string &name)
{
	return classes.find(name) != classes.end();
}

void GuiDocumentNode::RemoveClass(std::string &name)
{
	auto itClass = classes.find(name);
	if (itClass != classes.end())
	{
		classes.erase(itClass);
	}
}

void GuiDocumentNode::SetAttribute(std::string &name, std::string &value)
{
	auto itAttribute = attributes.find(name);
	if (itAttribute != attributes.end())
	{
		itAttribute->second = value;
		return;
	}
	attributes.insert(std::make_pair(name, value));
}

bool GuiDocumentNode::HasAttribute(std::string &name)
{
	return attributes.find(name) != attributes.end();
}

const std::string& GuiDocumentNode::GetAttribute(std::string name)
{
	static std::string blank;

	auto itAttribute = attributes.find(name);
	if (itAttribute != attributes.end())
	{
		return itAttribute->second;
	}
	return blank;
}

void GuiDocumentNode::AppendChild(GuiDocumentNode *element)
{
	if (element == this)
	{
		throw new std::logic_error("Cannot append a node to itself");
	}
	if (element->parent != nullptr)
	{
		element->parent->RemoveChild(element);
	}
	RemoveChild(element);
	children.push_back(element);
	element->parent = this;
}

void GuiDocumentNode::RemoveChild(GuiDocumentNode *element)
{
	element->parent = nullptr;
	children.remove(element);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
