/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gui/GuiObject.hh"
#include "gui/document/IDocumentNodeAllocator.hh"
#include "gui/document/GuiDocument.hh"
#include "gui/document/GuiDocumentNode.hh"
#include "library/geometry/Vector2.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class Gui : public IDocumentNodeAllocator {
public:
	Gui() : document(this), rootObject(nullptr), layoutRequired(false) {}

	void LoadDocument(const char *data, size_t length);
	void SetSize(Vector2F size);

	// IDocumentNodeAllocator
	virtual GuiDocumentNode *CreateNode() { return new GuiObject(); }
	virtual void DeleteNode(GuiDocumentNode *node) { delete node; }

private:
	GuiDocument document;
	GuiObject *rootObject;
	bool layoutRequired;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

