/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "gui/GuiObject.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

GuiObject::~GuiObject()
{
}

void GuiObject::PositionInfo::Set(std::string value)
{
	// leave null if empty
	if (value.length() < 1)
	{
		return;
	}
	std::vector<std::string> tokens;
	String::Tokenize(value, &tokens);
	if (tokens.size() < 1)
	{
		return;
	}
	offset = String::ToFloat(tokens[0]);
	if (tokens.size() < 2)
	{
		return;
	}
	if (tokens[1][tokens[1].length() - 1] == '%')
	{
		fraction = String::ToFloat(tokens[1].substr(0, -1)) / 100.0f;
	}
	else
	{
		fraction = String::ToFloat(tokens[1]);
	}
}

void GuiObject::UpdateFromDocumentNode()
{
	std::string type = GetType();
	layout = L_ABSOLUTE;
	if (!type.compare("grid"))
	{
		layout = L_GRID;
	}
	if (!type.compare("toolbar"))
	{
		layout = L_TOOLBAR;
	}
	if (!type.compare("scroll"))
	{
		layout = L_SCROLL;
	}
	top.Set(GetAttribute("top"));
	right.Set(GetAttribute("right"));
	bottom.Set(GetAttribute("bottom"));
	left.Set(GetAttribute("left"));
	width.Set(GetAttribute("width"));
	height.Set(GetAttribute("height"));
}

void GuiObject::RecursiveResize(Vector2F parentDimensions, int variableEdges)
{
	UpdateFromDocumentNode();
	SetRelativePosition(parentDimensions, variableEdges);
	Vector2F ourBounds = relativePosition.maxs - relativePosition.mins;
	std::vector<GuiObject*> children;
	GetChildObjects(&children);

	// TODO: Make layouts into strategy classes.

	switch (layout)
	{
		case L_ABSOLUTE:
			// Pass our dimensions to the child elements.
			// Accept the child element's dimensions.
			for (auto it = children.begin(); it != children.end(); ++it)
			{
				(*it)->RecursiveResize(ourBounds, E_ALL);
			}
			break;
		case L_GRID:
			{
				Vector2F cellSize(GetFloat("cellwidth"), GetFloat("cellheight"));
				int primaryEdge = GetEdge("primaryedge", E_TOP);
				int secondaryEdge = GetEdge("secondaryedge", E_LEFT);
				// basically determined by primary and secondary edges
				Vector2F startPosition;
				if (primaryEdge == E_RIGHT || secondaryEdge == E_RIGHT)
				{
					startPosition[0] = ourBounds[0] - cellSize[0];
				}
				if (primaryEdge == E_BOTTOM || secondaryEdge == E_BOTTOM)
				{
					startPosition[1] = ourBounds[1] - cellSize[1];
				}
				// direction away from primary edge
				Vector2F primaryStep;
				switch (primaryEdge)
				{
					case E_TOP:
						primaryStep[1] = cellSize[1];
						break;
					case E_BOTTOM:
						primaryStep[1] = -cellSize[1];
						break;
					case E_LEFT:
						primaryStep[0] = cellSize[0];
						break;
					case E_RIGHT:
						primaryStep[0] = -cellSize[0];
				}
				// direction away from secondary edge
				Vector2F secondaryStep;
				switch (secondaryEdge)
				{
					case E_TOP:
						secondaryStep[1] = cellSize[1];
						break;
					case E_BOTTOM:
						secondaryStep[1] = -cellSize[1];
						break;
					case E_LEFT:
						secondaryStep[0] = cellSize[0];
						break;
					case E_RIGHT:
						secondaryStep[0] = -cellSize[0];
				}
				// if we were doing tl -> r, this would be current column
				int primaryIndex = 0;
				// if we were doing tl -> r, this would be number of columns
				int primaryMax = (primaryEdge == E_TOP || primaryEdge == E_BOTTOM) ? floor(ourBounds[0] / cellSize[0]) : floor(ourBounds[1] / cellSize[1]);
				// if we were doing tl -> r, this would be current row
				int secondaryIndex = 0;
				for (auto it = children.begin(); it != children.end(); ++it)
				{
					GuiObject *child = *it;
					// Pass cell size to child elements.
					child->RecursiveResize(cellSize, E_NONE);
					// Reposition child elements on the grid.
					child->relativePosition.SetOffset(primaryStep * primaryIndex + secondaryStep * secondaryIndex);
					primaryIndex += 1;
					if (primaryIndex >= primaryMax)
					{
						primaryIndex = 0;
						secondaryIndex += 1;
					}
				}
			}
			break;
		case L_TOOLBAR:
			{
				int direction = GetDirection("direction", D_UP);
				int freeAxis = (direction & D_UP & D_DOWN) ? 1 : 0;
				int fixedAxis = (freeAxis == 0) ? 1 : 0;
				// Save our free axis and set it to the fixed axis.
				float freeAxisSize = ourBounds[freeAxis];
				ourBounds[freeAxis] = ourBounds[fixedAxis];
				Vector2F offset;
				for (auto it = children.begin(); it != children.end(); ++it)
				{
					GuiObject *child = *it;
					// Pass our size to child elements, specifying the free axis.
					child->RecursiveResize(ourBounds, EdgeForDirection(direction));
					// Reposition child elements along our free axis.
					child->relativePosition.SetOffset(offset);
					offset[freeAxis] += child->relativePosition.Size(freeAxis);
				}
				// Set our free axis.
				ourBounds[freeAxis] = freeAxisSize;
			}
			break;
		case L_SCROLL:
			{
				int axes = GetAxes("axis");
				Box2F childrenTotalBounds;
				for (auto it = children.begin(); it != children.end(); ++it)
				{
					GuiObject *child = *it;
					// Size our child elements specifying free (scrolling) axis.
					child->RecursiveResize(ourBounds, EdgesForAxes(axes));
					if (childrenTotalBounds.Size(0) == 0 && childrenTotalBounds.Size(1) == 0)
					{
						childrenTotalBounds = child->relativePosition;
					}
					else
					{
						childrenTotalBounds.Combine(child->relativePosition);
					}
				}
				// TODO: Calculate scroll range.
				// TODO: Update scroll position.
				for (auto it = children.begin(); it != children.end(); ++it)
				{
					// TODO: Update child positions based on scroll position.
				}
			}
			break;
	}
}

static void GetBounds(
	GuiObject::PositionInfo &min,
	GuiObject::PositionInfo &max,
	GuiObject::PositionInfo &size,
	float maxSize,
	bool exceedMin,
	bool exceedMax,
	float *resultMinOut,
	float *resultMaxOut
) {
	float resultMin, resultMax;

	if (min.set && max.set)
	{
		resultMin = min.Get(maxSize);
		resultMax = max.Get(maxSize);
		if (resultMax < resultMin)
		{
			resultMax = resultMin;
		}
	}
	else if (min.set && size.set)
	{
		resultMin = min.Get(maxSize);
		resultMax = resultMin + size.Get(maxSize);
		if (resultMax > maxSize && !exceedMax)
		{
			resultMax = maxSize;
		}
	}
	else if (max.set && size.set)
	{
		resultMax = max.Get(maxSize);
		resultMin = resultMax - size.Get(maxSize);
		if (resultMin < 0 && !exceedMin)
		{
			resultMin = 0;
		}
	}
	else if (size.set)
	{
		resultMin = 0;
		resultMax = size.Get(maxSize);
		if (resultMax > maxSize)
		{
			if (exceedMax)
			{
				// intentionally do nothing so that if exceedMax and exceedMin are specified, max takes priority
			}
			else if (exceedMin)
			{
				resultMin = maxSize - resultMax;
				resultMax = maxSize;
			}
			else
			{
				resultMax = maxSize;
			}
		}
	}
	else
	{
		resultMin = 0;
		resultMax = maxSize;
	}

	*resultMinOut = resultMin;
	*resultMaxOut = resultMax;
}

void GuiObject::SetRelativePosition(Vector2F parentDimensions, int variableEdges)
{
	// TODO: How to do text-based resizing?
	// We need to detect whether an axis was left free or not.
	// This can be done in the return value of GetBounds.

	GetBounds(left, right, width, parentDimensions[0], variableEdges & E_LEFT, variableEdges & E_RIGHT, &relativePosition.mins[0], &relativePosition.maxs[0]);
	GetBounds(top, bottom, height, parentDimensions[1], variableEdges & E_TOP, variableEdges & E_BOTTOM, &relativePosition.mins[1], &relativePosition.maxs[1]);
}

void GuiObject::GetChildObjects(std::vector<GuiObject*> *objects)
{
	auto documentChildren = GetChildren();
	for (auto it = documentChildren.begin(); it != documentChildren.end(); ++it)
	{
		GuiObject *object = dynamic_cast<GuiObject*>((*it));
		if (object != nullptr)
		{
			objects->push_back(object);
		}
	}
}

int GuiObject::GetAxes(std::string attribute)
{
	if (!attribute.compare("horizontal"))
	{
		return A_HORIZONTAL;
	}
	if (!attribute.compare("vertical"))
	{
		return A_VERTICAL;
	}
	if (!attribute.compare("both"))
	{
		return A_BOTH;
	}
	return A_NONE;
}

int GuiObject::GetEdge(std::string attribute, int defaultValue)
{
	if (!attribute.compare("top"))
	{
		return E_TOP;
	}
	if (!attribute.compare("left"))
	{
		return E_LEFT;
	}
	if (!attribute.compare("bottom"))
	{
		return E_BOTTOM;
	}
	if (!attribute.compare("right"))
	{
		return E_RIGHT;
	}
	return defaultValue;
}

float GuiObject::GetFloat(std::string attribute, float defaultValue)
{
	if (attribute.length() < 1)
	{
		return defaultValue;
	}
	return String::ToFloat(attribute);
}

int GuiObject::GetDirection(std::string attribute, int defaultValue)
{
	if (!attribute.compare("up"))
	{
		return D_UP;
	}
	if (!attribute.compare("down"))
	{
		return D_DOWN;
	}
	if (!attribute.compare("left"))
	{
		return D_LEFT;
	}
	if (!attribute.compare("right"))
	{
		return D_RIGHT;
	}
	return defaultValue;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
