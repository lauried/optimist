/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <string>
#include "library/geometry/Vector2.hh"
#include "scenegraph/IRenderMeshProvider.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * An object for GuiObject to make use of to render itself.
 * The GuiObject can make use of multiple instances of this if it needs to.
 *
 * Intended use cases by GuiObject:
 * - Just some text.
 * - Drawing borders and background on an object.
 * - Drawing a 3D scene, e.g. to integrate the main game display into the UI,
 *   or to draw some model illustratively.
 * - Text input, selected and hover state.
 * - Scrollable regions.
 * - Buttons, with a selected, hover and depressed state.
 */
class GuiRenderObject : public IRenderMeshProvider {
public:
	enum TextAlignment {
		TEXT_ALIGN_CENTRE,
		TEXT_ALIGN_LEFT,
		TEXT_ALIGN_RIGHT
	};

	Vector2F size;
	std::string text;
	TextAlignment textAlignment;
	std::string imageName;
	std::string fontName;
	int imageBorderRadius; // this is a border around the image which doesn't stretch or repeat.
	// TODO: Some vector drawing properties.

	bool updateRequired;

	// TODO: Resource objects, which are set by SetResources(ResourceManager)

	// TODO: Scene to render within these bounds (later when we move to gui-centred stuff)

	GuiRenderObject() : imageBorderRadius(0), updateRequired(true) {}

	~GuiRenderObject();

	// IRenderMeshProvider
	virtual void GetMeshes(const SceneInfo *scene, std::vector<MeshInstance*>* meshes);

private:
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

