/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "gui/Gui.hh"
#include "gui/GuiObject.hh"
#include "gui/document/XmlDocumentParser.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/*
Notes:
Not feeling a lot of confidence in the complexity of this just for the sake
of having a nice declarative layout at the moment.
Maybe an example of how the GUI should look to user code should help set
a goal.

Another troubling thing is figuring out how adding and removing elements should
be done in actual use cases, such as repopulating a list.

XML Tag names should avoid copying HTML to avoid implying similar behaviour to
HTML.
Specific tag names should be used to choose the derived GuiObject. We can then
use type erasure to make these objects available.
There should then be a general purpose tag for the general purpose object.

Tag types for default layouts:
<container> - Absolute positioned elements
<grid>
<toolbar>
<scrollable>

Some tag types can add functionality:
<scroll> - Automatically does scrollbars.
<textbox> - Text input.
<slider> - Slider input.

Some tag types can be shorthand:
<image> - Just sets an image.
<label> - Just sets the text.
<button> - Uses a default image for buttons automatically.

Some are just semantic:
<document> - Use for the root node.

Default images would be handled by dependency injection.

<?xml version="1.0"?>
<document>
  <toolbar top="0" left="0" right="0" height="48">
    <image src="gui/icons/myicon.png"/>
    <label text="" id="guiOriginReadout"/>
  </toolbar>
</document>

<?xml version="1.0"?>
<document>
  <container id="menu">
    <grid cellWidth="50%" cellheight="24" bottom="24">
      <label text="Name"/><textbox id="menuPlayerName"/>
      <label text="Height"/><slider id="menuPlayerHeight" min="0.5" max="2.2"/>
    </grid>
    <toolbar height="24">
		<button id="menuSubmit" label="Confirm"/>
	</toolbar>
  </container>
</document>

To avoid mess, the non-generic tags could also limit what else is set up that
would be available on the base object, as well as augmenting behaviour.
So basically non-generic tags would be a specialised subset of the generic
element.

TODO: If elements have text, they'll use its width when auto sizing.
	  This will let us make toolbars of text items, aka menus.

This will all be a bit rough until it actually gets used and then a refactor
will help fix it up.

*/

// First thing to sort out is how we set the resources
// on the GuiRenderObject. Figuring out the nicest way to do this is the
// main obstacle top a clear design.

// The next step to deal with is the lack of CSS-like language.
// We can just use xml properties to set the styles of objects but it would
// get messy. At the same time, we want some per-object override of css classes,
// so the xml properties can be used for that.

// A nice way to pass resources could be through IRenderMeshProvider:
// Alongside SceneInfo we can pass the resource manager too.
// This lets us use mesh providers by giving them parameters in the places
// that want to be rendered and then having them set themselves up on render.
// Maybe at this point we'd also want the ResourceManager to be behind an
// interface, to allow for testing of its users if needed.

// TODO:

// - GuiObject contain RenderObject
// - GuiObject read its properties and set RenderObject
//   (Later we can read calculated style too)

// - RenderMeshProvider accept resource manager.
// - RenderObject get its resources.
// - RenderObject implement all its rendering.
// - RenderObject maybe add some vector.

// - Wire up GUI creation in gamecode.
// - Plan overall rendering of the entire GUI.

// - Design how events will be handled.
// - Design gamecode interface for event callbacks and overrides.
//   It might be nice to avoid having 1:1 engine objects for document nodes,
//   and instead have query objects that we act upon.
//   We can use these (or just the ID string) for hooking up callbacks.

void Gui::LoadDocument(const char *data, size_t length)
{
    document.DeleteNode(document.GetRoot());
    XmlDocumentParser parser;
    parser.ParseDocument(&document, data, length);
    layoutRequired = true;

    rootObject = dynamic_cast<GuiObject*>(document.GetRoot());
}

void Gui::SetSize(Vector2F size)
{
	if (rootObject != nullptr)
	{
		rootObject->RecursiveResize(size, GuiObject::E_NONE);
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
