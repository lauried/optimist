/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "gui/GuiAdapter.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// TODO:
// - DONE DocumentListener which maintains the GUI objects.
// - DONE Parsing of some kind of data to build a document.
//   An XML parser would work out less messy than parsing from gamecode objects.
// - Layout of gui objects.
//   - Design the basic XML schema for the types of object we'll want to create.
// - Render objects.
// - Rendering.
// - Input handling.
// - Callbacks.

/*
Basic Schema Design:

1. What sort of things to we want to implement?
   - Hud:
	 - Edge or corner aligned groups of objects.
	 - Fixed lines of debug text.
	 - Scrolling chat or event text.
	 - Rows or columns of icons.
	 - Status bars.
	 - Numerical readouts.
   - Inventory:
     - Centre aligned.
     - Icon grids.
     - Text overlay.
   - Menus:
     - Centre aligned.
     - Scrolling inner items.
     - Text input.
     - Slider input.
     - Selection input.

Note that positioning is kind of a relationship between container and contained
elements. There's one type of layout of elements specified by the container,
which may then accept parameters from child elements.

It might be nicest to just have specific hard-coded container types, rather
than composing that behaviour from CSS. Such behaviour is simpler to understand.

<absolute>
  Child elements use middle/top/left/bottom/right/width/height to specify their
  position. It's possible to lay out a hud like this at the top level.

<grid>
  Child elements are given width and height by the parent.
  The parent specifies a primary and secondary alignment, e.g. "top left" will
  start at the top left corner and go right, while "left top" will start at the
  top left corner and go down.
  If its width and height are unspecified, it will set them to fit the given
  icons, but it will use the maximum space it's allowed by its parent in the
  primary alignment axis before wrapping a new line.

<toolbar>
  The parent specifies a primary alignment and a direction.
  The child items are arranged on the primary alignment in the given direction,
  using their own widths and heights, though not permitted to exceed the
  parent.
  The toolbar may constrain axes.
  The difference between toolbar and grid is that a grid requires uniform cells.

<scroll>
  If the child contents of this element are too large, scrollbars are displayed
  allowing the element to be scrolled.
  This means that when child elements are resized, they must be able to state
  that they are unable to fit and report their real dimensions.
  For the sake of simplicity, the scroll element always reserves the space it
  will require for scroll bars.

How should resizing work:

We tell the root node what size to be, which recursively positions all its
children, and reports back its actual size.

The parent needs to be able to tell the child element what axes it should be
constrained in and to what width. The child can then specify any free
dimensions.

The check would be a query of e.g. width given height or vice versa.
The child would have to figure out how it would arrange itself for a given
size and return that value. This would only recurse for nested toolbars.

Then the parent sets the child's absolute position and the child then does the
same for its children.

We could do both steps at once - tell the child object to resize to fit a given
bounds, let it operate recursively and set its own size, and then finally the
parent can override the bounds that the child object set for itself.

*/

std::map<std::string, IGamecodeAdapter::AdapterMethod> GuiAdapter::GetGlobalFunctions()
{
	return {
		// void setDocument(table)
		{ "setDocument", std::bind(&GuiAdapter::SetDocument, this, std::placeholders::_1, std::placeholders::_2) },
	};
}

void GuiAdapter::OnRegister(IGamecode *gamecode)
{
}

GamecodeValue GuiAdapter::SetDocument(GamecodeParameterList *values, IGamecode* gamecode)
{
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
