/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


/**
Common options for all builds.
*/

//## cpp_options gcc -std=c++11 -pthread
//## cpp_options msvc /EHsc

#ifdef DEBUG
//## c_options gcc -g
//## cpp_options gcc -g
//## c_options msvc /Zi
//## cpp_options msvc /Zi
#else
#define NDEBUG
//## c_options gcc -Os
//## cpp_options gcc -Os
//## cpp_options -DNDEBUG
//## c_options msvc /Os
//## cpp_options msvc /Os
#endif

#ifdef __MINGW32__
//## add_libraries mingw32
#endif

#ifdef _WIN32
//## add_source_dirs Libraries/Windows/include/
//## add_lib_dirs Libraries/Windows/lib/
//## target_append .exe
#endif
