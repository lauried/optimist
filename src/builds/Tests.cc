/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/



/**
This file is just a compilation entry point for the boot script which scans
source files and automatically performs a build.

It is useful to have a separate file to initialise the build so that parameters
can be changed without forcing a change to the whole build.
*/

//## target Build/Tests
//## add_source_dirs src src/library/extern/
//## object_dir obj/Tests/

#include "builds/build_common.hh"

//## add_source_dirs src/tests

// TODO: Implement recursive add_sources_in
//## add_sources_in src/tests/
//## add_sources_in src/tests/units/
//## add_sources_in src/tests/units/filesystem/
//## add_sources_in src/tests/units/gamecode/
//## add_sources_in src/tests/units/gamecode/vernacular/
//## add_sources_in src/tests/units/gui/
//## add_sources_in src/tests/units/gui/document/
//## add_sources_in src/tests/units/library/
//## add_sources_in src/tests/units/library/containers/
//## add_sources_in src/tests/units/library/text/
//## add_sources_in src/tests/units/library/maths/
