/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <memory>
#include "audio/SampleRate.hh"
#include "audio/AudioChannelInfo.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Represents an audio destination.
 */
class AudioBuffer {
public:
	/**
	 * Create an empty audio buffer.
	 */
	AudioBuffer(AudioChannelInfo pChannelInfo, int pNumSamples, int pSamplesPerSecond);

	/**
	 * Create a buffer which points to some subset of another buffer.
	 */
	AudioBuffer(const AudioBuffer &other, int pSampleOffset, int pNumSamples);

	~AudioBuffer();

	/**
	 * Returns a duplicate of this buffer's properties, with zeroed data.
	 */
	inline AudioBuffer CloneEmpty()
	{
		return AudioBuffer(channelInfo, numSamples, sampleRate.SamplesPerSecond());
	}

	inline int NumSamples()
	{
		return numSamples;
	}

	inline int NumChannelsPerSample()
	{
		return numChannelsPerSample;
	}

	inline SampleRate &GetSampleRate()
	{
		return sampleRate;
	}

	inline void SetSample(int sample, int channel, float value)
	{
		data[sample * numChannelsPerSample + channel] = value;
	}

	/**
	 * Mixes a channel value using a mix vector from AudioChannelInfo.
	 */
	inline void MixSample(int sample, float value, float *scales)
	{
		for (int i = 0; i < numChannelsPerSample; i += 1)
		{
			data[sample * numChannelsPerSample + i] += value * scales[i];
		}
	}

	/**
	 * Scales the existing samples by the specified mix vector.
	 * This is for applying volume.
	 */
	inline void ScaleSample(int sample, float *scales)
	{
		for (int i = 0; i < numChannelsPerSample; i += 1)
		{
			data[sample * numChannelsPerSample + i] *= scales[i];
		}
	}

	inline float GetSample(int sample, int channel)
	{
		return data[sample * numChannelsPerSample + channel];
	}

	void SetToZero();

	inline const float* GetRawSamples()
	{
		return data;
	}

	AudioChannelInfo& ChannelInfo()
	{
		return channelInfo;
	}

private:
	int numSamples;
	int numChannelsPerSample;
	SampleRate sampleRate;
	AudioChannelInfo channelInfo;

	/**
	 * True if we are responsible for deleting data, else false.
	 */
	bool ownsData;

	/**
	 * numSamples * numChannelsPerSample floats
	 */
	float *data;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

