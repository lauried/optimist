/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gamecode/IGamecodeAdapter.hh"
#include "audio/driver/IAudioDriver.hh"
#include "audio/AudioMessageManager.hh"
#include "audio/AbstractAudioMessageReceiver.hh"
#include "library/IdPool.hh"
#include "resources/ResourceManager.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class AudioAdapter : public IGamecodeAdapter {
public:
	AudioAdapter(IAudioDriver *driver, ResourceManager *resources) :
		driver(driver),
		resources(resources),
		effectIds(1),
		groupIds(1)
		{}

	void Frame(double interval);

	// IGamecodeAdapter
	virtual const std::string& GetName();
	virtual std::map<std::string, AdapterMethod> GetGlobalFunctions();
	virtual void OnRegister(IGamecode *gamecode);

	// Gamecode Callbacks
	GamecodeValue CreateEffect(GamecodeParameterList *values, IGamecode *gamecode);
	GamecodeValue CreateSpatialEffect(GamecodeParameterList *values, IGamecode *gamecode);
	GamecodeValue SetEffectPosition(GamecodeParameterList *values, IGamecode *gamecode);
	GamecodeValue SetEffectVolume(GamecodeParameterList *values, IGamecode *gamecode);
	GamecodeValue StopEffectLooping(GamecodeParameterList *values, IGamecode *gamecode);
	GamecodeValue RemoveEffect(GamecodeParameterList *values, IGamecode *gamecode);

	GamecodeValue CreateGroup(GamecodeParameterList *values, IGamecode *gamecode);
	GamecodeValue SetGroupVolume(GamecodeParameterList *values, IGamecode *gamecode);
	GamecodeValue SetGroupHeadPosition(GamecodeParameterList *values, IGamecode *gamecode);
	GamecodeValue RemoveGroup(GamecodeParameterList *values, IGamecode *gamecode);

private:
	IAudioDriver *driver;
	ResourceManager *resources;
	AudioMessageManager messageManager;
	IdPool<int> effectIds;
	IdPool<int> groupIds;

	IAudioSource *GetSourceFromString(std::string sourceString);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

