/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "audio/effects/AudioEffectsMixer.hh"
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

const int DEFAULT_GROUP_ID = 0;

AudioEffectsMixer::AudioEffectsMixer()
{
	AudioEffectGroup *defaultGroup = new AudioEffectGroup();
	defaultGroup->groupID = DEFAULT_GROUP_ID;
	defaultGroup->volume = 1.0;
	defaultGroup->spatialisationScale = 1.0;
	effectGroups.insert(std::make_pair(DEFAULT_GROUP_ID, defaultGroup));
}

AudioEffectsMixer::~AudioEffectsMixer()
{
	for (auto it = effectGroups.begin(); it != effectGroups.end(); ++it)
	{
		delete it->second;
	}
}

void AudioEffectsMixer::RenderToBuffer(AudioBuffer *buffer)
{
	buffer->SetToZero();
	AudioBuffer tempBuffer = buffer->CloneEmpty();

	for (auto itEffect = effects.begin(); itEffect != effects.end();)
	{
		// Render
		AudioEffect *effect = &(itEffect->second);
		effect->RenderToBuffer(&tempBuffer);

		// Mix
		for (int sample = 0; sample < buffer->NumSamples(); sample += 1)
		{
			for (int channel = 0; channel < buffer->NumChannelsPerSample(); channel += 1)
			{
				float value = buffer->GetSample(sample, channel) + tempBuffer.GetSample(sample, channel);
				buffer->SetSample(sample, channel, value);
			}
		}

		// Tidy up effect
		if (effect->Finished())
		{
			OnDestroyEffect(effect);
			itEffect = effects.erase(itEffect);
		}
		else
		{
			++itEffect;
		}

		// Clear temp buffer for next sample
		if (itEffect != effects.end())
		{
			tempBuffer.SetToZero();
		}
	}
}

void AudioEffectsMixer::HandleMessage(IAudioMessage *message)
{
	if (message->Is<BaseAudioEffectMessage>())
	{
		HandleEffectMessage(message->As<BaseAudioEffectMessage>());
		return;
	}

	if (message->Is<BaseAudioEffectGroupMessage>())
	{
		HandleGroupMessage(message->As<BaseAudioEffectGroupMessage>());
		return;
	}
}

void AudioEffectsMixer::HandleEffectMessage(BaseAudioEffectMessage *message)
{
	auto itEffect = effects.find(message->effectID);

	if (message->Is<CreateAudioEffectMessage>())
	{
		CreateAudioEffectMessage *create = message->As<CreateAudioEffectMessage>();
		if (itEffect != effects.end())
		{
			OnDestroyEffect(&(itEffect->second));
			effects.erase(itEffect);
		}
		auto itGroup = effectGroups.find(create->groupID);
		if (itGroup == effectGroups.end())
		{
			itGroup = effectGroups.find(DEFAULT_GROUP_ID);
		}
		effects.insert(std::make_pair(message->effectID, AudioEffect(create, itGroup->second)));
	}

	// The rest of the messages require an existing effect.
	if (itEffect == effects.end())
	{
		return;
	}

	if (message->Is<RemoveAudioEffectMessage>())
	{
		OnDestroyEffect(&(itEffect->second));
		effects.erase(itEffect);
	}

	// Effect itself can handle messages from here.
	itEffect->second.HandleMessage(message);
}

void AudioEffectsMixer::HandleGroupMessage(BaseAudioEffectGroupMessage *message)
{
	auto itGroup = effectGroups.find(message->groupID);

	if (message->Is<CreateAudioEffectGroupMessage>())
	{
		CreateAudioEffectGroupMessage *create = message->As<CreateAudioEffectGroupMessage>();
		AudioEffectGroup *group;
		if (itGroup == effectGroups.end())
		{
			group = new AudioEffectGroup();
			effectGroups.insert(std::make_pair(message->groupID, group));
		}
		else
		{
			group = itGroup->second;
		}
		group->groupID = message->groupID;
		group->volume = create->volume;
		group->spatialisationScale = create->spatialisationScale;
		group->headPosition = create->headPosition;
		OnUpdateGroup(group);
		return;
	}

	if (itGroup == effectGroups.end())
	{
		return;
	}

	if (message->Is<VolumeAudioEffectGroupMessage>())
	{
		VolumeAudioEffectGroupMessage *volume = message->As<VolumeAudioEffectGroupMessage>();
		AudioEffectGroup *group = itGroup->second;
		group->volume = volume->volume;
		OnUpdateGroup(group);
	}

	if (message->Is<HeadPositionAudioEffectGroupMessage>())
	{
		HeadPositionAudioEffectGroupMessage *headPosition = message->As<HeadPositionAudioEffectGroupMessage>();
		AudioEffectGroup *group = itGroup->second;
		group->headPosition = headPosition->headPosition;
		OnUpdateGroup(group);
	}

	if (message->Is<RemoveAudioEffectGroupMessage>())
	{
		if (message->groupID != DEFAULT_GROUP_ID)
		{
#if 1
			for (auto itEffect = effects.begin(); itEffect != effects.end(); ++itEffect)
			{
				AudioEffect *effect = &(itEffect->second);
				auto itDefaultGroup = effectGroups.find(DEFAULT_GROUP_ID);
				if (effect->group->groupID == message->groupID)
				{
					effect->group = itDefaultGroup->second;
				}
				OnUpdateGroup(itDefaultGroup->second);
			}
#else
			for (auto itEffect = effects.begin(); itEffect != effects.end();)
			{
				AudioEffect *effect = &(itEffect->second);
				if (effect->group->groupID == message->groupID)
				{
					OnDestroyEffect(effect);;
					itEffect = effects.erase(itEffect);
				}
				else
				{
					++itEffect;
				}
			}
#endif
			delete itGroup->second;
			effectGroups.erase(itGroup);
		}
	}
}

void AudioEffectsMixer::OnDestroyEffect(AudioEffect *effect)
{
	if (effect->destroyCallback != nullptr)
	{
		effect->destroyCallback->TriggerDelete();
		effect->destroyCallback->TriggerCall();
	}
}

void AudioEffectsMixer::OnUpdateGroup(AudioEffectGroup *group)
{
	for (auto it = effects.begin(); it != effects.end(); ++it)
	{
		AudioEffect *effect = &(it->second);
		if (effect->group == group)
		{
			effect->OnGroupUpdate();
		}
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
