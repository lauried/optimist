/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "audio/effects/AudioEffect.hh"
#include "audio/effects/AudioEffectMessages.hh"
#include "audio/effects/spatialiser/SimpleAudioSpatialiser.hh"
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

AudioEffect::AudioEffect(CreateAudioEffectMessage *message, AudioEffectGroup *group) : AudioEffect()
{
	effectID = message->effectID;
	this->group = group;

	effect = message->source;
	source = effect;

	if (message->loop)
	{
		looper = new LoopingAudioSource(source);
		source = looper;
	}

	if (message->spatialise)
	{
		position = message->position;
		spatialiser = new SimpleAudioSpatialiser();
		spatialiser->SetSource(source);
		spatialiser->SetPosition(position, group->headPosition);
		source = spatialiser;
	}
	destroyCallback = message->destroyCallback;
}

AudioEffect::AudioEffect(const AudioEffect& other)
{
	effectID = other.effectID;
	group = other.group;
	effect = other.effect;
	source = other.source;
	looper = other.looper;
	spatialiser = other.spatialiser;
	volume = other.volume;
	finished = other.finished;
	destroyCallback = other.destroyCallback;
	position = other.position;
}

AudioEffect::~AudioEffect()
{
	delete spatialiser;
}

// IAudioSource
void AudioEffect::RenderToBuffer(AudioBuffer *buffer)
{
	if (effect == nullptr)
	{
		return;
	}

	source->RenderToBuffer(buffer);

	float absoluteVolume = volume * group->volume;
	if (absoluteVolume != 1.0)
	{
		std::vector<float> volumes(buffer->NumChannelsPerSample(), absoluteVolume);
		for (int sample = 0; sample < buffer->NumSamples(); sample += 1)
		{
			buffer->ScaleSample(sample, &volumes[0]);
		}
	}

	if (source->Finished())
	{
		finished = true;
	}
}

void AudioEffect::HandleMessage(IAudioMessage *message)
{
	if (message->Is<PositionAudioEffectMessage>())
	{
		PositionAudioEffectMessage *position = message->As<PositionAudioEffectMessage>();
		if (spatialiser != nullptr)
		{
			spatialiser->SetPosition(position->position, group->headPosition);
		}
	}

	if (message->Is<VolumeAudioEffectMessage>())
	{
		VolumeAudioEffectMessage *volume = message->As<VolumeAudioEffectMessage>();
		this->volume = volume->volume;
	}

	if (message->Is<StopLoopingAudioEffectMessage>())
	{
		if (looper != nullptr)
		{
			looper->StopLooping();
		}
	}
}

void AudioEffect::OnGroupUpdate()
{
	if (spatialiser != nullptr)
	{
		spatialiser->SetPosition(position, group->headPosition);
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
