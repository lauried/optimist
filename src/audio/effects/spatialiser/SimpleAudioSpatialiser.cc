/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "audio/effects/spatialiser/SimpleAudioSpatialiser.hh"
#include <cmath>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

void SimpleAudioSpatialiser::RenderToBuffer(AudioBuffer *buffer)
{
	// Render source to a mono buffer.
	AudioBuffer monoBuffer(MonoChannelInfo(), buffer->NumSamples(), buffer->GetSampleRate().SamplesPerSecond());
	source->RenderToBuffer(&monoBuffer);

	// Calculate the volume levels for each destination channel.
	std::vector<float> mixVector(buffer->NumChannelsPerSample());
	Vector3F directionToSource = position;
	float distanceToSource = position.Normalize();
	float falloff = std::min(1.0f / (distanceToSource * distanceToSource), 1.0f);
	for (int channelNum = 0; channelNum < buffer->NumChannelsPerSample(); channelNum += 1)
	{
		float dot = buffer->ChannelInfo().SpatialisationInfo()[channelNum].direction.DotProduct(directionToSource);
		mixVector[channelNum] = falloff * std::max(dot, 0.0f);
	}

	// Mix the source.
	float *mixVectorPointer = &mixVector[0];
	for (int sample = 0; sample < buffer->NumSamples(); sample += 1)
	{
		float value = monoBuffer.GetSample(sample, 0);
		buffer->MixSample(sample, value, mixVectorPointer);
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
