/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "audio/IAudioSource.hh"
#include "library/geometry/Orientation.hh"
#include "library/geometry/Vector3.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Wraps an audio source and applies effects to give it the appearance of
 * coming from a particular position.
 *
 * Different implementations may use different techniques to give this effect.
 */
class IAudioSpatialiser : public virtual IAudioSource {
public:
	virtual ~IAudioSpatialiser();

	virtual void SetSource(IAudioSource *source) = 0;

	virtual void SetPosition(const Vector3F &sourcePosition, const OrientationF &headPosition) = 0;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

