/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "audio/effects/AudioEffectGroup.hh"
#include "audio/IAudioSource.hh"
#include "audio/AudioBuffer.hh"
#include "audio/driver/ThreadCallback.hh"
#include "audio/IAudioMessageHandler.hh"
#include "audio/effects/AudioEffectMessages.hh"
#include "audio/effects/spatialiser/IAudioSpatialiser.hh"
#include "audio/sources/LoopingAudioSource.hh"
#include "library/geometry/Vector3.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * A currently playing audio effect in the audio mixer.
 */
class AudioEffect : public IAudioSource {
public:
	AudioEffect() :
		effectID(0),
		group(nullptr),
		effect(nullptr),
		source(nullptr),
		looper(nullptr),
		spatialiser(nullptr),
		volume(1.0f),
		finished(false),
		destroyCallback(nullptr)
		{}

	AudioEffect(CreateAudioEffectMessage *message, AudioEffectGroup *group);

	AudioEffect(const AudioEffect& other);

	~AudioEffect();

	// IAudioSource
	virtual void RenderToBuffer(AudioBuffer *buffer);
	bool Finished() { return finished; }

	// IAudioMessageHandler
	virtual void HandleMessage(IAudioMessage *message);

	/**
	 * Sets the effect's audio source.
	 */
	void SetSource(IAudioSource *source) { effect = source; }

	void OnGroupUpdate();

	int effectID;
	AudioEffectGroup *group;
	IAudioSource *effect;
	IAudioSource *source;
	LoopingAudioSource *looper;
	IAudioSpatialiser *spatialiser;
	float volume;
	bool finished;
	ThreadCallback *destroyCallback;
	Vector3F position;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

