/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <map>
#include "audio/IAudioSource.hh"
#include "audio/IAudioMessageHandler.hh"
#include "audio/effects/AudioEffect.hh"
#include "audio/effects/AudioEffectGroup.hh"
#include "audio/effects/AudioEffectMessages.hh"
#include "audio/effects/AudioEffectGroupMessages.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Manages all currently playing effects and mixes them together.
 */
class AudioEffectsMixer : public IAudioSource, public IAudioMessageHandler {
public:
	AudioEffectsMixer();
	~AudioEffectsMixer();

	// IAudioSource
	virtual void RenderToBuffer(AudioBuffer *buffer);
	virtual bool Finished() { return false; }

	// IAudioMessageHandler
	virtual void HandleMessage(IAudioMessage *message);

	void HandleEffectMessage(BaseAudioEffectMessage *message);
	void HandleGroupMessage(BaseAudioEffectGroupMessage *message);

private:
	std::map<int, AudioEffect> effects;
	std::map<int, AudioEffectGroup*> effectGroups;

	void OnDestroyEffect(AudioEffect *effect);
	void OnUpdateGroup(AudioEffectGroup *group);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

