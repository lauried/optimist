/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "audio/AudioBuffer.hh"
#include <stdexcept>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

AudioBuffer::AudioBuffer(AudioChannelInfo pChannelInfo, int pNumSamples, int pSamplesPerSecond) : sampleRate(pSamplesPerSecond)
{
	channelInfo = pChannelInfo;
	numChannelsPerSample = channelInfo.NumChannels();
	numSamples = pNumSamples;
	data = new float[numChannelsPerSample * numSamples];
	ownsData = true;
}

AudioBuffer::AudioBuffer(const AudioBuffer &other, int pSampleOffset, int pNumSamples) : sampleRate(other.sampleRate)
{
	if (pSampleOffset < 0 || pSampleOffset + pNumSamples > other.numSamples)
	{
		throw std::invalid_argument("Sample selection out of range");
	}

	numChannelsPerSample = other.numChannelsPerSample;
	numSamples = pNumSamples;
	data = &other.data[pSampleOffset * numChannelsPerSample];
	ownsData = false;
}

AudioBuffer::~AudioBuffer()
{
	if (ownsData)
	{
		delete[] data;
	}
}

void AudioBuffer::SetToZero()
{
	for (int i = 0; i < numSamples * numChannelsPerSample; i += 1)
	{
		data[i] = 0;
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
