/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <map>
#include <functional>
#include "audio/IAudioSource.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Stores audio sources which are in the process of being played, allowing them
 * to be either deleted afterward, or using some other callback to free them.
 */
class AudioSourceManager {
public:
	void AddSource(IAudioSource *source, std::function<void(IAudioSource*)> onRemove);
	void RemoveSource(IAudioSource *source);
	void RemoveFinishedSources();

private:
	struct Item {
		IAudioSource *source;
		std::function<void(IAudioSource*)> onRemove;
	};

	std::map<IAudioSource*, Item> sources;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

