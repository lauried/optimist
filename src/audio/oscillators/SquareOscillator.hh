/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "audio/oscillators/SineOscillator.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class SquareOscillator : public SineOscillator {
public:
	SquareOscillator() : SineOscillator(), numSines(6) {}

	inline float GetValue()
	{
		float value = 0;
		float scale = 1;
		for (int i = 0; i < numSines; i += 1, scale += 2)
		{
			value += sin(position * scale) / scale;
		}
		position = fmod(position + step, M_PI * 2);
		return value * 0.25;
	}

protected:
	int numSines;

	void UpdateParameters()
	{
		step = (2 * M_PI * frequency) / sampleRate;
		numSines = floor((sampleRate / (2 * frequency)) - 2) / 4;
		if (numSines > 6)
		{
			numSines = 6;
		}
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

