/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "library/maths/Maths.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class SineOscillator {
public:
	SineOscillator() : frequency(440), sampleRate(44100), position(0), step(0) {}

	void SetPhase(float pPhase)
	{
		position = fmod(pPhase, 2 * M_PI);
	}

	void SetFrequency(float pFrequency)
	{
		frequency = pFrequency;
		UpdateParameters();
	}

	void SetSampleRate(int pSampleRate)
	{
		sampleRate = pSampleRate;
		UpdateParameters();
	}

	inline float GetValue()
	{
		float value = sin(position);
		position = fmod(position + step, 2 * M_PI);
		return value * 0.25;
	}

protected:
	float frequency, sampleRate, position, step;

	virtual void UpdateParameters()
	{
		step = (2 * M_PI * frequency) / sampleRate;
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

