/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Represents a samplerate but also encapsulates the conversions that we'd like
 * to perform.
 */
class SampleRate {
public:
	SampleRate(int pSamplesPerSecond)
	{
		samplesPerSecond = pSamplesPerSecond;
	}

	SampleRate(const SampleRate &other)
	{
		samplesPerSecond = other.samplesPerSecond;
	}

	int SamplesPerSecond()
	{
		return samplesPerSecond;
	}

	/**
	 * Number of seconds that the given number of samples will last.
	 */
	float DurationOfSamples(int samples)
	{
		return (float)samples / (float)samplesPerSecond;
	}

	/**
	 * Number of samples in the given duration.
	 */
	int SamplesInDuration(float duration)
	{
		return duration * (float)samplesPerSecond;
	}

private:
	int samplesPerSecond;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

