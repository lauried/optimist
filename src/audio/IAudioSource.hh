/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "audio/AudioBuffer.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * An audio source renders audio into a buffer on demand.
 * Internally it can do things like mixing multiple other audio sources, or
 * keeping track of time in a song, or splitting up into multiple other buffers
 * so it can control the boundaries where the audio is split.
 */
class IAudioSource {
public:
	virtual ~IAudioSource();

	virtual void RenderToBuffer(AudioBuffer *buffer) = 0;

	/**
	 * Returns the last number of samples rendered to the buffer.
	 * If the source finished rendering before filling the buffer, this will be
	 * smaller than the buffer.
	 * Returns zero if the source does not support returning this information.
	 */
	virtual int LastRenderedSamples() { return 0; }

	/**
	 * Attempts to reset the source to play from the beginning again.
	 * Returns true if successful, else false.
	 */
	virtual bool Reset() { return false; }

	/**
	 * If the source is non-looping and non-streaming it MUST return true once
	 * it has finished playing.
	 */
	virtual bool Finished() = 0;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

