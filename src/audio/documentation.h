/**
\page audio_interfaces Audio Interfaces

Audio is separated into sections:
- Sources, which are responsible for mixing some audio which we want to
  play to the listener or write to disk. A set of audio sources could be used
  to synthesize music or to represent spatialised and user-interface sounds in
  a game.
- Drivers, which render the output of an audio source. They might write to disk
  but usually play to the speakers over some audio API. They would typically
  use a separate thread to perform rendering, meaning that the entire set of
  audio sources rendering the sound would be kept isolated.
- Messages, which are used to communicate with the audio sources from other
  threads. The driver contains a message target which takes these messages and
  applies them to audio sources.
*/
