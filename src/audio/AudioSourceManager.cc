/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "audio/AudioSourceManager.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

void AudioSourceManager::AddSource(IAudioSource *source, std::function<void(IAudioSource*)> onRemove)
{
	Item item { source, onRemove };
	sources.insert(std::make_pair(source, item));
}

void AudioSourceManager::RemoveSource(IAudioSource *source)
{
	auto it = sources.find(source);
	if (it != sources.end())
	{
		sources.erase(it);
	}
}

void AudioSourceManager::RemoveFinishedSources()
{
	for (auto it = sources.begin(); it != sources.end();)
	{
		if (it->second.source->Finished())
		{
			it->second.onRemove(it->second.source);
			it = sources.erase(it);
		}
		else
		{
			++it;
		}
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
