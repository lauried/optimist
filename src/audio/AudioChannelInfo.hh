/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <vector>
#include "library/geometry/Vector3.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Provides information on a set of audio channels.
 * Specifically designed to aid in mixing sources.
 */
class AudioChannelInfo {
public:
	struct Spatialisation {
		bool enable;
		Vector3F direction;

		Spatialisation(const Spatialisation &other) :
			enable(other.enable),
			direction(other.direction)
			{}

		Spatialisation() : enable(false) {}

		Spatialisation(float x, float y, float z) : enable(true) { direction.Set(x, y, z); }
	};

	AudioChannelInfo() {}

	AudioChannelInfo(const AudioChannelInfo &other) :
		numChannels(other.numChannels),
		monoMix(other.monoMix),
		stereoLeftMix(other.stereoLeftMix),
		stereoRightMix(other.stereoRightMix),
		spatialisation(other.spatialisation)
		{}

	/**
	 * Returns the number of audio channels that this channelinfo represents.
	 */
	inline int NumChannels() { return numChannels; }

	/**
	 * Returns an array of NumChannels floats.
	 * Each float is the scale to apply to a mono channel when mixing.
	 */
	inline const float * MonoMix() { return &monoMix[0]; }

	/**
	 * Returns an array of NumChannels floats.
	 * Each float is the scale to apply to the stereo left channel when mixing.
	 */
	inline const float * StereoLeftMix() { return &stereoLeftMix[0]; }

	/**
	 * Returns an array of NumChannels floats.
	 * Each float is the scale to apply to the stereo left channel when mixing.
	 */
	inline const float * StereoRightMix() { return &stereoRightMix[0]; }

	/**
	 * Returns an array of NumChannels spatialisation descriptors.
	 */
	inline const Spatialisation * SpatialisationInfo() { return &spatialisation[0]; }

protected:
	int numChannels;
	std::vector<float> monoMix;
	std::vector<float> stereoLeftMix;
	std::vector<float> stereoRightMix;
	std::vector<Spatialisation> spatialisation;
};

class StereoChannelInfo : public AudioChannelInfo {
public:
	StereoChannelInfo()
	{
		numChannels = 2;
		monoMix = { 0.5f, 0.5f };
		stereoLeftMix = { 1.0f, 0.0f };
		stereoRightMix = { 0.0f, 1.0f };
		spatialisation.push_back(Spatialisation(0, -1, 0));
		spatialisation.push_back(Spatialisation(0, 1, 0));
	}
};

class MonoChannelInfo : public AudioChannelInfo {
public:
	MonoChannelInfo()
	{
		numChannels = 1;
		monoMix = { 1.0f };
		stereoLeftMix = { 0.5f, 0.5f };
		stereoRightMix = { 0.5f, 0.5f };
		spatialisation.push_back(Spatialisation(1, 0, 0));
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

