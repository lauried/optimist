/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "audio/sources/LoopingAudioSource.hh"
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

void LoopingAudioSource::RenderToBuffer(AudioBuffer *buffer)
{
	if (finished)
	{
		return;
	}
	source->RenderToBuffer(buffer);
	if (source->Finished())
	{
		if (looping)
		{
			int numSamples = source->LastRenderedSamples();
			if (!source->Reset())
			{
				// Source does not support looping, so we finish.
				finished = true;
				return;
			}
			// If we didn't fill the buffer, and the source isn't empty, try to fill the rest of the buffer.
            if (numSamples > 0 && numSamples < buffer->NumSamples())
            {
				AudioBuffer newBuffer(*buffer, numSamples, buffer->NumSamples() - numSamples);
				RenderToBuffer(&newBuffer);
			}
		}
		else
		{
			finished = true;
			return;
		}
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
