/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "audio/IAudioSource.hh"
#include "audio/oscillators/SineOscillator.hh"
#include "audio/oscillators/SquareOscillator.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Plays a sine wave for a given duration.
 * We could if we wanted mix in a bunch of sine waves to approach a square wave.
 */
class BeepAudioSource : public IAudioSource {
public:
	BeepAudioSource(float frequency, float duration);

	// IAudioSource
	virtual ~BeepAudioSource();

	// IAudioSource
	virtual void RenderToBuffer(AudioBuffer *buffer);
	virtual bool Finished() { return finished; }
	virtual int LastRenderedSamples() { return lastRenderedSamples; }
	virtual bool Reset() { duration = originalDuration; finished = false; return true; }

private:
	float frequency;
	float duration;
	float originalDuration;
	int lastRenderedSamples;
	bool finished;

	SquareOscillator osc;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

