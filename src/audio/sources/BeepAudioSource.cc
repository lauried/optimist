/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "audio/sources/BeepAudioSource.hh"
#include <iostream>
#include "library/maths/Maths.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

BeepAudioSource::BeepAudioSource(float frequency, float duration) :
	frequency(frequency),
	duration(duration),
	originalDuration(duration),
	lastRenderedSamples(false),
	finished(false)
{
	osc.SetFrequency(frequency);
}

BeepAudioSource::~BeepAudioSource()
{
}

void BeepAudioSource::RenderToBuffer(AudioBuffer *buffer)
{
	osc.SetSampleRate(buffer->GetSampleRate().SamplesPerSecond());

	int samplesRemaining = buffer->GetSampleRate().SamplesInDuration(duration);
	int numSamples = buffer->NumSamples();
	if (samplesRemaining < numSamples)
	{
		numSamples = samplesRemaining;
		finished = true;
	}
	int numChannels = buffer->NumChannelsPerSample();
	for (int sample = 0; sample < numSamples; sample += 1)
	{
		float value = osc.GetValue();
		for (int channel = 0; channel < numChannels; channel += 1)
		{
			buffer->SetSample(sample, channel, value);
		}
	}

	lastRenderedSamples = numSamples;

	duration -= buffer->GetSampleRate().DurationOfSamples(numSamples);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
