/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "audio/AudioAdapter.hh"
#include <iostream>
#include "audio/effects/AudioEffectMessages.hh"
#include "audio/effects/AudioEffectGroupMessages.hh"
#include "audio/sources/BeepAudioSource.hh"
#include "resources/SampleResource.hh"
#include "resources/ResourceManager.hh"
#include "gamecode/library/GamecodeVector.hh"
#include "gamecode/library/GamecodeOrientation.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

void AudioAdapter::Frame(double interval)
{
	messageManager.RemoveHandledMessages();
}

const std::string& AudioAdapter::GetName()
{
	static std::string name("Audio");
	return name;
}

std::map<std::string, IGamecodeAdapter::AdapterMethod> AudioAdapter::GetGlobalFunctions()
{
	return {
		// number createEffect(sourceSpecifier, volume, loop, groupID)
		{ "createEffect", std::bind(&AudioAdapter::CreateEffect, this, std::placeholders::_1, std::placeholders::_2) },

		// number createSpatialEffect(sourceSpecifier, volume, loop, position, groupID)
		{ "createSpatialEffect", std::bind(&AudioAdapter::CreateSpatialEffect, this, std::placeholders::_1, std::placeholders::_2) },

		// setEffectPosition(id, position)
		{ "setEffectPosition", std::bind(&AudioAdapter::SetEffectPosition, this, std::placeholders::_1, std::placeholders::_2) },

		// setEffectVolume(id, volume)
		{ "setEffectVolume", std::bind(&AudioAdapter::SetEffectVolume, this, std::placeholders::_1, std::placeholders::_2) },

		// stopEffectLooping()
		{ "stopEffectLooping", std::bind(&AudioAdapter::StopEffectLooping, this, std::placeholders::_1, std::placeholders::_2) },

		// removeEffect()
		{ "removeEffect", std::bind(&AudioAdapter::RemoveEffect, this, std::placeholders::_1, std::placeholders::_2) },

		// number createGroup()
		{ "createGroup", std::bind(&AudioAdapter::CreateGroup, this, std::placeholders::_1, std::placeholders::_2) },

		// setGroupVolume(id, volume)
		{ "setGroupVolume", std::bind(&AudioAdapter::SetGroupVolume, this, std::placeholders::_1, std::placeholders::_2) },

		// setGroupHeadPosition(id, orientation)
		{ "setGroupHeadPosition", std::bind(&AudioAdapter::SetGroupHeadPosition, this, std::placeholders::_1, std::placeholders::_2) },

		// removeGroup()
		{ "removeGroup", std::bind(&AudioAdapter::RemoveGroup, this, std::placeholders::_1, std::placeholders::_2) },
	};
}

void AudioAdapter::OnRegister(IGamecode *gamecode)
{
}

// number createEffect(sourceSpecifier, volume, loop, groupID)
GamecodeValue AudioAdapter::CreateEffect(GamecodeParameterList *values, IGamecode *gamecode)
{
	// Get parameters from gamecode
	values->RequireParmCount(3, 4);
	std::string sourceSpecifier(values->RequireType(0, GamecodeValue::T_String).GetString());
	float volume = values->RequireType(1, GamecodeValue::T_Number).GetFloat();
	bool loop = values->RequireType(2, GamecodeValue::T_Bool).GetBool();
	int groupID = 0;
	if (values->size() == 4)
	{
		groupID = values->RequireType(3, GamecodeValue::T_Number).GetInt();
	}

	// Get audio source and effect ID
	IAudioSource *source = GetSourceFromString(sourceSpecifier);
	if (source == nullptr)
	{
		return GamecodeValue(-1);
	}
	int effectID = effectIds.Get();
	IdPool<int> *effectIdsPointer = &effectIds;

	// Build and send message
	CreateAudioEffectMessage *message = new CreateAudioEffectMessage();
	message->effectID = effectID;
	message->groupID = groupID;
	message->source = source;
	message->volume = volume;
	message->loop = loop;
	message->spatialise = false;
	message->destroyCallback = driver->CreateThreadCallback([source, effectID, effectIdsPointer]() {
		delete source;
		effectIdsPointer->Free(effectID);
	});
	messageManager.AddMessage(message, [](IAudioMessage *message) {
		delete message;
	});
	driver->SendMessage(message);

	return GamecodeValue(effectID);
}

// number createSpatialEffect(sourceSpecifier, volume, loop, position, groupID)
GamecodeValue AudioAdapter::CreateSpatialEffect(GamecodeParameterList *values, IGamecode *gamecode)
{
	values->RequireParmCount(4, 5);
	std::string sourceSpecifier(values->RequireType(0, GamecodeValue::T_String).GetString());
	float volume = values->RequireType(1, GamecodeValue::T_Number).GetFloat();
	bool loop = values->RequireType(2, GamecodeValue::T_Bool).GetBool();
	Vector3F *position = values->RequireEngineType<GamecodeVector>(3, "Vector")->GetObject();
	int groupID = 0;
	if (values->size() == 5)
	{
		groupID = values->RequireType(4, GamecodeValue::T_Number).GetInt();
	}

	IAudioSource *source = GetSourceFromString(sourceSpecifier);
	if (source == nullptr)
	{
		return GamecodeValue(-1);
	}
	int effectID = effectIds.Get();
	IdPool<int> *effectIdsPointer = &effectIds;

	// Build and send message
	CreateAudioEffectMessage *message = new CreateAudioEffectMessage();
	message->effectID = effectID;
	message->groupID = groupID;
	message->source = source;
	message->volume = volume;
	message->loop = loop;
	message->spatialise = true;
	message->position = *position;
	message->destroyCallback = driver->CreateThreadCallback([source, effectID, effectIdsPointer]() {
		delete source;
		effectIdsPointer->Free(effectID);
	});
	messageManager.AddMessage(message, [](IAudioMessage *message) {
		delete message;
	});
	driver->SendMessage(message);

	return GamecodeValue(effectID);
}

// setEffectPosition(id, position)
GamecodeValue AudioAdapter::SetEffectPosition(GamecodeParameterList *values, IGamecode *gamecode)
{
	values->RequireParmCount(2);
	int effectID = values->RequireType(0, GamecodeValue::T_Number).GetInt();
	Vector3F *position = values->RequireEngineType<GamecodeVector>(1, "Vector")->GetObject();

	PositionAudioEffectMessage *message = new PositionAudioEffectMessage();
	message->effectID = effectID;
	message->position = *position;
	messageManager.AddMessage(message, [](IAudioMessage *message) {
		delete message;
	});
	driver->SendMessage(message);

	return GamecodeValue();
}

// setEffectVolume(id, volume)
GamecodeValue AudioAdapter::SetEffectVolume(GamecodeParameterList *values, IGamecode *gamecode)
{
	values->RequireParmCount(2);
	int effectID = values->RequireType(0, GamecodeValue::T_Number).GetInt();
	float volume = values->RequireType(1, GamecodeValue::T_Number).GetFloat();

	VolumeAudioEffectMessage *message = new VolumeAudioEffectMessage();
	message->effectID = effectID;
	message->volume = volume;
	messageManager.AddMessage(message, [](IAudioMessage *message) {
		delete message;
	});
	driver->SendMessage(message);

	return GamecodeValue();
}

// stopEffectLooping(id)
GamecodeValue AudioAdapter::StopEffectLooping(GamecodeParameterList *values, IGamecode *gamecode)
{
	values->RequireParmCount(1);
	int effectID = values->RequireType(0, GamecodeValue::T_Number).GetInt();

	StopLoopingAudioEffectMessage *message = new StopLoopingAudioEffectMessage();
	message->effectID = effectID;
	messageManager.AddMessage(message, [](IAudioMessage *message) {
		delete message;
	});
	driver->SendMessage(message);

	return GamecodeValue();
}

// removeEffect(id)
GamecodeValue AudioAdapter::RemoveEffect(GamecodeParameterList *values, IGamecode *gamecode)
{
	values->RequireParmCount(1);
	int effectID = values->RequireType(0, GamecodeValue::T_Number).GetInt();

	RemoveAudioEffectMessage *message = new RemoveAudioEffectMessage();
	message->effectID = effectID;
	messageManager.AddMessage(message, [](IAudioMessage *message) {
		delete message;
	});
	driver->SendMessage(message);

	return GamecodeValue();
}

// number createGroup(volume, spatialScale, headPosition)
GamecodeValue AudioAdapter::CreateGroup(GamecodeParameterList *values, IGamecode *gamecode)
{
	values->RequireParmCount(3);
	float volume = values->RequireType(0, GamecodeValue::T_Number).GetFloat();
	float spatialisationScale = values->RequireType(1, GamecodeValue::T_Number).GetFloat();
	OrientationF *headPosition = values->RequireEngineType<GamecodeOrientation>(2, "Orientation")->GetObject();

	int groupID = groupIds.Get();

	CreateAudioEffectGroupMessage *message = new CreateAudioEffectGroupMessage();
	message->groupID = groupID;
	message->volume = volume;
	message->spatialisationScale = spatialisationScale;
	message->headPosition = *headPosition;
	messageManager.AddMessage(message, [](IAudioMessage *message) {
		delete message;
	});
	driver->SendMessage(message);

	return GamecodeValue(groupID);
}

// setGroupVolume(id, volume)
GamecodeValue AudioAdapter::SetGroupVolume(GamecodeParameterList *values, IGamecode *gamecode)
{
	values->RequireParmCount(2);
	int groupID = values->RequireType(0, GamecodeValue::T_Number).GetInt();
	float volume = values->RequireType(1, GamecodeValue::T_Number).GetFloat();

	VolumeAudioEffectGroupMessage *message = new VolumeAudioEffectGroupMessage();
	message->groupID = groupID;
	message->volume = volume;
	messageManager.AddMessage(message, [](IAudioMessage *message) {
		delete message;
	});
	driver->SendMessage(message);

	return GamecodeValue();
}

// setGroupHeadPosition(id, orientation)
GamecodeValue AudioAdapter::SetGroupHeadPosition(GamecodeParameterList *values, IGamecode *gamecode)
{
	values->RequireParmCount(2);
	int groupID = values->RequireType(0, GamecodeValue::T_Number).GetInt();
	OrientationF *headPosition = values->RequireEngineType<GamecodeOrientation>(1, "Orientation")->GetObject();

	HeadPositionAudioEffectGroupMessage *message = new HeadPositionAudioEffectGroupMessage();
	message->groupID = groupID;
	message->headPosition = *headPosition;
	messageManager.AddMessage(message, [](IAudioMessage *message) {
		delete message;
	});
	driver->SendMessage(message);

	return GamecodeValue();
}

// removeGroup(id)
GamecodeValue AudioAdapter::RemoveGroup(GamecodeParameterList *values, IGamecode *gamecode)
{
	values->RequireParmCount(1);
	int groupID = values->RequireType(0, GamecodeValue::T_Number).GetInt();

	RemoveAudioEffectGroupMessage *message = new RemoveAudioEffectGroupMessage();
	message->groupID = groupID;
	messageManager.AddMessage(message, [](IAudioMessage *message) {
		delete message;
	});
	driver->SendMessage(message);

	return GamecodeValue();
}

//-----------------------------------------------------------------------------
// Helpers
//-----------------------------------------------------------------------------

IAudioSource* AudioAdapter::GetSourceFromString(std::string sourceString)
{
	// TODO: Either change the string for an object that can specify multiple types, or just parse the string.
	ResourceManager::ResourcePointer<SampleResource> sampleResource = resources->GetResource<SampleResource>(sourceString);
	if (sampleResource.GetResource()->GetFile() == nullptr)
	{
		return nullptr;
	}
	IAudioSource *sample = sampleResource.GetResource()->GetFile()->GetNewSource();
	return sample;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
