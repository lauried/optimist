/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <thread>
#include <functional>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// TODO: Allow for callbacks to be called multiple times and
// deleted separately.

/**
 * A callback that will only be executed within the thread it was constructed.
 */
class ThreadCallback {
public:
	ThreadCallback(std::function<void()> callback);

	/**
	 * Sets the callback to be called next time the owner thread triggers callbacks.
	 */
	void TriggerCall() { callNow = true; }

	/**
	 * Sets the callback to be deleted when it is next called.
	 */
	void TriggerDelete() { deleteNow = true; }

	/**
	 * Returns true if the callback was executed.
	 */
	bool CallIfReady();

	/**
	 * Returns true if the callback can be deleted.
	 */
	bool CanDelete() { return deleteNow; }

private:
	std::function<void()> callback;
	bool callNow;
	bool deleteNow;
	std::thread::id threadID;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

