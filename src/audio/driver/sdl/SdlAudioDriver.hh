/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <SDL/SDL.h>
#include "audio/driver/AbstractAudioDriver.hh"
#include "audio/AbstractAudioMessageReceiver.hh"
#include "library/threading/LockingQueue.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class SdlAudioDriver : public AbstractAudioDriver, public AbstractAudioMessageReceiver {
public:
	SdlAudioDriver();
	~SdlAudioDriver();

	virtual void SetAudioSource(IAudioSource *pSource);

	virtual void SetMessageHandler(IAudioMessageHandler *pHandler);

	//virtual void SendMessage(IAudioMessage *message);

	virtual void Start();

	virtual void Stop();

	/**
	 * Called internally only.
	 */
	void Callback(uint8_t *stream, int len);

private:
	SDL_AudioSpec desiredSpec, obtainedSpec;

	IAudioSource *source;
	IAudioMessageHandler *handler;

	bool rendering;
	LockingQueue<IAudioMessage*, 512> messageQueue;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

