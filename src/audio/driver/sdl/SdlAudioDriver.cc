/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "audio/driver/sdl/SdlAudioDriver.hh"
#include <exception>
#include <iostream>
#include <SDL/SDL.h>
#include "audio/AudioChannelInfo.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//## add_libraries SDL

//-----------------------------------------------------------------------------
// Mix AudioBuffer to our format
//-----------------------------------------------------------------------------

static void MixAudio(AudioBuffer *in, Uint8 *stream)
{
	int16_t *out = (int16_t*)stream;

	int channelsTimesSamples = in->NumSamples() * in->NumChannelsPerSample();
	const float *bufferData = in->GetRawSamples();
	for (int i = 0; i < channelsTimesSamples; i += 1)
	{
		float clamped = bufferData[i];
		if (clamped > 1) {
			clamped = 1;
		}
		if (clamped < -1) {
			clamped = -1;
		}
		*out = (int16_t)(clamped * 16384.0);
		out += 1;
	}
}

//-----------------------------------------------------------------------------
// Callback
//-----------------------------------------------------------------------------

static void SdlAudioCallback(void *userData, Uint8 *stream, int len)
{
	SdlAudioDriver *driver = (SdlAudioDriver*)userData;
	driver->Callback((uint8_t*)stream, len);
}

//-----------------------------------------------------------------------------
// SdlAudioDriver
//-----------------------------------------------------------------------------

SdlAudioDriver::SdlAudioDriver()
{
	source = nullptr;
	handler = nullptr;
	rendering = false;

	int init = SDL_WasInit(SDL_INIT_EVERYTHING);
	if (init == 0)
	{
		SDL_Init(SDL_INIT_AUDIO);
	}
	else if ((init & SDL_INIT_AUDIO) == 0)
	{
		SDL_InitSubSystem(SDL_INIT_AUDIO);
	}

	desiredSpec.freq = 44100;
	desiredSpec.format = AUDIO_S16SYS;
	desiredSpec.channels = 2;
	desiredSpec.samples = 8192;
	desiredSpec.callback = SdlAudioCallback;
	desiredSpec.userdata = (void*)this;

	if (SDL_OpenAudio(&desiredSpec, nullptr) < 0)
	{
		throw std::runtime_error("Failed to open audio");
	}

	obtainedSpec = desiredSpec;

	SDL_PauseAudio(1);
}

SdlAudioDriver::~SdlAudioDriver()
{
	int init = SDL_WasInit(SDL_INIT_AUDIO);
	if (init == SDL_INIT_AUDIO)
	{
		SDL_Quit();
	}
	else
	{
		SDL_QuitSubSystem(SDL_INIT_AUDIO);
	}
}

void SdlAudioDriver::SetAudioSource(IAudioSource *pSource)
{
	SDL_LockAudio();
	source = pSource;
	SDL_UnlockAudio();
}

void SdlAudioDriver::SetMessageHandler(IAudioMessageHandler *pHandler)
{
	SDL_LockAudio();
	handler = pHandler;
	SDL_UnlockAudio();
}

/*
void SdlAudioDriver::SendMessage(IAudioMessage *message)
{
	if (message == nullptr)
	{
		throw std::logic_error("Tried to send a null message");
	}
	messageQueue.Push(message);
}
*/

void SdlAudioDriver::Start()
{
	SDL_PauseAudio(0);
	rendering = true;
}

void SdlAudioDriver::Stop()
{
	rendering = false;
	SDL_PauseAudio(1);
	// Block until we're definitely not rendering.
	SDL_LockAudio();
	SDL_UnlockAudio();
}

static AudioChannelInfo InfoForNumChannels(int channels)
{
	switch (channels)
	{
	case 1:
		return MonoChannelInfo();
		break;
	case 2:
		return StereoChannelInfo();
		break;
	default:
		throw std::logic_error("Driver attempted to use a number of channels without providing AudioChannelInfo for it.");
	}
}

void SdlAudioDriver::Callback(uint8_t *stream, int len)
{
	const int AUDIO_SAMPLE_SIZE = 2;
	int numSamples = len / (obtainedSpec.channels * AUDIO_SAMPLE_SIZE);

	AudioBuffer buffer(InfoForNumChannels(obtainedSpec.channels), numSamples, obtainedSpec.freq);
	buffer.SetToZero();
	if (rendering && source != nullptr)
	{
		if (handler != nullptr)
		{
			HandleMessages(handler);
		}
		source->RenderToBuffer(&buffer);
	}
	MixAudio(&buffer, stream);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
