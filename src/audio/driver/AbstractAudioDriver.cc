/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "audio/driver/AbstractAudioDriver.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

ThreadCallback* AbstractAudioDriver::CreateThreadCallback(std::function<void()> callback)
{
	ThreadCallback *threadCallback = new ThreadCallback(callback);
    callbacks.push_back(threadCallback);
    return threadCallback;
}

void AbstractAudioDriver::CallThreadCallbacks()
{
	for (auto it = callbacks.begin(); it != callbacks.end();)
	{
		if ((*it)->CallIfReady())
		{
			if ((*it)->CanDelete())
			{
				delete (*it);
				it = callbacks.erase(it);
			}
			else
			{
				++it;
			}
		}
		else
		{
			++it;
		}
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
