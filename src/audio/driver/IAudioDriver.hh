/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <cinttypes>
#include "audio/IAudioSource.hh"
#include "audio/IAudioMessage.hh"
#include "audio/IAudioMessageHandler.hh"
#include "audio/IAudioMessageReceiver.hh"
#include "audio/driver/ThreadCallback.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * An audio target intended to render in realtime.
 * The design of this interface enables it to do so in another thread without
 * being blocked by any messages.
 */
class IAudioDriver : public virtual IAudioMessageReceiver {
public:
	virtual ~IAudioDriver();

	/**
	 * Sets the audio source that the driver will render from.
	 * Must only be performed while the driver is stopped.
	 * The audio source or anything it depends upon may not be altered by
	 * anything other than the audio driver or its message handler while the
	 * driver is running.
	 */
	virtual void SetAudioSource(IAudioSource *source) = 0;

	/**
	 * Sets the message handler that will handle messages passed by SendMessage.
	 * Must only be performed while the driver is stopped.
	 * The message handler or anything it depends upon may not be altered by
	 * anything other than the audio driver while the driver is running.
	 */
	virtual void SetMessageHandler(IAudioMessageHandler *handler) = 0;

	/**
	 * Sends a message to the audio driver via a threadsafe queue.
	 */
	//virtual void SendMessage(IAudioMessage *message) = 0;

	/**
	 * Starts the audio driver rendering.
	 */
	virtual void Start() = 0;

	/**
	 * Stops the audio driver from rendering audio, letting us change the
	 * audio source and message handler.
	 */
	virtual void Stop() = 0;

	/**
	 * Creates a callback object that code within the driver will mark as ready
	 * to call, and will then be called when the thread that created it calls
	 * CallThreadCallbacks.
	 */
	virtual ThreadCallback* CreateThreadCallback(std::function<void()> callback) = 0;

	/**
	 * Calls and deletes the callback objects which were created by the calling
	 * thread and had been triggered for calling.
	 */
	virtual void CallThreadCallbacks() = 0;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

