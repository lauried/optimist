/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "audio/AbstractAudioMessageReceiver.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

AbstractAudioMessageReceiver::~AbstractAudioMessageReceiver()
{
}

void AbstractAudioMessageReceiver::SendMessage(IAudioMessage *message)
{
	if (message == nullptr)
	{
		throw std::logic_error("Tried to send a null message");
	}
	messageQueue.Push(message);
}

void AbstractAudioMessageReceiver::HandleMessages(IAudioMessageHandler *handler)
{
	IAudioMessage *message = nullptr;
	while (messageQueue.Pop(&message))
	{
		if (message == nullptr)
		{
			throw std::logic_error("MessageQueue popped successfully but did not return a message");
		}
		handler->HandleMessage(message);
		message->SetHandled();
		message = nullptr;
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
