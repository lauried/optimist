/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * A message is passed to the audio driver and handled by an IMessageHandler.
 * It allows the driver to run the audio in a separate thread by passing
 * messages through a locking queue.
 */
class IAudioMessage {
public:
	IAudioMessage() : handled(false) {}
	virtual ~IAudioMessage();

	/**
	 * To be called by the audio driver after it's been passed to the IAudioMessageHandler.
	 */
	void SetHandled() { handled = true; }

	/**
	 * To be polled by code outside the audio driver which wants to hold on to the message.
	 */
	bool IsHandled() { return handled; }

	template<class T>
	bool Is() { return dynamic_cast<T*>(this) != nullptr; }

	template<class T>
	T* As() { return dynamic_cast<T*>(this); }

private:
	bool handled;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

