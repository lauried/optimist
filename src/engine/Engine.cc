/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "engine/Engine.hh"

#include "filesystem/PlatformFilesystem.hh"
#include "filesystem/ChrootFilesystem.hh"
#include "filesystem/OverlayFilesystem.hh"

#include "log/DummyLog.hh"
#include "resources/ResourceManager.hh"
#include "gamecode/vernacular/VernacularGamecode.hh"

#include "renderer/Renderer.hh"

#include "gamecode/library/LibraryAdapter.hh"

#include "platform/VideoAdapter.hh"
#include "platform/InputAdapter.hh"
#include "platform/TimeAdapter.hh"

#include "terrain/TerrainAdapter.hh"
#include "scenegraph/ScenegraphAdapter.hh"
#include "dynamics/DynamicsAdapter.hh"

#include "audio/effects/AudioEffectsMixer.hh"
#include "audio/AudioAdapter.hh"

#include "events/EventsAdapter.hh"
#include "events/EventListener.hh"
#include "platform/PlatformEventTranslator.hh"

#include "engine/FrameTimer.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

Engine::Engine(IPlatform *platform, IAudioDriver *audio, IRenderCore *renderCore)
{
	this->platform = platform;
	this->renderCore = renderCore;
	this->audio = audio;

	platform->AddListener(this);

	quit = false;
}

Engine::~Engine()
{
}

void Engine::Run(CommandLineArguments &arguments)
{
	std::string organisation = arguments.GetKey("--org", "SlightlyInteractive");
	std::string game = arguments.GetKey("--game", "Optimist");
	std::string basedir = arguments.GetKey("--baseDir", "");

	std::string appLocalDir = platform->GetUserLocalAppDataFolder(organisation, game);
	std::string appRoamingDir = platform->GetUserRoamingAppDataFolder(organisation, game);
	std::string userDocsDir = platform->GetUserDocumentsFolder(organisation, game);

	std::cout << "Base dir = " << basedir << std::endl;
	std::cout << "Local app data = " << appLocalDir << std::endl;
	std::cout << "Roaming app data = " << appRoamingDir << std::endl;
	std::cout << "Documents = " << userDocsDir << std::endl;

	PlatformFilesystem platformFilesystem;
	ChrootFilesystem chrootBase(&platformFilesystem, basedir);
	ChrootFilesystem chrootAppLocal(&platformFilesystem, appLocalDir);
	OverlayFilesystem overlayFilesystem;
	overlayFilesystem.AddFilesystem(&chrootBase, 1, "", false);
	overlayFilesystem.AddFilesystem(&chrootAppLocal, 0, "", true);

	IFilesystem *filesystem = &overlayFilesystem;

	// logs
	DummyLog gamecodeLog("gamecode");
	DummyLog rendererLog("renderer");

	ResourceManager resources(filesystem);
	resourceManager = &resources;

	Renderer renderer(renderCore, resourceManager, &rendererLog);

	// Set video mode.
	Vector2I screenSize;
	screenSize.Set(platform->RecommendWindowSize());
	platform->SetVideoMode(screenSize[0], screenSize[1], false, std::string("Framework Test"));
	renderCore->SetScreenSize(platform->GetScreenSize());

	// TODO: This is a bit of a hack - we need the renderer to load shaders
	// after the video mode has been set.
	// When we have a proper framework for engine components the renderer can
	// listen to video mode events and initialise its resources then.
	renderer.Init();

	// Init audio
	AudioEffectsMixer audioEffects;
	audio->SetAudioSource(&audioEffects);
	audio->SetMessageHandler(&audioEffects);
	audio->Start();

	// Init gamecode object and its adapters.
	VernacularGamecode vernacular(filesystem, &gamecodeLog);
	gamecode = &vernacular;

	// gamecode stuff
	LibraryAdapter libraryAdapter;
	gamecode->RegisterAdapter(&libraryAdapter);

	// platform type stuff
	AudioAdapter audioAdapter(audio, &resources);
	gamecode->RegisterAdapter(&audioAdapter);

	VideoAdapter videoAdapter(platform);
	gamecode->RegisterAdapter(&videoAdapter);
	InputAdapter inputAdapter(platform);
	gamecode->RegisterAdapter(&inputAdapter);
	TimeAdapter timeAdapter(platform);
	gamecode->RegisterAdapter(&timeAdapter);

	ScenegraphAdapter scenegraphAdapter(platform, &renderer, &resources);
	gamecode->RegisterAdapter(&scenegraphAdapter);

	// gamecode modules
	TerrainAdapter terrainAdapter(&resources, platform);
	gamecode->RegisterAdapter(&terrainAdapter);

	DynamicsAdapter dynamicsAdapter(&resources);
	gamecode->RegisterAdapter(&dynamicsAdapter);

	EventsAdapter eventsAdapter(platform);
	EventListener<PlatformEventTranslator, PlatformEventTranslator::DataType> platformEventListener("platform", &eventsAdapter);
	platform->AddListener(&platformEventListener);
	//platform->AddListener(&eventsAdapter);
	gamecode->RegisterAdapter(&eventsAdapter);

	// Gamecode load and start.
	vernacular.LoadFile("script/init.vrn");
	std::vector<GamecodeValue> emptyParms;
	std::string initFunc("init");
	std::string frameFunc("frame");
	std::string renderFunc("render");
	gamecode->CallFunction(initFunc, emptyParms);

	// Main loop.
	FrameTimer timer(platform);
	while(!quit)
	{
		timer.Update(20);
		audio->CallThreadCallbacks();
		audioAdapter.Frame(timer.FrameTime());
		platform->ProcessInput();
		gamecode->CallFunction(frameFunc, emptyParms);
		// TODO: Physics code here, or between two gamecode calls for pre and post physics.
		platform->SwapBuffers();
		renderCore->StartFrame();
		// TODO: Might be nicer to automatically render whatever scene is configured.
		// The scene could describe multiple viewports and things.
		gamecode->CallFunction(renderFunc, emptyParms);
		renderCore->EndFrame();
	}

	// Shutdown.
	audio->Stop();
	platform->Exit();
}

void Engine::HandleEvent(PlatformEvent &event, IPlatform::EventState &state)
{
	// TODO: If we put ourselves after the events adapter, then we can handle
	// this event if the gamecode doesn't do anything about it.
	if (event.type == PlatformEvent::PE_WindowClose)
	{
		quit = true;
		state.Finish();
		return;
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
