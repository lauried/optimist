/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "library/text/CommandLineArguments.hh"
#include "platform/IPlatform.hh"
#include "audio/driver/IAudioDriver.hh"
#include "renderer/IRenderCore.hh"
#include "gamecode/IGamecode.hh"
#include "resources/ResourceManager.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class Engine : public IPlatform::EventListener {
public:
	Engine(IPlatform *platform, IAudioDriver *audio, IRenderCore *renderCore);
	~Engine();

	// TODO: Various configuration.

	/**
	 * Runs the engine until it stops.
	 */
	void Run(CommandLineArguments &arguments);

	IPlatform* Platform() { return platform; }
	IRenderCore* RenderCore() { return renderCore; }
	ResourceManager* Resources() { return resourceManager; }

	// IPlatform::EventListener
	void HandleEvent(PlatformEvent &event, IPlatform::EventState &state);

private:
	ResourceManager *resourceManager;
	IPlatform *platform;
	IAudioDriver *audio;
	IRenderCore *renderCore;
	IGamecode *gamecode;
	bool quit;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

