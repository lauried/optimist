/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "platform/IPlatformTime.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Utility for calculating the total time elapsed as well as the time between
 * frames, and limiting the time each frame takes.
 */
class FrameTimer {
public:
	/**
	 * Constructs with the default ticks per second of 1024.0, which while
	 * causing game seconds to be slower will cause lose no precision in
	 * conversion from millisecond ticks.
	 */
	FrameTimer(IPlatformTime *platform) :
		platform(platform),
		oldTicks(0),
		cumulativeTicks(0),
		ticksPerSecond(1024.0),
		time(0),
		frameTime(0)
		{
			oldTicks = platform->GetTime();
		}

	/**
	 * Construct with the specified number of ticks per second.
	 */
	FrameTimer(IPlatformTime *platform, double ticksPerSecond) :
		platform(platform),
		oldTicks(0),
		cumulativeTicks(0),
		ticksPerSecond(ticksPerSecond),
		time(0),
		frameTime(0)
		{
			oldTicks = platform->GetTime();
		}

	/**
	 * Updates the time and frameTime. Frametime is the time between successive
	 * updates and time is the total time elapsed since instantiation.
	 */
	void Update(int64_t limit)
	{
		int64_t newTicks = platform->GetTime();
		int64_t deltaTime = newTicks - oldTicks;
		if (deltaTime < 0)
		{
			deltaTime = 0;
		}
		if (deltaTime < limit)
		{
			platform->Delay(limit - deltaTime);
			newTicks = platform->GetTime();
			deltaTime = newTicks - oldTicks;
		}
		oldTicks = newTicks;
		cumulativeTicks += deltaTime;
		time = (double)cumulativeTicks / ticksPerSecond;
		frameTime = (double)deltaTime / ticksPerSecond;
	}

	/**
	 * Returns the total time elapsed since timer instantiation until the most
	 * recent call to Update().
	 */
	double Time() { return time; }

	/**
	 * Returns the time elapsed between the two most recent calls to Update().
	 */
	double FrameTime() { return frameTime; }

private:
	IPlatformTime *platform;
	int64_t oldTicks;
	int64_t cumulativeTicks;

	double ticksPerSecond;

	double time;
	double frameTime;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

