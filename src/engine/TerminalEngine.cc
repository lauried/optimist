/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "engine/TerminalEngine.hh"
#include "log/FileLog.hh"
#include "log/DummyLog.hh"
#include "log/NullLog.hh"
#include "filesystem/PlatformFilesystem.hh"
#include "filesystem/ChrootFilesystem.hh"
#include "filesystem/OverlayFilesystem.hh"
#include "filesystem/File.hh"
#include "platform/ncurses/NcursesPlatform.hh"
#include "platform/InputAdapter.hh"
#include "library/text/Path.hh"
#include "gamecode/vernacular/VernacularGamecode.hh"
#include "gamecode/library/LibraryAdapter.hh"
#include "platform/VideoAdapter.hh"
#include "platform/InputAdapter.hh"
#include "platform/TimeAdapter.hh"
#include "platform/PlatformEventTranslator.hh"
#include "events/EventsAdapter.hh"
#include "events/EventListener.hh"
#include "terminal/TerminalDisplayAdapter.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

void TerminalEngine::Run(CommandLineArguments &arguments, IPlatformInfo *info)
{
	std::string organisation = arguments.GetKey("--org", "SlightlyInteractive");
	std::string game = arguments.GetKey("--game", "Optimist");
	std::string baseDir = arguments.GetKey("--baseDir", "");
	std::string logTarget = arguments.GetKey("--logFile", "log.txt");

	std::string appLocalDir = info->GetUserLocalAppDataFolder(organisation, game);
	std::string appRoamingDir = info->GetUserRoamingAppDataFolder(organisation, game);
	std::string userDocsDir = info->GetUserDocumentsFolder(organisation, game);

	PlatformFilesystem platformFilesystem;
	platformFilesystem.CreateDirectory(appLocalDir);
	platformFilesystem.CreateDirectory(appRoamingDir);
	platformFilesystem.CreateDirectory(userDocsDir);

	// This allows us to read from the current directory, but writes will be
	// performed in the app local directory.
	ChrootFilesystem chrootBase(&platformFilesystem, baseDir);
	ChrootFilesystem chrootAppLocal(&platformFilesystem, appLocalDir);
	OverlayFilesystem overlayFilesystem;
	overlayFilesystem.AddFilesystem(&chrootBase, 1, "", false);
	overlayFilesystem.AddFilesystem(&chrootAppLocal, 0, "", true);

	ILogTarget *log;
	IFile *logFile = nullptr;
	if (logTarget.size())
	{
		Path path(logTarget, Path::F_Path);
		overlayFilesystem.CreateDirectory(path.path);
		logFile = overlayFilesystem.Open(logTarget, IFilesystem::T_Append);
		log = new FileLog(logFile);
	}
	else
	{
		log = new NullLog();
	}

	log->Start(ILogTarget::NOTIFICATION)->Write("Starting")->End();

	log->Start(ILogTarget::NOTIFICATION)->Write("baseDir=" + baseDir)->End();
	log->Start(ILogTarget::NOTIFICATION)->Write("appLocalDir=" + appLocalDir)->End();
	log->Start(ILogTarget::NOTIFICATION)->Write("appRoamingDir=" + appRoamingDir)->End();
	log->Start(ILogTarget::NOTIFICATION)->Write("userDocsDir=" + userDocsDir)->End();
	log->Start(ILogTarget::NOTIFICATION)->Write("logTarget=" + logTarget)->End();

	VernacularGamecode vernacular(&overlayFilesystem, log);
	IGamecode *gamecode = &vernacular;

	LibraryAdapter libraryAdapter;
	gamecode->RegisterAdapter(&libraryAdapter);

	NcursesPlatform ncurses;
	InputAdapter inputAdapter(&ncurses);
	gamecode->RegisterAdapter(&inputAdapter);
	TimeAdapter timeAdapter(&ncurses);
	gamecode->RegisterAdapter(&timeAdapter);
	TerminalDisplayAdapter terminalDisplayAdapter(&ncurses);
	gamecode->RegisterAdapter(&terminalDisplayAdapter);

	EventsAdapter events(&ncurses);
	EventListener<PlatformEventTranslator, PlatformEventTranslator::DataType> platformEventListener("platform", &events);
	ncurses.AddListener(&platformEventListener);
	gamecode->RegisterAdapter(&events);

	try {
		// Load and run the init file.
		std::string initFile = arguments.GetKey("--initFile", "init.vrn");
		vernacular.LoadFile(initFile);

		// This lets the gamecode decide when to quit, simply by not triggering any
		// new events. It can also control the framerate.
		for (uint64_t nextEvent = events.TimeUntilNextEvent(); nextEvent != 0; nextEvent = events.TimeUntilNextEvent())
		{
			ncurses.Delay(nextEvent);
			ncurses.ProcessInput();
			events.TriggerEvents();
		}
	} catch (std::exception &e) {
		// Let us exit gracefully if an exception is thrown (like by gamecode).
		log->Start(ILogTarget::ERROR)->Write("Exception: ")->Write(e.what())->End();
		std::cout << "EXCEPTION! " << e.what() << std::endl;
	}

	log->Start(ILogTarget::NOTIFICATION)->Write("Finished")->End();

	delete log;
	if (logFile != nullptr)
	{
		overlayFilesystem.Close(logFile);
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

int main(int argc, char *argv[])
{
#ifdef _WIN32
	opti::IPlatformInfo *platformInfo = new opti::WindowsPlatformInfo();
#else
	opti::IPlatformInfo *platformInfo = new opti::LinuxPlatformInfo();
#endif
	opti::CommandLineArguments arguments(argc, argv);
	opti::TerminalEngine engine;
	engine.Run(arguments, platformInfo);

	return 0;
}
