/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "engine/Main.hh"

#include <iostream>

#include "engine/Engine.hh"

#include "platform/IPlatform.hh"
#include "platform/sdl/SdlPlatform.hh"

#include "renderer/IRenderCore.hh"
#include "renderer/opengl/OpenGLRenderCore.hh"

#include "audio/driver/IAudioDriver.hh"
#include "audio/driver/sdl/SdlAudioDriver.hh"

#include "library/text/CommandLineArguments.hh"

#ifdef _WIN32
#include "platform/WindowsPlatformInfo.hh"
#else
#include "platform/LinuxPlatformInfo.hh"
#endif

using namespace opti;

int main(int argc, char *argv[])
{
	CommandLineArguments arguments(argc, argv);

	try
	{
#ifdef _WIN32
		IPlatformInfo *platformInfo = new WindowsPlatformInfo();
#else
		IPlatformInfo *platformInfo = new LinuxPlatformInfo();
#endif
		SdlPlatform sdlPlatform(platformInfo);
		SdlAudioDriver sdlAudio;
		OpenGLRenderCore glRenderCore;

		Engine engine(&sdlPlatform, &sdlAudio, &glRenderCore);
		engine.Run(arguments);
	}
	catch (std::exception &e)
	{
		std::cout << "EXCEPTION! " << e.what() << std::endl;
		return 1;
	}
	return 0;
}
