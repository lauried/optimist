/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "renderer/MaterialManager.hh"
#include "renderer/IRenderResources.hh"
#include "renderer/RendererStacks.hh"
#include "resources/ResourceManager.hh"
#include "log/ILogTarget.hh"
#include "library/geometry/Vector3.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class Renderer : public RendererStacks, public IRenderResources {
public:
	Renderer(IRenderCore *renderCore, ResourceManager *resourceManager, ILogTarget *log)
		: RendererStacks(renderCore)
		, renderCore(renderCore)
		, resourceManager(resourceManager)
		, log(log)
		, defaultTexture(nullptr)
		{}

	~Renderer();

	void Init();

	// Own interface
	void SetSaturation(float saturation);
	void SetGamma(float gamma);
	void SetLighting(Vector3F normal, Vector3F colour, float multiply, float add);
	void SetLightingFull();
	void SetLightingOff();

	// IRenderCore
	void ClearBuffer(int buffer);
	void SetColourEnabled(bool enable);
	void SetDepthEnabled(bool enable);
	void SetBlend(IRenderCore::Blend blend);
	void SetMaterial(const Material &material);
	void DrawMesh(const MeshData &mesh);

	// IRenderResources
	virtual const Material * GetMaterial(const MaterialDescriptor& descriptor);

private:
	struct Shader {
		IShader *shader;
		int gamma, saturation;
		int lightNormal, lightColour, lightMultiply, lightAdd;
	};

	MaterialManager materialManager;
	IRenderCore *renderCore;
	ResourceManager *resourceManager;
	ILogTarget *log;

	Image defaultImage;
	ITexture *defaultTexture;

	struct {
		Shader lighting;
	} shaders;

	struct {
		float gamma;
		float saturation;
		Vector3F lightNormal;
		Vector3F lightColour;
		float lightMultiply;
		float lightAdd;
	} options;

	void UpdateMaterial(const Material& material);
	ITexture *GetDefaultTexture();
	void LoadShader(std::string filename, Shader *data);
	int ReadShaderUniform(IShader *shader, std::string name);
	void SetShaderParameters(Shader *data);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
