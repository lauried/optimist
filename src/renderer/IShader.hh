/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <string>
#include <sstream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class IShader {
public:
	/**
	 * Returns a string identifying the native type of the shader, e.g.
	 * "arb", "glsl" without any version.
	 */
	virtual const std::string & GetNativeType() = 0;

	/**
	 * Initialises the shader from a source file in its native format.
	 * Returns true if the script was successfully set, else false.
	 */
	virtual bool SetNative(std::stringstream &source, std::string name) = 0;

	/**
	 * Binds this shader, or unbinds the previous and binds this one.
	 */
	virtual void Bind() = 0;

	/**
	 * Unbinds this shader.
	 */
	virtual void Unbind() = 0;

	virtual int GetUniformIndex(std::string name) = 0;
	virtual void SetUniform4f(int index, float x, float y, float z, float w) = 0;
	virtual void SetUniform3f(int index, float x, float y, float z) = 0;
	virtual void SetUniform2f(int index, float x, float y) = 0;
	virtual void SetUniform1f(int index, float x) = 0;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

