/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "renderer/MeshBuilder.hh"
#include "library/geometry/Vector3.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

void MeshBuilder::AddAttribute(AttributeType type, int components)
{
	VertexAttribute attr;
	attr.type = type;
	attr.offset = vertSize;
	attr.components = components;

	switch (type)
	{
	case VERTEX:
		vertexAttribute = attributes.size();
		break;
	case COLOUR:
		colourAttribute = attributes.size();
		break;
	case NORMAL:
		normalAttribute = attributes.size();
		break;
	case TEXCOORD:
		texCoordAttribute = attributes.size();
		break;
	default:
		break;
	}

	attributes.push_back(attr);
	vertSize += components;
}

int MeshBuilder::FindAttribute(AttributeType type, int minComponents)
{
	for (int i = 0; i < attributes.size(); ++i)
	{
		if (attributes[i].type == type && attributes[i].components >= minComponents)
			return i;
	}
	return -1;
}

void MeshBuilder::AllocateTris(int allocTris)
{
	AllocateVerts(allocTris * 3);
}

void MeshBuilder::AllocateQuads(int allocQuads)
{
	AllocateVerts(allocQuads * 4);
}

void MeshBuilder::AllocateVerts(int allocVerts)
{
	assert(allocVerts >= 1);

	// If there's no attributes, add some.
	if (attributes.size() == 0)
	{
		AddAttribute(VERTEX, 3);
		AddAttribute(NORMAL, 3);
		AddAttribute(COLOUR, 3);
	}

	// Allocate space for everything.
	delete data;
	data = new float[allocVerts * vertSize];
	numVerts = 0;
	allocatedVerts = allocVerts;

	// Hook up the legacy pointers.
	for (int i = 0; i < attributes.size(); ++i)
	{
		switch (attributes[i].type)
		{
		case VERTEX:
			verts.data.f = &data[attributes[i].offset];
			verts.components = attributes[i].components;
			verts.step = vertSize * sizeof(float);
			break;
		case NORMAL:
			normals.data.f = &data[attributes[i].offset];
			normals.components = attributes[i].components;
			normals.step = vertSize * sizeof(float);
			break;
		case TEXCOORD:
			texcoords.data.f = &data[attributes[i].offset];
			texcoords.components = attributes[i].components;
			texcoords.step = vertSize * sizeof(float);
			break;
		case COLOUR:
			colours.data.f = &data[attributes[i].offset];
			colours.components = attributes[i].components;
			colours.step = vertSize * sizeof(float);
			break;
		}
	}

	for (int i = 0; i < attributes.size(); ++i)
	{
		attributes[i].SetData(data, vertSize);
	}
}

inline void CopyElements(const float *src, float *dest, int numElements)
{
	for (int i = 0; i < numElements; ++i)
	{
		dest[i] = src[i];
	}
}

void MeshBuilder::AddVertex(float *v, float *n, float *c, float *t)
{
	assert(numVerts < allocatedVerts);
	int index = numVerts * vertSize;
	if (v && vertexAttribute >= 0)
	{
		CopyElements(v, &data[index + attributes[vertexAttribute].offset], attributes[vertexAttribute].components);
		// TODO: Should this be done externally instead?
		for (int axis = 0; axis < attributes[vertexAttribute].components; ++axis)
		{
			if (v[axis] < bounds.mins[axis]) bounds.mins[axis] = v[axis];
			if (v[axis] > bounds.maxs[axis]) bounds.maxs[axis] = v[axis];
		}
	}
	if (n && normalAttribute >= 0)
	{
		CopyElements(n, &data[index + attributes[normalAttribute].offset], attributes[normalAttribute].components);
	}
	if (c && colourAttribute >= 0)
	{
		CopyElements(c, &data[index + attributes[colourAttribute].offset], attributes[colourAttribute].components);
	}
	if (t && texCoordAttribute >= 0)
	{
		CopyElements(t, &data[index + attributes[texCoordAttribute].offset], attributes[texCoordAttribute].components);
	}
	++numVerts;
}

//-----------------------------------------------------------------------------
// TriangleMesh::VertexAttribute
//-----------------------------------------------------------------------------

void MeshBuilder::VertexAttribute::SetData(float *data, int vertSize)
{
	this->data = data;
	this->vertSize = vertSize;
}

float *MeshBuilder::VertexAttribute::Element(int i)
{
	return &data[(i * vertSize) + offset];
}

const float *MeshBuilder::VertexAttribute::Element(int i) const
{
	return &data[(i * vertSize) + offset];
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
