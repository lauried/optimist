/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <vector>
#include "library/geometry/Orientation.hh"
#include "library/file_types/BvhFile.hh"
#include "renderer/Model.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// TODO:
// Look at LOD support.
// Look at whether the classes are grouped right or if they can be arranged a
// little more conveniently.

class ModelPartAnimation {
public:
	ModelPartAnimation(const Model *model, const BvhFile *bvh);

	int NumFrames() const;
	float FrameRate() const { return frameRate; }
	float FrameTime() const { return frameTime; }
	void SetFrame(float frame);

	// Returns the orientation in modelspace for the given model part index.
	// If there's no such part in the bvh then an identity orientation is
	// returned.
	const OrientationF &GetPartOrientation(int part) const;

private:
	const Model *model;
	const BvhFile *bvh;
	std::vector<Vector3F> modelJointOffsets;
	std::vector<OrientationF> joints;
	std::vector<int> partToJointMap;
	std::vector<int> jointToPartMap;

	float frameRate;
	float frameTime;
	float currentFrame;

	int GetPartParent(int part);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
