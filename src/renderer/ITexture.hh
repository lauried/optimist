/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "library/graphics/Image.hh"
#include <functional>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class ITexture {
public:
	// TODO: Hint when setting image whether to resample or pad it (for wrapping)
	// TODO: Get the texture coordinates for a padded texture.

	enum SizingHint {
		SH_Resample,
		SH_Border
	};

	/**
	 * Sets a new image to the texture resource.
	 */
	virtual void SetImage(Image *img, SizingHint sizing = SH_Resample) = 0;

	/**
	 * Sets a callback which will be called when uploading the image.
	 */
	virtual void SetUploadCallback(std::function<void(Image*, Image*)> uploadCallback) = 0;

	/**
	 * Notifies the renderer that the texture has been modified and must be
	 * re-uploaded.
	 */
	virtual void RequireUpload() = 0;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
