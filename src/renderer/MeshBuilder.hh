/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <limits>
#include <vector>
#include "renderer/MeshData.hh"
#include "library/geometry/Box3.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// Triangle mesh represents an array of triangles of a single material.
// A model will consist of one or more meshes.
// A rigid model with moving parts will have an array of orientations to apply
// to each mesh, according to which part they belong to.

/**
 * This is actually a mesh builder.
 * The real mesh class is MeshData.
 */
class MeshBuilder : public MeshData {
public:
	static const int MAX_ATTRIBUTES = 8;

	enum AttributeType {
		VERTEX,
		COLOUR,
		NORMAL,
		TEXCOORD
	};

	class VertexAttribute {
	public:
		AttributeType type;
		int offset;
		int components;
		VertexAttribute() : type(VERTEX), offset(0), components(0) { }
		void SetData(float *data, int vertSize);
		float *Element(int i);
		const float *Element(int i) const;
	private:
		float *data;
		int vertSize;
	};

	MeshBuilder() :
		MeshData(),
		vertSize(0),
		vertexAttribute(-1),
		normalAttribute(-1),
		colourAttribute(-1),
		texCoordAttribute(-1),
		allocatedVerts(0),
		data(nullptr),
		fullbright(false)
	{
		bounds.mins.Set( std::numeric_limits<float>::max());
		bounds.maxs.Set(-std::numeric_limits<float>::max());
	}

	~MeshBuilder()
	{
		delete[] data;
	}

	void AddAttribute(AttributeType type, int components);
	void AllocateVerts(int allocVerts);
	void AllocateTris(int allocTris);
	void AllocateQuads(int allocQuads);
	void AddVertex(float *v, float *n, float *c, float *t);
	void ClearVerts() { numVerts = 0; }

	int FindAttribute(AttributeType, int minComponents);

	// Size of vertex in floats.
	int VertexSize() const { return vertSize; }

	std::vector<VertexAttribute> attributes;
	int vertSize;

	// Indexes to the attribute data vector.
	int vertexAttribute;
	int normalAttribute;
	int colourAttribute;
	int texCoordAttribute;

	int allocatedVerts; // number of vertices allocated

	float *data;

	Box3F bounds;

	// This is a quick hack that need eliminating by the addition of materials.
	bool fullbright;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
