/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "renderer/Material.hh"
#include <cstdint>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * This contains all the pointer data at once.
 * This is the real mesh, the Mesh class is really a builder.
 */
class MeshData {
public:
	enum ChannelType {
		VERTS,
		NORMALS,
		TEXCOORDS,
		COLOURS,
		AUX
	};

	enum MeshType {
		TRIANGLE,
		TRIANGLE_FAN,
		TRIANGLE_STRIP,
		QUAD,
		QUAD_STRIP,
		LINE,
		LINE_STRIP,
		LINE_LOOP,
		POINT
	};

	enum DataType {
		DOUBLE,
		FLOAT,
		UINT,
		INT,
		SHORT,
		USHORT,
		CHAR,
		UCHAR
	};

	union DataPointer {
		double *d;
		float *f;
		uint32_t *ui;
		uint16_t *us;
		uint8_t *uc;
		int32_t *i;
		int16_t *s;
		int8_t *c;
		DataPointer() : d(nullptr) {}
	};

	struct Channel {
		DataType type;
		DataPointer data;
		int components;
		int step;
		Channel() : type(FLOAT), components(0), step(0) {}
	};

	MeshData() : type(TRIANGLE), numVerts(0), numIndices(0), indices(nullptr) {}

	MeshType type;

	int numVerts;
	int numIndices;
	int *indices;

	Channel verts;
	Channel normals;
	Channel texcoords;
	Channel colours;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

