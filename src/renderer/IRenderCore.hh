/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "library/geometry/Vector2.hh"
#include "library/geometry/Vector3.hh"
#include "library/geometry/Orientation.hh"
#include "renderer/ITexture.hh"
#include "renderer/IShader.hh"
#include "renderer/Material.hh"
#include "renderer/MeshData.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class IRenderCore {
public:
	/**
	 * Enum for buffer clearing. Bitfield.
	 */
	enum Buffer {
		ColourBuffer = 1,
		DepthBuffer = 2,
	};

	enum Blend {
		BlendSolid = 0,
		BlendAlpha = 1,
		BlendAdditive = 2,
	};

	/**
	 * Signals the start of the frame to the renderer.
	 */
	virtual void StartFrame() = 0;

	/**
	 * Signals the end of the frame to the renderer, that no more rendering
	 * will be done for this frame. The platform may be responsible for
	 * swapping buffers however.
	 */
	virtual void EndFrame() = 0;

	/**
	 * Indicate the size of the screen in pixels.
	 */
	virtual void SetScreenSize(const Vector2I &size) = 0;

	/**
	 * Sets the position of the viewport on screen (or the current window),
	 * in pixels.
	 */
	virtual void SetViewport(const Vector2I &topLeft, const Vector2I &bottomRight) = 0;

	/**
	 * Gets the position of the viewport.
	 */
	virtual void GetViewport(Vector2I *topLeft, Vector2I *bottomRight) = 0;

	/**
	 * Set an orthographic projection.
	 */
	virtual void SetOrthographicProjection(float left, float top, float right, float bottom, float nearPlane, float farPlane) = 0;

	/**
	 * Set a perspective projection.
	 */
	virtual void SetPerspectiveProjection(float left, float top, float right, float bottom, float nearPlane, float farPlane) = 0;

	/**
	 * Set the position of the camera in world space.
	 */
	virtual void SetView(const OrientationF &view) = 0;

	/**
	 * Set the position of the model in world space.
	 */
	virtual void SetModel(const OrientationF &model) = 0;

	/**
	 * Clears the colour and/or depth buffer.
	 * The default clear colour is black.
	 * Uses Buffer enum as bitfield.
	 */
	virtual void ClearBuffer(int buffer) = 0;

	/**
	 * Sets whether the colour buffer is written.
	 */
	virtual void SetColourEnabled(bool enable) = 0;

	/**
	 * Sets whether the depth buffer is written.
	 */
	virtual void SetDepthEnabled(bool enable) = 0;

	/**
	 * Sets the blend mode.
	 */
	virtual void SetBlend(IRenderCore::Blend blend) = 0;

	/**
	 * Creates and returns an object implementing ITexture.
	 * The caller is responsible for freeing this object by calling FreeTexture.
	 */
	virtual ITexture *CreateTexture() = 0;

	/**
	 * Deletes an ITexture object created by this renderer instance.
	 */
	virtual void FreeTexture(ITexture *texture) = 0;

	/**
	 * Creates and returns an object implementing IShader.
	 * The caller is responsible for freeing this object by calling FreeShader.
	 * @param desiredType Specifies the desired type of the shader. If the
	 * rendercore can create that type of shader, one must be returned. If not
	 * then the renderer must return a format that it supports.
	 * The renderer may only return nullptr if no shader types are supported.
	 */
	virtual IShader *CreateShader(std::string desiredType = "") = 0;

	/**
	 * Deletes an IShader object created by this renderer instance.
	 */
	virtual void FreeShader(IShader *shader) = 0;

	/**
	 * Copies the currently drawn viewport to the given texture.
	 * The texture coordinates are also set in case a texture cannot be created
	 * at the size of the viewport.
	 */
	virtual void ViewportToTexture(ITexture *texture, Vector2F *texTopLeft, Vector2F *texBottomRight) = 0;

	/**
	 * Sets state for the specified material.
	 */
	virtual void SetMaterial(const Material &material) = 0;

	/**
	 * Draws a triangle mesh with the currently set material, or default state
	 * otherwise.
	 */
	virtual void DrawMesh(const MeshData &mesh) = 0;

	/**
	 * Draws a test pattern. The pattern chosen is implementation specific.
	 */
	virtual void DrawTest(int test) = 0;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
