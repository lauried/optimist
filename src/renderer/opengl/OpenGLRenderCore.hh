/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <set>
#include <vector>
#include <string>
#include "renderer/IRenderCore.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class OpenGLRenderCore : public IRenderCore {
public:
	OpenGLRenderCore();
	~OpenGLRenderCore();

	// IRenderCore
	void StartFrame();
	void EndFrame();
	void SetScreenSize(const Vector2I &size) { screenSize = size; }
	void SetViewport(const Vector2I &topLeft, const Vector2I &bottomRight);
	void GetViewport(Vector2I *topLeft, Vector2I *bottomRight);
	void SetOrthographicProjection(float left, float top, float right, float bottom, float nearPlane, float farPlane);
	void SetPerspectiveProjection(float left, float top, float right, float bottom, float nearPlane, float farPlane);
	void SetView(const OrientationF &view);
	void SetModel(const OrientationF &model);
	const OrientationF& GetModelview();
	void ClearBuffer(int buffer);
	void SetColourEnabled(bool enable);
	void SetDepthEnabled(bool enable);
	void SetBlend(IRenderCore::Blend blend);
	ITexture *CreateTexture();
	void FreeTexture(ITexture *texture);
	IShader *CreateShader(std::string desiredType = "");
	void FreeShader(IShader *shader);
	void ViewportToTexture(ITexture *texture, Vector2F *topLeft, Vector2F *bottomRight);
	void SetMaterial(const Material &material);
	void DrawMesh(const MeshData &mesh);
	void DrawTest(int test);

	// Our own methods.
	void GetExtensions(std::vector<std::string> *extensions);

private:
	class TextureGl : public ITexture {
	public:
		// ITexture
		void SetImage(Image *img, ITexture::SizingHint sizing = SH_Resample);
		void SetUploadCallback(std::function<void(Image*, Image*)> uploadCallback);
		void RequireUpload();

		// Implementation
		Image *image;
		bool requiresUpload;
		ITexture::SizingHint sizing;
		uint32_t glTextureNum;

		bool mipmap;
		MaterialDescriptor::FilterType filter;
		bool updateTexParams;

		std::function<void(Image*, Image*)> uploadCallback;

		TextureGl() :
			image(nullptr),
			requiresUpload(false),
			glTextureNum(0),
			mipmap(true),
			filter(MaterialDescriptor::FilterLinear),
			updateTexParams(false)
			{ }

		void SetMipmap(bool enabled);
		void SetFilter(MaterialDescriptor::FilterType filter);

		bool Upload();
		void Bind();
	};

	std::set<TextureGl*> textures;
	Vector2I screenSize;
	Vector2I viewportBottomLeft, viewportSize;
	OrientationF modelMatrix, viewMatrix;
	OrientationF currentModelview;

	float *meshBuffer;
	int meshBufferSize;

	// Error checking
	bool hasViewport;
	bool hasProjection;
	bool inFrame;

	void SetUpModelView();
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
