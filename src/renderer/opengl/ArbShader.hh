/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include "renderer/IShader.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class ArbShader : public IShader {
public:
	ArbShader();
	~ArbShader();

	virtual const std::string & GetNativeType();
	virtual bool SetNative(std::stringstream &input, std::string name);
	virtual void Bind();
	virtual void Unbind();
	virtual int GetUniformIndex(std::string name);
	virtual void SetUniform4f(int index, float x, float y, float z, float w);
	virtual void SetUniform3f(int index, float x, float y, float z);
	virtual void SetUniform2f(int index, float x, float y);
	virtual void SetUniform1f(int index, float x);

private:
	struct Uniform {
		int local; // must always index a valid local in locals.
		int offset;
		int size;
	};
	struct Local {
		bool marked;
		float v[4];

		Local()
			: marked(0)
			, v{ 0.0f, 0.0f, 0.0f, 0.0f }
			{}
	};

	bool isLoaded;
	std::string filename;
	int program;
	std::vector<Local> locals;
	std::vector<Uniform> uniforms;
	std::map<std::string, int> uniformIndexes;
	bool uniformChanged;

	void ProcessFile(std::string name, std::stringstream &input, std::stringstream &output);
	void Parse(std::stringstream &code, std::string name);
	void CheckUniformIndex(int index);
	void WriteUniformValue(Uniform &uniform, int index, float value);
	void UpdateLocals();
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

