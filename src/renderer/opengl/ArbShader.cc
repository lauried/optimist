/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "renderer/opengl/ArbShader.hh"
#include <cstdio>
#include <iostream>
#include <stdexcept>
#include <sstream>
#ifdef _WIN32
// Fixes compile errors in gl.h
#include <windows.h>
#endif
#include "library/extern/GLee.h"
#include "library/text/String.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

ArbShader::ArbShader()
{
	isLoaded = false;
	uniformChanged = false;
}

ArbShader::~ArbShader()
{
	if (isLoaded)
	{
		glDeleteProgramsARB(1, (unsigned int*)&program);
	}
}

const std::string & ArbShader::GetNativeType()
{
	static std::string type("arb");
	return type;
}

//-----------------------------------------------------------------------------
// Load
//-----------------------------------------------------------------------------

static void GetUniformOffsetSize(std::string spec, int* offset, int* size)
{
	*size = spec.size();
	if (String::StartsWith(spec, "xyzw"))
	{
		*offset = 0;
		return;
	}
	if (String::StartsWith(spec, "yzw"))
	{
		*offset = 1;
		return;
	}
	if (String::StartsWith(spec, "zw"))
	{
		*offset = 3;
		return;
	}
	if (!spec.compare("w"))
	{
		*offset = 4;
		return;
	}
	throw std::runtime_error("Invalid uniform specifier " + spec + ", must be something like 'x', 'zw', 'xyz' etc.");
}

static std::string GetUniformName(int index, int offset, int size)
{
	if (offset + size > 4)
	{
		throw std::logic_error("Invalid offset and size for uniform");
	}
	char buf[6] = { '.', 'x', 'y', 'z', 'w', '\0' };
	buf[1 + offset + size] = '\0';
	if (size == 4)
	{
		buf[0] = '\0';
	}
	return "program.local[" + String::ToString(index) + "]" + buf;
}

void ArbShader::ProcessFile(std::string name, std::stringstream &input, std::stringstream &output)
{
	std::vector<std::string> searches;
	std::vector<std::string> replaces;

	const int MAX_LINE_LENGTH = 512;
	char buffer[MAX_LINE_LENGTH];
	while (!input.eof())
	{
		std::string line;
		std::getline(input, line);
		if (line[0] == '@')
		{
			std::vector<std::string> parts;
			String::Split(line, &parts, " ", 0, false);
			// @alias <search> <replace>
			if (!parts[0].compare("@alias") && parts.size() >= 3)
			{
				searches.push_back("{" + parts[1] + "}");
				replaces.push_back(parts[2]);
			}
			// @uniform <name> <index> <part> (part is x, xy, zw, etc.)
			if (!parts[0].compare("@uniform") && parts.size() >= 4)
			{
				std::string uniformName = parts[1];
				int index = String::ToInt(parts[2]);
				int offset, size;
				GetUniformOffsetSize(parts[3], &offset, &size);
				if (index > locals.size() - 1)
				{
					locals.resize(index + 1);
				}
				uniforms.push_back({ index, offset, size });
				uniformIndexes.insert(std::make_pair(uniformName, locals.size() - 1));
				searches.push_back("{" + uniformName + "}");
				replaces.push_back(GetUniformName(index, offset, size));
			}
			output << std::endl;
			continue;
		}
		for (unsigned int i=0; i<searches.size(); ++i)
		{
			size_t pos;
			pos = line.find(searches[i]);
			while(pos != std::string::npos)
			{
				line.replace(pos, searches[i].size(), replaces[i]);
				pos -= searches[i].size();
				pos += replaces[i].size();
				pos = line.find(searches[i], pos);
			}
		}
		output << line << std::endl;
	}
}

void ArbShader::Parse(std::stringstream &code, std::string name)
{
	if (isLoaded)
	{
		glDeleteProgramsARB(1, (unsigned int*)&program);
	}
	this->filename = name;
	isLoaded = false;

	std::string codeCopy = code.str();

	glGenProgramsARB(1, (unsigned int*)&program);
	glBindProgramARB(GL_FRAGMENT_PROGRAM_ARB, program);
	glProgramStringARB(GL_FRAGMENT_PROGRAM_ARB, GL_PROGRAM_FORMAT_ASCII_ARB,
			codeCopy.length(), codeCopy.c_str());

	int errorPos;
	glGetIntegerv(GL_PROGRAM_ERROR_POSITION_ARB, &errorPos);
	if (errorPos >= 0)
	{
		char *msg = (char *)glGetString(GL_PROGRAM_ERROR_STRING_ARB);
		throw std::runtime_error("Error in ARB program " + name + ": " + msg);
		return;
	}
	isLoaded = true;
}

bool ArbShader::SetNative(std::stringstream &input, std::string name)
{
	isLoaded = false;
	std::stringstream output;
	ProcessFile(name, input, output);
	Parse(output, name);
	return isLoaded;
}

//-----------------------------------------------------------------------------
// Use
//-----------------------------------------------------------------------------

void ArbShader::Bind()
{
	glEnable(GL_FRAGMENT_PROGRAM_ARB);
	glBindProgramARB(GL_FRAGMENT_PROGRAM_ARB, program);
}

void ArbShader::Unbind()
{
	glDisable(GL_FRAGMENT_PROGRAM_ARB);
}

//-----------------------------------------------------------------------------
// Set parameters
//-----------------------------------------------------------------------------

int ArbShader::GetUniformIndex(std::string name)
{
	auto it = uniformIndexes.find(name);
	if (it == uniformIndexes.end())
	{
		throw std::runtime_error("Could not find uniform named " + name);
	}
	return it->second;
}

void ArbShader::SetUniform4f(int index, float x, float y, float z, float w)
{
	CheckUniformIndex(index);
	WriteUniformValue(uniforms[index], 0, x);
	WriteUniformValue(uniforms[index], 1, y);
	WriteUniformValue(uniforms[index], 2, z);
	WriteUniformValue(uniforms[index], 3, w);
	UpdateLocals();
}

void ArbShader::SetUniform3f(int index, float x, float y, float z)
{
	CheckUniformIndex(index);
	WriteUniformValue(uniforms[index], 0, x);
	WriteUniformValue(uniforms[index], 1, y);
	WriteUniformValue(uniforms[index], 2, z);
	UpdateLocals();
}

void ArbShader::SetUniform2f(int index, float x, float y)
{
	CheckUniformIndex(index);
	WriteUniformValue(uniforms[index], 0, x);
	WriteUniformValue(uniforms[index], 1, y);
	UpdateLocals();
}

void ArbShader::SetUniform1f(int index, float x)
{
	CheckUniformIndex(index);
	WriteUniformValue(uniforms[index], 0, x);
	UpdateLocals();
}

void ArbShader::CheckUniformIndex(int index)
{
	// Just throws if the index is out of range, so that returning guarantees the index is valid.
	if (index < 0 && index >= uniforms.size())
	{
		throw std::logic_error("index out of range");
	}
}

inline void ArbShader::WriteUniformValue(Uniform &uniform, int index, float value)
{
	if (index > uniform.size)
	{
		return;
	}
	int i = uniform.offset + index;
	if (i >= 4)
	{
		return;
	}
	locals[uniform.local].v[i] = value;
	locals[uniform.local].marked = true;
}

inline void ArbShader::UpdateLocals()
{
	glBindProgramARB(GL_FRAGMENT_PROGRAM_ARB, program);
	for (int i = 0; i < locals.size(); i += 1)
	{
		glProgramLocalParameter4fARB(GL_FRAGMENT_PROGRAM_ARB, i, locals[i].v[0], locals[i].v[1], locals[i].v[2], locals[i].v[3]);
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
