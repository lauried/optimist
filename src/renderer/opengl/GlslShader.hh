/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <string>
#include <sstream>
#include "log/ILogTarget.hh"
#include "renderer/IShader.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class GlslShader : public IShader {
public:
	GlslShader();
	~GlslShader();

	virtual const std::string & GetNativeType();
	virtual bool SetNative(std::stringstream &input, std::string name);
	virtual void Bind();
	virtual void Unbind();
	virtual int GetUniformIndex(std::string name);
	virtual void SetUniform1f(int location, float v0);
	virtual void SetUniform2f(int location, float v0, float v1);
	virtual void SetUniform3f(int location, float v0, float v1, float v2);
	virtual void SetUniform4f(int location, float v0, float v1, float v2, float v3);

	// Extra ones for ints - dunno whether to use or now.
	void SetUniform1i(int location, int v0);
	void SetUniform2i(int location, int v0, int v1);
	void SetUniform3i(int location, int v0, int v1, int v2);
	void SetUniform4i(int location, int v0, int v1, int v2, int v3);

private:

	bool isLoaded;
	std::string filename;
	int program;
	int vertex_shader;
	int fragment_shader;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

