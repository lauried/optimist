/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#ifdef _WIN32
//## add_libraries opengl32
#else
//## add_libraries GL
#endif

#include "renderer/opengl/OpenGLRenderCore.hh"
#include <iostream>
#include <exception>
#include <stdexcept>
#include <sstream>
#ifdef _WIN32
// Fixes compile errors in gl.h
#include <windows.h>
#endif
#include "renderer/opengl/GlslShader.hh"
#include "renderer/opengl/ArbShader.hh"
#include "library/extern/GLee.h"
#include "library/maths/Maths.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

#define DEBUG_PRINT 0
#define ERROR_CHECK 0

static void GlThrowError(const char *file, int line)
{
	int e = glGetError();
	if (e == GL_NO_ERROR)
		return;

	std::ostringstream ss;
	switch (e)
	{
	case GL_INVALID_ENUM:      ss << "GL_INVALID_ENUM";      break;
	case GL_INVALID_VALUE:     ss << "GL_INVALID_VALUE";     break;
	case GL_INVALID_OPERATION: ss << "GL_INVALID_OPERATION"; break;
	case GL_STACK_UNDERFLOW:   ss << "GL_STACK_UNDERFLOW";   break;
	case GL_STACK_OVERFLOW:    ss << "GL_STACK_OVERFLOW";    break;
	case GL_OUT_OF_MEMORY:     ss << "GL_OUT_OF_MEMORY";     break;
	case GL_TABLE_TOO_LARGE:   ss << "GL_TABLE_TOO_LARGE";   break;
	default: ss << "unknown";
	}

	ss <<  " @" << line;

	std::string error = ss.str();
	throw std::logic_error(error);
}

bool gDebugPrint = true;

#if DEBUG_PRINT
#define DPX_AS_STRING(s) #s
#define DPX(s) { s; if (gDebugPrint) { std::cout << DPX_AS_STRING(s) << std::endl; } }
#define DPRINT(s) { if (gDebugPrint) { std::cout << s << std::endl; } }
#else
#define DPX(s) { s; }
#define DPRINT(s)
#endif

#if ERROR_CHECK
#define DEC GlThrowError(__FILE__, __LINE__);
#else
#define DEC
#endif

OpenGLRenderCore::OpenGLRenderCore()
{
	hasViewport = false;
	hasProjection = false;
	inFrame = false;

	meshBufferSize = 1024;
	meshBuffer = new float[meshBufferSize];
}

OpenGLRenderCore::~OpenGLRenderCore()
{
	delete[] meshBuffer;
	for (auto it = textures.begin(); it != textures.end(); ++it)
	{
		delete *it;
	}
}

//-----------------------------------------------------------------------------
// Matrix Stuff
//-----------------------------------------------------------------------------

#define MATRIX_DOUBLE 0
#if MATRIX_DOUBLE
typedef double MatrixType;
#else
typedef float MatrixType;
#endif

struct Matrix {
	MatrixType m[16];

	MatrixType &M(int row, int col)
	{
		return m[col * 4 + row];
	}

	MatrixType &M(int ofs)
	{
		return m[ofs];
	}

	void Load()
	{
#if MATRIX_DOUBLE
		DPX(glLoadMatrixd(m)) DEC
#else
		DPX(glLoadMatrixf(m)) DEC
#endif
	}
};

static void BuildNearFarProjection(float left, float top, float right, float bottom, float nearDist, float farDist, Matrix *projection)
{
	float near2 = nearDist * 2.0;
	float a = (right + left) / (right - left);
	float b = (top + bottom) / (top - bottom);
	float c = (0 - (farDist + nearDist)) / (farDist - nearDist);
	float d = (-2 * farDist * nearDist) / (farDist - nearDist);

	// See http://www.opengl.org/sdk/docs/man/xhtml/glFrustum.xml

	// dot with vector for new x
	projection->M(0, 0) =  near2 / (right - left);
	projection->M(0, 1) =  0;
	projection->M(0, 2) =  a;
	projection->M(0, 3) =  0;
	// dot with vector for new y
	projection->M(1, 0) =  0;
	projection->M(1, 1) =  near2 / (top - bottom);
	projection->M(1, 2) =  b;
	projection->M(1, 3) =  0;
	// dot with vector for new z
	projection->M(2, 0) =  0;
	projection->M(2, 1) =  0;
	projection->M(2, 2) =  c;
	projection->M(2, 3) =  d;
	// dot with vector for new w
	projection->M(3, 0) =  0;
	projection->M(3, 1) =  0;
	projection->M(3, 2) = -1;
	projection->M(3, 3) =  0;
}

static void BuildNearFarProjection(float fovX, float fovY, float nearDist, float farDist, Matrix *projection)
{
	float c = (-(farDist + nearDist)) / (farDist - nearDist);
	float d = (-2 * farDist * nearDist) / (farDist - nearDist);

	projection->M(0, 0) =  nearDist / fovX;
	projection->M(0, 1) =  0;
	projection->M(0, 2) =  0;
	projection->M(0, 3) =  0;

	projection->M(1, 0) =  0;
	projection->M(1, 1) =  nearDist / fovY;
	projection->M(1, 2) =  0;
	projection->M(1, 3) =  0;

	projection->M(2, 0) =  0;
	projection->M(2, 1) =  0;
	projection->M(2, 2) =  c;
	projection->M(2, 3) =  d;

	projection->M(3, 0) =  0;
	projection->M(3, 1) =  0;
	projection->M(3, 2) = -1;
	projection->M(3, 3) =  0;
}

static void BuildInfiniteProjection(float fovX, float fovY, float nearDist, Matrix *projection)
{
	float nudge = 1.0 - (1.0 / (1 << 23));

	projection->M(0, 0) =  nearDist / fovX;
	projection->M(0, 1) =  0;
	projection->M(0, 2) =  0;
	projection->M(0, 3) =  0;

	projection->M(1, 0) =  0;
	projection->M(1, 1) =  nearDist / fovY;
	projection->M(1, 2) =  0;
	projection->M(1, 3) =  0;

	projection->M(2, 0) =  0;
	projection->M(2, 1) =  0;
	projection->M(2, 2) = -nudge;
	projection->M(2, 3) = -2 * nearDist * nudge;

	projection->M(3, 0) =  0;
	projection->M(3, 1) =  0;
	projection->M(3, 2) = -1;
	projection->M(3, 3) =  0;
}

static void BuildOrthographicProjection(float left, float top, float right, float bottom, float nearDist, float farDist, Matrix *projection)
{
	projection->M(0, 0) = 2 / (right - left);
	projection->M(0, 1) = 0;
	projection->M(0, 2) = 0;
	projection->M(0, 3) = - (right + left) / (right - left);

	projection->M(1, 0) = 0;
	projection->M(1, 1) = 2 / (top - bottom);
	projection->M(1, 2) = 0;
	projection->M(1, 3) = - (top + bottom) / (top - bottom);

	projection->M(2, 0) = 0;
	projection->M(2, 1) = 0;
	projection->M(2, 2) = -2 / (farDist - nearDist);
	projection->M(2, 3) = - (farDist + nearDist) / (farDist - nearDist);

	projection->M(3, 0) = 0;
	projection->M(3, 1) = 0;
	projection->M(3, 2) = 0;
	projection->M(3, 3) = 1;
}

// This takes a camera orientation.
static void BuildCameraMatrix(const OrientationF &ori, Matrix *modelview)
{
	// Same pattern, we dotproduct with the eye vectors to get the point in
	// eye space.

	Vector3F v = ori.DirectionToInternalSpace(ori.origin);
	// OpenGL X axis runs right across the screen
	modelview->M(0, 0) =  ori.v[OrientationF::Right][0];
	modelview->M(0, 1) =  ori.v[OrientationF::Right][1];
	modelview->M(0, 2) =  ori.v[OrientationF::Right][2];
	modelview->M(0, 3) = -v[OrientationF::Right];
	// OpenGL Y axis goes up the screen
	modelview->M(1, 0) =  ori.v[OrientationF::Up][0];
	modelview->M(1, 1) =  ori.v[OrientationF::Up][1];
	modelview->M(1, 2) =  ori.v[OrientationF::Up][2];
	modelview->M(1, 3) = -v[OrientationF::Up];
	// OpenGL Z axis comes out of the screen - into the scene is negative z
	modelview->M(2, 0) = -ori.v[OrientationF::Forward][0];
	modelview->M(2, 1) = -ori.v[OrientationF::Forward][1];
	modelview->M(2, 2) = -ori.v[OrientationF::Forward][2];
	modelview->M(2, 3) =  v[OrientationF::Forward];

	modelview->M(3, 0) =  0;
	modelview->M(3, 1) =  0;
	modelview->M(3, 2) =  0;
	modelview->M(3, 3) =  1;
}

//-----------------------------------------------------------------------------
// IRenderCore
//-----------------------------------------------------------------------------

void OpenGLRenderCore::StartFrame()
{
	DPRINT("+StartFrame")

	if (inFrame)
	{
		throw std::logic_error("OpenGLRenderCore::StartFrame called before last frame's EndFrame");
	}

	DPX(glEnable(GL_DEPTH_TEST)) DEC
	DPX(glDepthFunc(GL_LEQUAL)) DEC

	DPX(glFrontFace(GL_CCW)) DEC
	DPX(glEnable(GL_CULL_FACE)) DEC
	DPX(glCullFace(GL_BACK)) DEC

#if 1
	DPX(glClearColor(0, 0, 0, 0)) DEC
#else
	DPX(glClearColor(0.66f, 0.22f, 0.44f, 1.0f)) DEC
#endif

	for (auto it = textures.begin(); it != textures.end(); ++it)
	{
		if ((*it)->requiresUpload)
		{
			(*it)->requiresUpload = false;
			(*it)->Upload();
		}
	}

	inFrame = true;
	hasProjection = false;

	DPRINT("-StartFrame")
}

void OpenGLRenderCore::EndFrame()
{
	DPRINT("+EndFrame")

	// TODO: This is intended for batching, when we batch meshes as drawn and
	// render the whole buffer at once. This would tell us that all meshes are
	// drawn for the frame so flush the last of the buffer.
	// Any other operation that required it would automatically flush the
	// mesh buffers, so we only need to also catch the end of a frame.

	if (!inFrame)
	{
		throw std::logic_error("OpenGLRenderCore::EndFrame called without corresponding StartFrame");
	}

	inFrame = false;
	hasProjection = false;

	DPRINT("-EndFrame")
}

void OpenGLRenderCore::SetViewport(const Vector2I &topLeft, const Vector2I &bottomRight)
{
	// TODO: What coordinates does glViewport require? Can this code be checked for correctness?
	Vector2I bottomLeft(topLeft[0], bottomRight[1]);
	viewportBottomLeft.Set(topLeft[0], screenSize[1] - bottomLeft[1]);
	viewportSize = bottomRight - topLeft;
	DPX(glViewport(
		viewportBottomLeft[0], viewportBottomLeft[1],
		viewportSize[0], viewportSize[1]
		)) DEC
	hasViewport = true;
}

void OpenGLRenderCore::GetViewport(Vector2I *topLeft, Vector2I *bottomRight)
{
	Vector2I tmpTopLeft, tmpBottomRight;

	tmpTopLeft[0] = viewportBottomLeft[0];
	tmpBottomRight[0] = tmpTopLeft[0] + screenSize[0];
	tmpBottomRight[1] = screenSize[1] - viewportBottomLeft[1];
	tmpTopLeft[1] - tmpBottomRight[1] - screenSize[1];

	topLeft->Set(tmpTopLeft);
	bottomRight->Set(tmpBottomRight);
}

void OpenGLRenderCore::SetOrthographicProjection(float left, float top, float right, float bottom, float nearPlane, float farPlane)
{
	DPRINT("+SetOrthographicProjection")

	if (!hasViewport)
	{
		throw std::logic_error("OpenGLRenderCore::SetProjection called with no viewport set up");
	}

	DPX(glMatrixMode(GL_PROJECTION)) DEC
	Matrix matrix;
	BuildOrthographicProjection(left, top, right, bottom, nearPlane, farPlane, &matrix);
	matrix.Load();

	hasProjection = true;
	DPX(glMatrixMode(GL_MODELVIEW)) DEC
	DPX(glLoadIdentity()) DEC

	DPRINT("-SetOrthographicProjection")
}

void OpenGLRenderCore::SetPerspectiveProjection(float left, float top, float right, float bottom, float nearPlane, float farPlane)
{
	DPRINT("+SetPerspectiveProjection")

	if (!hasViewport)
	{
		throw std::logic_error("OpenGLRenderCore::SetProjection called with no viewport set up");
	}

	Matrix matrix;
	left *= nearPlane;
	right *= nearPlane;
	top *= nearPlane;
	bottom *= nearPlane;
	BuildNearFarProjection(left, top, right, bottom, nearPlane, farPlane, &matrix);
	DPX(glMatrixMode(GL_PROJECTION)) DEC
	matrix.Load();

	hasProjection = true;
	DPX(glMatrixMode(GL_MODELVIEW)) DEC

	DPRINT("-SetPerspectiveProjection")
}

void OpenGLRenderCore::SetView(const OrientationF &view)
{
	DPRINT("+SetView")
	viewMatrix = view;
	SetUpModelView();
	DPRINT("-SetView")
}

void OpenGLRenderCore::SetModel(const OrientationF &model)
{
	DPRINT("+SetModel")
	modelMatrix = model;
	SetUpModelView();
	DPRINT("-SetModel")
}

void OpenGLRenderCore::SetUpModelView()
{
	OrientationF modelView = viewMatrix;
	modelView.ToInternalSpaceOf(modelMatrix);

	Matrix matrix;
	BuildCameraMatrix(modelView, &matrix);
	matrix.Load();
}

const OrientationF& OpenGLRenderCore::GetModelview()
{
	return currentModelview;
}

void OpenGLRenderCore::ClearBuffer(int buffer)
{
	if (!hasViewport)
	{
		throw std::logic_error("OpenGLRenderCore::ClearBuffer called with no viewport set up");
	}

	GLint flags = 0;
	if (buffer & IRenderCore::ColourBuffer)
		flags |= GL_COLOR_BUFFER_BIT;
	if (buffer & IRenderCore::DepthBuffer)
		flags |= GL_DEPTH_BUFFER_BIT;
	DPX(glClear(flags)) DEC
}

void OpenGLRenderCore::SetColourEnabled(bool enable)
{
	GLboolean flag = enable ? GL_TRUE : GL_FALSE;
	DPX(glColorMask(flag, flag, flag, flag)) DEC
}

void OpenGLRenderCore::SetDepthEnabled(bool enable)
{
	GLboolean flag = enable ? GL_TRUE : GL_FALSE;
	DPX(glDepthMask(flag)) DEC
}

void OpenGLRenderCore::SetBlend(IRenderCore::Blend blend)
{
	switch (blend)
	{
	case IRenderCore::BlendAlpha:
		DPX(glEnable(GL_BLEND)) DEC
		DPX(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)) DEC
		break;
	case IRenderCore::BlendAdditive:
		DPX(glEnable(GL_BLEND)) DEC
		DPX(glBlendFunc(GL_SRC_ALPHA, GL_ONE)) DEC
		break;
	default:
		DPX(glDisable(GL_BLEND)) DEC
		break;
	}
}

ITexture *OpenGLRenderCore::CreateTexture()
{
	TextureGl *tex = new TextureGl();
	textures.insert(tex);
	return tex;
}

void OpenGLRenderCore::FreeTexture(ITexture *texture)
{
	TextureGl *t = dynamic_cast<TextureGl*>(texture);
	if (t == 0) return;

	auto it = textures.find(t);
	if (it != textures.end())
	{
		textures.erase(it);
		delete t;
	}
}

IShader *OpenGLRenderCore::CreateShader(std::string desiredType)
{
    bool glslAvailable = (GLEE_VERSION_2_0 || (GLEE_ARB_vertex_shader && GLEE_ARB_fragment_shader && GLEE_ARB_shader_objects));
    bool arbAvailable = (GLEE_ARB_fragment_program);

	if (arbAvailable && (!desiredType.compare("arb") || !glslAvailable))
	{
		return new ArbShader();
	}
    if (glslAvailable)
    {
		return new GlslShader();
    }
    return nullptr;
}

void OpenGLRenderCore::FreeShader(IShader *shader)
{
	delete shader;
}

void OpenGLRenderCore::ViewportToTexture(ITexture *texture, Vector2F *topLeft, Vector2F *bottomRight)
{
	if (!hasViewport)
	{
		throw std::logic_error("OpenGLRenderCore::ViewportToTexture called with no viewport set up");
	}

	TextureGl *t = dynamic_cast<TextureGl*>(texture);
	if (t == 0) return;

	Vector2I imageSize(1, 1);
	while (imageSize[0] <= viewportSize[0])
	{ imageSize[0] *= 2; }
	while (imageSize[1] <= viewportSize[1])
	{ imageSize[1] *= 2; }

	DPX(glBindTexture(GL_TEXTURE_2D, t->glTextureNum)) DEC
	DPX(glCopyTexImage2D(
		GL_TEXTURE_2D,
		0, // level zero
		GL_RGBA,
		viewportBottomLeft[0], viewportBottomLeft[1],
		imageSize[0], imageSize[1],
		0 // zero border
		)) DEC

	(*topLeft)[0] = 0.0f; // left
	(*bottomRight)[1] = 0.0f; // bottom
	(*bottomRight)[0] = (float)viewportSize[0] / (float)imageSize[0]; // right
	(*topLeft)[1] = (float)viewportSize[1] / (float)imageSize[1]; // top
}

void OpenGLRenderCore::SetMaterial(const Material &material)
{
	MaterialDescriptor::FilterType filterType = MaterialDescriptor::FilterNearest;
	bool mipmap = false;
	Color color;

	if (material.descriptor != nullptr)
	{
		filterType = material.descriptor->texture.filter;
		color = material.descriptor->color;
		mipmap = material.descriptor->texture.mipmap;
	}

	DPX(glColor4fv(color.rgba)) DEC

	TextureGl *t = dynamic_cast<TextureGl*>(material.texture);
	if (t == nullptr)
	{
		DPX(glDisable(GL_TEXTURE_2D)) DEC
	}
	else
	{
		DPX(glEnable(GL_TEXTURE_2D)) DEC
		if (t->requiresUpload)
		{
			t->Upload();
		}
		t->SetMipmap(mipmap);
		t->SetFilter(filterType);
		t->Bind();
	}
}

static void AssertRange(int value, int min, int max)
{
	if (value < min || value > max)
	{
		throw std::logic_error("OpenGLRenderCore::DrawMesh: invalid number of mesh attribute components");
	}
}

static int GlMeshType(const MeshData::MeshType type)
{
	switch (type)
	{
		case MeshData::TRIANGLE:       return GL_TRIANGLES;
		case MeshData::TRIANGLE_FAN:   return GL_TRIANGLE_FAN;
		case MeshData::TRIANGLE_STRIP: return GL_TRIANGLE_STRIP;
		case MeshData::QUAD:           return GL_QUADS;
		case MeshData::QUAD_STRIP:     return GL_QUAD_STRIP;
		case MeshData::LINE:           return GL_LINES;
		case MeshData::LINE_STRIP:     return GL_LINE_STRIP;
		case MeshData::LINE_LOOP:      return GL_LINE_LOOP;
		case MeshData::POINT:          return GL_POINTS;
		default: throw std::logic_error("OpenGLRenderCore::DrawMesh given mesh with invalid channel type");
	}
}

static int GlDataType(const MeshData::DataType type)
{
	switch (type)
	{
		case MeshData::FLOAT:  return GL_FLOAT;
		case MeshData::DOUBLE: return GL_DOUBLE;
		case MeshData::INT:    return GL_INT;
		case MeshData::UINT:   return GL_UNSIGNED_INT;
		case MeshData::SHORT:  return GL_SHORT;
		case MeshData::USHORT: return GL_UNSIGNED_SHORT;
		case MeshData::CHAR:   return GL_BYTE;
		case MeshData::UCHAR:  return GL_UNSIGNED_BYTE;
		default: throw std::logic_error("OpenGLRenderCore::DrawMesh given mesh channel with invalid data type");
	}
}

void OpenGLRenderCore::DrawMesh(const MeshData &mesh)
{
	if (!hasProjection)
	{
		throw std::logic_error("OpenGLRenderCore::DrawMesh called with no projection set up");
	}
	// TODO: Add method to MeshData to validate it.

	int meshType = GlMeshType(mesh.type);

	if (mesh.verts.data.d != nullptr)
	{
		DPX(glEnableClientState(GL_VERTEX_ARRAY)) DEC
		DPX(glVertexPointer(
			mesh.verts.components,
			GlDataType(mesh.verts.type),
            mesh.verts.step,
            mesh.verts.data.d
		)) DEC
	}

	if (mesh.normals.data.d != nullptr)
	{
		DPX(glEnableClientState(GL_NORMAL_ARRAY)) DEC
		DPX(glNormalPointer(
			GlDataType(mesh.normals.type),
			mesh.normals.step,
			mesh.normals.data.d
		)) DEC
	}

	if (mesh.colours.data.d != nullptr)
	{
		DPX(glEnableClientState(GL_COLOR_ARRAY)) DEC
		DPX(glColorPointer(
			mesh.colours.components,
			GlDataType(mesh.colours.type),
			mesh.colours.step,
			mesh.colours.data.d
		)) DEC
	}

	if (mesh.texcoords.data.d != nullptr)
	{
		DPX(glEnableClientState(GL_TEXTURE_COORD_ARRAY)) DEC
		DPX(glTexCoordPointer(
			mesh.texcoords.components,
			GlDataType(mesh.texcoords.type),
			mesh.texcoords.step,
			mesh.texcoords.data.d
		)) DEC
	}

	// TODO: Handle index array.

	DPX(glDrawArrays(meshType, 0, mesh.numVerts)) DEC

	if (mesh.verts.data.d != nullptr)
	{
		DPX(glDisableClientState(GL_VERTEX_ARRAY)) DEC
	}
	if (mesh.normals.data.d != nullptr)
	{
		DPX(glDisableClientState(GL_NORMAL_ARRAY)) DEC
	}
	if (mesh.colours.data.d != nullptr)
	{
		DPX(glDisableClientState(GL_COLOR_ARRAY)) DEC
	}
	if (mesh.texcoords.data.d != nullptr)
	{
		DPX(glDisableClientState(GL_TEXTURE_COORD_ARRAY)) DEC
	}
}

void OpenGLRenderCore::DrawTest(int test)
{
	if (!hasProjection)
	{
		throw std::logic_error("OpenGLRenderCore::DrawTest called with no projection set up");
	}

	if (test == 1)
	{
		float scale = 1024.0f;
		float push = scale / 256.0f;

		Vector3F colour, vertex;
		for (int axis = 0; axis < 3; ++axis)
		{
			int x = (axis + 1) % 3;
			int y = (axis + 2) % 3;

			colour.Set(0, 0, 0);
			colour[axis] = 1;
			DPX(glColor3fv(colour.v)) DEC

			DPX(glBegin(GL_LINE_LOOP))
			vertex[x] =  scale; vertex[y] =  scale; vertex[axis] =  scale + push; DPX(glVertex3fv(vertex.v))
			vertex[x] = -scale; vertex[y] =  scale; vertex[axis] =  scale + push; DPX(glVertex3fv(vertex.v))
			vertex[x] = -scale; vertex[y] = -scale; vertex[axis] =  scale + push; DPX(glVertex3fv(vertex.v))
			vertex[x] =  scale; vertex[y] = -scale; vertex[axis] =  scale + push; DPX(glVertex3fv(vertex.v))
			DPX(glEnd()) DEC

			colour.Set(1, 1, 1);
			colour[axis] = 0;
			DPX(glColor3fv(colour.v)) DEC

			DPX(glBegin(GL_LINE_LOOP))
			vertex[x] =  scale; vertex[y] =  scale; vertex[axis] = -scale - push; DPX(glVertex3fv(vertex.v))
			vertex[x] = -scale; vertex[y] =  scale; vertex[axis] = -scale - push; DPX(glVertex3fv(vertex.v))
			vertex[x] = -scale; vertex[y] = -scale; vertex[axis] = -scale - push; DPX(glVertex3fv(vertex.v))
			vertex[x] =  scale; vertex[y] = -scale; vertex[axis] = -scale - push; DPX(glVertex3fv(vertex.v))
			DPX(glEnd()) DEC
		}
		DPX(glColor3f(1, 1, 1)) DEC
	}
	else if (test == 2)
	{
		float scale = 1024.0f;
		float divisions = 32.0f;

		DPX(glBegin(GL_LINES))
		float step = scale / divisions;
		for (int xy = -divisions; xy <= divisions; ++xy)
		{
			DPX(glVertex3f(step * (float)xy, -scale, 0))
			DPX(glVertex3f(step * (float)xy,  scale, 0))

			DPX(glVertex3f(-scale, step * (float)xy, 0))
			DPX(glVertex3f( scale, step * (float)xy, 0))
		}
		DPX(glEnd()) DEC
	}
	else
	{
		float scale = 0.9;
		DPX(glMatrixMode(GL_PROJECTION)) DEC
		DPX(glLoadIdentity()) DEC
		DPX(glMatrixMode(GL_MODELVIEW)) DEC
		DPX(glLoadIdentity()) DEC
		DPX(glDisable(GL_DEPTH_TEST)) DEC
		DPX(glDisable(GL_BLEND)) DEC
		DPX(glBegin(GL_LINES))
		DPX(glColor3f(1, 1, 1)) DPX(glVertex3f(0, 0, 0)) DPX(glVertex3f( scale,  scale,  scale))
		DPX(glColor3f(1, 0, 0)) DPX(glVertex3f(0, 0, 0)) DPX(glVertex3f( scale,      0,      0))
		DPX(glColor3f(0, 1, 0)) DPX(glVertex3f(0, 0, 0)) DPX(glVertex3f(     0,  scale,      0))
		DPX(glColor3f(0, 0, 1)) DPX(glVertex3f(0, 0, 0)) DPX(glVertex3f(     0,      0,  scale))
		DPX(glColor3f(0, 1, 1)) DPX(glVertex3f(0, 0, 0)) DPX(glVertex3f(-scale,      0,      0))
		DPX(glColor3f(1, 0, 1)) DPX(glVertex3f(0, 0, 0)) DPX(glVertex3f(     0, -scale,      0))
		DPX(glColor3f(1, 1, 0)) DPX(glVertex3f(0, 0, 0)) DPX(glVertex3f(     0,      0, -scale))
		DPX(glEnd()) DEC
	}
}

//-----------------------------------------------------------------------------
// TextureGL
//-----------------------------------------------------------------------------

void OpenGLRenderCore::TextureGl::SetImage(Image *img, ITexture::SizingHint sizing)
{
	image = img;
	requiresUpload = true;
	(*this).sizing = sizing;
}

void OpenGLRenderCore::TextureGl::SetUploadCallback(std::function<void(Image*, Image*)> uploadCallback)
{
	(*this).uploadCallback = uploadCallback;
}

void OpenGLRenderCore::TextureGl::RequireUpload()
{
	requiresUpload = true;
}

bool OpenGLRenderCore::TextureGl::Upload()
{
	if (image == 0)
	{
		return false;
	}

	requiresUpload = false;

	Image resizeImage;
	if (uploadCallback != nullptr)
	{
		uploadCallback(image, &resizeImage);
	}
	else
	{
		resizeImage.Copy(*image);
	}

	if (!Maths::IsPowerOfTwo(resizeImage.Width()) || !Maths::IsPowerOfTwo(resizeImage.Height()))
	{
		switch (sizing)
		{
		case ITexture::SH_Border:
			{
				int w, h;
				for (w = 1; w < resizeImage.Width(); w <<= 1);
				for (h = 1; h < resizeImage.Width(); h <<= 1);
				resizeImage.Pad(w, h);
			}
			break;
		case ITexture::SH_Resample:
			throw std::logic_error("OpenGLRenderCore::TextureGl::Upload - Resampling not implemented");
			break;
		default:
			throw std::logic_error("OpenGLRenderCore::TextureGl::Upload - Invalid value for this.sizing");
		}
	}

	if (!glTextureNum)
	{
		DPX(glGenTextures(1, &glTextureNum)) DEC
	}

	int format;
	switch (resizeImage.Bpp())
	{
	case 1: format = GL_LUMINANCE; break;
	case 2: format = GL_LUMINANCE_ALPHA; break;
	case 3: format = GL_RGB; break;
	case 4: format = GL_RGBA; break;
	default: throw std::logic_error("OpenGLRenderCore::TextureGl::Upload Invalid image bpp");
	}

	mipmap = true;
	filter = MaterialDescriptor::FilterLinear;
	updateTexParams = true;

	DPX(glBindTexture(GL_TEXTURE_2D, glTextureNum)) DEC
	DPX(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)) DEC
	DPX(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR)) DEC
	DPX(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)) DEC
	DPX(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)) DEC
	DPX(glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE)) DEC

	int level = 0;
	while(true)
	{
		int width = resizeImage.Width();
		int height = resizeImage.Height();
		uint8_t *data = resizeImage.Data();

		DPX(glTexImage2D(GL_TEXTURE_2D, level, format, width, height, 0, format, GL_UNSIGNED_BYTE, data)) DEC

		if ((width == 1) && (height == 1))
			break;

		resizeImage.DownsampleInPlace();
		level += 1;
	}

	return true;
}

void OpenGLRenderCore::TextureGl::Bind()
{
	DPX(glBindTexture(GL_TEXTURE_2D, glTextureNum)) DEC
	if (updateTexParams)
	{
		int min;
		switch (filter)
		{
		case MaterialDescriptor::FilterLinear:
			min = mipmap ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR;
			DPX(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)) DEC
			DPX(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min)) DEC
			break;
		case MaterialDescriptor::FilterNearest:
			min = mipmap ? GL_NEAREST_MIPMAP_LINEAR : GL_NEAREST;
			DPX(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)) DEC
			DPX(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min)) DEC
			break;
		}
		updateTexParams = false;
	}
}

void OpenGLRenderCore::TextureGl::SetMipmap(bool mipmap)
{
	if (this->mipmap != mipmap)
	{
		this->mipmap = mipmap;
		updateTexParams = true;
	}
}

void OpenGLRenderCore::TextureGl::SetFilter(MaterialDescriptor::FilterType filter)
{
	if (this->filter != filter)
	{
		this->filter = filter;
		updateTexParams = true;
	}
}

//-----------------------------------------------------------------------------
// Own methods
//-----------------------------------------------------------------------------

void OpenGLRenderCore::GetExtensions(std::vector<std::string> *extensions)
{
	GLint numExtensions = 0;
	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);

	for (int i = 0; i < numExtensions; i += 1)
	{
		extensions->push_back(std::string((const char *)glGetStringi(GL_EXTENSIONS, i)));
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
