/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "renderer/opengl/GlslShader.hh"
#include <cstdio>
#include <iostream>
#include <stdexcept>
#ifdef _WIN32
// Fixes compile errors in gl.h
#include <windows.h>
#endif
#include "library/extern/GLee.h"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

static void GlThrowError(const char *file, int line)
{
	int e = glGetError();
	if (e == GL_NO_ERROR)
		return;

	std::ostringstream ss;
	switch (e)
	{
	case GL_INVALID_ENUM:      ss << "GL_INVALID_ENUM";      break;
	case GL_INVALID_VALUE:     ss << "GL_INVALID_VALUE";     break;
	case GL_INVALID_OPERATION: ss << "GL_INVALID_OPERATION"; break;
	case GL_STACK_UNDERFLOW:   ss << "GL_STACK_UNDERFLOW";   break;
	case GL_STACK_OVERFLOW:    ss << "GL_STACK_OVERFLOW";    break;
	case GL_OUT_OF_MEMORY:     ss << "GL_OUT_OF_MEMORY";     break;
	case GL_TABLE_TOO_LARGE:   ss << "GL_TABLE_TOO_LARGE";   break;
	default: ss << "unknown";
	}

	ss <<  " @" << line;

	std::string error = ss.str();
	throw std::logic_error(error);
}

#define ERROR_CHECK 0

#if ERROR_CHECK
#define DEC GlThrowError(__FILE__, __LINE__);
#else
#define DEC
#endif

GlslShader::GlslShader()
{
	isLoaded = false;
}

GlslShader::~GlslShader()
{
	if (isLoaded)
	{
		glDeleteShader(vertex_shader);
		glDeleteShader(fragment_shader);
		glDeleteProgram(program);
	}
}

const std::string & GlslShader::GetNativeType()
{
	static std::string type("glsl");
	return type;
}

void GlslShader::Bind()
{
	if (isLoaded)
	{
		glUseProgram(program);
	}
}

void GlslShader::Unbind()
{
	glUseProgram(0);
}

//-----------------------------------------------------------------------------
// ShaderProgram - Load
//-----------------------------------------------------------------------------

const int cMessageBufferSize = 1024;

static bool LoadFileIntoString(std::string filename, std::string *dest)
{
	FILE* f = fopen(filename.c_str(), "r");
	if (f == NULL)
	{
		return false;
	}

	fseek(f, 0, SEEK_END);
	size_t fileSize = ftell(f);
	char data[fileSize + 1];
	data[fileSize] = '\0';

	fseek(f, 0, SEEK_SET);
	if (fread(data, 1, fileSize, f) != fileSize)
	{
		fclose(f);
		return false;
	}

	(*dest) = data;
	fclose(f);
	return true;
}

bool GlslShader::SetNative(std::stringstream &input, std::string name)
{
	if (isLoaded)
	{
		glDeleteShader(vertex_shader);
		glDeleteShader(fragment_shader);
		glDeleteProgram(program);
	}

	this->filename = name;
	isLoaded = false;

	std::string progSource = input.str();
	std::string vertSource = "#define VERTEX " + progSource;
	const char *vertSP = vertSource.c_str();
	std::string fragSource = "#define FRAGMENT " + progSource;
	const char *fragSP = fragSource.c_str();

	int compiled;
	char msg[cMessageBufferSize];
	msg[0] = 'e';
	msg[1] = 'r';
	msg[2] = 'r';
	msg[3] = '\0';

	vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, &vertSP, NULL);
	glCompileShader(vertex_shader);
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &compiled);
	glGetShaderInfoLog(vertex_shader, cMessageBufferSize, NULL, msg);
	if (!compiled)
	{
		throw std::runtime_error("Vertex shader for " + filename + " compile error: " + msg);
	}

	if (!compiled)
	{
		glDeleteShader(vertex_shader);
		return false;
	}

	fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragSP, NULL);
	glCompileShader(fragment_shader);
	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &compiled);
	glGetShaderInfoLog(fragment_shader, cMessageBufferSize, NULL, msg);
	if (!compiled)
	{
		throw std::runtime_error("Fragment shader for " + filename + " compile error: " + msg);
	}

	if (!compiled)
	{
		glDeleteShader(vertex_shader);
		glDeleteShader(fragment_shader);
		return false;
	}

	program = glCreateProgram();
	glAttachShader(program, vertex_shader);
	glAttachShader(program, fragment_shader);
	glLinkProgram(program);
	int linked;
	glGetProgramiv(program, GL_LINK_STATUS, &linked);
	glGetProgramInfoLog(program, sizeof(msg), NULL, msg);
	if (!linked)
	{
		throw std::runtime_error("Program " + filename + " link error: " + msg);
	}

	if (!linked)
	{
		glDeleteShader(vertex_shader);
		glDeleteShader(fragment_shader);
		glDeleteProgram(program);
		return false;
	}

	isLoaded = true;
	return true;
}

//-----------------------------------------------------------------------------
// ShaderProgram - Uniforms
//-----------------------------------------------------------------------------

int GlslShader::GetUniformIndex(std::string name)
{
	if (!isLoaded)
	{
		throw std::runtime_error("Could not get uniform " + name + " because program " + filename + "is not loaded");
	}
	int i = glGetUniformLocation(program, name.c_str());
	if (i == -1)
	{
		throw std::runtime_error("Could not get uniform " + name + " in loaded program " + filename);
	}
	return i;
}

void GlslShader::SetUniform1f(int location, float v0)
{
	glUniform1f(location, v0); DEC
}

void GlslShader::SetUniform2f(int location, float v0, float v1)
{
	glUniform2f(location, v0, v1); DEC
}

void GlslShader::SetUniform3f(int location, float v0, float v1, float v2)
{
	glUniform3f(location, v0, v1, v2); DEC
}

void GlslShader::SetUniform4f(int location, float v0, float v1, float v2, float v3)
{
	glUniform4f(location, v0, v1, v2, v3); DEC
}

void GlslShader::SetUniform1i(int location, int v0)
{
	glUniform1i(location, v0); DEC
}

void GlslShader::SetUniform2i(int location, int v0, int v1)
{
	glUniform2i(location, v0, v1); DEC
}

void GlslShader::SetUniform3i(int location, int v0, int v1, int v2)
{
	glUniform3i(location, v0, v1, v2); DEC
}

void GlslShader::SetUniform4i(int location, int v0, int v1, int v2, int v3)
{
	glUniform4i(location, v0, v1, v2, v3); DEC
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
