/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "renderer/ModelPartAnimation.hh"
#include <iostream>
#include <cmath>
#include "library/maths/Maths.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

ModelPartAnimation::ModelPartAnimation(const Model *model, const BvhFile *bvh)
{
	frameRate = 0;
	currentFrame = -1;
	this->model = model;
	this->bvh = bvh;

	if ((model->NumParts() == 0) || !bvh->IsValid() || (bvh->NumJoints() == 0))
		return;

	frameTime = bvh->FrameTime();
	if (frameTime == 0)
		frameTime = 0.1f;
	frameRate = 1.0f / frameTime;

	// Build an index from joints to parts, and back.
	partToJointMap.resize(model->NumParts(), -1);
	jointToPartMap.resize(bvh->NumJoints(), -1);
	for (int jointIndex = 0; jointIndex < bvh->NumJoints(); ++jointIndex)
	{
		int partIndex = model->GetPartIndex(bvh->GetJoint(jointIndex).name);
		if (partIndex >= 0)
		{
			partToJointMap[partIndex] = jointIndex;
			jointToPartMap[jointIndex] = partIndex;
		}
	}

	// Look up each part's parent to calculate its offset.
	// We guarantee them to be ordered parent-to-child by using the joint index.
	modelJointOffsets.resize(bvh->NumJoints());
	for (int jointIndex = 0; jointIndex < bvh->NumJoints(); ++jointIndex)
	{
		int partIndex = jointToPartMap[jointIndex];
		if (partIndex < 0)
			continue;

		modelJointOffsets[jointIndex] = model->GetPart(partIndex)->origin;
		int parentPartIndex = GetPartParent(partIndex);
		if (parentPartIndex >= 0)
		{
			modelJointOffsets[jointIndex] -= model->GetPart(parentPartIndex)->origin;
		}
	}

	// Allocate space for the frame's joint data.
	joints.resize(bvh->NumJoints());
}

int ModelPartAnimation::GetPartParent(int part)
{
	int jointIndex = partToJointMap[part];
	if (jointIndex < 0) return -1;

	int jointParent = bvh->GetJoint(jointIndex).parent;
	if (jointParent < 0) return -1;

	while (jointToPartMap[jointParent] < 0)
	{
		jointParent = bvh->GetJoint(jointParent).parent;
		if (jointParent < 0) return -1;
	}

	return jointToPartMap[jointParent];
}

int ModelPartAnimation::NumFrames() const
{
	return bvh->NumFrames();
}

// TODO: Alter this so that we take a floating point parameter and use it to
// blend between frames. It should be zero or more, and less than the number
// of frames.

void ModelPartAnimation::SetFrame(float frame)
{
	if (frame == currentFrame)
		return;

	currentFrame = frame;

	int frame1 = (int)std::floor(frame) % bvh->NumFrames();
	int frame2 = (int)std::ceil(frame) % bvh->NumFrames();

	float blend = frame - (float)frame1;
	float invBlend = 1.0f - blend;

	const float *frameData1 = &bvh->ChannelData()[frame1 * bvh->NumChannels()];
	const float *frameData2 = &bvh->ChannelData()[frame2 * bvh->NumChannels()];

	for (int jointIndex = 0; jointIndex < joints.size(); ++jointIndex)
	{
		const BvhFile::Joint joint = bvh->GetJoint(jointIndex);
		OrientationF &ori = joints[jointIndex];

		Vector3F rotation1, rotation2;
		for (int axis = 0; axis < 3; ++axis)
		{
			rotation1[axis] = (joint.rotationMap[axis] < 0) ? 0 : frameData1[joint.rotationMap[axis]];
			rotation2[axis] = (joint.rotationMap[axis] < 0) ? 0 : frameData2[joint.rotationMap[axis]];
		}

		Vector3F rotation = rotation1 * invBlend + rotation2 * blend;

		// Override the animation offset with the one from the model.
		ori.origin = modelJointOffsets[jointIndex];
		ori.v[0].Set(1, 0, 0);
		ori.v[1].Set(0, 1, 0);
		ori.v[2].Set(0, 0, 1);

		// Use the bvh rotations but apply them in our own space.
		ori.Rotate(OrientationF::Y, -Maths::DegreesToRadians(rotation[0]));
		ori.Rotate(OrientationF::X,  Maths::DegreesToRadians(rotation[2]));
		ori.Rotate(OrientationF::Z,  Maths::DegreesToRadians(rotation[1]));

		if (joint.parent >= 0)
		{
			ori.ToExternalSpaceOf(joints[joint.parent]);
		}
	}
}

const OrientationF &ModelPartAnimation::GetPartOrientation(int part) const
{
	static OrientationF identityOrientation;

	if ((joints.size() == 0) || (part < 0))
		return identityOrientation;

	int joint = partToJointMap.at(part);
	if (joint < 0)
		return identityOrientation;
	return joints[joint];
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
