/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "renderer/MeshBuilder.hh"
#include "library/geometry/Orientation.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class ParticleMeshBuilder {
public:
	enum ParticleType {
		FACE_EYE,
		FACE_DEPTH,
	};

	ParticleMeshBuilder();
	~ParticleMeshBuilder();

	MeshBuilder* GetMesh() { return mesh; }

	void AllocateParticles(int parts);
	void SetCameraOrientation(OrientationF *orientation) { this->orientation = orientation; }
	void AddParticle(ParticleType type, Vector3F origin, float size, Color colour);
	void ClearParticles();

private:
	MeshBuilder *mesh;
	OrientationF *orientation;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
