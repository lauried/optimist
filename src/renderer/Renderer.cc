/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "renderer/Renderer.hh"
#include "resources/ImageResource.hh"
#include "resources/TextResource.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

void Renderer::Init()
{
	SetSaturation(1.0f);
	SetGamma(2.2f);
	SetLightingFull();

	LoadShader("shaders/lighting", &(shaders.lighting));
}

Renderer::~Renderer()
{
}

void Renderer::SetSaturation(float saturation)
{
	options.saturation = saturation;
}

void Renderer::SetGamma(float gamma)
{
	options.gamma = gamma;
}

void Renderer::SetLighting(Vector3F normal, Vector3F colour, float multiply, float add)
{
	options.lightNormal = normal;
	options.lightColour = colour;
	options.lightMultiply = multiply;
	options.lightAdd = add;
}

void Renderer::SetLightingFull()
{
	options.lightNormal.Set(0, 0, 1);
	options.lightColour.Set(1, 1, 1);
	options.lightMultiply = 0;
	options.lightAdd = 1;
}

void Renderer::SetLightingOff()
{
	options.lightNormal.Set(0, 0, 1);
	options.lightColour.Set(0, 0, 0);
	options.lightMultiply = 0;
	options.lightAdd = 0;
}

void Renderer::LoadShader(std::string filename, Renderer::Shader *data)
{
	IShader *shader = renderCore->CreateShader();
	if (shader == nullptr)
	{
		log->Start(ILogTarget::ERROR)
			->Write("Unable to create shader ")
			->Write(filename)
			->End();
		return;
	}
	filename = filename + "." + shader->GetNativeType();
	ResourceManager::ResourcePointer<TextResource> pointer = resourceManager->GetResource<TextResource>(filename, ResourceManager::RF_FORCE_IMMEDIATE);
	if (pointer.GetState() != ResourceManager::LOADED)
	{
		log->Start(ILogTarget::ERROR)
			->Write("Unable to load shader ")
			->Write(filename)
			->End();
		return;
	}
	std::stringstream stream(std::string(pointer.GetResource()->GetText(), pointer.GetResource()->GetTextSize()));
	try
	{
		shader->SetNative(stream, filename);
		shader->Bind();
		data->shader = shader;
		data->gamma = ReadShaderUniform(shader, "gamma");
		data->saturation = ReadShaderUniform(shader, "saturation");
		data->lightNormal = ReadShaderUniform(shader, "lightNormal");
		data->lightColour = ReadShaderUniform(shader, "lightColour");
		data->lightMultiply = ReadShaderUniform(shader, "lightMultiply");
		data->lightAdd = ReadShaderUniform(shader, "lightAdd");
		SetShaderParameters(data);
		shader->Unbind();
	}
	catch (std::exception &e)
	{
		log->Start(ILogTarget::ERROR)
			->Write(e.what())
			->End();
	}
	log->Start(ILogTarget::NOTIFICATION)
		->Write("Loaded shader ")
		->Write(filename)
		->End();
}

int Renderer::ReadShaderUniform(IShader *shader, std::string name)
{
	try
	{
		return shader->GetUniformIndex(name);
	}
	catch (std::exception &e)
	{
		log->Start(ILogTarget::ERROR)
			->Write(e.what())
			->End();
		return -1;
	}
}

void Renderer::SetShaderParameters(Shader *data)
{
	data->shader->SetUniform2f(data->gamma, options.gamma, 1.0f / options.gamma);
	data->shader->SetUniform2f(data->saturation, options.saturation, 1.0f - options.saturation);
	data->shader->SetUniform3f(data->lightNormal, options.lightNormal[0], options.lightNormal[1], options.lightNormal[2]);
	data->shader->SetUniform3f(data->lightColour, options.lightColour[0], options.lightColour[1], options.lightColour[2]);
	data->shader->SetUniform1f(data->lightMultiply, options.lightMultiply);
	data->shader->SetUniform1f(data->lightAdd, options.lightAdd);
}

//-----------------------------------------------------------------------------
// IRenderCore Pass-Through
//-----------------------------------------------------------------------------

void Renderer::ClearBuffer(int buffer)
{
	renderCore->ClearBuffer(buffer);
}

void Renderer::SetColourEnabled(bool enable)
{
	renderCore->SetColourEnabled(enable);
}

void Renderer::SetDepthEnabled(bool enable)
{
	renderCore->SetDepthEnabled(enable);
}

void Renderer::SetBlend(IRenderCore::Blend blend)
{
	renderCore->SetBlend(blend);
}

void Renderer::SetMaterial(const Material &material)
{
	UpdateMaterial(material);
	renderCore->SetMaterial(material);
}

void Renderer::DrawMesh(const MeshData &mesh)
{
	shaders.lighting.shader->Bind();
	SetShaderParameters(&(shaders.lighting));
	renderCore->DrawMesh(mesh);
}

//-----------------------------------------------------------------------------
// IRenderResources
//-----------------------------------------------------------------------------

const Material * Renderer::GetMaterial(const MaterialDescriptor& descriptor)
{
	const Material *material = materialManager.GetMaterial(descriptor);
	UpdateMaterial(*material);
	return material;
}

//-----------------------------------------------------------------------------
// Helpers
//-----------------------------------------------------------------------------

void Renderer::UpdateMaterial(const Material &material)
{
	if (!material.descriptor)
	{
		return;
	}

	Material &notConst = const_cast<Material&>(material);
	if (material.descriptor->texture.provider != nullptr)
	{
		ITextureProvider *provider = material.descriptor->texture.provider;
		if (material.texture == nullptr)
		{
			notConst.texture = renderCore->CreateTexture();
			notConst.texture->SetImage(provider->GetImage());
			notConst.texture->RequireUpload();
		}
		else if (provider->ImageUpdated())
		{
			notConst.texture->SetImage(provider->GetImage());
			notConst.texture->RequireUpload();
		}

		return;
	}

	if (material.texture == nullptr)
	{
		if (material.descriptor->texture.resource.length() > 0)
		{
			ResourceManager::ResourcePointer<ImageResource> resource = resourceManager->GetResource<ImageResource>(notConst.descriptor->texture.resource);
			switch (resource.GetState())
			{
			case ResourceManager::LOADED:
				notConst.texture = renderCore->CreateTexture();
				notConst.texture->SetImage(resource.GetResource()->GetImage());
				break;
			case ResourceManager::LOAD_FAILED:
				notConst.texture = renderCore->CreateTexture();
				break;
			}
		}
		else
		{
			notConst.texture = GetDefaultTexture();
		}
		notConst.texture->RequireUpload();
	}
}

ITexture *Renderer::GetDefaultTexture()
{
	if (defaultTexture == nullptr)
	{
		uint8_t initVal[4] = { 0xff, 0xff, 0xff, 0xff };
		defaultImage.Clear(2, 2, 4, initVal);
		defaultTexture = renderCore->CreateTexture();
		defaultTexture->SetImage(&defaultImage);
		defaultTexture->RequireUpload();
	}
	return defaultTexture;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
