/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "renderer/MeshData.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Mesh class.
 * The meshdata struct is separate so that we can pass data to the renderer
 * regardless of the owner.
 *
 * @todo Support arbitrarily many auxilliary channels.
 */
class Mesh {
public:
	static const int DEFAULT_NUM_VERTS = 32;

	struct ChannelDescriptor {
		MeshData::ChannelType type;
		MeshData::DataType dataType;
		int components;
	};

	Mesh(MeshData::MeshType type, std::vector<ChannelDescriptor> channels, size_t reservedVerts = 0);

	/**
	 * Moves on to the next vertex.
	 */
	void NextVertex();

	/**
	 * Chooses a specific vertex.
	 */
	void GoToVertex(size_t index);

	/**
	 * Number of vertices we currently have space for.
	 */
	size_t NumVertices();

	/**
	 * Sets the current vertex coordinates.
	 */
	template <typename T>
	void SetVertex(T *v);

	/**
	 * Sets the current vertex normals.
	 */
	template <typename T>
	void SetNormal(T *n);

	/**
	 * Sets the current vertex colour.
	 */
	template <typename T>
	void SetColour(T *c);

	/**
	 * Sets the current vertex texture coordinates.
	 */
	template <typename T>
	void SetTexCoord(T *t);

	template <typename T>
	void SetChannelData(T *data, MeshData::Channel MeshData::*channelPtr);

	MeshData &GetMeshData() { return meshData; }

private:
	struct ChannelMetaData {
		MeshData::Channel *channel;
		size_t offset;
	};

	size_t vertSize;
	size_t currentVert;
	std::vector<uint8_t> data;
	std::vector<ChannelMetaData> metaData;
    MeshData meshData;

    void Resize(size_t verts);
    void SetPointers();
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

