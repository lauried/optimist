/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "renderer/Mesh.hh"
#include <stdexcept>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

static void ValidateVertexChannel(Mesh::ChannelDescriptor &channel)
{
	switch (channel.type)
	{
	case MeshData::DOUBLE:
	case MeshData::FLOAT:
	case MeshData::INT:
	case MeshData::SHORT:
		break;
	default:
		throw std::logic_error("Invalid vertex type");
	}
	if (channel.components != 2 && channel.components != 3 && channel.components != 4)
	{
		throw std::logic_error("Invalid vertex components");
	}
}

static void ValidateNormalChannel(Mesh::ChannelDescriptor &channel)
{
	switch (channel.type)
	{
	case MeshData::DOUBLE:
	case MeshData::FLOAT:
	case MeshData::INT:
	case MeshData::SHORT:
	case MeshData::CHAR:
		break;
	default:
		throw std::logic_error("Invalid normal type");
	}
	channel.components = 3;
}

static void ValidateTexCoordChannel(Mesh::ChannelDescriptor &channel)
{
	switch (channel.type)
	{
	case MeshData::DOUBLE:
	case MeshData::FLOAT:
	case MeshData::INT:
	case MeshData::SHORT:
		break;
	default:
		throw std::logic_error("Invalid texcoord type");
	}
	if (channel.components != 1 && channel.components != 2 && channel.components != 3 && channel.components != 4)
	{
		throw std::logic_error("Invalid vertex components");
	}
}

static void ValidateColourChannel(Mesh::ChannelDescriptor &channel)
{
	switch (channel.type)
	{
	case MeshData::DOUBLE:
	case MeshData::FLOAT:
	case MeshData::UINT:
	case MeshData::INT:
	case MeshData::SHORT:
	case MeshData::USHORT:
	case MeshData::CHAR:
	case MeshData::UCHAR:
		break;
	default:
		throw std::logic_error("Invalid colour type");
	}
	if (channel.components != 3 && channel.components != 4)
	{
		throw std::logic_error("Invalid colour components");
	}
}

static void ValidateChannel(Mesh::ChannelDescriptor &channel)
{
    switch (channel.type)
    {
	case MeshData::VERTS:
		ValidateVertexChannel(channel);
		return;
	case MeshData::NORMALS:
		ValidateNormalChannel(channel);
		return;
	case MeshData::TEXCOORDS:
		ValidateTexCoordChannel(channel);
		return;
	case MeshData::COLOURS:
		ValidateColourChannel(channel);
		return;
    }
    throw std::logic_error("Unexpected channel type");
}

static MeshData::Channel* ChannelForType(MeshData::ChannelType type, MeshData *meshData)
{
	switch (type)
	{
	case MeshData::VERTS:
		return &(meshData->verts);
	case MeshData::NORMALS:
		return &(meshData->normals);
	case MeshData::TEXCOORDS:
		return &(meshData->texcoords);
	case MeshData::COLOURS:
		return &(meshData->colours);
	}
}

static int SizeForChannelType(MeshData::DataType type)
{
	switch (type)
	{
	case MeshData::DOUBLE:
		return 8;
	case MeshData::FLOAT:
	case MeshData::UINT:
	case MeshData::INT:
		return 4;
	case MeshData::SHORT:
	case MeshData::USHORT:
		return 2;
	case MeshData::CHAR:
	case MeshData::UCHAR:
		return 1;
	}
	throw std::logic_error("Invalid channel type");
}

size_t PadTo(size_t offset, size_t alignment)
{
	size_t remainder = offset % alignment;
	if (remainder == 0)
	{
		return offset;
	}
	return offset + (alignment - remainder);
}

Mesh::Mesh(MeshData::MeshType type, std::vector<ChannelDescriptor> channels, size_t reservedVerts)
{
	size_t currentOffset = 0;
	size_t maxComponentSize = 0;
	metaData.resize(channels.size());
	for (int i = 0; i < channels.size(); i += 1)
	{
		ValidateChannel(channels[i]);
		MeshData::Channel *channel = ChannelForType(channels[i].type, &meshData);

		metaData[i].channel = channel;

		channel->type = channels[i].dataType;
		channel->components = channels[i].components;

        size_t componentSize = SizeForChannelType(channels[i].dataType);

        if (maxComponentSize < componentSize)
        {
			maxComponentSize = componentSize;
        }

		currentOffset = PadTo(currentOffset, componentSize);
        metaData[i].offset = currentOffset;
        currentOffset += channels[i].components * componentSize;
	}
	vertSize = PadTo(currentOffset, maxComponentSize);

	Resize(reservedVerts);
	currentVert = 0;
}

void Mesh::Resize(size_t verts)
{
	if (verts < 1)
	{
		verts = DEFAULT_NUM_VERTS;
	}
	meshData.numVerts = verts;
	data.resize(verts * vertSize);
	SetPointers();
}

void Mesh::SetPointers()
{
	for (size_t i = 0; i < metaData.size(); i += 1)
	{
		MeshData::Channel *channel = metaData[i].channel;
		size_t offset = metaData[i].offset;
		switch (channel->type)
		{
		case MeshData::DOUBLE:
			channel->data.d = (double*)&data[offset];
			break;
		case MeshData::FLOAT:
			channel->data.f = (float*)&data[offset];
			break;
		case MeshData::UINT:
		case MeshData::INT:
			channel->data.i = (int32_t*)&data[offset];
			break;
		case MeshData::SHORT:
		case MeshData::USHORT:
			channel->data.s = (int16_t*)&data[offset];
			break;
		case MeshData::CHAR:
		case MeshData::UCHAR:
			channel->data.c = (int8_t*)&data[offset];
			break;
		}
	}
}

void Mesh::NextVertex()
{
	currentVert += 1;
	if (currentVert >= meshData.numVerts)
	{
		if (meshData.numVerts = 0)
		{
			Resize(DEFAULT_NUM_VERTS);
		}
		else
		{
			Resize(meshData.numVerts * 2);
		}
	}
}

template <typename T>
void Mesh::SetVertex(T *v)
{
	SetChannelData(v, MeshData::verts);
}

template <typename T>
void Mesh::SetNormal(T *n)
{
	SetChannelData(n, MeshData::normals);
}

template <typename T>
void Mesh::SetColour(T *c)
{
	SetChannelData(c, MeshData::colours);
}

template <typename T>
void Mesh::SetTexCoord(T *t)
{
	SetChannelData(t, MeshData::texcoords);
}

template <typename T>
void Mesh::SetChannelData(T *data, MeshData::Channel MeshData::*channelPtr)
{
	MeshData::Channel *channel = &(meshData.*channelPtr);
	int offset = (currentVert * vertSize) / SizeForChannelType(channel->type);
	switch (channel->type)
	{
	case MeshData::DOUBLE:
		for (int i = 0; i < channel->components; i += 1)
		{
			channel->data.d[offset + i] = (double)data[i];
		}
		break;
	case MeshData::FLOAT:
		for (int i = 0; i < channel->components; i += 1)
		{
			channel->data.f[offset + i] = (float)data[i];
		}
		break;
	case MeshData::INT:
		for (int i = 0; i < channel->components; i += 1)
		{
			channel->data.i[offset + i] = (int32_t)data[i];
		}
		break;
	case MeshData::UINT:
		for (int i = 0; i < channel->components; i += 1)
		{
			channel->data.i[offset + i] = (uint32_t)data[i];
		}
		break;
	case MeshData::SHORT:
		for (int i = 0; i < channel->components; i += 1)
		{
			channel->data.s[offset + i] = (int16_t)data[i];
		}
		break;
	case MeshData::USHORT:
		for (int i = 0; i < channel->components; i += 1)
		{
			channel->data.s[offset + i] = (uint16_t)data[i];
		}
		break;
	case MeshData::CHAR:
		for (int i = 0; i < channel->components; i += 1)
		{
			channel->data.c[offset + i] = (int32_t)data[i];
		}
		break;
	case MeshData::UCHAR:
		for (int i = 0; i < channel->components; i += 1)
		{
			channel->data.c[offset + i] = (uint32_t)data[i];
		}
		break;
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
