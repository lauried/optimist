/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "renderer/Model.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

Model::Model()
{
}

Model::~Model()
{
	for (auto it = meshes.begin(); it != meshes.end(); ++it)
	{
		delete it->mesh;
	}
}

Model::MeshData *Model::AddMesh()
{
	meshes.push_back(MeshData());
	MeshBuilder *mesh = new MeshBuilder();
	mesh->AddAttribute(MeshBuilder::VERTEX, 3);
	mesh->AddAttribute(MeshBuilder::NORMAL, 3);
	mesh->AddAttribute(MeshBuilder::COLOUR, 3);
	meshes.at(meshes.size() -1).mesh = mesh;
	return &(meshes.at(meshes.size() -1));
}

int Model::AddPart(std::string name)
{
	int i = GetPartIndex(name);
	if (i > 0)
		return i;
	parts.push_back(PartData(name));
	return parts.size() - 1;
}

int Model::GetPartIndex(std::string name) const
{
	for (int i = 0; i < parts.size(); ++i)
	{
		if (!parts.at(i).name.compare(name))
			return i;
	}
	return -1;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

