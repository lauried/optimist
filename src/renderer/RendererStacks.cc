/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "renderer/RendererStacks.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

void RendererStacks::PushViewport(Vector2I topLeft, Vector2I bottomRight)
{
	Box2I box(topLeft, bottomRight);
	viewportStack.push(box);
	renderCore->SetViewport(box.mins, box.maxs);
}

Box2I RendererStacks::PeekViewport()
{
	if (viewportStack.size() < 1)
	{
		throw std::logic_error("Cannot peek empty viewport stack");
	}
	return viewportStack.top();
}

void RendererStacks::PopViewport()
{
	if (viewportStack.size() < 1)
	{
		throw std::logic_error("Cannot pop empty viewport stack");
	}
	Box2I box = PeekViewport();
	renderCore->SetViewport(box.mins, box.maxs);
	viewportStack.pop();
}

void RendererStacks::SetOrthographicProjection(float left, float top, float right, float bottom, float nearPlane, float farPlane)
{
	renderCore->SetOrthographicProjection(left, top, right, bottom, nearPlane, farPlane);
}

void RendererStacks::SetPerspectiveProjection(float left, float top, float right, float bottom, float nearPlane, float farPlane)
{
	renderCore->SetPerspectiveProjection(left, top, right, bottom, nearPlane, farPlane);
}

void RendererStacks::SetCameraPosition(const OrientationF *orientation)
{
	renderCore->SetView(*orientation);
	std::stack<OrientationF> empty;
	modelTranslationStack.swap(empty);
}

void RendererStacks::PushModelTranslation(const OrientationF *orientation)
{
	modelTranslationStack.push(modelMatrix);
	modelMatrix.Set(*orientation);
	renderCore->SetModel(modelMatrix);
}

const OrientationF& RendererStacks::PeekModelTranslation()
{
	return modelTranslationStack.top();
}

void RendererStacks::PopModelTranslation()
{
	if (modelTranslationStack.size() < 1)
	{
		throw std::logic_error("Model Translation Stack popped too much");
	}
	renderCore->SetModel(modelTranslationStack.top());
	modelTranslationStack.pop();
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
