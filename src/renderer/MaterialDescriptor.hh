/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <string>
#include <vector>
#include "library/graphics/Color.hh"
#include "renderer/ITextureProvider.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

struct MaterialDescriptor {
	enum LightMode {
		LightNormal = 0,
		LightFullbright = 1
	};

	enum FilterType {
		FilterLinear = 0,
		FilterNearest = 1
	};

	struct Texture {
		// Provider takes precedence over resource name.
		ITextureProvider *provider;
		std::string resource;
		FilterType filter;
		bool mipmap;

		Texture()
			: provider(nullptr)
			, filter(FilterNearest)
			, mipmap(true)
			{}
		bool operator==(const Texture &other) const;
		size_t Hash() const;
	};

	LightMode lightMode;
	Texture texture;
	Color color;

	MaterialDescriptor()
		: lightMode(LightNormal)
		, texture()
		, color(1.0f, 1.0f, 1.0f, 1.0f)
		{}

	struct Hash {
		size_t operator()(const MaterialDescriptor& descriptor) const;
	};

	struct Equal {
		bool operator()(const MaterialDescriptor& lhs, const MaterialDescriptor& rhs) const;
	};
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

