/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "renderer/MaterialDescriptor.hh"
#include <functional>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

size_t MaterialDescriptor::Texture::Hash() const
{
	std::string resourceCopy;
	if (provider == nullptr)
	{
		resourceCopy = resource;
	}

	std::hash<std::string> stringHash;
	std::hash<ITextureProvider*> providerHash;
	std::hash<int> intHash;
	std::hash<bool> boolHash;
	return
		stringHash(resourceCopy)
		^ intHash(filter)
		^ providerHash(provider)
		^ boolHash(mipmap);
}

bool MaterialDescriptor::Texture::operator==(const MaterialDescriptor::Texture &other) const
{
	std::string resourceCopy, otherResourceCopy;
	if (provider == nullptr)
	{
		resourceCopy = resource;
	}
	if (other.provider == nullptr)
	{
		otherResourceCopy = other.resource;
	}

	return filter == other.filter
		&& mipmap == other.mipmap
		&& provider == other.provider
		&& provider == other.provider
		&& (resourceCopy.compare(otherResourceCopy) == 0);
}

size_t MaterialDescriptor::Hash::operator()(const MaterialDescriptor& descriptor) const
{
	std::hash<int> intHash;
	std::hash<float> floatHash;
	return descriptor.texture.Hash()
		^ (intHash((int)descriptor.lightMode) << 1)
		^ (floatHash(descriptor.color.rgba[0]) << 0)
		^ (floatHash(descriptor.color.rgba[1]) << 1)
		^ (floatHash(descriptor.color.rgba[2]) << 2)
		^ (floatHash(descriptor.color.rgba[3]) << 3);
}

bool MaterialDescriptor::Equal::operator()(const MaterialDescriptor& lhs, const MaterialDescriptor& rhs) const
{
	return lhs.lightMode == rhs.lightMode
		&& lhs.color == rhs.color
		&& lhs.texture == rhs.texture;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
