/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <stack>
#include "renderer/IRenderCore.hh"
#include "resources/ResourceManager.hh"
#include "library/geometry/Vector2.hh"
#include "library/geometry/Box2.hh"
#include "library/geometry/Orientation.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// TODO: This should just implement state stacks and be renamed to something else.

/**
 * Wraps an IRenderCore, manages state stacks and resource loading.
 */
class RendererStacks {
public:
	RendererStacks(IRenderCore *renderCore) : renderCore(renderCore) {}

	IRenderCore* GetRenderCore() { return renderCore; }

	// push/peek/pop viewport
	// TODO: PushViewport accept relative/absolute parameter.
	void PushViewport(Vector2I topLeft, Vector2I bottomRight);
	Box2I PeekViewport();
	void PopViewport();

	void SetOrthographicProjection(float left, float top, float right, float bottom, float nearPlane, float farPlane);
	void SetPerspectiveProjection(float left, float top, float right, float bottom, float nearPlane, float farPlane);

	/**
	 * @param orientation Position of the camera in world space.
	 */
	void SetCameraPosition(const OrientationF *orientation);

	/**
	 * @param orientation The origin of the coordinate of the model space in
	 *                    world space (if this is the first push), or in the
	 *                    previously pushed coordinate space.
	 */
	void PushModelTranslation(const OrientationF *orientation);

	/**
	 * @return The current coordinate system relative to world space.
	 */
	const OrientationF& PeekModelTranslation();

	void PopModelTranslation();

private:
	IRenderCore *renderCore;
	std::stack<Box2I> viewportStack;
	std::stack<OrientationF> modelTranslationStack;
	OrientationF modelMatrix;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

