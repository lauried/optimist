/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <vector>
#include <string>
#include "library/geometry/Vector3.hh"
#include "library/geometry/Box3.hh"
#include "library/file_types/QEntityFile.hh"
#include "renderer/MeshBuilder.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class Model {
public:
	Model();
	~Model();

	struct MeshData {
		int part;
		MeshBuilder *mesh;
		MeshData() : part(0), mesh(0) { }
	};

	struct PartData {
		std::string name;
		Vector3F origin;

		PartData(std::string name) { this->name = name; }
	};

	MeshData *AddMesh();
	size_t NumMeshes() const { return meshes.size(); }
	MeshBuilder *GetMesh(size_t i) { return meshes.at(i).mesh; }
	const MeshBuilder *GetMesh(size_t i) const { return meshes.at(i).mesh; }
	MeshData *GetMeshData(size_t i) { return &meshes.at(i); }
	const MeshData *GetMeshData(size_t i) const { return &meshes.at(i); }

	// Adds the part if it can't find it.
	int AddPart(std::string name);
	// Returns -1 if it can't find the part.
	int GetPartIndex(std::string name) const;
	PartData *GetPart(int i) { return &parts.at(i); }
	const PartData *GetPart(int i) const { return &parts.at(i); }
	int NumParts() const { return parts.size(); }

	// Point data.
	QEntityFile &PointData() { return pointData; }

	// Loaded scale.
	void SetScale(float s) { loadedScale = s; }
	float Scale() { return loadedScale; }

	Box3F &Bounds() { return bounds; }

private:
	std::vector<MeshData> meshes;
	std::vector<PartData> parts;
	QEntityFile pointData;
	Box3F bounds;
	float loadedScale;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
