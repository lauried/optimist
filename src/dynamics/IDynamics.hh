/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <vector>
#include <functional>
#include "dynamics/IDynamicsObject.hh"
#include "dynamics/IStaticObjectProvider.hh"
#include "dynamics/IDynamicsObject.hh"
#include "dynamics/DynamicsShapeData.hh"
#include "library/geometry/Vector3.hh"
#include "library/geometry/Orientation.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// TODO: Query functions

/**
 * Simple dynamics interface for basic game elements.
 * The goal of this interface is to allow game creators to quickly and easily
 * implement typical game objects.
 */
class IDynamics {
public:
	enum MotionType {
		/**
		 * The shape moves freely about the environment as though it were a
		 * tumbling box.
		 */
		T_Free,
		/**
		 * The shape rolls as though it has wheels.
		 * Parameters about the wheels can be set on the object.
		 */
		T_Vehicle,
		/**
		 * This usually ignores shape, reading boxSize instead.
		 * Represents a type of object which is always oriented vertically, e.g
		 * a person.
		 * The orientation of the object may always have fixed angles, so
		 * yawing must be simulated outside of the dynamics simulation.
		 */
		T_Character,
		/**
		 * The object does not move at all. It is completely rigid.
		 */
		T_Static
	};

	virtual ~IDynamics();

	/**
	 * Create an object. The dynamics implementation owns the object and its
	 * interface is returned.
	 *
	 * The implementation MUST allow the shape data to be destroyed after the
	 * call to AddObject has returned.
	 */
	virtual IDynamicsObject* AddObject(
		IDynamicsUserData *userData,
		OrientationF &position,
		MotionType motion,
		Vector3F boxSize,
		DynamicsShapeData *shape = nullptr,
		float dynamicsMass = 1
	) = 0;

	/**
	 * Removes an object that had been added.
	 * An exception should be thrown if the object is not present in the simulation.
	 */
	virtual void RemoveObject(IDynamicsObject *object) = 0;

	/**
	 * Add a static object provider.
	 * The dynamics implementation must request all objects which its dynamics
	 * objects may intercept. It can calculate the potential volumes of
	 * intersection from their positions and velocities.
	 *
	 * Adding the same provider twice should have no effect, leaving the
	 * instance listed internally only once.
	 */
	virtual void AddStaticObjectProvider(IStaticObjectProvider *provider) = 0;

	/**
	 * Removes a static object provider that had been added.
	 * An exception should be thrown if the provider is not present in the simulation.
	 */
	virtual void RemoveStaticObjectProvider(IStaticObjectProvider *provider) = 0;

	/**
	 * Runs the simulation for an amount of time.
	 * Time is somewhat arbitrary but implementations may break the time down
	 * into sub-steps. Seconds, or 1024/1000 of a second may be assumed.
	 */
	virtual void Run(double time) = 0;

	/**
	 * The callback will be called during Run() calls with the two objects
	 * which have collided. It is called once per colliding pair.
	 */
	virtual void SetCollisionCallback(std::function<void(IDynamicsObject*, IDynamicsObject*)> callback) = 0;

	/**
	 * Returns all objects currently active (dynamic).
	 */
	virtual const std::vector<IDynamicsObject*>* Objects() = 0;

	/**
	 * Sets an implementation specific property of the implementation.
	 */
	virtual void SetProperty(std::string name, double value) = 0;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

