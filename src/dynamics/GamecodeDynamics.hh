/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gamecode/IGamecodeEngineObject.hh"
#include "dynamics/IDynamics.hh"
#include <map>
#include <vector>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Represents a dynamics world to the gamecode.
 * Behind the scenes this may use any implementation of the simple dynamics
 * interface.
 *
 * TODO: Callbacks to individual objects on collision.
 *
 * TODO: Do we invoke physics step manually or automatically?
 */
class GamecodeDynamics : public IGamecodeEngineObject {
public:
	GamecodeDynamics(IDynamics *dynamics) :
		dynamics(dynamics),
		collisionCallback(nullptr)
		{}

	~GamecodeDynamics();

	void SetCollisionCallback(IGamecodeFunction *callback) { collisionCallback = callback; }

	void Run(double time);

	// IGamecodeEngineObject
	//void SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode);
	//GamecodeValue GetProperty(std::string key, IGamecode *gamecode);
	//void SetIndex(int i, GamecodeValue val, IGamecode *gamecode);
	//GamecodeValue GetIndex(int i, IGamecode *gamecode);
	std::vector<IGamecodeObject*>* GetChildObjects();

	static IGamecodeEngineObject* Construct(GamecodeParameterList *parameters, IGamecode *gamecode);

	// Gamecode Methods
	GamecodeValue Run(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue AddObject(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue RemoveObject(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue AddStaticObjectProvider(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue RemoveStaticObjectProvider(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue SetProperty(GamecodeParameterList *parameters, IGamecode *gamecode);

private:
	IDynamics *dynamics;
	IGamecodeFunction *collisionCallback;

	// Keeps track of contained static object providers so we can return the list for GC.
	std::map<IGamecodeEngineObject*, IGamecodeEngineObject*> staticObjectProviders;
	std::vector<IGamecodeObject*> childObjects;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

