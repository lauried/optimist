/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gamecode/IGamecodeEngineObject.hh"
#include "dynamics/IDynamics.hh"
#include "dynamics/IDynamicsObject.hh"
#include "dynamics/IDynamicsUserData.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class GamecodeDynamicsObject : public IGamecodeEngineObject, public IDynamicsUserData {
public:
	GamecodeDynamicsObject(IDynamics *dynamics, IDynamicsObject *object) : dynamics(dynamics), object(object) {}
	~GamecodeDynamicsObject();

	/**
	 * Remove this object from the simulation it's in.
	 */
	void Remove();

	// IGamecodeEngineObject
	void SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode);
	GamecodeValue GetProperty(std::string key, IGamecode *gamecode);

	static IGamecodeEngineObject* Construct(GamecodeParameterList *parameters, IGamecode *gamecode);

	// Gamecode Methods
	GamecodeValue GetOrientation(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue SetOrientation(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue GetVelocity(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue SetVelocity(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue GetAVelocity(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue SetAVelocity(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue ApplyForce(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue ApplyForceRelative(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue GetGroundNormal(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue SetGravity(GamecodeParameterList *parameters, IGamecode *gamecode);

private:
	IDynamics *dynamics;
	IDynamicsObject *object;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

