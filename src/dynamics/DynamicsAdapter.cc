/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "dynamics/DynamicsAdapter.hh"
#include "dynamics/GamecodeDynamics.hh"
#include "dynamics/GamecodeDynamicsObject.hh"
#include "dynamics/GamecodeTerrainDynamics.hh"
#include "gamecode/ModularEngineTypes.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

const std::string& DynamicsAdapter::GetName()
{
	static std::string name("Dynamics");
	return name;
}

std::map<std::string, IGamecodeAdapter::AdapterMethod> DynamicsAdapter::GetGlobalFunctions()
{
	return {};
}

void DynamicsAdapter::OnRegister(IGamecode *gamecode)
{
	gamecode->RegisterEngineType("Dynamics", gamecode->CreateEngineType<GamecodeDynamics>()
		->Constructor(&GamecodeDynamics::Construct)
		->AddMethod("run", &GamecodeDynamics::Run)
		->AddMethod("addObject", &GamecodeDynamics::AddObject)
		->AddMethod("removeObject", &GamecodeDynamics::RemoveObject)
		->AddMethod("addStaticObjectProvider", &GamecodeDynamics::AddStaticObjectProvider)
		->AddMethod("removeStaticObjectProvider", &GamecodeDynamics::RemoveStaticObjectProvider)
		->AddMethod("setProperty", &GamecodeDynamics::SetProperty)
	);

	gamecode->RegisterEngineType("DynamicsObject", gamecode->CreateEngineType<GamecodeDynamicsObject>()
		->Constructor(&GamecodeDynamicsObject::Construct)
		->AddMethod("getOrientation", &GamecodeDynamicsObject::GetOrientation)
		->AddMethod("setOrientation", &GamecodeDynamicsObject::SetOrientation)
		->AddMethod("getVelocity", &GamecodeDynamicsObject::GetVelocity)
		->AddMethod("setVelocity", &GamecodeDynamicsObject::SetVelocity)
		->AddMethod("getAVelocity", &GamecodeDynamicsObject::GetAVelocity)
		->AddMethod("setAVelocity", &GamecodeDynamicsObject::SetAVelocity)
		->AddMethod("applyForce", &GamecodeDynamicsObject::ApplyForce)
		->AddMethod("applyForceRelative", &GamecodeDynamicsObject::ApplyForceRelative)
		->AddMethod("getGroundNormal", &GamecodeDynamicsObject::GetGroundNormal)
		->AddMethod("setGravity", &GamecodeDynamicsObject::SetGravity)
		->AddProperty("onGround")
	);

	gamecode->RegisterEngineType("TerrainDynamics", gamecode->CreateEngineType<GamecodeTerrainDynamics>()
		->Constructor(&GamecodeTerrainDynamics::Construct)
	);

	ModularEngineTypes::Instance()->Register<DynamicsAdapter>(gamecode, resources);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
