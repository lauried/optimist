/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "dynamics/GamecodeTerrainDynamics.hh"
#include <cmath>
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

GamecodeTerrainDynamics::~GamecodeTerrainDynamics()
{
}

//-----------------------------------------------------------------------------
// IGamecodeEngineObject
//-----------------------------------------------------------------------------

IGamecodeEngineObject* GamecodeTerrainDynamics::Construct(GamecodeParameterList *parameters, IGamecode *gamecode)
{
    parameters->RequireParmCount(1);
    GamecodeTerrain *terrain = parameters->RequireEngineType<GamecodeTerrain>(0, "GamecodeTerrain");
    return new GamecodeTerrainDynamics(terrain);
}

//-----------------------------------------------------------------------------
// IStaticObjectProvider
//-----------------------------------------------------------------------------

static Array2D<char> *GenerateDiagonalsArray(TerrainChunk *terrain)
{
	const Array2D<TileInfo> *tileInfo = terrain->GetTileInfo();
	size_t size = tileInfo->NumElements();
	Array2D<char> *diagonals = new Array2D<char>(tileInfo->Width(),  tileInfo->Height());

	for (size_t i = 0; i < size; i += 1)
	{
		diagonals->at(i) = tileInfo->at(i).diagonality;
	}

	return diagonals;
}

void GamecodeTerrainDynamics::GetObjectsForVolume(std::vector<Box3F> *bounds, std::vector<IStaticObject*> *objects)
{
	for (auto it = bounds->begin(); it != bounds->end(); ++it)
	{
		Vector2F chunkMins = terrain->WorldCoordsToChunk(Vector2F((*it).mins.v));
		Vector2F chunkMaxs = terrain->WorldCoordsToChunk(Vector2F((*it).maxs.v));

		Vector2I mins(std::floor(chunkMins[0]), std::floor(chunkMins[1]));
		Vector2I maxs(std::ceil(chunkMaxs[0]), std::ceil(chunkMaxs[1]));

		Vector2I coord;
		for (coord[0] = mins[0]; coord[0] < maxs[0]; coord[0] += 1)
		{
			for (coord[1] = mins[1]; coord[1] < maxs[1]; coord[1] += 1)
			{
				auto it = chunks.find(coord);
				if (it == chunks.end())
				{
					TerrainChunk *data = terrain->GetChunk(coord[0], coord[1]);

					auto diagonals = GenerateDiagonalsArray(data);

					DynamicsHeightmapData *heightmap = new DynamicsHeightmapData();
					heightmap->tileSize = data->HorizScale();
					heightmap->heights = data->GetHeights();
					heightmap->diagonals = diagonals;

					TerrainStaticObject *object = new TerrainStaticObject();
					object->orientation.origin = data->GetOrigin();
					object->bounds = data->GetBounds();
					object->bounds.mins += object->orientation.origin;
					object->bounds.maxs += object->orientation.origin;
					object->shape = heightmap;
					heightmap->minHeight = object->bounds.mins[2];
					heightmap->maxHeight = object->bounds.maxs[2];
					object->userData = nullptr;

					chunks.insert(std::make_pair(coord, ChunkData(data, object, diagonals)));
					chunksByObject.insert(std::make_pair(object, coord));

					objects->push_back(object);
				}
			}
		}
	}
}

void GamecodeTerrainDynamics::FreeObject(IStaticObject *object)
{
	auto it = chunksByObject.find(object);
	if (it == chunksByObject.end())
	{
		throw std::logic_error("Attempted to call FreeObject with an object that wasn't in use");
	}
	auto it2 = chunks.find(it->second);
	if (it2 == chunks.end())
	{
		throw std::logic_error("Internal error");
	}

	delete it2->second.diagonals;
	delete it2->second.object->shape;
	delete it2->second.object;
	terrain->FreeChunk(it2->second.terrain);

	chunksByObject.erase(it);
	chunks.erase(it2);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
