/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "dynamics/ode/OdeDynamics.hh"
#include <iostream>
#include "dynamics/ode/OdeObject.hh"
#include "dynamics/ode/OdeGeometryData.hh"
#include "dynamics/DynamicsShapeData.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

static int gOdeInstances = 0;

void OdeErrorCallback(int errnum, const char *msg, va_list ap)
{
	std::cout << "ODE Error " << errnum << ": " << msg << std::endl;
}

void OdeDebugCallback(int errnum, const char *msg, va_list ap)
{
	std::cout << "ODE Debug " << errnum << ": " << msg << std::endl;
}

void OdeMessageCallback(int errnum, const char *msg, va_list ap)
{
	std::cout << "ODE Message " << errnum << ": " << msg << std::endl;
}

OdeDynamics::OdeDynamics()
{
	gOdeInstances += 1;
	if (gOdeInstances == 1)
	{
		dInitODE();
		dSetErrorHandler(OdeErrorCallback);
		dSetDebugHandler(OdeDebugCallback);
		dSetMessageHandler(OdeMessageCallback);
	}

	world = dWorldCreate();
	space = dHashSpaceCreate(0);
	dWorldSetGravity(world, 0, 0, -4.905); // TODO: Make into a parameter.
	dWorldSetContactMaxCorrectingVel(world, 1.0);
	stepSize = 1.0 / 256.0; // TODO: Make into a parameter.
	framesBeforeFreeingStaticObjects = 12;
	onGroundGradient = 0.701;
	maxFrameTime = 0.05; // We won't run slower than 20fps.
	dynamicObjectListValid = false;
	tempContactJointGroup = dJointGroupCreate(0);
	timeLeft = 0;
}

OdeDynamics::~OdeDynamics()
{
	for (auto it = dynamicObjects.begin(); it != dynamicObjects.end(); ++it)
	{
		RemoveObject(*it, false);
	}
	for (auto sop = staticObjectProviders.begin(); sop != staticObjectProviders.end(); ++sop)
	{
		for (auto itEntry = sop->second.begin(); itEntry != sop->second.end(); ++itEntry)
		{
			sop->first->FreeObject(itEntry->odeObject->staticObject);
		}
	}
	for (auto it = staticObjects.begin(); it != staticObjects.end(); ++it)
	{
		RemoveObject(*it, false);
	}

	dJointGroupDestroy(tempContactJointGroup);
	dSpaceDestroy(space);
	dWorldDestroy(world);

	gOdeInstances -= 1;
	if (gOdeInstances < 0)
	{
		std::cout << "Error: ODE closed more times than opened" << std::endl;
	}
	if (gOdeInstances == 0)
	{
		dCloseODE();
	}
}

//-----------------------------------------------------------------------------
// Own Methods
//-----------------------------------------------------------------------------

void OdeDynamics::SetGravity(Vector3F gravity)
{
	dWorldSetGravity(world, gravity[0], gravity[1], gravity[2]);
}

//-----------------------------------------------------------------------------
// IDynamics
//-----------------------------------------------------------------------------

static void BuildObjectGeometry(OdeStaticObject *object, dWorldID world, dSpaceID space, IDynamics::MotionType motion, DynamicsShapeData *shape, Vector3F boxSize);
static void DeleteGeometryData(OdeGeometryData *geometryData);

IDynamicsObject* OdeDynamics::AddObject(
		IDynamicsUserData *userData,
		OrientationF &position,
		IDynamics::MotionType motion,
		Vector3F boxSize,
		DynamicsShapeData *shape,
		float dynamicsMass
)
{
	OdeStaticObject *staticObject;

	if (motion == IDynamics::T_Static)
	{
		OdeStaticObject *object = new OdeStaticObject();
		BuildObjectGeometry(object, world, space, motion, shape, boxSize);

		staticObjects.insert(object);
		staticObjectListValid = false;

		staticObject = object;
	}
	else
	{
		OdeDynamicObject *object = new OdeDynamicObject();
		BuildObjectGeometry(object, world, space, motion, shape, boxSize);

		object->body = dBodyCreate(world);
		dGeomSetBody(object->geometry, object->body);

		dMass mass;
		float density = dynamicsMass / (boxSize[0] * boxSize[1] * boxSize[2]);
		dMassSetBox(&mass, density, boxSize[0], boxSize[1], boxSize[2]);
		dBodySetMass(object->body, &mass);

		if (motion == IDynamics::T_Character)
		{
			// http://ode-wiki.org/wiki/index.php?title=HOWTO_upright_capsule
			object->isCharacter = true;
			dBodySetMaxAngularSpeed(object->body, 0);
		}

		// TODO: Deal with vehicle.
		// Vehicle supports wheels for driving or sliding.
		// TODO: Add support for vehicle parameters to the interface.

		dBodySetData(object->body, (void*)object);

		dynamicObjects.insert(object);
		dynamicObjectListValid = false;

		staticObject = object;
	}

	staticObject->SetOrientation(position);
	staticObject->SetUserData(userData);
	dGeomSetData(staticObject->geometry, (void*)staticObject);

	return staticObject;
}

void OdeDynamics::RemoveObject(IDynamicsObject *object)
{
	RemoveObject(object, true);
}

void OdeDynamics::RemoveObject(IDynamicsObject *object, bool erase)
{
	OdeStaticObject *sobject = dynamic_cast<OdeStaticObject*>(object);
	if (sobject != nullptr)
	{
		if (erase)
		{
			staticObjects.erase(sobject);
		}
		dGeomDestroy(sobject->geometry);
		DeleteGeometryData(sobject->geometryData);
	}

	OdeDynamicObject *dobject = dynamic_cast<OdeDynamicObject*>(object);
	if (dobject != nullptr)
	{
		if (erase)
		{
			dynamicObjects.erase(dobject);
		}
		dBodyDestroy(dobject->body);
	}

	delete sobject;
}

void OdeDynamics::AddStaticObjectProvider(IStaticObjectProvider *provider)
{
	staticObjectProviders.insert(std::make_pair(provider, std::vector<StaticObjectEntry>()));
}

void OdeDynamics::RemoveStaticObjectProvider(IStaticObjectProvider *provider)
{
	auto sop = staticObjectProviders.find(provider);
	if (sop == staticObjectProviders.end())
	{
		throw std::logic_error("Attempted to remove StaticObjectProvider which wasn't in simulation");
	}
	for (auto itEntry = sop->second.begin(); itEntry != sop->second.end(); ++itEntry)
	{
		sop->first->FreeObject(itEntry->odeObject->staticObject);
	}
	staticObjectProviders.erase(sop);
}

void OdeDynamics::Run(double time)
{
	// Get the volumes that the dynamic objects might move within.
	staticObjectVolumes.resize(0);
	for (auto it = dynamicObjects.begin(); it != dynamicObjects.end(); ++it)
	{
		// Clear onground/flags.
		(*it)->onGround = false;
		(*it)->onObject = nullptr;

		// Get possible area that the dynamic object could move within:
		dReal bounds[6];
		dGeomGetAABB((*it)->geometry, bounds);
		Box3F size(Vector3F(bounds[0], bounds[2], bounds[4]), Vector3F(bounds[1], bounds[3], bounds[5]));
		size.Expand(1.0f);

		Vector3F velocity;
		(*it)->GetVelocity(&velocity);
		size.Expand(velocity.Length() * time);

		// Merge the volumes (naively - it doesn't matter if they intersect)
		bool combined = false;
		for (int i = 0; i < staticObjectVolumes.size(); i += 1)
		{
			if (size.Intersects(staticObjectVolumes[i]))
			{
				staticObjectVolumes[i].Combine(size);
				combined = true;
				break;
			}
		}
		if (!combined)
		{
			staticObjectVolumes.push_back(size);
		}
	}
	// Get the static objects that overlap the volumes from all providers.
	std::vector<IStaticObjectProvider::IStaticObject*> staticObjects;
	for (auto itSop = staticObjectProviders.begin(); itSop != staticObjectProviders.end(); ++itSop)
	{
		// Remove static objects that haven't been intersected for a long time.
		for (auto itEntry = itSop->second.begin(); itEntry != itSop->second.end(); ++itEntry)
		{
			bool intersect = false;
			for (int i = 0; i < staticObjectVolumes.size(); i += 1)
			{
				if (staticObjectVolumes[i].Intersects(itEntry->odeObject->staticObject->GetStaticObjectData()->bounds))
				{
					intersect = true;
					break;
				}
			}
			if (intersect)
			{
				itEntry->framesBeforeFree = framesBeforeFreeingStaticObjects;
				continue;
			}

			itEntry->framesBeforeFree -= 1;
			if (itEntry->framesBeforeFree <= 0)
			{
				IStaticObjectProvider::IStaticObject *staticObject = itEntry->odeObject->staticObject;
				RemoveObject(itEntry->odeObject);
				itSop->first->FreeObject(staticObject);
				itEntry = itSop->second.erase(itEntry);
				if (itEntry == itSop->second.end())
				{
					break;
				}
			}
		}

		// Get new static objects from the provider.
		staticObjects.resize(0);
		(*itSop).first->GetObjectsForVolume(&staticObjectVolumes, &staticObjects);
		for (auto itObj = staticObjects.begin(); itObj != staticObjects.end(); ++itObj)
		{
			IStaticObjectProvider::StaticObjectData *data = (*itObj)->GetStaticObjectData();
			OdeStaticObject *odeObject = dynamic_cast<OdeStaticObject*>(AddObject(data->userData, data->orientation, IDynamics::T_Static, data->boxSize, data->shape));
			odeObject->staticObject = (*itObj);
			(*itSop).second.push_back(StaticObjectEntry(odeObject, framesBeforeFreeingStaticObjects));
		}
	}

	// Iterate the simulation.
	timeLeft += time;
	if (timeLeft > maxFrameTime)
	{
		timeLeft = maxFrameTime;
	}
	for (; timeLeft > 0; timeLeft -= stepSize)
	{
		dJointGroupEmpty(tempContactJointGroup);
		tempContacts.resize(0);
		dSpaceCollide(space, (void*)this, OdeDynamics::NearCallback);

		// TODO: Select with a parameter.
		// Higher precision:
		dWorldStep(world, stepSize);
		// Lower precision, faster with lots of objects:
		//dWorldQuickStep(world, stepSize);

		// Upright all capsule shapes.
		// http://ode-wiki.org/wiki/index.php?title=HOWTO_upright_capsule
		dMatrix3 identity;
		dRSetIdentity(identity);
		for (auto it = dynamicObjects.begin(); it != dynamicObjects.end(); ++it)
		{
			if (!(*it)->isCharacter)
			{
				continue;
			}
			dBodySetRotation((*it)->body, identity);
		}
	}
}

// Bound collision callback. (static)
void OdeDynamics::NearCallback(void *data, dGeomID geom1, dGeomID geom2)
{
	OdeDynamics *ode = (OdeDynamics*)data;

    dBodyID body1 = dGeomGetBody(geom1);
    dBodyID body2 = dGeomGetBody(geom2);

	OdeDynamicObject *dyn1 = nullptr;
	OdeDynamicObject *dyn2 = nullptr;
	if (body1 != nullptr)
	{
		dyn1 = (OdeDynamicObject*)dBodyGetData(body1);
	}
	if (body2 != nullptr)
	{
		dyn2 = (OdeDynamicObject*)dBodyGetData(body2);
	}

	const int MAX_CONTACTS = 64;
	dContactGeom contactGeoms[MAX_CONTACTS];
	int numContacts = dCollide(geom1, geom2, MAX_CONTACTS, contactGeoms, sizeof(dContactGeom));
	if (numContacts == MAX_CONTACTS)
	{
		std::cout << "MAX_CONTACTS filled" << std::endl;
	}
	if (numContacts < 1)
	{
		return;
	}
	OdeDynamicObject *contactDyn1 = nullptr;
	OdeDynamicObject *contactDyn2 = nullptr;
	for (int i = 0; i < numContacts; i += 1)
	{
		// Convention: if geometry 1 is moved by depth along normal, the two objects will no longer intersect.
		dContact contact;
		contact.surface.mode = dContactBounce | dContactSoftCFM;
		contact.surface.mu = 1.0; // between 0 friction and infinite friction
		contact.surface.mu2 = 0;
		contact.surface.bounce = 0.01;
		contact.surface.bounce_vel = 0.1;
		contact.surface.soft_cfm = 0.01;
		contact.geom = contactGeoms[i];

		// TODO: See if we can guarantee that dCollide will always put the geometries into dContactGeom in the order they were passed.
		if (geom1 == contact.geom.g1)
		{
			contactDyn1 = dyn1;
			contactDyn2 = dyn2;
		}
		else
		{
			contactDyn1 = dyn2;
			contactDyn2 = dyn1;
		}

		// Onground check for any object.
		if (contactDyn1 != nullptr && contact.geom.normal[2] > ode->onGroundGradient)
		{
			contactDyn1->onGround = true;
			contactDyn1->groundNormal.Set(contact.geom.normal[0], contact.geom.normal[1], contact.geom.normal[2]);
			if (contactDyn2 != nullptr)
			{
				contactDyn1->onObject = contactDyn2;
			}
		}
		if (contactDyn2 != nullptr && contact.geom.normal[2] < -ode->onGroundGradient)
		{
			contactDyn2->groundNormal.Set(-contact.geom.normal[0], -contact.geom.normal[1], -contact.geom.normal[2]);
			contactDyn2->onGround = true;
			if (contactDyn1 != nullptr)
			{
				contactDyn2->onObject = contactDyn1;
			}
		}

		dContact *pc = ode->tempContacts.push_back(contact);
		dJointID j = dJointCreateContact(ode->world, ode->tempContactJointGroup, pc);
		// If one object is static, its body id will be zero.
		// This is the correct parameter to attach a body to the static environment.
		dJointAttach(j, dGeomGetBody(contact.geom.g1), dGeomGetBody(contact.geom.g2));
	}

	if (ode->collisionCallback)
	{
		ode->collisionCallback(contactDyn1, contactDyn2);
	}
}

const std::vector<IDynamicsObject*>* OdeDynamics::Objects()
{
	if (!dynamicObjectListValid || !staticObjectListValid)
	{
		objectList.clear();
		for (auto it = dynamicObjects.begin(); it != dynamicObjects.end(); ++it)
		{
			objectList.push_back((*it));
		}
		for (auto it = staticObjects.begin(); it != staticObjects.end(); ++it)
		{
			objectList.push_back((*it));
		}
		dynamicObjectListValid = true;
	}

	return &objectList;
}

void OdeDynamics::SetProperty(std::string name, double value)
{
	if (!name.compare("maxFrameTime"))
	{
        maxFrameTime = value;
	}
	// TODO: Our other config ought to go here too.
}

//-----------------------------------------------------------------------------
// Helpers
//-----------------------------------------------------------------------------

static void DeleteGeometryData(OdeGeometryData *geometryData)
{
	delete geometryData;
}

static void BuildObjectGeometry(OdeStaticObject *object, dWorldID world, dSpaceID space, IDynamics::MotionType motion, DynamicsShapeData *shape, Vector3F boxSize)
{
	if (motion == IDynamics::T_Character)
	{
		float radius = (boxSize[0] + boxSize[1]) * 0.5;
		float length = boxSize[2] - radius;
		if (radius <= 0 || length <= 0)
		{
			throw std::invalid_argument("Radius and length must be positive");
		}
		object->geometry = dCreateCapsule(space, radius, length);
		return;
	}

	DynamicsMeshData *mesh = dynamic_cast<DynamicsMeshData*>(shape);
	if (mesh != nullptr)
	{
		object->geometryData = new MeshGeometryData(world, mesh);
		object->geometry = object->geometryData->CreateGeometry(world, space);
		return;
	}

	DynamicsBrushData *brush = dynamic_cast<DynamicsBrushData*>(shape);
	if (brush != nullptr)
	{
		object->geometryData = new BrushGeometryData(world, brush);
		object->geometry = object->geometryData->CreateGeometry(world, space);
		return;
	}

	DynamicsHeightmapData *heightmap = dynamic_cast<DynamicsHeightmapData*>(shape);
	if (heightmap != nullptr)
	{
		if (heightmap->diagonals == nullptr)
		{
			object->geometryData = new HeightmapGeometryData(world, heightmap);
			object->geometry = object->geometryData->CreateGeometry(world, space);
			object->flipYZ = true;
			object->offset[0] = (float)(heightmap->heights->Width() - 1) * heightmap->tileSize * 0.5;
			object->offset[1] = (float)(heightmap->heights->Height() - 1) * heightmap->tileSize * 0.5;
			return;
		}
		else
		{
			object->geometryData = new MeshGeometryData(world, heightmap);
			object->geometry = object->geometryData->CreateGeometry(world, space);
			return;
		}
	}

	if (boxSize[0] <= 0 || boxSize[1] <= 0 || boxSize[2] <= 0)
	{
		throw std::invalid_argument("Box must have positive dimensions");
	}
	object->geometry = dCreateBox(space, boxSize[0], boxSize[1], boxSize[2]);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
