/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "dynamics/ode/OdeGeometryData.hh"
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

OdeGeometryData::~OdeGeometryData()
{
}

//-----------------------------------------------------------------------------
// Mesh
//-----------------------------------------------------------------------------

MeshGeometryData::MeshGeometryData(dWorldID world, DynamicsMeshData *mesh)
{
	throw std::logic_error("Not implemented");
}

MeshGeometryData::MeshGeometryData(dWorldID world, DynamicsHeightmapData *heightmap)
{
	int size = heightmap->diagonals->Width() * heightmap->diagonals->Height() * 6;
	verts = new dVector3[size];
	indices = new int[size];
	int numVerts = 0;
	int numIndices = 0;

	int xx[6], yy[6];

	for (int y = 0; y < heightmap->diagonals->Height(); y += 1)
	{
		for (int x = 0; x < heightmap->diagonals->Width(); x += 1)
		{
			// Note: The winding here is the opposite to what we gave to Bullet.
			if (heightmap->diagonals->at(x, y) == 0)
			{
				xx[0] = 0; yy[0] = 1;
				xx[1] = 0; yy[1] = 0;
				xx[2] = 1; yy[2] = 1;
				xx[3] = 1; yy[3] = 0;
				xx[4] = 1; yy[4] = 1;
				xx[5] = 0; yy[5] = 0;
			}
			else
			{
				xx[0] = 0; yy[0] = 0;
				xx[1] = 1; yy[1] = 0;
				xx[2] = 0; yy[2] = 1;
				xx[3] = 1; yy[3] = 1;
				xx[4] = 0; yy[4] = 1;
				xx[5] = 1; yy[5] = 0;
			}

			for (int i = 0; i < 6; i += 1)
			{
				verts[numVerts][0] = ((float)(x + xx[i]) * heightmap->tileSize);
				verts[numVerts][1] = ((float)(y + yy[i]) * heightmap->tileSize);
				verts[numVerts][2] = heightmap->heights->at(x + xx[i], y + yy[i]);
				verts[numVerts][3] = 0;
				indices[numIndices] = numVerts;
				numVerts += 1;
				numIndices += 1;
			}
		}
	}

	triMeshData = dGeomTriMeshDataCreate();

	// dGeomTriMeshDataBuild appears not to exist.
	// This method takes different parameters from what's documented.
	// To be fair, the interface is documented as unstable.
	dGeomTriMeshDataBuildSimple(triMeshData, (dReal*)verts, numVerts, (dTriIndex*)indices, numIndices);
}

MeshGeometryData::~MeshGeometryData()
{
	dGeomTriMeshDataDestroy(triMeshData);
	delete[] verts;
	delete[] indices;
}

dGeomID MeshGeometryData::CreateGeometry(dWorldID world, dSpaceID space)
{
	return dCreateTriMesh(space, triMeshData, nullptr, nullptr, nullptr);
}

//-----------------------------------------------------------------------------
// Brush
//-----------------------------------------------------------------------------

BrushGeometryData::BrushGeometryData(dWorldID world, DynamicsBrushData *brush)
{
	throw std::logic_error("Not implemented");
}

BrushGeometryData::~BrushGeometryData()
{
}

dGeomID BrushGeometryData::CreateGeometry(dWorldID world, dSpaceID space)
{
	return nullptr;
}

//-----------------------------------------------------------------------------
// Heightmap
//-----------------------------------------------------------------------------

HeightmapGeometryData::HeightmapGeometryData(dWorldID world, DynamicsHeightmapData *heightmap)
{
	shapeData = heightmap;

	if (!heightmap->IsValid())
	{
		throw std::domain_error("Heightmap data not valid");
	}

	Array2D<float> *interpolatedHeights = nullptr;
	const Array2D<float> *heights;
	float scale;
	if (heightmap->diagonals != nullptr)
	{
		// TODO: Move this method to DynamicsHeightmapData itself,
		// but then we'd require the class to handle ownership.

		// Build new height data that uses midpoints:
		// o = original vertex, h = horizontal average, v = vertical average, x = diagonal average depending on diagonality
		// o h o h o h o  ...
		// v x v x v x v  ...
		// o h o h o h o  ...
		// ...
		interpolatedHeights = new Array2D<float>((heightmap->heights->Width() * 2) - 1, (heightmap->heights->Height() * 2) - 1);
		// Best way to interpolate is with one iteration through each array, basically streaming from one to the other.
		for (size_t y = 0; y < interpolatedHeights->Height(); y += 2)
		{
			size_t x;
			size_t ox, ox2;
			size_t oy = y >> 1;
			size_t oy2 = oy + 1;

			// copy/h-interpolate
			for (x = 0; x < interpolatedHeights->Width() - 1; x += 2)
			{
				ox = x >> 1;
				interpolatedHeights->at(x, y) = heightmap->heights->at(ox, oy);
				interpolatedHeights->at(x + 1, y) = (interpolatedHeights->at(x, y) + heightmap->heights->at(ox + 1, oy)) * 0.5f;
			}
			// copy the last element
			interpolatedHeights->at(x, y) = heightmap->heights->at(ox + 1, oy);

			if (y == interpolatedHeights->Height() - 1) { break; }
			size_t y2 = y + 1;

			// v-interpolate/diagonal
			for (x = 0; x < interpolatedHeights->Width() - 1; x += 2)
			{
				ox = x >> 1;
				ox2 = ox + 1;
				interpolatedHeights->at(x, y2) = (interpolatedHeights->at(x, y) + heightmap->heights->at(ox, oy2)) * 0.5f;
				if (heightmap->diagonals->at(ox, oy) == 0)
				{
					interpolatedHeights->at(x + 1, y2) = (interpolatedHeights->at(x, y) + heightmap->heights->at(ox2, oy2)) * 0.5f;
				}
				else
				{
					interpolatedHeights->at(x + 1, y2) = (heightmap->heights->at(ox2, oy) + heightmap->heights->at(ox, oy2)) * 0.5f;
				}
			}
			// v-interpolate the last element
			interpolatedHeights->at(x, y2) = (interpolatedHeights->at(x, y) + heightmap->heights->at(ox + 1, oy2)) * 0.5f;
		}

		heights = interpolatedHeights;
		scale = heightmap->tileSize * 0.5f;
	}
	else
	{
		heights = heightmap->heights;
		scale = heightmap->tileSize;
	}

	heightfieldData = dGeomHeightfieldDataCreate();
	dGeomHeightfieldDataBuildSingle(
		heightfieldData,
		heights->RawElements(),
		1, // copy the data we've been given, allowing it to be destroyed
		scale * (dReal)(heights->Width() - 1),
		scale * (dReal)(heights->Height() - 1),
		heights->Width(),
		heights->Height(),
		1, // vertical scale
		0, // no offset
		0.1f, // 1.0f units padding to bounding box underneath
		0 // no infinite wrapping
	);
	dGeomHeightfieldDataSetBounds(heightfieldData, heightmap->minHeight, heightmap->maxHeight);

	// Tidy up after ourselves.
	delete interpolatedHeights;
}

HeightmapGeometryData::~HeightmapGeometryData()
{
	dGeomHeightfieldDataDestroy(heightfieldData);
}

dGeomID HeightmapGeometryData::CreateGeometry(dWorldID world, dSpaceID space)
{
	return dCreateHeightfield(space, heightfieldData, 1);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
