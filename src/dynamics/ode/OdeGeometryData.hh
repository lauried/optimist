/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#ifndef _DYNAMICS_ODE_ODEGEOMETRYDATA_HH_
#define _DYNAMICS_ODE_ODEGEOMETRYDATA_HH_
//-----------------------------------------------------------------------------
#include "dynamics/ode/Ode.hh"
#include "dynamics/DynamicsMeshData.hh"
#include "dynamics/DynamicsBrushData.hh"
#include "dynamics/DynamicsHeightmapData.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class OdeGeometryData {
public:
	OdeGeometryData() : shapeData(nullptr) {}
	virtual ~OdeGeometryData();
	virtual dGeomID CreateGeometry(dWorldID world, dSpaceID space) = 0;
	DynamicsShapeData *GetShapeData() { return shapeData; }

protected:
	DynamicsShapeData *shapeData;
};

class MeshGeometryData : public OdeGeometryData {
public:
	MeshGeometryData(dWorldID world, DynamicsMeshData *mesh);
	MeshGeometryData(dWorldID world, DynamicsHeightmapData *heightmap);
	~MeshGeometryData();
	dGeomID CreateGeometry(dWorldID world, dSpaceID space);

private:
	dTriMeshDataID triMeshData;
	dVector3 *verts;
	int *indices;
};

class BrushGeometryData : public OdeGeometryData {
public:
	BrushGeometryData(dWorldID world, DynamicsBrushData *brush);
	~BrushGeometryData();
	dGeomID CreateGeometry(dWorldID world, dSpaceID space);
};

class HeightmapGeometryData : public OdeGeometryData {
public:
	HeightmapGeometryData(dWorldID world, DynamicsHeightmapData *heightmap);
	~HeightmapGeometryData();
	dGeomID CreateGeometry(dWorldID world, dSpaceID space);

private:
	dHeightfieldDataID heightfieldData;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
#endif // _DYNAMICS_ODE_ODEGEOMETRYDATA_HH_
//-----------------------------------------------------------------------------

