/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#ifndef _DYNAMICS_ODE_ODEOBJECT_HH_
#define _DYNAMICS_ODE_ODEOBJECT_HH_
//-----------------------------------------------------------------------------
#include "dynamics/ode/Ode.hh"
#include "dynamics/ode/OdeGeometryData.hh"
#include "dynamics/IDynamicsObject.hh"
#include "dynamics/IStaticObjectProvider.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class OdeObject : public IDynamicsObject {
public:
	IDynamicsUserData *userData;

	OdeObject() : userData(nullptr) {}
	virtual ~OdeObject();

	// IDynamicsObject (incomplete; we're abstract)
	void SetUserData(IDynamicsUserData *data) { userData = data; }
	IDynamicsUserData* GetUserData() { return userData; }
};

class OdeStaticObject : public OdeObject {
public:
	IStaticObjectProvider::IStaticObject *staticObject;
	OdeGeometryData *geometryData;
	dGeomID geometry;
	bool flipYZ; // just used for terrain position and orientation.
	Vector3F offset;

	OdeStaticObject() : OdeObject(), staticObject(nullptr), geometryData(nullptr), geometry(0), flipYZ(false) {}
	~OdeStaticObject();

	// IDynamicObject (no effect; we're static)
	void SetOrientation(OrientationF &src);
	void GetOrientation(OrientationF *dest);
	// Dynamic stuff does nothing.
	void SetVelocity(Vector3F &src) {}
	void GetVelocity(Vector3F *dest) {}
	void SetAngularVelocity(Vector3F &src) {}
	void GetAngularVelocity(Vector3F *dest) {}
	void ApplyForce(Vector3F& position, Vector3F& force) {}
	void ApplyForceRelative(Vector3F& position, Vector3F& force) {}
	bool IsOnGround() { return false; }
	void GetGroundNormal(Vector3F *dest) {}
	void SetGravity(bool gravity) {}
};

class OdeDynamicObject : public OdeStaticObject {
public:
	dBodyID body;
	bool isCharacter;
	bool onGround;
	Vector3F groundNormal;
	OdeDynamicObject *onObject;

	OdeDynamicObject() : OdeStaticObject(), body(0), isCharacter(false), onGround(false), onObject(nullptr) {}
	~OdeDynamicObject();

	// IDynamicObject
	void SetOrientation(OrientationF &src);
	void GetOrientation(OrientationF *dest);
	void SetVelocity(Vector3F &src);
	void GetVelocity(Vector3F *dest);
	void SetAngularVelocity(Vector3F &src);
	void GetAngularVelocity(Vector3F *dest);
	void ApplyForce(Vector3F& position, Vector3F& force);
	void ApplyForceRelative(Vector3F& position, Vector3F& force);
	bool IsOnGround() { return onGround; }
	void GetGroundNormal(Vector3F *dest);
	void SetGravity(bool gravity);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
#endif // _DYNAMICS_ODE_ODEOBJECT_HH_
//-----------------------------------------------------------------------------

