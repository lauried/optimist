/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "dynamics/ode/OdeObject.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// OdeObject
//-----------------------------------------------------------------------------

OdeObject::~OdeObject()
{
}

//-----------------------------------------------------------------------------
// OdeStaticObject
//-----------------------------------------------------------------------------

OdeStaticObject::~OdeStaticObject()
{
}

void OdeStaticObject::SetOrientation(OrientationF &src)
{
	if (!flipYZ)
	{
		dGeomSetPosition(geometry, src.origin[0], src.origin[1], src.origin[2]);
		dMatrix3 rotation = {
			src.v[0][0], src.v[0][1], src.v[0][2], 0,
			src.v[1][0], src.v[1][1], src.v[1][2], 0,
			src.v[2][0], src.v[2][1], src.v[2][2], 0,
		};
		dGeomSetRotation(geometry, rotation);
	}
	else
	{
		// This mode is for heightmaps.
		// They're oriented with Y = up, so we just swap their right and up axes.
		dGeomSetPosition(geometry, src.origin[0] + offset[0], src.origin[1] + offset[1], src.origin[2] + offset[2]);
		dMatrix3 rotation = {
			src.v[0][0], src.v[0][1], src.v[0][2], 0,
			src.v[2][0], src.v[2][1], src.v[2][2], 0,
			src.v[1][0], src.v[1][1], src.v[1][2], 0,
		};
		dGeomSetRotation(geometry, rotation);
	}
}

void OdeStaticObject::GetOrientation(OrientationF *dest)
{
	const dReal *pos = dGeomGetPosition(geometry);
	const dReal *rot = dGeomGetRotation(geometry);

	if (!flipYZ)
	{
		dest->origin.Set(pos[0], pos[1], pos[2]);
		dest->v[0].Set(rot[0], rot[1], rot[2]);
		dest->v[1].Set(rot[4], rot[5], rot[6]);
		dest->v[2].Set(rot[8], rot[9], rot[10]);
	}
	else
	{
		dest->origin.Set(pos[0] - offset[0], pos[1] - offset[1], pos[2] - offset[2]);
		dest->v[0].Set(rot[0], rot[1], rot[2]);
		dest->v[2].Set(rot[8], rot[9], rot[10]);
		dest->v[1].Set(rot[4], rot[5], rot[6]);
	}
}

//-----------------------------------------------------------------------------
// OdeDynamicObject
//-----------------------------------------------------------------------------

OdeDynamicObject::~OdeDynamicObject()
{
}

void OdeDynamicObject::SetOrientation(OrientationF &src)
{
	dBodySetPosition(body, src.origin[0], src.origin[1], src.origin[2]);
	dMatrix3 rotation = {
		src.v[0][0], src.v[0][1], src.v[0][2], 0,
		src.v[1][0], src.v[1][1], src.v[1][2], 0,
		src.v[2][0], src.v[2][1], src.v[2][2], 0,
	};
	dBodySetRotation(body, rotation);
}

void OdeDynamicObject::GetOrientation(OrientationF *dest)
{
	const dReal *pos = dBodyGetPosition(body);
	const dReal *rot = dBodyGetRotation(body);
	dest->origin.Set(pos[0], pos[1], pos[2]);
	dest->v[0].Set(rot[0], rot[1], rot[2]);
	dest->v[1].Set(rot[4], rot[5], rot[6]);
	dest->v[2].Set(rot[8], rot[9], rot[10]);
}

void OdeDynamicObject::SetVelocity(Vector3F &src)
{
	dBodySetLinearVel(body, src[0], src[1], src[2]);
}

void OdeDynamicObject::GetVelocity(Vector3F *dest)
{
	const dReal *vel = dBodyGetLinearVel(body);
	dest->Set(vel[0], vel[1], vel[2]);
}

void OdeDynamicObject::SetAngularVelocity(Vector3F &src)
{
	dBodySetAngularVel(body, src[0], src[1], src[2]);
}

void OdeDynamicObject::GetAngularVelocity(Vector3F *dest)
{
	const dReal *vel = dBodyGetAngularVel(body);
	dest->Set(vel[0], vel[1], vel[2]);
}

void OdeDynamicObject::ApplyForce(Vector3F& position, Vector3F& force)
{
	dBodyAddForceAtPos(body, force[0], force[1], force[2], position[0], position[1], position[2]);
}

void OdeDynamicObject::ApplyForceRelative(Vector3F& position, Vector3F& force)
{
	dBodyAddRelForceAtRelPos(body, force[0], force[1], force[2], position[0], position[1], position[2]);
}

void OdeDynamicObject::GetGroundNormal(Vector3F *dest)
{
	dest->Set(groundNormal);
}

void OdeDynamicObject::SetGravity(bool gravity)
{
	dBodySetGravityMode(body, (int)gravity);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
