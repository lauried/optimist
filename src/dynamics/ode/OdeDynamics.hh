/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#ifndef _DYNAMICS_ODE_ODEDYNAMICS_HH_
#define _DYNAMICS_ODE_ODEDYNAMICS_HH_
//-----------------------------------------------------------------------------
#include <set>
#include <map>
#include <vector>
#include "dynamics/ode/Ode.hh"
#include "dynamics/ode/OdeObject.hh"
#include "dynamics/IDynamics.hh"
#include "library/geometry/Vector3.hh"
#include "library/containers/UnorderedPair.hh"
#include "library/containers/PagedArray.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class OdeDynamics : public IDynamics {
public:
	OdeDynamics();
	~OdeDynamics();

	// Own methods
	void SetGravity(Vector3F gravity);
	// TODO: Other configuration stuff like scaling factor and ODE specific world config.

	// IDynamics
	IDynamicsObject* AddObject(
		IDynamicsUserData *userData,
		OrientationF &position,
		IDynamics::MotionType motion,
		Vector3F boxSize,
		DynamicsShapeData *shape = nullptr,
		float dynamicsMass = 1
	);
	void RemoveObject(IDynamicsObject *object);
	void AddStaticObjectProvider(IStaticObjectProvider *provider);
	void RemoveStaticObjectProvider(IStaticObjectProvider *provider);
	void Run(double time);
	const std::vector<IDynamicsObject*>* Objects();
	void SetProperty(std::string name, double value);
	void SetCollisionCallback(std::function<void(IDynamicsObject*, IDynamicsObject*)> callback) { collisionCallback = callback; }

private:
	dWorldID world;
	dSpaceID space;

	// Config.
	dReal stepSize;
	int framesBeforeFreeingStaticObjects;
	float onGroundGradient;
	double maxFrameTime;

	// Time remainder used for stepping the world.
	double timeLeft;

	std::set<OdeStaticObject*> staticObjects;
	std::set<OdeDynamicObject*> dynamicObjects;
	struct StaticObjectEntry {
		OdeStaticObject *odeObject;
		int framesBeforeFree;
		StaticObjectEntry();
		StaticObjectEntry(OdeStaticObject *odeObject, int framesBeforeFree) : odeObject(odeObject), framesBeforeFree(framesBeforeFree) {}
	};
	std::map<IStaticObjectProvider*, std::vector<StaticObjectEntry>> staticObjectProviders;

	bool dynamicObjectListValid;
	bool staticObjectListValid;
	std::vector<IDynamicsObject*> objectList;
	std::function<void(IDynamicsObject*,IDynamicsObject*)> collisionCallback;

	std::vector<Box3F> staticObjectVolumes;

	dJointGroupID tempContactJointGroup;
	PagedArray<dContact> tempContacts;
	static void NearCallback(void *data, dGeomID geom1, dGeomID geom2);

	void RemoveObject(IDynamicsObject *object, bool erase);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
#endif // _DYNAMICS_ODE_ODEDYNAMICS_HH_
//-----------------------------------------------------------------------------

