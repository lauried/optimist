/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "library/containers/Array2D.hh"
#include "dynamics/DynamicsShapeData.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * There are two use cases for this type of data:
 * 1. Instances of models where the model remains cached anyway.
 * 2. Instances from a static object provider, where the dynamics would tell
 *    the provider that the instance is no longer needed.
 * In both cases this data is owned externally.
 */
class DynamicsHeightmapData : public DynamicsShapeData {
public:
	float tileSize;
	float minHeight, maxHeight;
	const Array2D<float> *heights;
	const Array2D<char> *diagonals; ///< 0 = divided positive to negative, 1 = pn to np, optional.

	DynamicsHeightmapData() : tileSize(1), heights(nullptr), diagonals(nullptr) {}

	~DynamicsHeightmapData();

	// TODO: Method to convert diagonals to double resolution height data.
	// We'd have to track ownership of data.

	/**
	 * Returns true if the data is internally consistent, else false.
	 * The data is consistent if:
	 * - tileSize is positive.
	 * - height data is present and has positive dimensions.
	 * - if there is diagonals data it matches the height data's dimensions.
	 */
	bool IsValid()
	{
		if (tileSize <= 0)
		{
			return false;
		}
		if (heights == nullptr)
		{
			return false;
		}
		if (heights->Width() <= 0 || heights->Height() <= 0)
		{
			return false;
		}
		if (diagonals != nullptr && (heights->Width() - 1 != diagonals->Width() || heights->Height() - 1 != diagonals->Height()))
		{
			return false;
		}
		return true;
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
