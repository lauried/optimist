/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <vector>
#include <limits>
#include "library/geometry/Plane3.hh"
#include "library/geometry/Vector3.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Return information structure for tracing/colliding against any
 * traceable objects.
 * @deprecated
 */
struct TraceInfo {
	int surfaceType;
	int entity;
	Vector3F normal;
};

/**
 * Base class for a brush - a convex object described by outward facing planes.
 * It's for physics to trace against.
 */
class Brush {
public:
	// TODO: This version of the function is deprecated.
	virtual float TraceBox(
		const Vector3F &p1,
		const Vector3F &p2,
		const Box3F &box,
		int flags,
		TraceInfo *traceInfo
	)
	{
		float lastEntry = -std::numeric_limits<float>::max(); // where we enter the brush
		float firstLeave = std::numeric_limits<float>::max(); // where we leave the brush
		Vector3F boxCorner;
		for (unsigned int i = 0; i < planes.size(); ++i)
		{
			float offset = BoxExpandPlaneAmount(planes[i], box);
			float direction;
			float fraction = planes[i].Intersects(p1, p2, offset, &direction);

			if (direction < 0)
			{
				if (fraction > lastEntry)
				{
					traceInfo->normal.Set(planes[i].normal);
					lastEntry = fraction;
				}
			}
			else
			{
				if (fraction < firstLeave)
				{
					firstLeave = fraction;
				}
			}
		}
		if (firstLeave < lastEntry)
			return std::numeric_limits<float>::max();
		return lastEntry;
	}

	virtual float TraceBox(
		const Vector3F &p1,
		const Vector3F &p2,
		const Box3F &box,
		Vector3F *normal
	)
	{
		float lastEntry = -std::numeric_limits<float>::max(); // where we enter the brush
		float firstLeave = std::numeric_limits<float>::max(); // where we leave the brush
		Vector3F boxCorner;
		for (unsigned int i = 0; i < planes.size(); ++i)
		{
			float offset = BoxExpandPlaneAmount(planes[i], box);
			float direction;
			float fraction = planes[i].Intersects(p1, p2, offset, &direction);

			if (direction < 0)
			{
				if (fraction > lastEntry)
				{
					normal->Set(planes[i].normal);
					lastEntry = fraction;
				}
			}
			else
			{
				if (fraction < firstLeave)
				{
					firstLeave = fraction;
				}
			}
		}
		if (firstLeave < lastEntry)
			return std::numeric_limits<float>::max();
		return lastEntry;
	}

protected:
	static float BoxExpandPlaneAmount(const Plane3F &plane, const Box3F &box)
	{
		Vector3F boxCorner;
		for (int axis = 0; axis < 3; ++axis)
		{
			if (plane.normal.v[axis] > 0)
				boxCorner[axis] = box.mins.v[axis];
			else
				boxCorner[axis] = box.maxs.v[axis];
		}
		return 0 - plane.normal.DotProduct(boxCorner);
	}

	std::vector<Plane3F> planes;
};

/**
 * A brush created from a bounding box.
 */
class BoxBrush : public Brush {
public:
	BoxBrush() { planes.resize(6); }

	void Set(Vector3F &origin, Box3F &box)
	{
		for (int axis = 0; axis < 3; ++axis)
		{
			planes[axis*2  ].normal.Set(0, 0, 0);
			planes[axis*2  ].normal[axis] =  1.0f;
			planes[axis*2  ].distance = origin[axis] + box.maxs[axis];

			planes[axis*2+1].normal.Set(0, 0, 0);
			planes[axis*2+1].normal[axis] = -1.0f;
			planes[axis*2+1].distance = -(origin[axis] + box.mins[axis]);
		}
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

