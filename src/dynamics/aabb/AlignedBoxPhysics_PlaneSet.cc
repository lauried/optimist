/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "dynamics/aabb/AlignedBoxPhysics.hh"
#include <limits>
#include <memory.h>
#include "library/maths/Maths.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// AlignedBoxPhysics::PlaneSet
//-----------------------------------------------------------------------------

AlignedBoxPhysics::PlaneSet::PlaneSet(PhysicsPlaneSet *planeSet, bool persist)
{
	planes = planeSet->planes;
	numPlanes = planeSet->numPlanes;
	ownPlanes = false;
	if (!persist)
	{
		planes = new Plane3F[numPlanes];
		for (int i = 0; i < numPlanes; ++i)
		{
			planes[i] = planeSet->planes[i];
			isTerrain = (planes[i].normal[2] > 0);
		}
		ownPlanes = true;
	}
}

AlignedBoxPhysics::PlaneSet::~PlaneSet()
{
	if (ownPlanes)
		delete[] planes;
}

void AlignedBoxPhysics::PlaneSet::TraceBox(
	const Vector3F &p1,
	const Vector3F &p2,
	const Box3F &box,
	const Box3F &worldMoveBounds,
	PhysicsTraceInfo *traceInfo
)
{
	float direction;
	for (int i = 0; i < numPlanes; ++i)
	{
		float offset = BoxExpandPlaneAmount(planes[i], box);
		float fraction = planes[i].Intersects(p1, p2, offset, &direction);
		if (direction == -1 && fraction < traceInfo->fraction)
		{
			traceInfo->fraction = fraction;
			traceInfo->normal = planes[i].normal;
		}
	}
}

float AlignedBoxPhysics::PlaneSet::BoxTerrainHeight(const Vector3F &pos, const Box3F &box)
{
	// TODO: Calculate the height of each plane at each point, adding on
	// boxexpandplaneamount to the distance.
	Vector3F start = pos;
	Vector3F end   = pos;
	start[2] =  65536.0f;
	end[2]   = -65536.0f;

	PhysicsTraceInfo info;
	info.fraction = 1.0f;
	TraceBox(start, end, box, box, &info);
	return start[2] + (end[2] - start[2]) * info.fraction;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
