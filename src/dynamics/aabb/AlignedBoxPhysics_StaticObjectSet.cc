/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "dynamics/aabb/AlignedBoxPhysics.hh"
#include <limits>
#include <memory.h>
#include "dynamics/aabb/Brush.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// AlignedBoxPhysics::StaticObjectSet
//-----------------------------------------------------------------------------

AlignedBoxPhysics::StaticObjectSet::StaticObjectSet(PhysicsStaticObject *objects, int number, bool persist)
{
	if (persist)
	{
		this->objects = objects;
		this->number  = number;
		this->owned   = false;
	}
	else
	{
		this->objects = new PhysicsStaticObject[number];
		this->number  = number;
		this->owned   = true;
		// Copy this way so it works with derived objects.
		// TODO: This makes no difference as the array means that no extra data
		// can be stored between the structs anwyay.
		for (int i = 0; i < number; ++i)
		{
			this->objects[i].orientation = objects[i].orientation;
			this->objects[i].bounds      = objects[i].bounds;
		}
	}

	// Set bounds to contain all the objects.
	this->worldBounds.mins.Set(std::numeric_limits<float>::max());
	this->worldBounds.maxs.Set(-std::numeric_limits<float>::max());
	for (int i = 0; i < this->number; ++i)
	{
		Vector3F mins = this->objects[i].orientation.origin + this->objects[i].bounds.mins;
		Vector3F maxs = this->objects[i].orientation.origin + this->objects[i].bounds.maxs;

		for (int axis = 0; axis < 3; ++axis)
		{
			if (mins[axis] < this->worldBounds.mins[axis])
				this->worldBounds.mins[axis] = mins[axis];

			if (maxs[axis] > this->worldBounds.maxs[axis])
				this->worldBounds.maxs[axis] = maxs[axis];
		}
	}
}

AlignedBoxPhysics::StaticObjectSet::~StaticObjectSet()
{
	if (owned)
		delete objects;
}

// Expects traceInfo to already be set up with the closest trace found, or
// FLT_MAX. Only updates traceInfo if we collided with something closer.
void AlignedBoxPhysics::StaticObjectSet::TraceBox(
	const Vector3F &p1,
	const Vector3F &p2,
	const Box3F &box,
	const Box3F &worldMoveBounds,
	PhysicsTraceInfo *traceInfo
)
{
	if (!worldBounds.Intersects(worldMoveBounds))
		return;

	BoxBrush brush;
	Vector3F normal;

	for (int i = 0; i < number; ++i)
	{
		if (!worldMoveBounds.Intersects(objects[i].bounds, objects[i].orientation.origin))
			continue;

		brush.Set(objects[i].orientation.origin, objects[i].bounds);
		float fraction = brush.TraceBox(p1, p2, box, &normal);
		if (fraction < traceInfo->fraction && fraction >= 0.0f)
		{
			traceInfo->fraction = fraction;
			traceInfo->normal = normal;
		}
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

