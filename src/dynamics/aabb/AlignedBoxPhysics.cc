/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "dynamics/aabb/AlignedBoxPhysics.hh"
#include <limits>
#include <iostream>
#include <memory.h>
#include "library/maths/Maths.hh"
#include "dynamics/aabb/Brush.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

inline void EpsilonVelocity(Vector3F &vel)
{
	const float STOP_EPSILON = 1.0f / 256.0f;
	for (int axis = 0; axis < 3; ++axis)
	{
		if (vel[axis] < STOP_EPSILON && vel[axis] > -STOP_EPSILON)
			vel[axis] = 0;
	}
}

inline float BoundedAdd(float val, float addition, float min, float max)
{
	if (max < min)
	{
		float tmp = max;
		max = min;
		min = tmp;
	}
	if (val > max) max = val;
	if (val < min) min = val;
	val += addition;
	if (val < min) val = min;
	if (val > max) val = max;
	return val;
}

inline void MakeSymmetricalBox(const Box3F in, Box3F &out)
{
	// Make out a copy of in, but make its sides symmetrical and centred
	// on its origin. That is, make each extent an average of its length and
	// breadth.
	float size = ((in.maxs[0] - in.mins[0]) + (in.maxs[1] - in.mins[1])) * 0.25;
	out.mins.Set(-size, -size, in.mins[2]);
	out.maxs.Set( size,  size, in.maxs[2]);
}

/**
 * Direction of force and position are in object coordinates.
 */
inline void ApplyForceToObject(AlignedBoxPhysicsObject *object, Vector3F& force, Vector3F& position, float interval)
{
	float scale = interval / object->Mass();
	object->Velocity() += object->Orientation().DirectionToExternalSpace(force) * scale;

	// Consider the plane which lies on the centre of the mass and a line in
	// the direction of force passing through the point at which it's applied.
	// Find the closest point to the centre of mass, as that's the distance we
	// use to calculate moment of torque.
	Vector3F torquePoint = Maths::ClosestPointOnLine(Vector3F(0, 0, 0), position, force);
	float torque = force.Length() * torquePoint.Length();

	// The axis of rotation is perpendicular to this plane. The values of the
	// axis are also conveniently the angular velocity values.
	// Moment of inertia is done for each of these components too.
	Vector3F axisOfRotation = position.CrossProduct(force); // TODO: Make sure this cross product is the right way round.
	axisOfRotation.Normalize();

	const float ONE_TWELFTH = 0.08333333333f;

	// This ignores offset centres of mass.
	Vector3F size = object->Bounds().maxs - object->Bounds().mins;
	Vector3F sizeSquared(size[0] * size[0], size[1] * size[1], size[2] * size[2]);
	Vector3F momentOfInertia(
		ONE_TWELFTH * object->Mass() * (sizeSquared[1] + sizeSquared[2]),
		ONE_TWELFTH * object->Mass() * (sizeSquared[0] + sizeSquared[2]),
		ONE_TWELFTH * object->Mass() * (sizeSquared[0] + sizeSquared[1])
		);

	Vector3F &angularVelocity = object->AngularVelocity();
	angularVelocity[0] += (torque / momentOfInertia[0]) * axisOfRotation[0] * scale;
	angularVelocity[1] += (torque / momentOfInertia[1]) * axisOfRotation[1] * scale;
	angularVelocity[2] += (torque / momentOfInertia[2]) * axisOfRotation[2] * scale;
}

void AlignedBoxPhysics::Add(AlignedBoxPhysicsObject *obj)
{
	objects.insert(obj);
}

void AlignedBoxPhysics::Remove(AlignedBoxPhysicsObject *obj)
{
	auto it = objects.find(obj);
	if (it != objects.end())
	{
		objects.erase(it);
	}
}

void AlignedBoxPhysics::ObjectUpdated(AlignedBoxPhysicsObject *obj)
{
}

int AlignedBoxPhysics::AddStatic(PhysicsHeightMap *heightMap, bool persist, uint32_t collisionGroup)
{
	HeightMap *map = new HeightMap(heightMap, persist);
	map->collisionGroup = collisionGroup;
	int index = staticObjects.Allocate();
	staticObjects[index] = map;
	return index;
}

int AlignedBoxPhysics::AddStatic(PhysicsStaticObject *objectSet, int number, bool persist, uint32_t collisionGroup)
{
	StaticObjectSet *set = new StaticObjectSet(objectSet, number, persist);
	set->collisionGroup = collisionGroup;
	int index = staticObjects.Allocate();
	staticObjects[index] = set;
	return index;
}

int AlignedBoxPhysics::AddStatic(PhysicsPlaneSet *planeSet, bool persist, uint32_t collisionGroup)
{
	PlaneSet *planes = new PlaneSet(planeSet, persist);
	planes->collisionGroup = collisionGroup;
	int index = staticObjects.Allocate();
	staticObjects[index] = planes;
	return index;
}

void AlignedBoxPhysics::RemoveStatic(int id)
{
	Traceable *traceable = staticObjects[id];
	delete traceable;
	staticObjects.Free(id);
}

void AlignedBoxPhysics::Update(double interval)
{
	// Objects event need access to physics to be able to query their onground
	// status etc.
	for (auto it = objects.begin(); it != objects.end(); ++it)
	{
		AlignedBoxPhysicsObject *obj = *it;

		if (!obj->Moving() || !obj->Active())
			continue;

		Box3F symBox;
		MakeSymmetricalBox(obj->Bounds(), symBox);

		// On-ground check.
		bool onGround = false;
		{
			PhysicsTraceInfo onGroundTrace;
			Vector3F above(obj->Orientation().origin);
			Vector3F below(obj->Orientation().origin);
			above[2] += BACKOFF * 2;
			below[2] -= BACKOFF * 2;
			TraceBox(above, below, symBox, &onGroundTrace, obj, obj->CollisionMask());
			if (onGroundTrace.fraction <= 1.0f)
				onGround = true;
		}
		obj->OnGround() = onGround;

		// Gravity.
		// Walking objects just shouldn't slide on terrain unless it's
		// really steep.
		if (!(onGround && obj->PhysicsType() == AlignedBoxPhysicsObject::PT_CHARACTER))
		{
			obj->Velocity()[2] -= 9.81 * obj->Gravity() * interval;
		}

		// Buoyancy.
		{
			// Waterlevel must be within the bounds.
			float waterLevel = obj->WaterLevel();
			Vector3F origin = obj->Orientation().origin;
			Box3F box = obj->Bounds();
			origin[2]   += waterLevel;
			box.mins[2] -= waterLevel;
			box.maxs[2] -= waterLevel;
			// Negate gravity for fraction of box below waterlevel submerged...
			if (box.mins[2] < 0)
			{
				float submerged = (box.mins[2] + origin[2]) / box.mins[2];
				obj->Velocity()[2] += 9.81 * Maths::Bound(0.0f, submerged, 1.0f) * interval;
			}
			// Add upward thrust for fraction of box above waterlevel submerged...
			if (box.maxs[2] > 0)
			{
				float submerged = -origin[2] / box.maxs[2];
				obj->Velocity()[2] += 9.81 * Maths::Bound(0.0f, submerged, 1.0f) * interval;
			}
		}

		// Apply node forces.
		// TODO: All forces should be applied at once, after the calculation of
		// what they should be.
		int totalWheels = 0;
		float totalWheelSteering = 0;
		float totalWheelTorque = 0;
		float totalWheelBrakeTorque = 0;
		for (int i = 0; i < obj->MaxNodes(); i += 1)
		{
			AlignedBoxPhysicsObject::PhysicsNode &node = obj->NodeProperties(i);

			switch (node.type)
			{
			case AlignedBoxPhysicsObject::PhysicsNode::NT_NONE:
				break;
			case AlignedBoxPhysicsObject::PhysicsNode::NT_WHEEL:
				totalWheels += 1;
				// Normalize the wheel angle to the range -0.5 to 0.5.
				while (node.data.wheel.currentAngle > 0.5f)
				{ node.data.wheel.currentAngle -= 1.0f; }
				while (node.data.wheel.currentAngle < -0.5f)
				{ node.data.wheel.currentAngle += 1.0f; }
				if (node.position[0] > 0)
					totalWheelSteering += node.data.wheel.currentAngle;
				else
					totalWheelSteering -= node.data.wheel.currentAngle;
				totalWheelTorque += node.data.wheel.currentTorque;
				totalWheelBrakeTorque += node.data.wheel.currentBrakeTorque;
				break;
			case AlignedBoxPhysicsObject::PhysicsNode::NT_JET:
				{
					Vector3F position(node.position);
					Vector3F force(node.data.jet.currentDirection);
					force *= node.data.jet.currentThrust;
					ApplyForceToObject(obj, position, force, interval);
				}
				break;
			case AlignedBoxPhysicsObject::PhysicsNode::NT_FIN:
				{
					Vector3F position(node.position);
					Vector3F normal(node.data.fin.planeNormal);
					Vector3F axis(node.data.fin.rotationAxis);

					normal = normal.RotateAroundVector(axis, Maths::TauToRadians(node.data.fin.currentAngle));
					Vector3F flatVelocity = normal * normal.DotProduct(obj->Velocity());
					Vector3F perpendicularVelocity = obj->Velocity() - flatVelocity;

					Vector3F force = flatVelocity * node.data.fin.flatDrag * -1;
					force += perpendicularVelocity * node.data.fin.edgeDrag * -1;

					ApplyForceToObject(obj, position, force, interval);
				}
				break;
			}
		}

		// Wheels could perform their forces individually, but there's no point
		// with aligned boxes.
		if (totalWheels > 0 && onGround)
		{
			float wheelSteering = totalWheelSteering / (float)totalWheels;
			float wheelTorque = totalWheelTorque / (float)totalWheels;
			float wheelBrakeTorque = totalWheelBrakeTorque / (float)totalWheels;

			const float turnSpeedRamp = 8.0f;
			const float maxTurnSpeed = 1.0f;
			const float gripThreshold = 1.0f;
			const float maxForwardSpeed = 12.0f;
			const float maxReverseSpeed = -5.0f;
			const float acceleration = 8.0f;
			const float braking = 11.0f;

			// Convert torque and braking.
			// Note: Bullet implementation will be more correct, so this is the
			// code that should be adapted to match.
			wheelTorque = Maths::Bound(-1.0f, wheelTorque * 0.1f, 1.0f);
			// Braking is just a boolean.

			Vector3F relativeVelocity = obj->Orientation().DirectionToInternalSpace(obj->Velocity());
			bool grip = (relativeVelocity[1] > -gripThreshold && relativeVelocity[1] < gripThreshold);

			// Turn based on our forward speed.
			float desiredAngularVelocity = 0.0f;
			float turning = 1.0f;
			if (wheelSteering)
			{
				desiredAngularVelocity = maxTurnSpeed * wheelSteering * -1 * std::min(relativeVelocity[0] / turnSpeedRamp, 1.0f);
			}
			else
			{
				desiredAngularVelocity = 0.0f;
				turning = grip ? 1.0f : 0.125f;
			}
			{
				float delta = desiredAngularVelocity - obj->AngularVelocity()[2];
				float turnPower = interval;
				if ((delta < 0 && obj->AngularVelocity()[2] < 0) || (delta > 0 && obj->AngularVelocity()[2] > 0))
				{
					turnPower *= turning * grip ? 1.0f : 0.25f;
				}
				if (delta < -turnPower) delta = -turnPower;
				if (delta >  turnPower) delta =  turnPower;
				obj->AngularVelocity()[2] += delta;
			}

			// Lateral friction.
			float lateralFriction = grip ? 1.0f : 0.015f;
			obj->Velocity() -= obj->Orientation().v[1] * relativeVelocity[1] * lateralFriction;

			// Forward friction - braking/acceleration.
			float forwardFriction = grip ? 1.0f : 0.25f;
			if (wheelBrakeTorque)
			{
				float force = braking * interval * forwardFriction;
				if (relativeVelocity[0] > 0)
				{
					force *= -1;
				}
				float newForwardSpeed = BoundedAdd(relativeVelocity[0], force, maxReverseSpeed, maxForwardSpeed);
				obj->Velocity() += obj->Orientation().v[0] * (newForwardSpeed - relativeVelocity[0]);
			}
			if (wheelTorque)
			{
				float force = wheelTorque * acceleration * interval * forwardFriction;
				float newForwardSpeed = BoundedAdd(relativeVelocity[0], force, maxReverseSpeed, maxForwardSpeed);
				obj->Velocity() += obj->Orientation().v[0] * (newForwardSpeed - relativeVelocity[0]);
			}
		}

		// Pre movement event
		//OBSERVABLE_EVENT(observer->OnPrePhysics(obj, interval))

		// Movement
		float moveTime = interval;
		for (int j = 0; j < 4 && moveTime > 0; ++j)
		{
			Vector3F movement = obj->Velocity() * moveTime;
			PhysicsTraceInfo traceInfo;
			TraceBox(
				obj->Orientation().origin,
				obj->Orientation().origin + movement,
				symBox,
				&traceInfo,
				obj,
				obj->CollisionMask()
			);
			float frac = traceInfo.fraction;

			if (frac >= 1.0)
			{
				obj->Orientation().origin += movement;
				moveTime = 0;
			}
			else
			{
				// clip velocity
				float dot = -1 * traceInfo.normal.DotProduct(obj->Velocity());
				obj->Velocity() += traceInfo.normal * (dot + BACKOFF);
				// clip motion & back away from surface (upward from terrain)
				if (traceInfo.staticId >= 0 && staticObjects[traceInfo.staticId]->IsTerrain())
				{
					traceInfo.normal.Set(0, 0, 1);
				}
				obj->Orientation().origin += (movement * frac) + (traceInfo.normal * BACKOFF);
				// update time
				moveTime *= 1 - frac;

				if (traceInfo.object)
				{
					//OBSERVABLE_EVENT(observer->OnCollide(obj, traceInfo.object))
				}
			}
		}

		{
			// make sure we're never in terrain
			// this is a hack to cover up the fact that we sometimes don't
			// collide properly
			float height = BoxTerrainHeight(obj->Orientation().origin, symBox, obj->CollisionMask()) + BACKOFF;
			if (obj->Orientation().origin[2] < height)
			{
				obj->Orientation().origin[2] = height;
			}
		}

		// Recalculate onGround post-move.
		Vector3F groundNormal;
		{
			PhysicsTraceInfo onGroundTrace;
			Vector3F above(obj->Orientation().origin);
			Vector3F below(obj->Orientation().origin);
			above[2] += BACKOFF * 2;
			below[2] -= BACKOFF * 2;
			TraceBox(above, below, symBox, &onGroundTrace, obj, obj->CollisionMask());
			if (onGroundTrace.fraction <= 1.0f)
				onGround = true;
			groundNormal = onGroundTrace.normal;
		}
		obj->OnGround() = onGround;

		// Set onGround state for all wheels.
		for (int i = 0; i < obj->MaxNodes(); i += 1)
		{
			AlignedBoxPhysicsObject::PhysicsNode &node = obj->NodeProperties(i);
			if (node.type == AlignedBoxPhysicsObject::PhysicsNode::NT_WHEEL) {
				// Fully compressed if onground, else fully extended.
				node.data.wheel.currentPosition = (onGround) ? 0.0f : 1.0f;
			}
		}

		if (obj->PhysicsType() == AlignedBoxPhysicsObject::PT_VEHICLE) {
			// Conform to terrain.
			if (onGround)
			{
				Vector3F normalCraftSpace = obj->Orientation().DirectionToInternalSpace(groundNormal);
				float roll = atan(normalCraftSpace[1]);
				float pitch = -atan(normalCraftSpace[0]);
				float maxRoll = 1;
				Maths::Bound(-maxRoll, roll, maxRoll);
				Maths::Bound(-maxRoll, pitch, maxRoll);
				obj->AngularVelocity()[0] = roll * TERRAIN_CONFORM_SPEED;
				obj->AngularVelocity()[1] = pitch * TERRAIN_CONFORM_SPEED;
			}

			// Try to self-right when floating in water.
			if (!onGround && obj->Orientation().origin[2] + obj->Bounds().mins[2] <= 0.0f) {
				Vector3F normalCraftSpace = obj->Orientation().DirectionToInternalSpace(Vector3F(0.0f, 0.0f, 1.0f));
				float roll = atan(normalCraftSpace[1]);
				float pitch = -atan(normalCraftSpace[0]);
				float maxRoll = 1;
				Maths::Bound(-maxRoll, roll, maxRoll);
				Maths::Bound(-maxRoll, pitch, maxRoll);
				obj->AngularVelocity()[0] = roll * TERRAIN_CONFORM_SPEED;
				obj->AngularVelocity()[1] = pitch * TERRAIN_CONFORM_SPEED;
			}
		}

		// Angular velocity.
		obj->Orientation().Rotate(obj->AngularVelocity() * interval);

		// TODO: Air/water friction.

		// Post movement event.
		//OBSERVABLE_EVENT(observer->OnPostPhysics(obj, interval))
	}
}

// Exclude parameter is just used in pointer comparison, so setting to null
// will exclude nothing.
void AlignedBoxPhysics::TraceBox(
	const Vector3F &p1,
	const Vector3F &p2,
	const Box3F &box,
	PhysicsTraceInfo *traceInfo,
	const AlignedBoxPhysicsObject *exclude,
	uint32_t collisionMask
)
{
	traceInfo->fraction = std::numeric_limits<float>::max();

	// Always use the centre of the box for the real physics, this stops some
	// issues.
	Vector3F offset = (box.mins + box.maxs) * 0.5;
	Vector3F tp1 = p1 + offset;
	Vector3F tp2 = p2 + offset;
	Box3F tbox = box;
	tbox.mins -= offset;
	tbox.maxs -= offset;

	Box3F moveBounds;
	moveBounds.SetFromLine(tp1, tp2);
	moveBounds.mins += tbox.mins;
	moveBounds.maxs += tbox.maxs;

	// Other physics objects...
	BoxBrush brush;
	for (auto it = objects.begin(); it != objects.end(); ++it)
	{
		AlignedBoxPhysicsObject *obj = *it;

		if (!obj->Solid() || !obj->Active()
			|| (obj == exclude)
			|| !(obj->CollisionGroup() & collisionMask)
			)
			continue;

		Box3F symBox = obj->Bounds();
		if (obj->Moving())
		{
			MakeSymmetricalBox(obj->Bounds(), symBox);
		}
		brush.Set(obj->Orientation().origin, symBox);

		if (!moveBounds.Intersects(symBox, obj->Orientation().origin))
			continue;

		Vector3F normal;
		float fraction = brush.TraceBox(tp1, tp2, tbox, &normal);
		if (fraction < traceInfo->fraction)
		{
			traceInfo->fraction = fraction;
			traceInfo->normal   = normal;
			traceInfo->object   = obj;
			traceInfo->staticId = -1;
		}
	}

	// Heightmaps and static object sets...
	for (int i = 0; i < staticObjects.CurrentCapacity(); ++i)
	{
		if (staticObjects.IsFree(i))
			continue;
		if (!(staticObjects[i]->collisionGroup & collisionMask))
			continue;

		float beforeFrac = traceInfo->fraction;
		staticObjects[i]->TraceBox(tp1, tp2, tbox, moveBounds, traceInfo);
		if (traceInfo->fraction < beforeFrac)
		{
			traceInfo->staticId = i;
			traceInfo->object   = 0;
		}
	}
}

float AlignedBoxPhysics::BoxTerrainHeight(const Vector3F &pos, const Box3F &box, uint32_t collisionMask)
{
	const float startCheckHeight = -std::numeric_limits<float>::max();
	float maxHeight = startCheckHeight;
	for (int i = 0; i < staticObjects.CurrentCapacity(); ++i)
	{
		if (staticObjects.IsFree(i))
			continue;
		if (!staticObjects[i]->IsTerrain())
			continue;
		if (!(staticObjects[i]->collisionGroup & collisionMask))
			continue;

		float height = staticObjects[i]->BoxTerrainHeight(pos, box);
		if (height > maxHeight)
			maxHeight = height;
	}
	// TODO: This triggered a bug with the car, because BoxTerrainHeight didn't
	// hit anything.
	// set to zero if we never collided with any terrain
	//if (maxHeight == startCheckHeight)
	//	maxHeight = 0;
	return maxHeight;
}

void AlignedBoxPhysics::Trace(
	const Vector3F &p1,
	const Vector3F &p2,
	const AlignedBoxPhysicsObject *exclude,
	PhysicsTraceInfo *traceInfo,
	uint32_t collisionMask
)
{
	Box3F box;
	TraceBox(p1, p2, box, traceInfo, exclude, collisionMask);
}

bool AlignedBoxPhysics::CheckIntersection(AlignedBoxPhysicsObject *object)
{
	// TODO: This really needs to be implemented.
	return false;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
