/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "dynamics/aabb/AlignedBoxPhysics.hh"
#include <iostream>
#include <memory.h>
#include <limits>
#include "library/containers/Array2D.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//#define TERRAIN_DEBUG_PRINT

//-----------------------------------------------------------------------------
// AlignedBoxPhysics::Heightmap
//-----------------------------------------------------------------------------

AlignedBoxPhysics::HeightMap::HeightMap(PhysicsHeightMap *heightMap, bool persist)
{
	heightMap->Validate();

	heightMapData = *heightMap;
	heightMapDataOwned = false;

	if (!persist)
	{
		heightMapData.heights = new Array2D<float>(heightMap->heights);
		heightMapData.diagonals = new Array2D<char>(heightMap->diagonals);
		heightMapDataOwned = true;
	}

	tilesW = heightMapData.diagonals->Width();
	tilesH = heightMapData.diagonals->Height();
	triangles.resize(tilesW * tilesH * 2);

	float max = -std::numeric_limits<float>::max();
	float min = std::numeric_limits<float>::max();
	for (int i = 0; i < heightMapData.heights->NumElements(); ++i)
	{
		if (heightMapData.heights->at(i) > max) max = heightMapData.heights->at(i);
		if (heightMapData.heights->at(i) < min) min = heightMapData.heights->at(i);
	}
	worldBounds.mins.Set(heightMapData.x, heightMapData.y, min);
	worldBounds.maxs.Set(heightMapData.x + heightMapData.scale * tilesW, heightMapData.y + heightMapData.scale * tilesH, max);
	origin.Set(heightMapData.x, heightMapData.y, 0);
}

AlignedBoxPhysics::HeightMap::~HeightMap()
{
	if (heightMapDataOwned)
	{
		delete[] heightMapData.heights;
		delete heightMapData.diagonals;
	}
}

void AlignedBoxPhysics::HeightMap::InitTriangle(int x, int y)
{
	float triangleVerts[9];
	int diagonality;
	Triangle *tri = &triangles[(y * tilesW + x) * 2];
	GetTriangle(x, y, 0, triangleVerts, &diagonality);
	tri->SetPlanes(triangleVerts, 0, diagonality);
	++tri;
	GetTriangle(x, y, 1, triangleVerts, &diagonality);
	tri->SetPlanes(triangleVerts, 1, diagonality);
}

void AlignedBoxPhysics::HeightMap::GetTriangle(int x, int y, int triNum, float *verts, int *diagonality)
{
	int x1, x2, x3, y1, y2, y3;

	*diagonality = heightMapData.Diagonal(x, y);
	if (*diagonality == 0)
	{
		if (triNum == 0)
		{
			x1 = 1; y1 = 0;
			x2 = 0; y2 = 0;
			x3 = 1; y3 = 1;
		}
		else
		{
			x1 = 0; y1 = 1;
			x2 = 1; y2 = 1;
			x3 = 0; y3 = 0;
		}
	}
	else
	{
		if (triNum == 0)
		{
			x1 = 0; y1 = 0;
			x2 = 0; y2 = 1;
			x3 = 1; y3 = 0;
		}
		else
		{
			x1 = 1; y1 = 1;
			x2 = 1; y2 = 0;
			x3 = 0; y3 = 1;
		}
	}

	verts[0] = (x + x1) * heightMapData.scale + heightMapData.x;
	verts[1] = (y + y1) * heightMapData.scale + heightMapData.y;
	verts[2] = heightMapData.Height(x + x1, y + y1);
	verts[3] = (x + x2) * heightMapData.scale + heightMapData.x;
	verts[4] = (y + y2) * heightMapData.scale + heightMapData.y;
	verts[5] = heightMapData.Height(x + x2, y + y2);
	verts[6] = (x + x3) * heightMapData.scale + heightMapData.x;
	verts[7] = (y + y3) * heightMapData.scale + heightMapData.y;
	verts[8] = heightMapData.Height(x + x3, y + y3);
}

// Expects world coordinates.
// Expects traceInfo to already be set up with the closest trace found, or
// FLT_MAX. Only updates traceInfo if we collided with something closer.
void AlignedBoxPhysics::HeightMap::TraceBox(
	const Vector3F &p1,
	const Vector3F &p2,
	const Box3F &box,
	const Box3F &worldMoveBounds,
	PhysicsTraceInfo *traceInfo
)
{
	if (!worldBounds.Intersects(worldMoveBounds))
		return;

	// Convert to local coordinates.
	Vector3F lp1 = p1;// - origin;
	Vector3F lp2 = p2;// - origin;

	// Check all tiles by coordinate range motion's bounds.
	Box3F moveBounds;
	moveBounds.SetFromLine(lp1, lp2);
	moveBounds.mins += box.mins;
	moveBounds.maxs += box.maxs;

	int xMin = (int)floor((moveBounds.mins[0] - origin[0]) / heightMapData.scale);
	int xMax = (int)floor((moveBounds.maxs[0] - origin[0]) / heightMapData.scale);
	int yMin = (int)floor((moveBounds.mins[1] - origin[1]) / heightMapData.scale);
	int yMax = (int)floor((moveBounds.maxs[1] - origin[1]) / heightMapData.scale);

	if (xMin < 0) xMin = 0;
	if (yMin < 0) yMin = 0;
	if (xMax > tilesW - 1) xMax = tilesW - 1;
	if (yMax > tilesH - 1) yMax = tilesH - 1;

#ifdef TERRAIN_DEBUG_PRINT
	std::cout << "origin " << origin[0] << "," << origin[1] << "," << origin[2]
		<< " lp1 " << lp1[0] << "," << lp1[1] << "," << lp1[2]
		<< " lp2 " << lp2[0] << "," << lp2[1] << "," << lp2[2];
	std::cout << " tc " << xMin << "," << yMin << " - " << xMax << "," << yMax << std::endl;
#endif

	PhysicsTraceInfo info;
	info.fraction = std::numeric_limits<float>::max();
	for (int y = yMin; y <= yMax; ++y)
	{
		for (int x = xMin; x <= xMax; ++x)
		{
			for (int t = 0; t < 2; ++t)
			{
				int index = (y * tilesW + x) * 2 + t;
				if (triangles[index].numPlanes == 0)
				{
					InitTriangle(x, y);
				}
				triangles[index].TraceBox(lp1, lp2, box, &info);
				if (info.fraction < traceInfo->fraction && info.fraction >= 0)
				{
					*traceInfo = info;
				}
			}
		}
	}

#ifdef TERRAIN_DEBUG_PRINT
	std::cout << " hm got " << traceInfo->fraction << std::endl;
#endif
}

float AlignedBoxPhysics::HeightMap::BoxTerrainHeight(const Vector3F &pos, const Box3F &box)
{
	Box3F infiniteBox = box;
	infiniteBox.mins += pos;
	infiniteBox.maxs += pos;
	infiniteBox.maxs[2] =  std::numeric_limits<float>::max();
	infiniteBox.mins[2] = -std::numeric_limits<float>::max();

	if (!worldBounds.Intersects(infiniteBox))
		return -std::numeric_limits<float>::max();

	Vector3F start = pos;
	Vector3F end   = pos;
	start[2] = (worldBounds.maxs[2] - box.mins[2]) + 1.0f;
	end[2]   = (worldBounds.mins[2] - box.maxs[2]) - 1.0f;

	PhysicsTraceInfo info;
	info.fraction = std::numeric_limits<float>::max();
	TraceBox(start, end, box, infiniteBox, &info);
	return start[2] + (end[2] - start[2]) * info.fraction;
}

//-----------------------------------------------------------------------------
// AlignedBoxPhysics::Heightmap::Triangle
//-----------------------------------------------------------------------------

inline void MakePlaneForTriangle(const float *p1, const float *p2, const float *p3, Plane3F *dest)
{
#if 0
	dest->Set(Vector3F(p1), Vector3F(p2), Vector3F(p3));
#else
	float u[3], v[3];
	Vector3F::Sub(p2, p1, v);
	Vector3F::Sub(p3, p1, u);
	Vector3F::CrossProduct(u, v, dest->normal.v);
	dest->normal.Normalize();
	dest->distance = Vector3F::DotProduct(dest->normal.v, p1);
#endif
}

// p1 and p2 describe a line, pOther is the other point of the triangle.
// For p1 and p2 to be a higher line, then pOther needs to be below the
// plane that lies on top of them (containing them and pointing as much up as
// possible.)
// Returns true if the other point is below the plane.
// Always writes to dest.
inline bool MakePlaneForLine(const float *p1, const float *p2, const float *pOther, Plane3F *dest)
{
	float dir[3], up[3], side[3];

	Vector3F::Sub(p1, p2, dir);
	Vector3F::Normalize(dir);

	Vector3F::Set(up, 0, 0, 1);
	Vector3F::CrossProduct(up, dir, side);
	Vector3F::CrossProduct(dir, side, dest->normal.v);

	if (dest->normal[2] < 0)
		dest->normal *= -1;

	dest->distance = Vector3F::DotProduct(dest->normal.v, p1);

	float f = Vector3F::DotProduct(dest->normal.v, pOther);
	return (f < dest->distance);
}

void AlignedBoxPhysics::HeightMap::Triangle::SetPlanes(float *triangleVerts, int triangleNum, int diagonality)
{
	// The first vertex is always the right angle, and winding is CCW.
	// Axes we need for vertical corner planes given the diagonality and
	// number of the triangle.
	// Diag.   Tri-num.    2     3
	//   0        0       -x    +y
	//   0        1       +x    -y
	//   1        0       +y    +x
	//   1        1       -y    -x

	float raisedVerts[9];
	for (int i = 0; i < 9; i += 3)
	{
		raisedVerts[i  ] = triangleVerts[i  ];
		raisedVerts[i+1] = triangleVerts[i+1];
		raisedVerts[i+2] = triangleVerts[i+2] + 16.0f;
	}

	MakePlaneForTriangle(&triangleVerts[0], &triangleVerts[3], &raisedVerts[0], &planes[0]);
	MakePlaneForTriangle(&triangleVerts[3], &triangleVerts[6], &raisedVerts[3], &planes[1]);
	MakePlaneForTriangle(&triangleVerts[6], &triangleVerts[0], &raisedVerts[6], &planes[2]);
	MakePlaneForTriangle(&triangleVerts[0], &triangleVerts[3], &triangleVerts[6], &planes[3]);

	if (diagonality == 0)
	{
		if (triangleNum == 0)
		{
			planes[4].normal.Set(-1, 0, 0);
			planes[4].distance = Vector3F::DotProduct(planes[4].normal.v, &triangleVerts[3]);
			planes[5].normal.Set(0, 1, 0);
			planes[5].distance = Vector3F::DotProduct(planes[5].normal.v, &triangleVerts[6]);
		}
		else
		{
			planes[4].normal.Set(1, 0, 0);
			planes[4].distance = Vector3F::DotProduct(planes[4].normal.v, &triangleVerts[3]);
			planes[5].normal.Set(0, -1, 0);
			planes[5].distance = Vector3F::DotProduct(planes[5].normal.v, &triangleVerts[6]);
		}
	}
	else
	{
		if (triangleNum == 0)
		{
			planes[4].normal.Set(0, 1, 0);
			planes[4].distance = Vector3F::DotProduct(planes[4].normal.v, &triangleVerts[3]);
			planes[5].normal.Set(1, 0, 0);
			planes[5].distance = Vector3F::DotProduct(planes[5].normal.v, &triangleVerts[6]);
		}
		else
		{
			planes[4].normal.Set(0, -1, 0);
			planes[4].distance = Vector3F::DotProduct(planes[4].normal.v, &triangleVerts[3]);
			planes[5].normal.Set(-1, 0, 0);
			planes[5].distance = Vector3F::DotProduct(planes[5].normal.v, &triangleVerts[6]);
		}
	}

	float highest = triangleVerts[2];
	int highestPoint = 0, nhp1 = 1, nhp2 = 2;
	if (triangleVerts[5] > highest)
	{
		highest = triangleVerts[5];
		highestPoint = 1;
		nhp1 = 0;
		nhp2 = 2;
	}
	if (triangleVerts[8] > highest)
	{
		highest = triangleVerts[8];
		highestPoint = 2;
		nhp1 = 0;
		nhp2 = 1;
	}

	planes[6].normal.Set(0, 0, 1);
	planes[6].distance = highest;

	numPlanes = 7;

	// Test each of the 3 lines, if they exist increase num planes.
	for (int i=0; i<3; ++i)
	{
		int j = (i + 1) % 3;
		int k = (i + 2) % 3;

		if (MakePlaneForLine(&triangleVerts[i*3], &triangleVerts[j*3], &triangleVerts[k*3], &planes[numPlanes]))
		{
			++numPlanes;
		}
	}
}

// Always sets traceinfo with the result, without any comparison of the
// existing traceinfo.
void AlignedBoxPhysics::HeightMap::Triangle::TraceBox(
	const Vector3F &p1,
	const Vector3F &p2,
	const Box3F &box,
	PhysicsTraceInfo *traceInfo
)
{
	// TODO: Re-do this so that we test the up-to-4 surface planes for the
	// lowest collision point, and then check that it's within the other planes.

	// There's always 4 horizontal bounding planes, so we might as well put
	// those first, and have the variable number of planes after.

	// normal 4, expanded 9
	// We put #3 first so we can break out if we're heading in the wrong
	// direction against the triangle.
	static int triangle[] = { 3, 0, 1, 2, 4, 5, 6, 7, 8 };

	int *planeIndex = triangle;
	int numPlanes = this->numPlanes;

	float lastEntry = -std::numeric_limits<float>::max(); // where we enter the brush
	float firstLeave = std::numeric_limits<float>::max(); // where we leave the brush

#ifdef TERRAIN_DEBUG_PRINT
	std::cout << "init " << lastEntry << "," << firstLeave << std::endl;
#endif

	for (int i = 0; i < numPlanes; ++i)
	{
		float offset = BoxExpandPlaneAmount(planes[planeIndex[i]], box);
		float direction;
		float fraction = planes[planeIndex[i]].Intersects(p1, p2, offset, &direction);

#ifdef TERRAIN_DEBUG_PRINT
		std::cout << "Plane " << i;
		std::cout << " frac " << fraction << " dir " << direction;
#endif

		// If we're heading in the wrong direction, don't collide.
		if (planeIndex[i] == 3 && direction > 0)
		{
			traceInfo->fraction = std::numeric_limits<float>::max();
#ifdef TERRAIN_DEBUG_PRINT
			std::cout << " wrong way to hit" << std::endl;
#endif
			return;
		}

		if (direction < 0)
		{
			if (fraction > lastEntry)
			{
				traceInfo->normal.Set(planes[planeIndex[i]].normal);
				lastEntry = fraction;
#ifdef TERRAIN_DEBUG_PRINT
				std::cout << " last entry";
#endif
			}
		}
		else
		{
			if (fraction < firstLeave)
			{
				firstLeave = fraction;
#ifdef TERRAIN_DEBUG_PRINT
				std::cout << " first leave";
#endif
			}
		}

#ifdef TERRAIN_DEBUG_PRINT
		std::cout << std::endl;
#endif
	}

#ifdef TERRAIN_DEBUG_PRINT
	std::cout << " fl " << firstLeave << " le " << lastEntry << std::endl;
#endif

	// If the first leaving plane is before the last entry plane, then we've
	// missed the brush.
	if (firstLeave < lastEntry)
	{
		traceInfo->fraction = std::numeric_limits<float>::max();
		return;
	}
#ifdef TERRAIN_DEBUG_PRINT
	std::cout << "return le " << lastEntry << std::endl;
#endif
	traceInfo->fraction = lastEntry;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

