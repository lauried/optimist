/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <set>
#include <vector>
#include "library/containers/Array2D.hh"
#include "library/geometry/Plane3.hh"
#include "library/geometry/Box3.hh"
#include "library/containers/PagedAllocator.hh"
#include "dynamics/aabb/AlignedBoxPhysicsObject.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Return data for traces.
 */
struct PhysicsTraceInfo {
	float fraction;
	Vector3F endPoint;
	Vector3F normal;
	int staticId; ///< if we hit a static object or set, else null
	AlignedBoxPhysicsObject *object; ///< if we hit a dynamic object, else null

	PhysicsTraceInfo() : fraction(0), staticId(0), object(0) { }
};

/**
 * Base class for static objects.
 * Static objects are anything that does not move and is not interacted
 * with in an individual way, e.g. terrain, generic trees, etc.
 */
class PhysicsStaticObject {
public:
	OrientationF orientation;
	Box3F        bounds;
};

/**
 * Struct containing all information describing a heightmap.
 */
struct PhysicsHeightMap {
	/**
	 * Heights of the terrain in world coordinates.
	 */
	const Array2D<float> *heights;
	/**
	 * Specifies the direction in which each tile is split.
	 * 0 for x=y and 1 for x+y=1.
	 * Contains one fewer rows and columns than height samples, as the
	 * heights are corners of tiles and diagonals are tiles themselves.
	 */
	const Array2D<char>  *diagonals;
	float x, y; ///< World coordinates of the minimum corner of the tile.
	float scale;

	PhysicsHeightMap() :
		heights(0),
		diagonals(0),
		x(0), y(0),
		scale(0.0f)
		{ }

	float Height(int x, int y) { return heights->at(x, y); }
	bool  Diagonal(int x, int y) { return diagonals->at(x, y); }

	/**
	 * Asserts that the heightmap is valid data:
	 * There must always be one fewer rows and columns of tiles than height
	 * samples.
	 * Both heights and diagonals must be specified.
	 * Scale must be positive and nonzero.
	 */
	void Validate()
	{
		assert(heights != 0);
		assert(diagonals != 0);
		assert(heights->Width() == diagonals->Width() + 1);
		assert(heights->Height() == diagonals->Height() + 1);
		assert(scale > 0.0f);
	}
};

/**
 * Struct containing all information describing a set of one or more planes.
 * The planes could describe a finite or infinite object.
 */
struct PhysicsPlaneSet
{
	Plane3F *planes;
	int numPlanes;
};

/**
 * Actual implementation of aligned box physics.
 */
class AlignedBoxPhysics {
public:
	void Add(AlignedBoxPhysicsObject *obj);
	void Remove(AlignedBoxPhysicsObject *obj);
	void ObjectUpdated(AlignedBoxPhysicsObject *obj);

	int AddStatic(PhysicsHeightMap *heightMap, bool persist, uint32_t collisionGroup);
	int AddStatic(PhysicsStaticObject *objects, int number, bool persist, uint32_t collisionGroup);
	int AddStatic(PhysicsPlaneSet *planeSet, bool persist, uint32_t collisionGroup);
	void RemoveStatic(int id);

	void Update(double interval);

	void Trace(
		const Vector3F &p1,
		const Vector3F &p2,
		const AlignedBoxPhysicsObject *exclude,
		PhysicsTraceInfo *traceInfo,
		uint32_t collisionMask
	);

	bool CheckIntersection(AlignedBoxPhysicsObject *object);

private:
	static constexpr float BACKOFF = 1.0f / 512.0f;
	static constexpr float TERRAIN_CONFORM_SPEED = 8.0f;

	std::set<AlignedBoxPhysicsObject*> objects;

	// Base class for grouping heightmap and static objects.
	struct Traceable {
		virtual void TraceBox(
			const Vector3F &p1,
			const Vector3F &p2,
			const Box3F &box,
			const Box3F &worldMoveBounds,
			PhysicsTraceInfo *traceInfo
		) = 0;
		virtual bool IsTerrain() = 0;
		virtual float BoxTerrainHeight(const Vector3F &pos, const Box3F &box) = 0;
		virtual ~Traceable() {}
		uint32_t collisionGroup;
	};
	PagedAllocator<Traceable*> staticObjects;

	// Set of box objects which never move or interact.
	struct StaticObjectSet : public Traceable {
		PhysicsStaticObject *objects;
		int  number;
		bool owned; // true if we own the objects (i.e. copied it from !persist)
		Box3F worldBounds;

		StaticObjectSet(PhysicsStaticObject *objects, int number, bool persist);
		~StaticObjectSet();

		void TraceBox(
			const Vector3F &p1,
			const Vector3F &p2,
			const Box3F &box,
			const Box3F &worldMoveBounds,
			PhysicsTraceInfo *traceInfo
		);

		bool IsTerrain() { return false; }
		float BoxTerrainHeight(const Vector3F &pos, const Box3F &box) { return 0; }
	};

	// Everything within a heightmap is relative to its origin.
	struct HeightMap : public Traceable {
		struct Triangle {
			// 3 vertical planes, 1 top plane,
			// 2 vertical corner caps, 1 top corner cap.
			// 2 highest edge caps.
			// TODO: For an optimization we'll want to reorder the planes so
			// that we can check for intersection on the surface first, and
			// then just check the intersection point is within the horizontal
			// bounds of the remaining planes, without them being something to
			// collide with.
			Plane3F planes[10];
			int           numPlanes;

			Triangle() : numPlanes(0) { }

			void SetPlanes(float *triangleVerts, int triangleNum, int diagonality);

			void TraceBox(
				const Vector3F &p1,
				const Vector3F &p2,
				const Box3F &box,
				PhysicsTraceInfo *traceInfo
			);
		};
		PhysicsHeightMap heightMapData;
		bool heightMapDataOwned; // true if we own the heightmap data.
		std::vector<Triangle> triangles;
		int tilesW, tilesH;
		Vector3F origin;
		Box3F worldBounds;

		HeightMap(PhysicsHeightMap *heightMap, bool persist);
		~HeightMap();

		void InitTriangle(int x, int y);
		void GetTriangle(int x, int y, int triNum, float *verts, int *diagonality);

		void TraceBox(
			const Vector3F &p1,
			const Vector3F &p2,
			const Box3F &box,
			const Box3F &worldMoveBounds,
			PhysicsTraceInfo *traceInfo
		);

		bool IsTerrain() { return true; }
		float BoxTerrainHeight(const Vector3F &pos, const Box3F &box);
	};

	// Set of planes.
	struct PlaneSet : Traceable {
		Plane3F *planes;
		int numPlanes;
		bool ownPlanes;
		bool isTerrain;

		PlaneSet(PhysicsPlaneSet *planeSet, bool persist);
		~PlaneSet();

		void TraceBox(
			const Vector3F &p1,
			const Vector3F &p2,
			const Box3F &box,
			const Box3F &worldMoveBounds,
			PhysicsTraceInfo *traceInfo
		);

		bool IsTerrain() { return isTerrain; }
		float BoxTerrainHeight(const Vector3F &pos, const Box3F &box);
	};

	static float BoxExpandPlaneAmount(const Plane3F &plane, const Box3F &box)
	{
		Vector3F boxCorner;
		for (int axis = 0; axis < 3; ++axis)
		{
			if (plane.normal.v[axis] > 0)
				boxCorner[axis] = box.mins.v[axis];
			else
				boxCorner[axis] = box.maxs.v[axis];
		}
		return 0 - plane.normal.DotProduct(boxCorner);
	}

	void TraceBox(
		const Vector3F &p1,
		const Vector3F &p2,
		const Box3F &box,
		PhysicsTraceInfo *traceInfo,
		const AlignedBoxPhysicsObject *exclude,
		uint32_t collisionMask
	);
	float BoxTerrainHeight(const Vector3F &pos, const Box3F &box, uint32_t collisionMask = 1);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
