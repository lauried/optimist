/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "library/geometry/Vector3.hh"
#include "library/geometry/Box3.hh"
#include "library/geometry/Orientation.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class AlignedBoxPhysicsObject {
public:
	enum PhysicsType {
		/// Vertical axis is always oriented 'up'.
		PT_CHARACTER = 7, // gravity + no_gravity_onground + buoyancy
		/// A tumbling box with nodes for wheels, jets, fins, etc.
		PT_VEHICLE = 13 // gravity + buoyancy + terrain_conform
		// TODO: There could be more types, for example bsp model buildings,
		// or a box that isn't a vehicle.
	};

	bool active;
	bool moving;
	bool solid;
	OrientationF orientation;
	Vector3F velocity;
	Vector3F angularVelocity;
	Box3F bounds;
	int physicsType;
	bool onGround;
	float waterLevel;
	uint32_t collisionGroup;
	uint32_t collisionMask;
	float mass;
	float gravity;

	virtual bool& Active() { return active; }
	virtual bool& Moving() { return moving; }
	virtual bool& Solid() { return solid; }

	virtual OrientationF& Orientation() { return orientation; }
	virtual Vector3F& Velocity() { return velocity; }
	/// Body rates, i.e. rotation relative to the body axes.
	/// According to http://www.euclideanspace.com/physics/kinematics/angularvelocity/
	/// this can be treated like any other vector (addition, subtraction, etc.)
	virtual Vector3F& AngularVelocity() { return angularVelocity; }
	virtual Box3F& Bounds() { return bounds; }

	virtual int& PhysicsType() { return physicsType; }
	virtual bool& OnGround() { return onGround; }

	virtual float WaterLevel() { return waterLevel; }

	virtual uint32_t& CollisionGroup() { return collisionGroup; }
	virtual uint32_t& CollisionMask() { return collisionMask; }

	virtual float& Mass() { return mass; }
	virtual float& Gravity() { return gravity; }

	/**
	 * Physics nodes.
	 * Nodes are enabled by setting NodeType to anything but NT_NONE for any
	 * positive index less than MaxNodes.
	 */
	struct PhysicsNode {
		enum NodeType {
			NT_NONE,
			NT_WHEEL,
			NT_JET,
			NT_FIN
		};
		NodeType type;
		/// The position at which the node is attached.
		float position[3];
		union {
			struct {
				/// The end point of the suspension, away from its attached
				/// point. The bottom (or furthest side) of the wheel is at
				/// this point when the suspension is fully extended.
				float suspensionDirection[3];
				/// The forward direction of the wheel.
				float forwardDirection[3];
				float radius;
				/// 0.0 is floppy, 1.0 is rigid.
				float suspensionStiffness;
				/// -----------------------------------------------------------
				/// Rotation of the wheel from its original forward direction
				/// about the suspensionDirection axis, in Tau.
				float currentAngle;
				/// As powered by the engine. Positive would roll forward.
				float currentTorque;
				float currentBrakeTorque;
				/// The fraction of the distance between position and (position
				/// + suspensionDirection) that the bottom of the wheel is.
				/// If this is greater than or equal to 1, then the wheel is
				/// not in contact with the ground.
				/// This is set by the implementation.
				float currentPosition;
			} wheel;
			struct {
				/// -----------------------------------------------------------
				/// The direction in which the jet is pushing the object, i.e.
				/// opposite to the ejected matter.
				float currentDirection[3];
				/// The force applied. (Units?)
				float currentThrust;
			} jet;
			struct {
				float planeNormal[3];
				/// Axis vector around which the plane normal can be rotated.
				float rotationAxis[3];
				/// Drag when motion is perpendicular to the plane.
				/// This is the fraction of the force with which the fin
				/// pushes through its medium that is applied back.
				float flatDrag;
				/// Drag when motion is parallel to the plane.
				float edgeDrag;
				/// -----------------------------------------------------------
				/// Rotation of the planenormal from its original position
				/// around the rotation axis, in Tau.
				float currentAngle;
				/// Multiplier from 0.0 to 1.0 scaling the effect of the fin.
				/// This is to let us toggle things like boat rudders, which
				/// wouldn't have much effect out of water.
				float currentScaleEffect;
			} fin;
		} data;
	};

	static const int NUM_NODES = 12;
	PhysicsNode nodes[NUM_NODES];

	virtual int MaxNodes() { return NUM_NODES; }
	virtual PhysicsNode& NodeProperties(int index) { return nodes[index]; }
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

