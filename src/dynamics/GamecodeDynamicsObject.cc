/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "dynamics/GamecodeDynamicsObject.hh"
#include <iostream>

#include "gamecode/library/GamecodeOrientation.hh"
#include "gamecode/library/GamecodeVector.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

GamecodeDynamicsObject::~GamecodeDynamicsObject()
{
	Remove();
}

void GamecodeDynamicsObject::Remove()
{
	if (dynamics != nullptr)
	{
		dynamics->RemoveObject(object);
		dynamics = nullptr;
		object = nullptr;
	}
}

//-----------------------------------------------------------------------------
// IGamecodeEngineObject
//-----------------------------------------------------------------------------

void GamecodeDynamicsObject::SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode)
{
	if (!key.compare("onGround"))
	{
		throw gamecode_exception("onGround read only");
	}
	throw gamecode_exception("Unknown property");
}

GamecodeValue GamecodeDynamicsObject::GetProperty(std::string key, IGamecode *gamecode)
{
	if (!key.compare("onGround"))
	{
		return GamecodeValue(object->IsOnGround() ? 1 : 0);
	}
	throw gamecode_exception("Unknown property");
}

IGamecodeEngineObject* GamecodeDynamicsObject::Construct(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	throw gamecode_exception("DynamicsObject cannot be constructed, it must be created by Dynamics");
}

//-----------------------------------------------------------------------------
// Gamecode Methods
//-----------------------------------------------------------------------------

GamecodeValue GamecodeDynamicsObject::GetOrientation(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	GamecodeOrientation *orientation = parameters->RequireEngineType<GamecodeOrientation>(0, "Orientation");
	object->GetOrientation(orientation->GetObject());
	return GamecodeValue();
}

GamecodeValue GamecodeDynamicsObject::SetOrientation(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	GamecodeOrientation *orientation = parameters->RequireEngineType<GamecodeOrientation>(0, "Orientation");
	object->SetOrientation(*(orientation->GetObject()));
	return GamecodeValue();
}

GamecodeValue GamecodeDynamicsObject::GetVelocity(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	GamecodeVector *vector = parameters->RequireEngineType<GamecodeVector>(0, "Vector");
	object->GetVelocity(vector->GetObject());
	return GamecodeValue();
}

GamecodeValue GamecodeDynamicsObject::SetVelocity(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	GamecodeVector *vector = parameters->RequireEngineType<GamecodeVector>(0, "Vector");
	object->SetVelocity(*(vector->GetObject()));
	return GamecodeValue();
}

GamecodeValue GamecodeDynamicsObject::GetAVelocity(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	GamecodeVector *vector = parameters->RequireEngineType<GamecodeVector>(0, "Vector");
	object->GetAngularVelocity(vector->GetObject());
	return GamecodeValue();
}

GamecodeValue GamecodeDynamicsObject::SetAVelocity(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	GamecodeVector *vector = parameters->RequireEngineType<GamecodeVector>(0, "Vector");
	object->SetAngularVelocity(*(vector->GetObject()));
	return GamecodeValue();
}

GamecodeValue GamecodeDynamicsObject::ApplyForce(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(2);
	GamecodeVector *position = parameters->RequireEngineType<GamecodeVector>(0, "Vector");
	GamecodeVector *force = parameters->RequireEngineType<GamecodeVector>(1, "Vector");
	object->ApplyForce(*(position->GetObject()), *(force->GetObject()));
	return GamecodeValue();
}

GamecodeValue GamecodeDynamicsObject::ApplyForceRelative(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(2);
	GamecodeVector *position = parameters->RequireEngineType<GamecodeVector>(0, "Vector");
	GamecodeVector *force = parameters->RequireEngineType<GamecodeVector>(1, "Vector");
	object->ApplyForceRelative(*(position->GetObject()), *(force->GetObject()));
	return GamecodeValue();
}

GamecodeValue GamecodeDynamicsObject::GetGroundNormal(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	GamecodeVector *position = parameters->RequireEngineType<GamecodeVector>(0, "Vector");
	object->GetGroundNormal(position->GetObject());
	return GamecodeValue();
}

GamecodeValue GamecodeDynamicsObject::SetGravity(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	int value = parameters->RequireType(0, GamecodeValue::T_Number).GetInt();
	object->SetGravity((bool) value);
	return GamecodeValue();
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
