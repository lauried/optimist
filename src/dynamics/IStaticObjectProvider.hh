/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <vector>
#include "dynamics/IDynamicsObject.hh"
#include "library/geometry/Box3.hh"
#include "dynamics/DynamicsShapeData.hh"
#include "dynamics/IDynamicsUserData.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// TODO: Document the overall usage of this interface - dynamics gets objects
// which intersect a given volume and frees them when they aren't needed.

/**
 * A static object provider is used to provide the simulation with geometry
 * that spans a large area.
 */
class IStaticObjectProvider {
public:
	struct StaticObjectData {
		Box3F bounds;
		OrientationF orientation;
		Vector3F boxSize;
		DynamicsShapeData *shape;
		IDynamicsUserData *userData;
	};

	class IStaticObject {
	public:
		virtual StaticObjectData *GetStaticObjectData() = 0;
	};

	virtual ~IStaticObjectProvider();

	/**
	 * Add objects which (potentially) intersect the bounds to the dynamics
	 * simulation that requests them. The implementation appends to the vector
	 * the objects to be added.
	 *
	 * The implementation MUST provide all objects which intersect the bounds.
	 * Failure to do so will cause erroneous simulation.
	 * The concrete static object provider may over-provide, i.e. give objects
	 * that don't intersect the bounds, though this is not advised.
	 *
	 * The implementation MUST NOT provide the same object again until it has
	 * been freed.
	 *
	 * The IStaticObject instances must persist until the dynamics frees them.
	 */
	virtual void GetObjectsForVolume(std::vector<Box3F> *bounds, std::vector<IStaticObject*> *objects) = 0;

	/**
	 * Releases an object previously retrieved via a call on this object to
	 * GetObjectsForVolume.
	 *
	 * This MUST NEVER be called with an object which was not previously
	 * provided with GetObjectsForVolume. The implementation MAY check for this
	 * occurrance and throw an exception or otherwise report the error.
	 */
	virtual void FreeObject(IStaticObject *object) = 0;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

