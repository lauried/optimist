/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "library/geometry/Vector3.hh"
#include "library/geometry/Orientation.hh"
#include "dynamics/IDynamicsUserData.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class IDynamicsObject {
public:
	~IDynamicsObject();

	// Note: The implementation can assume that dest is not null in all cases.

	// TODO: Get the object's movement type.

	virtual void SetOrientation(OrientationF &src) = 0;
	virtual void GetOrientation(OrientationF *dest) = 0;

	virtual void SetVelocity(Vector3F &src) = 0;
	virtual void GetVelocity(Vector3F *dest) = 0;

	virtual void SetAngularVelocity(Vector3F &src) = 0;
	virtual void GetAngularVelocity(Vector3F *dest) = 0;

	// Note: The force is applied as an impulse in the next physics frame (call to Run())
	virtual void ApplyForce(Vector3F& position, Vector3F& force) = 0;
	virtual void ApplyForceRelative(Vector3F& position, Vector3F& force) = 0;

	virtual void SetUserData(IDynamicsUserData *) = 0;
	virtual IDynamicsUserData* GetUserData() = 0;

	// Static objects should return false.
	virtual bool IsOnGround() = 0;
	virtual void GetGroundNormal(Vector3F *dest) = 0;
	virtual void SetGravity(bool gravity) = 0;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

