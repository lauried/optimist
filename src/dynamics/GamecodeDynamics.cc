/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "dynamics/GamecodeDynamics.hh"
#include <iostream>

#include "dynamics/GamecodeDynamicsObject.hh"
#include "dynamics/ode/OdeDynamics.hh"
#include "gamecode/library/GamecodeVector.hh"
#include "gamecode/library/GamecodeOrientation.hh"
#include "gamecode/IGamecode.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// GamecodeDynamics
//-----------------------------------------------------------------------------

GamecodeDynamics::~GamecodeDynamics()
{
	// Unhook all our child objects.
	const std::vector<IDynamicsObject*> *objects = dynamics->Objects();
	for (auto it = objects->begin(); it != objects->end(); ++it)
	{
		IDynamicsUserData *userData = (*it)->GetUserData();
		if (userData == nullptr)
		{
			continue;
		}
		GamecodeDynamicsObject *gcdo = dynamic_cast<GamecodeDynamicsObject*>(userData);
		if (gcdo == nullptr)
		{
			continue;
		}
		gcdo->Remove();
	}
	delete dynamics;
}

void GamecodeDynamics::Run(double time)
{
	// TODO: Instead of pre/post events, it'd be nice to have a callback
	// at regular physics intervals, i.e. between each point at which objects
	// can touch.

	dynamics->Run(time);
}

//-----------------------------------------------------------------------------
// IGamecodeEngineObject
//-----------------------------------------------------------------------------

IGamecodeEngineObject* GamecodeDynamics::Construct(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1, 2);

	IDynamics *dynamics = new OdeDynamics();

	std::string type = parameters->RequireType(0, GamecodeValue::T_String).GetString();
	// TODO: String type selects from available dynamics engines.
	// Currently just "ODE"

	IGamecodeFunction *collisionCallback = nullptr;

	if (parameters->size() > 1)
	{
		collisionCallback = parameters->RequireType(1, GamecodeValue::T_Function).GetFunction();
		IGamecode *gamecodePtr = gamecode;
		dynamics->SetCollisionCallback([collisionCallback, gamecodePtr](IDynamicsObject* obj1, IDynamicsObject* obj2){
			IGamecodeEngineObject *engine1 = dynamic_cast<IGamecodeEngineObject*>(obj1->GetUserData());
			IGamecodeEngineObject *engine2 = dynamic_cast<IGamecodeEngineObject*>(obj2->GetUserData());
			if (engine1 == nullptr || engine2 == nullptr)
			{
				return;
			}
			std::vector<GamecodeValue> parameters;
			parameters.push_back(GamecodeValue(engine1));
			parameters.push_back(GamecodeValue(engine2));
			gamecodePtr->CallFunction(GamecodeValue(collisionCallback), parameters, GamecodeValue());
		});
	}

	GamecodeDynamics *gamecodeDynamics = new GamecodeDynamics(dynamics);
	if (collisionCallback != nullptr)
	{
		gamecodeDynamics->SetCollisionCallback(collisionCallback);
	}
	return gamecodeDynamics;
}

std::vector<IGamecodeObject*>* GamecodeDynamics::GetChildObjects()
{
	childObjects.resize(0);
	for (auto it = staticObjectProviders.begin(); it != staticObjectProviders.end(); ++it)
	{
		childObjects.push_back(it->second);
	}
	if (collisionCallback != nullptr)
	{
		childObjects.push_back(collisionCallback);
	}
	return &childObjects;
}

//-----------------------------------------------------------------------------
// GamecodeDynamics Gamecode Methods
//-----------------------------------------------------------------------------

GamecodeValue GamecodeDynamics::Run(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	float time = parameters->RequireType(0, GamecodeValue::T_Number).GetFloat();
	Run(time);
	return GamecodeValue();
}

// orientation, type, size, [mass]
GamecodeValue GamecodeDynamics::AddObject(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(3, 4);

	GamecodeOrientation *position = parameters->RequireEngineType<GamecodeOrientation>(0, "Orientation");

	// TODO: Also handle integer enums.
	GamecodeValue motionType = parameters->RequireType(1, GamecodeValue::T_String);
	IDynamics::MotionType motion;
	if (motionType.IsString())
	{
		const char *motionTypeStr = motionType.GetString();
		if (!strcmp(motionTypeStr, "free"))
		{
			motion = IDynamics::T_Free;
		}
		else if (!strcmp(motionTypeStr, "vehicle"))
		{
			motion = IDynamics::T_Vehicle;
		}
		else if (!strcmp(motionTypeStr, "character"))
		{
			motion = IDynamics::T_Character;
		}
		else if (!strcmp(motionTypeStr, "static"))
		{
			motion = IDynamics::T_Static;
		}
		else
		{
			throw gamecode_exception("Parameter 3 must be one of \"free\", \"vehicle\", \"character\" or \"static\"");
		}
	}

	GamecodeVector *boxSize = parameters->RequireEngineType<GamecodeVector>(2, "Vector");

	// TODO: Shape - we need shape objects to exist first.
	// Maybe we could just specify a resource string.

	float mass = 1;
	if (parameters->size() > 3)
	{
		mass = parameters->RequireType(3, GamecodeValue::T_Number).GetFloat();
	}

	IDynamicsObject *object = dynamics->AddObject(nullptr, *(position->GetObject()), motion, *(boxSize->GetObject()), nullptr, mass);
	GamecodeDynamicsObject *gcdo = new GamecodeDynamicsObject(dynamics, object);
	object->SetUserData(gcdo);
	return GamecodeValue(gcdo);
}

GamecodeValue GamecodeDynamics::RemoveObject(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	GamecodeDynamicsObject *object = parameters->RequireEngineType<GamecodeDynamicsObject>(0, "DynamicsObject");
	object->Remove();
	return GamecodeValue();
}

GamecodeValue GamecodeDynamics::AddStaticObjectProvider(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	IStaticObjectProvider *provider = parameters->RequireEngineType<IStaticObjectProvider>(0, "StaticObjectProvider");
	dynamics->AddStaticObjectProvider(provider);

	IGamecodeEngineObject *obj = parameters->at(0).GetEngine();
	staticObjectProviders.insert(std::pair<IGamecodeEngineObject*, IGamecodeEngineObject*>(obj, obj));

	return GamecodeValue();
}

GamecodeValue GamecodeDynamics::RemoveStaticObjectProvider(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	IStaticObjectProvider *provider = parameters->RequireEngineType<IStaticObjectProvider>(0, "StaticObjectProvider");
	dynamics->RemoveStaticObjectProvider(provider);

	IGamecodeEngineObject *obj = parameters->at(0).GetEngine();
	staticObjectProviders.erase(obj);

	return GamecodeValue();
}

GamecodeValue GamecodeDynamics::SetProperty(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(2);
	std::string key = parameters->RequireType(0, GamecodeValue::T_String).GetString();
	GamecodeValue value = parameters->RequireType(1, GamecodeValue::T_Number);
	double d = value.GetDouble();
	dynamics->SetProperty(key, d);
	return GamecodeValue();
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
