/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <map>
#include "gamecode/IGamecodeEngineObject.hh"
#include "dynamics/IStaticObjectProvider.hh"
#include "dynamics/DynamicsHeightmapData.hh"
#include "terrain/GamecodeTerrain.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * A terrain static object provider.
 * Hooks a Terrain up to dynamics. We pretty much just construct it from a
 * GamecodeTerrain.
 */
class GamecodeTerrainDynamics : public IGamecodeEngineObject, public IStaticObjectProvider {
public:
	GamecodeTerrainDynamics(GamecodeTerrain *terrain) : terrain(terrain) {}
	~GamecodeTerrainDynamics();

	// IGamecodeEngineObject
	//void SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode);
	//GamecodeValue GetProperty(std::string key, IGamecode *gamecode);
	//void SetIndex(int i, GamecodeValue val, IGamecode *gamecode);
	//GamecodeValue GetIndex(int i, IGamecode *gamecode);

	static IGamecodeEngineObject* Construct(GamecodeParameterList *parameters, IGamecode *gamecode);

	// IStaticObjectProvider
	void GetObjectsForVolume(std::vector<Box3F> *bounds, std::vector<IStaticObject*> *objects);
	void FreeObject(IStaticObject *object);

private:
	GamecodeTerrain *terrain;

	struct TerrainStaticObject : public IStaticObjectProvider::IStaticObject, public IStaticObjectProvider::StaticObjectData {
		IStaticObjectProvider::StaticObjectData *GetStaticObjectData() { return this; }
	};

	struct ChunkData {
		TerrainChunk *terrain;
		TerrainStaticObject *object;
		Array2D<char> *diagonals;

		ChunkData(TerrainChunk *terrain, TerrainStaticObject *object, Array2D<char> *diagonals) : terrain(terrain), object(object), diagonals(diagonals) {}
	};
	std::map<Vector2I, ChunkData> chunks;
	std::map<IStaticObject*, Vector2I> chunksByObject;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

