/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "log/ThreadsafeLogWrapper.hh"
#include <stdexcept>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

ThreadsafeLogWrapper::~ThreadsafeLogWrapper()
{
}

ILogTarget* ThreadsafeLogWrapper::Start(MessageType type)
{
	return new InnerWrapper(this, allocator());
}

ILogTarget* ThreadsafeLogWrapper::Code(int code)
{
	throw std::logic_error("ThreadsafeLogWrapper::Code should not get called - we forgot to Start() first");
}

ILogTarget* ThreadsafeLogWrapper::Write(std::string message)
{
	throw std::logic_error("ThreadsafeLogWrapper::Write should not get called - we forgot to Start() first");
}

ILogTarget* ThreadsafeLogWrapper::End()
{
	throw std::logic_error("ThreadsafeLogWrapper::End should not get called - we forgot to Start() first");
}

void ThreadsafeLogWrapper::DisposeWrapper(InnerWrapper *wrapper)
{
    deallocator(wrapper->GetChild());
    delete wrapper;
}

ThreadsafeLogWrapper::InnerWrapper::~InnerWrapper()
{
}

ThreadsafeLogWrapper::InnerWrapper::ILogTarget* Start(MessageType type)
{
	throw std::logic_error("InnerWrapper::Start should not get called - we forgot to End() first");
}

ThreadsafeLogWrapper::InnerWrapper::ILogTarget* Code(int code)
{
	child->Code(code);
	return this;
}

ThreadsafeLogWrapper::InnerWrapper::ILogTarget* Write(std::string message)
{
	child->Write(message);
	return this;
}

ThreadsafeLogWrapper::InnerWrapper::ILogTarget* End()
{
	// delete this wrapper and return parent, so future calls can be made
	ThreadsafeLogWrapper *tslw = parent;
	tslw->DisposeWrapper(this);
	return tslw;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
