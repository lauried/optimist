/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "log/DummyLog.hh"
#include <string>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

ILogTarget* DummyLog::Start(MessageType type)
{
	if (started)
	{
		End();
	}

	std::cout << "[" << system << "][";
	switch(type)
	{
	case ILogTarget::ERROR:
		std::cout << "error";
		break;
	case ILogTarget::WARNING:
		std::cout << "warning";
		break;
	case ILogTarget::NOTIFICATION:
	default:
		std::cout << "notification";
		break;
	}

	std::cout << "] ";
	started = true;
	return this;
}

ILogTarget* DummyLog::Code(int code)
{
	if (!started)
	{
		Start(ILogTarget::NOTIFICATION);
	}
	std::cout << "[" << code << "] ";
	return this;
}

ILogTarget* DummyLog::Write(std::string message)
{
	if (!started)
	{
		Start(ILogTarget::NOTIFICATION);
	}
	std::cout << message;
	return this;
}

ILogTarget* DummyLog::End()
{
	std::cout << std::endl;
	started = false;
	return this;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
