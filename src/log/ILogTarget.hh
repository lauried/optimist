/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class ILogTarget {
public:
	enum MessageType {
		DEBUG,
		NOTIFICATION,
		WARNING,
		ERROR
	};

	virtual ~ILogTarget();

	/**
	 * Starts a message line.
	 * Returns an ILogTarget which is ready to accept further calls.
	 * Note that this does not necessarily return 'this' though it may.
	 *
	 * @param MessageType type The type (or level) of the message.
	 * @return ILogTarget An object which will receive further log target calls
	 *                    regarding the same context as this one.
	 */
	virtual ILogTarget* Start(MessageType type) = 0;

	/**
	 * Sets the current message's code. The code is specific to the system
	 * writing the message. It is to aid other code to determine the meaning
	 * of the message.
	 *
	 * @param int code The message code, whose meaning is determined by the
	 *                 system writing the message.
	 * @return ILogTarget An object which will receive further log target calls
	 *                    regarding the same context as this one.
	 */
	virtual ILogTarget* Code(int code) = 0;

	/**
	 * Appends to a message line.
	 * Returns an ILogTarget which is ready to accept further calls.
	 * Note that this does not necessarily return 'this' though it may.
	 *
	 * @param string message Text to be appended to the message.
	 * @return ILogTarget An object which will receive further log target calls
	 *                    regarding the same context as this one.
	 */
	virtual ILogTarget* Write(std::string message) = 0;

	/**
	 * Ends the message line.
	 * Returns an ILogTarget which is ready to accept further calls.
	 * Note that this does not necessarily return 'this' though it may.
	 *
	 * @return ILogTarget An object which will receive further log target calls
	 *                    regarding the same context as this one.
	 */
	virtual ILogTarget* End() = 0;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
