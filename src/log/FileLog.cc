/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "log/FileLog.hh"
#include <chrono>
#include <iomanip>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

FileLog::~FileLog()
{
}

ILogTarget* FileLog::Start(MessageType type)
{
	if (started)
	{
		End();
	}

	std::time_t rawTime;
	std::time(&rawTime);
	std::tm *tm = std::localtime(&rawTime);

	// timestamp (note: why does it say 0117 as the year?
	message << "[" << std::setfill('0')
		<< std::setw(4) << (tm->tm_year + 1900)
		<< "-"
		<< std::setw(2) << tm->tm_mon
		<< "-"
		<< std::setw(2) << tm->tm_mday
		<< " "
		<< std::setw(2) << tm->tm_hour
		<< ":"
		<< std::setw(2) << tm->tm_min
		<< ":"
		<< std::setw(2) << tm->tm_sec
		<< "] ";

	started = true;
	return this;
}

ILogTarget* FileLog::Code(int code)
{
	message << "[" << code << "] ";
	return this;
}

ILogTarget* FileLog::Write(std::string message)
{
	this->message << message;
	return this;
}

ILogTarget* FileLog::End()
{
	if (message.str().size() > 0)
	{
		message << std::endl;
		std::string str = message.str();
		file->WriteString(str);
		file->Flush();
		message.str(std::string());
		message.clear();
	}
	started = false;
	return this;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
