/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "log/ILogTarget.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Creates new instances of a log target on each Start() call, so that messages
 * are built independently.
 */
class ThreadsafeLogWrapper : public ILogTarget {
	class InnerWrapper;
public:
	typedef std::function<ILogTarget*()> Allocator;
	typedef std::function<void(ILogTarget*)> Deallocator;

	ThreadsafeLogWrapper(Allocator allocator, Deallocator deallocator)
		: allocator(allocator)
		, deallocator(deallocator)
		{}

	virtual ~ThreadsafeLogWrapper();
	virtual ILogTarget* Start(MessageType type);
	virtual ILogTarget* Code(int code);
	virtual ILogTarget* Write(std::string message);
	virtual ILogTarget* End();

	void DisposeWrapper(InnerWrapper *wrapper);

private:
	class InnerWrapper : public ILogTarget {
	public:
		InnerWrapper(ThreadsafeLogWrapper *parent, ILogTarget *child)
			: parent(parent)
			, child(child)
			{}
		virtual ~InnerWrapper();
		virtual ILogTarget* Start(MessageType type);
		virtual ILogTarget* Code(int code);
		virtual ILogTarget* Write(std::string message);
		virtual ILogTarget* End();

		ILogTarget* GetChild() { return child; }
	private:
		ThreadsafeLogWrapper *parent;
		ILogTarget *child;
	};

	Allocator allocator;
	Deallocator deallocator;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

