/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "scratchpad/Main.hh"

#include <iostream>

#include "minecraft/NbtFile.hh"
#include "minecraft/NbtDumper.hh"
#include "filesystem/PlatformFilesystem.hh"
#include "filesystem/File.hh"
#include "log/DummyLog.hh"

using namespace opti;

int main(int argc, char *argv[])
{
	std::string filename("bigtest.nbt");

	PlatformFilesystem filesystem;
	File file(&filesystem, filename);

	NbtFile nbt;
	nbt.Load(file.file);

	NbtDumper dumper;
	dumper.DumpFile(nbt);

	return 0;
}
