/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <list>
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Lets us generate and free IDs, making sure we never reuse them.
 * Freed items are stored on a list and reused first, prior to increasing
 * the list size.
 */
template <class T>
class IdPool {
public:
	IdPool() : maxId(0) {}
	IdPool(T initialId) : maxId(initialId) {}

	T Get()
	{
		if (freeIds.size() > 0)
		{
			T id = freeIds.front();
			freeIds.pop_front();
			return id;
		}
		maxId += 1;
		return maxId;
	}

	void Free(T id)
	{
		freeIds.push_back(id);
		// shrink the max ID if we freed the highest
		// scanning the entire free list might be too expensive
		if (id == maxId + 1)
		{
			maxId = id;
		}
	}

private:
	std::list<T> freeIds;
	T maxId;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

