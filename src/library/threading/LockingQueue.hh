/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <mutex>
#include <stdexcept>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * A queue for passing data between threads.
 * The idea is that senders will never block receivers and vice versa, so a
 * single sender or a single receiver will never be blocked.
 */
template <typename T, int SIZE>
class LockingQueue {
public:
	LockingQueue() : front(0), back(0) {}

	/**
	 * Pushes an item onto the front of the queue.
	 * Returns true if there was sufficient space and the item was pushed,
	 * otherwise returns false.
	 */
	bool Push(T item)
	{
		writeMutex.lock();

		int nextBack = (back + 1) % SIZE;
		if (nextBack == front)
		{
			writeMutex.unlock();
			return false;
		}

		data[nextBack] = item;
		back = nextBack;

		writeMutex.unlock();
		return true;
	}

	/**
	 * Pops an item off the back of the queue.
	 * Returns true if the item was popped and written to output,
	 * otherwise returns false.
	 */
	bool Pop(T* output)
	{
		readMutex.lock();

		if (front == back)
		{
			readMutex.unlock();
			return false;
		}

		front = (front + 1) % SIZE;
		*output = data[front];

		readMutex.unlock();
		return true;
	}

private:
	T data[SIZE];
	// We push onto the back and pop from the front.
	int front, back;
	std::mutex readMutex;
	std::mutex writeMutex;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

