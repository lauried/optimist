/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <mutex>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

template <class T>
class ReferenceCounter {
public:
	class Reference {
	public:
		Reference(T* object, ReferenceCounter<T> *counter) : object(object), counter(counter)
		{
			// already incremented
		}

		Reference(const Reference& other)
		{
			object = other.object;
			counter = other.counter;
			counter->Increment();
		}

		~Reference()
		{
			counter->Decrement();
		}

		T* Object()
		{
			return object;
		}

	private:
		T* object;
		ReferenceCounter<T> *counter;
	};

	ReferenceCounter()
	{
	}

	ReferenceCounter(std::function<void()> callback)
	{
		onNoReferences = callback;
	}

	// TODO: Prevent setting a reference counter after we've created references.
	// TODO: Copying a reference counter is bad.
	void Set(T *object)
	{
		this->object = object;
	}

	Reference MakeReference()
	{
		Increment();
		return Reference(object, this);
	}

	void Increment()
	{
		mutex.lock();
		references += 1;
		mutex.unlock();
	}

	void Decrement()
	{
		mutex.lock();
		references -= 1;
		mutex.unlock();

		if (references == 0)
		{
			NoReferences();
		}
	}

private:
	T* object;
	int references;
	std::mutex mutex;
	std::function<void()> onNoReferences;

	void NoReferences()
	{
		if (onNoReferences != nullptr)
		{
			onNoReferences();
		}
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
