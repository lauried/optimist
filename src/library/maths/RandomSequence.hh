/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <limits>
#include "library/geometry/Vector2.hh"
#include "library/geometry/Vector3.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class RandomSequence {
public:
	RandomSequence()
	{
		state.u = 12;
	}

	RandomSequence(int32_t seed)
	{
		Seed(seed);
	}

	void Seed(int32_t seed)
	{
		state.s = seed;
	}

	int32_t GetInt()
	{
		Iterate();
		return state.s;
	}

	int32_t GetInt(int min, int max)
	{
		Iterate();
		return min + (state.u % max - min);
	}

	double GetDouble()
	{
		Iterate();
		return (double)state.u / (double)std::numeric_limits<uint32_t>::max();
	}

	float GetFloat()
	{
		return (float)GetDouble();
	}

	double GetDoubleSigned()
	{
		Iterate();
		return (double)state.s / (double)std::numeric_limits<int32_t>::max();
	}

	float GetFloatSigned()
	{
		return (float)GetDoubleSigned();
	}

	Vector2F GetVector2F()
	{
		Iterate();
		return Vector2F(GetFloatSigned(), GetFloatSigned());
	}

	Vector3F GetVector3F()
	{
		Iterate();
		return Vector3F(GetFloatSigned(), GetFloatSigned(), GetFloatSigned());
	}

private:

	union {
		uint32_t u;
		int32_t s;
	} state;

	static inline uint32_t Lcg(uint32_t input)
	{
		uint64_t tempValue = ((uint64_t)1103515245 * (uint64_t)input) + (uint64_t)12345;
		return tempValue & 0xffffffff;
	}

	void Iterate()
	{
		state.u = Lcg(state.u);
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
