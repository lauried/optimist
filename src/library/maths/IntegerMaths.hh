/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <inttypes.h>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class IntegerMaths {
public:
	/**
	 * Returns true if the integer is a power of two, else false.
	 */
	static bool IsPowerOfTwo(int i)
	{
		return i && !(i & (i - 1));
	}

	// TODO: All the other bit twiddling stuff can go here:
	// http://graphics.stanford.edu/~seander/bithacks.html
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
