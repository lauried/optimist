/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "library/geometry/Vector2.hh"
#include "library/maths/AbstractSplit.hh"
#include <iostream>
#include <vector>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Splits a rectangular region into portions that lie on a grid.
 */
class GridSplit : public AbstractSplit {
public:
	struct BoundInfo {
		Vector2I coordMin, coordMax;
		Vector2F fracMin, fracMax;
	};

	/**
	 * Outputs a bunch of rectangular regions such that the regions don't intersect
	 * a grid.
	 */
	void GenerateBounds(int startX, int startY, int endX, int endY, int grid, std::vector<BoundInfo> *bounds)
	{
		int curY = startY;
		float curFracY = CurrentFraction(curY, grid);
		int nextY;
		float nextFracY;

		while (curY < endY)
		{
			if (curFracY == 1.0f)
			{
				curFracY = 0.0f;
			}

			NextPosAndFraction(curY, grid, endY, &nextY, &nextFracY);

			int curX = startX;
			float curFracX = CurrentFraction(curX, grid);
			int nextX;
			float nextFracX;

			while (curX < endX)
			{
				if (curFracX == 1.0f)
				{
					curFracX = 0.0f;
				}

				NextPosAndFraction(curX, grid, endX, &nextX, &nextFracX);
				BoundInfo info;
				info.coordMin.Set(curX, curY);
				info.coordMax.Set(nextX, nextY);
				info.fracMin.Set(curFracX, curFracY);
				info.fracMax.Set(nextFracX, nextFracY);

				bounds->push_back(info);

				curX = nextX;
				curFracX = nextFracX;
			}

			curY = nextY;
			curFracY = nextFracY;
		}
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

