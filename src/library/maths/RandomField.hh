/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <limits>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

static const int32_t MAX_INT_32_VALUE = std::numeric_limits<int32_t>::max();

class RandomField {
public:
	/// Linear Congruential Generator - a single iteration of the LCG used
	/// by gcc (32 bit). Using this for procedural generation guarantees
	/// the same results on any platform.
	static inline uint32_t Lcg(uint32_t input)
	{
		uint64_t tempValue = ((uint64_t)1103515245 * (uint64_t)input) + (uint64_t)12345;
		return tempValue & 0xffffffff;
	}

	// Sample from a random field of various dimensions.
	static inline int32_t Calculate(int32_t x)
	{
		int32_t val = WrappedLcg(x) ^ seed1;
		return WrappedLcg(val);
	}
	static inline int32_t Calculate(int32_t x, int32_t y)
	{
		int32_t val = WrappedLcg(x)       ^ seed1;
				val = WrappedLcg(y + val) ^ seed2;
		return WrappedLcg(val);
	}
	static inline int32_t Calculate(int32_t x, int32_t y, int32_t z)
	{
		int32_t val = WrappedLcg(x)       ^ seed1;
				val = WrappedLcg(y + val) ^ seed2;
				val = WrappedLcg(z + val) ^ seed3;
		return WrappedLcg(val);
	}
	static inline int32_t Calculate(int32_t x, int32_t y, int32_t z, int32_t w)
	{
		int32_t val = WrappedLcg(x)       ^ seed1;
				val = WrappedLcg(y + val) ^ seed2;
				val = WrappedLcg(z + val) ^ seed3;
				val = WrappedLcg(w + val) ^ seed4;
		return WrappedLcg(val);
	}

	// Wrappers for float. The range is -1 to 1, simply by scaling the int
	// version down. Float and double will correspond to the same value,
	// but float loses data (down to 24 bits).
	static inline float CalculateF(int32_t x)
	{
		// TODO: Find the right constant in <stdint.h>
		return (float)Calculate(x) / (float)MAX_INT_32_VALUE;
	}
	static inline float CalculateF(int32_t x, int32_t y)
	{
		return (float)Calculate(x, y) / (float)MAX_INT_32_VALUE;
	}
	static inline float CalculateF(int32_t x, int32_t y, int32_t z)
	{
		return (float)Calculate(x, y, z) / (float)MAX_INT_32_VALUE;
	}
	static inline float CalculateF(int32_t x, int32_t y, int32_t z, int32_t w)
	{
		return (float)Calculate(x, y, z, w) / (float)MAX_INT_32_VALUE;
	}

	// Wrappers for double. The range is -1 to 1, simply by scaling the int
	// version down. Float and double will correspond to the same value,
	// but float loses data and double has some unused bits.
	static inline double CalculateD(int32_t x)
	{
		return (double)Calculate(x) / (double)MAX_INT_32_VALUE;
	}
	static inline double CalculateD(int32_t x, int32_t y)
	{
		return (double)Calculate(x, y) / (double)MAX_INT_32_VALUE;
	}
	static inline double CalculateD(int32_t x, int32_t y, int32_t z)
	{
		return (double)Calculate(x, y, z) / (double)MAX_INT_32_VALUE;
	}
	static inline double CalculateD(int32_t x, int32_t y, int32_t z, int32_t w)
	{
		return (double)Calculate(x, y, z, w) / (double)MAX_INT_32_VALUE;
	}


private:

	// TODO: We could possibly change the seed values to change the set of
	// random data we get.
	static const uint32_t seed0 = 0x44ca1571;
	static const  int32_t seed1 = 0xe35dc708;
	static const  int32_t seed2 = 0x5019c3ba;
	static const  int32_t seed3 = 0x193b2a36;
	static const  int32_t seed4 = 0xdf6d195f;

	static inline int32_t WrappedLcg(int32_t value)
	{
		return Lcg(value ^ seed0);
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
