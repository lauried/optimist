/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <list>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * An object pool provides the consumer with a means to obtain and dispose of
 * POD structs.
 *
 * A SimpleObjectPool just allocates objects individually.
 */
template <typename T>
class SimpleObjectPool {
public:
	/**
	 * Returns an available object. Its current data is memset to zeros, so the
	 * consumer is responsible for initializing the object in any other way.
	 * The pointer is guaranteed to be valid until it is freed.
	 */
	T *Allocate()
	{
		return new T();
	}

	/**
	 * Frees the object. This must only be called on the same pool as from
	 * which the object was created.
	 */
	void Free(T *object)
	{
		delete object;
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
