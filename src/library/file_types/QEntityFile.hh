/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <string>
#include <vector>
#include <map>
#include "library/geometry/Vector3.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Queryable representation of Quake's entities, quoted key-value pairs grouped
 * into entities by curly braces.
 */
class QEntityFile {
public:
	/**
	 * Constructs from text data, which need not be zero terminated.
	 * Data is copied so the source can be discarded.
	 */
	QEntityFile() {}
	QEntityFile(const char *data, size_t length);
	~QEntityFile();

	/**
	 * Quake entity object contains key-value pairs.
	 */
	class Entity {
	public:
		/**
		 * Returns the value of the specified key as a string, or an empty
		 * string if there was no such key.
		 */
		std::string GetString(std::string key) const;
		/**
		 * Returns the value of the specified key as a float, or zero if there
		 * was no such key.
		 */
		float GetFloat(std::string key) const;
		/**
		 * Returns the value of the specified key as a Vector3F, or (0, 0, 0)
		 * if there was no such key.
		 */
		Vector3F GetVector(std::string key) const;

		void Add(std::string key, std::string value);
		void Add(std::string key, float value);
		void Add(std::string key, const Vector3F &value);
		void Remove(std::string key);
	private:
		std::map<std::string, std::string> data;
	};

	/**
	 * Returns the number of entities in the file.
	 */
	size_t NumEntities() const { return entities.size(); }
	/**
	 * Provides writable access to the entities in the file.
	 */
	Entity &GetEntity(size_t i) { return *entities.at(i); }
	/**
	 * Provides const access to the entities in the file.
	 */
	const Entity &GetEntity(size_t i) const { return *entities.at(i); }

	/**
	 * Adds another entity and returns its index.
	 */
	int AddEntity();
	/**
	 * Removes the entity at the given position.
	 * No entities with a lower index will be modified.
	 */
	void RemoveEntity(size_t i);

	/**
	 * Move all entities to another entityfile, leaving this one empty.
	 */
	void MoveEntitiesTo(QEntityFile &other);

private:
	std::vector<Entity*> entities;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
