/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <vector>
#include <string>
#include "library/geometry/Vector3.hh"
#include "library/geometry/Orientation.hh"
#include "library/text/TextParser.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Parses and represents a Biovision Hierarchy (BVH) file.
 */
class BvhFile {
public:
	BvhFile();

	BvhFile(const char *data, int length);
	BvhFile(const char *filename);

	bool ParseFromText(const char *data, int length);

	/**
	 * Returns true if valid data was loaded, else false.
	 */
	bool IsValid() const { return valid; }

	struct Joint {
		int parent; ///< The index of the joint which is this parent, or -1 if it's root.
		std::string name; ///< The name of the joint. End sites have no name.
		bool isEndSite;   ///< True if the joint is an end site, else false.
		Vector3F offset;
		int positionMap[3]; ///< Channel numbers for the components of position, -1 if there's no channel for it.
		int rotationMap[3]; ///< Channel numbers for the components of rotation, -1 if there's no channel for it.
		Joint() : parent(0), isEndSite(false), positionMap{-1, -1, -1}, rotationMap{-1, -1, -1} { }
	};

	int NumJoints() const { return joints.size(); }
	const Joint& GetJoint(size_t i) const { return joints.at(i); }

	int NumChannels() const { return numChannels; }
	int NumFrames() const { return numFrames; }
	float FrameTime() const { return frameTime; }

	/**
	 * The raw channel data.
	 * There are NumFrames() * NumChannels() values.
	 * Each frame is adjacent.
	 */
	const float *ChannelData() const { return &channelData[0]; }

	/**
	 * Orientations are in BVH's coordinate system, in model space.
	 * +Y is up, +X is left, +Z is forward.
	 */
	void SetJointPositions(int frame, OrientationF *orientations, int numOrientations) const;

	// TODO: Interpolate between two frames, interpolating between the two
	// source frames data to build the orientations.

private:
	bool valid;

	std::vector<Joint> joints;

	int numChannels;
	int numFrames;
	float frameTime;
	std::vector<float> channelData;

	void ParseJoint(TextParser &lines, Joint &joint, int index);
	void ParseEndSite(TextParser &lines, Joint &joint, int index);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
