/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <cstdlib>
#include <cinttypes>
#include <string>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Represents a Quake3 BSP file, as the data is stored on disk.
 */
class Q3BspFile {
public:
	/**
	 * Construct from raw data. Here we act as an interpretation of the
	 * underlying data and so don't actually take ownership of it.
	 */
	Q3BspFile(uint8_t *data, size_t length);
	/**
	 * Load from file. Here we allocate the data ourself.
	 */
	Q3BspFile(const char *filename);

	~Q3BspFile();

	/**
	 * Returns true if the data is completely valid with no incorrect indexes,
	 * otherwise false.
	 */
	bool IsValid() { return valid; }

	/**
	 * If the data is not valid, returns a string explaining why. Otherwise it
	 * will say something like "Loaded" or "Valid".
	 */
	std::string GetStatus() { return status; }

	struct Header {
		char magicNumber[4];
		int32_t version;
		struct {
			uint32_t offset;
			uint32_t length;
		} lumps[17];
	};

	struct Texture {
		char name[64];
		int32_t flags;
		int32_t contents;
	};

	struct Plane {
		float normal[3];
		float distance;
	};

	struct Node {
		int32_t planeIndex;
		int32_t children[2]; ///< Negative numbers are leaf indexes: -(leaf+1)
		int32_t mins[3];
		int32_t maxs[3];
	};

	struct Leaf {
		int32_t cluster; ///< If cluster is negative, the leaf is invalid.
		int32_t area;
		int32_t mins[3];
		int32_t maxs[3];
		int32_t leafFace;       ///< Index into leafFaces.
		int32_t numLeafFaces;   ///< Run of leafFaces in the array.
		int32_t leafBrush;      ///< Index into leafBrushes.
		int32_t numLeafBrushes; ///< Run of leafbrushes in the array.
	};

	struct Model {
		float mins[3];
		float maxs[3];
		int32_t face; ///< Index into faces.
		int32_t numFaces;
		int32_t brush; ///< Index into brushes.
		int32_t numBrushes;
	};

	struct Brush {
		int32_t brushSide;
		int32_t numBrushSides;
		int32_t texture;
	};

	struct BrushSide {
		int32_t planeIndex;
		int32_t texture;
	};

	struct Vertex {
		float position[3];
		float texCoord[2][2]; ///< 0 = surface, 1 = lightmap
		float normal[3];
		uint8_t color[4];
	};

	struct Effect {
		char name[64];
		int32_t brush;   ///< Brush that generated this effect.
		int32_t unknown; ///< Specs did not say what this does.
	};

	struct Face {
		int32_t texture;
		int32_t effect;
		int32_t type; ///< 1 = polygon, 2 = patch, 3 = mesh, 4 = billboard
		int32_t vertex;
		int32_t numVertexes;
		int32_t meshVert;
		int32_t numMeshVerts;
		int32_t lightmap;
		int32_t lightmapStart[2];
		int32_t lightmapSize[2];
		float lightmapOrigin[3];
		float lightmapVectors[2][3];
		float normal[3];
		int32_t size[2]; ///< patch dimensions

		/*
		For type 1 faces (polygons), vertex and n_vertexes describe a set of
		vertices that form a polygon. The set always contains a loop of
		vertices, and sometimes also includes an additional vertex near the
		center of the polygon. For these faces, meshvert and n_meshverts
		describe a valid polygon triangulation. Every three meshverts describe
		a triangle. Each meshvert is an offset from the first vertex of the
		face, given by vertex.

		For type 2 faces (patches), vertex and n_vertexes describe a 2D
		rectangular grid of control vertices with dimensions given by size.
		Within this rectangular grid, regions of 3×3 vertices represent
		biquadratic Bezier patches. Adjacent patches share a line of three
		vertices. There are a total of (size[0] - 1) / 2 by (size[1] - 1) / 2
		patches. Patches in the grid start at (i, j) given by:

		i = 2n, n in [ 0 .. (size[0] - 1) / 2 ), and
		j = 2m, m in [ 0 .. (size[1] - 1) / 2 ).

		For type 3 faces (meshes), meshvert and n_meshverts are used to
		describe the independent triangles that form the mesh. As with type 1
		faces, every three meshverts describe a triangle, and each meshvert is
		an offset from the first vertex of the face, given by vertex.

		For type 4 faces (billboards), vertex describes the single vertex that
		determines the location of the billboard. Billboards are used for
		effects such as flares. Exactly how each billboard vertex is to be
		interpreted has not been investigated.
		*/
	};

	struct Lightmap {
		uint8_t map[128][128][3];
	};

	struct LightVol {
		uint8_t ambient[3];
		uint8_t directional[3];
		uint8_t dir[2]; ///< 0 = phi, 1 = theta
	};

	struct VisData {
		int32_t numVecs;
		int32_t vecSize;
		uint8_t vecs[1]; ///< Actually sized numVecs * vecSize.
	};

	char *entities;
	int entitiesSize;

	Texture *textures;
	int numTextures;

	Plane *planes;
	int numPlanes;

	Node *nodes;
	int numNodes;

	Leaf *leaves;
	int numLeaves;

	int32_t *leafFaces; ///< Index into faces.
	int numLeafFaces;

	int32_t *leafBrushes; ///< Index into brushes.
	int numLeafBrushes;

	Model *models;
	int numModels;

	Brush *brushes;
	int numBrushes;

	BrushSide *brushSides;
	int numBrushSides;

	Vertex *vertexes;
	int numVertexes;

	int32_t *meshVerts;
	int numMeshVerts;

	Effect *effects;
	int numEffects;

	Face *faces;
	int numFaces;

	Lightmap *lightmaps;
	int numLightmaps;

	LightVol *lightVols;
	int numLightVols;

	VisData *visData;

private:
	uint8_t *rawData;
	int dataSize;
	bool ownData; ///< If we allocated the data ourself or not.
	bool valid;

	std::string status;

	void Clear();
	void SetupPointers();
	void CheckData();
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
