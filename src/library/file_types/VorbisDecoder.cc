/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "library/file_types/VorbisDecoder.hh"
#include <stdexcept>
#include <vector>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

VorbisDecoder::VorbisDecoder(const unsigned char *data, size_t size) : finished(false)
{
	const stb_vorbis_alloc *NO_ALLOC = nullptr;
	int error;
	vorbis = stb_vorbis_open_memory(data, size, &error, NO_ALLOC);
	// If vorbis == nullptr then some kind of error occurred.
	// We'll simply automatically finish and never reset.
}

VorbisDecoder::~VorbisDecoder()
{
	if (vorbis != nullptr)
	{
		stb_vorbis_close(vorbis);
	}
}

void VorbisDecoder::RenderToBuffer(AudioBuffer *buffer)
{
	if (vorbis == nullptr)
	{
		lastRenderedSamples = 0;
		finished = true;
		return;
	}

	int numChannels = buffer->NumChannelsPerSample();
	int bufferSize = buffer->NumSamples() * numChannels;
	std::vector<float> samples(bufferSize);
	int numSamplesReturned = stb_vorbis_get_samples_float_interleaved(vorbis, numChannels, &samples[0], bufferSize);

	float *p = &samples[0];
	for (int sampleIndex = 0; sampleIndex < numSamplesReturned; sampleIndex += 1)
	{
		for (int channelIndex = 0; channelIndex < numChannels; channelIndex += 1)
		{
			buffer->SetSample(sampleIndex, channelIndex, *p++);
		}
	}

	lastRenderedSamples = numSamplesReturned;

	if (numSamplesReturned < buffer->NumSamples())
	{
		finished = true;
	}
}

bool VorbisDecoder::Reset()
{
	if (vorbis == nullptr)
	{
		return false;
	}

	stb_vorbis_seek_start(vorbis);
	finished = false;
	return true;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
