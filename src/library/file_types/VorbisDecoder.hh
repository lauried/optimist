/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <cstddef>
#include "audio/IAudioSource.hh"
#include "library/extern/stb_vorbis.h"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Decodes a vorbis stream. Doesn't resample its output - that can be done with
 * a general purpose resampling audio source.
 */
class VorbisDecoder : public IAudioSource {
public:
	/**
	 * Constructs from a buffer containing the entire vorbis file.
	 * If the data is malformed in any way, then the source will behave as
	 * though it were zero bytes in length, entering into a 'finished' state
	 * at the first RenderToBuffer call.
	 */
	VorbisDecoder(const unsigned char *pData, size_t size);
	~VorbisDecoder();

	virtual void RenderToBuffer(AudioBuffer *buffer);
	virtual bool Finished() { return finished; };
	virtual int LastRenderedSamples() { return lastRenderedSamples; }
	virtual bool Reset();

private:
	bool finished;
	int lastRenderedSamples;
	stb_vorbis *vorbis;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

