/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "library/file_types/Q3BspFile.hh"
#include <cassert>
#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

const char *gLumpNames[17] = {
	"entities",
	"textures",
	"planes",
	"nodes",
	"leaves",
	"leaffaces",
	"leafbrushes",
	"models",
	"brushes",
	"brushsides",
	"vertexes",
	"meshverts",
	"effects",
	"faces",
	"lightmaps",
	"lightvols",
	"visdata"
};

std::string itos(int i)
{
	std::stringstream ss;
	ss << i;
	return ss.str();
}

Q3BspFile::Q3BspFile(uint8_t *data, size_t length)
{
	rawData  = data;
	ownData  = false;
	dataSize = length;

	Clear();
	SetupPointers();
}

Q3BspFile::Q3BspFile(const char *filename)
{
	rawData = 0;
	ownData = false;
	dataSize = 0;

	Clear();

	std::ifstream in;
	in.open(filename, std::ios::binary);

	in.seekg(0, std::ios::end);
	dataSize = in.tellg();
	in.seekg(0, std::ios::beg);

	if (dataSize <= 0)
		return;

	rawData = new uint8_t[dataSize];
	ownData = true;
	in.read((char *)rawData, dataSize);
	in.close();

	SetupPointers();
}

Q3BspFile::~Q3BspFile()
{
	if (ownData)
		delete[] rawData;
}

// Clear our data for initial loading.
void Q3BspFile::Clear()
{
	entities = 0;
	entitiesSize = 0;

	textures = 0;
	numTextures = 0;

	planes = 0;
	numPlanes = 0;

	nodes = 0;
	numNodes = 0;

	leaves = 0;
	numLeaves = 0;

	leafFaces = 0;
	numLeafFaces = 0;

	leafBrushes = 0;
	numLeafBrushes = 0;

	models = 0;
	numModels = 0;

	brushes = 0;
	numBrushes = 0;

	brushSides = 0;
	numBrushSides = 0;

	vertexes = 0;
	numVertexes = 0;

	meshVerts = 0;
	numMeshVerts = 0;

	effects = 0;
	numEffects = 0;

	faces = 0;
	numFaces = 0;

	lightmaps = 0;
	numLightmaps = 0;

	lightVols = 0;
	numLightVols = 0;

	visData = 0;

	valid = false;
	status = "Not loaded";
}

// Check and set up pointers.
void Q3BspFile::SetupPointers()
{
	Header *header = (Header*)rawData;
	std::stringstream statusMsg;

	if (dataSize < sizeof(Header))
	{
		statusMsg << "Data is too small to even contain a Q3BSP Header: data is "
			<< dataSize << " but a header is " << sizeof(Header);
		status = statusMsg.str();
		return;
	}

	if (header->magicNumber[0] != 'I' ||
		header->magicNumber[1] != 'B' ||
		header->magicNumber[2] != 'S' ||
		header->magicNumber[3] != 'P')
	{
		status = "Not a BSP file: First four bytes should be ascii IBSP";
		return;
	}

	if (header->version != 0x2e)
	{
		status = "Not a Q3 BSP file: version should be 0x2e (46), not ";
		status += header->version;
		return;
	}

#if 1
#define SetupLump(lumpNum, ptr, type, num) \
	if (header->lumps[lumpNum].offset < 0) \
	{ status = #type " lump values out of range"; return; }\
	if (header->lumps[lumpNum].offset + header->lumps[lumpNum].length > dataSize)\
	{ status = #type " lump values out of range"; return; }\
	ptr = (type*)&rawData[header->lumps[lumpNum].offset];\
	num = header->lumps[lumpNum].length / sizeof(type);
#else
#define SetupLump(lumpNum, ptr, type, num) \
	if (header->lumps[lumpNum].offset < 0) \
	{ status = #type " lump values out of range"; return; }\
	if (header->lumps[lumpNum].offset + header->lumps[lumpNum].length > dataSize)\
	{ status = #type " lump values out of range"; return; }\
	ptr = (type*)&rawData[header->lumps[lumpNum].offset];\
	num = header->lumps[lumpNum].length / sizeof(type);\
	std::cout << lumpNum << " (" << gLumpNames[lumpNum] << ") : ofs "\
		<< header->lumps[lumpNum].offset << ", siz " << header->lumps[lumpNum].length << ", num "\
		<< (header->lumps[lumpNum].length / sizeof(type)) << std::endl;
#endif

	if (header->lumps[0].offset < 0)
	{ status = "Entities lump values out of range"; return; }
	if (header->lumps[0].offset + header->lumps[0].length > dataSize)
	{ status = "Entities lump values out of range"; return; }
	entities = (char*)&rawData[header->lumps[0].offset];
	entitiesSize = header->lumps[0].length;

	SetupLump(1,  textures,    Texture,   numTextures);
	SetupLump(2,  planes,      Plane,     numPlanes);
	SetupLump(3,  nodes,       Node,      numNodes);
	SetupLump(4,  leaves,      Leaf,      numLeaves);
	SetupLump(5,  leafFaces,   int32_t,   numLeafFaces);
	SetupLump(6,  leafBrushes, int32_t,   numLeafBrushes);
	SetupLump(7,  models,      Model,     numModels);
	SetupLump(8,  brushes,     Brush,     numBrushes);
	SetupLump(9,  brushSides,  BrushSide, numBrushSides);
	SetupLump(10, vertexes,    Vertex,    numVertexes);
	SetupLump(11, meshVerts,   int32_t,   numMeshVerts);
	SetupLump(12, effects,     Effect,    numEffects);
	SetupLump(13, faces,       Face,      numFaces);
	SetupLump(14, lightmaps,   Lightmap,  numLightmaps);
	SetupLump(15, lightVols,   LightVol,  numLightVols);

	if (header->lumps[16].offset < 0 || header->lumps[16].offset + header->lumps[16].length > dataSize)
	{
		statusMsg << "VisData lump values out of range: offset = "
			<< header->lumps[16].offset << ", length = "
			<< header->lumps[16].length << ", data size is " << dataSize;
		status = statusMsg.str();
		return;
	}

	if (header->lumps[17].length > 0)
	{
		visData = (VisData*)&rawData[header->lumps[16].offset];
		if (visData->vecSize * visData->numVecs > header->lumps[16].length)
		{
			statusMsg << "Visdata isn't the size it said it was: vecsize of "
				<< visData->vecSize << " * numVecs of " << visData->numVecs
				<< " is " << visData->vecSize * visData->numVecs
				<< " and dataSize is " << dataSize;
			status = statusMsg.str();
			return;
		}
	}

#undef SetupLump

	CheckData();
}

// Check through all the data.
void Q3BspFile::CheckData()
{
	std::stringstream statusMsg;

	/*
	Node:
	Check planeIndex references valid plane, and children index valid nodes or
	leafs.
	*/
	for (int i = 0; i < numNodes; ++i)
	{
		if (nodes[i].planeIndex < 0 || nodes[i].planeIndex >= numPlanes)
		{
			statusMsg << "Invalid node[" << i << "].planeIndex of " << nodes[i].planeIndex;
			status = statusMsg.str();
			return;
		}
		for (int j = 0; j < 2; ++j)
		{
			if (nodes[i].children[j] >= numNodes || nodes[i].children[j] < -numLeaves)
			{
				statusMsg << "nodes[" << i << "].children[" << j << "] of " << nodes[i].children[j]
					<< " is invalid - numNodes = " << numNodes << ", numLeaves = "
					<< numLeaves;
				status = statusMsg.str();
				return;
			}
		}
	}

	/*
	Leaf:
	Check cluster is a valid vis cluster if it's not negative.
	Check leaffaces and leafbrushes fit.
	*/
	// TODO: It crashes here somewhere...
	for (int i = 0; i < numLeaves; ++i)
	{
		if (visData != 0 && leaves[i].cluster >= visData->numVecs)
		{
			statusMsg << "Invalid leaves[" << i << "].cluster of " << leaves[i].cluster;
			status = statusMsg.str();
			return;
		}
		if (leaves[i].leafFace + leaves[i].numLeafFaces > numLeafFaces || leaves[i].leafFace < 0)
		{
			statusMsg << "Invalid leaf[" << i << "].leafFace of " << leaves[i].leafFace
				<< " + numLeafFaces of " << leaves[i].numLeafFaces;
			status = statusMsg.str();
			return;
		}
		if (leaves[i].leafBrush + leaves[i].numLeafBrushes > numLeafBrushes || leaves[i].leafBrush < 0)
		{
			statusMsg << "Invalid leaf[" << i << "].leafBrush of " << leaves[i].leafBrush
				<< " + numLeafBrushes of " << leaves[i].numLeafBrushes;
			status = statusMsg.str();
			return;
		}
	}

	/*
	LeafFaces:
	Check they index a valid face.
	*/
	for (int i = 0; i < numLeafFaces; ++i)
	{
		if (leafFaces[i] < 0 || leafFaces[i] >= numFaces)
		{
			statusMsg << "Invalid leafFace[" << i << "] of " << leafFaces[i];
			status = statusMsg.str();
			return;
		}
	}

	/*
	LeafBrushes:
	Check they index a valid brush.
	*/
	for (int i = 0; i < numLeafBrushes; ++i)
	{
		for (int i = 0; i < numLeafBrushes; ++i)
		{
			if (leafBrushes[i] < 0 || leafBrushes[i] >= numBrushes)
			{
				statusMsg << "Invalid leafbrush[" << i << "] of " << leafBrushes[i];
				status = statusMsg.str();
				return;
			}
		}
	}

	/*
	Model:
	Check they index a valid range of faces and brushes.
	*/
	for (int i = 0; i < numModels; ++i)
	{
		if (models[i].face + models[i].numFaces > numFaces || models[i].face < 0)
		{
			statusMsg << "Invalid model[" << i << "].face of " << models[i].face
				<< " + numFaces of " << models[i].numFaces;
			status = statusMsg.str();
			return;
		}
		if (models[i].brush + models[i].numBrushes > numBrushes || models[i].brush < 0)
		{
			statusMsg << "Invalid model[" << i << "].brush of " << models[i].brush
				<< " + numBrushes of " << models[i].numBrushes;
			status = statusMsg.str();
			return;
		}
	}

	/*
	Brush:
	Check they index valid brushsides, and a texture.
	*/
	for (int i = 0; i < numBrushes; ++i)
	{
		if (brushes[i].brushSide + brushes[i].numBrushSides > numBrushSides || brushes[i].brushSide < 0)
		{
			status = "Invalid Brush.brushSide + numBrushSides";
			return;
		}
		if (brushes[i].texture < 0 || brushes[i].texture >= numTextures)
		{
			statusMsg << "Invalid brush[" << i << "].texture of " << brushes[i].texture;
			status = statusMsg.str();
			return;
		}
	}

	/*
	MeshVert:
	Check they index a valid vertex.
	*/
	for (int i = 0; i < numMeshVerts; ++i)
	{
		if (meshVerts[i] < 0 || meshVerts[i] >= numVertexes)
		{
			statusMsg << "Invalid meshVert[" << i << "] of " << meshVerts[i];
			status = statusMsg.str();
			return;
		}
	}

	/*
	Effect:
	Check they index a valid brush.
	*/
	for (int i = 0; i < numEffects; ++i)
	{
		if (effects[i].brush < 0 || effects[i].brush >= numBrushes)
		{
			statusMsg << "Invalid effect[" << i << "].brush of " << effects[i].brush;
			status = statusMsg.str();
			return;
		}
	}

	/*
	Face:
	Check texture/effect/lightmap is valid index,
	type is 1 to 4,
	vertex and meshverts are valid ranges
	*/
	for (int i = 0; i < numFaces; ++i)
	{
		if (faces[i].texture < 0 || faces[i].texture >= numTextures)
		{
			statusMsg << "Invalid face[" << i << "].texture of " << faces[i].texture;
			status = statusMsg.str();
			return;
		}
		if (faces[i].effect >= numEffects)
		{
			// negative values mean no effect
			statusMsg << "Invalid face[" << i << "].effect of " << faces[i].effect;
			status = statusMsg.str();
			return;
		}
		if (faces[i].lightmap >= numLightmaps)
		{
			// negative values mean no lightmap
			statusMsg << "Invalid face[" << i << "].lightmap of " << faces[i].lightmap;
			status = statusMsg.str();
			return;
		}
		if (faces[i].type < 1 || faces[i].type > 4)
		{
			statusMsg << "Invalid face[" << i << "].type of " << faces[i].type
				<< " should be 1, 2, 3 or 4";
			status = statusMsg.str();
			return;
		}
		if (faces[i].vertex + faces[i].numVertexes > numVertexes || faces[i].vertex < 0)
		{
			statusMsg << "Invalid face[" << i << "].vertex of " << faces[i].vertex
				<< " + numVertexes of " << faces[i].numVertexes;
			status = statusMsg.str();
			return;
		}
		if (faces[i].meshVert + faces[i].numMeshVerts > numMeshVerts || faces[i].meshVert < 0)
		{
			statusMsg << "Invalid face[" << i << "].meshVert of " << faces[i].meshVert
				<< " + numMeshVerts of " << faces[i].numMeshVerts;
			status = statusMsg.str();
			return;
		}
	}

	status = "Loaded";
	valid = true;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

