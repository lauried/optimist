/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "library/file_types/OptiFontFile.hh"
#include "library/text/String.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

OptiFontFile::OptiFontFile(const char *csvData, size_t dataLength)
{
	width = 0;
	height = 0;
	lineHeight = 0;
	int position = 0;
	// TODO: Maybe read first line separately.
	while (position < dataLength)
	{
		std::string line = String::ReadLine(csvData, dataLength, &position);
		std::vector<std::string> tokens;
		String::Tokenize(line, &tokens, "\t");

		if (tokens.size() < 2)
		{
			continue;
		}
		if (!tokens[0].compare("OPTIFONT"))
		{
			image = tokens[1];
		}
		if (tokens.size() < 3)
		{
			continue;
		}
		if (!tokens[0].compare("OPTION"))
		{
			if (!tokens[1].compare("line-height"))
			{
				lineHeight = String::ToInt(tokens[2]);
			}
			else if (!tokens[1].compare("width"))
			{
				width = String::ToInt(tokens[2]);
			}
			else if (!tokens[1].compare("height"))
			{
				height = String::ToInt(tokens[2]);
			}
			// We could also include custom kerning rules here (stuff that overrides the advance for specific letter pairs)
			// except that those rules would also require the font interface and render mesh provider to support them.
			continue;
		}
		if (!tokens[0].compare("CHAR"))
		{
			continue;
		}
		if (tokens.size() < 8)
		{
			continue;
		}
		// TODO: char, s0, t0, s1, t1, x, y, adv
		// Allocate and build a glyph.
		int glyph;
		if (String::UTF8ToUnicode(tokens[0], 0, &glyph) > 0)
		{
			GlyphInfo info;
			info.glyph = glyph;
			info.s0 = String::ToInt(tokens[1]);
			info.t0 = String::ToInt(tokens[2]);
			info.s1 = String::ToInt(tokens[3]);
			info.t1 = String::ToInt(tokens[4]);
			info.x = String::ToInt(tokens[5]);
			info.y = String::ToInt(tokens[6]);
			info.adv = String::ToInt(tokens[7]);
			glyphInfo.push_back(info);
		}
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
