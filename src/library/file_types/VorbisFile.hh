/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <cstddef>
#include "filesystem/IFilesystem.hh"
#include "audio/IAudioSource.hh"
#include "library/file_types/IAudioSampleFile.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * A vorbis file just stores the data. We create instances of decoders in order
 * to play the file.
 */
class VorbisFile : public IAudioSampleFile {
public:
	VorbisFile(IFile *file);
	~VorbisFile();

	virtual IAudioSource *GetNewSource();
private:
	unsigned char *data;
	size_t size;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

