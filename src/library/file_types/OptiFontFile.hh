/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <string>
#include <vector>
#include "font/IFont.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class OptiFontFile {
public:
	struct GlyphInfo {
		int glyph;
		int s0, t0, s1, t1, x, y, adv;
	};

	OptiFontFile(const char *csvData, size_t dataLength);

	std::string GetImage() { return image; }
	std::vector<GlyphInfo>& GetGlyphInfo() { return glyphInfo; }
	int GetWidth() { return width; }
	int GetHeight() { return height; }
	int GetLineHeight() { return lineHeight; }

private:
	std::string image;
	int width, height, lineHeight;
	std::vector<GlyphInfo> glyphInfo;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

