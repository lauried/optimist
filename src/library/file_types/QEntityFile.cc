/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "library/file_types/QEntityFile.hh"
#include <iostream>
#include "library/text/TextParser.hh"
#include "library/text/String.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

QEntityFile::QEntityFile(const char *data, size_t length)
{
	QuotedStringsTokenizer tokenizer;
	TextParser parser(data, length, tokenizer);

	try
	{
		// until eof...
		while (!parser.Eof())
		{
			// expect { creating new entity
			parser.ExpectToken("{");
			entities.push_back(new Entity());
			while (true)
			{
				// expect pair of tokens indefinitely until }
				std::string key = parser.GetToken();
				if (!key.compare("}"))
					break;
				std::string value = parser.GetToken();
				entities.at(entities.size() - 1)->Add(String::Trim(key, "\""), String::Trim(value, "\""));
			}
		}
	}
	catch (ParseException &e)
	{
		std::cout << "Parsing entities failed at line " << e.line << ": " << e.message << std::endl;
	}
}

QEntityFile::~QEntityFile()
{
	for (int i = 0; i < entities.size(); ++i)
	{
		delete entities[i];
	}
}

int QEntityFile::AddEntity()
{
	entities.push_back(new Entity());
	return entities.size() - 1;
}

void QEntityFile::RemoveEntity(size_t i)
{
	delete entities.at(i);
	entities.at(i) = entities[entities.size() - 1];
	entities.resize(entities.size() - 1);
}

void QEntityFile::MoveEntitiesTo(QEntityFile &other)
{
	for (int i = 0; i < entities.size(); ++i)
	{
		other.entities.push_back(entities.at(i));
	}
	entities.clear();
}

//-----------------------------------------------------------------------------
// Entity
//-----------------------------------------------------------------------------

void QEntityFile::Entity::Add(std::string key, std::string value)
{
	auto it = data.find(key);
	if (it != data.end())
	{
		it->second = value;
		return;
	}
	data.insert(std::pair<std::string, std::string>(key, value));
}

void QEntityFile::Entity::Add(std::string key, float value)
{
	Add(key, String::ToString(value));
}

void QEntityFile::Entity::Add(std::string key, const Vector3F &value)
{
	std::ostringstream o;
	o << value[0] << " " << value[1] << " " << value[2];
	Add(key, o.str());
}

void QEntityFile::Entity::Remove(std::string key)
{
	auto it = data.find(key);
	if (it != data.end())
	{
		data.erase(it);
	}
}

std::string QEntityFile::Entity::GetString(std::string key) const
{
	auto it = data.find(key);
	if (it != data.end())
		return it->second;
	return "";
}

float QEntityFile::Entity::GetFloat(std::string key) const
{
	return std::atof(GetString(key).c_str());
}

Vector3F QEntityFile::Entity::GetVector(std::string key) const
{
	std::vector<std::string> tokens;
	String::Tokenize(GetString(key), &tokens);
	Vector3F v;
	for (int i = 0; i < 3 && i < tokens.size(); ++i)
	{
		v[i] = std::atof(tokens[i].c_str());
	}
	return v;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

