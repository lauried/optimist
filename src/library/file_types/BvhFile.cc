/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "library/file_types/BvhFile.hh"
#include <cstdlib>
#include <iostream>
#include <fstream>
#include "library/maths/Maths.hh"
#include "library/text/TextParser.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

BvhFile::BvhFile()
{
	valid = false;
}

BvhFile::BvhFile(const char *filename)
{
	valid = false;

	std::ifstream in;
	in.open(filename, std::ios::binary);

	in.seekg(0, std::ios::end);
	int dataSize = in.tellg();
	in.seekg(0, std::ios::beg);

	char *data = new char[dataSize];
	in.read(data, dataSize);
	in.close();

	ParseFromText(data, dataSize);
}

BvhFile::BvhFile(const char *data, int length)
{
	ParseFromText(data, length);
}

bool BvhFile::ParseFromText(const char *data, int length)
{
	joints.resize(0);
	joints.reserve(24);
	numChannels = 0;
	numFrames   = 0;
	frameTime   = 1;
	channelData.resize(0);
	valid = false;

	WhitespaceTokenizer tokenizer;
	TextParser parser(data, length, tokenizer);
	TextParser line;

	try
	{
		// expect 'HIERARCHY'
		parser.ExpectToken("HIERARCHY");

		// expect 'ROOT name'
		line = parser.GetLine();
		line.ExpectToken("ROOT");
		std::string jointName = line.GetToken();
		joints.push_back(Joint());
		joints[joints.size() - 1].name = jointName;
		joints[joints.size() - 1].parent = -1;
		ParseJoint(parser, joints[joints.size() - 1], joints.size() - 1);

		// expect 'MOTION'
		parser.ExpectToken("MOTION");

		// expect 'Frames: ' X
		line = parser.GetLine();
		line.ExpectToken("Frames:");
		numFrames = std::atoi(line.GetToken().c_str());
		line.ExpectEof();

		// expect 'Frame Time: ' X
		line = parser.GetLine();
		line.ExpectToken("Frame");
		line.ExpectToken("Time:");
		frameTime = std::atof(line.GetToken().c_str());
		line.ExpectEof();

		// expect <frames> lines of <numChannels> floats.
		channelData.reserve(numFrames * numChannels);
		for (int l = 0; l < numFrames; ++l)
		{
			line = parser.GetLine();
			for (int c = 0; c < numChannels; ++c)
			{
				channelData.push_back(std::atof(line.GetToken().c_str()));
			}
			line.ExpectEof();
		}
		parser.ExpectEof();

		valid = true;
	}
	catch (ParseException &e)
	{
		std::cout << "Parsing failed at line " << e.line << ": " << e.message << std::endl;
		joints.clear();
		channelData.reserve(0);
	}

	return valid;
}

void BvhFile::ParseJoint(TextParser &parser, Joint &joint, int index)
{
	TextParser line;

	// expect '{'
	line = parser.GetLine();
	line.ExpectToken("{");
	line.ExpectEof();

	while (true)
	{
		// expect 'OFFSET ...' or 'CHANNELS ...'
		// or 'JOINT name' until '}'
		line = parser.GetLine();
		std::string token = line.GetToken();
		if (!token.compare("OFFSET"))
		{
			joint.offset[0] = std::atof(line.GetToken().c_str());
			joint.offset[1] = std::atof(line.GetToken().c_str());
			joint.offset[2] = std::atof(line.GetToken().c_str());
		}
		else if (!token.compare("CHANNELS"))
		{
			int numChannelsForJoint = std::atoi(line.GetToken().c_str());
			for (int i = 0; i < numChannelsForJoint; ++i)
			{
				std::string s = line.GetToken();
				if (!s.compare("Xrotation"))
					joint.rotationMap[0] = numChannels;
				else if (!s.compare("Yrotation"))
					joint.rotationMap[1] = numChannels;
				else if (!s.compare("Zrotation"))
					joint.rotationMap[2] = numChannels;
				else if (!s.compare("Xposition"))
					joint.positionMap[0] = numChannels;
				else if (!s.compare("Yposition"))
					joint.positionMap[1] = numChannels;
				else if (!s.compare("Zposition"))
					joint.positionMap[2] = numChannels;
				++numChannels;
			}
		}
		else if (!token.compare("JOINT"))
		{
			std::string jointName = line.GetToken();
			line.ExpectEof();
			joints.push_back(Joint());
			joints[joints.size() - 1].name = jointName;
			joints[joints.size() - 1].parent = index;
			ParseJoint(parser, joints[joints.size() - 1], joints.size() - 1);
		}
		else if (!token.compare("End"))
		{
			line.ExpectToken("Site");
			line.ExpectEof();
			joints.push_back(Joint());
			joints[joints.size() - 1].parent = index;
			joints[joints.size() - 1].isEndSite = true;
			ParseEndSite(parser, joints[joints.size() - 1], joints.size() - 1);
		}
		else if (!token.compare("}"))
		{
			line.ExpectEof();
			return;
		}
		else
		{
			parser.ThrowCustomParseException("Unexpected token " + token + " in JOINT");
		}
	}
}

void BvhFile::ParseEndSite(TextParser &parser, Joint &joint, int index)
{
	TextParser line;

	line = parser.GetLine();
	line.ExpectToken("{");
	line.ExpectEof();

	while (true)
	{
		// expect 'OFFSET ...' until '}'
		line = parser.GetLine();
		std::string token = parser.GetToken();
		if (!token.compare("OFFSET"))
		{
			joint.offset[0] = std::atof(line.GetToken().c_str());
			joint.offset[1] = std::atof(line.GetToken().c_str());
			joint.offset[2] = std::atof(line.GetToken().c_str());
		}
		else if (!token.compare("}"))
		{
			return;
		}
		else
		{
			parser.ThrowCustomParseException("Unexpected token " + token + " in End Site");
		}
	}
}

void BvhFile::SetJointPositions(int frame, OrientationF *orientations, int numOrientations) const
{
	const float *frameData = &channelData[frame * numChannels];

	int count = (joints.size() < numOrientations) ? joints.size() : numOrientations;
	for (int i = 0; i < count; ++i)
	{
		const Joint *joint = &joints[i];
		OrientationF *ori = orientations + i;

		Vector3F rotation, position;
		for (int axis = 0; axis < 3; ++axis)
		{
			rotation[axis] = (joint->rotationMap[axis] < 0) ? 0 : frameData[joint->rotationMap[axis]];
			position[axis] = (joint->positionMap[axis] < 0) ? 0 : frameData[joint->positionMap[axis]];
		}

		ori->origin[0] = joint->offset[0] + position[0];
		ori->origin[1] = joint->offset[1] + position[1];
		ori->origin[2] = joint->offset[2] + position[2];

		// Rotation order Y X Z apparently.
		// But that's for concatenation, not for 'order in which the rotations
		// are applied'.
		ori->Rotate(OrientationF::Y, Maths::DegreesToRadians(rotation[1]));
		ori->Rotate(OrientationF::X, Maths::DegreesToRadians(rotation[0]));
		ori->Rotate(OrientationF::Z, Maths::DegreesToRadians(rotation[2]));

		// To model space (still bvh coordinate system)
		if (joint->parent >= 0)
		{
			ori->ToExternalSpaceOf(orientations[joint->parent]);
		}

		// There's some translation to be done with bvh: z is vertical and
		// x is lateral and +x is left rather than right.
		// BVH  OPTI    OPTI  BVH
		//  X    -Y       X    Z
		//  Y     Z       Y   -X
		//  Z     X       Z    Y
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

