/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "library/file_types/VorbisFile.hh"
#include <stdexcept>
#include "library/file_types/VorbisDecoder.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

VorbisFile::VorbisFile(IFile *file)
{
	if (!file->IsOpen())
	{
		throw std::runtime_error("File is not open");
	}

	size = file->Size();
	data = new unsigned char[size];
	size_t read = file->ReadBytes((char*)data, size);

	if (read != size)
	{
		delete[] data;
		throw std::runtime_error("Could not read from file");
	}
}

VorbisFile::~VorbisFile()
{
	delete[] data;
}

IAudioSource* VorbisFile::GetNewSource()
{
	return new VorbisDecoder(data, size);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
