/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/
//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "library/stream/IByteDestination.hh"
#include <vector>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class BinaryWriter {
public:
	BinaryWriter(IByteDestination *destination)
		: destination(destination)
		{}

	void WriteUByte(uint8_t val)
	{
		destination->WriteBytes(1, &val);
	}

	void WriteLittleUint16(uint16_t val)
	{
		uint8_t buffer[2];
		BufferLittleUint16(val, buffer);
		destination->WriteBytes(2, buffer);
	}

	void WriteLittleUint16Array(uint16_t *val, size_t num)
	{
		std::vector<uint8_t> buffer;
		buffer.resize(num * sizeof(uint16_t));
		for (int element = 0, index = 0; element < num; element += 1, index += sizeof(uint16_t))
		{
			BufferLittleUint16(val[element], &buffer[index]);
		}
		destination->WriteBytes(buffer.size(), &buffer[0]);
	}

	void WriteBigUint16(uint16_t val)
	{
		uint8_t buffer[2];
		BufferBigUint16(val, buffer);
		destination->WriteBytes(2, buffer);
	}

	void WriteBigUint16Array(uint16_t *val, size_t num)
	{
		std::vector<uint8_t> buffer;
		buffer.resize(num * sizeof(uint16_t));
		for (int element = 0, index = 0; element < num; element += 1, index += sizeof(uint16_t))
		{
			BufferBigUint16(val[element], &buffer[index]);
		}
		destination->WriteBytes(buffer.size(), &buffer[0]);
	}

	void WriteLittleUint32(uint32_t val)
	{
		uint8_t buffer[4];
		BufferLittleUint32(val, buffer);
		destination->WriteBytes(4, buffer);
	}

	void WriteLittleUint32Array(uint32_t *val, size_t num)
	{
		std::vector<uint8_t> buffer;
		buffer.resize(num * sizeof(uint32_t));
		for (int element = 0, index = 0; element < num; element += 1, index += sizeof(uint32_t))
		{
			BufferLittleUint32(val[element], &buffer[index]);
		}
		destination->WriteBytes(buffer.size(), &buffer[0]);
	}

	void WriteBigUint32(uint32_t val)
	{
		uint8_t buffer[4];
		BufferBigUint32(val, buffer);
		destination->WriteBytes(4, buffer);
	}

	void WriteBigUint32Array(uint32_t *val, size_t num)
	{
		std::vector<uint8_t> buffer;
		buffer.resize(num * sizeof(uint32_t));
		for (int element = 0, index = 0; element < num; element += 1, index += sizeof(uint32_t))
		{
			BufferBigUint32(val[element], &buffer[index]);
		}
		destination->WriteBytes(buffer.size(), &buffer[0]);
	}

	void WriteLittleUint64(uint64_t val)
	{
		uint8_t buffer[8];
		BufferLittleUint64(val, buffer);
		destination->WriteBytes(8, buffer);
	}

	void WriteLittleUint64Array(uint64_t *val, size_t num)
	{
		std::vector<uint8_t> buffer;
		buffer.resize(num * sizeof(uint64_t));
		for (int element = 0, index = 0; element < num; element += 1, index += sizeof(uint64_t))
		{
			BufferLittleUint64(val[element], &buffer[index]);
		}
		destination->WriteBytes(buffer.size(), &buffer[0]);
	}

	void WriteBigUint64(uint64_t val)
	{
		uint8_t buffer[8];
		BufferBigUint64(val, buffer);
		destination->WriteBytes(8, buffer);
	}

	void WriteBigUint64Array(uint64_t *val, size_t num)
	{
		std::vector<uint8_t> buffer;
		buffer.resize(num * sizeof(uint64_t));
		for (int element = 0, index = 0; element < num; element += 1, index += sizeof(uint64_t))
		{
			BufferBigUint64(val[element], &buffer[index]);
		}
		destination->WriteBytes(buffer.size(), &buffer[0]);
	}

	void WriteByte(int8_t val)
	{
		WriteUByte(reinterpret_cast<uint8_t&>(val));
	}

	void WriteLittleInt16(int16_t val)
	{
		WriteLittleUint16(reinterpret_cast<uint16_t&>(val));
	}

	void WriteLittleInt16Array(int16_t *val, size_t num)
	{
		WriteLittleUint16Array((uint16_t*)val, num);
	}

	void WriteBigInt16(int16_t val)
	{
		WriteBigUint16(reinterpret_cast<uint16_t&>(val));
	}

	void WriteBigInt16Array(int16_t *val, size_t num)
	{
		WriteBigUint16Array((uint16_t*)val, num);
	}

	void WriteLittleInt32(int32_t val)
	{
		WriteLittleUint32(reinterpret_cast<uint32_t&>(val));
	}

	void WriteLittleInt32Array(int32_t *val, size_t num)
	{
		WriteLittleUint32Array((uint32_t*)val, num);
	}

	void WriteBigInt32(int32_t val)
	{
		WriteBigUint32(reinterpret_cast<uint32_t&>(val));
	}

	void WriteBigInt32Array(int32_t *val, size_t num)
	{
		WriteBigUint32Array((uint32_t*)val, num);
	}

	void WriteLittleInt64(int64_t val)
	{
		WriteLittleUint64(reinterpret_cast<uint64_t&>(val));
	}

	void WriteLittleInt64Array(int64_t *val, size_t num)
	{
		WriteLittleUint64Array((uint64_t*)val, num);
	}

	void WriteBigInt64(int64_t val)
	{
		WriteBigUint64(reinterpret_cast<uint64_t&>(val));
	}

	void WriteBigInt64Array(int64_t *val, size_t num)
	{
		WriteBigUint64Array((uint64_t*)val, num);
	}

	void WriteLittleFloat(float val)
	{
		WriteLittleUint32(reinterpret_cast<uint32_t&>(val));
	}

	void WriteLittleFloatArray(float *val, size_t num)
	{
		WriteLittleUint32Array((uint32_t*)val, num);
	}

	void WriteBigFloat(float val)
	{
		WriteBigUint32(reinterpret_cast<uint32_t&>(val));
	}

	void WriteBigFloatArray(float *val, size_t num)
	{
		WriteBigUint32Array((uint32_t*)val, num);
	}

	void WriteLittleDouble(double val)
	{
		WriteLittleUint64(reinterpret_cast<uint64_t&>(val));
	}

	void WriteLittleDoubleArray(double *val, size_t num)
	{
		WriteLittleUint64Array((uint64_t*)val, num);
	}

	void WriteBigDouble(double val)
	{
		WriteBigUint64(reinterpret_cast<uint64_t&>(val));
	}

	void WriteBigDoubleArray(double *val, size_t num)
	{
		WriteBigUint64Array((uint64_t*)val, num);
	}

	void WriteBytes(size_t bytes, const uint8_t *buffer)
	{
		destination->WriteBytes(bytes, buffer);
	}

private:
	IByteDestination *destination;

	void BufferLittleUint16(uint16_t val, uint8_t *buffer)
	{
		for (int i = 0; i < 2; i += 1)
		{
			buffer[i] = (val >> (i * 8)) & 0xff;
		}
	}

	void BufferBigUint16(uint16_t val, uint8_t *buffer)
	{
		for (int i = 0; i < 2; i += 1)
		{
			buffer[1 - i] = (val >> (i * 8)) & 0xff;
		}
	}

	void BufferLittleUint32(uint32_t val, uint8_t *buffer)
	{
		for (int i = 0; i < 4; i += 1)
		{
			buffer[i] = (val >> (i * 8)) & 0xff;
		}
	}

	void BufferBigUint32(uint32_t val, uint8_t *buffer)
	{
		for (int i = 0; i < 4; i += 1)
		{
			buffer[3 - i] = (val >> (i * 8)) & 0xff;
		}
	}

	void BufferLittleUint64(uint64_t val, uint8_t *buffer)
	{
		for (int i = 0; i < 8; i += 1)
		{
			buffer[i] = (val >> (i * 8)) & 0xff;
		}
	}

	void BufferBigUint64(uint64_t val, uint8_t *buffer)
	{
		for (int i = 0; i < 8; i += 1)
		{
			buffer[7 - i] = (val >> (i * 8)) & 0xff;
		}
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

