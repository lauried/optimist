/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/
//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class IByteSource {
public:
	/**
	 * Copies the given number of bytes into the buffer.
	 * The buffer must have room for all of them, and is guaranteed to have all
	 * of them copied to it.
	 * Throws std::overflow_error if there are not enough bytes left in the
	 * source.
	 */
	virtual void ReadBytes(size_t numBytes, uint8_t *buffer) = 0;

	/**
	 * Reads and discards bytes, without requiring a buffer.
	 * Throws std::overflow_error if there are not enough bytes left in the
	 * source.
	 */
	virtual void Skip(size_t numBytes) = 0;

	/**
	 * Return the number of bytes remaining.
	 * If the source is infinite, return the maximum value for a size_t.
	 * A value of zero or less indicates the equivalent of EOF.
	 */
	virtual size_t BytesLeft() = 0;

	/**
	 * May throw an exception if unsupported.
	 * It's the user code's responsibility to ensure that the concrete source
	 * being used supports this and that the underlying memory will be kept
	 * for as long as the pointer is in use.
	 */
	virtual uint8_t *CurrentPointer() = 0;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

