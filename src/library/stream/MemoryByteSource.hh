/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/
//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <stdexcept>
#include <vector>
#include "library/stream/IByteSource.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class MemoryByteSource : public IByteSource {
public:
	MemoryByteSource(std::vector<uint8_t> &data)
	{
		buffer = pointer = &data[0];
		bufferSize = data.size();
	}

	MemoryByteSource(uint8_t *data, size_t size)
	{
		buffer = pointer = data;
		bufferSize = size;
	}

	virtual void ReadBytes(size_t numBytes, uint8_t *buffer);
	virtual void Skip(size_t numBytes);
	virtual size_t BytesLeft();
	virtual uint8_t *CurrentPointer();

private:
	uint8_t *buffer;
	uint8_t *pointer;
	size_t bufferSize;

	void CheckBytesRemaining(size_t size)
	{
		if (BytesLeft() < size)
		{
			throw std::overflow_error("End of buffer reached");
		}
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

