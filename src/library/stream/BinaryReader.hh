/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <stdexcept>
#include <iostream>
#include "library/stream/IByteSource.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class BinaryReader {
public:
	BinaryReader(IByteSource *source)
		: source(source)
		, bytesRead(0)
		{}

	uint8_t ReadUbyte()
	{
		uint8_t buffer[1];
		source->ReadBytes(1, buffer);
		bytesRead += 1;
		return buffer[0];
	}

	uint16_t ReadLittleUint16()
	{
		uint8_t buffer[2];
		source->ReadBytes(2, buffer);
		uint16_t val = 0;
		for (int i = 0; i < 2; i += 1)
		{
			val |= (uint16_t)buffer[i] << (i * 8);
		}
		bytesRead += 2;
		return val;
	}

	uint16_t ReadBigUint16()
	{
		uint8_t buffer[2];
		source->ReadBytes(2, buffer);
		uint16_t val = 0;
		for (int i = 0; i < 2; i += 1)
		{
			val |= (uint16_t)buffer[1 - i] << (i * 8);
		}
		bytesRead += 2;
		return val;
	}

	uint32_t ReadLittleUint32()
	{
		uint8_t buffer[4];
		source->ReadBytes(4, buffer);
		uint32_t val = 0;
		for (int i = 0; i < 4; i += 1)
		{
			val |= (uint32_t)buffer[i] << (i * 8);
		}
		bytesRead += 4;
		return val;
	}

	uint32_t ReadBigUint32()
	{
		uint8_t buffer[4];
		source->ReadBytes(4, buffer);
		uint32_t val = 0;
		for (int i = 0; i < 4; i += 1)
		{
			val |= (uint32_t)buffer[3 - i] << (i * 8);
		}
		bytesRead += 4;
		return val;
	}

	uint64_t ReadLittleUint64()
	{
		uint8_t buffer[8];
		source->ReadBytes(8, buffer);
		uint64_t val = 0;
		for (int i = 0; i < 8; i += 1)
		{
			val |= (uint64_t)buffer[i] << (i * 8);
		}
		bytesRead += 8;
		return val;
	}

	uint64_t ReadBigUint64()
	{
		uint8_t buffer[8];
		source->ReadBytes(8, buffer);
		uint64_t val = 0;
		for (int i = 0; i < 8; i += 1)
		{
			val |= (uint64_t)buffer[7 - i] << (i * 8);
		}
		bytesRead += 8;
		return val;
	}

	int8_t ReadByte()
	{
		uint8_t i = ReadUbyte();
		return reinterpret_cast<uint8_t&>(i);
	}

	int16_t ReadLittleInt16()
	{
		uint16_t i = ReadLittleUint16();
		return reinterpret_cast<int16_t&>(i);
	}

	int16_t ReadBigInt16()
	{
		uint16_t i = ReadBigUint16();
		return reinterpret_cast<int16_t&>(i);
	}

	int32_t ReadLittleInt32()
	{
		uint32_t i = ReadLittleUint32();
		return reinterpret_cast<int32_t&>(i);
	}

	int32_t ReadBigInt32()
	{
		uint32_t i = ReadBigUint32();
		return reinterpret_cast<int32_t&>(i);
	}

	int64_t ReadLittleInt64()
	{
		uint64_t i = ReadLittleUint64();
		return reinterpret_cast<int64_t&>(i);
	}

	int64_t ReadBigInt64()
	{
		uint64_t i = ReadBigUint64();
		return reinterpret_cast<int64_t&>(i);
	}

	float ReadLittleFloat()
	{
		uint32_t i = ReadLittleUint32();
		return reinterpret_cast<float&>(i);
	}

	float ReadBigFloat()
	{
		uint32_t i = ReadBigUint32();
		return reinterpret_cast<float&>(i);
	}

	double ReadLittleDouble()
	{
		uint64_t i = ReadLittleUint64();
		return reinterpret_cast<double&>(i);
	}

	double ReadBigDouble()
	{
		uint64_t i = ReadBigUint64();
		return reinterpret_cast<double&>(i);
	}

	void ReadBytes(size_t bytes, uint8_t *buffer)
	{
		source->ReadBytes(bytes, buffer);
		bytesRead += bytes;
	}

	void Skip(size_t bytes)
	{
		source->Skip(bytes);
		bytesRead += bytes;
	}

	bool Eof()
	{
		return BytesRemaining() <= 0;
	}

	size_t BytesRemaining()
	{
		return source->BytesLeft();
	}

	size_t BytesRead()
	{
		return bytesRead;
	}

	uint8_t *CurrentPointer()
	{
		return source->CurrentPointer();
	}

private:
	IByteSource *source;
	size_t bytesRead;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

