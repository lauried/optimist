/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <memory.h>
#include <cmath>
#include <functional>
#include <stdexcept>
#include "library/geometry/Vector2.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Base template for Array2D.
 */
template <typename T>
class Array2DBase {
public:
	/**
	 * Sets a region inside this array to be a copy of the array passed, offset
	 * at the given coordinates.
	 * The other region must fit inside this one.
	 */
	void SetRegion(const Array2DBase<T> &other, int x, int y)
	{
		int w = other.Width();
		int h = other.Height();
		if (x < 0 || y < 0 || x + w > Width() || y + h > Height())
		{
			throw std::logic_error("Source array does not fit at the given offset");
		}

		for (int sy = 0; sy < h; sy += 1)
		{
			int dy = sy + y;
			for (int sx = 0; sx < w; sx += 1)
			{
				int dx = sx + x;
				at(dx, dy) = other.at(sx, sy);
			}
		}
	}

	/**
	 * Set part of this array to part of the passed array.
	 */
	void SetRegion(const Array2DBase<T> &other, int toX, int toY, int fromX, int fromY, int fromW, int fromH)
	{
		if (fromX + fromW > other.Width() || fromY + fromH > other.Height())
		{
			throw std::logic_error("Source region does not fit within source array");
		}
		if (toX + fromW > Width() || fromY + fromH > Height())
		{
			throw std::logic_error("Source region does not fit within destination array");
		}

		for (int y = 0; y < fromW; y += 1)
		{
			int sy = y + fromY;
			int dy = y + toY;
			for (int x = 0; x < fromH; x += 1)
			{
				int sx = x + fromX;
				int dx = x + toX;
				at(dx, dy) = other.at(sx, sy);
			}
		}
	}

	/**
	 * Set part of this array to a specific value.
	 */
	void SetRegion(int x, int y, int w, int h, T value)
	{
		int xMax = x + w;
		int yMax = y + h;

		for (int dy = y; dy < yMax; dy += 1)
		{
			for (int dx = x; dx < xMax; dx += 1)
			{
				at(dx, dy) = value;
			}
		}
	}

	/**
	 * Access the data two-dimensionally.
	 */
	T& at(int x, int y)
	{
#ifdef DEBUG
		if (x < 0 || x >= size[0] || y < 0 || y >= size[1])
		{
			throw std::out_of_range("Array2D::at - index out of bounds");
		}
#endif
		return data[y*size[0] + x];
	}

	/**
	 * Const 2D access.
	 */
	const T& at(int x, int y) const
	{
#ifdef DEBUG
		if (x < 0 || x >= size[0] || y < 0 || y >= size[1])
		{
			throw std::out_of_range("Array2D::at - index out of bounds");
		}
#endif
		return data[y*size[0] + x];
	}

	/**
	 * Returns true if the coordinates are out of bounds, else false.
	 */
	bool oob(int x, int y)
	{
		return (x < 0 || y < 0 || x >= size[0] || y >= size[1]);
	}

	/**
	 * Access the array in 2D with clamped coordinates.
	 */
	T& at_clamped(int x, int y)
	{
		if (x < 0)
		{
			x = 0;
		}
		else if (x >= size[0])
		{
			x = size[0] - 1;
		}
		if (y < 0)
		{
			y = 0;
		}
		else if (y >= size[1])
		{
			y = size[1] - 1;
		}
		return at(x, y);
	}

	/**
	 * Const access to the array in 2D with clamped coordinates.
	 */
	const T& at_clamped(int x, int y) const
	{
		if (x < 0)
		{
			x = 0;
		}
		else if (x >= size[0])
		{
			x = size[0] - 1;
		}
		if (y < 0)
		{
			y = 0;
		}
		else if (y >= size[1])
		{
			y = size[1] - 1;
		}
		return at(x, y);
	}

	/**
	 * Access to the array in 2D with a border value for coordinates outside
	 * the array. Writing to coordinates outside the array will have no effect.
	 */
	T& at_bordered(int x, int y, T borderValue)
	{
		if (y < 0 || y >= size[1] || x < 0 || x >= size[0])
		{
			static T bv = borderValue;
			return bv;
		}
		return at(x, y);
	}

	/**
	 * Const access to the array in 2D with a border value for coordinates
	 * outside array.
	 */
	const T& at_bordered(int x, int y, T borderValue) const
	{
		if (y < 0 || y >= size[1] || x < 0 || x >= size[0])
		{
			static T bv = borderValue;
			return bv;
		}
		return at(x, y);
	}

	/**
	 * Access to the array in 2D with a callback when the value is out of bounds.
	 */
	T& at_oob_callback(int x, int y, std::function<T(void)> callback)
	{
		if (y < 0 || y >= size[1] || x < 0 || x >= size[0])
		{
			static T defVal = callback();
			return defVal;
		}
		return at(x, y);
	}

	/**
	 * Const access to the array in 2D with a callback when the value is out of bounds.
	 */
	const T& at_oob_callback(int x, int y, std::function<T(void)> callback) const
	{
		if (y < 0 || y >= size[1] || x < 0 || x >= size[0])
		{
			static T defVal = callback();
			return defVal;
		}
		return at(x, y);
	}

	/**
	 * Access the data linearly in the order it's stored in the array. This is
	 * useful if you want to iterate through all the data without knowing its
	 * coordinates.
	 */
	T& at(int i)
	{
#ifdef DEBUG
		if (i < 0 || i >= size[0] * size[1])
		{
			throw std::out_of_range("Array2D::at - index out of bounds");
		}
#endif
		return data[i];
	}

	/**
	 * Access the data linearly in the order it's stored in the array. This is
	 * useful if you want to iterate through all the data without knowing its
	 * coordinates. Const access.
	 */
	const T& at(int i) const
	{
#ifdef DEBUG
		if (i < 0 || i >= size[0] * size[1])
		{
			throw std::out_of_range("Array2D::at - index out of bounds");
		}
#endif
		return data[i];
	}

	/**
	 * Array operator to access data linearly.
	 */
	T& operator[](int i)
	{
#ifdef DEBUG
		if (i < 0 || i >= size[0] * size[1])
		{
			throw std::out_of_range("Array2D::[] - index out of bounds");
		}
#endif
		return data[i];
	}

	/**
	 * Const array operator.
	 */
	const T& operator[](int i) const
	{
#ifdef DEBUG
		if (i < 0 || i >= size[0] * size[1])
		{
			throw std::out_of_range("Array2D::[] - index out of bounds");
		}
#endif
		return data[i];
	}

	/**
	 * Returns the number of elements across the width of the array
	 * (x coordinate).
	 */
	int Width() const { return size[0]; }

	/**
	 * Returns the number of elements across the height of the array
	 * (y coordinate).
	 */
	int Height() const { return size[1]; }

	/**
	 * Returns both size dimensions as a 2D vector.
	 */
	const Vector2I& Size() const { return size; }

	/**
	 * Returns the total number of elements.
	 */
	const int NumElements() const { return size[0] * size[1]; }

	/**
	 * Sets all elements to a specified value.
	 */
	void ClearTo(const T& value)
	{
		for (int i = 0; i < size[0] * size[1]; i += 1)
		{
			data[i] = value;
		}
	}

	/**
	 * Raw access to the elements.
	 */
	T* RawElements()
	{
		return data;
	}
	const T* RawElements() const
	{
		return data;
	}

protected:
	Vector2I size;
	T *data;
};

/**
 * Wraps existing values and provides Array2D access methods.
 */
template <typename T>
class Array2DWrapper : public Array2DBase<T> {
protected:
	using Array2DBase<T>::size;
	using Array2DBase<T>::data;

public:
	Array2DWrapper(T *values, int width, int height)
	{
		data = values;
		size.Set(width, height);
	}

	~Array2DWrapper()
	{
	}
};

/**
 * Template for an array that can be accessed two-dimensionally.
 * Also provides the means to accesss data in a one-dimensional way.
 * @todo Either prevent copying, or have copying copy the data.
 */
template <typename T>
class Array2D : public Array2DBase<T> {
public:
	using Array2DBase<T>::at;

protected:
	using Array2DBase<T>::size;
	using Array2DBase<T>::data;

public:
	/**
	 * Construct with zero size. Must call SetSize in order to use the array.
	 */
	Array2D()
	{
		data = nullptr;
	}

	/**
	 * Construct with a new array of the given dimensions.
	 */
	Array2D(int x, int y)
	{
		data = nullptr;
		SetSize(x, y);
	}

	/**
	 * Construct with a new array of the given dimensions and default value.
	 */
	Array2D(int x, int y, T defaultValue)
	{
		data = nullptr;
		SetSize(x, y, defaultValue);
	}

	/**
	 * Construct with a new array of the given dimensions.
	 */
	Array2D(Vector2I s)
	{
		data = nullptr;
		SetSize(s);
	}

	/**
	 * Construct with a new array of the given dimensions and default value.
	 */
	Array2D(Vector2I s, T defaultValue)
	{
		data = nullptr;
		SetSize(s[0], s[1], defaultValue);
	}

	/**
	 * Construct as a copy of another array.
	 */
	Array2D(const Array2D &other)
	{
		data = nullptr;
		SetSize(other.Width(), other.Height());
		memcpy(data, other.data, sizeof(T) * other.NumElements());
	}

	Array2D(const Array2D &other, int x, int y, int w, int h)
	{
		if (x < 0 || y < 0 || x + w > other.Width() || y + h > other.Height())
		{
			throw std::logic_error("Out of bounds when extracting terrain data");
		}

		data = nullptr;
		SetSize(w, h);

		for (int sy = y, dy = 0; dy < h; sy += 1, dy += 1)
		{
			for (int sx = x, dx = 0; dx < w; sx += 1, dx += 1)
			{
				at(dx, dy) = other.at(sx, sy);
			}
		}
	}

	/**
	 * Constructs as a copy of another array.
	 */
	Array2D(const Array2D *other)
	{
		data = nullptr;
		SetSize(other->Width(), other->Height());
		memcpy(data, other->data, sizeof(T) * other->NumElements());
	}

	~Array2D()
	{
		delete[] data;
	}

	/**
	 * Sets the size of the array. Any existing data is deleted, and the new
	 * contents of the array is undefined.
	 */
	void SetSize(int x, int y)
	{
		delete[] data;
		size[0] = x;
		size[1] = y;
		data = new T[size[0] * size[1]];
	}

	/**
	 * Sets the size and initializes to a default value.
	 * Each element will be set to the specified value.
	 */
	void SetSize(int x, int y, T defaultValue)
	{
		delete[] data;
		size[0] = x;
		size[1] = y;
		data = new T[size[0] * size[1]];
		for (int i = 0; i < x * y; ++i)
		{
			data[i] = defaultValue;
		}
	}

	/**
	 * Sets the size of the array. Any existing data is deleted, and the new
	 * contents of the array is undefined.
	 */
	 void SetSize(Vector2I s)
	 {
	 	SetSize(s[0], s[1]);
	 }

	 /**
	  * Sets the size and initializes to a default value.
	  * Each element will be set to the specified value.
	  */
	 void SetSize(Vector2I s, T defaultValue)
	 {
	 	SetSize(s[0], s[1], defaultValue);
	 }

	/**
	 * Moves the contents of this array to another instance, clearing its
	 * previous contents and leaving this array zero sized.
	 */
	void MoveTo(Array2D<T> &other)
	{
		if (other.data)
		{
			delete[] other.data;
		}
		other.size = size;
		other.data = data;
		size.Set(0, 0);
		data = nullptr;
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
