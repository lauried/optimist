/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <functional>
#include "library/containers/OrderedMap.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Wraps a method to retrieve and release items, retaining some number of items
 * based on their weight, as calculated by a consumer provided method.
 * Items are prioritised for retention by the order that they were last
 * accessed.
 *
 * Note: An item may no longer be valid once another item has been requested.
 */
template <typename KEY, typename VALUE>
class RecentAccessCache {
public:
	typedef std::function<VALUE(KEY)> AllocFunc;
	typedef std::function<void(VALUE)> FreeFunc;
	/**
	 * The weight for a given value must be constant while the value is in the cache.
	 */
	typedef std::function<float(VALUE)> WeighFunc;

	RecentAccessCache(AllocFunc allocate, FreeFunc free, float maxWeight, WeighFunc weigh = [](VALUE v) { return 1; })
		: allocate(allocate)
		, free(free)
		, maxWeight(maxWeight)
		, weigh(weigh)
		, currentWeight(0.0f)
		{}

	~RecentAccessCache()
	{
		for (auto it = items.begin(); it != items.end(); ++it)
		{
			free(it->second);
		}
	}

	VALUE Get(KEY key)
	{
		auto it = items.find(key);
		if (it != items.end())
		{
			VALUE value = it->second;
			items.erase(it);
			items.push_front(std::make_pair(key, value));
			return value;
		}

		VALUE value = allocate(key);
		items.push_front(std::make_pair(key, value));
		currentWeight += weigh(value);

		while (currentWeight > maxWeight && items.size() > 1)
		{
			VALUE deleteValue = items.back().second;
			currentWeight -= weigh(deleteValue);
			free(deleteValue);
			items.pop_back();
		}

		return value;
	}

private:
	AllocFunc allocate;
	FreeFunc free;
	float maxWeight;
	WeighFunc weigh;
	float currentWeight;
	OrderedMap<KEY, VALUE> items;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
