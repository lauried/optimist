/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "library/object_pools/SimpleObjectPool.hh"
#include <vector>
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

template <class T>
class LinkedList {
public:
	// List structure:
	// > = next, < = prev

	// Start's next points to the first node, and its prev points to the last
	// node. If there are no nodes, these point to end.
	// End always points to itself.

	// Empty:
	//       START> END> END...
	// ...END <END <START

	// With nodes:
	//       START> A> B> C> D> E> F> G> END> END...
	// ...END <END <A <B <C <D <E <F <G <START

	struct Node {
		T data;
		Node *next;
		Node *prev;
		Node() : next(nullptr), prev(nullptr) {}
	};

	/**
	 * User code must never examine or modify the contents or value of IndexType
	 * except through methods on the list to which it belongs.
	 */
	typedef Node* IndexType;

	typedef SimpleObjectPool<Node> PoolType;

	LinkedList(SimpleObjectPool<Node> *pool) : pool(pool)
	{
		start.next = &end;
		start.prev = &end;
		end.next = &end;
		end.prev = &end;
		size = 0;
	}

	LinkedList(const LinkedList<T> &other)
	{
		start.next = &end;
		start.prev = &end;
		end.next = &end;
		end.prev = &end;
		size = 0;
		for (const Node* node = other.start.next; node != &(other.end); node = node->next)
		{
			Append(node->data);
		}
	}

	~LinkedList()
	{
		for (Node *node = start.next; node != &end; node = node->next)
		{
			pool->Free(node);
		}
	}

	IndexType Append(T item)
	{
		IndexType newItem = pool->Allocate();
		newItem->data = item;

		// OLDTAIL> E
		// OLDTAIL <S
		IndexType oldTail = start.prev;
		if (oldTail == &end)
		{
			oldTail = &start;
		}

		// OLDTAIL> ITEM> E
		// OLDTAIL <ITEM <S
		oldTail->next = newItem;
		newItem->next = &end;
		start.prev = newItem;
		newItem->prev = oldTail;

		end.next = &end;
		end.prev = &end;

		size += 1;
		return newItem;
	}

	IndexType Prepend(T item)
	{
		IndexType newItem = pool->Allocate();
		newItem->data = item;

		// S> OLDHEAD> ?
		// E <OLDHEAD
		IndexType oldHead = start.next;

		// S> ITEM> OLDHEAD> ?
		// E <ITEM <OLDHEAD
		oldHead->prev = newItem;
		newItem->prev = &end;
		start.next = newItem;
		newItem->next = oldHead;

		end.next = &end;
		end.prev = &end;

		size += 1;
		return newItem;
	}

	IndexType Next(IndexType current)
	{
		return current->next;
	}

	IndexType Prev(IndexType current)
	{
		return current->prev;
	}

	void Remove(IndexType index)
	{
		if (index == &end || index == &start)
		{
			throw std::logic_error("Tried to remove start or end of a list");
		}

		IndexType prev = index->prev;
		IndexType next = index->next;

		// PREV> ITEM> NEXT  ->  PREV> NEXT
		// PREV <ITEM <NEXT  ->  PREV <NEXT
		prev->next = next;
		next->prev = prev;

		end.next = &end;
		end.prev = &end;

		index->next = nullptr;
		index->prev = nullptr;

		pool->Free(index);

		size -= 1;
	}

	T& operator[](IndexType index)
	{
		return index->data;
	}

	size_t Size() const
	{
		return size;
	}

	IndexType Start() { return &start; }

	IndexType End() { return &end; }

	void Reverse()
	{
		// swap next for previous on all nodes, even our special ones
		for (IndexType index = &start; index != &end; index = index->next)
		{
			IndexType tmp = index->prev;
			index->prev = index->next;
			index->next = tmp;
		}
		// end.next == end.prev == &end, so there's no need to swap end.
	}

private:
	Node start;
	Node end;
	size_t size;
	SimpleObjectPool<Node> *pool;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
