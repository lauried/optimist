/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <cassert>
#include <vector>
#include <cinttypes>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * A resizable array that guarantees elements exist at the same memory location
 * and index for the duration of their existence.
 * Elements are no longer guaranteed to be all adjacent or without spaces.
 */
template <typename T>
class PagedArray {
public:
	PagedArray(size_t size = 256) : pageSize(size), numElements(0) { }
	~PagedArray() { clear(); }

	/**
	 * Returns the current number of elements in the array.
	 */
	size_t size() const
	{
		return numElements;
	}

	/**
	 * Sets the new size of the array.
	 * If autoconstruct is true then:
	 * If the new array is smaller some elements will be destructed.
	 * If the new array is larger then some elements will be constructed.
	 */
	void resize(size_t size, bool autoConstruct = true)
	{
		MakeSpace(size);
		if (autoConstruct)
			ConstructElements(size);
		numElements = size;
	}

	/**
	 * Returns the number of elements the array has reserved space for.
	 */
	size_t capacity() const
	{
		return pages.size() * pageSize;
	}

	/**
	 * Returns true if the array is empty, else false.
	 */
	bool empty() const
	{
		return (numElements == 0);
	}

	/**
	 * Appends an element to the end of the array.
	 */
	T* push_back(T element)
	{
		resize(numElements + 1, false);
		at(numElements - 1) = element;
		return &at(numElements - 1);
	}

	/**
	 * Removes all elements from the array, calling their destructors.
	 */
	void clear(bool keepCapacity = false)
	{
		if (keepCapacity)
		{
			ConstructElements(0);
		}
		else
		{
			ClearAll();
		}
	}

	/**
	 * Provides access to the elements.
	 */
	T& at(size_t i)
	{
		if (i >= numElements)
		{
			throw std::out_of_range("PagedArray::at - index exceeds number of elements");
		}
		return pages.at(i / pageSize).elements[i % pageSize];
	}

	/**
	 * Provides const access to the elements.
	 */
	const T& at(size_t i) const
	{
		if (i >= numElements)
		{
			throw std::out_of_range("PagedArray::at - index exceeds number of elements");
		}
		return pages.at(i / pageSize).elements[i % pageSize];
	}

	/**
	 * Provides access to the elements.
	 */
	T& operator[](size_t i)
	{
		if (i >= numElements)
		{
			throw std::out_of_range("PagedArray::at - index exceeds number of elements");
		}
		return pages.at(i / pageSize).elements[i % pageSize];
	}

	/**
	 * Provides const access to the elements.
	 */
	const T& operator[](size_t i) const
	{
		if (i >= numElements)
		{
			throw std::out_of_range("PagedArray::at - index exceeds number of elements");
		}
		return pages.at(i / pageSize).elements[i % pageSize];
	}

protected:
	struct Page {
		size_t numConstructed;
		char* data;
		T* elements;
	};

	size_t pageSize;
	size_t numElements;
	std::vector<Page> pages;

	// make sure that the given page has this many elements constructed
	void SetPagesConstructedElements(int page, size_t size)
	{
		if (size > pages[page].numConstructed)
		{
			for (int i = pages[page].numConstructed; i < size; i += 1)
			{
				new (&(pages[page].elements[i])) T();
			}
		}
		else if (size < pages[page].numConstructed)
		{
			for (int i = size; i < pages[page].numConstructed; i += 1)
			{
				pages[page].elements[i].~T();
			}
		}
	}

	// make sure there's enough pages for the number of elements
	void MakeSpace(size_t elements)
	{
		int requiredPages = (numElements / pageSize) + 1;
		while (pages.size() < requiredPages)
		{
			Page p;
			p.numConstructed = 0;
			p.data = (char*) new char[pageSize * sizeof(T)];
			p.elements = (T*)p.data;
			pages.push_back(p);
		}
	}

	// make sure only the given number of elements are constructed
	void ConstructElements(size_t elements)
	{
		int fullPages = elements / pageSize;
		int numOnPartPage = elements % pageSize;
		for (int i = 0; i < fullPages; i += 1)
		{
			SetPagesConstructedElements(i, pageSize);
		}
		if (fullPages == pages.size())
			return;
		SetPagesConstructedElements(fullPages, numOnPartPage);
		for (int i = fullPages + 1; i < pages.size(); ++i)
		{
			SetPagesConstructedElements(i, 0);
		}
	}

	// free unused pages from the end of the list
	void FreeUnusedPages()
	{
		int i;
		for (i = pages.size() - 1; pages >= 0; i -= 1)
		{
			if (pages[i].numConstructed)
				break;

			delete[] pages[i].data;
		}
		pages.resize(i + 1);
	}

	// clear all data
	void ClearAll()
	{
		ConstructElements(0);
		for (int i = 0; i < pages.size(); i += 1)
		{
			delete[] pages[i].data;
		}
		pages.clear();
		numElements = 0;
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
