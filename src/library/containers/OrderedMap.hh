/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <unordered_map>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Cross between a key/value std::map and a std::deque, except we only provide
 * a forward iterator (for now).
 */
template <typename KEY, typename VALUE>
class OrderedMap {
public:
	typedef std::pair<KEY, VALUE> value_type;

private:
	struct ListNode : public value_type {
		ListNode() : next(nullptr), prev(nullptr) {}
		ListNode(value_type &keyValue) : value_type(keyValue), next(nullptr), prev(nullptr) {}
		// Internally we MUST guarantee that these always point to something.
		ListNode *next;
		ListNode *prev;
	};

public:
	struct iterator {
		iterator(ListNode *node) : node(node) {}
		ListNode *node;
		bool operator==(iterator other) { return node == other.node; }
		bool operator!=(iterator other) { return node != other.node; }
		iterator& operator++() { node = node->next; return *this; }
		iterator  operator++(int) { return iterator(node->next); }
		value_type* operator->() { return node; }
	};

	OrderedMap()
	{
		head.next = &tail;
		head.prev = &head;
		tail.next = &tail;
		tail.prev = &head;
	}

	size_t size()
	{
		return map.size();
	}

	/**
	 * If the key already exists, will overwrite, invalidating any pointer into
	 * the value_type. The new keyValue will be at the front.
	 */
	void push_front(value_type keyValue)
	{
		auto it = map.find(keyValue.first);
		if (it != map.end())
		{
			Unlink(&(it->second));
			map.erase(it);
		}
		auto pair = map.insert(std::make_pair(keyValue.first, ListNode(keyValue)));
		ListNode *node = &(pair.first->second);
		LinkToHead(node);
	}

	void pop_front()
	{
		ListNode *node = head.next;
		if (node == &tail)
		{
			return;
		}
		Unlink(node);
		map.erase(map.find(node->first));
	}

	value_type &front()
	{
		return *(head.next);
	}

	/**
	 * If the key already exists, will overwrite, invalidating any pointer into
	 * the value_type. The new keyValue will be at the front.
	 */
	void push_back(value_type keyValue)
	{
		auto it = map.find(keyValue.first);
		if (it != map.end())
		{
			Unlink(&(it->second));
			map.erase(it);
		}
		auto pair = map.insert(std::make_pair(keyValue.first, ListNode(keyValue)));
		ListNode *node = &(pair.first->second);
		LinkToTail(node);
	}

	void pop_back()
	{
		ListNode *node = tail.prev;
		if (node == &head)
		{
			return;
		}
		Unlink(node);
		map.erase(map.find(node->first));
	}

	value_type &back()
	{
		return *(tail.prev);
	}

	iterator begin() { return iterator(head.next); }
	iterator end() { return iterator(&tail); }

	iterator find(KEY key)
	{
		auto it = map.find(key);
		if (it == map.end())
		{
			return end();
		}
		return iterator(&(it->second));
	}

	iterator erase(iterator &it)
	{
		auto mapIt = map.find(it->first);
		if (mapIt == map.end())
		{
			return it;
		}
		iterator newIt = iterator(it.node->next);
		Unlink(&(mapIt->second));
		map.erase(mapIt);
		return newIt;
	}

private:
	std::unordered_map<KEY, ListNode> map;
	ListNode head, tail;

	void LinkToHead(ListNode *node)
	{
		// head > currentfront
		// head < currentfront
		// BECOMES
		// head > node > currentfront
		// head < node < currentfront
		node->next = head.next;
		node->prev = &head;
		head.next->prev = node;
		head.next = node;
	}

	void LinkToTail(ListNode *node)
	{
		// currentback > tail
		// currentback < tail
		// BECOMES
		// currentback > node > tail
		// currentback < node < tail
		node->prev = tail.prev;
		node->next = &tail;
		tail.prev->next = node;
		tail.prev = node;
	}

	void Unlink(ListNode *node)
	{
		// prev > node > next
		// prev < node < next
		// BECOMES
		// prev > next
		// prev < next
		node->prev->next = node->next;
		node->next->prev = node->prev;
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

