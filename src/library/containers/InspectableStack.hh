/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <vector>
#include <cstddef>
#include <cstdint>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Implement std::stack but allowing inspection of the elements inside.
 * It is built around a vector. Index zero is the top of the stack.
 */
template <class T>
class InspectableStack {
public:
	InspectableStack(size_t reserve = 0)
	{
		vector.reserve(reserve);
	}

	// Implement stack:

	bool empty() const
	{
		return vector.empty();
	}

	size_t size() const
	{
		return vector.size();
	}

	T& top()
	{
		return at_top(0);
	}

	const T& top() const
	{
		return at_top(0);
	}

	void push(const T& val)
	{
		vector.push_back(val);
	}

	template <class... Args>
	void emplace(Args&&... args)
	{
		vector.emplace(vector.end, args...);
	}

	void pop()
	{
		vector.pop_back();
	}

	// Inspection behaviour:

	T& at_top(size_t index)
	{
		return vector.at((vector.size() - index) - 1);
	}

	T& at_bottom(size_t index)
	{
		return vector.at(index);
	}

private:
	std::vector<T> vector;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
