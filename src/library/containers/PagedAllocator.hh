/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <cassert>
#include <cstring>
#include <vector>
#include <iostream>
#include <map>
#include "library/containers/FixedQueue.hh"
#include "library/containers/PagedArray.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * A random creation-deletion container that guarantees pointer permanence for
 * the lifetime of the object. The items in the array will not be stored
 * consecutively in memory.
 */
template <class T>
class PagedAllocator {
public:
	PagedAllocator(size_t pageSize = 256) : elements(pageSize) { }

	T& operator[](size_t i)
	{
		return elements[i].value;
	}

	const T& operator[](size_t i) const
	{
		return elements[i].value;
	}

	/**
	 * Allocates and returns the index of the object.
	 */
	int Allocate()
	{
		// Check freed queue
		if (freed.CanPop())
		{
			int index = freed.Pop();
			if (IsFree(index))
			{
				AllocateElement(index);
				return index;
			}
		}
		// Search free space
		if (elements.size())
		{
			for (int i = 0; i < elements.size(); i += 1)
			{
				if (IsFree(i))
				{
					AllocateElement(i);
					return i;
				}
			}
		}
		// Expand the array size by 1
		int index = elements.size();
		elements.resize(index + 1, false);
		AllocateElement(index);
		return index;
	}

	/**
	 * Allocates and returns a pointer to the object.
	 * The index will not be known externally.
	 */
	T* AllocatePtr()
	{
		return &(elements[Allocate()].value);
	}

	/**
	 * Finds the index of an object given a pointer.
	 */
	int IndexOf(T* ptr)
	{
		auto it = ptrMap.find(ptr);
		if (it == ptrMap.end())
			return -1;
		return it->second;
	}

	/**
	 * Frees an object by index.
	 */
	void Free(int index)
	{
		auto it = ptrMap.find(&(elements[index].value));
		if (it == ptrMap.end())
		{
			throw std::logic_error("PagedAllocator::Free(int) - could not find index in ptrMap");
		}
		ptrMap.erase(it);
		elements[index].free = true;
		elements[index].value.~T();
		if (freed.CanPush())
			freed.Push(index);
	}

	/**
	 * Frees an object by its pointer.
	 */
	void Free(T* element)
	{
		auto it = ptrMap.find(element);
		if (it == ptrMap.end())
		{
			throw std::logic_error("PagedAllocator::Free(T*) - tried to free element that isn't part of array");
		}
		Free(it->second);
	}

	/**
	 * Free all objects.
	 */
	void FreeAll()
	{
		for (int i = 0; i < elements.size(); i += 1)
		{
			elements[i].value.~T();
		}
		elements.resize(0, false);
	}

	/**
	 * Return true if an index is free, else false.
	 */
	bool IsFree(int index)
	{
		return elements[index].free;
	}

	/**
	 * Returns one greater than the highest possible object index.
	 */
	size_t CurrentCapacity()
	{
		return elements.size();
	}

private:
	struct Wrapper {
		bool free;
		T value;
	};
	// Here we rely on PagedArray's guarantee of pointer permanence.
	PagedArray<Wrapper> elements;
	FixedQueue<int, 256> freed;
	std::map<T*, int> ptrMap;

	void AllocateElement(int index)
	{
		elements[index].free = false;
		T* ptr = &(elements[index].value);
		new (ptr) T();
		ptrMap.insert(std::pair<T*, int>(ptr, index));
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
