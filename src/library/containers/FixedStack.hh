/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * A fixed size stack. The maximum size of the stack is a template parameter.
 */
template <class T, size_t I>
class FixedStack {
public:
	FixedStack() : top(0) { }

	/**
	 * Returns the number of items in the stack.
	 */
	int Size() const
	{
		return top;
	}

	/**
	 * Returns true if there is space to push another item on the stack,
	 * (i.e. it is not full), else false.
	 */
	bool CanPush() const
	{
		return top < I;
	}

	/**
	 * Push an item onto the stack.
	 */
	bool Push(T ele)
	{
		if (top >= I) return false;
		stack[top++] = ele;
		return true;
	}

	/**
	 * Returns the item from the top of the stack.
	 */
	T Peek() const
	{
		assert(top > 0);
		return stack[top - 1];
	}

	/**
	 * Returns true if there are any items on the stack, else false.
	 */
	bool CanPop() const
	{
		return (top > 0);
	}

	/**
	 * Removes the item from the top of the stack and returns it.
	 */
	T Pop()
	{
		assert(top > 0);
		return stack[top--];
	}

private:
	int top;
	T stack[I];
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
