/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * A fixed size queue. The maximum size of the queue is a template parameter.
 */
template <class T, size_t I>
class FixedQueue {
public:
	FixedQueue() : in(0), out(0), full(false) { }

	/**
	 * Returns the number of items in the queue.
	 */
	int Size() const
	{
		if (full) return I;
		int d = in - out;
		if (d < 0) return I - (out - in);
		return d;
	}

	/**
	 * Returns true if there is space to push another item on the queue,
	 * (i.e. it is not full), else false.
	 */
	bool CanPush() const
	{
		return !full;
	}

	/**
	 * Push an item onto the back of the queue.
	 */
	bool Push(T ele)
	{
		if (full) return false;
		queue[in] = ele;
		in = (in + 1) % I;
		if (in == out) full = true;
		return true;
	}

	/**
	 * Returns the item from the front of the queue.
	 */
	T Peek() const
	{
		assert((in != out) || full);
		return queue[out];
	}

	/**
	 * Returns true if there are any items in the queue, else false.
	 */
	bool CanPop() const
	{
		return ((in != out) || full);
	}

	/**
	 * Removes the item from the front of the queue and returns it.
	 */
	T Pop()
	{
		assert((in != out) || full);
		int index = out;
		out = (out + 1) % I;
		return queue[index];
	}

private:
	// Index to put the next element, and to get the next element from.
	int in, out;
	// If in == out, either all the elements are used or empty. This is true
	// if they're all used.
	bool full;
	T queue[I];
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
