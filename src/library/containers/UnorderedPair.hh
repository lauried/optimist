/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Represents a pair of the same type of object with no particular order.
 * Two instances with the objects in opposite orders will be considered identical.
 */
template <class T>
class UnorderedPair {
public:
	T a;
	T b;

	UnorderedPair(T pa, T pb) : a(pa), b(pb) {}

	bool operator< (const T& rhs) const
	{
		const T& lhs = *this;

		T h1, l1, h2, l2;
		if (lhs.a > lhs.b)
		{
			h1 = lhs.a;
			l1 = lhs.b;
		}
		else
		{
			h1 = lhs.b;
			l1 = lhs.a;
		}
		if (rhs.a > rhs.b)
		{
			h2 = rhs.a;
			l2 = rhs.b;
		}
		else
		{
			h2 = rhs.b;
			l2 = rhs.a;
		}
		if (h1 < h2) return true;
		if (h1 > h2) return false;
		if (l1 < l2) return true;
		// if (l1 > l2) return false;
		return false;
	}
	bool operator> (const T& rhs) const { return rhs < *this; }
	bool operator<=(const T& rhs) const { return !(*this > rhs); }
	bool operator>=(const T& rhs) const { return !(*this < rhs); }

	bool operator==(const T& rhs) const
	{
		const T& lhs = *this;
		return ((lhs.a == rhs.a && lhs.b == rhs.b) || (lhs.a == rhs.b && lhs.b == rhs.a));
	}
	bool operator!=(const T& rhs) const { return !(*this == rhs); }
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
