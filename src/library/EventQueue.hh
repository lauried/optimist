/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <functional>
#include <vector>
#include <list>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Represents a type of event that can be listened for and triggered with data.
 */
template <typename DATA>
class EventQueue {
public:
	/**
	 * An interface for the listener to alter the event's state when handling
	 * it. The event template implements this itself.
	 */
	class IEventState {
	public:
		/**
		 * Stops the event from being handled by any subsequent listeners.
		 */
		virtual void Finish() = 0;
		/**
		 * Inserts a new event, but only listeners later in the chain than the
		 * current listener will see the event.
		 * Handlers will process the new event before they receive the current.
		 */
		virtual void InsertEvent(DATA &data) = 0;
	};

	class IListener {
	public:
		/**
		 * This method will be overloaded if the listener handles more than one
		 * event type.
		 */
		virtual void HandleEvent(DATA &data, IEventState &state) = 0;
	};

	/**
	 * Adds a listener.
	 * Messages are passed to listeners in the order in which listeners are
	 * added.
	 */
	void AddListener(IListener *listener)
	{
		listeners.push_back(listener);
	}

	/**
	 * Adds an event to the end of the queue.
	 */
	void AddEvent(DATA data)
	{
		events.push_back(data);
	}

	/**
	 * Places an event at the front of the queue.
	 * Be sure that doing this doesn't change the queue order.
	 */
	void AddEventFront(DATA data)
	{
		events.push_front(data);
	}

	/**
	 * Processes the queue.
	 */
	void ProcessQueue()
	{
		ProcessQueueInternal();
	}

private:
	struct EventState : public IEventState {
		bool cancel;
		int index;
		std::function<void(DATA&, int)> processEvent;
		EventState() : cancel(false), index(0) {}
		virtual void Finish() { cancel = true; }
		virtual void InsertEvent(DATA &data) { processEvent(data, index + 1); }
	};

	std::vector<IListener *> listeners;
	std::vector<DATA> events;

	void ProcessQueueInternal()
	{
		int numEvents = events.size();
		for (int eventIndex = 0; eventIndex < numEvents; eventIndex += 1)
		{
			ProcessEvent(events[eventIndex]);
		}
		events.resize(0);
	}

	void ProcessEvent(DATA &data, int start = 0)
	{
		EventState state;
		state.processEvent = std::bind(&EventQueue<DATA>::ProcessEvent, this, std::placeholders::_1, std::placeholders::_2);

		for (state.index = start; state.index < listeners.size() && !state.cancel; state.index += 1)
		{
			listeners[state.index]->HandleEvent(data, state);
		}
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

