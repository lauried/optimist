/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <cmath>
#include <iostream>
#include <limits>
#include "library/geometry/Vector3.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Represents a plane, with a normal pointing out from its front side.
 */
template<typename T>
class Plane3 {
public:
	Vector3<T> normal;
	T distance;

	/**
	 * Construct as all zero, which does not represent any plane.
	 */
	Plane3() : distance(0) {}

	/**
	 * Construct with the specified normal and distance.
	 */
	Plane3(Vector3<T> n, T d) : normal(n), distance(d) {}

	/**
	 * Construct from points describing a triangle that lies on the plane.
	 */
	Plane3(Vector3<T> v1, Vector3<T> v2, Vector3<T> v3)
	{
		Set(v1, v2, v3);
	}

	/**
	 * Construct as a copy of another plane.
	 */
	Plane3(const Plane3<T> &p)
	{
		normal = p.normal;
		distance = p.distance;
	}

	/**
	 * Set normal and distance as scalars.
	 */
	void Set(T n1, T n2, T n3, T d)
	{
		normal.Set(n1, n2, n3);
		distance = d;
	}

	/**
	 * Set the normal and distance.
	 */
	void Set(Vector3<T> n, T d)
	{
		normal   = n;
		distance = d;
	}

	/**
	 * Set as a copy of another plane.
	 */
	void Set(const Plane3<T> &p)
	{
		normal = p.normal;
		distance = p.distance;
	}

	/**
	 * Set from points describing a triangle that lies on the plane.
	 */
	void Set(Vector3<T> p1, Vector3<T> p2, Vector3<T> p3)
	{
		Vector3<T> v1 = p2 - p1;
		Vector3<T> v2 = p3 - p1;
		normal = v2.CrossProduct(v1);
		normal.Normalize();
		distance = normal.DotProduct(p1);
	}

	/**
	 * Negate all values, swapping the front and back side of the plane around.
	 */
	void Flip()
	{
		normal *= -1;
		distance *= -1;
	}

	/**
	 * returns true if the specified point is in front of the plane, else false.
	 */
	bool InFront(Vector3<T> v) const
	{
		return (normal.DotProduct(v) >= distance);
	}

	/**
	 * Returns true if the specified point is in front of the plane, with the
	 * plane expanded (moved in its direction) by a given offset.
	 */
	bool InFront(Vector3<T> v, T offset) const
	{
		return (normal.DotProduct(v) >= distance + offset);
	}

	/**
	 * Returns the fraction along a line at which it intersects the plane, as
	 * well as determining the direction that the line travelled.
	 * The line is considered to be 'travelling' from its first point to its
	 * second point but we return the fraction even if it's beyond these points.
	 * @param p1 the first point on the line.
	 * @param p2 the second point on the line.
	 * @param offset a distance in our normal to offset the plane for this
	 *        calculation.
	 * @param direction Set to the direction of the line, negative if it's
	 *        going from infront to behind, positive if it's from behind to
	 *        infront.
	 * In the special case of the line being parallel, if infront we return
	 * negative direction and infinite distance as it will never end up behind
	 * the plane, and if it's behind we return positive and an infinite
	 * distance.
	 */
	// We want to know if we enter or leave the plane, and the fraction.
	// We also need to know if we're inside or outside if we're parallel.
	// But if we're parallel and outside we say we're entering the plane in the
	// future and if we're inside we say we're leaving in the future.
	// Direction is positive for leaving and negative for entering.
	float Intersects(Vector3<T> p1, Vector3<T> p2, float offset, float *direction) const
	{
#if 0
		// TODO: I get the feeling this isn't quite working right,
		// unless the outside code makes bad assumptions based on bugs and
		// quirks of the old method.
		//Vector3<T> pointOnPlane = normal * (distance + offset);
		// Our start distance in front of the plane, negated so negative if in front.
		float dividend = normal.DotProduct(p1) - (distance + offset);
		// Direction of travel. Negative if inward, positive if outward.
		float divisor  = normal.DotProduct(p2 - p1);
		*direction = -1;
		if (divisor == 0) return (dividend < 0) ? std::numeric_limits<float>::max() : -std::numeric_limits<float>::max();
		if (divisor > 0) *direction = 1;
		return dividend / divisor;
#else
		// This version actually works
		T dot1 = normal.DotProduct(p1) - (distance + offset);
		T dot2 = normal.DotProduct(p2) - (distance + offset);
		if (dot1 == dot2)
		{
			if (dot1 > 0)
			{
				*direction = -1;
				return std::numeric_limits<float>::max();
			}
			*direction = -1;
			return -std::numeric_limits<float>::max();
		}
		*direction = (dot2 < dot1) ? -1 : 1;
		return -dot1 / (dot2 - dot1);
#endif
	}
};

typedef Plane3<float>  Plane3F;
typedef Plane3<double> Plane3D;

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
