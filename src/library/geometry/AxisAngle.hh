/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <cmath>
#include "framework/Vector3.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Stores a rotation as an axis and the angle rotated around it in radians.
 * This code is currently not very well tested.
 */
template< class T>
class AxisAngle {
public:
	Vector3<T> axis;
	T angle;

	AxisAngle()
	{
		angle = 0;
	}

	/**
	 * Construct from the vector and the angle.
	 */
	AxisAngle(const Vector3<T> &v, T a)
	{
		Set(v, a);
	}

	AxisAngle(const AxisAngle<T> &src)
	{
		Set(src.axis, src.angle);
	}

	/**
	 * Construct from the rotation specified by two vectors. Axis will always
	 * be perpendicular to the two, and angle will always be the shortest way
	 * round.
	 */
	AxisAngle(const Vector3<T> &original, const Vector3<T> &translated)
	{
		Set(original, translated);
	}

	/**
	 * Set the axis and angle.
	 */
	void Set(const Vector3<T> &v, T a)
	{
		axis  = v;
		angle = a;
		axis.Normalize();
	}

	/**
	 * Set from the rotation specified by two vectors. Axis will always be
	 * perpendicular to the two, and angle will always be the shortest way
	 * round.
	 */
	void Set(const Vector3<T> &original, const Vector3<T> &translated)
	{
		Vector3<T> origN  = original;
		Vector3<T> transN = translated;
		origN .Normalize();
		transN.Normalize();
		axis  = origN.CrossProduct(transN);
		angle = acos(origN.DotProduct(transN));
		axis.Normalize();
	}

/*
	/// Rotate a vector around us.
	Vector Rotate(const Vector &vec) const
	{
		// convert to matrix
		Scalar matrix[3][3];
		Scalar c = cos(angle);
		Scalar s = sin(angle);
		Scalar t = 1.0 - c;

		matrix[0][0] = c + axis.v[0]*axis.v[0]*t;
		matrix[1][1] = c + axis.v[1]*axis.v[1]*t;
		matrix[2][2] = c + axis.v[2]*axis.v[2]*t;

		Scalar tmp1, tmp2;

		tmp1 = axis.v[0]*axis.v[1]*t;
		tmp2 = axis.v[2]*s;
		matrix[1][0] = tmp1 + tmp2;
		matrix[0][1] = tmp1 - tmp2;

		tmp1 = axis.v[0]*axis.v[2]*t;
		tmp2 = axis.v[1]*s;
		matrix[2][0] = tmp1 - tmp2;
		matrix[0][2] = tmp2 + tmp1;

		tmp1 = axis.v[1]*axis.v[2]*t;
		tmp2 = axis.v[0]*s;
		matrix[2][1] = tmp1 + tmp2;
		matrix[1][2] = tmp1 - tmp2;

		// multiply with the matrix
		Vector result(
			vec.Element(0)*matrix[0][0] + vec.Element(1)*matrix[0][1] + vec.Element(2)*matrix[0][2],
			vec.Element(0)*matrix[1][0] + vec.Element(1)*matrix[1][1] + vec.Element(2)*matrix[1][2],
			vec.Element(0)*matrix[2][0] + vec.Element(1)*matrix[2][1] + vec.Element(2)*matrix[2][2]
			);

		return result;
	}
*/

	void operator+=(const AxisAngle<T> &other)
	{
		axis = (axis * angle) + (other.axis * other.angle);
		angle = axis.Normalize();
	}

	void operator+(const AxisAngle<T> &other)
	{
		AxisAngle result = *this;
		result += other;
		return result;
	}

	void operator-=(const AxisAngle<T> &other)
	{
		axis = (axis * angle) - (other.axis * other.angle);
		angle = axis.Normalize();
	}

	void operator-(const AxisAngle<T> &other)
	{
		AxisAngle result = *this;
		result -= other;
		return result;
	}

	void operator*=(T scale)
	{
		angle *= scale;
	}

	void operator*(T scale)
	{
		AxisAngle result = *this;
		result *= scale;
		return result;
	}

	Vector3<T> ToEuler() const
	{
		T x = axis[0];
		T y = axis[1];
		T z = axis[2];

		Vector3<T> ang;
		T s = sin(angle);
		T c = cos(angle);
		T t = 1.0 - c;
		T val = x*y*t + z*s;
		if (val == 1.0)
		{
			ang[2] = 2.0*atan2(x*sin(angle*0.5),cos(angle*0.5));
			ang[1] = M_PI*0.5;
		}
		else if (val == -1.0)
		{
			ang[2] = -2.0*atan2(x*sin(angle*0.5),cos(angle*0.5));
			ang[1] = -M_PI*0.5;
		}
		else
		{
			ang[2] = atan2(y*s - x*z*t, 1.0 - (y*y+z*z) *t);
			ang[1] = -asin(x*y*t + z*s);
			ang[0] = atan2(x*s - y*z*t, 1 - (x*x+z*z) *t);

		}
		return ang;
	}
};

typedef AxisAngle<float> AxisAngleF;
typedef AxisAngle<double> AxisAngleD;

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
