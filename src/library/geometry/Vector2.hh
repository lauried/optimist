/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <cassert>
#include <cmath>
#include <functional>
#ifndef M_PI
#define M_PI 3.14159265358979323846264
#endif
#include "library/geometry/Vector3.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * 2 Dimensional Vector template.
 * An object representing a 2D Vector, along with static methods for array
 * manipulation.
 */
template <typename T>
class Vector2 {
public:
	//-------------------------------------------------------------------------
	// Data
	//-------------------------------------------------------------------------
	T v[2];

	//-------------------------------------------------------------------------
	// VectorBase
	//-------------------------------------------------------------------------

	int NumElements() { return 2; }
	const T& Element(int i) const { return v[i]; }
	T& Element(int i) { return v[i]; }

	//-------------------------------------------------------------------------
	// Constructors / setters
	//-------------------------------------------------------------------------
	Vector2()
	{
		v[0] = 0;
		v[1] = 0;
	}

	Vector2(T s0, T s1)
	{
		v[0] = s0;
		v[1] = s1;
	}

	Vector2(T *sv)
	{
		v[0] = sv[0];
		v[1] = sv[1];
	}

	void Set(T s0, T s1)
	{
		v[0] = s0;
		v[1] = s1;
	}

	Vector2(const Vector2<T> &other)
	{
		v[0] = other.v[0];
		v[1] = other.v[1];
	}

	template <typename U>
	void Set(const Vector2<U>& other)
	{
		v[0] = other.v[0];
		v[1] = other.v[1];
	}

	//-------------------------------------------------------------------------
	// Overloaded operators
	//-------------------------------------------------------------------------

	template <typename U>
	void operator=(const Vector2<U>& v)
	{
		Set(v);
	}

	bool operator==(const Vector2<T>& rhs) const
	{
		return (v[0] == rhs.v[0] && v[1] == rhs.v[1]);
	}
	bool operator<(const Vector2<T>& rhs) const
	{
		if (v[0] < rhs.v[0]) return true;
		if (v[0] > rhs.v[0]) return false;
		if (v[1] < rhs.v[1]) return true;
		return false;
	}
	bool operator!=(const Vector2<T>& rhs) const { return !(*this == rhs); }
	bool operator<=(const Vector2<T>& rhs) const { return (*this == rhs) || (*this < rhs); }
	bool operator>=(const Vector2<T>& rhs) const { return !(*this < rhs); }
	bool operator>(const Vector2<T>& rhs) const { return !(*this <= rhs); }

	void operator+=(const Vector2<T>& other)
	{
		v[0] += other.v[0];
		v[1] += other.v[1];
	}
	Vector2<T> operator+(const Vector2<T>& other) const
	{
		Vector2<T> result(*this);
		result += other;
		return result;
	}

	void operator-=(const Vector2<T>& other)
	{
		v[0] -= other.v[0];
		v[1] -= other.v[1];
	}
	Vector2<T> operator-(const Vector2<T>& other) const
	{
		Vector2<T> result(*this);
		result -= other;
		return result;
	}

	template <typename U>
	void operator*=(U s)
	{
		v[0] *= s;
		v[1] *= s;
	}
	template <typename U>
	Vector2<T> operator*(U s) const
	{
		Vector2<T> result(*this);
		result *= s;
		return result;
	}

	T& operator[](int index)
	{
		return v[index];
	}

	const T& operator[](int index) const
	{
		return v[index];
	}

	//-------------------------------------------------------------------------
	// Methods
	//-------------------------------------------------------------------------

	void DotProduct(const Vector2<T> &other) const
	{
		DotProduct(v, other.v);
	}

	double Length() const
	{
		return sqrt((double)DotProduct(*this));
	}

	void Normalize()
	{
		return Normalize(v);
	}

	/**
	 * Return a normalized version of this vector.
	 */
	Vector2<T> GetNormalized() const
	{
		Vector2<T> result(*this);
		result.Normalize();
		return result;
	}

	/**
	 * Rotates the vector. Positive angles are in the direction from axis 0 to
	 * axis 1.
	 */
	void Rotate(double radians)
	{
		Vector2<T> temp(*this);
		Rotate(temp.v, radians, v);
	}

	/**
	 * Rotates the vector. Positive angles are in the direction from axis 0 to
	 * axis 1.
	 */
	void RotateDegrees(double degrees)
	{
		Vector2<T> temp(*this);
		RotateDegrees(temp.v, degrees, v);
	}

	/**
	 * Returns a vector perpendicular to this one.
	 * It will be ninety degrees in the direction from the first axis to the
	 * second axis. e.g. If +x is right and +y is up it will be ninety degrees
	 * clockwise.
	 * @return The perpendicular vector.
	 */
	Vector2<T> GetPerpendicular() const
	{
		Vector2<T> result;
		Perpendicular(v, result.v);
		return result;
	}

	/**
	 * Returns a vector perpendicular to this one.
	 * It will be ninety degrees in the direction from the second axis to the
	 * first. e.g. If +x is right and +y is up it will be ninety degrees
	 * anti-clockwise.
	 * @return The perpendicular vector.
	 */
	Vector2<T> GetPerpendicular2() const
	{
		Vector2<T> result;
		Perpendicular2(v, result.v);
		return result;
	}

	void Bound(T min, T max)
	{
		Bound(this, min, max);
	}

	size_t Hash() const
	{
		size_t h1 = std::hash<T>{}(v[0]);
		size_t h2 = std::hash<T>{}(v[1]);
		return (h1 << 1) ^ h2;
	}

	//-------------------------------------------------------------------------
	// Static Methods
	//-------------------------------------------------------------------------

	/**
	 * Set all components to zero.
	 */
	static void Clear(T *out)
	{
		out[0] = 0;
		out[1] = 0;
	}

	static void Set(T *out, T s0, T s1)
	{
		out[0] = s0;
		out[1] = s1;
	}

	static void Copy(const T *src, T *out)
	{
		out[0] = src[0];
		out[1] = src[1];
	}

	static void Add(const T *in1, const T *in2, T *out)
	{
		out[0] = in1[0] + in2[0];
		out[1] = in1[1] + in2[1];
	}

	/**
	 * out = in1 - in2
	 */
	static void Sub(const T *in1, const T *in2, T *out)
	{
		out[0] = in1[0] - in2[0];
		out[1] = in1[1] - in2[1];
	}

	/**
	 * The scale is a template parameter, so casting may be necessary.
	 */
	template <typename U>
	static void Scale(const T *in, U scale, T *out)
	{
		out[0] = in[0] * scale;
		out[1] = in[1] * scale;
	}

	/**
	 * Multiply inBase by scale and then adds inAdd.
	 * The scale is a template parameter, so casting may be necessary.
	 */
	template <typename U>
	static void MA(const T *inBase, U scale, const T *inAdd, T *out)
	{
		out[0] = inBase[0] * scale + inAdd[0];
		out[1] = inBase[1] * scale + inAdd[1];
	}

	static T DotProduct(const T *in1, const T *in2)
	{
		return in1[0]*in2[0] + in1[1]*in2[1];
	}

	static double Length(const T *in)
	{
		return sqrt((double)DotProduct(in, in));
	}

	static double Normalize(T *inout)
	{
		double len = Length(inout);
		Scale(inout, 1.0 / len, inout);
		return len;
	}

	/**
	 * Rotates the vector. Positive angles are in the direction from axis 0 to
	 * axis 1.
	 * There is no temporary variable created here, so it's down to the user
	 * code to implement this if the result is to be placed in the input
	 * vector.
	 */
	static void Rotate(const T *in, double radians, T *out)
	{
		assert(in != out);
		radians = -radians;
		double c = cos(radians);
		double s = sin(radians);
		out[0] = in[0]*c - in[1]*s;
		out[1] = in[1]*c + in[0]*s;
	}

	/**
	 * Rotates the vector. Positive angles are in the direction from axis 0 to
	 * axis 1.
	 * There is no temporary variable created here, so it's down to the user
	 * code to implement this if the result is to be placed in the input
	 * vector.
	 */
	static void RotateDegrees(const T *in, double degrees, T *out)
	{
		Rotate(in, degrees * (M_PI / 180.0), out);
	}

	/**
	 * Sets output vector to be perpendicular to the input one.
	 * It will be ninety degrees in the direction from the first axis to the
	 * second. e.g. If +x is right and +y is up it will be ninety degrees
	 * clockwise.
	 *
	 * There is no temporary variable created here, so it's down to the user
	 * code to implement this if the result is to be placed in the input
	 * vector.
	 */
	static void Perpendicular(const T *in, T *out)
	{
		assert(in != out);
		out[0] = in[1];
		out[1] = -in[0];
	}

	/**
	 * Sets output vector to be perpendicular to the input one.
	 * It will be ninety degrees in the direction from the second axis to the
	 * first. e.g. If +x is right and +y is up it will be ninety degrees
	 * anti-clockwise.
	 *
	 * There is no temporary variable created here, so it's down to the user
	 * code to implement this if the result is to be placed in the input
	 * vector.
	 */
	static void Perpendicular2(const T *in, T *out)
	{
		assert(in != out);
		out[0] = -in[1];
		out[1] = in[0];
	}

	static void Bound(T *inout, T min, T max)
	{
		if (inout[0] < min) inout[0] = min; if (inout[0] > max) inout[0] = max;
		if (inout[1] < min) inout[1] = min; if (inout[1] > max) inout[1] = max;
	}
};

typedef Vector2<double> Vector2D;
typedef Vector2<float>  Vector2F;
typedef Vector2<int>    Vector2I;

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
namespace std {
//-----------------------------------------------------------------------------
	template <typename T>
	struct hash<opti::Vector2<T>> {
		size_t operator() (const opti::Vector2<T> &v) const
		{
			return v.Hash();
		}
	};

	template <typename T>
	std::ostream & operator<<(std::ostream& str, opti::Vector2<T> const & v) {
		str << v[0] << "," << v[1];
		return str;
	}
//-----------------------------------------------------------------------------
} // namespace std
//-----------------------------------------------------------------------------
