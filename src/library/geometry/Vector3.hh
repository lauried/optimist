/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <cassert>
#include <cmath>
#include <string>
#include <sstream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * 3 Dimensional Vector template.
 * An object representing a 3D Vector, along with static methods for array
 * manipulation.
 */
template <typename T>
class Vector3 {
public:
	//-------------------------------------------------------------------------
	// Data
	//-------------------------------------------------------------------------
	T v[3];

	//-------------------------------------------------------------------------
	// VectorBase
	//-------------------------------------------------------------------------

	int NumElements() { return 3; }
	const T& Element(int i) const { return v[i]; }
	T& Element(int i) { return v[i]; }

	//-------------------------------------------------------------------------
	// Constructors / setters
	//-------------------------------------------------------------------------

	/**
	 * Construct empty, all values are defaulted to zero.
	 */
	Vector3()
	{
		v[0] = 0;
		v[1] = 0;
		v[2] = 0;
	}

	Vector3(T s)
	{
		v[0] = s;
		v[1] = s;
		v[2] = s;
	}

	Vector3(const T *s)
	{
		v[0] = s[0];
		v[1] = s[1];
		v[2] = s[2];
	}

	Vector3(T s0, T s1, T s2)
	{
		v[0] = s0;
		v[1] = s1;
		v[2] = s2;
	}

	Vector3(const Vector3<T>& other)
	{
		v[0] = other.v[0];
		v[1] = other.v[1];
		v[2] = other.v[2];
	}

	void Set(T s)
	{
		v[0] = s;
		v[1] = s;
		v[2] = s;
	}

	void Set(T s0, T s1, T s2)
	{
		v[0] = s0;
		v[1] = s1;
		v[2] = s2;
	}

	template <typename U>
	void Set(const U* other)
	{
		v[0] = (T)other[0];
		v[1] = (T)other[1];
		v[2] = (T)other[2];
	}

	template <typename U>
	void Set(const Vector3<U>& other)
	{
		v[0] = (T)other.v[0];
		v[1] = (T)other.v[1];
		v[2] = (T)other.v[2];
	}

	//-------------------------------------------------------------------------
	// Overloaded operators
	//-------------------------------------------------------------------------

	bool operator==(const Vector3<T>& rhs) const
	{
		return (v[0] == rhs.v[0] && v[1] == rhs.v[1] && v[2] == rhs.v[2]);
	}
	bool operator<(const Vector3<T>& rhs) const
	{
		if (v[0] < rhs.v[0]) return true;
		if (v[0] > rhs.v[0]) return false;
		if (v[1] < rhs.v[1]) return true;
		if (v[1] > rhs.v[1]) return false;
		if (v[2] < rhs.v[2]) return true;
		return false;
	}
	bool operator!=(const Vector3<T>& rhs) const { return !(*this == rhs); }
	bool operator<=(const Vector3<T>& rhs) const { return (*this == rhs) || (*this < rhs); }
	bool operator>=(const Vector3<T>& rhs) const { return !(*this < rhs); }
	bool operator>(const Vector3<T>& rhs) const { return !(*this <= rhs); }

	void operator+=(const Vector3<T>& other)
	{
		v[0] += other.v[0];
		v[1] += other.v[1];
		v[2] += other.v[2];
	}
	Vector3<T> operator+(const Vector3<T>& other) const
	{
		Vector3<T> result(*this);
		result += other;
		return result;
	}

	void operator-=(const Vector3<T>& other)
	{
		v[0] -= other.v[0];
		v[1] -= other.v[1];
		v[2] -= other.v[2];
	}
	Vector3<T> operator-(const Vector3<T>& other) const
	{
		Vector3<T> result(*this);
		result -= other;
		return result;
	}

	template <typename U>
	void operator*=(U s)
	{
		v[0] *= s;
		v[1] *= s;
		v[2] *= s;
	}
	template <typename U>
	Vector3<T> operator*(U s) const
	{
		Vector3<T> result(*this);
		result *= s;
		return result;
	}

	T& operator[](int index)
	{
		return v[index];
	}

	const T& operator[](int index) const
	{
		return v[index];
	}

	//-------------------------------------------------------------------------
	// Methods
	//-------------------------------------------------------------------------

	T DotProduct(const Vector3<T> &other) const
	{
		return DotProduct(v, other.v);
	}

	double Length() const
	{
		return sqrt((double)DotProduct(*this));
	}

	double Normalize()
	{
		return Normalize(v);
	}

	/**
	 * Return a normalized version of this vector.
	 */
	Vector3<T> GetNormalized() const
	{
		Vector3<T> result(*this);
		result.Normalize();
		return result;
	}

	Vector3<T> CrossProduct(const Vector3<T> &other) const
	{
		Vector3<T> result;
		CrossProduct(v, other.v, result.v);
		return result;
	}

	Vector3<T> RotateAroundVector(const Vector3<T> &vector, float radians) const
	{
		Vector3<T> result;
		RotatePointAroundVector(v, vector.v, radians, result.v);
		return result;
	}

	void Bound(T min, T max)
	{
		Bound(v, min, max);
	}

	size_t Hash() const
	{
		size_t h1 = std::hash<T>{}(v[0]);
		size_t h2 = std::hash<T>{}(v[1]);
		size_t h3 = std::hash<T>{}(v[2]);
		return (h3 << 2) ^ (h2 << 1) ^ h1;
	}

	//-------------------------------------------------------------------------
	// Static Methods
	//-------------------------------------------------------------------------

	/**
	 * Set all components to zero.
	 */
	static void Clear(T *out)
	{
		out[0] = 0;
		out[1] = 0;
		out[2] = 0;
	}

	static void Set(T *out, T s0, T s1, T s2)
	{
		out[0] = s0;
		out[1] = s1;
		out[2] = s2;
	}

	static void Copy(const T *src, T *out)
	{
		out[0] = src[0];
		out[1] = src[1];
		out[2] = src[2];
	}

	static void Add(const T *in1, const T *in2, T *out)
	{
		out[0] = in1[0] + in2[0];
		out[1] = in1[1] + in2[1];
		out[2] = in1[2] + in2[2];
	}

	/**
	 * out = in1 - in2
	 */
	static void Sub(const T *in1, const T *in2, T *out)
	{
		out[0] = in1[0] - in2[0];
		out[1] = in1[1] - in2[1];
		out[2] = in1[2] - in2[2];
	}

	/**
	 * The scale is a template parameter, so casting may be necessary.
	 */
	template <typename U>
	static void Scale(const T *in, U scale, T *out)
	{
		out[0] = in[0] * scale;
		out[1] = in[1] * scale;
		out[2] = in[2] * scale;
	}

	/**
	 * Multiply inBase by scale and then adds inAdd.
	 * The scale is a template parameter, so casting may be necessary.
	 */
	template <typename U>
	static void MA(const T *inBase, U scale, const T *inAdd, T *out)
	{
		out[0] = inBase[0] * scale + inAdd[0];
		out[1] = inBase[1] * scale + inAdd[1];
		out[2] = inBase[2] * scale + inAdd[2];
	}

	static T DotProduct(const T *in1, const T *in2)
	{
		return in1[0]*in2[0] + in1[1]*in2[1] + in1[2]*in2[2];
	}

	static double Length(const T *in)
	{
		return sqrt((double)DotProduct(in, in));
	}

	static double Normalize(T *inout)
	{
		double len = Length(inout);
		if (len == 0)
			return 0;
		Scale(inout, 1.0 / len, inout);
		return len;
	}

	/**
	 * There is no temporary variable created here, so it's down to the user
	 * code to implement this if the result is to be placed in one of the
	 * input vectors.
	 */
	static void CrossProduct(const T *in1, const T *in2, T *out)
	{
		assert(in1 != out);
		assert(in2 != out);
		out[0] = in1[1]*in2[2] - in1[2]*in2[1];
		out[1] = in1[2]*in2[0] - in1[0]*in2[2];
		out[2] = in1[0]*in2[1] - in1[1]*in2[0];
	}

	static void Bound(T *inout, T min, T max)
	{
		if (inout[0] < min) inout[0] = min; if (inout[0] > max) inout[0] = max;
		if (inout[1] < min) inout[1] = min; if (inout[1] > max) inout[1] = max;
		if (inout[2] < min) inout[2] = min; if (inout[2] > max) inout[2] = max;
	}

	/**
	 * There is no temporary variable created here, so it's down to the user
	 * code to implement this if the result is to be placed in one of the
	 * input vectors.
	 */
	static void RotatePointAroundVector(const T* point, const T* vector, float radians, T* result)
	{
		T a = point[0] * vector[0] + point[1] * vector[1] + point[2] * vector[2];
		T s = sin(radians);
		T c = cos(radians);
		T au = a * vector[0];
		T av = a * vector[1];
		T aw = a * vector[2];
		result[0] = au + (point[0] - au) * c + (vector[1] * point[2] - vector[2] * point[1]) * s;
		result[1] = av + (point[1] - av) * c + (vector[2] * point[0] - vector[0] * point[2]) * s;
		result[2] = aw + (point[2] - aw) * c + (vector[0] * point[1] - vector[1] * point[0]) * s;
	}
};

typedef Vector3<double> Vector3D;
typedef Vector3<float>  Vector3F;
typedef Vector3<int>    Vector3I;

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
namespace std {
//-----------------------------------------------------------------------------
	template <typename T>
	struct hash<opti::Vector3<T>> {
		size_t operator() (const opti::Vector3<T> &v) const
		{
			return v.Hash();
		}
	};

	template <typename T>
	std::ostream & operator<<(std::ostream& str, opti::Vector3<T> const & v) {
		str << v[0] << "," << v[1] << "," << v[2];
		return str;
	}
//-----------------------------------------------------------------------------
} // namespace std
//-----------------------------------------------------------------------------
