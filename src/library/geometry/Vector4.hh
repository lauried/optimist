/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * 3 Dimensional Vector template.
 * An object representing a 3D Vector, along with static methods for array
 * manipulation.
 */
template <typename T>
class Vector4 {
public:
	//-------------------------------------------------------------------------
	// Data
	//-------------------------------------------------------------------------
	T v[4];

	//-------------------------------------------------------------------------
	// VectorBase
	//-------------------------------------------------------------------------

	int NumElements() { return 4; }
	const T& Element(int i) const { return v[i]; }
	T& Element(int i) { return v[i]; }

	//-------------------------------------------------------------------------
	// Constructors / setters
	//-------------------------------------------------------------------------

	/**
	 * Construct empty, all values are defaulted to zero.
	 */
	Vector4()
	{
		v[0] = 0;
		v[1] = 0;
		v[2] = 0;
		v[3] = 0;
	}

	Vector4(T s)
	{
		v[0] = s;
		v[1] = s;
		v[2] = s;
		v[3] = s;
	}

	Vector4(const T *s)
	{
		v[0] = s[0];
		v[1] = s[1];
		v[2] = s[2];
		v[3] = s[3];
	}

	Vector4(T s0, T s1, T s2, T s3)
	{
		v[0] = s0;
		v[1] = s1;
		v[2] = s2;
		v[3] = s3;
	}

	Vector4(const Vector4<T>& other)
	{
		v[0] = other.v[0];
		v[1] = other.v[1];
		v[2] = other.v[2];
		v[3] = other.v[3];
	}

	void Set(T s)
	{
		v[0] = s;
		v[1] = s;
		v[2] = s;
		v[3] = s;
	}

	void Set(T s0, T s1, T s2, T s3)
	{
		v[0] = s0;
		v[1] = s1;
		v[2] = s2;
		v[3] = s3;
	}

	template <typename U>
	void Set(const U* other)
	{
		v[0] = (T)other[0];
		v[1] = (T)other[1];
		v[2] = (T)other[2];
		v[3] = (T)other[3];
	}

	template <typename U>
	void Set(const Vector4<U>& other)
	{
		v[0] = (T)other.v[0];
		v[1] = (T)other.v[1];
		v[2] = (T)other.v[2];
		v[3] = (T)other.v[3];
	}

	//-------------------------------------------------------------------------
	// Overloaded operators
	//-------------------------------------------------------------------------

	bool operator==(const Vector4<T>& rhs) const
	{
		return (v[0] == rhs.v[0] && v[1] == rhs.v[1] && v[2] == rhs.v[2] && v[3] == rhs.v[3]);
	}
	bool operator<(const Vector4<T>& rhs) const
	{
		if (v[0] < rhs.v[0]) return true;
		if (v[0] > rhs.v[0]) return false;
		if (v[1] < rhs.v[1]) return true;
		if (v[1] > rhs.v[1]) return false;
		if (v[2] < rhs.v[2]) return true;
		if (v[3] > rhs.v[3]) return false;
		if (v[3] < rhs.v[3]) return true;
		return false;
	}
	bool operator!=(const Vector4<T>& rhs) const { return !(*this == rhs); }
	bool operator<=(const Vector4<T>& rhs) const { return (*this == rhs) || (*this < rhs); }
	bool operator>=(const Vector4<T>& rhs) const { return !(*this < rhs); }
	bool operator>(const Vector4<T>& rhs) const { return !(*this <= rhs); }

	void operator+=(const Vector4<T>& other)
	{
		v[0] += other.v[0];
		v[1] += other.v[1];
		v[2] += other.v[2];
		v[3] += other.v[3];
	}
	Vector4<T> operator+(const Vector4<T>& other) const
	{
		Vector4<T> result(*this);
		result += other;
		return result;
	}

	void operator-=(const Vector4<T>& other)
	{
		v[0] -= other.v[0];
		v[1] -= other.v[1];
		v[2] -= other.v[2];
		v[3] -= other.v[3];
	}
	Vector4<T> operator-(const Vector4<T>& other) const
	{
		Vector4<T> result(*this);
		result -= other;
		return result;
	}

	template <typename U>
	void operator*=(U s)
	{
		v[0] *= s;
		v[1] *= s;
		v[2] *= s;
		v[3] *= s;
	}
	template <typename U>
	Vector4<T> operator*(U s) const
	{
		Vector4<T> result(*this);
		result *= s;
		return result;
	}

	T& operator[](int index)
	{
		return v[index];
	}

	const T& operator[](int index) const
	{
		return v[index];
	}

	//-------------------------------------------------------------------------
	// Methods
	//-------------------------------------------------------------------------

	size_t Hash() const
	{
		size_t h1 = std::hash<T>{}(v[0]);
		size_t h2 = std::hash<T>{}(v[1]);
		size_t h3 = std::hash<T>{}(v[2]);
		size_t h4 = std::hash<T>{}(v[3]);
		return (h4 << 3) ^ (h3 << 2) ^ (h2 << 1) ^ h1;
	}
};

typedef Vector4<double> Vector4D;
typedef Vector4<float>  Vector4F;
typedef Vector4<int>    Vector4I;

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
namespace std {
//-----------------------------------------------------------------------------
	template <typename T>
	struct hash<opti::Vector4<T>> {
		size_t operator() (const opti::Vector4<T> &v) const
		{
			return v.Hash();
		}
	};

	template <typename T>
	std::ostream & operator<<(std::ostream& str, opti::Vector4<T> const & v) {
		str << v[0] << "," << v[1] << "," << v[2] << "," << v[3];
		return str;
	}
//-----------------------------------------------------------------------------
} // namespace std
//-----------------------------------------------------------------------------
