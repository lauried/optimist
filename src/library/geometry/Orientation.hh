/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <cmath>
#include "library/geometry/Vector3.hh"
#include "library/geometry/Plane3.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * An orientation, stored as forward, right and up vectors, plus orientation.
 * This is essentially a matrix without homogeneous coordinates.
 */
template <typename T>
class Orientation {
public:
	typedef enum {
		Forward = 0,
		Right = 1,
		Up = 2,
		X = 0,
		Y = 1,
		Z = 2
	} Axis;

	/**
	 * Each component of v corresponds to the same numbered spatial axis in
	 * Vector3.
	 */
	Vector3<T> v[3];
	Vector3<T> origin;

	//-------------------------------------------------------------------------
	// Constructors
	//-------------------------------------------------------------------------

	Orientation()
	{
		v[0].v[0] = 1;
		v[1].v[1] = 1;
		v[2].v[2] = 1;
	}

	Orientation(const Orientation<T>& ori)
	{
		v[0] = ori.v[0];
		v[1] = ori.v[1];
		v[2] = ori.v[2];
		origin = ori.origin;
	}

	Orientation(const Vector3<T> eulerAngles)
	{
		v[0].v[0] = 1;
		v[1].v[1] = 1;
		v[2].v[2] = 1;
		Rotate(eulerAngles);
	}

	Orientation(const Vector3<T> eulerAngles, Vector3<T> org)
	{
		origin = org;
		v[0].v[0] = 1;
		v[1].v[1] = 1;
		v[2].v[2] = 1;
		Rotate(eulerAngles);
	}
	Orientation(const Vector3<T> target, const Vector3<T> up, const Vector3<T> org)
	{
		// TODO: Look up crossproduct to check the sign will be right
		v[Forward] = target - origin;
		v[Forward].Normalize();

		v[Right] = v[Forward].CrossProduct(up);
		v[Right].Normalize();

		v[Up] = v[Forward].CrossProduct(v[Right]);
		v[Up].Normalize;

		origin = org;
	}

	template <typename U>
	void Set(const Orientation<U> &other)
	{
		v[0].Set(other.v[0]);
		v[1].Set(other.v[1]);
		v[2].Set(other.v[2]);
		origin.Set(other.origin);
	}

	template <typename U>
	void Set(const Vector3<U> &forward, const Vector3<U> &right, const Vector3<U> &up, const Vector3<U> &org)
	{
		v[Forward].Set(forward);
		v[Right].Set(right);
		v[Up].Set(up);
		origin.Set(org);
	}

	//-------------------------------------------------------------------------
	// Transformation of vectors
	//-------------------------------------------------------------------------

	Vector3<T> DirectionToInternalSpace(const Vector3<T> vec) const
	{
		return (Vector3<T>(v[0].DotProduct(vec), v[1].DotProduct(vec), v[2].DotProduct(vec)));
	}

	Vector3<T> DirectionToExternalSpace(const Vector3<T> vec) const
	{
		return v[0] * vec[0] + v[1] * vec[1] + v[2] * vec[2];
	}

	Vector3<T> PointToInternalSpace(const Vector3<T> vec) const
	{
		Vector3<T> tmp = vec - origin;
		return Vector3<T>(v[0].DotProduct(tmp), v[1].DotProduct(tmp), v[2].DotProduct(tmp));
	}

	Vector3<T> PointToExternalSpace(const Vector3<T> vec) const
	{
		return v[0] * vec.v[0] + v[1] * vec.v[1] + v[2] * vec.v[2] + origin;
	}

	//-------------------------------------------------------------------------
	// Transformation of planes
	//-------------------------------------------------------------------------

	Plane3<T> PlaneToInternalSpace(const Plane3<T> &p) const
	{
		Plane3<T> newP = p;
		newP.distance -= newP.normal.DotProduct(origin);
		newP.normal = DirectionToInternalSpace(newP.normal);
		return newP;
	}

	Plane3<T> PlaneToExternalSpace(const Plane3<T> &p) const
	{
		Plane3<T> newP = p;
		newP.normal = DirectionToExternalSpace(newP.normal);
		newP.distance += newP.normal.DotProduct(origin);
		return newP;
	}

	//-------------------------------------------------------------------------
	// Transformation of self
	//-------------------------------------------------------------------------

	void ToInternalSpaceOf(const Orientation<T>& ori)
	{
		v[0] = ori.DirectionToInternalSpace(v[0]);
		v[1] = ori.DirectionToInternalSpace(v[1]);
		v[2] = ori.DirectionToInternalSpace(v[2]);
		origin = ori.PointToInternalSpace(origin);
	}

	void ToExternalSpaceOf(const Orientation<T>& ori)
	{
		v[0] = ori.DirectionToExternalSpace(v[0]);
		v[1] = ori.DirectionToExternalSpace(v[1]);
		v[2] = ori.DirectionToExternalSpace(v[2]);
		origin = ori.PointToExternalSpace(origin);
	}

	//-------------------------------------------------------------------------
	// Rotation
	//-------------------------------------------------------------------------

	void Rotate(Axis axis, T radians)
	{
		T c1 = cos(radians);
		T s1 = sin(radians);
		int axis0 = 0, axis1 = 0;
		switch (axis)
		{
		case 0:
			axis0 = 2;
			axis1 = 1;
			break;
		case 1:
			axis0 = 0;
			axis1 = 2;
			break;
		case 2:
			axis0 = 1;
			axis1 = 0;
			break;
		default:
			assert(false);
		}
		Vector3<T> v0 = v[axis0] * c1 + v[axis1] * s1;
		Vector3<T> v1 = v[axis1] * c1 - v[axis0] * s1;

		v[axis0] = v0;
		v[axis0].Normalize();
		v[axis1] = v1;
		v[axis1].Normalize();
	}

	void Rotate(Vector3<T> eulerAngles)
	{
		Rotate(Up,      eulerAngles[Up]);
		Rotate(Right,   eulerAngles[Right]);
		Rotate(Forward, eulerAngles[Forward]);
	}

	//-------------------------------------------------------------------------
	// Setting
	//-------------------------------------------------------------------------

	void Set(const Orientation<T>& ori)
	{
		v[0] = ori.v[0];
		v[1] = ori.v[1];
		v[2] = ori.v[2];
		origin = ori.origin;
	}

	void SetAngles(const Orientation<T>& ori)
	{
		v[0] = ori.v[0];
		v[1] = ori.v[1];
		v[2] = ori.v[2];
	}

	void SetAngles(const Vector3<T> eulerAngles)
	{
		v[0].Set(1, 0, 0);
		v[1].Set(0, 1, 0);
		v[2].Set(0, 0, 1);
		Rotate(eulerAngles);
	}
};

typedef Orientation<double> OrientationD;
typedef Orientation<float>  OrientationF;

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
