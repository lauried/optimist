/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "library/geometry/Plane3.hh"
#include "library/geometry/Box3.hh"
#include "library/geometry/Orientation.hh"
#include "library/geometry/Camera.hh"
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// TODO: Fix near and far plane support.
// TODO: Fix orthographic projection.

// Note: Planes point inward.

template <class T>
class Frustum {
public:
	Frustum() : numPlanes(4) { }

	Frustum(const Frustum& f)
	{
		numPlanes = f.numPlanes;
		for (int i = 0; i < numPlanes; i += 1)
		{
			planes[i] = f.planes[i];
		}
	}

	Frustum(const Orientation<T> &ori, T fovX, T fovY)
	{
		CalculatePerspectivePlanes(ori, -fovX, fovY, fovX, -fovY, 0, 0);
	}

	Frustum(const Camera<T> &camera)
	{
		Set(camera);
	}

	void Set(const Orientation<T> &ori, T fovX, T fovY)
	{
		CalculatePerspectivePlanes(ori, -fovX, fovY, fovX, -fovY, 0, 0);
	}

	void Set(const Camera<T> &camera)
	{
		float t, l, b, r;
		if (camera.fovX == 0 && camera.fovY == 0)
		{
			t = camera.top;
			l = camera.left;
			b = camera.bottom;
			r = camera.right;
		}
		else
		{
			l = -camera.fovX;
			r =  camera.fovX;
			t =  camera.fovY;
			b = -camera.fovY;
		}

		if (camera.projection == Camera<T>::Perspective)
		{
			CalculatePerspectivePlanes(camera.orientation, l, t, r, b, camera.nearPlane, camera.farPlane);
		}
		else
		{
			CalculateOrthographicPlanes(camera.orientation, l, t, r, b, camera.nearPlane, camera.farPlane);
		}
	}

	bool Contains(Vector3<T> p) const
	{
		for (int i = 0; i < numPlanes; ++i)
		{
			if (!planes[i].InFront(p))
				return false;
		}
		return true;
	}

	bool Contains(Vector3<T> p, T radius) const
	{
		for (int i = 0; i < numPlanes; ++i)
		{
			if (!planes[i].InFront(p, radius))
				return false;
		}
		return true;
	}

	bool Contains(const Orientation<T> &ori, const Box3<T> &box) const
	{
		for (int i = 0; i < numPlanes; ++i)
		{
			Plane3<T> translatedPlane = ori.PlaneToInternalSpace(planes[i]);
			Vector3<T> boxCorner;
			for (int axis = 0; axis < 3; ++axis)
			{
				if (translatedPlane.normal[axis] > 0)
					boxCorner[axis] = box.maxs[axis];
				else
					boxCorner[axis] = box.mins[axis];
			}
			if (!translatedPlane.InFront(boxCorner))
				return false;
		}
		return true;
	}

	bool Contains(const Vector3<T> &p, const Box3<T> &box) const
	{
		for (int i = 0; i < numPlanes; ++i)
		{
			// Get the corner of the bounding box which lies furthest in the
			// direction of this normal. If this corner is behind the plane
			// then all the others will be too.
			Vector3<T> boxCorner;
			for (int axis = 0; axis < 3; ++axis)
			{
				if (planes[i].normal[axis] > 0)
					boxCorner[axis] = p[axis] + box.maxs[axis];
				else
					boxCorner[axis] = p[axis] + box.mins[axis];
			}
			// Test the box corner.
			if (!planes[i].InFront(boxCorner))
				return false;
		}
		return true;
	}

	bool Contains(const Box3<T> &box) const
	{
		for (int i = 0; i < numPlanes; ++i)
		{
			// Get the corner of the bounding box which lies furthest in the
			// direction of this normal. If this corner is behind the plane
			// then all the others will be too.
			Vector3<T> boxCorner;
			for (int axis = 0; axis < 3; ++axis)
			{
				if (planes[i].normal[axis] > 0)
					boxCorner[axis] = box.maxs[axis];
				else
					boxCorner[axis] = box.mins[axis];
			}
			// Test the box corner.
			if (!planes[i].InFront(boxCorner))
				return false;
		}
		return true;
	}

private:
	Plane3<T> planes[6];
	int numPlanes;

	void CalculatePerspectivePlanes(const Orientation<T> &ori, T left, T top, T right, T bottom, T nearPlane, T farPlane)
	{
		numPlanes = (farPlane <= 0) ? 5 : 6;

		Vector3<T> tr = ori.v[Orientation<T>::Forward] + ori.v[Orientation<T>::Up] * top    + ori.v[Orientation<T>::Right] * right;
		Vector3<T> tl = ori.v[Orientation<T>::Forward] + ori.v[Orientation<T>::Up] * top    + ori.v[Orientation<T>::Right] * left;
		Vector3<T> br = ori.v[Orientation<T>::Forward] + ori.v[Orientation<T>::Up] * bottom + ori.v[Orientation<T>::Right] * right;
		Vector3<T> bl = ori.v[Orientation<T>::Forward] + ori.v[Orientation<T>::Up] * bottom + ori.v[Orientation<T>::Right] * left;

		// left
		planes[0].normal = tl.CrossProduct(bl);
		planes[0].normal.Normalize();
		planes[0].distance = planes[0].normal.DotProduct(ori.origin);
		// right
		planes[1].normal = br.CrossProduct(tr);
		planes[1].normal.Normalize();
		planes[1].distance = planes[1].normal.DotProduct(ori.origin);
		// top
		planes[2].normal = tr.CrossProduct(tl);
		planes[2].normal.Normalize();
		planes[2].distance = planes[2].normal.DotProduct(ori.origin);
		// bottom
		planes[3].normal = bl.CrossProduct(br);
		planes[3].normal.Normalize();
		planes[3].distance = planes[3].normal.DotProduct(ori.origin);

		if (numPlanes > 4)
		{
			// near
			planes[4].normal = ori.v[Orientation<T>::Forward];
			planes[4].normal.Normalize();
			planes[4].distance = planes[4].normal.DotProduct(ori.origin); // zero near plane just for culling behind
		}

		if (numPlanes > 5)
		{
			// far
			planes[5].normal = ori.v[Orientation<T>::Forward] * -1.0;
			planes[5].normal.Normalize();
			planes[5].distance = planes[5].normal.DotProduct(ori.origin) - farPlane;
		}
	}

	void CalculateOrthographicPlanes(const Orientation<T> &ori, float left, float top, float right, float bottom, float nearPlane, float farPlane)
	{
		numPlanes = (farPlane <= 0) ? 5 : 6;

		// left
		planes[0].normal = ori.v[1];
		planes[0].normal.Normalize();
		planes[0].distance = planes[0].normal.DotProduct(ori.origin) + left;

		// right
		planes[1].normal = planes[0].normal * -1;
		planes[1].distance = planes[1].normal.DotProduct(ori.origin) - right;

		// top
		planes[2].normal = ori.v[2] * -1;
		planes[2].normal.Normalize();
		planes[2].distance = planes[2].normal.DotProduct(ori.origin) - top;

		// bottom
		planes[3].normal = planes[2].normal * -1;
		planes[3].distance = planes[3].normal.DotProduct(ori.origin) + bottom;

		if (numPlanes > 4)
		{
			// near
			planes[4].normal = ori.v[0];
			planes[4].normal.Normalize();
			planes[4].distance = planes[4].normal.DotProduct(ori.origin) + nearPlane;
		}

		if (numPlanes > 5)
		{
			// far
			planes[5].normal = ori.v[0] * -1.0;
			planes[5].normal.Normalize();
			planes[5].distance = planes[5].normal.DotProduct(ori.origin) - farPlane;
		}
	}
};

typedef Frustum<float>  FrustumF;
typedef Frustum<double> FrustumD;

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
