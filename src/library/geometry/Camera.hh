/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <cassert>
#include <iostream>
#include "library/geometry/Orientation.hh"
#include "library/geometry/Vector2.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Represents a camera in world space, used to set up the modelview matrix
 * when rendering.
 */
template <typename T>
class Camera {
public:
	enum ProjectionType {
		Perspective = 0,
		Orthographic = 1
	};

	enum FovType {
		Horizontal        = 0,
		Vertical          = 1,
		HorizontalDegrees = 2,
		VerticalDegrees   = 3
	};

	// Either both fovX and fovY must be zero, or left, top, right and bottom must be zero.
	T fovX, fovY;
	T left, top, right, bottom;
	T nearPlane, farPlane;
	Orientation<T> orientation;
	ProjectionType projection;

	Camera() : fovX(1), fovY(1), left(0), top(0), right(0), bottom(0), nearPlane(0), farPlane(0), projection(Perspective) { }

	Camera(const Camera<T> &c)
	{
		fovX = c.fovX;
		fovY = c.fovY;
		left = c.left;
		top = c.top;
		right = c.right;
		bottom = c.bottom;
		nearPlane = c.nearPlane;
		farPlane = c.farPlane;
		orientation = c.orientation;
		projection = c.projection;
	}

	/**
	 * Construct at the origin oriented with no rotation in world space.
	 * (i.e. an identity matrix).
	 * @param aspect The aspect ratio of the screen. Greater than 1 is wider
	 *        than tall, and the opposite is also true.
	 * @param fovType The dimension we specify with the fov parameter.
	 * @param fov The field of view in the aspect and values specified by the
	 *        fovType parameter.
	 */
	Camera(T aspect, FovType fovType, T fov, ProjectionType projection = Perspective)
		: left(0), top(0), right(0), bottom(0), nearPlane(0), farPlane(0), projection(projection)
	{
		InitFov(aspect, fovType, fov);
	}

	/**
	 * Construct with the specified fov and orientation.
	 * @param aspect The aspect ratio of the screen. Greater than 1 is wider
	 *        than tall, and the opposite is also true.
	 * @param fovType The dimension we specify with the fov parameter.
	 * @param fov The field of view in the aspect and values specified by the
	 *        fovType parameter.
	 * @param ori The orientation.
	 */
	Camera(T aspect, FovType fovType, T fov, const Orientation<T> &ori, ProjectionType projection = Perspective)
		: left(0), top(0), right(0), bottom(0), nearPlane(0), farPlane(0), projection(projection)
	{
		orientation.Set(ori);
		InitFov(aspect, fovType, fov);
	}

	/**
	 * Construct with the fov specified in four directions rather than two.
	 */
	Camera(T left, T top, T right, T bottom, const Orientation<T> &ori, ProjectionType projection = Orthographic)
		: fovX(0), fovY(0), left(left), top(top), right(right), bottom(bottom), nearPlane(0), farPlane(0), projection(projection)
	{
		orientation.Set(ori);
	}

	/**
	 * Construct with a screen size rather than a specific aspect ratio.
	 * Construct at the origin oriented with no rotation in world space.
	 * (i.e. an identity matrix).
	 * @param screenSize The screen dimensions from which an aspect ratio will
	 *        be automatically calculated.
	 * @param fovType The dimension we specify with the fov parameter.
	 * @param fov The field of view in the aspect and values specified by the
	 *        fovType parameter.
	 */
	template <typename U>
	Camera(Vector2<U> screenSize, FovType fovType, T fov, ProjectionType projection = Perspective)
		: left(left), top(top), right(right), bottom(bottom), nearPlane(0), farPlane(0), projection(projection)
	{
		InitFov((T)screenSize[0] / (T)screenSize[1], fovType, fov);
	}

	/**
	 * Construct with a screen size rather than a specific aspect ratio.
	 * Construct with the specified fov and orientation.
	 * (i.e. an identity matrix).
	 * @param screenSize The screen dimensions from which an aspect ratio will
	 *        be automatically calculated.
	 * @param fovType The dimension we specify with the fov parameter.
	 * @param fov The field of view in the aspect and values specified by the
	 *        fovType parameter.
	 * @param ori The orientation.
	 */
	template <typename U, typename V>
	Camera(Vector2<U> screenSize, FovType fovType, T fov, const Orientation<V> &ori, ProjectionType projection = Perspective)
		: left(left), top(top), right(right), bottom(bottom), nearPlane(0), farPlane(0), projection(projection)
	{
		orientation.Set(ori);
		InitFov((T)screenSize[0] / (T)screenSize[1], fovType, fov);
	}

	/**
	 * Sets with angular field of view.
	 */
	void Set(T aspect, FovType fovType, T fov, const Orientation<T> &ori, ProjectionType projection = Perspective)
	{
		left = top = right = bottom = 0;
		this->projection = projection;
		orientation.Set(ori);
		InitFov(aspect, fovType, fov);
	}

	/**
	 * Sets with linear width and height field of view.
	 */
	void Set(T fovX, T fovY, const Orientation<T> &ori, ProjectionType projection = Perspective)
	{
		left = top = right = bottom = 0;
		this->fovX = fovX;
		this->fovY = fovY;
		this->projection = projection;
		orientation.Set(ori);
	}

	/**
	 * Set top/left/bottom/right, typically most useful for orthographic projection.
	 */
	void Set(T left, T top, T right, T bottom, const Orientation<T> &ori, ProjectionType projection = Orthographic)
	{
		fovX = fovY = 0;
		this->left = left;
		this->top = top;
		this->right = right;
		this->bottom = bottom;
		this->projection = projection;
		orientation.Set(ori);
	}

private:
	void InitFov(T aspect, FovType fovType, T fov)
	{
		assert(aspect > 0);
		switch (fovType)
		{
		case Horizontal:
			fovX = fov;
			fovY = fovX / aspect;
			break;
		case Vertical:
			fovY = fov;
			fovX = fovY * aspect;
			break;
		case HorizontalDegrees:
			fovX = atan(fov * (M_PI / 180.0));
			fovY = fovX / aspect;
			break;
		case VerticalDegrees:
			fovY = atan(fov * (M_PI / 180.0));
			fovX = fovY * aspect;
			break;
		default:
			assert(false);
		}
	}
};

typedef Camera<double> CameraD;
typedef Camera<float>  CameraF;

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
