/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "library/geometry/Vector2.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

template <typename T>
class Box2 {
public:
	Vector2<T> mins;
	Vector2<T> maxs;

	Box2() { }

	/**
	 * Construct from vector minimum and maximum corners.
	 */
	Box2(const Vector2<T> &pMins, const Vector2<T> &pMaxs)
	{
		Set(pMins, pMaxs);
	}

	/**
	 * Construct from scalar values for minimum and maximum coordinates.
	 */
	Box2(T ix, T iy, T ax, T ay)
	{
		Set(ix, iy, ax, ay);
	}

	/**
	 * Set vector minimum and maximum corners.
	 */
	void Set(const Vector2<T> &pMins, const Vector2<T> &pMaxs)
	{
		mins = pMins;
		maxs = pMaxs;
	}

	/**
	 * Set minimum and maximum from scalars.
	 */
	void Set(T ix, T iy, T ax, T ay)
	{
		mins.Set(ix, iy);
		maxs.Set(ax, ay);
	}

	/**
	 * Return size in a given axis.
	 */
	T Size(int axis)
	{
		return maxs[axis] - mins[axis];
	}

	/**
	 * Set from a line. The box will exactly contain the line.
	 * Another way to picture this is that the start and end contain
	 * either the minimum and maximum value for the axis, and this
	 * method sorts these into maxs and mins.
	 */
	void SetFromLine(const Vector2<T> &start, const Vector2<T> &end)
	{
		for (int i = 0; i < 2; ++i)
		{
			if (start.v[i] < end.v[i])
			{
				mins[i] = start.v[i];
				maxs[i] = end.v[i];
			}
			else
			{
				mins[i] = end.v[i];
				maxs[i] = start.v[i];
			}
		}
	}

	/**
	 * Returns true if this box contains the specified point.
	 * It is considered to be contained if it lies exactly on the edge.
	 */
	bool Contains(const Vector2<T> &p) const
	{
		if (p[0] < mins[0] ||
			p[0] > maxs[0] ||
			p[1] < mins[1] ||
			p[1] > maxs[1])
		{
			return false;
		}
		return true;
	}

	/**
	 * Returns true if this box completely contains the other box.
	 * It is considered to be contained if an edge is shared.
	 */
	bool Contains(const Box2<T> &b, int excludeAxisMask = 0) const
	{
		for (int axis = 0, bit = 1; axis < 2; ++axis, bit *= 2)
		{
			if (bit & excludeAxisMask)
			{
				continue;
			}
			if (b.mins[axis] < mins[axis] ||
				b.maxs[axis] > maxs[axis])
			{
				return false;
			}
		}
		return true;
	}

	static const int EXCLUDE_X = 1;
	static const int EXCLUDE_Y = 2;

	/**
	 * Returns true if the given box intersects this one.
	 * Sharing an edge is considered an intersection.
	 * @param b The other box.
	 * @param excludeAxisMask A bitmask to exclude one or more axes. The box
	 *        will be considered infinite in the flagged axes.
	 *        Uses the const values EXCLUDE_X, EXCLUDE_Y and EXCLUDE_Z.
	 */
	bool Intersects(const Box2<T> &b, int excludeAxisMask = 0) const
	{
		for (int axis = 0, bit = 1; axis < 2; ++axis, bit *= 2)
		{
			if (bit & excludeAxisMask)
			{
				continue;
			}
			if (b.mins[axis] > maxs[axis] ||
				b.maxs[axis] < mins[axis])
			{
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns true if the given box intersects this one.
	 * Sharing an edge is considered an intersection.
	 * @param b The other box.
	 * @param offset The offset applied to the other box before checking if it
	 *        intersects us.
	 * @param excludeAxisMask A bitmask to exclude one or more axes. The box
	 *        will be considered infinite in the flagged axes.
	 *        Uses the const values EXCLUDE_X, EXCLUDE_Y and EXCLUDE_Z.
	 */
	bool Intersects(const Box2<T> &b, const Vector2<T> &offset, int excludeAxisMask = 0) const
	{
		for (int axis = 0, bit = 1; axis < 2; ++axis, bit *= 2)
		{
			if (bit & excludeAxisMask)
			{
				continue;
			}
			if ((b.mins[axis] + offset[axis]) > maxs[axis] ||
				(b.maxs[axis] + offset[axis]) < mins[axis])
			{
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns the shortest distance between this box and the specified point.
	 */
	T ShortestDistance(const Vector2<T> &v) const
	{
		Vector2<T> p = v;
		for (int axis = 0; axis < 2; ++axis)
		{
			if (p[axis] < mins[axis])
			{
				p[axis] = mins[axis];
			}
			if (p[axis] > maxs[axis])
			{
				p[axis] = maxs[axis];
			}
		}
		p -= v;
		return p.Length();
	}

	/**
	 * Returns the box corner that protrudes the furthest in the given axis.
	 */
	Vector2<T> FurthestCornerInDirection(const Vector2<T> &direction) const
	{
		Vector2<T> v;
		for (int axis = 0; axis < 2; ++axis)
		{
			if (direction[axis] > 0)
			{
				v[axis] = maxs[axis];
			}
			else
			{
				v[axis] = mins[axis];
			}
		}
		return v;
	}

	/**
	 * Calculate the most extreme corners of the box in a given axis.
	 */
	void CalculateAxisExtents(const Vector2<T> &direction, Vector2<T> *min, Vector2<T> *max) const
	{
		for (int axis = 0; axis < 2; ++axis)
		{
			if (direction[axis] > 0)
			{
				min->v[axis] = mins[axis];
				max->v[axis] = maxs[axis];
			}
			else
			{
				min->v[axis] = maxs[axis];
				max->v[axis] = mins[axis];
			}
		}
	}

	/**
	 * Expands the box by radius in every axis.
	 */
	void Expand(T radius)
	{
		for (int axis = 0; axis < 2; axis += 1)
		{
			mins[axis] -= radius;
			mins[axis] -= radius;
		}
	}

	/**
	 * Expands the box in the given vector direction.
	 * Positive components are added to maxs, negative components are added to
	 * mins.
	 */
	void Expand(const Vector2<T> &direction)
	{
		for (int axis = 0; axis < 2; axis += 1)
		{
			if (direction[axis] > 0)
			{
				maxs[axis] += direction;
			}
			else
			{
				mins[axis] += direction;
			}
		}
	}

	/**
	 * Expands this box to contain the other box.
	 */
	void Combine(const Box2<T> &other)
	{
		for (int axis = 0; axis < 2; axis += 1)
		{
			maxs[axis] = std::max(maxs[axis], other.maxs[axis]);
			mins[axis] = std::min(mins[axis], other.mins[axis]);
		}
	}

	/**
	 * Sets the mins of a box maintaining its size.
	 */
	void SetOffset(Vector2<T> offset)
	{
		maxs -= mins;
		maxs += offset;
		mins = offset;
	}

	/**
	 * Moves the entire box.
	 */
	void Move(Vector2<T> offset)
	{
		mins += offset;
		maxs += offset;
	}

	/**
	 * Comparison operators.
	 */
	bool operator==(const Box2<T>& rhs) const
	{
		return (mins == rhs.mins && maxs == rhs.maxs);
	}
	bool operator<(const Box2<T>& rhs) const
	{
		if (mins < rhs.mins) return true;
		if (mins > rhs.mins) return false;
		if (maxs < rhs.maxs) return true;
		return false;
	}
	bool operator!=(const Box2<T>& rhs) const { return !(*this == rhs); }
	bool operator<=(const Box2<T>& rhs) const { return (*this == rhs) || (*this < rhs); }
	bool operator>=(const Box2<T>& rhs) const { return !(*this < rhs); }
	bool operator>(const Box2<T>& rhs) const { return !(*this <= rhs); }
};

typedef Box2<double> Box2D;
typedef Box2<float>  Box2F;
typedef Box2<int>    Box2I;

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
