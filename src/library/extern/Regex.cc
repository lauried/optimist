/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "library/extern/Regex.hh"
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

Regex::Regex(std::string regex)
{
	numBrackets = 0;
	bool escape = false;
	for (int i = 0; i < regex.size(); i += 1)
	{
		if (regex[i] == '(')
		{
			if (escape == false)
			{
				numBrackets += 1;
			}
			escape = false;
		}
		else if (regex[i] == '\\')
		{
			escape = true;
		}
		else
		{
			escape = false;
		}
	}
	ready = true;
	this->regex = std::string(regex);
}

bool Regex::Match(const char *search, int searchLength, std::vector<std::string> *matches)
{
	std::vector<slre_cap> captures(numBrackets + 1);
	for (int i = 0; i < numBrackets + 1; i += 1)
	{
		captures[i].ptr = nullptr;
		captures[i].len = 0;
	}

	int result = slre_match(regex.c_str(), search, searchLength, &captures[0], numBrackets, 0);
	if (result >= 0)
	{
		if (matches != nullptr)
		{
			for (int i = 0; i < numBrackets + 1; i += 1)
			{
				if (captures[i].ptr != nullptr)
				{
					matches->push_back(std::string(captures[i].ptr, captures[i].len)); // now crashes here because the capture data is bad
				}
			}
		}
		return true;
	}
	return false;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
