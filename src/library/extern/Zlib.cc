/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "library/extern/Zlib.hh"
#include <iostream>
#include "zlib.h"
#include "library/text/String.hh"

//## add_libraries z

//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

void Zlib::Decompress(const std::vector<uint8_t> &source, std::vector<uint8_t> *dest, size_t maxSize)
{
	const int WINDOWBITS = 15;
	const int DETECT_FORMAT = 32; ///< Detect whether the data is zlib or gzip.

	if (maxSize < 1024)
	{
		throw zlib_decode_error("Maximum allowed size is too small");
	}
	if (dest->size() < 1024)
	{
		dest->resize(1024);
	}

    // Init the zlib for decompression
    z_stream stream;
    stream.zalloc = Z_NULL;
    stream.zfree = Z_NULL;
    stream.opaque = Z_NULL;
    stream.next_in = (Bytef*)&(source[0]);
    stream.avail_in = source.size();
    stream.next_out = (Bytef*)&(dest->at(0));
    stream.avail_out = dest->size();

    if (inflateInit2(&stream, WINDOWBITS + DETECT_FORMAT) != Z_OK)
    {
		throw zlib_decode_error("Failed to initialise stream");
    }

	// Decompress, expanding until we run out of size.
	while (true)
	{
		auto result = inflate(&stream, Z_FINISH);
		switch (result)
		{
		case Z_BUF_ERROR:
			{
				size_t lastSize = dest->size();
				if (lastSize == maxSize)
				{
					throw zlib_decode_error("Maximum size reached");
				}
				size_t newSize = lastSize * 2;
				if (newSize > maxSize)
				{
					newSize = maxSize;
				}
				dest->resize(newSize);
				stream.next_out = (Bytef*)&(dest->at(lastSize));
				stream.avail_out = newSize - lastSize;
			}
			break;
		case Z_STREAM_END:
			dest->resize(dest->size() - stream.avail_out);
			return;
		case Z_NEED_DICT:
			throw zlib_decode_error("Z_NEED_DICT");
		case Z_ERRNO:
			throw zlib_decode_error("Z_ERRNO");
		case Z_STREAM_ERROR:
			throw zlib_decode_error("Z_STREAM_ERROR");
		case Z_DATA_ERROR:
			throw zlib_decode_error("Z_DATA_ERROR");
		case Z_MEM_ERROR:
			throw zlib_decode_error("Z_MEM_ERROR");
		case Z_VERSION_ERROR:
			throw zlib_decode_error("Z_VERSION_ERROR");
		default:
			throw zlib_decode_error(String::ToString(result));
		}
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
