/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <vector>
#include <cstdint>
#include <cstddef>
#include <stdexcept>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class zlib_decode_error : public std::runtime_error {
public:
	zlib_decode_error(const std::string& what_arg) : runtime_error(what_arg) {}
	zlib_decode_error(const char* what_arg) : runtime_error(what_arg) {}
};

/**
 * A class for handling short compressed streams in memory.
 */
class Zlib {
public:
    /**
     * Decompress source into dest.
     * This method may resize the destination vector, but it won't attempt to
     * do so until it runs out of space, letting the consumer code estimate
     * the size of what it's to decompress and allocate what's probably
     * sufficient space.
     * If the maximum size is specified then no attempt will be made to
     * decompress more data than this specifies.
     * Returns true if we decompresseed everything, or false if the maximum size was reached or the stream was invalid.
     * If there was an error, dest may be modified but its content will be useless.
     * Maybe it would be better to throw exceptions to indicate those cases.
     */
    void Decompress(const std::vector<uint8_t> &source, std::vector<uint8_t> *dest, size_t maxSize = 0xfffff);

private:
	size_t firstIncrement;
	size_t incrementScale;
	size_t maxDecompressedSize;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

