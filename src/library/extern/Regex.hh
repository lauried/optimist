/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <string>
#include <vector>

extern "C"
{
#include "library/extern/slre.h"
}
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Super Light Regular Expressions: C++ wrapper
 */
class Regex {
public:
	Regex(std::string regex);

	/**
	 * Return true if the regex compiled successfully, else false.
	 */
	bool Ready() { return ready; }

	/**
	 * Returns true if the regular expression matched the search buffer, else
	 * false. If matches is provided, fills the array with a list of string
	 * matches.
	 */
	bool Match(const char *search, int searchLength, std::vector<std::string> *matches = nullptr);

private:
	bool ready;
	int numBrackets;
	std::string regex;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
