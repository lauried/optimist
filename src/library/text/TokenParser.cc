/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "library/text/TokenParser.hh"
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

TokenParser::TokenParser(const char *data, int dataLength, ITokenizer *tokenizer)
{
	buffer = data;
	bufferLength = dataLength;

	tokenizer->SetData(data, dataLength);
	this->tokenizer = tokenizer;
	eof = false;

	// We're always one token ahead, so we know if it's an eof.
	GetNextToken();
}

TokenParser::~TokenParser()
{
}

bool TokenParser::Eof()
{
	return eof;
}

const Token* TokenParser::PeekToken() const
{
	ExpectNotEof();
	return &currentToken;
}

const Token* TokenParser::PeekTokenSafe() const
{
	return &currentToken;
}

Token TokenParser::GetToken()
{
	ExpectNotEof();
	Token token = currentToken;
	GetNextToken();
	return token;
}

Token TokenParser::ExpectToken(std::string value)
{
	Token token = GetToken();
	if (token.str().compare(value))
	{
		throw ParseException(token.line, token.column, "Expected token '" + value + "', got '" + token.str() + "'");
	}
	return token;
}

Token TokenParser::ExpectTokenType(std::string typeId)
{
	Token token = GetToken();
	if (token.typeId.compare(typeId))
	{
		throw ParseException(token.line, token.column, "Unexpected token type");
	}
	return token;
}

void TokenParser::ExpectEof() const
{
	if (!eof)
	{
		throw ParseException(currentToken.line, currentToken.column, "Expected EOF");
	}
}

void TokenParser::ExpectNotEof() const
{
	if (eof)
	{
		throw ParseException(currentToken.line, currentToken.column, "Unexpected EOF");
	}
}

void TokenParser::ThrowCustomParseException(std::string message) const
{
	throw ParseException(currentToken.line, currentToken.column, message);
}

// Either currentToken is updated, or eof is set to true.
// This will only happen after the current token is used.
void TokenParser::GetNextToken()
{
	// After returning the current token, we try to pull the next one.
	if (tokenizer->GetToken(&currentToken))
	{
		if (currentToken.buffer == nullptr)
		{
			currentToken.buffer = &buffer[currentToken.offset];
		}
	}
	else
	{
		eof = true;
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
