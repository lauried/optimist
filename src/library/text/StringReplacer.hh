/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <string>
#include <map>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class StringReplacer {
public:
	void AddReplacement(const std::string &search, const std::string &replace);

	std::string Replace(const std::string &input);

private:
	struct SearchNode {
		std::map<char, SearchNode*> children;
		std::string search;
		std::string replacement;

		~SearchNode();

		/**
		 * Inserts a key.
		 * @param at The character we're inserting from. I.e. if we're already
		 * inside the node for the first character, then at == 1.
		 */
		void Insert(const std::string &search, const std::string &replace, int at = 0);

		SearchNode* GetChild(char c);
	};

	SearchNode searchTree;

	/**
	 * Traverses the search tree using the characters at the offset in input.
	 * If the resultant node has a replacement, then we return the node, else
	 * we return nullptr.
	 *
	 * @param input The input string that we're trying to match against.
	 * @param offset The offset in input string that we're starting from.
	 */
	SearchNode* FindMatch(const std::string &input, int offset);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
