/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "library/text/ITokenizer.hh"
#include "library/text/ParseException.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class TokenParser {
public:
	TokenParser(const char *data, int dataLength, ITokenizer *tokenizer);
	~TokenParser();

	bool Eof();
	const Token* PeekToken() const;
	const Token* PeekTokenSafe() const; // doesn't throw an exception
	Token GetToken();

	Token ExpectToken(std::string value);
	Token ExpectTokenType(std::string typeId);
	void ExpectEof() const;
	void ExpectNotEof() const;

	void ThrowCustomParseException(std::string message) const;

private:
	const char *buffer;
	int bufferLength;
	ITokenizer *tokenizer;

	Token currentToken;
	bool eof;

	// Read next token into currentToken, if we don't already have one.
	void GetNextToken();
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
