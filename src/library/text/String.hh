/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <string>
#include <sstream>
#include <vector>
#include <list>
#include <map>
#include <cstdlib>
#include <locale>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class String {
public:
	/**
	 * Return true if either string starts with the other.
	 */
	static bool StartsWith(std::string a, std::string b)
	{
		int alen = a.size();
		int blen = b.size();
		int len = alen < blen ? alen : blen;
		return a.compare(0, len, b) == 0;
	}

	/**
	 * Return true if a string is not empty and composed entirely of numeric characters.
	 */
	static bool IsNumeric(std::string &str)
	{
		std::locale loc;
		size_t len = str.length();
		if (len < 1)
		{
			return 0;
		}
		for (int i = 0; i < len; i += 1)
		{
			if (!std::isdigit(str[i], loc))
			{
				return false;
			}
		}
		return true;
	}

	static int CaseCompare(const char *s1, const char *s2); ///< Non-posix string comparison ignoring case.
	static void ToLower(char *s); ///< Non-posix conversion of string to lower case.
	static void ToUpper(char *s); ///< Non-posix conversion of string to upper case.

	static int ParseHex(const std::string &s, int start, int end = 0); ///< Parses hexadecimal to an int.

	/**
	 * Template for converting simple types to strings, one-off to keep code clean.
	 */
	template <typename T>
	static inline std::string ToString(T t)
	{
		std::ostringstream o;
		o << t;
		return o.str();
	}

	/**
	 * Convert string to float.
	 */
	static inline float ToFloat(const std::string &s)
	{
		// atof returns double, not float
		return std::atof(s.c_str());
	}

	/**
	 * Convert string to double.
	 */
	static inline double ToDouble(const std::string &s)
	{
		// atof returns double, not float
		return std::atof(s.c_str());
	}

	/**
	 * Convert string to int.
	 */
	static inline float ToInt(const std::string &s)
	{
		return std::atoi(s.c_str());
	}

	/**
	 * Parses an integer, either normal or hexadecimal starting with '0x'.
	 */
	static int ParseInt(const std::string &s);

	/**
	 * Converts a char to a binary representation in string form, e.g. "00000000".
	 */
	static std::string BinaryCharToString(unsigned char c);

	/**
	 * Format an integer placing the spacer in between blocks of 3 digits.
	 * @todo add support for fractional numbers by specifying by how much the
	 * real number was multiplied to get the integer.
	 * i.e. integer = floatVal * 1000 to show three fractional digits at base 10.
	 * However, the parameter should just show the number of digits and use base
	 * to the power of digits to get the multiplier.
	 */
	static std::string FormatInteger(int integer, const char spacer, const int base=10);

	/**
	 * Trim leading and trailing characters in c from s.
	 */
	static std::string Trim(const std::string &s, const std::string &c);

	/**
	 * Splits a string by the given delimiter.
	 */
	static void Split(const std::string& str, std::vector<std::string>* tokens, const std::string& delimiter = " ", int maxTokens = 0, bool allowEmpty = true);

	/**
	 * Joins a container back by its string.
	 * The first parameter must be an iterable container with begin() and end()
	 * and a value type that can be pushed to an ostringstream.
	 */
	template <typename T>
	static std::string Join(T parts, const std::string &delimiter)
	{
		std::ostringstream o;
		for (auto it = parts.begin(); it != parts.end(); ++it)
		{
			if (it != parts.begin())
			{
				o << delimiter;
			}
			o << *it;
		}
		return o.str();
	}

	/**
	 * Performs searches and replaces in a string.
	 * The replacements are performed simultaneously, so if any replacement
	 * string contains any searched-for string, the replacement will not be
	 * replaced.
	 * Also, longer searches take precedence over shorter ones.
	 */
	static std::string Replace(const std::string &str, const std::map<std::string, std::string> *replacements);

	/**
	 * Tokenize str into tokens by delimiters.
	 * Made somewhat obsolete by the more versatile TextParser.
	 * @todo Rename to Split.
	 */
	static void Tokenize(const std::string& str, std::vector<std::string>* tokens, const std::string& delimiters = " ", int maxTokens = 0, bool allowEmpty = true);

	/**
	 * Tokenize string into tokens by whitespace, except when within quotes.
	 * Legacy Note: EntityTokenize was the equivalent of this with stripQuotes
	 * true, MyTokenize had stripQuotes false.
	 * TODO: allow the delimiters to be specified too.
	 * @param str    - The string to be tokenized
	 * @param tokens - A pointer to a vector or list of strings to which the
	 *                 tokens will be added.
	 * @param quotes - A string containing all the characters used as quotes.
	 * @param stripQuotes - if true, will strip quotes from the tokens.
	 */
	static void TokenizeQuoted(const std::string& str, std::vector<std::string> *tokens, bool stripQuotes=true, const std::string& quotes="\"");
	static void TokenizeQuoted(const std::string& str, std::list<std::string> *tokens, bool stripQuotes=true, const std::string& quotes="\"");

	/**
	 * Template for converting our vector types to string.
	 */
	template <typename T>
	static inline std::string VectorToString(T t)
	{
		std::ostringstream o;
		o << t.Element(0);
		for (int i = 1; i < t.NumElements(); ++i)
		{
			o << " ";
			o << t.Element(i);
		}
		return o.str();
	}

	/**
	 * Template for converting string to a vector.
	 */
	template <typename T>
	static inline T ToVector(const std::string s)
	{
		std::vector<std::string> tokens;
		Tokenize(s, &tokens);
		T vec;
		for (int i = 0; i < vec.NumElements() && i < tokens.size(); ++i)
		{
			vec[i] = std::atof(tokens[i].c_str());
		}
		return vec;
	}

	/**
	 * Transformation of camel case strings.
	 *
	 * Detects word boundaries either by the first letter being capital, or by a
	 * specified separator character, e.g. '_'.
	 *
	 * Non-separator characters are ignored.
	 */
	static std::string TransformCamelCase(std::string in, bool firstWordCapital, bool wordCapital, char outputSeparator = '\0', char inputSeparator = '_');

	/**
	 * Reads off lines of text from a const text buffer, breaking by the newline character '\n'.
	 *
	 * @param buffer A const char array.
	 * @param bufferLength The maximum length of the buffer. Reading will not overrun this.
	 * @param position An int pointer which is taken as the start position within the buffer
	 * for reading, and will be updated with the position after the line which has been read.
	 * @return The line which has been read.
	 */
	static std::string ReadLine(const char *buffer, int bufferLength, int *position);

	/**
	 * Returns the number of characters in a UTF-8 string.
	 *
	 * @param const char *utf8 A buffer containing characters to measure the length of.
	 * @param int maxLength The maximum length of the buffer. If omitted, then no maximum is used.
	 *                      The string will always be considered to terminate at the first terminator character.
	 */
	static inline int UTF8Length(const char *utf8, int maxLength = -1)
	{
		int length = 0;
		for (int i = 0; i != maxLength && utf8[i] != '\0'; i += 1)
		{
			// Characters starting 10xx are ignored, as they are continuation
			// characters.
			if (utf8[i] & 0xC0 != 0x80)
			{
				length += 1;
			}
		}
		return length;
	}

	/**
	 * Reads a unicode character from a string.
	 *
	 * @param string& utf8 The string being parsed.
	 * @param unsigned int offset The offset at which to start reading the string.
	 * @param int* unicode A pointer to an int which will be written with the unicode value of the character read.
	 *
	 * @return int The number of bytes read. Note that this can be zero if there were malformed characters.
	 *
	 * @todo Make the return value skip all the malformed characters and just set *unicode to zero.
	 */
	static inline int UTF8ToUnicode(const std::string& utf8, unsigned int offset, int* unicode)
	{
		if (!utf8.length())
			return 0;

		if (utf8.length() < offset+1)
		{
			printf("Utf8ToUnicode: string '%s' was shorter than offset %i\n", utf8.c_str(), offset);
			return 0;
		}

		unsigned char bytes[4];
		bytes[0] = utf8[offset];

		// 1 byte long code
		if ((bytes[0] & 0x80) == 0) // starts 0xxx
		{
			*unicode = bytes[0];
			return 1;
		}
		// 2 byte long code
		if ((bytes[0] & 0xE0) == 0xC0) // starts 110x
		{
			if (utf8.length() < offset+2)
			{
				printf("Utf8ToUnicode: string '%s' was shorter than offset %i (2 bytes)\n", utf8.c_str(), offset);
				return 0;
			}

			bytes[1] = utf8[offset+1];
			if ((bytes[1] & 0xC0) != 0x80) // doesn't start 10xx
			{
				printf("Utf8ToUnicode: string '%s' offset %i byte %i (%s) doesn't start 10xx\n",
					utf8.c_str(), offset+1, 2, BinaryCharToString(bytes[1]).c_str());
				return 0;
			}

			*unicode = ((bytes[0] & 0x1F) << 6)
					 +  (bytes[1] & 0x3F);
			return 2;
		}
		// 3 byte long code
		if ((bytes[0] & 0xF0) == 0xE0) // starts 1110
		{
			if (utf8.length() < offset+3)
			{
				printf("Utf8ToUnicode: string '%s' was shorter than offset %i (3 bytes)\n", utf8.c_str(), offset);
				return 0;
			}

			for (int i=1; i<=2; ++i)
			{
				bytes[i] = utf8[offset+i];
				if ((bytes[i] & 0xC0) != 0x80) // doesn't start 10xx
				{
					printf("Utf8ToUnicode: string '%s' offset %i byte %i (%s) doesn't start 10xx\n",
						utf8.c_str(), offset+i, i+1, BinaryCharToString(bytes[i]).c_str());
					return 0;
				}
			}

			*unicode = ((bytes[0] & 0x0F) << 12)
					 + ((bytes[1] & 0x3F) << 6)
					 +  (bytes[2] & 0x3F);
			return 3;
		}
		// 4 byte long code
		if ((bytes[0] & 0xF8) == 0xF0) // starts 11110xxx
		{
			if (utf8.length() < offset+4)
			{
				printf("Utf8ToUnicode: string '%s' was shorter than offset %i (3 bytes)\n", utf8.c_str(), offset);
				return 0;
			}

			for (int i=1; i<=3; ++i)
			{
				bytes[i] = utf8[offset+i];
				if ((bytes[i] & 0xC0) != 0x80) // doesn't start 10xx
				{
					printf("Utf8ToUnicode: string '%s' offset %i byte %i (%s) doesn't start 10xx\n",
						utf8.c_str(), offset+i, i+1, BinaryCharToString(bytes[i]).c_str());
					return 0;
				}
			}

			*unicode = ((bytes[0] & 0x07) << 18)
					 + ((bytes[1] & 0x3F) << 12)
					 + ((bytes[2] & 0x3F) << 6)
					 +  (bytes[3] & 0x3F);
			return 4;
		}

		printf("Utf8ToUnicode: string '%s' offset %i invalid sequence start %x\n", utf8.c_str(), offset, bytes[0]);
		return 0;
	}

	/**
	 * Converts a unicode character to a string.
	 *
	 * @param code The unicode to convert.
	 * @param output An array of at least 7 bytes into which the output will be written and zero terminated.
	 *
	 * @return The number of bytes in the code, excluding the terminator.
	 */
	static inline int UnicodeToUTF8(int code, char *output)
	{
		if (code < 0x80)
		{
			output[0] = code & 0x7F;
			output[1] = '\0';
			return 1;
		}
		if (code < 0x800)
		{
			output[0] = ((code >> 6) & 0x1F) + 0xC0;
			output[1] = (code & 0x3F) + 0x80;
			output[2] = '\0';
			return 2;
		}
		if (code < 0x10000)
		{
			output[0] = ((code >> 12) & 0x0F) + 0xE0;
			output[1] = ((code >> 6) & 0x3F) + 0x80;
			output[2] = (code & 0x3F) + 0x80;
			output[3] = '\0';
			return 3;
		}
		if (code < 0x200000)
		{
			output[0] = ((code >> 18) & 0x07) + 0xF0;
			output[1] = ((code >> 12) & 0x3F) + 0x80;
			output[2] = ((code >> 6) & 0x3F) + 0x80;
			output[3] = (code & 0x3F) + 0x80;
			output[4] = '\0';
			return 4;
		}
		if (code < 0x4000000)
		{
			output[0] = ((code >> 24) & 0x03) + 0xF8;
			output[1] = ((code >> 18) & 0x3F) + 0x80;
			output[2] = ((code >> 12) & 0x3F) + 0x80;
			output[3] = ((code >> 6) & 0x3F) + 0x80;
			output[4] = (code & 0x3F) + 0x80;
			output[5] = '\0';
			return 5;
		}
		output[0] = ((code >> 30) & 0x01) + 0xFC;
		output[1] = ((code >> 24) & 0x3F) + 0x80;
		output[2] = ((code >> 18) & 0x3F) + 0x80;
		output[3] = ((code >> 12) & 0x3F) + 0x80;
		output[4] = ((code >> 6) & 0x3F) + 0x80;
		output[5] = (code & 0x3F) + 0x80;
		output[6] = '\0';
		return 6;
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
