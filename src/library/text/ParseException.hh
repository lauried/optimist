/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <exception>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Exception object thrown by TextParser.
 * @todo Needs an enum to specify the specific error type.
 * @todo Derive from our library exception.
 */
struct ParseException : public std::exception {
	std::string message; ///< The reason the exception was thrown.
	int line; ///< The current line number in the original file containing the current token.
	int token; ///< The number of tokens already read. @todo This woule be better off as the index of the token on the line it's on.
	ParseException(int l, int t, std::string m) : line(l), token(t), message(m) { }
	~ParseException() throw() { }
	const char *what() { return message.c_str(); }
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
