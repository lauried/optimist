/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <map>
#include <string>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Simple (for now) pre-processor.
 * Directives are lines that start with #
 * The only directive is #define <match> <replacement> where the replacement is
 * the rest of the line.
 * The match must start with a-Z, and can then be followed by a-Z0-9_
 * Instances of the match which are not adjacent to a-Z0-9_ will be replaced
 * with the replacement.
 * Line numbering will always be preserved.
 *
 * @todo: Needs a callback to perform immediate includes, otherwise we could
 * include a definitions file and not have it available immediately.
 */
class Preprocessor {
public:
	std::string Process(std::string &input);
	std::string Process(const char *input, int inputLen);
	void AddDefine(std::string match, std::string replacement);
	void Undefine(std::string match);

private:
	std::map<std::string, std::string> defines;

	// Replaces the token if it matches one of the defines.
	void Replace(std::string *token);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
