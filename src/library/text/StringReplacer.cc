/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "library/text/StringReplacer.hh"
#include <sstream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// StringReplacer
//-----------------------------------------------------------------------------

void StringReplacer::AddReplacement(const std::string &search, const std::string &replace)
{
	if (search.length() < 1)
	{
		throw std::logic_error("Search string must be at least one character long");
	}
	searchTree.Insert(search, replace);
}

std::string StringReplacer::Replace(const std::string &input)
{
	std::ostringstream output;

	for (int i = 0; i < input.length(); i += 1)
	{
		SearchNode *node = FindMatch(input, i);
		if (node != nullptr)
		{
			output << node->replacement;
			i += node->search.length() - 1;
		}
		else
		{
			output << input[i];
		}
	}

	return output.str();
}

StringReplacer::SearchNode* StringReplacer::FindMatch(const std::string &input, int offset)
{
	SearchNode *currentNode = &searchTree;
	for (int i = offset; i < input.length(); i += 1)
	{
		char c = input[i];
        SearchNode *node = currentNode->GetChild(c);
        if (node == nullptr)
        {
			if (currentNode->replacement.length() > 0)
			{
				return currentNode;
			}
			return nullptr;
        }
        currentNode = node;
	}
}

//-----------------------------------------------------------------------------
// StringReplacer::SearchNode
//-----------------------------------------------------------------------------

StringReplacer::SearchNode::~SearchNode()
{
	for (auto it = children.begin(); it != children.end(); ++it)
	{
		delete it->second;
	}
}

void StringReplacer::SearchNode::Insert(const std::string &search, const std::string &replace, int at)
{
	if (at == search.length())
	{
		this->search = search;
		replacement = replace;
		return;
	}

	SearchNode *node;
	auto it = children.find(search[at]);
	if (it != children.end())
	{
		node = it->second;
	}
	else
	{
		node = new SearchNode();
		children.insert(std::make_pair(search[at], node));
	}
	node->Insert(search, replace, at + 1);
}

StringReplacer::SearchNode* StringReplacer::SearchNode::GetChild(char c)
{
	auto it = children.find(c);
	if (it != children.end())
	{
		return it->second;
	}
	return nullptr;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
