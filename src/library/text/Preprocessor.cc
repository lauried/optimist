/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "library/text/Preprocessor.hh"
#include <cctype>
#include <list>
#include <iostream>
#include <sstream>
#include "library/text/TextParser.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Custom Tokenizer
//-----------------------------------------------------------------------------

/**
 * Tokenizer which separates tokens on word boundaries.
 * Words, blocks of whitespace, blocks of newlines and non-words are all kept
 * intact as tokens.
 */
class PreprocessorTokenizer : public ITokenizer {
public:
	PreprocessorTokenizer();
	void SetData(const char *str, int length);
	bool GetToken(Token *token);

private:
	const char *data;
	int dataLength;

	enum {
		NOTHING,
		WORD,
		STRING,
		WHITESPACE,
		NEWLINE,
		OTHER
	} type;

	int pos;
	int tokenStart;
	char c, oldc, oldoldc;
};

PreprocessorTokenizer::PreprocessorTokenizer()
{
	type = NOTHING;
}

void PreprocessorTokenizer::SetData(const char *str, int length)
{
	data = str;
	dataLength = length;
	pos = 0;
	tokenStart = 0;
	type = NOTHING;
	c = '\0';
	oldc = '\0';
}

bool PreprocessorTokenizer::GetToken(Token *token)
{
	bool breakToken = false;
	while (!breakToken)
	{

		pos += 1;
		if (pos > dataLength)
		{
			return false;
		}
        // last character of file
		if (pos == dataLength)
		{
			token->offset = tokenStart;
			token->length = pos - tokenStart;
			return true;
		}

		oldoldc = oldc;
		oldc = c;
		c = data[pos];

		switch (type)
		{
		case WORD:
			if (!std::isalnum(c) && c != '_') { breakToken = true; }
			break;
		case STRING:
			if (oldc == '"' && oldoldc != '\\' && ((pos - tokenStart) > 2 || oldoldc == '"')) { breakToken = true; }
			break;
		case WHITESPACE:
			if (c != '\t' && c != ' ') { breakToken = true; }
			break;
		case NEWLINE:
			breakToken = true;
			break;
		case OTHER:
			if ((std::isalpha(c) && !std::isalnum(oldc)) || c == '\r' || c == '\n' || c == '\t' || c == ' ' || c == '"') { breakToken = true; }
			break;
		default:
			break;
		}

		if (breakToken)
		{
			token->offset = tokenStart;
			token->length = pos - tokenStart;
			tokenStart = pos;
			type = NOTHING;
		}

		if (type == NOTHING)
		{
			if (std::isalnum(c) || c == '_')
			{
				type = WORD;
			}
			else if (c == '"')
			{
				type = STRING;
			}
			else if (c == '\t' || c == ' ')
			{
				type = WHITESPACE;
			}
			else if (c == '\r' || c == '\n')
			{
				type = NEWLINE;
			}
			else
			{
				type = OTHER;
			}
		}
	}

	return true;
}

//-----------------------------------------------------------------------------
// Preprocessor
//-----------------------------------------------------------------------------

std::string Preprocessor::Process(std::string &input)
{
	return Process(input.c_str(), input.length());
}

static bool IsNewLine(std::string &s)
{
	for (int i = 0; i < s.length(); i += 1)
	{
		if (s[i] != '\n' && s[i] != '\r')
		{
			return false;
		}
	}
	return true;
}

static void ExpectWhitespace(TextParser &t)
{
	std::string s = t.GetToken();
	for (int i = 0; i < s.length(); i += 1)
	{
		if (s[i] != ' ' && s[i] != '\t')
		{
			std::cout << s << std::endl;
			t.ThrowCustomParseException("Whitespace expected in directive");
		}
	}
}

std::string Preprocessor::Process(const char *input, int inputLen)
{
	try
	{
		PreprocessorTokenizer tokenizer;
		TextParser parser(input, inputLen, tokenizer, true);

		std::ostringstream output;

		while (!parser.Eof())
		{
			std::string token = parser.GetToken();
			//std::cout << "TOKEN:" << token << std::endl;
			if (!token.compare("#"))
			{
				std::string directive = parser.GetToken();
				if (!directive.compare("const"))
				{
					ExpectWhitespace(parser);
					std::string key = parser.GetToken();
					ExpectWhitespace(parser);
					//std::cout << "KEY:" << key << std::endl;
					std::ostringstream value;
					for (token = parser.GetToken(); !IsNewLine(token) && !parser.Eof(); token = parser.GetToken())
					{
						//std::cout << "VAL TOK:" << token << std::endl;
						Replace(&token);
						value << token;
					}
					AddDefine(key, value.str());
				}
				else if (!directive.compare("unconst"))
				{
					ExpectWhitespace(parser);
					std::string key = parser.GetToken();
					Undefine(key);
				}
				else
				{
					parser.ThrowCustomParseException("Unknown directive");
				}

				if (!parser.Eof())
				{
					if (!IsNewLine(token))
					{
						parser.ThrowCustomParseException("Expected newline after directive");
					}
					output << token; // put the newline onto the output so we maintain line numbers
				}
			}
			else
			{
				for (; !IsNewLine(token) && !parser.Eof(); token = parser.GetToken())
				{
					//std::cout << "REPLACABLE TOK:" << token << std::endl;
					Replace(&token);
					output << token;
				}
                //std::cout << "NEWLINE" << std::endl;
                output << token;
			}
		}

		//std::cout << output.str() << std::endl;
		return output.str();
	}
	catch (ParseException &e)
	{
		std::cout << "Preprocessing failed at line " << e.line << ": " << e.message << std::endl;
		return std::string(input);
	}
}

void Preprocessor::AddDefine(std::string match, std::string replacement)
{
	//std::cout << "ADD DEFINE: " << match << " => " << replacement << std::endl;

	auto it = defines.find(match);
	if (it == defines.end())
	{
		defines.insert(std::make_pair(match, replacement));
	}
	else
	{
		it->second = replacement;
	}
}

void Preprocessor::Undefine(std::string match)
{
	auto it = defines.find(match);
	if (it != defines.end())
	{
		defines.erase(it);
	}
}

void Preprocessor::Replace(std::string *token)
{
	auto it = defines.find(*token);
	if (it != defines.end())
	{
		*token = it->second;
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
