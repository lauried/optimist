/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <string>
#include <vector>
#include <initializer_list>
#include "library/text/String.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class Path {
public:
	enum ParseFlags {
		F_Path = 1,
		F_PathParts = 2,
		F_Filename = 4,
		F_Basename = 8,
		F_Extension = 16,
		F_NormalizePath = 32, ///< Normalize the path, removing relative parts.
	};

	std::string path; ///< Entire path up to the filename.
	std::vector<std::string> pathParts; ///< The path split into individual directories.
	std::string filename; ///< Filename with extension.
	std::string basename; ///< Filename without extension.
	std::string extension; ///< Extension.

	/**
	 * Parses a filename into the parts specified in flags.
	 * Guarantees to contain the information specified by ParseFlags.
	 * May contain more information than specified, if it was needed in the
	 * course of calculating the required information.
	 */
	Path(const std::string &filename, int flags);

	/**
	 * Join two bits of path, ensuring that only one directory separator lies
	 * between the two path parts.
	 */
	static std::string Join(std::initializer_list<std::string> strings);

private:
	void BuildPathParts();
	void NormalizePath();
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
