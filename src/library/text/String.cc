/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "library/text/String.hh"
#include <cmath>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

int String::CaseCompare(const char *s1, const char *s2)
{
	register char c1, c2;

	if (!s1)
	{
		return s2 ? -1 : 0;
	}
	if (!s2)
	{
		return 1;
	}

	do
	{
		c1 = toupper(*s1++);
		c2 = toupper(*s2++);
	} while (c1 && c1 == c2);

	// The following case analysis is necessary so that characters
	// which look negative collate low against normal characters but
	// high against the end-of-string NUL.
	if (c1 == c2)
	{
		return(0);
	}
	else if (c1 == '\0')
	{
		return(-1);
	}
	else if (c2 == '\0')
	{
		return(1);
	}
	else
	{
		return(c1 - c2);
	}
}

void String::ToLower(char *s)
{
	for (; *s; s += 1)
	{
		*s = tolower(*s);
	}
}

void String::ToUpper(char *s)
{
	for (; *s; s += 1)
	{
		*s = toupper(*s);
	}
}

int String::ParseHex(const std::string &s, int start, int end)
{
	union {
		unsigned int u;
		signed int i;
	} un;
	un.u = 0;

	for (int i = start; i < s.length() - end; ++i)
	{
		unsigned char val = s[i];
		if (val >= '0' && val <= '9')
		{
			val -= '0';
		}
		else if (val >= 'a' && val <= 'f')
		{
			val -= 'a';
			val += 10;
		}
		else if (val >= 'A' && val <= 'F')
		{
			val -= 'A';
			val += 10;
		}
		else
		{
			return 0;
		}
		un.u <<= 4;
		un.u += val;
	}
	return un.i;
}

int String::ParseInt(const std::string &s)
{
	if (s[0] == '#' || s[0] == 'x')
		return ParseHex(s, 1);
	if (s[0] == '0' && s[1] == 'x')
		return ParseHex(s, 2);
	return std::atoi(s.c_str());
}

void String::Split(const std::string &str, std::vector<std::string>* tokens, const std::string& delimiter, int maxTokens, bool allowEmpty)
{
	std::string::size_type delimiterLength = delimiter.length();
	std::string::size_type lastPos = 0;
	std::string::size_type pos = str.find(delimiter);

	while (std::string::npos != pos || std::string::npos != lastPos)
	{
		std::string token = str.substr(lastPos, pos - lastPos);
		if (allowEmpty || token.length())
		{
			tokens->push_back(token);
		}
		lastPos = pos + delimiterLength;
		pos = str.find(delimiter, lastPos);

		if (maxTokens && tokens->size() >= maxTokens - 1)
		{
			token = str.substr(lastPos);
			if (allowEmpty || token.length())
			{
				tokens->push_back(token);
			}
			break;
		}
	}
}

void String::Tokenize(const std::string& str, std::vector<std::string>* tokens, const std::string& delimiters, int maxTokens, bool allowEmpty)
{
	std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
	std::string::size_type pos = str.find_first_of(delimiters, lastPos);

	while (std::string::npos != pos || std::string::npos != lastPos)
	{
		std::string token = str.substr(lastPos, pos - lastPos);
		if (allowEmpty || token.length())
		{
			tokens->push_back(token);
		}
		lastPos = str.find_first_not_of(delimiters, pos);
		pos = str.find_first_of(delimiters, lastPos);

		if (maxTokens && tokens->size() >= maxTokens - 1)
		{
			token = str.substr(lastPos);
			if (allowEmpty || token.length())
			{
				tokens->push_back(token);
			}
			break;
		}
	}
}

std::string TrimCharacter(const std::string &s, const std::string &c)
{
	std::string::size_type start, end;
	start = s.find_first_not_of(c);
	if (start == std::string::npos)
		return "";
	end = s.find_last_not_of(c);
	if (end == std::string::npos)
		return "";
	return s.substr(start, (end - start) + 1);
}

std::string String::Trim(const std::string &s, const std::string &c)
{
	return TrimCharacter(s, c);
}

inline bool StringContains(const std::string& str, const char c)
{
	// TODO: is this faster or slower than string::findfirstof?
	for (unsigned int i=0; i<str.length(); ++i)
	{
		if (str.at(i) == c)
			return true;
	}
	return false;
}

template<class T>
void TokenizeQuotedTemplate(const std::string& str, T* tokens, bool stripQuotes, const std::string& quotes)
{
	// FixMe: This segfaults when given "\n" as a string - presumably when
	// the string is too short to actually contain tokens.

	std::string::size_type firstNonSpace = 0;
	char inQuotes = 0;
	for (std::string::size_type i=0; i<str.length(); ++i)
	{
		if (!inQuotes)
		{
			if (str[i] == ' ' || str[i] == '\t' || str[i] == '\n' || str[i] == '\r')
			{
				if (firstNonSpace < i)
				{
					std::string s;
					switch (stripQuotes)
					{
					case true:
						// FixMe: This trims " quotes, not whatever quotes we specify in the parameter
						s = TrimCharacter(str.substr(firstNonSpace, (i - firstNonSpace)), "\"");
						break;
					case false:
						s = str.substr(firstNonSpace, (i - firstNonSpace));
						break;
					default:
						break;
					}
					tokens->push_back(s);
				}
				firstNonSpace = i + 1;
			}
			else if (StringContains(quotes, str[i]))
			{
				inQuotes = str[i];
			}
		}
		else
		{
			if (str[i] == inQuotes)
			{
				inQuotes = 0;
			}
		}
	}

	if (firstNonSpace < str.length() - 1)
	{
		std::string s = TrimCharacter(str.substr(firstNonSpace, str.length() - firstNonSpace), "\"");
		tokens->push_back(s);
	}
}

void String::TokenizeQuoted(const std::string& str, std::vector<std::string>* tokens,
	bool stripQuotes, const std::string& quotes)
{
	TokenizeQuotedTemplate< std::vector<std::string> >(str, tokens, stripQuotes, quotes);
}

void String::TokenizeQuoted(const std::string& str, std::list<std::string>* tokens,
	bool stripQuotes, const std::string& quotes)
{
	TokenizeQuotedTemplate< std::list<std::string> >(str, tokens, stripQuotes, quotes);
}

std::string String::FormatInteger(int integer, const char spacer, const int base)
{
	int size = 0;
	for (int i=integer; i!=0; i/=base) { ++size; }

	size = size + floor((size-1) / 3);

	std::vector<char> buffer(size + 1);
	buffer[size] = '\0';

	for (int i=0; integer; ++i)
	{
		if (i && (i%3 == 0))
			buffer[--size] = spacer;

		int digit = integer%base;
		integer /= base;
		buffer[--size] = '0' + digit;
	}
	std::string result(&buffer[0]);
	return result;
}

std::string String::BinaryCharToString(unsigned char c)
{
	std::string s;
	for (int i=1; i<256; i=i<<1)
	{
		if (c & i)
			s += "1";
		else
			s += "0";
	}
	return s;
}

std::string String::TransformCamelCase(std::string in, bool firstWordCapital, bool wordCapital, char outputSeparator, char inputSeparator)
{
	std::ostringstream o;

	bool wordStart = false;
	bool firstWord = true;
	for (int i = 0; i < in.length(); i += 1)
	{
		char c = in[i];
		if (i == 0 || isupper(c))
		{
			wordStart = true;
		}
		if (isalpha(c) && wordStart)
		{
			if (outputSeparator != '\0')
			{
				o << outputSeparator;
			}
			if (firstWord == true)
			{
				o << (char)(firstWordCapital ? toupper(c) : tolower(c));
				firstWord = false;
			}
			else
			{
				o << (char)(wordCapital ? toupper(c) : tolower(c));
			}
			wordStart = false;
		}
		else if (c == inputSeparator)
		{
			wordStart = true;
		}
		else
		{
			o << c;
			wordStart = false;
		}
	}

	return o.str();
}

std::string String::ReadLine(const char *buffer, int bufferLength, int *position)
{
	std::stringstream output;
	for (; *position < bufferLength; *position += 1)
	{
		if (buffer[*position] == '\n')
		{
			*position += 1;
			return output.str();
		}
		output << buffer[*position];
	}
	return output.str();
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

