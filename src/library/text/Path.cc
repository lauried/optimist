/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "library/text/Path.hh"
#include <sstream>
#include "library/containers/InspectableStack.hh"
#include "library/text/String.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

Path::Path(const std::string &str, int flags)
{
	size_t lastSlash = str.find_last_of("/");
	if (lastSlash != std::string::npos && (flags & (F_Path | F_PathParts | F_NormalizePath)))
	{
		path = str.substr(0, lastSlash);
		if (flags & (F_PathParts | F_NormalizePath))
		{
			BuildPathParts();
		}
		if (flags & F_NormalizePath)
		{
			NormalizePath();
		}
	}
	if (flags & (F_Filename | F_Basename | F_Extension))
	{
		filename = str.substr(lastSlash + 1);
		if (flags & (F_Basename | F_Extension))
		{
			size_t firstDot = filename.find_first_of(".");
			if (firstDot == std::string::npos)
			{
				basename = filename;
			}
			else
			{
				basename = filename.substr(0, firstDot);
				extension = filename.substr(firstDot + 1);
			}
		}
	}
}

void Path::BuildPathParts()
{
	if (path.length() < 1)
	{
		return;
	}
	std::string tmpPath = (path[0] == '/') ? path.substr(1) : path;
	std::string delims("/");
	String::Tokenize(tmpPath, &pathParts, delims);
}

void Path::NormalizePath()
{
	// push actual path parts onto a stack, and remove them when ".." is encountered.
	InspectableStack<std::string> folderStack;
	for (auto it = pathParts.begin(); it != pathParts.end(); ++it)
	{
		if (!it->compare("."))
		{
			continue;
		}
		if (!it->compare(".."))
		{
			if (folderStack.size() == 0 || !folderStack.at_top(0).compare(".."))
			{
				folderStack.push(*it);
			}
			else
			{
				folderStack.pop();
			}
		}
		else
		{
			folderStack.push(*it);
		}
	}

	// then replace pathparts with the contents of the stack, starting from the bottom
	pathParts.resize(0);
	for (int i = 0; i < folderStack.size(); i += 1)
	{
		pathParts.push_back(folderStack.at_bottom(i));
	}

	// then join them all into the real path
	path = String::Join(pathParts, "/");
}

std::string Path::Join(std::initializer_list<std::string> strings)
{
	std::ostringstream o;
	bool needSeparator = false;
	for (auto it = strings.begin(); it != strings.end(); ++it)
	{
		std::string pathPart = *it;
		if (pathPart.length() < 1)
		{
			continue;
		}
		if (pathPart[0] == '/' || pathPart[0] == '\\')
		{
			needSeparator = 0;
		}
		if (needSeparator)
		{
			o << "/";
		}
		o << pathPart;
		needSeparator = true;
		if (pathPart[pathPart.length() - 1] == '/' || pathPart[pathPart.length() - 1] == '\\')
		{
			needSeparator = false;
		}
	}
	return o.str();
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
