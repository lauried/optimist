/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "library/text/CommandLineArguments.hh"
#include <stdexcept>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

CommandLineArguments::CommandLineArguments(int argc, char *argv[])
{
	for (int i = 0; i < argc; i += 1)
	{
		std::string arg(argv[i]);
		size_t pos = arg.find_first_of("=");
		if (pos == std::string::npos)
		{
			commands.push_back(arg);
		}
		else
		{
			values.insert(std::make_pair(arg.substr(0, pos), arg.substr(pos + 1)));
		}
	}
}

bool CommandLineArguments::HasKey(const char *key)
{
	auto it = values.find(std::string(key));
	return it != values.end();
}

std::string CommandLineArguments::GetKey(const char *key, std::string defaultValue)
{
	auto it = values.find(std::string(key));
	if (it != values.end())
	{
		return it->second;
	}
	return defaultValue;
}

std::string CommandLineArguments::RequireKey(const char *key)
{
	auto it = values.find(std::string(key));
	if (it != values.end())
	{
		return it->second;
	}
	throw std::runtime_error("Missing required parameter");
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
