/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "library/text/Tokenizer.hh"
#include <stdexcept>
#include <iostream>
#include <cctype>
#include "library/text/String.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

TokenType::~TokenType()
{
}

Tokenizer::~Tokenizer()
{
	for (int i = 0; i < tokenTypeInfo.size(); i += 1)
	{
		delete tokenTypeInfo[i].type;
	}
}

void Tokenizer::SetData(const char *str, int length)
{
	buffer = str;
	bufferLength = length;
	state = 0;
	numLines = 0;
	column = 0;
	currentPosition = 0;
}

bool Tokenizer::GetToken(Token *token)
{
retry:
	if (currentPosition >= bufferLength)
	{
		return false;
	}

	// Try each token type. The first to parse is the matched token, so we must
	// order our token types so that tokens with priority are matched first.
	for (int typeIndex = 0; typeIndex < tokenTypeInfo.size(); typeIndex += 1)
	{
		TokenTypeInfo *typeInfo = &tokenTypeInfo[typeIndex];
		if (typeInfo->state != state)
		{
			continue;
		}
		TokenType *type = typeInfo->type;
		TokenTypeFlags flags = typeInfo->flags;

        int length = type->TokenLength(&buffer[currentPosition], bufferLength - currentPosition);
        if (!length)
		{
			continue;
		}

		// else this token matches
		state = typeInfo->nextState;

		// If we skip this type, we don't write it to the token data.
		if(flags != SKIP)
		{
			token->offset = currentPosition;
			token->length = length;
			token->line = numLines + 1;
			token->column = column + 1;
			token->buffer = &buffer[currentPosition];
			token->typeId = typeInfo->name;
		}

		for (int tokenCharIndex = 0; tokenCharIndex < length; tokenCharIndex += 1)
		{
			if (buffer[tokenCharIndex + currentPosition] == '\n')
			{
				numLines += 1;
				column = 0;
			}
			else if (buffer[tokenCharIndex + currentPosition] == '\t')
			{
				column += 4; // TODO: Configurable tab width
			}
			else
			{
				column += 1;
			}
		}

		currentPosition += length;

		// If we skip this type, we just carry on instead of returning.
		if(flags == SKIP)
		{
			goto retry;
		}
		return true;
	}
	throw std::logic_error("No TokenType was able to read a token, at line " + String::ToString(numLines + 1) + " column " + String::ToString(column + 1));
}

//-----------------------------------------------------------------------------
// Tokenizer types
//-----------------------------------------------------------------------------

RegexTokenType::RegexTokenType(std::string expression) : regex("^(" + expression + ")")
{
	if (!regex.Ready())
	{
		throw std::logic_error("Could not compile regular expression");
	}
}

int RegexTokenType::TokenLength(const char *buffer, int maxLength)
{
	std::vector<std::string> matches;
	if (!regex.Match(buffer, maxLength, &matches))
	{
		return 0;
	}
	if (matches.size() < 1)
	{
		return 0;
	}
	return matches[0].length();
}

StringMatchTokenType::StringMatchTokenType(std::string match, Flags flags) : flags(flags)
{
	matches.push_back(match);
	lengths.push_back(match.length());
}

StringMatchTokenType::StringMatchTokenType(std::vector<std::string> matches, Flags flags) : flags(flags), matches(matches)
{
	for (int i = 0; i < matches.size(); i += 1)
	{
		lengths.push_back(matches[i].length());
	}
}

int StringMatchTokenType::TokenLength(const char *buffer, int maxLength)
{
	for (int i = 0; i < matches.size(); i += 1)
	{
		int length = lengths[i];
		if (maxLength < length)
		{
			continue;
		}
		if (!matches[i].compare(0, length, buffer, length))
		{
			if (flags & WHOLE_WORDS_ONLY && length < maxLength && std::isalpha(buffer[length]))
			{
				return 0;
			}
			return length;
		}
	}
	return 0;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
