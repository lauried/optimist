/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <vector>
#include "library/text/ITokenizer.hh"
#include "library/extern/Regex.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// We use an object rather than just a function call in case we want to create
// configurable token types, in which case we're now able to store the state
// information.
class TokenType {
public:
	virtual ~TokenType();

	/**
	 * Returns the number of characters of the start of buffer that match the
	 * token type. Zero means no match.
	 */
	virtual int TokenLength(const char *buffer, int maxLength) = 0;
};

class RegexTokenType : public TokenType {
public:
	RegexTokenType(std::string expression);
	int TokenLength(const char *buffer, int maxLength);

private:
	Regex regex;
};

// Only matches an exact string.
class StringMatchTokenType : public TokenType {
public:
	enum Flags {
		NONE = 0,
		WHOLE_WORDS_ONLY = 1
	};

	StringMatchTokenType(std::string match, Flags flags = NONE);
	StringMatchTokenType(std::vector<std::string> matches, Flags flags = NONE);
	int TokenLength(const char *buffer, int maxLength);

private:
	Flags flags;
	std::vector<std::string> matches;
	std::vector<int> lengths;
};

/**
 * General purpose tokenizer. We give it a list of token types.
 * Token types are functions which parse the head of a string for a particular
 * pattern.
 *
 * This tokenizer will maintain line numbers in the tokens and set their buffer
 * pointer properties.
 */
class Tokenizer : public ITokenizer {
public:
	typedef char State;

	enum TokenTypeFlags {
		NONE = 0,
		SKIP = 1, ///< Skip this token type as though it weren't there.
	};

	// Constructor.
	Tokenizer() : buffer(nullptr) {}
	~Tokenizer();

	/**
	 * Add a token type. The tokenizer now has ownership of the object and will
	 * delete it upon closing.
	 */
	void AddTokenType(std::string name, TokenType *type, TokenTypeFlags flags = NONE)
	{
		AddTokenType(0, 0, name, type, flags);
	}

	void AddTokenType(State state, State nextState, std::string name, TokenType *type, TokenTypeFlags flags = NONE)
	{
		tokenTypeInfo.push_back({ type, name, flags, state, nextState });
	}

	void GetTokenTypeName(int index);

	// Implement ITokenizer.
	void SetData(const char *str, int length);
	bool GetToken(Token *token);

private:
	struct TokenTypeInfo {
		TokenType *type;
		std::string name;
		TokenTypeFlags flags;
		State state;
		State nextState;
	};

	// The buffer we're tokenizing.
	const char *buffer;
	int bufferLength;

	// List of token types.
	std::vector<TokenTypeInfo> tokenTypeInfo;

	// Working data.
	State state;
	int numLines;
	int column;
	int currentPosition;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
