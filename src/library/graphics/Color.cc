/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "library/graphics/Color.hh"
#include <cmath>
#include <cstring>
#include <cctype>
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// Gamma table for colour adjustment.
class GammaTable {
private:
	static constexpr float GAMMA_DEFAULT = 2.2f;
	static constexpr int GAMMA_TABLE_SIZE = 256;
	static constexpr float GAMMA_TABLE_MAXVAL = GAMMA_TABLE_SIZE - 1.0f;

public:
	GammaTable() { SetGamma(GAMMA_DEFAULT); }
	GammaTable(float g) { SetGamma(g); }
	void SetGamma(float g)
	{
		float ig = 1.0f / g;
		for (int i = 0; i < GAMMA_TABLE_SIZE; ++i)
		{
			table[i] = pow((float)i / GAMMA_TABLE_MAXVAL, g);
			invTable[i] = pow((float)i / GAMMA_TABLE_MAXVAL, ig);
		}
	}
	float Lookup(float f)
	{
		int v = (int)(f * GAMMA_TABLE_MAXVAL) % GAMMA_TABLE_SIZE;
		return table[v];
	}
	float LookupInverse(float f)
	{
		int v = (int)(f * GAMMA_TABLE_MAXVAL) % GAMMA_TABLE_SIZE;
		return invTable[v];
	}
private:
	float table[GAMMA_TABLE_SIZE];
	float invTable[GAMMA_TABLE_SIZE];
};

GammaTable gGamma;

// Requires the digit to definitely be hex.
inline int GetDigitValue(char c)
{
	c = tolower(c);
	if (isdigit(c))
		return c - '0';
	else return 10 + (c - 'a');
}

// Decode hex or decimal digits to float components.
// Requires that the string is already checked to be hex or decimal.
// c is the string.
// components are the number of colour components to decode.
// componentDigits are the number of digits per component.
// base is the number of possible values per digit.
inline void DecodeHex(const char *str, int components, int componentDigits, int base, float *result)
{
	int mv = base;
	for (int i = 1; i < componentDigits; ++i) { mv *= base; }
	float scale = 1.0f / (float)(mv - 1);

	for (int i = 0; i < components; ++i)
	{
		int value = 0;
		for (int j = 0; j < componentDigits; ++j)
		{
			value *= base;
			value += GetDigitValue(str[i*componentDigits + j]);
		}
		result[i] = (float)value * scale;
	}
}

void Color::Set(const char *str, int length)
{
	// Prefix:
	// 'textures/h/' or '#' or 'h' - hex
	// 'textures/d/' or 'd' - decimal
	// Support 3, 4, 6 or 7 digits following.

	if (length != -1)
	{
		// If a length is supplied the string may be unterminated, but we check
		// for a terminator sooner than the maximum possible length allowed.
		int maxLength = length;
		for (length = 0; length < maxLength; ++length)
		{
			if (str[length] == '\0')
				break;
		}
	}
	else
	{
		length = strlen(str);
	}

	if (length < 1)
	{
		Set(0, 0, 0, 0);
		return;
	}

	int start = -1;
	bool hex = false;

	if (str[0] == '#' || str[0] == 'h')
	{
		hex = true;
		start = 1;
	}
	else if (str[0] == 'd')
	{
		hex = false;
		start = 1;
	}
	else if (length > 11 && !strncmp(str, "textures/h/", 11))
	{
		hex = true;
		start = 11;
	}
	else if (length > 11 && !strncmp(str, "textures/d/", 11))
	{
		hex = false;
		start = 11;
	}

	int digits = length - start;
	if (digits != 3 && digits != 4 && digits != 6 && digits != 8)
	{
		Set(0, 0, 0, 0);
		std::cout << "could not parse " << str << " - invalid length " << digits << std::endl;
		return;
	}
	if (start < 0)
	{
		Set(0, 0, 0, 0);
		std::cout << "could not parse " << str << " - could not determine format" << std::endl;
		return;
	}

	if (hex)
	{
		for (int i = start; i < length; ++i)
		{
			if (!isxdigit(str[i]))
			{
				Set(0, 0, 0, 0);
				std::cout << "could not parse " << str << " - not hexadecimal" << std::endl;
				return;
			}
		}
	}
	else
	{
		for (int i = start; i < length; ++i)
		{
			if (!isdigit(str[i]))
			{
				std::cout << "could not parse " << str << " - not decimal" << std::endl;
				Set(0, 0, 0, 0);
				return;
			}
		}
	}

	int base = hex ? 16 : 10;
	int componentDigits = (digits == 6 || digits == 8) ? 2 : 1;
	int components = (digits == 8 || digits == 4) ? 4 : 3;

	DecodeHex(&str[start], components, componentDigits, base, rgba);

	//std::cout << "parsed " << str << " as " << rgba[0] << "," << rgba[1] << ","
	//	<< rgba[2] << "," << rgba[3] << std::endl;
}

void Color::Set(int rgb)
{
	rgba[0] = (float)((rgb & 0x00ff0000) >> 16) / 255.0f;
	rgba[1] = (float)((rgb & 0x0000ff00) >>  8) / 255.0f;
	rgba[2] = (float) (rgb & 0x000000ff)        / 255.0f;
	rgba[3] = 1.0f;
}

void Color::Set(float r, float g, float b, float a)
{
	rgba[0] = r;
	rgba[1] = g;
	rgba[2] = b;
	rgba[3] = a;
}

void Color::SetArgb(uint32_t argb)
{
	rgba[0] = (float)((argb & 0x00ff0000) >> 16) / 255.0f;
	rgba[1] = (float)((argb & 0x0000ff00) >>  8) / 255.0f;
	rgba[2] = (float) (argb & 0x000000ff)        / 255.0f;
	rgba[3] = (float)((argb & 0xff000000) >> 24) / 255.0f;
}

uint32_t Color::GetArgb()
{
	return (((int)(rgba[3] * 255.0f) & 0xff) << 24)
		+ (((int)(rgba[0] * 255.0f) & 0xff) << 16)
		+ (((int)(rgba[1] * 255.0f) & 0xff) << 8)
		+ ((int)(rgba[2] * 255.0f) & 0xff);
}

uint32_t Color::GetRgb()
{
	return 0xff000000
		+ (((int)(rgba[0] * 255.0f) & 0xff) << 16)
		+ (((int)(rgba[1] * 255.0f) & 0xff) << 8)
		+ (((int)(rgba[2] * 255.0f) & 0xff));
}

void Color::Blend(const Color &a, float fracA, const Color &b)
{
	float iFrac = 1.0f - fracA;
	for (int component = 0; component < 4; ++component)
	{
		rgba[component] = gGamma.LookupInverse(
			  gGamma.Lookup(a.rgba[component]) * fracA
			+ gGamma.Lookup(b.rgba[component]) * iFrac
			);
	}
}

void Color::Blend(float fracA, const Color &b)
{
	Blend(*this, fracA, b);
}

float Color::Luminance() const
{
	return gGamma.LookupInverse((gGamma.Lookup(rgba[0]) + gGamma.Lookup(rgba[1]) + gGamma.Lookup(rgba[2])) / 3.0f);
}

bool Color::operator==(const Color &other) const
{
	for (int i = 0; i < 4; ++i)
	{
		if (other.rgba[0] != rgba[0])
			return false;
	}
	return true;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

