/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <cassert>
#include <cinttypes>
#include <string>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// TODO: New methods:
// Access specific pixels given coordinates, with optional clamping, wrapping,
// bordering.

class Image {
public:
	typedef enum {
		PixelFormat_Unknown = 0,
		PixelFormat_Grey,
		PixelFormat_GreyAlpha,
		PixelFormat_RGB,
		PixelFormat_RGBA
	} PixelFormat;

	Image();
	~Image();
	void Init();

	// TODO: Should these perform clone, or should the clone method be forced
	// in order to remind that a copy of the data is being made?
	Image(const Image&); // NO IMPLEMENTATION - PREVENT COPY
	Image& operator=(const Image&); // NO IMPLEMENTATION - PREVENT COPY

	void Copy(const Image &other);

	void Set(int width, int height, uint8_t *data, int bpp);
	void Load(std::string filename);
	void Load(const unsigned char *memory, int length);
	void Unload();
	void Clear(int x, int y, int bpp, const uint8_t* initVal);
	void Clear(int x, int y, int bpp);

	float Gamma() { return gamma; }
	void SetGamma(float g) { gamma = g; }

	// If not loaded, w, h, bpp and data are all zero.
	int Width() { return width; }
	int Height() { return height; }
	PixelFormat Format()
	{
		switch (bpp)
		{
			case 1: return PixelFormat_Grey;
			case 2: return PixelFormat_GreyAlpha;
			case 3: return PixelFormat_RGB;
			case 4: return PixelFormat_RGBA;
		}
		assert(false);
	}
	int Bpp() { return bpp; }

	// Allowing editing of the data but not reallocation.
	uint8_t *Data() { return data; }
	// Const access to the data.
	const uint8_t *Data() const { return data; }

	// Resampling
	void Resample(const Image &other, int width, int height);
	void DownsampleInPlace();

	void Pad(int width, int height);

private:
	typedef enum {
		Data_External,
		Data_Created,
		Data_Stbi
	} DataState;

	float gamma;
	int width, height, bpp;
	DataState dataState;
	uint8_t *data;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
