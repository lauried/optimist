/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <cinttypes>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Represents a colour and performs various colour related translations and
 * so on, with gamma correction.
 * @todo Move to framework, except that the string parsing is game-specific.
 */
class Color {
public:
	float rgba[4];

	Color()
	{
		rgba[0] = 0.0f;
		rgba[1] = 0.0f;
		rgba[2] = 0.0f;
		rgba[3] = 0.0f;
	}

	Color(const char *str, int length = -1)
	{
		Set(str, length);
	}

	Color(float r, float g, float b, float a = 1.0f)
	{
		Set(r, g, b, a);
	}

	/**
	 * Sets from a string in a variety of supported formats.
	 * @param str The string to set from.
	 * @param length If the string is unterminated, this is the length of the
	 *               string, otherwise it's -1.
	 */
	void Set(const char *str, int length = -1);

	/**
	 * Set from a 24 bit integer, bits 16 to 23 red, bits 8 to 15 green,
	 * bits 0 to 7 blue.
	 */
	void Set(int rgb);

	/**
	 * Set from a 32 bit integer, bits 24 to 31 alpha, then as rgb in set.
	 */
	void SetArgb(uint32_t argb);

	/**
	 * Set from components.
	 */
	void Set(float r, float g, float b, float a = 1.0f);

	/**
	 * Get as argb integer.
	 */
	uint32_t GetArgb();

	/**
	 * Get as rgb integer.
	 */
	uint32_t GetRgb();

	/**
	 * Set as a blend of two other colours, gamma corrected.
	 * @param a The first colour.
	 * @param fracA The fraction of the first colour to lend.
	 * @param b The second colour. This will be blended in the amount 1 - fracA.
	 */
	void Blend(const Color &a, float fracA, const Color &b);

	/**
	 * Blend another colour with this one, gamma corrected.
	 * @param fracA The amount to blend this colour.
	 * @param b The other colour to blend, blended in the amount 1 - fracA.
	 */
	void Blend(float fracA, const Color &b);

	/**
	 * Returns the average of red green and blue channels, gamma corrected.
	 */
	float Luminance() const;

	float &operator[](int i) { return rgba[i]; }
	const float &operator[](int i) const { return rgba[i]; }

	bool operator==(const Color &other) const;
	bool operator!=(const Color &other) const { return !(*this == other); }

private:
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
