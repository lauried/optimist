/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "library/graphics/Image.hh"
#include <iostream>
#include <cstring>
#include <stdexcept>
#include <vector>
#define STBI_HEADER_FILE_ONLY 1
#include "library/extern/stb_image.cc"
#undef STBI_HEADER_FILE_ONLY
#include "library/maths/Maths.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

Image::Image()
{
	Init();
}

Image::~Image()
{
	Unload();
}

void Image::Init()
{
	gamma = 2.2;
	width = 0;
	height = 0;
	bpp = 0;
	dataState = Data_External;
	data = 0;
}

//-----------------------------------------------------------------------------
// Load / Unload / Clear
//-----------------------------------------------------------------------------

void Image::Set(int width, int height, uint8_t *data, int bpp)
{
	Unload();
	this->width = width;
	this->height = height;
	this->data = data;
	this->bpp = bpp;
	dataState = Data_External;
}

void Image::Load(std::string filename)
{
	Unload();
	dataState = Data_Stbi;
	data = stbi_load(filename.c_str(), &width, &height, &bpp, 0);
	if (!data)
	{
		std::cout << "Image load failed: " << stbi_failure_reason() << std::endl;
		width  = 0;
		height = 0;
		bpp    = 0;
	}
}

void Image::Load(const unsigned char *memory, int length)
{
	Unload();
	dataState = Data_Stbi;
	data = stbi_load_from_memory(memory, length, &width, &height, &bpp, 0);
	if (!data)
	{
		std::cout << "Image load from memory failed: " << stbi_failure_reason() << std::endl;
		width  = 0;
		height = 0;
		bpp    = 0;
	}
}

void Image::Unload()
{
	if (data)
	{
		switch (dataState)
		{
		case Data_Created:
			delete[] data;
			break;
		case Data_Stbi:
			stbi_image_free((void*)data);
			break;
		}
		width  = 0;
		height = 0;
		bpp    = 0;
		data   = 0;
		dataState = Data_External;
	}
}

void Image::Clear(int width, int height, int bpp, const uint8_t* initVal)
{
	Unload();
	this->width = width;
	this->height = height;
	this->bpp = bpp;
	dataState = Data_Created;
	data = new uint8_t[width * height * bpp];
	for (int i = 0; i < width * height * bpp; i += 1)
	{
		data[i] = initVal[i % bpp];
	}
}

void Image::Clear(int width, int height, int bpp)
{
	std::vector<uint8_t> initVal;
	initVal.resize(bpp, 0);
	Clear(width, height, bpp, &initVal[0]);
}

//-----------------------------------------------------------------------------
// Copy
//-----------------------------------------------------------------------------

void Image::Copy(const Image &other)
{
	Unload();
	gamma = other.gamma;
	width = other.width;
	height = other.height;
	bpp = other.bpp;
	dataState = Data_Created;
	data = new uint8_t[width * height * bpp];
	memcpy(data, other.data, width * height * bpp);
}

//-----------------------------------------------------------------------------
// Resampling
//-----------------------------------------------------------------------------

static void ResampleLineByHalf(const uint8_t *source, uint8_t *dest, int destLength, int pixelSize, int pixelStep)
{
	for (int dp = 0, sp1 = 0, sp2 = pixelStep;
	     dp < destLength;
	     dp += pixelStep, sp1 += pixelStep * 2, sp2 += pixelStep * 2)
	{
		for (int j = 0; j < pixelSize; ++j)
		{
			dest[dp+j] = (source[sp1+j] >> 1) + (source[sp2+j] >> 1);
		}
	}
}

static void ResampleLineLengthen(const uint8_t *source, int sourceLength, uint8_t *dest, int destLength, int pixelSize, int pixelStep)
{
	// TODO
}

static void ResampleLineShorten(const uint8_t *source, int sourceLength, uint8_t *dest, int destLength, int pixelSize, int pixelStep)
{
	// TODO
}

void Image::Resample(const Image &other, int width, int height)
{
	Unload();
	gamma = other.gamma;
	this->width = width;
	this->height = height;
	bpp = other.bpp;
	dataState = Data_Created;
	data = new uint8_t[width * height * bpp];

	// TODO
}

void Image::DownsampleInPlace()
{
	if (!Maths::IsPowerOfTwo(width))
		return;
	if (!Maths::IsPowerOfTwo(height))
		return;
	if (width == 1 && height == 1)
		return;

	if (width == 1 || height == 1)
	{
		int length = (height == 1) ? width : height;

		length >>= 1;
		ResampleLineByHalf(data, data, length, bpp, bpp);

		width >>= 1;
		height >>= 1;
		width = (width) ? width : 1;
		height = (height) ? height : 1;
	}
	else
	{
		int newHeight = height >> 1;
		int newWidth = width >> 1;
		for (int x = 0; x < width; ++x)
		{
			uint8_t *p = &data[x * bpp];
			ResampleLineByHalf(p, p, newHeight, bpp, width * bpp);
		}
		for (int y = 0; y < newHeight; ++y)
		{
			uint8_t *p = &data[y * width * bpp];
			ResampleLineByHalf(p, p, newWidth, bpp, bpp);
		}

		width = newWidth;
		height = newHeight;
	}
}

void Image::Pad(int newWidth, int newHeight)
{
	if (width < newWidth || height < newHeight)
	{
		throw std::out_of_range("Image::Pad - attempting to pad an image to smaller dimensions than the original image");
	}
	if (width == newWidth && height == newHeight)
	{
		return;
	}

	uint8_t *oldData = data;
	data = new uint8_t[width * height * bpp];
	uint8_t *src = oldData;
	uint8_t *dest = data;

	for (int y = 0; y < newHeight; y += 1)
	{
		if (y < height)
		{
			int x;
			for (x = 0; x < width; x += 1)
			{
				for (int c = 0; c < bpp; c += 1)
				{
					*(dest++) = *(src++);
				}
			}
			for (; x < newWidth; x += 1)
			{
				for (int c = 0; c < bpp; c += 1)
				{
					*(dest++) = 0;
				}
			}
		}
		else
		{
			for (int x = 0; x < newWidth; x += 1)
			{
				for (int c = 0; c < bpp; c += 1)
				{
					*(dest++) = 0;
				}
			}
		}
	}

	switch (dataState)
	{
	case Data_Created:
		delete[] data;
		break;
	case Data_Stbi:
		stbi_image_free((void*)data);
		break;
	}
	dataState = Data_Created;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
