/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <vector>
#include "library/geometry/Vector3.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Represents intermediate or final calculated voxel data.
 * From this, physics and rendering geometry can be generated.
 */
class VoxelData {
public:
	typedef uint16_t VoxelBlockType;

	static const VoxelBlockType BLOCK_NULL = 0;
	static const VoxelBlockType BLOCK_TRANSPARENT = 0x8000;

	inline float BlockScale() const
	{
		return blockScale;
	}

	inline const Vector3I &Origin() const
	{
		return origin;
	}

	inline const Vector3I &Size() const
	{
		return size;
	}

	inline std::vector<VoxelBlockType> &Blocks()
	{
		return blocks;
	}

	inline const std::vector<VoxelBlockType> &Blocks() const
	{
		return blocks;
	}

private:
	float blockScale;
	Vector3I origin;
	Vector3I size;
	std::vector<VoxelBlockType> blocks;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

