/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <string>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class VoxelBlockData {
public:
	typedef uint16_t BlockType;
	typedef uint32_t DoubleBlockType;
	static const int BLOCK_TYPE_BITS = 16;

	static const BlockType BLOCK_NULL = 0;
	static const BlockType BLOCK_TRANSPARENT = 0x8000;

	std::string &TextureForBlockType(BlockType type, int side);

private:
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

