/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "library/geometry/Vector3.hh"
#include "voxel/VoxelData.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * This interface is to provide final voxel data to consumers, in a way that's
 * interchangeable.
 */
class IVoxelSource {
public:
	virtual ~IVoxelSource();

	/**
	 * Returns the size of blocks that the source returns.
	 */
	virtual Vector3I BlockSize() = 0;

	/**
	 * Return empty data if the shape is not valid or if the region is empty.
	 * Returning empty data simplifies user code, because the voxeldata object
	 * can still be passed around and treated through a single path.
	 */
	virtual VoxelData *GetData(Vector3I minsBlockCoords) = 0;

	/**
	 * Free data which was requested from this source.
	 */
	virtual void FreeData(VoxelData *data) = 0;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
