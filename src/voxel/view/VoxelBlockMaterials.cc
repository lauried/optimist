/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "voxel/view/VoxelBlockMaterials.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

Material& VoxelBlockMaterials::MaterialForBlockType(VoxelBlockData *data, VoxelBlockData::BlockType blockType, int side)
{
	return MaterialForTextureName(data->TextureForBlockType(blockType, side));
}

Material& VoxelBlockMaterials::MaterialForTextureName(std::string name)
{
	auto it = materials.find(name);
	if (it == materials.end())
	{
		Material material;
		SetupMaterial(material, name);
		it = materials.insert(std::make_pair(name, material)).first;
	}
	return it->second;
}

void VoxelBlockMaterials::SetupMaterial(Material &material, std::string name)
{
	material.texInfo.info = &TextureInfoForName(name);
}

TextureInfo& VoxelBlockMaterials::TextureInfoForName(std::string name)
{
	auto it = textureInfos.find(name);
	if (it == textureInfos.end())
	{
		TextureInfo info;
		info.resource = resources->GetResource<ImageResource>(name);
		it = textureInfos.insert(std::make_pair(name, info)).first;
	}
	return it->second;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
