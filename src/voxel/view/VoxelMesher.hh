/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <functional>
#include "voxel/VoxelData.hh"
#include "voxel/view/VoxelBlockMaterials.hh"
#include "voxel/view/VoxelChunkModel.hh"
#include "renderer/Mesh.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * VoxelMesher builds VoxelChunkModel objects from VoxelData.
 */
class VoxelMesher {
public:
	typedef std::function<Mesh*()> MeshAllocator;

	VoxelMesher(VoxelBlockMaterials *materials, MeshAllocator meshAllocator)
		: materials(materials)
		, meshAllocator(meshAllocator)
		{}

	void BuildChunkModel(VoxelData *chunk, VoxelData *neighbours[6], VoxelChunkModel *model);

private:
	VoxelBlockMaterials *materials;
	MeshAllocator meshAllocator;

	void VoxelDataToMeshesChunk(VoxelData *chunk, int axis, VoxelChunkModel *model);
	void VoxelDataToMeshesNeighbourBoundary(VoxelData *chunk, VoxelData *neighbour, int axis, int direction, VoxelChunkModel *model);
	void GetPlaneVisibleBlocks(
		VoxelBlockData::BlockType *plane1,
		VoxelBlockData::BlockType *plane2,
		VoxelBlockData::BlockType *result1,
		VoxelBlockData::BlockType *result2,
		size_t sizeMinor,
		size_t stepMinor,
		size_t sizeMajor,
		size_t stepMajor
	);
	void BlocksToMeshes(
		size_t sizeMinor,
		size_t sizeMajor,
		VoxelBlockData::BlockType *data,
		VoxelBlockData::BlockType *workingBuffer,
		int side,
		size_t plane,
		VoxelChunkModel *model
	);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

