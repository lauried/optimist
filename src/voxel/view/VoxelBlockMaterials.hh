/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "renderer/Material.hh"
#include <unordered_map>
#include "voxel/VoxelBlockData.hh"
#include "resources/ResourceManager.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Returns materials for voxel block types.
 */
class VoxelBlockMaterials {
public:
	VoxelBlockMaterials(ResourceManager *resources)
		: resources(resources)
		{}

	Material &MaterialForBlockType(VoxelBlockData *data, VoxelBlockData::BlockType blockType, int side);
	Material &MaterialForTextureName(std::string name);

private:
	ResourceManager *resources;
	std::unordered_map<std::string, Material> materials;
	std::unordered_map<std::string, TextureInfo> textureInfos;

	void SetupMaterial(Material &material, std::string name);
	TextureInfo &TextureInfoForName(std::string name);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

