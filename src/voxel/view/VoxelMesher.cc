/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "voxel/view/VoxelMesher.hh"
#include <algorithm>
#include <stdexcept>
#include <cstring>
#include <unordered_map>
#include "library/geometry/Vector3.hh"
#include "voxel/VoxelBlockData.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// NOTES:
// Axis: 0,1,2.
// Direction: (relates to axis - 0 for positive and 1 for negative).
// Side: 0,1,2 are +x, +y and +z. 3,4,5 are the negative versions. side = axis + direction * 3

// TODO:
// - Review Material and MeshInstance.
//   * Material is essentially a pointer passed to the rendercore, so it's
//     never owned by the renderer. It's only safe to copy if the copies are
//     read-only and aren't held on to.
//   * It has a pointer to a TextureInfo.
//   * It has comparison operators which are intended to determine whether
//     materials would require a renderer state change.
//     This might be better left to the rendercore itself.

// Blah blah.
// Reorganise the actual class structure to be more like dependency inversion,
// with dependency-less UDTs and dependency-full mostly-singleton data
// processors.
// Except that things like mesh providers represent instances of data that can
// also process extra data.

// TODO:
// - Minecraft voxel source.
// - DONE Some implementation for VoxelBlockData.
//   For now this can just fall back to some default texture.
// - DONE Materials object sets up the material for a given blocktype and side, wrapping the block data.
// - DONE Multimeshbuilder lets us push data onto meshes repeatedly.
// * A mesher instance is created for each model we build, and it holds the
//   temporary data.
// - DONE Mesh object with the MeshData interface, self-building like MultiMeshBuilder
//   and ownership of its data. We're sort of rebuilding MeshBuilder but with
//   a more flexible interface supporting different channel data types.
// - Model uses mesher to construct itself, providing an allocator and taking ownership of the meshes/instances once the mesher is done.
// - Cache of chunk models that the mesh provider uses.
// - Clipmap which wraps the cache.
// - Single mesh provider which would wrap a voxel source and the clipmap.
// - Gamecode engine objects for the voxel source and voxel scenegraph object.

// Note: Some things could do with a review, but not till later:
// * Renderer, and a more generic mesh interface.
// * How we handle transparency, both in the voxel model and the renderer.
// * A more flexible method for specifying the channels for a mesh when constructing one.
// * How to make the renderer features more abstract, so that we can just give it mesh and material data.
//   This would ultimately involve some layer between sending meshes and the rendercore abstraction.

/**
 * Turns an array of visible blocks into meshes.
 * @todo Mesh output target.
 */
void VoxelMesher::BlocksToMeshes(
	size_t sizeMinor,
	size_t sizeMajor,
	VoxelBlockData::BlockType *data,
	VoxelBlockData::BlockType *workingBuffer,
	int side,
	size_t plane,
	VoxelChunkModel *model
) {
	size_t size = sizeMinor * sizeMajor;
	std::memcpy(workingBuffer, data, size * sizeof(VoxelBlockData::BlockType));
	std::sort(workingBuffer, workingBuffer + size);

	VoxelBlockData::BlockType prev = VoxelBlockData::BLOCK_NULL;
	for (size_t typeIndex = 0; typeIndex < size; typeIndex += 1, workingBuffer += 1)
	{
		if (*workingBuffer == prev)
		{
			continue;
		}
		// TODO: Get mesh target for block type.
		for (size_t dataMajor = 0; dataMajor< sizeMajor; dataMajor += 1)
		{
			for (size_t dataMinor = 0; dataMinor< sizeMinor; dataMinor += 1, data += 1)
			{
				if (*data != *workingBuffer)
				{
					continue;
				}
				// TODO: Output block type to mesh target.
			}
		}
	}
}

/**
 * Compares two planes of a chunk (or a chunk and its neighbour) and places
 * visible blocks of plane1 into result1, and visible blocks of plane2 into
 * result2.
 * A block is visible if its neighbour is transparent and a different value.
 */
void VoxelMesher::GetPlaneVisibleBlocks(
	VoxelBlockData::BlockType *plane1,
	VoxelBlockData::BlockType *plane2,
	VoxelBlockData::BlockType *result1,
	VoxelBlockData::BlockType *result2,
	size_t sizeMinor,
	size_t stepMinor,
	size_t sizeMajor,
	size_t stepMajor
) {
	VoxelBlockData::BlockType *resultPtr1 = result1;
	VoxelBlockData::BlockType *resultPtr2 = result2;
	for (size_t y = 0; y < sizeMajor; y += 1)
	{
		size_t offset = y * stepMajor;
		VoxelBlockData::BlockType *planePtr1 = &plane1[offset];
		VoxelBlockData::BlockType *planePtr2 = &plane2[offset];

		for (size_t x = 0; x < sizeMinor; x += 1)
		{
			planePtr1 += 1;
			planePtr2 += 1;
			resultPtr1 += 1;
			resultPtr2 += 1;

			if (*planePtr1 & VoxelBlockData::BLOCK_TRANSPARENT && *planePtr1 != *planePtr2)
			{
				*resultPtr2 = *planePtr2;
			}
			else
			{
				*resultPtr1 = VoxelBlockData::BLOCK_NULL;
			}
			if (*planePtr2 & VoxelBlockData::BLOCK_TRANSPARENT && *planePtr2 != *planePtr1)
			{
				*resultPtr1 = *planePtr1;
			}
			else
			{
				*resultPtr2 = VoxelBlockData::BLOCK_NULL;
			}
		}
	}
}

void VoxelMesher::VoxelDataToMeshesNeighbourBoundary(VoxelData *chunk, VoxelData *neighbour, int axis, int direction, VoxelChunkModel *model)
{
    Vector3I size = chunk->Size();
    size_t stepMinor, stepMajor, sizeMinor, sizeMajor, axisOffset;

    switch (axis)
    {
	case 0:
		// X - iterate Y and Z
		sizeMinor = size[1];
		sizeMajor = size[2];
		stepMinor = size[0];
		stepMajor = size[0] * size[1];
		axisOffset = (size[0] - 1);
		break;
	case 1:
		// Y - iterate X and Z
		sizeMinor = size[0];
		sizeMajor = size[2];
		stepMinor = 1;
		stepMajor = size[0] * size[1];
		axisOffset = size[0] * (size[1] - 1);
		break;
	case 2:
		// Z - iterate X and Y
		sizeMinor = size[0];
		sizeMajor = size[1];
		stepMinor = 1;
		stepMajor = size[0];
		axisOffset = size[0] * size[1] * (size[2] - 1);
		break;
	default:
		throw std::invalid_argument("Invalid value for axis");
    }

    size_t chunkOffset = 0, neighbourOffset = 0;
    switch (direction)
    {
	case 0:
		neighbourOffset = axisOffset;
		break;
	case 1:
		chunkOffset = axisOffset;
		break;
	default:
		throw std::invalid_argument("Invalid value for direction");
    }

    size_t planeSize = sizeMinor * sizeMajor;
    std::vector<VoxelData::VoxelBlockType> result;
    result.resize(planeSize * 2);

    GetPlaneVisibleBlocks(
		&(chunk->Blocks()[chunkOffset]),
		&(neighbour->Blocks()[neighbourOffset]),
		&result[0],
		&result[planeSize],
		sizeMinor,
		stepMinor,
		sizeMajor,
		stepMajor
	);

	BlocksToMeshes(
		sizeMinor,
		sizeMajor,
		&result[0],
		&result[planeSize], // reuse neighbouring chunk's result as the working buffer.
		axis + (direction * 3),
		direction ? 0 : size[axis] - 1,
		model
	);
}

void VoxelMesher::VoxelDataToMeshesChunk(VoxelData *chunk, int axis, VoxelChunkModel *model)
{
	Vector3I size = chunk->Size();
	size_t sizeMinor, sizeMajor, stepMinor, stepMajor, stepAxis, planeSize;
	switch (axis)
	{
	case 0:
		// X - iterate Y and Z
		sizeMinor = size[1];
		sizeMajor = size[2];
		stepMinor = size[0];
		stepMajor = size[0] * size[1];
		stepAxis = 1;
		planeSize = size[1] * size[2];
		break;
	case 1:
		// Y - iterate X and Z
		sizeMinor = size[0];
		sizeMajor = size[2];
		stepMinor = 1;
		stepMajor = size[0] * size[1];
		stepAxis = size[0];
		planeSize = size[0] * size[2];
		break;
	case 2:
		// Z - iterate X and Y
		sizeMinor = size[0];
		sizeMajor = size[1];
		stepMinor = 1;
		stepMajor = size[0];
		stepAxis = size[0] * size[1];
		planeSize = size[0] * size[1];
		break;
	}
	size_t sizeAxis = size[axis];
	std::vector<VoxelBlockData::BlockType> result;
	result.resize(planeSize * 3);
	VoxelBlockData::BlockType *result1 = &result[0];
	VoxelBlockData::BlockType *result2 = &result[planeSize];
	VoxelBlockData::BlockType *workingBuffer = &result[planeSize * 2];
	VoxelBlockData::BlockType *plane1 = &(chunk->Blocks()[0]);
	VoxelBlockData::BlockType *plane2 = plane1 + stepAxis;
	for (size_t plane = 0; plane < size[axis] - 1; plane += 1, plane1 += stepAxis, plane2 += stepAxis)
	{
		GetPlaneVisibleBlocks(plane1, plane2, result1, result2, sizeMinor, stepMinor, sizeMajor, stepMajor);

		BlocksToMeshes(
			sizeMinor,
			sizeMajor,
			result1,
			workingBuffer,
			axis + 3,
			plane,
			model
		);

		BlocksToMeshes(
			sizeMinor,
			sizeMajor,
			result2,
			workingBuffer,
			axis,
			plane,
			model
		);
	}
}

void VoxelMesher::BuildChunkModel(VoxelData *chunk, VoxelData *neighbours[6], VoxelChunkModel *model)
{
	for (int i = 0; i < 6; i += 1)
	{
		if (neighbours[i] != nullptr)
		{
			int axis = i % 3;
			int plane = i > 2 ? 1 : 0;
			VoxelDataToMeshesNeighbourBoundary(chunk, neighbours[i], axis, plane, model);
		}
	}
	VoxelDataToMeshesChunk(chunk, 0, model);
	VoxelDataToMeshesChunk(chunk, 1, model);
	VoxelDataToMeshesChunk(chunk, 2, model);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
