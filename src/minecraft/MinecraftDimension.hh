/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "minecraft/MinecraftRegionFile.hh"
#include "minecraft/NbtFile.hh"
#include "minecraft/MinecraftChunk.hh"
#include "filesystem/IFilesystem.hh"
#include "library/containers/RecentAccessCache.hh"
#include "library/geometry/Vector2.hh"
#include "log/ILogTarget.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class MinecraftDimension {
public:
	MinecraftDimension(IFilesystem *filesystem, ILogTarget *log);

	bool DecodeChunk(Vector2I chunkWorldCoords, std::vector<uint8_t> *data);

private:
	IFilesystem *filesystem;
	ILogTarget *log;
	RecentAccessCache<Vector2I, MinecraftRegionFile*> regions;

	MinecraftRegionFile* AllocateRegion(Vector2I coords);
	void FreeRegion(MinecraftRegionFile *region);
	float WeighRegion(MinecraftRegionFile *region);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

