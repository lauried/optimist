/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <vector>
#include <cstdint>
#include <stdexcept>
#include "filesystem/IFilesystem.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class region_no_chunk_error : public std::runtime_error {
public:
	region_no_chunk_error(const std::string& what_arg) : runtime_error(what_arg) {}
	region_no_chunk_error(const char* what_arg) : runtime_error(what_arg) {}
};

/**
 * Holds on to an open file and lets us read chunks out of it.
 */
class MinecraftRegionFile {
public:
	const size_t SECTOR_SIZE = 4096;
	const int NUM_CHUNKS = 1024;
	const int LOCATION_SIZE = 4;
	const int TIMESTAMP_SIZE = 4;

	void SetFile(IFile *file);

	// For closing the file
	IFile* GetFile() { return file; }

	/**
	 * Returns true if the region has a chunk at the given coordinates.
	 */
	bool HasChunk(int x, int y);

	/**
	 * Returns the timestamp for the chunk at the given coordinates.
	 */
	uint32_t GetChunkTimestamp(int x, int y);

	/**
	 * If a chunk is present at x, y, then resizes the given vector to the
	 * required size and loads it into the vector.
	 */
	void DecodeChunk(int x, int y, std::vector<uint8_t> *data);

private:
	IFile *file;
	std::vector<uint8_t> locations;
	std::vector<uint8_t> timestamps;

	inline size_t OffsetForCoordinates(int x, int z)
	{
		return ((x & 31) + (z & 31) << 5) << 2;
	}

	uint32_t GetChunkSectorOffset(int x, int z);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

