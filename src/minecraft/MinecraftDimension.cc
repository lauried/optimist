/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "minecraft/MinecraftDimension.hh"
#include <sstream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

MinecraftDimension::MinecraftDimension(IFilesystem *filesystem, ILogTarget *logTarget)
	: filesystem(filesystem)
	, log(log)
	, regions(
		std::bind(&MinecraftDimension::AllocateRegion, this, std::placeholders::_1),
		std::bind(&MinecraftDimension::FreeRegion, this, std::placeholders::_1),
		16,
		std::bind(&MinecraftDimension::WeighRegion, this, std::placeholders::_1)
	)
{
}

bool MinecraftDimension::DecodeChunk(Vector2I chunkWorldCoords, std::vector<uint8_t> *data)
{
	Vector2I regionCoords(chunkWorldCoords[0] >> 5, chunkWorldCoords[1] >> 5);
	MinecraftRegionFile *regionFile = regions.Get(regionCoords);
	if (regionFile == nullptr)
	{
		return false;
	}
	regionFile->DecodeChunk(chunkWorldCoords[0] & 0x1f, chunkWorldCoords[1] & 0x1f, data);
	return true;
}

MinecraftRegionFile* MinecraftDimension::AllocateRegion(Vector2I coords)
{
	std::ostringstream filenameStream;
	filenameStream << "r." << coords[0] << "." << coords[1] << ".mca";
	std::string filename = filenameStream.str();
	if (!filesystem->FileExists(filename))
	{
		return nullptr;
	}
	IFile *file = filesystem->Open(filename);
	if (file == nullptr || !file->IsOpen())
	{
		return nullptr;
	}
	MinecraftRegionFile *regionFile = new MinecraftRegionFile();
	regionFile->SetFile(file);
	return regionFile;
}

void MinecraftDimension::FreeRegion(MinecraftRegionFile *region)
{
	filesystem->Close(region->GetFile());
	delete region;
}

float MinecraftDimension::WeighRegion(MinecraftRegionFile *region)
{
	return (region == nullptr) ? 0.0f : 1.0f;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
