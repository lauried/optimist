/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "minecraft/NbtDumper.hh"
#include <iostream>
#include <limits>
#include <iomanip>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

const char *TagName(int tag)
{
	switch (tag)
	{
	case NbtFile::Tag_End:
		return "Tag_End";
	case NbtFile::Tag_Byte:
		return "Tag_Byte";
	case NbtFile::Tag_Short:
		return "Tag_Short";
	case NbtFile::Tag_Int:
		return "Tag_Int";
	case NbtFile::Tag_Long:
		return "Tag_Long";
	case NbtFile::Tag_Float:
		return "Tag_Float";
	case NbtFile::Tag_Double:
		return "Tag_Double";
	case NbtFile::Tag_Byte_Array:
		return "Tag_Byte_Array";
	case NbtFile::Tag_String:
		return "Tag_String";
	case NbtFile::Tag_List:
		return "Tag_List";
	case NbtFile::Tag_Compound:
		return "Tag_Compound";
	case NbtFile::Tag_Int_Array:
		return "Tag_Int_Array";
	}
	return "unknown";
}

void NbtDumper::DumpTag(NbtFile::NbtTag *tag, int level)
{
	std::string indent(level, '\t');

	switch (tag->Type())
	{
	case NbtFile::Tag_Compound:
		std::cout << "(compound)[" << std::endl;
		for (auto it = tag->ChildrenBegin(); it != tag->ChildrenEnd(); ++it)
		{
			DumpNamedTag(it->first, it->second, level + 1);
		}
		std::cout << indent << "]" << std::endl;
		break;
	case NbtFile::Tag_List:
		std::cout << "(list)(" << TagName(tag->ListType()) << ")[" << std::endl;
		if (tag->ListIsScalar())
		{
			for (int i = 0; i < tag->ListSize(); i += 1)
			{
				std::cout << indent << "\t";
				switch (tag->ListType())
				{
					case NbtFile::Tag_Byte:
						std::cout << tag->GetData()[i];
						break;
					case NbtFile::Tag_Short:
						std::cout << tag->GetInt16Data()[i];
						break;
					case NbtFile::Tag_Int:
						std::cout << tag->GetInt32Data()[i];
						break;
					case NbtFile::Tag_Long:
						std::cout << tag->GetInt64Data()[i];
						break;
					case NbtFile::Tag_Float:
						std::cout << tag->GetFloatData()[i];
						break;
					case NbtFile::Tag_Double:
						std::cout << tag->GetDoubleData()[i];
						break;
				}
				std::cout << std::endl;
			}
		}
		else
		{
			for (int i = 0; i < tag->NumValues(); i += 1)
			{
				std::cout << indent << '\t' << "Value: ";
				DumpTag(tag->Value(i), level + 1);
			}
		}
		std::cout << indent << "]" << std::endl;
		break;
	case NbtFile::Tag_Byte:
		std::cout << "(byte)" << tag->GetInt() << std::endl;
		return;
	case NbtFile::Tag_Short:
		std::cout << "(short)" << tag->GetInt() << std::endl;
		return;
	case NbtFile::Tag_Int:
		std::cout << "(int)" << tag->GetInt() << std::endl;
		return;
	case NbtFile::Tag_Long:
		std::cout << "(long)" << tag->GetInt() << std::endl;
		return;
	case NbtFile::Tag_Float:
		std::cout << "(float)" << std::setprecision(std::numeric_limits<float>::digits10) << tag->GetDouble() << std::endl;
		return;
	case NbtFile::Tag_Double:
		std::cout << "(double)" << std::setprecision(std::numeric_limits<double>::digits10) << tag->GetDouble() << std::endl;
		return;
	case NbtFile::Tag_String:
		std::cout << "(string)" << tag->GetString() << std::endl;
		return;
	case NbtFile::Tag_Byte_Array:
		std::cout << "(bytearray) [" << tag->Size() << " bytes]" << std::endl;
		return;
	case NbtFile::Tag_Int_Array:
		std::cout << "(intarray) [" << tag->Size() << " bytes]" << std::endl;
		return;
	default:
		std::cout << "*unknown*" << std::endl;
	}
}

void NbtDumper::DumpNamedTag(std::string name, NbtFile::NbtTag *tag, int level)
{
	std::string indent(level, '\t');
	std::cout << indent << "Name: “" << name << "”, Value: ";
	DumpTag(tag, level);
}

void NbtDumper::DumpFile(NbtFile &nbt)
{
	DumpTag(nbt.Root());
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
