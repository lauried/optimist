/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <string>
#include <stdexcept>
#include <vector>
#include <map>
#include "filesystem/IFilesystem.hh"
#include "log/ILogTarget.hh"
#include "library/stream/BinaryReader.hh"
#include "library/stream/BinaryWriter.hh"
#include "library/Exceptions.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class nbt_decode_error : public file_load_error {
public:
	nbt_decode_error(const std::string& what_arg) : file_load_error(what_arg) {}
	nbt_decode_error(const char* what_arg) : file_load_error(what_arg) {}
};

/**
 * Decodes an NBT file and provides some methods for traversing it.
 *
 * There are also methods for modifying and saving the NBT structure.
 * These methods don't copy or take ownership of any data that they're made to
 * point to. It is the responsibility of the users of this interface to ensure
 * that the modified NBT structure is not used after any memory it points to is
 * freed.
 */
class NbtFile {
public:
	enum TagType {
		Tag_End        = 0,
		Tag_Byte       = 1,
		Tag_Short      = 2,
		Tag_Int        = 3,
		Tag_Long       = 4,
		Tag_Float      = 5,
		Tag_Double     = 6,
		Tag_Byte_Array = 7,
		Tag_String     = 8,
		Tag_List       = 9,
		Tag_Compound   = 10,
		Tag_Int_Array  = 11,
		Tag_Long_Array = 12,
	};

	//class NbtNamedTag;

	class NbtTag {
	public:
		NbtTag(NbtFile *file)
			: file(file)
			{}

		void Load(int tagType, BinaryReader *reader);

		int Type() { return type; }
		/// Size of the data in bytes.
		size_t Size() { return size; }
		/// Number of elements in a scalar list.
		size_t ListSize() { return listSize; }
		int ListType() { return listType; }
		bool ListIsScalar();

		const uint8_t *GetData() { return data; }
		const uint16_t *GetUint16Data() { return (uint16_t*)data; }
		const uint32_t *GetUint32Data() { return (uint32_t*)data; }
		const uint64_t *GetUint64Data() { return (uint64_t*)data; }
		const int16_t *GetInt16Data() { return (int16_t*)data; }
		const int32_t *GetInt32Data() { return (int32_t*)data; }
		const int64_t *GetInt64Data() { return (int64_t*)data; }
		const float *GetFloatData() { return (float*)data; }
		const double *GetDoubleData() { return (double*)data; }
		std::string GetString();
		int64_t GetInt();
		double GetDouble();

		std::map<std::string, NbtTag*>::iterator ChildrenBegin() { return children.begin(); }
		std::map<std::string, NbtTag*>::iterator ChildrenEnd() { return children.end(); }
		size_t NumValues() { return values.size(); }
		NbtTag* Value(size_t i) { return values[i]; }

		NbtTag* FindChild(std::string name);

		NbtTag* SetType(int newType);
		NbtTag* Set(int64_t value);
		NbtTag* Set(double value);
		NbtTag* Set(char *value, int length);
		NbtTag* Set(uint8_t *value, int num); ///< for byte array and list of byte
		NbtTag* Set(uint16_t *value, int num); ///< for list of short
		NbtTag* Set(uint32_t *value, int num); ///< for int array and list of int
		NbtTag* Set(uint64_t *value, int num); ///< for long array and list of long
		NbtTag* Set(float *value, int num); ///< for list of float
		NbtTag* Set(double *value, int num); ///< for list of double

		NbtTag* Set(std::string name, NbtTag *value); ///< Adds to or updates compound
		NbtTag* Clear(std::string name); ///< Removes from compound
		NbtTag* Insert(size_t index, NbtTag *tag); ///< Inserts at index
		NbtTag* Append(NbtTag *tag); ///< Appends to list
		NbtTag* Clear(size_t index); ///< Removes index, all above it are moved down one

		void Save(BinaryWriter *writer);

		void Reset();

	private:
		NbtFile *file;
		int type;
		int listType;
		int size; // only the size of the data, not of the vectors, always in bytes.
		int listSize; // number of elements in a scalar list
		uint8_t *data;
		int64_t intData;
		double doubleData;
		std::map<std::string, NbtTag*> children; // compound
		std::vector<NbtTag*> values; // list (for non-scalar tag types)

		std::string ReadString(BinaryReader *reader);
		void WriteString(std::string str, BinaryWriter *writer);

		void LoadListData(BinaryReader *reader);
		void SaveListData(BinaryWriter *writer);
	};

	~NbtFile();

	/**
	 * Load a file into our data vector and process it.
	 */
	void Load(IFile *file);

	/**
	 * Save to a file.
	 */
	void Save(IFile *file);

	/**
	 * Access our data vector, in order to give it to other methods to fill it,
	 * for example to decompress into it.
	 */
	std::vector<uint8_t> &Data() { return data; }

	/**
	 * Looks for a tag using the slash separated path.
	 * If the root has a name it is ignored.
	 */
	NbtTag *GetTag(std::string path);

	NbtTag *Root() { return root; }

	NbtTag *CreateTag();
	void FreeTag(NbtTag *tag);

	/**
	 * Delete all owned tags not in the tree.
	 */
	void GarbageCollect();

private:
	std::vector<uint8_t> data;
	std::vector<NbtTag *> tags;
	NbtTag *root;

	/**
	 * Process the data in our data vector.
	 * Note that reprocessing the same data will mess up the endianness of
	 * int arrays. There's also no need to do so.
	 */
	void Process();
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
