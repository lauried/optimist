/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "filesystem/IFilesystem.hh"
#include <vector>
#include <unordered_map>
#include "log/ILogTarget.hh"
#include "minecraft/MinecraftDimension.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class MinecraftWorld {
public:
	MinecraftWorld(IFilesystem *filesystem, ILogTarget *log);
	~MinecraftWorld();

	bool HasDimension(std::string name);
	void GetDimensions(std::vector<std::string> *result);
	MinecraftDimension *GetDimension(std::string name);

private:
	IFilesystem *filesystem;
	ILogTarget *log;
	std::vector<IFilesystem*> wrapperFilesystems;
	std::unordered_map<std::string, MinecraftDimension *> dimensions;
	std::unordered_map<std::string, std::string> aliases;

	void SetupDimensions();
	void SetupDimension(std::string basePath);
	void SetupAliases(std::string basePath);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

