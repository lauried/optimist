/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "minecraft/MinecraftWorld.hh"
#include "filesystem/ChrootFilesystem.hh"
#include "library/text/Path.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

MinecraftWorld::MinecraftWorld(IFilesystem *filesystem, ILogTarget *log)
	: filesystem(filesystem)
	, log(log)
{
	SetupDimensions();
}

MinecraftWorld::~MinecraftWorld()
{
	for (auto it = dimensions.begin(); it != dimensions.end(); ++it)
	{
		delete it->second;
	}
	for (auto it = wrapperFilesystems.begin(); it != wrapperFilesystems.end(); ++it)
	{
		delete *it;
	}
}

bool MinecraftWorld::HasDimension(std::string name)
{
	return dimensions.find(name) != dimensions.end() || aliases.find(name) == aliases.end();
}

void MinecraftWorld::GetDimensions(std::vector<std::string> *result)
{
	for (auto it = dimensions.begin(); it != dimensions.end(); ++it)
	{
		result->push_back(it->first);
	}
	for (auto it = aliases.begin(); it != aliases.end(); ++it)
	{
		result->push_back(it->first);
	}
}

MinecraftDimension *MinecraftWorld::GetDimension(std::string name)
{
	auto itAlias = aliases.find(name);
	if (itAlias != aliases.end())
	{
		name = itAlias->second;
	}
	auto itDimension = dimensions.find(name);
	if (itDimension != dimensions.end())
	{
		return itDimension->second;
	}
	return nullptr;
}

void MinecraftWorld::SetupDimensions()
{
	std::string baseDir("");

	SetupDimension(baseDir);

	std::vector<std::string> files;
	filesystem->GetFilesInDirectory(baseDir, &files);
	for (auto it = files.begin(); it != files.end(); ++it)
	{
		std::string file = *it;
		if (!file.compare(0, 3, "DIM")) {
			SetupDimension(file);
		}
	}
}

void MinecraftWorld::SetupDimension(std::string basePath)
{
	std::string regionPath = Path::Join({ basePath, "region" });
	if (!filesystem->DirectoryExists(regionPath))
	{
		return;
	}
	ChrootFilesystem *dimensionFilesystem = new ChrootFilesystem(filesystem, regionPath);
	wrapperFilesystems.push_back(dimensionFilesystem);
	MinecraftDimension *dimension = new MinecraftDimension(dimensionFilesystem, log);
	dimensions.insert(std::make_pair(basePath, dimension));

	SetupAliases(basePath);
}

void MinecraftWorld::SetupAliases(std::string basePath)
{
	if (!basePath.compare(""))
	{
		aliases.insert(std::make_pair(std::string("overworld"), basePath));
	}
	if (!basePath.compare("DIM-1"))
	{
		aliases.insert(std::make_pair(std::string("nether"), basePath));
	}
	if (!basePath.compare("DIM1"))
	{
		aliases.insert(std::make_pair(std::string("end"), basePath));
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
