/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "minecraft/MinecraftChunkFactory.hh"
#include <stdexcept>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

MinecraftChunkFactory::ChunkData *MinecraftChunkFactory::GetChunk(std::string dimensionName, Vector2I coordinates)
{
	MinecraftDimension *dimension = world->GetDimension(dimensionName);
	if (dimension == nullptr)
	{
		return nullptr;
	}
	ChunkData *chunkData = new ChunkData();
	try
	{
		auto data = &(chunkData->nbt.Data());
		dimension->DecodeChunk(coordinates, data);
		chunkData->nbt.Process(log);
		chunkData->chunk.LoadNbt(&chunkData->nbt);
		return chunkData;
	}
	catch (std::exception &e)
	{
		delete chunkData;
		// TODO: Log error, or we can rethrow the exception.
		return nullptr;
	}
	return chunkData;
}

void MinecraftChunkFactory::FreeChunk(ChunkData *chunk)
{
	delete chunk;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
