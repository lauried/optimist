/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "minecraft/MinecraftRegionFile.hh"
#include "library/BinaryReader.hh"
#include "library/extern/Zlib.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

void MinecraftRegionFile::SetFile(IFile *file)
{
	this->file = file;
	locations.resize(NUM_CHUNKS * LOCATION_SIZE);
	file->ReadBytes((char*)&locations[0], NUM_CHUNKS * LOCATION_SIZE);
	timestamps.resize(NUM_CHUNKS * TIMESTAMP_SIZE);
	file->ReadBytes((char*)&timestamps[0], NUM_CHUNKS * TIMESTAMP_SIZE);
}

bool MinecraftRegionFile::HasChunk(int x, int z)
{
	size_t offset = OffsetForCoordinates(x, z);
	return locations[0] != 0 || locations[1] != 0 || locations[2] != 0 || locations[3] != 0;
}

uint32_t MinecraftRegionFile::GetChunkTimestamp(int x, int z)
{
	uint8_t *timestamp = &timestamps[OffsetForCoordinates(x, z)];
	return ((uint32_t)timestamp[0] << 24) | ((uint32_t)timestamp[1] << 16) | ((uint32_t)timestamp[2] << 8) | (uint32_t)timestamp[3];
}

uint32_t MinecraftRegionFile::GetChunkSectorOffset(int x, int z)
{
	uint8_t *chunkOffset = &locations[OffsetForCoordinates(x, z)];
	return (((uint32_t)chunkOffset[0] << 16) | ((uint32_t)chunkOffset[1] << 8) | (uint32_t)chunkOffset[2]);
}

void MinecraftRegionFile::DecodeChunk(int x, int z, std::vector<uint8_t> *data)
{
	if (!HasChunk(x, z))
	{
		throw region_no_chunk_error("No chunk stored at the given coordinates");
	}

	size_t offset = (size_t)GetChunkSectorOffset(x, z) * SECTOR_SIZE;

	uint8_t chunkHeader[5];
	file->Seek(offset);
	file->ReadBytes((char*)chunkHeader, 5);

	BinaryReader reader(chunkHeader, 5);
	uint32_t size = reader.ReadBigInt32(); // 1 is gzip, 2 = zlib
	uint8_t compressionType = reader.ReadByte();

	std::vector<uint8_t> compressedData;
	compressedData.resize(size - 1);
	file->ReadBytes((char*)&compressedData[0], compressedData.size());

	Zlib zlib;
	zlib.Decompress(compressedData, data);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
