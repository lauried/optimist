/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "minecraft/MinecraftChunk.hh"
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

void MinecraftChunk::LoadNbt(NbtFile *nbt)
{
	this->nbt = nbt;

	// Heightmap
	NbtFile::NbtTag *heightmapTag = nbt->GetTag("Level/HeightMap");
	if (heightmapTag == nullptr)
	{
		throw minecraft_chunk_decode_error("Tag Level/HeightMap not present");
	}
	if (heightmapTag->Type() != NbtFile::Tag_Int_Array)
	{
		throw minecraft_chunk_decode_error("Tag Level/Heightmap is not a Tag_Int_Array");
	}
	if (heightmapTag->Size() != 1024)
	{
		throw minecraft_chunk_decode_error("Tag Level/Heightmap Array size in bytes is not 1024");
	}
	heightmap = (int*)heightmapTag->GetData();

	// Sections
	// When data is missing, use a blank buffer
    blankSectionData.resize(4096);
    for (size_t i = 0; i < 0xf; i += 1)
    {
		blockData[i] = blockAdd[i] = block[i] = &blankSectionData[0];
    }
    NbtFile::NbtTag *sectionsTag = nbt->GetTag("Level/Sections");
    if (sectionsTag == nullptr)
    {
		throw minecraft_chunk_decode_error("Tag Level/Sections not present");
    }
    if (sectionsTag->Type() != NbtFile::Tag_List)
    {
		throw minecraft_chunk_decode_error("Tag Level/Sections not a Tag_List");
    }
    for (size_t i = 0; i < sectionsTag->NumValues(); i += 1)
    {
		NbtFile::NbtTag &sectionTag = sectionsTag->Value(i);
		NbtFile::NbtTag *sectionY = sectionTag.FindChild("Y");
		int yValue = i;
		if (sectionY != nullptr)
		{
			yValue = sectionY->GetInt();
		}
		yValue &= 0xf;

		NbtFile::NbtTag *sectionBlocks = sectionTag.FindChild("Blocks");
		if (sectionBlocks != nullptr && sectionBlocks->Size() == 4096)
		{
			block[yValue] = sectionBlocks->GetData();
		}

		NbtFile::NbtTag *sectionAdd = sectionTag.FindChild("Add");
		if (sectionAdd != nullptr && sectionAdd->Size() == 2048)
		{
			blockAdd[yValue] = sectionAdd->GetData();
		}

		NbtFile::NbtTag *sectionBlockData = sectionTag.FindChild("Data");
		if (sectionBlockData != nullptr && sectionBlockData->Size() == 2048)
		{
			blockData[yValue] = sectionBlockData->GetData();
		}

		NbtFile::NbtTag *sectionBlockLight = sectionTag.FindChild("BlockLight");
		if (sectionBlockLight != nullptr && sectionBlockLight->Size() == 2048)
		{
			blockLight[yValue] = sectionBlockLight->GetData();
		}

		NbtFile::NbtTag *sectionSkyLight = sectionTag.FindChild("SkyLight");
		if (sectionSkyLight != nullptr && sectionSkyLight->Size() == 2048)
		{
			skyLight[yValue] = sectionSkyLight->GetData();
		}
    }
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
