/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "minecraft/NbtFile.hh"
#include <iostream>
#include <cstring>
#include "library/text/String.hh"
#include "library/stream/MemoryByteSource.hh"
#include "library/stream/FileByteDestination.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

NbtFile::~NbtFile()
{
	root = nullptr;
	GarbageCollect();
}

void NbtFile::Load(IFile *file)
{
	data.resize(file->Size());
	file->ReadBytes((char*)&data[0], data.size());
	Process();
}

void NbtFile::Save(IFile *file)
{
	FileByteDestination destination(file);
	BinaryWriter writer(&destination);
	root->Save(&writer);
}

void NbtFile::Process()
{
	root = nullptr;
	GarbageCollect();

	MemoryByteSource source(data);
	BinaryReader reader(&source);
	try
	{
		root = CreateTag();
		root->Load(Tag_Compound, &reader);
	}
	catch (std::out_of_range &e)
	{
		throw nbt_decode_error("Unexpected EOF reached at buffer position " + String::ToString(reader.BytesRead()) + ": " + e.what());
	}
	catch (std::overflow_error &e)
	{
		throw nbt_decode_error("Unexpected EOF reached at buffer position " + String::ToString(reader.BytesRead()) + ": " + e.what());
	}
}

NbtFile::NbtTag *NbtFile::GetTag(std::string path)
{
	std::vector<std::string> parts;
	String::Tokenize(path, &parts, "/");

	if (parts.size() == 0)
	{
		return nullptr;
	}
	NbtTag *current = root;
	for (int i = 0; i < parts.size() && current != nullptr; i += 1)
	{
		current = current->FindChild(parts[i]);
	}
	return current;
}

NbtFile::NbtTag *NbtFile::CreateTag()
{
	NbtTag *tag = new NbtTag(this);
	tags.push_back(tag);
	return tag;
}

void NbtFile::FreeTag(NbtFile::NbtTag *tag)
{
	for (auto it = tags.begin(); it != tags.end(); ++it)
	{
		if (tag == *it)
		{
			tags.erase(it);
			delete tag;
			return;
		}
	}
	throw std::logic_error("Tag does not belong to file");
}

void NbtFile::GarbageCollect()
{
	// TODO:
	// Unmark all tags.
	// Traverse tree, marking all tags that get traversed, and putting the tags into a vector.
	// Iterate current vector, deleting all unmarked entries.
	// Replace the contents of the current vector with the contents of the new vector.
}

//-----------------------------------------------------------------------------
// NbtFile::NbtTag loading
//-----------------------------------------------------------------------------

void NbtFile::NbtTag::Reset()
{
	type = 0;
	size = 0;
	listSize = 0;
	data = nullptr;
	children.clear();
	values.resize(0);
}

// Loads the bytes into a value in big endian order, and then writes the value
// back to the buffer.
static void EndianAdjustShortArray(uint8_t *data, size_t numInts)
{
	uint16_t *pointer = (uint16_t *)data;
	for (int i = 0; i < numInts; i += 1, pointer += 1, data += 2)
	{
		*pointer = (data[0] << 8) | data[1];
	}
}

// Loads the bytes into a value in big endian order, and then writes the value
// back to the buffer.
static void EndianAdjustIntArray(uint8_t *data, size_t numInts)
{
	uint32_t *pointer = (uint32_t *)data;
	for (int i = 0; i < numInts; i += 1, pointer += 1, data += 4)
	{
		*pointer = (data[0] << 24) | (data[1] << 16) | (data[2] << 8) | data[3];
	}
}

// Loads the bytes into a value in big endian order, and then writes the value
// back to the buffer.
static void EndianAdjustLongArray(uint8_t *data, size_t numInts)
{
	uint64_t *pointer = (uint64_t *)data;
	for (int i = 0; i < numInts; i += 1, pointer += 1, data += 8)
	{
		*pointer = ((uint64_t)data[0] << 56)
			| ((uint64_t)data[1] << 48)
			| ((uint64_t)data[2] << 40)
			| ((uint64_t)data[3] << 32)
			| ((uint64_t)data[4] << 24)
			| ((uint64_t)data[5] << 16)
			| ((uint64_t)data[6] << 8)
			| data[7];
	}
}

void NbtFile::NbtTag::Load(int tagType, BinaryReader *reader)
{
	type = tagType;

	switch (tagType)
	{
	case Tag_Byte:
		{
			size = 1;
			data = reader->CurrentPointer();
			intData = reader->ReadByte();
			doubleData = intData;
			return;
		}
	case Tag_Short:
		{
			size = 2;
			data = reader->CurrentPointer();
			intData = reader->ReadBigInt16();
			doubleData = intData;
			return;
		}
	case Tag_Int:
		{
			size = 4;
			data = reader->CurrentPointer();
			intData = reader->ReadBigInt32();
			doubleData = intData;
			return;
		}
	case Tag_Float:
		{
			size = 4;
			data = reader->CurrentPointer();
			doubleData = reader->ReadBigFloat();
			intData = doubleData;
			return;
		}
	case Tag_Long:
		{
			size = 8;
			data = reader->CurrentPointer();
			intData = reader->ReadBigInt64();
			doubleData = intData;
			return;
		}
	case Tag_Double:
		{
			size = 8;
			data = reader->CurrentPointer();
			doubleData = reader->ReadBigDouble();
			intData = doubleData;
			return;
		}
	case Tag_Byte_Array:
		{
			size = reader->ReadBigInt32();
			data = reader->CurrentPointer();
			reader->Skip(size);
			doubleData = intData = 0;
			return;
		}
	case Tag_Int_Array:
		{
			listSize = reader->ReadBigInt32();
			size = listSize * 4;
			data = reader->CurrentPointer();
			reader->Skip(size);
			EndianAdjustIntArray(reader->CurrentPointer(), listSize);
			doubleData = intData = 0;
			return;
		}
	case Tag_Long_Array:
		{
			listSize = reader->ReadBigInt32();
			size = listSize * 8;
			data = reader->CurrentPointer();
			reader->Skip(size);
			EndianAdjustLongArray(reader->CurrentPointer(), listSize);
			doubleData = intData = 0;
			return;
		}
	case Tag_String:
		{
			size = reader->ReadBigInt16();
			data = reader->CurrentPointer();
			reader->Skip(size);
			doubleData = intData = 0;
			return;
		}
	case Tag_Compound:
		{
			while (true)
			{
				if (reader->Eof())
				{
					// Hack for root, which is really just a single named tag.
					break;
				}
				int tagType = reader->ReadByte();
				if (tagType == Tag_End)
				{
					break;
				}
				std::string name = ReadString(reader);
				NbtTag *tag = file->CreateTag();
				tag->Load(tagType, reader);
				children.insert(std::make_pair(name, tag));
			}
			data = nullptr;
			doubleData = intData = 0;
			return;
		}
	case Tag_List:
		{
			listType = reader->ReadByte();
			LoadListData(reader);
			return;
		}
	case Tag_End:
		{
			size = 0;
			data = nullptr;
			doubleData = intData = 0;
			return;
		}
	default:
		throw std::out_of_range("Unexpected tag type");
	}
}

std::string NbtFile::NbtTag::ReadString(BinaryReader *reader)
{
	int numBytes = reader->ReadBigInt16();
	std::vector<char> bytes;
	bytes.resize(numBytes);
	reader->ReadBytes(numBytes, (uint8_t*)&bytes[0]);
	return std::string(&bytes[0], numBytes);
}

void NbtFile::NbtTag::WriteString(std::string str, BinaryWriter *writer)
{
	size_t sizeTruncated = str.length() & 0x7fff;
	writer->WriteBigInt16(sizeTruncated);
	if (sizeTruncated > 0)
	{
		writer->WriteBytes(sizeTruncated, (uint8_t*)str.c_str());
	}
}

void NbtFile::NbtTag::LoadListData(BinaryReader *reader)
{
	doubleData = intData = 0;
	listSize = reader->ReadBigInt32();

	switch (listType)
	{
	case Tag_Byte:
		size = listSize;
		data = reader->CurrentPointer();
		reader->Skip(size);
		break;
	case Tag_Short:
		size = listSize * 2;
		data = reader->CurrentPointer();
		EndianAdjustShortArray(data, listSize);
		reader->Skip(size);
		break;
	case Tag_Int:
	case Tag_Float:
		size = listSize * 4;
		data = reader->CurrentPointer();
		EndianAdjustIntArray(data, listSize);
		reader->Skip(size);
		break;
	case Tag_Long:
	case Tag_Double:
		size = listSize * 8;
		data = reader->CurrentPointer();
		EndianAdjustLongArray(data, listSize);
		reader->Skip(size);
		break;
	case Tag_Byte_Array:
	case Tag_String:
	case Tag_List:
	case Tag_Compound:
	case Tag_Int_Array:
	case Tag_Long_Array:
		for (int i = 0; i < listSize; i += 1)
		{
			NbtTag *tag = file->CreateTag();
			tag->Load(listType, reader);
			values.push_back(tag);
		}
		size = 0;
		listSize = 0;
		data = nullptr;
		break;
	}
}

void NbtFile::NbtTag::Save(BinaryWriter *writer)
{
	switch (type)
	{
	case Tag_Byte:
		{
			writer->WriteByte(intData);
			return;
		}
	case Tag_Short:
		{
			writer->WriteBigInt16(intData);
			return;
		}
	case Tag_Int:
		{
			writer->WriteBigInt32(intData);
			return;
		}
	case Tag_Float:
		{
			writer->WriteBigFloat(doubleData);
			return;
		}
	case Tag_Long:
		{
			writer->WriteBigInt64(intData);
			return;
		}
	case Tag_Double:
		{
			writer->WriteBigDouble(doubleData);
			return;
		}
	case Tag_Byte_Array:
		{
			size_t truncSize = size & 0x7fffffff;
			writer->WriteBigInt32(truncSize);
			if (truncSize > 0)
			{
				writer->WriteBytes(truncSize, data);
			}
			return;
		}
	case Tag_Int_Array:
		{
			size_t truncSize = listSize & 0x7fffffff;
			writer->WriteBigInt32(listSize);
			if (truncSize > 0)
			{
				writer->WriteBigUint32Array((uint32_t *)data, listSize);
			}
			return;
		}
	case Tag_Long_Array:
		{
			size_t truncSize = listSize & 0x7fffffff;
			writer->WriteBigInt32(listSize);
			if (truncSize > 0)
			{
				writer->WriteBigUint64Array((uint64_t *)data, listSize);
			}
			return;
		}
	case Tag_String:
		{
			size_t truncSize = size & 0x7fff;
			writer->WriteBigInt16(truncSize);
			if (truncSize > 0)
			{
				writer->WriteBytes(truncSize, data);
			}
			return;
		}
	case Tag_Compound:
		{
			for (auto it = children.begin(); it != children.end(); ++it)
			{
				writer->WriteByte(it->second->Type());
				WriteString(it->first, writer);
				it->second->Save(writer);
			}
			writer->WriteByte(Tag_End);
			return;
		}
	case Tag_List:
		{
            writer->WriteByte(listType);
            SaveListData(writer);
			return;
		}
	case Tag_End:
		{
			return;
		}
	default:
		throw std::out_of_range("Unexpected tag type");
	}
}

void NbtFile::NbtTag::SaveListData(BinaryWriter *writer)
{
	size_t truncSize = listSize & 0x7fffffff;

	switch (listType)
	{
	case Tag_Byte:
		writer->WriteBigInt32(truncSize);
		writer->WriteBytes(truncSize, data);
		return;
	case Tag_Short:
		writer->WriteBigInt32(truncSize);
		writer->WriteBigUint16Array((uint16_t*)data, truncSize);
		return;
	case Tag_Float:
	case Tag_Int:
		writer->WriteBigInt32(truncSize);
		writer->WriteBigUint32Array((uint32_t*)data, truncSize);
		return;
	case Tag_Double:
	case Tag_Long:
		writer->WriteBigInt32(truncSize);
		writer->WriteBigUint64Array((uint64_t*)data, truncSize);
		return;
	}

	writer->WriteBigInt32(truncSize);
	truncSize = values.size() & 0x7fffffff;
	for (int i = 0; i < truncSize; i += 1)
	{
		values[i]->Save(writer);
	}
}

bool NbtFile::NbtTag::ListIsScalar()
{
	if (type != Tag_List)
	{
		return false;
	}
	switch (listType)
	{
	case Tag_Byte:
	case Tag_Short:
	case Tag_Int:
	case Tag_Float:
	case Tag_Long:
	case Tag_Double:
		return true;
	}
	return false;
}

std::string NbtFile::NbtTag::GetString()
{
	return std::string((char *)data, size);
}

int64_t NbtFile::NbtTag::GetInt()
{
	return intData;
}

double NbtFile::NbtTag::GetDouble()
{
	return doubleData;
}

NbtFile::NbtTag* NbtFile::NbtTag::FindChild(std::string name)
{
	if (type == Tag_List)
	{
		int index = String::ToInt(name);
		if (index > 0 && index < values.size())
		{
			return values[index];
		}
		return nullptr;
	}
	if (type == Tag_Compound)
	{
		auto it = children.find(name);
		if (it == children.end())
		{
			return nullptr;
		}
		return it->second;
	}

	return nullptr;
}

//-----------------------------------------------------------------------------
// NbtFile::NbtTag setting data
//-----------------------------------------------------------------------------

NbtTag* NbtFile::NbtTag::SetType(int newType)
{
	if (type == newType)
	{
		return this;
	}

	type = newType;
	listType = Tag_End;
	size = 0;
	children.clear();
	values.resize(0);
	data = nullptr;
	intData = 0;
	doubleData = 0;

	return this;
}

NbtTag* NbtFile::NbtTag::Set(int64_t value)
{
	switch (type)
	{
	case Tag_Byte:
	case Tag_Short:
	case Tag_Int:
	case Tag_Long:
	case Tag_Float:
	case Tag_Double:
		doubleData = value;
		intData = value;
		break;
	default:
		throw std::logic_error("Cannot set a non-scalar type to a scalar value");
	}

	return this;
}

NbtTag* NbtFile::NbtTag::Set(double value)
{
	switch (type)
	{
	case Tag_Byte:
	case Tag_Short:
	case Tag_Int:
	case Tag_Long:
	case Tag_Float:
	case Tag_Double:
		doubleData = value;
		intData = value;
		break;
	default:
		throw std::logic_error("Cannot set a non-scalar type to a scalar value");
	}

	return this;
}

NbtTag* NbtFile::NbtTag::Set(char *value, int length)
{
	if (type != Tag_String)
	{
		throw std::logic_error("Cannot set a non-string type to a string value");
	}

	data = (uint8_t *)value;
	size = length;

	return this;
}

NbtTag* NbtFile::NbtTag::Set(uint8_t *value, int num)
{
	if (type != Tag_Byte_Array && type != Tag_List)
	{
		throw std::logic_error("Cannot set a non-byte-array, non-list type to a byte-array value");
	}

	data = value;
	size = num;

	if (type == Tag_List)
	{
		values.resize(0);
		listType = Tag_Byte;
		listSize = num;
	}

	return this;
}

NbtTag* NbtFile::NbtTag::Set(uint16_t *value, int num)
{
	if (type != Tag_List)
	{
		throw std::logic_error("Cannot set non-list type to short-array value");
	}
	data = (uint8_t *)value;
	size = num * 2;
	values.resize(0);
	listType = Tag_Short;
	listSize = num;

	return this;
}

NbtTag* NbtFile::NbtTag::Set(uint32_t *value, int num)
{
	if (type != Tag_Int_Array && type != Tag_List)
	{
		throw std::logic_error("Cannot set a non-int-array, non-list type to an int-array value");
	}

	data = (uint8_t *)value;
	size = num * 4;

	if (type == Tag_List)
	{
		values.resize(0);
		listType = Tag_Int;
		listSize = num;
	}

	return this;
}

NbtTag* NbtFile::NbtTag::Set(uint64_t *value, int num)
{
	if (type != Tag_Int_Array && type != Tag_List)
	{
		throw std::logic_error("Cannot set a non-int-array, non-list type to an int-array value");
	}

	data = (uint8_t *)value;
	size = num * 8;

	if (type == Tag_List)
	{
		values.resize(0);
		listType = Tag_Long;
		listSize = num;
	}

	return this;
}

NbtTag* NbtFile::NbtTag::Set(float *value, int num)
{
	if (type != Tag_List)
	{
		throw std::logic_error("Cannot set non-list type to float-array value");
	}
	Set((uint32_t *)value, num);
	listType = Tag_Float;

	return this;
}

NbtTag* NbtFile::NbtTag::Set(double *value, int num)
{
	if (type != Tag_List)
	{
		throw std::logic_error("Cannot set non-list type to double-array value");
	}
	Set((uint64_t *)value, num);
	listType = Tag_Double;

	return this;
}

NbtTag* NbtFile::NbtTag::Set(std::string name, NbtFile::NbtTag *value)
{
	if (type != Tag_Compound)
	{
		throw std::logic_error("Only compound type can be set with string indexes");
	}
	children.insert(std::make_pair(name, value));

	return this;
}

NbtTag* NbtFile::NbtTag::Clear(std::string name)
{
	if (type != Tag_Compound)
	{
		throw std::logic_error("Only compound type can be set with string indexes");
	}
	auto it = children.find(name);
	if (it != children.end())
	{
		children.erase(it);
	}

	return this;
}

NbtTag* NbtFile::NbtTag::Insert(size_t index, NbtFile::NbtTag *tag)
{
	if (type != Tag_List)
	{
		throw std::logic_error("Only list type can be set with int indexes");
	}
	if (values.size() > 0 && tag->Type() != listType)
	{
		throw std::logic_error("Cannot mix types in a list");
	}
	listType = tag->Type();
	data = nullptr;
	size = 0;
	listSize = 0;
	values.insert(values.begin() + index, tag);

	return this;
}

NbtTag* NbtFile::NbtTag::Append(NbtFile::NbtTag *tag)
{
	if (type != Tag_List)
	{
		throw std::logic_error("Only list type can be set with int indexes");
	}
	if (values.size() > 0 && tag->Type() != listType)
	{
		throw std::logic_error("Cannot mix types in a list");
	}
	listType = tag->Type();
	data = nullptr;
	size = 0;
	listSize = 0;
	values.push_back(tag);

	return this;
}

NbtTag* NbtFile::NbtTag::Clear(size_t index)
{
	if (type != Tag_List)
	{
		throw std::logic_error("Only list type can be set with int indexes");
	}
	values.erase(values.begin() + index);

	return this;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
