/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "minecraft/MinecraftWorld.hh"
#include "log/ILogTarget.hh"
#include "library/geometry/Vector2.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Wraps around a world to give us the chunk for given coords in a given
 * dimension.
 */
class MinecraftChunkFactory {
public:
	struct ChunkData {
		NbtFile nbt;
		MinecraftChunk chunk;
	};

	MinecraftChunkFactory(MinecraftWorld *world, ILogTarget *log)
		: world(world)
		, log(log)
		{}

	ChunkData *GetChunk(std::string dimensionName, Vector2I coordinates);
	void FreeChunk(ChunkData *chunk);

private:
	MinecraftWorld *world;
	ILogTarget *log;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

