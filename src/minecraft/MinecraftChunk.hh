/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "minecraft/NbtFile.hh"
#include <cstdint>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------


class minecraft_chunk_decode_error : public std::runtime_error {
public:
	minecraft_chunk_decode_error(const std::string& what_arg) : runtime_error(what_arg) {}
	minecraft_chunk_decode_error(const char* what_arg) : runtime_error(what_arg) {}
};

/**
 * Facade/adapter around an NBT file to let it be accessed in a way that makes
 * more sense for a chunk.
 */
class MinecraftChunk {
public:
	const int CHUNK_SIZE = 16;
	const int SECTION_HEIGHT = 16;

	void LoadNbt(NbtFile *nbt);

	inline int GetHeightmap(int x, int z)
	{
		int offset = (x & 0xf) + ((z & 0xf) << 4);
		return heightmap[offset];
	}

	inline int GetBlockID(int x, int y, int z)
	{
		int section = (y & 0xf0) >> 4;
		int offset = (x & 0xf) + ((z & 0xf) << 4) + ((y & 0xf) << 8);
		int shift = (1 ^ (offset & 1)) << 2; // if offset & 1 then 0, else 1
		int halfOffset = offset >> 1;
		return block[section][offset] + (((blockData[section][halfOffset] >> shift) & 0xf) << 8);
	}

	inline int GetBlockData(int x, int y, int z)
	{
		int section = (y & 0xf0) >> 4;
		int offset = (x & 0xf) + ((z & 0xf) << 4) + ((y & 0xf) << 8);
		int shift = (offset & 1) << 2;
		int halfOffset = offset >> 1;
		return ((blockData[section][halfOffset] >> shift) & 0xf);
	}

	inline int GetBlockLight(int x, int y, int z)
	{
		int section = (y & 0xf0) >> 4;
		int offset = (x & 0xf) + ((z & 0xf) << 4) + ((y & 0xf) << 8);
		int shift = (offset & 1) << 2;
		int halfOffset = offset >> 1;
		return ((blockLight[section][halfOffset] >> shift) & 0xf);
	}

	inline int GetSkyLight(int x, int y, int z)
	{
		int section = (y & 0xf0) >> 4;
		int offset = (x & 0xf) + ((z & 0xf) << 4) + ((y & 0xf) << 8);
		int shift = (offset & 1) << 2;
		int halfOffset = offset >> 1;
		return ((skyLight[section][halfOffset] >> shift) & 0xf);
	}

	// TODO: Also enable querying of entities.
	// Build our own structures for them, so that the properties can just be
	// traversed.

	// TODO: Also investigate making a multidimensional iterator.
	// Something that lets us inject a strategy into say a 2D lerp and compile
	// to something fast would also be good.

private:
	NbtFile *nbt;
	const int32_t *heightmap;
	const uint8_t *block[0xf];
	const uint8_t *blockAdd[0xf];
	const uint8_t *blockData[0xf];
	const uint8_t *blockLight[0xf];
	const uint8_t *skyLight[0xf];
	std::vector<uint8_t> blankSectionData;

};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

