/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "font/IFont.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Built-in bitmap font.
 * The purpose is to always have a 2D font even when no other resources have
 * been loaded.
 */
class StbFont : public IFont {
public:
	StbFont();
	~StbFont();

	// IFont
	GlyphInfo* GetGlyph(int glyph);
	int NumImages();
	ITextureProvider *GetTextureProvider(int index);
	int LineHeight() { return lineHeight; }

	static StbFont instance;

private:
	struct TextureProvider : public ITextureProvider {
		bool updated;
		Image *image;
		virtual Image* GetImage();
		virtual bool ImageUpdated();
	};

	Image image;
	TextureProvider textureProvider;
	GlyphInfo *glyphs;
	int firstChar;
	int numChars;
	int lineHeight;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
