/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "font/StbFont.hh"
#include <stdexcept>
#include <iostream>
#include "font/stb/stb_font_consolas_14_usascii.h"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

StbFont StbFont::instance;

StbFont::StbFont()
{
	firstChar = STB_SOMEFONT_FIRST_CHAR;
	numChars = STB_SOMEFONT_NUM_CHARS;
	lineHeight = 14;//STB_SOMEFONT_LINE_SPACING;

	stb_fontchar fontdata[STB_SOMEFONT_NUM_CHARS];
	unsigned char fontpixels[STB_SOMEFONT_BITMAP_HEIGHT_POW2][STB_SOMEFONT_BITMAP_WIDTH];
	STB_SOMEFONT_CREATE(fontdata, fontpixels, STB_SOMEFONT_BITMAP_HEIGHT_POW2);

	glyphs = new IFont::GlyphInfo[numChars];
	image.Clear(STB_SOMEFONT_BITMAP_WIDTH, STB_SOMEFONT_BITMAP_HEIGHT_POW2, 4);

	unsigned char *src = &fontpixels[0][0];
	uint8_t *dest = image.Data();
	for (int i = 0; i < STB_SOMEFONT_BITMAP_WIDTH * STB_SOMEFONT_BITMAP_HEIGHT_POW2; i += 1)
	{
		*(dest++) = 0xff;
		*(dest++) = 0xff;
		*(dest++) = 0xff;
		*(dest++) = *(src++);
	}

	for (int i = 0; i < numChars; i += 1)
	{
		glyphs[i].imageIndex = 0;
		glyphs[i].s0 = fontdata[i].s0 * STB_SOMEFONT_BITMAP_WIDTH;
		glyphs[i].t0 = fontdata[i].t0 * STB_SOMEFONT_BITMAP_HEIGHT_POW2;
		glyphs[i].s1 = fontdata[i].s1 * STB_SOMEFONT_BITMAP_WIDTH;
		glyphs[i].t1 = fontdata[i].t1 * STB_SOMEFONT_BITMAP_HEIGHT_POW2;
		glyphs[i].fs0 = fontdata[i].s0;
		glyphs[i].ft0 = fontdata[i].t0;
		glyphs[i].fs1 = fontdata[i].s1;
		glyphs[i].ft1 = fontdata[i].t1;
		glyphs[i].px = fontdata[i].x0;
		glyphs[i].py = fontdata[i].y0;
		glyphs[i].advance = fontdata[i].advance_int;
#if 0
		std::cout << (char)(i + firstChar)
			<< " s0=" << glyphs[i].s0
			<< " t0=" << glyphs[i].t0
			<< " s1=" << glyphs[i].s1
			<< " t1=" << glyphs[i].t1
			<< " px=" << glyphs[i].px
			<< " py=" << glyphs[i].py
			<< " advance=" << glyphs[i].advance
			<< std::endl;
#endif
	}

	textureProvider.image = &image;
	textureProvider.updated = true;
}

StbFont::~StbFont()
{
	delete[] glyphs;
}

IFont::GlyphInfo* StbFont::GetGlyph(int glyph)
{
	if (glyph < firstChar || glyph >= firstChar + numChars)
	{
		return nullptr;
	}
	return &glyphs[glyph - firstChar];
}

int StbFont::NumImages()
{
	return 1;
}

ITextureProvider* StbFont::GetTextureProvider(int index)
{
	if (index != 0)
	{
		throw std::out_of_range("Invalid image index");
	}
	return &textureProvider;
}

Image* StbFont::TextureProvider::GetImage()
{
	updated = false;
	return image;
}

bool StbFont::TextureProvider::ImageUpdated()
{
	return updated;
}


//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
