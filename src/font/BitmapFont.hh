/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <map>
#include <vector>
#include "font/IFont.hh"
#include "library/graphics/Image.hh"
#include "library/file_types/OptiFontFile.hh"
#include "resources/ResourceManager.hh"
#include "resources/ImageResource.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// BitmapFont currently uses the ResourceManager to get the image on which its
// glyphs are stored.

// This dependency came about because it combines the file format with the
// final data. In reality the file format should be separate, and the loader
// should read the file, load the image and pass both to the constructor of
// BitmapFont.

// We'll essentially be constructing bitmapfont with the glyphinfo and the
// texture.

class BitmapFont : public IFont {
public:
	BitmapFont(OptiFontFile *optiFont, ResourceManager::ResourcePointer<ImageResource> imageResource);
	~BitmapFont();

	// IFont
	GlyphInfo* GetGlyph(int glyph);
	int NumImages();
	ITextureProvider* GetTextureProvider(int index);
	int LineHeight() { return lineHeight; }
private:
	struct TextureProvider : public ITextureProvider {
		bool updated;
		Image *image;
		virtual Image* GetImage();
		virtual bool ImageUpdated();
	};

	Image image;
	TextureProvider textureProvider;
	int lineHeight;
	std::map<int, GlyphInfo*> glyphsByChar;
	std::vector<GlyphInfo> glyphInfo;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

