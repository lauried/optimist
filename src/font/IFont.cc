/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "font/IFont.hh"
#include <sstream>
#include <iostream>
#include "library/text/String.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

IFont::~IFont()
{
}

// static
std::string IFont::WrapText(std::string text, IFont *font, int wrapWidth, int *numLines, int *maxWidth, int flags)
{
	std::stringstream output;
	std::stringstream currentWord;
	int currentWordWidth = 0;
	int currentWidth = 0;
	int currentLine = 0;
	int longestLineWidth = 0;
	bool firstWord = true; // never try wrapping if we're the first word

	// TODO: Re-do so that we only add to current width when we've added the
	// longest word.

	for (int code = 0, pos = 0, bytesRead = 0; pos < text.size() && (bytesRead = String::UTF8ToUnicode(text, pos, &code)) > 0; pos += bytesRead)
	{
		if (code == '\n')
		{
			output << currentWord.str() << '\n';
			currentWord.str(std::string());
			currentWidth += currentWordWidth;
			if (longestLineWidth < currentWidth)
			{
				longestLineWidth = currentWidth;
			}
			currentWordWidth = 0;
			currentWidth = 0;
			currentLine += 1;
			firstWord = true;
			continue;
		}
		GlyphInfo *glyph = font->GetGlyph(code);
		if (glyph == nullptr)
		{
			continue;
		}
		if (code == ' ')
		{
			output << currentWord.str() << ' ';
			currentWord.str(std::string());
			currentWidth += currentWordWidth + glyph->advance;
			currentWordWidth = 0;
			firstWord = false;
			continue;
		}
		int glyphWidth = glyph-> px + (glyph->s1 - glyph->s0);
        if (wrapWidth != 0 && currentWidth + currentWordWidth + glyphWidth > wrapWidth)
		{
			if (!firstWord)
			{
				output << '\n';
				currentWidth = currentWordWidth;
				currentWordWidth = 0;
				currentLine += 1;
				firstWord = true;
			}
			else if (flags & WRAP_FORCE_WORD_BREAK)
			{
				output << currentWord.str() << '\n';
				currentWord.str(std::string());
				currentWidth += currentWordWidth;
				if (longestLineWidth < currentWidth)
				{
					longestLineWidth = currentWidth;
				}
				currentWordWidth = 0;
				currentWidth = 0;
				currentLine += 1;
				firstWord = true;
			}
		}
		char buf[7];
		String::UnicodeToUTF8(code, buf);
		currentWord << buf;
		currentWordWidth += glyph->advance;
	}

	if (numLines != nullptr)
	{
		*numLines = currentLine + 1;
	}
	if (maxWidth != nullptr)
	{
		*maxWidth = longestLineWidth;
	}

	output << currentWord.str();
	return output.str();
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
