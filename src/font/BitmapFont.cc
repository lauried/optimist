/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "font/BitmapFont.hh"
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

BitmapFont::BitmapFont(OptiFontFile *optiFont, ResourceManager::ResourcePointer<ImageResource> imageResource)
{
	lineHeight = optiFont->GetLineHeight();

	float invWidth = 1.0f / (float)optiFont->GetWidth();
	float invHeight = 1.0f / (float)optiFont->GetHeight();

	std::vector<OptiFontFile::GlyphInfo> &glyphData = optiFont->GetGlyphInfo();

	glyphInfo.resize(glyphData.size());
	for (int i = 0; i < glyphData.size(); i += 1)
	{
#if 0
		char txtGlyph[7];
		String::UnicodeToUTF8(glyphData[i].glyph, txtGlyph);
		std::cout << txtGlyph
			<<  " s0="  << glyphData[i].s0
			<< ", t0="  << glyphData[i].t0
			<< ", s1="  << glyphData[i].s1
			<< ", t1="  << glyphData[i].t1
			<< ", x="   << glyphData[i].x
			<< ", y="   << glyphData[i].y
			<< ", adv=" << glyphData[i].adv
			<< std::endl;
#endif
		glyphInfo[i].imageIndex = 0;
		glyphInfo[i].s0 = glyphData[i].s0;
		glyphInfo[i].t0 = glyphData[i].t0;
		glyphInfo[i].s1 = glyphData[i].s1;
		glyphInfo[i].t1 = glyphData[i].t1;
		glyphInfo[i].fs0 = (float)glyphData[i].s0 * invWidth;
		glyphInfo[i].fs1 = (float)glyphData[i].s1 * invWidth;
		glyphInfo[i].ft0 = (float)glyphData[i].t0 * invHeight;
		glyphInfo[i].ft1 = (float)glyphData[i].t1 * invHeight;
		glyphInfo[i].px = glyphData[i].x;
		glyphInfo[i].py = glyphData[i].y;
		glyphInfo[i].advance = glyphData[i].adv;
		glyphsByChar.insert(std::make_pair(glyphData[i].glyph, &glyphInfo[i]));
	}

	// Alpha map to our image.
	Image *sourceImage = imageResource.GetResource()->GetImage();
	image.Clear(sourceImage->Width(), sourceImage->Height(), 4);
	uint8_t *source = sourceImage->Data();
	uint8_t *dest = image.Data();
	int numPixels = image.Width() * image.Height();
	for (int i = 0; i < numPixels; i += 1, dest += image.Bpp(), source += sourceImage->Bpp())
	{
		dest[0] = 0xff;
		dest[1] = 0xff;
		dest[2] = 0xff;
		dest[3] = source[0];
	}

	// Set up our image provider.
	textureProvider.image = &image;
	textureProvider.updated = true;
}

BitmapFont::~BitmapFont()
{
}

IFont::GlyphInfo* BitmapFont::GetGlyph(int glyph)
{
	auto it = glyphsByChar.find(glyph);
	if (it == glyphsByChar.end())
	{
		return nullptr;
	}
	return it->second;
}

int BitmapFont::NumImages()
{
	return 1;
}

ITextureProvider* BitmapFont::GetTextureProvider(int index)
{
	if (index != 0)
	{
		return nullptr;
	}
	return &textureProvider;
}

Image* BitmapFont::TextureProvider::GetImage()
{
	updated = false;
	return image;
}

bool BitmapFont::TextureProvider::ImageUpdated()
{
	return updated;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
