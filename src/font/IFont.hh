/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "library/graphics/Image.hh"
#include "renderer/ITextureProvider.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Interface for an abstract font object.
 * The font provides a set of glyphs on one or more images, which can then be
 * rendered in a renderer.
 */
class IFont {
public:
	struct GlyphInfo {
		int imageIndex;
		int s0, s1, t0, t1; ///< Position on the image of the glyph, in pixels.
		float fs0, fs1, ft0, ft1; ///< Position on the image of the glyph, in fractions of the whole image.
		int px, py; ///< Position relative from the cursor, with the cursor at the top.
		int advance; ///< Amount that the cursor is advanced after this character.
	};

	virtual ~IFont();

	/**
	 * Returns the info for the given glyph, or nullptr if there's no such
	 * glyph in the font.
	 * The glyph is specified as a unicode index.
	 */
	virtual GlyphInfo* GetGlyph(int glyph) = 0;

	/**
	 * Returns the number of images.
	 */
	virtual int NumImages() = 0;

	/**
	 * Return an ITextureProvider for the given GlyphInfo::imageIndex.
	 * Expects a valid index, which glyph data is guaranteed to provide.
	 * Indexed from zero to 1 - NumImages();
	 */
	virtual ITextureProvider *GetTextureProvider(int index) = 0;

	/**
	 * Returns the height of a line in pixels.
	 */
	virtual int LineHeight() = 0;

	/**
	 * Flags for WrapText.
	 */
	enum {
		WRAP_NONE = 0,
		WRAP_FORCE_WORD_BREAK = 1, ///< Force line breaks inside of words that are longer than the desired width.
	} WrapFlags;

    /**
     * Returns a string with newlines inserted to wrap it to fit inside the
     * given width for the given font object.
     *
     * @param text The original string to add linebreaks to.
     * @param font The font to use for measurements.
     * @param width The width at which to wrap text. If zero, the text won't be wrapped but we'll still count the number of lines and the maximum width.
     * @param numLines A pointer to an int. If not nullptr, will be written with the number of lines in the wrapped text.
     * @param maxWidth A pointer to an int. If not nullptr, will be written with the maximum line width of the text.
     * @param flags See WrapFlags.
     */
    static std::string WrapText(std::string text, IFont *font, int wrapWidth, int *numLines = nullptr, int *maxWidth = nullptr, int flags = WRAP_NONE);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

