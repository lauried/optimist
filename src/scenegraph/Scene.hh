/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "renderer/IRenderCore.hh"
#include <stack>
#include <list>
#include <map>
#include <unordered_set>
#include "scenegraph/IRenderMeshProvider.hh"
#include "library/geometry/Orientation.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

struct SceneLight {
	enum Type {
		T_Ambient,
		T_Directional,
		T_Point,
	};
	enum Attenuation {
		A_None,
		A_Inverse,
		A_InvSquare
	};

	Type type;
	Attenuation attenuation;
	Vector3F position; // or direction. Unused for ambient.
	Vector3F colour;
	float brightness;

	SceneLight() : type(T_Point), attenuation(A_None), brightness(1.0f) {}
};

class Scene {
public:
	struct ObjectEntry {
		IRenderMeshProvider *object;
		ObjectEntry(IRenderMeshProvider *object) : object(object) {}
	};

	Scene();

	void Draw();
	void AddObject(IRenderMeshProvider *object) { objects.insert(std::make_pair(object, ObjectEntry(object))); }
	void RemoveObject(IRenderMeshProvider *object) { objects.erase(object); }

	void InsertSceneBefore(Scene *scene, Scene *ref = nullptr);
	void InsertSceneAfter(Scene *scene, Scene *ref = nullptr);
	void RemoveScene(Scene *scene);

	void AddLight(SceneLight *light) { lights.insert(light); }
	void RemoveLight(SceneLight *light) { lights.erase(light); }

	std::map<IRenderMeshProvider*, ObjectEntry> objects;
	std::unordered_set<SceneLight*> lights;
	std::list<Scene*> scenes;

	OrientationF position;
	bool originFollowsCamera;

	bool clearDepth;
	bool clearColour;
	bool additiveBlend;
	bool rearShading;
	float saturation;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
