/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gamecode/IGamecodeEngineObject.hh"
#include "gamecode/library/GamecodeOrientation.hh"
#include "scenegraph/Scene.hh"
#include "scenegraph/ScenegraphAdapter.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class GamecodeScene : public Scene, public ParentGamecodeEngineObject {
public:
	GamecodeScene() : positionWrapper(nullptr) {}

	// IGamecodeEngineObject
	void SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode);
	GamecodeValue GetProperty(std::string key, IGamecode *gamecode);
	void Destruct();

	// ParentGamecodeEngineObject
	void RebuildChildObjectList();

	// Gamecode Engine Methods
	GamecodeValue AddObject(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue RemoveObject(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue InsertSceneBefore(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue InsertSceneAfter(GamecodeParameterList *parameters, IGamecode *gamecode);

	// Static stuff
	static IGamecodeEngineObject* Construct(GamecodeParameterList *parameters, IGamecode *gamecode);

	// Used to keep track of our own child objects.
	std::map<IGamecodeEngineObject*, IGamecodeEngineObject*> childEngineObjects;

	GamecodeOrientation *positionWrapper;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
