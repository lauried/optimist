/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "scenegraph/GamecodeScene.hh"
#include "scenegraph/GamecodeScenegraphLight.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// IGamecodeEngineObject
//-----------------------------------------------------------------------------

void GamecodeScene::SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode)
{
	if (!key.compare("saturation"))
	{
		saturation = val.GetFloat();
		return;
	}
	if (!key.compare("rearShading"))
	{
		rearShading = (bool)val.GetInt();
		return;
	}
	if (!key.compare("clearDepth"))
	{
		clearDepth = (bool)val.GetInt();
		return;
	}
	if (!key.compare("clearColour"))
	{
		clearColour = (bool)val.GetInt();
		return;
	}
	if (!key.compare("additiveBlend"))
	{
		additiveBlend = (bool)val.GetInt();
		return;
	}

	if (!key.compare("position"))
	{
		throw gamecode_exception("position property may not be set, but it may be acted upon");
	}
	if (!key.compare("originFollowsCamera"))
	{
		originFollowsCamera = (bool)val.GetInt();
		return;
	}
	throw gamecode_exception("Unknown property");
}

GamecodeValue GamecodeScene::GetProperty(std::string key, IGamecode *gamecode)
{
	if (!key.compare("saturation"))
	{
		return GamecodeValue(saturation);
	}
	if (!key.compare("rearShading"))
	{
		return GamecodeValue(rearShading ? 1 : 0);
	}
	if (!key.compare("clearDepth"))
	{
		return GamecodeValue(clearDepth ? 1 : 0);
	}
	if (!key.compare("clearColour"))
	{
		return GamecodeValue(clearColour ? 1 : 0);
	}
	if (!key.compare("additiveBlend"))
	{
		return GamecodeValue(additiveBlend ? 1 : 0);
	}
	if (!key.compare("position"))
	{
		if (positionWrapper == nullptr)
		{
			positionWrapper = new GamecodeOrientation(&position);
			AddChildObject(positionWrapper);
		}
		return GamecodeValue(positionWrapper);
	}
	if (!key.compare("originFollowsCamera"))
	{
		return GamecodeValue(originFollowsCamera ? 1 : 0);
	}
	throw gamecode_exception("Unknown property");
}

GamecodeValue GamecodeScene::AddObject(GamecodeParameterList *values, IGamecode *gamecode)
{
	values->RequireParmCount(1);
	IGamecodeEngineObject *object = values->RequireType(0, GamecodeValue::T_Engine).GetEngine();

	IRenderMeshProvider *sgo = dynamic_cast<IRenderMeshProvider*>(object);
	if (sgo != nullptr)
	{
		Scene::AddObject((IRenderMeshProvider*)sgo);
		return GamecodeValue();
	}

	SceneLight *light = dynamic_cast<SceneLight*>(object);
	if (light != nullptr)
	{
		Scene::AddLight((SceneLight*)light);
		childEngineObjects.insert(std::make_pair(object, object));
		return GamecodeValue();
	}

	throw gamecode_exception("Not a SceneObject or SceneLight");
}

GamecodeValue GamecodeScene::RemoveObject(GamecodeParameterList *values, IGamecode *gamecode)
{
	values->RequireParmCount(1);
	IGamecodeEngineObject *object = values->RequireType(0, GamecodeValue::T_Engine).GetEngine();

	IRenderMeshProvider *sgo = dynamic_cast<IRenderMeshProvider*>(object);
	if (sgo != nullptr)
	{
		Scene::RemoveObject((IRenderMeshProvider*)sgo);
		childEngineObjects.erase(object);
		return GamecodeValue();
	}

	// TODO light

	throw gamecode_exception("Not a SceneObject or SceneLight");
}

GamecodeValue GamecodeScene::InsertSceneBefore(GamecodeParameterList *values, IGamecode *gamecode)
{
    values->RequireParmCount(1, 2);
    GamecodeScene *scene = values->RequireEngineType<GamecodeScene>(0, "Scene");
    GamecodeScene *ref = (values->size() > 1) ? values->RequireEngineType<GamecodeScene>(1, "Scene") : nullptr;
	Scene::InsertSceneBefore(scene, ref);
	childEngineObjects.insert(std::pair<IGamecodeEngineObject*, IGamecodeEngineObject*>(scene, scene));

	return GamecodeValue();
}

GamecodeValue GamecodeScene::InsertSceneAfter(GamecodeParameterList *values, IGamecode *gamecode)
{
	values->RequireParmCount(1, 2);
    GamecodeScene *scene = values->RequireEngineType<GamecodeScene>(0, "Scene");
    GamecodeScene *ref = (values->size() > 1) ? values->RequireEngineType<GamecodeScene>(1, "Scene") : nullptr;
	Scene::InsertSceneAfter(scene, ref);
	childEngineObjects.erase(scene);

	return GamecodeValue();
}

void GamecodeScene::Destruct()
{
	delete this;
}

//-----------------------------------------------------------------------------
// ParentGamecodeEngineObject
//-----------------------------------------------------------------------------

void GamecodeScene::RebuildChildObjectList()
{
	// TODO: Here we need better access to the child objects.
	// Maybe we keep better track of them when we add/remove them, with our own
	// map/list/whatever.

	ClearChildObjects();
	if (positionWrapper != nullptr)
	{
		AddChildObject(positionWrapper);
	}
	for (auto it = childEngineObjects.begin(); it != childEngineObjects.end(); ++it)
	{
		AddChildObject(it->second);
	}
}

//-----------------------------------------------------------------------------
// Static bits and bobs
//-----------------------------------------------------------------------------

IGamecodeEngineObject* GamecodeScene::Construct(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	return new GamecodeScene();
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
