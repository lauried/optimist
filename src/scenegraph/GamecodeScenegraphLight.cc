/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "scenegraph/GamecodeScenegraphLight.hh"
#include "gamecode/IGamecode.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

GamecodeScenegraphLight::~GamecodeScenegraphLight()
{
}

//-----------------------------------------------------------------------------
// IGamecodeEngineObject
//-----------------------------------------------------------------------------

void GamecodeScenegraphLight::SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode)
{
	if (!key.compare("type"))
	{
		std::string valStr = val.GetString();
		if (!valStr.compare("ambient"))
		{
			type = T_Ambient;
			return;
		}
		if (!valStr.compare("directional"))
		{
			type = T_Directional;
			return;
		}
		if (!valStr.compare("point"))
		{
			type = T_Point;
			return;
		}
		throw gamecode_exception("type must be \"ambient\", \"directional\" or \"point\"");
	}

	if (!key.compare("attenuation"))
	{
		std::string valStr = val.GetString();
		if (!valStr.compare("none"))
		{
			attenuation = A_None;
			return;
		}
		if (!valStr.compare("inverse"))
		{
			attenuation = A_Inverse;
			return;
		}
		if (!valStr.compare("invsquare"))
		{
			attenuation = A_InvSquare;
			return;
		}
		throw gamecode_exception("attenuation must be \"none\", \"inverse\" or \"invsquare\"");
	}

	if (!key.compare("brightness"))
	{
		brightness = val.GetFloat();
		return;
	}

	throw gamecode_exception("Unknown or read-only property");
}

GamecodeValue GamecodeScenegraphLight::GetProperty(std::string key, IGamecode *gamecode)
{
	if (!key.compare("type"))
	{
		switch (type)
		{
		case T_Ambient:
			return gamecode->CreateString("ambient");
		case T_Directional:
			return gamecode->CreateString("directional");
		case T_Point:
			return gamecode->CreateString("point");
		}
		throw std::logic_error("Unhandled GamecodeScenegraphLight::Type value");
	}

	if (!key.compare("position"))
	{
		if (positionWrapper == nullptr)
		{
			positionWrapper = new GamecodeVector(&position);
			AddChildObject(positionWrapper);
		}
		return GamecodeValue(positionWrapper);
	}

	if (!key.compare("colour"))
	{
		if (colourWrapper == nullptr)
		{
			colourWrapper = new GamecodeVector(&colour);
			AddChildObject(colourWrapper);
		}
		return GamecodeValue(colourWrapper);
	}

	if (!key.compare("brightness"))
	{
		return GamecodeValue(brightness);
	}

	throw gamecode_exception("Unknown property");
}

void GamecodeScenegraphLight::Destruct()
{
	delete this;
}

//-----------------------------------------------------------------------------
// Static Stuff
//-----------------------------------------------------------------------------

IGamecodeEngineObject* GamecodeScenegraphLight::Construct(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	return new GamecodeScenegraphLight();
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
