/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "scenegraph/SimpleScenegraphRenderer.hh"
#include <stdexcept>
#include "engine/Engine.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

SimpleScenegraphRenderer::SimpleScenegraphRenderer(IPlatform *platform, Renderer *renderer)
	: platform(platform)
	, renderer(renderer)
	, globalSaturation(1.0f)
{
}


//-----------------------------------------------------------------------------
// GammaTable
//-----------------------------------------------------------------------------

class GammaTable {
public:
	GammaTable() { SetGamma(2.2f); }
	GammaTable(float g) { SetGamma(g); }
	void SetGamma(float g)
	{
		float ig = 1.0f / g;
		for (int i = 0; i < 256; ++i)
		{
			table[i] = pow((float)i / 255.0f, g);
			invTable[i] = pow((float)i / 255.0f, ig);
		}
	}
	float Lookup(float f)
	{
		uint8_t v = f * 255.0f;
		return table[v];
	}
	float LookupInverse(float f)
	{
		uint8_t v = f * 255.0f;
		return invTable[v];
	}
private:
	float table[256];
	float invTable[256];
};

//-----------------------------------------------------------------------------
// SimpleScenegraphRenderer
//-----------------------------------------------------------------------------

void SimpleScenegraphRenderer::SetCamera(OrientationF& orientation, float fovX, float fovY, CameraF::ProjectionType projection)
{
	camera.Set(fovX, fovY, orientation, projection);
}

void SimpleScenegraphRenderer::SetCamera(OrientationF& orientation, float left, float top, float right, float bottom, CameraF::ProjectionType projection)
{
	camera.Set(left, top, right, bottom, orientation, projection);
}

void SimpleScenegraphRenderer::DrawScene(Scene *scene)
{
	// TODO: Allow externally set viewport.
	Vector2I viewportPos(0, 0);
	Vector2I viewportSize = platform->GetScreenSize();

	renderer->PushViewport(viewportPos, viewportSize);

	// TODO: Automatically calculate near and far planes.
	if (camera.projection == CameraF::Perspective)
	{
		camera.nearPlane = 0.125f;
		camera.farPlane = 8192.0f;
	}
	else
	{
		camera.nearPlane = -512.0f;
		camera.farPlane = 512.0f;
	}

	SetProjection(camera);

	std::list<Scene*> renderedScenes;
	RecursiveDrawScene(scene, &renderedScenes);

	renderer->PopViewport();
}

void SimpleScenegraphRenderer::RecursiveDrawScene(Scene *scene, std::list<Scene*> *renderedScenes)
{
	for (auto it = renderedScenes->begin(); it != renderedScenes->end(); ++it)
	{
		if (*it == scene)
		{
			throw std::logic_error("Loop in scenegraph detected");
		}
	}
	renderedScenes->push_back(scene);

	// Draw child scenes.
	for (auto it = scene->scenes.begin(); it != scene->scenes.end(); ++it)
	{
		Scene *nestedScene = *it;

		// push
		cameraStack.push(camera);
		if (nestedScene->originFollowsCamera)
		{
			camera.orientation.origin.Set(0, 0, 0);
		}
		camera.orientation.ToExternalSpaceOf(nestedScene->position);
		// render
        RecursiveDrawScene(nestedScene, renderedScenes);
        // pop
		camera = cameraStack.top();
		cameraStack.pop();
	}

	renderer->SetCameraPosition(&(camera.orientation));
	InnerDrawScene(scene);

	renderedScenes->pop_back();
}

void SimpleScenegraphRenderer::InnerDrawScene(Scene *scene)
{
	// Clear buffers.
	int bufferFlags = 0;
	if (scene->clearDepth)
	{
		bufferFlags |= IRenderCore::DepthBuffer;
	}
	if (scene->clearColour)
	{
		bufferFlags |= IRenderCore::ColourBuffer;
	}
	if (bufferFlags)
	{
		renderer->ClearBuffer(bufferFlags);
	}

	renderer->SetSaturation(scene->saturation);

	// Set up scene info.
	CameraF sceneCamera(camera);
	FrustumF frustum(sceneCamera);
	SceneInfo sceneInfo;
	sceneInfo.camera = &sceneCamera;
	sceneInfo.frustum = &frustum;
	sceneInfo.viewportMin.Set(0, 0);
	sceneInfo.viewportSize.Set(platform->GetScreenSize());

	// Draw objects.
	Material defaultMaterial;
	std::vector<MeshInstance*> meshInstances;
	for (auto it = scene->objects.begin(); it != scene->objects.end(); ++it)
	{
		it->second.object->SetResources(&sceneInfo, renderer);
		it->second.object->GetMeshes(&sceneInfo, &meshInstances);
		for (int i = 0; i < meshInstances.size(); i += 1)
		{
			const Material *material = meshInstances[i]->material;
			if (material == nullptr)
			{
				material = &defaultMaterial;
			}
			DrawMesh(scene, meshInstances[i]->mesh, material, *meshInstances[i]->orientation);
		}
		meshInstances.resize(0);
	}
}

struct DirectionalLight {
	Vector3F normal;
	Vector3F colour;
};

void SimpleScenegraphRenderer::DrawMesh(Scene *scene, MeshBuilder *mesh, const Material *material, const OrientationF &orientation, float meshScale)
{
	renderer->PushModelTranslation(&orientation);

	renderer->SetMaterial(*material);

	bool fullbright =
		(material->descriptor != nullptr && material->descriptor->lightMode == MaterialDescriptor::LightFullbright)
		|| (mesh->fullbright);

	float lightMultiply = 1;
	float lightAdd = 0;
	if (scene != nullptr && scene->rearShading)
	{
		lightMultiply = 0.5;
		lightAdd = 0.5;
	}

	if (fullbright)
	{
		renderer->SetLightingFull();
		if (scene->additiveBlend)
		{
			renderer->SetBlend(IRenderCore::BlendAdditive);
		}
		else
		{
			renderer->SetBlend(IRenderCore::BlendAlpha);
		}
		renderer->DrawMesh(*mesh);
	}
	else
	{
		if (scene->additiveBlend)
		{
			renderer->SetColourEnabled(false);
			renderer->SetLightingOff();
			renderer->SetBlend(IRenderCore::BlendAdditive);
			renderer->DrawMesh(*mesh);
			renderer->SetColourEnabled(true);
		}
		else
		{
			renderer->SetLightingOff();
			renderer->SetBlend(IRenderCore::BlendAlpha);
			renderer->DrawMesh(*mesh);
		}

		renderer->SetDepthEnabled(false);
		renderer->SetBlend(IRenderCore::BlendAdditive);

		for (auto it = scene->lights.begin(); it != scene->lights.end(); ++it)
		{
			SceneLight *sl = (*it);
			Vector3F normal, colour;

			switch (sl->type)
			{
			case SceneLight::T_Ambient:
				colour = sl->colour * sl->brightness;
				renderer->SetLighting(normal, colour, 0, 1);
				break;
			case SceneLight::T_Directional:
				normal = orientation.DirectionToInternalSpace(sl->position).GetNormalized();
				colour = sl->colour * sl->brightness;
				renderer->SetLighting(normal, colour, lightMultiply, lightAdd);
				break;
			case SceneLight::T_Point:
				// TODO: To work in the main scene with small lights, we need
				// to support per-vertex calculation of the direction to the
				// light.
				normal = orientation.DirectionToInternalSpace(sl->position - orientation.origin).GetNormalized();
				colour = sl->colour * sl->brightness;
				renderer->SetLighting(normal, colour, lightMultiply, lightAdd);
				break;
			}

			renderer->DrawMesh(*mesh);
		}

		renderer->SetDepthEnabled(true);
	}

	renderer->SetLightingFull();
	renderer->SetBlend(IRenderCore::BlendAlpha);

	renderer->PopModelTranslation();
}

void SimpleScenegraphRenderer::SetProjection(const CameraF &cam)
{
	float l, t, r, b;
	if (cam.fovX == 0 && cam.fovY == 0)
	{
		l = cam.left;
		t = cam.top;
		r = cam.right;
		b = cam.bottom;
	}
	else
	{
		l = -cam.fovX;
		t = cam.fovY;
		r = cam.fovX;
		b = -cam.fovY;
	}

	if (camera.projection == CameraF::Perspective)
	{
		renderer->SetPerspectiveProjection(l, t, r, b, cam.nearPlane, cam.farPlane);
		return;
	}

	if (camera.projection == CameraF::Orthographic)
	{
		renderer->SetOrthographicProjection(l, t, r, b, cam.nearPlane, cam.farPlane);
		return;
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
