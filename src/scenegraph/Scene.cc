/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "scenegraph/Scene.hh"
#include "engine/Engine.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Scene
//-----------------------------------------------------------------------------

Scene::Scene()
	: clearDepth(true)
	, clearColour(false)
	, originFollowsCamera(false)
	, additiveBlend(false)
	, rearShading(false)
	, saturation(1.0f)
{
}

void Scene::InsertSceneBefore(Scene *scene, Scene *ref)
{
	if (ref == nullptr)
	{
		scenes.push_front(scene);
		return;
	}
	for (auto it = scenes.begin(); it != scenes.end(); ++it)
	{
		if (*it == ref)
		{
			scenes.insert(it, scene);
			return;
		}
	}
	scenes.push_back(scene);
}

void Scene::InsertSceneAfter(Scene *scene, Scene *ref)
{
	if (ref == nullptr)
	{
		scenes.push_back(scene);
		return;
	}
	for (auto it = scenes.begin(); it != scenes.end(); ++it)
	{
		if (*it == ref)
		{
			++it;
			if (it == scenes.end())
			{
				scenes.push_back(scene);
				return;
			}
			scenes.insert(it, scene);
			return;
		}
	}
	scenes.push_front(scene);
}

void Scene::RemoveScene(Scene *scene)
{
	for (auto it = scenes.begin(); it != scenes.end(); ++it)
	{
		if (*it == scene)
		{
			scenes.erase(it);
			return;
		}
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
