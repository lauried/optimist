/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gamecode/IGamecodeEngineObject.hh"
#include "scenegraph/SimpleScenegraphRenderer.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class GamecodeScenegraphRenderer : public IGamecodeEngineObject {
public:
	GamecodeScenegraphRenderer(IPlatform *platform, Renderer *renderer)
		: renderer(platform, renderer) {}
	~GamecodeScenegraphRenderer();

	// IGamecodeEngineObject
	void SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode);
	GamecodeValue GetProperty(std::string key, IGamecode *gamecode);
	void Destruct();

	// Gamecode Engine Methods
	GamecodeValue SetCamera(GamecodeParameterList *parameters, IGamecode *gamecode);
	GamecodeValue RenderScene(GamecodeParameterList *parameters, IGamecode *gamecode);

	// Static stuff
	static IGamecodeEngineObject* Construct(GamecodeParameterList *parameters, IGamecode *gamecode);

private:
	SimpleScenegraphRenderer renderer;
	CameraF camera;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
