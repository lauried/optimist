/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "scenegraph/GamecodeScenegraphRenderer.hh"
#include "scenegraph/GamecodeScene.hh"
#include "library/geometry/Camera.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// IGamecodeEngineObject
//-----------------------------------------------------------------------------

GamecodeScenegraphRenderer::~GamecodeScenegraphRenderer()
{
}

void GamecodeScenegraphRenderer::SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode)
{
	if (!key.compare("globalSaturation"))
	{
		renderer.globalSaturation = val.GetFloat();
		return;
	}
	throw gamecode_exception("Unknown property");
}

GamecodeValue GamecodeScenegraphRenderer::GetProperty(std::string key, IGamecode *gamecode)
{
	if (!key.compare("globalSaturation"))
	{
		return GamecodeValue(renderer.globalSaturation);
	}
	throw gamecode_exception("Unknown property");
}

void GamecodeScenegraphRenderer::Destruct()
{
	delete this;
}

GamecodeValue GamecodeScenegraphRenderer::SetCamera(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(3, 6);

	GamecodeOrientation *orientation = parameters->RequireEngineType<GamecodeOrientation>(0, "Orientation");

	CameraF::ProjectionType projectionType;
	std::string projection = "perspective";
	if (parameters->size() == 4)
	{
		projection = parameters->RequireType(3, GamecodeValue::T_String).GetString();
	}
	else if (parameters->size() == 6)
	{
		projection = parameters->RequireType(5, GamecodeValue::T_String).GetString();
	}
	if (!projection.compare("orthographic"))
	{
		projectionType = CameraF::Orthographic;
	}
	else if (!projection.compare("perspective"))
	{
		projectionType = CameraF::Perspective;
	}

	if (parameters->Match(0, {
		GamecodeValue::T_Engine,
		GamecodeValue::T_Number,
		GamecodeValue::T_Number,
		}))
	{
		float fovX = parameters->at(1).GetFloat();
		float fovY = parameters->at(2).GetFloat();
		renderer.SetCamera(*(orientation->GetObject()), fovX, fovY, projectionType);
	}
	else if (parameters->Match(0, {
		GamecodeValue::T_Engine,
		GamecodeValue::T_Number,
		GamecodeValue::T_Number,
		GamecodeValue::T_Number,
		GamecodeValue::T_Number,
		}))
	{
		float left   = parameters->at(1).GetFloat();
		float top    = parameters->at(2).GetFloat();
		float right  = parameters->at(3).GetFloat();
		float bottom = parameters->at(4).GetFloat();
		renderer.SetCamera(*(orientation->GetObject()), left, top, right, bottom, projectionType);
	}
	else
	{
		throw gamecode_exception("Incorrect parameters, expects: (Orientation, fovX, fovY [, projection]) or (Orientation, left, top, right, bottom [, projection])");
	}

	return GamecodeValue();
}

GamecodeValue GamecodeScenegraphRenderer::RenderScene(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	GamecodeScene *scene = parameters->RequireEngineType<GamecodeScene>(0, "Scene");
	renderer.DrawScene(scene);
	return GamecodeValue();
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
