/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <vector>
#include "library/geometry/Vector3.hh"
#include "library/geometry/Orientation.hh"
#include "library/geometry/Camera.hh"
#include "library/geometry/Frustum.hh"
#include "renderer/MeshBuilder.hh"
#include "renderer/Material.hh"
#include "renderer/IRenderResources.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// An instance of a mesh at a particular position in the scene.
struct MeshInstance {
	MeshInstance() : mesh(nullptr), material(nullptr), orientation(nullptr), lightMode(1) {}
	MeshBuilder *mesh;
	const Material *material;
	OrientationF *orientation; // final orientation of the mesh in scene.
	int lightMode;
};

struct SceneInfo {
	SceneInfo() : camera(nullptr), frustum(nullptr) {}
	CameraF *camera;
	FrustumF *frustum;
	Vector2I viewportMin, viewportSize;
	double gameTime;
	double programTime;
};

/**
 * An object which provides meshes for a scene, based on its internal
 * parameters and data about the scene (such as camera position, etc.)
 */
class IRenderMeshProvider {
public:
	virtual ~IRenderMeshProvider();

	/**
	 * Allows the mesh provider to request any materials it needs by passing
	 * material descriptors to the resources object.
	 * The current scene settings are provided in case we want to change
	 * textures for different levels of detail.
	 * It's a separate step from getting meshes so that the scenegraph renderer
	 * could, if it wanted, load resources in a separate step.
	 */
	virtual void SetResources(const SceneInfo *scene, IRenderResources *resources) = 0;

	/**
	 * Here the mesh provider gives the required meshes to draw itself given
	 * the current scene settings.
	 */
	virtual void GetMeshes(const SceneInfo *scene, std::vector<MeshInstance*>* meshes) = 0;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
