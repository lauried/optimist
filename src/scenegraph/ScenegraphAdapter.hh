/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <map>
#include <unordered_set>
#include "gamecode/IGamecode.hh"
#include "renderer/Renderer.hh"
#include "platform/IPlatform.hh"
#include "resources/ResourceManager.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class GamecodeScene;

/**
 * Implements a scenegraph interface for the gamecode.
 */
class ScenegraphAdapter : public IGamecodeAdapter {
public:
	ScenegraphAdapter(IPlatform *platform, Renderer *renderer, ResourceManager *resources) :
		platform(platform),
		renderer(renderer),
		resources(resources)
		{}
	~ScenegraphAdapter();

	// IGamecodeAdapter
	virtual const std::string& GetName();
	virtual std::map<std::string, IGamecodeAdapter::AdapterMethod> GetGlobalFunctions();
	virtual void OnRegister(IGamecode *gamecode);

private:
	IPlatform *platform;
	Renderer *renderer;
	ResourceManager *resources;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
