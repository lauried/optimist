/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#ifndef _SCENEGRAPH_SCENEGRAPHOBJECTTYPE_HH_
#define _SCENEGRAPH_SCENEGRAPHOBJECTTYPE_HH_
//-----------------------------------------------------------------------------
#include "scenegraph/IScenegraphObject.hh"
#include "gamecode/IGamecode.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * A ScenegraphObjectType is a class for instantiating a concrete
 * ScenegraphObject.
 */
class IScenegraphObjectType {
public:
	virtual void Register(IGamecode *gamecode) = 0;
};

/**
 * Template for instantiating a concrete IScenegraphObject implementation.
 * The parameter class requires the methods:
 * std::string Name()
 * std::vector<GamecodeEngineObjectMethod<T>> Methods()
 * IGamecodeEngineObject* Construct(GamecodeParameterList *parameters)
 */
template <class T>
class ScenegraphObjectType : public IScenegraphObjectType {
public:
	void Register(IGamecode *gamecode)
	{
		GamecodeEngineObjectMethodList methods(*T::Methods());
		gamecode->RegisterEngineClass<T>(T::Name(), T::Construct, &methods);
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
#endif // _SCENEGRAPH_SCENEGRAPHOBJECTTYPE_HH_
//-----------------------------------------------------------------------------

