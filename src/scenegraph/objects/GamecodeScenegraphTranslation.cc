/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "scenegraph/objects/GamecodeScenegraphTranslation.hh"
#include "gamecode/ModularEngineTypes.hh"
#include "scenegraph/ScenegraphAdapter.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

GamecodeScenegraphTranslation::~GamecodeScenegraphTranslation()
{
}

void GamecodeScenegraphTranslation::SetResources(const SceneInfo *scene, IRenderResources *resources)
{
	// TODO: We need to translate the camera inversely with our own scene info.
	wrappedObject->SetResources(scene, resources);
}

void GamecodeScenegraphTranslation::GetMeshes(const SceneInfo *scene, std::vector<MeshInstance*>* meshes)
{
	int start = meshes->size();
	// TODO: We need to translate the camera inversely with our own scene info.
	wrappedObject->GetMeshes(scene, meshes);
	for (int i = start; i < meshes->size(); i += 1)
	{
		(*meshes)[i]->orientation->ToExternalSpaceOf(translation);
	}
}

//-----------------------------------------------------------------------------
// Static
//-----------------------------------------------------------------------------

IGamecodeEngineObject* GamecodeScenegraphTranslation::Construct(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	IRenderMeshProvider *object = parameters->RequireEngineType<IRenderMeshProvider>(0, "ScenegraphObject");
	return new GamecodeScenegraphTranslation(object);
}

void RegisterGamecodeScenegraphTranslation(IGamecode *gamecode, ResourceManager *resources)
{
	gamecode->RegisterEngineType("ScenegraphTranslation", gamecode->CreateEngineType<GamecodeScenegraphTranslation>()
		->Constructor(&GamecodeScenegraphTranslation::Construct)
		->AddObject("translation", &GamecodeScenegraphTranslation::translation, &GamecodeScenegraphTranslation::translationWrapper)
	);
}

MODULAR_ENGINE_TYPE(ScenegraphAdapter, RegisterGamecodeScenegraphTranslation)

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
