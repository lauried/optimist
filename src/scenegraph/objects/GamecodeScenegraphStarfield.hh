/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "scenegraph/IRenderMeshProvider.hh"
#include "gamecode/IGamecodeEngineObject.hh"
#include "scenegraph/objects/StarfieldRenderMeshProvider.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class GamecodeScenegraphStarfield : public IRenderMeshProvider, public IGamecodeEngineObject {
public:
	GamecodeScenegraphStarfield() {}
	~GamecodeScenegraphStarfield() {}

	// IRenderMeshProvider
	void SetResources(const SceneInfo *scene, IRenderResources *resources) { starfield.SetResources(scene, resources); }
	void GetMeshes(const SceneInfo *scene, std::vector<MeshInstance*>* meshes) { starfield.GetMeshes(scene, meshes); }

	// IGamecodeEngineObject
	void SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode);
	GamecodeValue GetProperty(std::string key, IGamecode *gamecode);
	void SetIndex(int i, GamecodeValue val, IGamecode *gamecode);
	GamecodeValue GetIndex(int i, IGamecode *gamecode);
	void Destruct();

	// Required by ScenegraphObjectType
	static IGamecodeEngineObject* Construct(GamecodeParameterList *parameters, IGamecode *gamecode);

	// TODO: Engine methods

private:
	StarfieldRenderMeshProvider starfield;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
