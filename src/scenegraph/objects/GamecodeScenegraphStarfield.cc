/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "scenegraph/objects/GamecodeScenegraphStarfield.hh"
#include "gamecode/ModularEngineTypes.hh"
#include "scenegraph/ScenegraphAdapter.hh"
#include "gamecode/ModularEngineTypes.hh"
#include "scenegraph/ScenegraphAdapter.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// IGamecodeEngineObject
//-----------------------------------------------------------------------------

void GamecodeScenegraphStarfield::SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode)
{
	if (!key.compare("brightness"))
	{
		starfield.SetBrightness(val.GetFloat());
		return;
	}
	throw gamecode_exception("No such property");
}

GamecodeValue GamecodeScenegraphStarfield::GetProperty(std::string key, IGamecode *gamecode)
{
	if (!key.compare("brightness"))
	{
		return GamecodeValue(starfield.GetBrightness());
	}
	throw gamecode_exception("No such property, or read-only");
}

void GamecodeScenegraphStarfield::SetIndex(int i, GamecodeValue val, IGamecode *gamecode)
{
	throw gamecode_exception("No Such Index");
}

GamecodeValue GamecodeScenegraphStarfield::GetIndex(int i, IGamecode *gamecode)
{
	throw gamecode_exception("No Such Index");
}

void GamecodeScenegraphStarfield::Destruct()
{
	delete this;
}

//-----------------------------------------------------------------------------
// ScenegraphObjectType requirements
//-----------------------------------------------------------------------------

IGamecodeEngineObject* GamecodeScenegraphStarfield::Construct(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	return new GamecodeScenegraphStarfield();
}

void RegisterGamecodeScenegraphStarfield(IGamecode *gamecode, ResourceManager *resources)
{
	gamecode->RegisterEngineType("ScenegraphStarfield", gamecode->CreateEngineType<GamecodeScenegraphStarfield>()
		->Constructor(&GamecodeScenegraphStarfield::Construct)
		->AddProperty("brightness")
	);
}

MODULAR_ENGINE_TYPE(ScenegraphAdapter, RegisterGamecodeScenegraphStarfield)

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
