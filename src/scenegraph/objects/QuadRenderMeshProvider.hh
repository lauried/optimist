/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "scenegraph/IRenderMeshProvider.hh"
#include "resources/ResourceManager.hh"
#include "resources/ImageResource.hh"
#include "library/graphics/Color.hh"
#include "library/geometry/Vector2.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class QuadRenderMeshProvider : public IRenderMeshProvider {
public:
	QuadRenderMeshProvider() : mesh(nullptr), width(0), height(0), rebuild(false), hasImage(false)
	{
		colour.Set(1, 1, 1, 1);
	}

	void SetImage(std::string resource);
	void ClearImage();
	// TODO: Also border/repeat mode.

	void SetColour(Color colour);
	void SetWidth(float width);
	void SetHeight(float height);
	float GetWidth() { return width; }
	float GetHeight() { return height; }

	// IRenderMeshProvider
	~QuadRenderMeshProvider();
	void SetResources(const SceneInfo *scene, IRenderResources *resources);
	void GetMeshes(const SceneInfo *scene, std::vector<MeshInstance*>* meshes);

	OrientationF orientation;

private:
	float width, height;
	Color colour;

	MeshBuilder *mesh;
	MaterialDescriptor material;
	MeshInstance meshInstance;

	bool rebuild;
	bool hasImage;

	void BuildMesh();
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
