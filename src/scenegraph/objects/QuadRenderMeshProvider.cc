/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "scenegraph/objects/QuadRenderMeshProvider.hh"
#include <functional>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

QuadRenderMeshProvider::~QuadRenderMeshProvider()
{
}

void QuadRenderMeshProvider::SetResources(const SceneInfo *scene, IRenderResources *resources)
{
	if (rebuild)
	{
		BuildMesh();
		rebuild = false;
	}

	if (mesh && hasImage)
	{
		meshInstance.material = resources->GetMaterial(material);
	}
}

void QuadRenderMeshProvider::GetMeshes(const SceneInfo *scene, std::vector<MeshInstance*>* meshes)
{
	if (rebuild)
	{
		BuildMesh();
		rebuild = false;
	}

	if (mesh)
	{
		meshInstance.mesh = mesh;
		meshInstance.orientation = &orientation;
		meshes->push_back(&meshInstance);
	}
}

void QuadRenderMeshProvider::SetImage(std::string resource)
{
	material.texture.resource = resource;
	rebuild = true;
	hasImage = true;
}

void QuadRenderMeshProvider::ClearImage()
{
	material.texture.resource = "";
	rebuild = true;
	hasImage = false;
}

void QuadRenderMeshProvider::SetColour(Color colour)
{
	this->colour = colour;
	rebuild = true;
}

void QuadRenderMeshProvider::SetWidth(float width)
{
	this->width = width;
	rebuild = true;
}

void QuadRenderMeshProvider::SetHeight(float height)
{
	this->height = height;
	rebuild = true;
}

void QuadRenderMeshProvider::BuildMesh()
{
	delete mesh;

	mesh = new MeshBuilder();
	mesh->type = MeshBuilder::QUAD;
	mesh->AddAttribute(MeshBuilder::VERTEX, 3);
	mesh->AddAttribute(MeshBuilder::COLOUR, 4);

	float vertex[3] = {0, 0, 0};
	float tc[2] = {0, 0};
	float tcmin[2] = {0, 0};
	float tcmax[2] = {0, 0};
	float *tcp = nullptr;

	if (hasImage)
	{
		mesh->AddAttribute(MeshBuilder::TEXCOORD, 2);
		tcmin[0] = 0;
		tcmin[1] = 0;
		tcmax[0] = 1;
		tcmax[1] = 1;

		// TODO: Here we can pre-init our texcoords to support offset and tiling.
		// There are two modes - either scale the texture to fit the image, or
		// control the scale of the image as rendered and allowing it to tile.

		// We still only render one mesh here. We can use an array of this
		// object in the ScenegraphObject if we want to support borders.

		// It might be worth implementing 'sprite' support in TexInfo, whereby
		// the actual loaded texture can be specified as a particular part of
		// an actual image. This way we can still load directly from a single
		// resource.

		// Anything that used sprites would already have knowledge of the image
		// dimensions.
		// There would be some difficulty uploading and performing the
		// upload callback efficiently - the callback would need to be given
		// the final callback image.

		tcp = tc;
	}

	mesh->AllocateQuads(1);

	tc[0] = tcmin[0];
	tc[1] = tcmin[1];

	mesh->AddVertex(vertex, nullptr, colour.rgba, tcp);
	vertex[1] += height;
	tc[1] = tcmax[1];
	mesh->AddVertex(vertex, nullptr, colour.rgba, tcp);
	vertex[0] += width;
	tc[0] = tcmax[0];
	mesh->AddVertex(vertex, nullptr, colour.rgba, tcp);
	vertex[1] -= height;
	tc[1] = tcmin[1];
	mesh->AddVertex(vertex, nullptr, colour.rgba, tcp);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
