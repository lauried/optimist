/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "scenegraph/objects/GamecodeScenegraphModel.hh"
#include "resources/ResourceManager.hh"
#include "gamecode/GamecodeException.hh"
#include "gamecode/ModularEngineTypes.hh"
#include "scenegraph/ScenegraphAdapter.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

GamecodeScenegraphModel::~GamecodeScenegraphModel()
{
	delete meshProvider;
}

//-----------------------------------------------------------------------------
// IRenderMeshProvider
//-----------------------------------------------------------------------------

void GamecodeScenegraphModel::SetResources(const SceneInfo *scene, IRenderResources *resources)
{
	if (meshProvider == nullptr)
	{
		meshProvider = new ModelRenderMeshProvider();
	}
	meshProvider->SetResources(scene, resources);
}

void GamecodeScenegraphModel::GetMeshes(const SceneInfo *scene, std::vector<MeshInstance*>* meshes)
{
	if (meshProvider == nullptr)
	{
		meshProvider = new ModelRenderMeshProvider();
	}

	if (modelChanged)
	{
		meshProvider->SetModel(resources->GetResource<ModelResource>(model));
	}
	if (modelChanged || animationsChanged)
	{
		for (auto it = animations.begin(); it != animations.end(); ++it)
		{
			meshProvider->AddAnimation(resources->GetResource<BvhResource>(*it));
		}
	}
	if (modelChanged || animationsChanged || currentAnimationChanged)
	{
		meshProvider->SetCurrentAnimation(currentAnimation);
	}

	meshProvider->position = position;
	meshProvider->GetMeshes(scene, meshes);
}

//-----------------------------------------------------------------------------
// IGamecodeEngineObject
//-----------------------------------------------------------------------------

// Set the model.

// Set animations.
// How to do this? We'd want to set it with an array and keep the array in
// memory even if nothing else contains it.
// This means we'd need some IGamecodeEngineObject interface code for GC when
// engine objects contain references to other objects.
// Alternately, given the usage is mostly to set an array of animations, we can
// just copy the table and make note of that in the interface.

// Set its orientation.

// Set its current animation (and position through the animation, or time that
// the animation would have started.)

// We ought to note when models/animations are changed but delay the update to
// the GetMeshes call. This allows us to update model and animations in any
// order, without reacting to an incorrect state where the set of animations
// aren't necessarily related to the model.

void GamecodeScenegraphModel::SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode)
{
	if (!key.compare("orientation"))
	{
		throw gamecode_exception("Property cannot be written, but it can be acted upon");
	}
	else if (!key.compare("model"))
	{
		model = val.GetString();
		modelChanged = true;
	}
	else if (!key.compare("animations"))
	{
		animations.clear();
		if (!val.IsTable())
		{
			throw gamecode_exception("Not a table");
		}
		// TODO: Also support arrays when they're implemented.
		for (int i = 0; i < val.GetTable()->NumKeys(); i += 1)
		{
			animations.push_back(val.GetTable()->GetValue(i).GetString());
		}
		animationsChanged = true;
	}
	else if (!key.compare("currentAnimation"))
	{
		currentAnimation = val.GetInt();
		currentAnimationChanged = true;
	}
	else
	{
		throw gamecode_exception("No such property");
	}
}

GamecodeValue GamecodeScenegraphModel::GetProperty(std::string key, IGamecode *gamecode)
{
	if (!key.compare("orientation"))
	{
		if (positionWrapper == nullptr)
		{
			positionWrapper = new GamecodeOrientation(&position);
			AddChildObject(positionWrapper);
		}
		return GamecodeValue(positionWrapper);
	}
	if (!key.compare("model"))
	{
		return GamecodeValue(gamecode->CreateString(model.c_str()));
	}
	if (!key.compare("animations"))
	{
		GamecodeValue table = gamecode->CreateTable();
		for (int i = 0; i < animations.size(); i += 1)
		{
			table.GetTable()->Set(i, GamecodeValue(gamecode->CreateString(animations[i].c_str())));
		}
		return table;
	}
	if (!key.compare("currentAnimation"))
	{
		return GamecodeValue(currentAnimation);
	}

	throw gamecode_exception("No such property, or read-only");
}

void GamecodeScenegraphModel::SetIndex(int i, GamecodeValue val, IGamecode *gamecode)
{
	throw gamecode_exception("No Such Index");
}

GamecodeValue GamecodeScenegraphModel::GetIndex(int i, IGamecode *gamecode)
{
	throw gamecode_exception("No Such Index");
}

void GamecodeScenegraphModel::Destruct()
{
	delete this;
}

//-----------------------------------------------------------------------------
// ScenegraphObjectType requirements
//-----------------------------------------------------------------------------

void RegisterGamecodeScenegraphModel(IGamecode *gamecode, ResourceManager *resources)
{
	gamecode->RegisterEngineType("ScenegraphModel", gamecode->CreateEngineType<GamecodeScenegraphModel>()
		->Constructor([resources](GamecodeParameterList *parameters, IGamecode *gamecode){
			return new GamecodeScenegraphModel(resources);
		})
		->AddProperty("orientation")
		->AddProperty("model")
		->AddProperty("animation")
		->AddProperty("currentAnimation")
	);
}

MODULAR_ENGINE_TYPE(ScenegraphAdapter, RegisterGamecodeScenegraphModel)

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
