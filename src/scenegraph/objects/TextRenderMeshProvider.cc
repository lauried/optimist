/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "scenegraph/objects/TextRenderMeshProvider.hh"
#include "library/text/String.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

void TextRenderMeshProvider::SetFont(ResourceManager::ResourcePointer<FontResource> font)
{
	(*this).font = font;
	height = 0;
	width = 0;
}

void TextRenderMeshProvider::SetText(std::string text)
{
	originalText = text;
	height = 0;
	width = 0;
}

void TextRenderMeshProvider::SetWrapWidth(float width)
{
	(*this).wrapWidth = width;
	height = 0;
	width = 0;
}

float TextRenderMeshProvider::GetHeight()
{
	if (font.GetState() == ResourceManager::LOADED && height == 0)
	{
		Update();
	}
	return height;
}

float TextRenderMeshProvider::GetWidth()
{
	if (font.GetState() == ResourceManager::LOADED && height == 0)
	{
		Update();
	}
	return width;
}

TextRenderMeshProvider::~TextRenderMeshProvider()
{
}

void TextRenderMeshProvider::SetResources(const SceneInfo *scene, IRenderResources *resources)
{
	if (font.GetState() != ResourceManager::LOADED)
	{
		return;
	}
	if (height == 0)
	{
		Update();
	}

	for (int i = 0; i < meshInstances.size(); i += 1)
	{
		if (meshInstances[i].material == nullptr)
		{
			meshInstances[i].material = resources->GetMaterial(materials[i]);
		}
	}
}

void TextRenderMeshProvider::GetMeshes(const SceneInfo *scene, std::vector<MeshInstance*>* meshes)
{
	if (font.GetState() != ResourceManager::LOADED)
	{
		return;
	}
	if (height == 0)
	{
		Update();
	}

	for (int i = 0; i < meshInstances.size(); i += 1)
	{
		if (this->meshes[i] != nullptr)
		{
			meshes->push_back(&meshInstances[i]);
		}
	}
}

void TextRenderMeshProvider::Update()
{
	IFont *ifont = font.GetResource()->GetFont();

	int numLines = 0;
	int actualWidth = 0;
	wrappedText = IFont::WrapText(originalText, ifont, wrapWidth, &numLines, &actualWidth, IFont::WRAP_FORCE_WORD_BREAK); // TODO: This is broken.
	height = ifont->LineHeight() * numLines;
	width = actualWidth;

	for (int i = 0; i < meshes.size(); i += 1)
	{
		delete meshes[i];
		meshes[i] = nullptr;
	}

	// Calculate the number of glyphs using each image.
	std::vector<int> glyphsPerMesh;
	for (int code = 0, pos = 0, bytesRead = 0; pos < wrappedText.size() && (bytesRead = String::UTF8ToUnicode(wrappedText, pos, &code)) > 0; pos += bytesRead)
	{
		IFont::GlyphInfo *glyph = ifont->GetGlyph(code);
		if (glyph == nullptr)
		{
			continue;
		}
		if (glyphsPerMesh.size() < glyph->imageIndex + 1)
		{
			glyphsPerMesh.resize(glyph->imageIndex + 1, 0);
		}
		glyphsPerMesh[glyph->imageIndex] += 1;
	}

	std::vector<Image*> images;

	meshes.resize(glyphsPerMesh.size());
	materials.resize(glyphsPerMesh.size());
	meshInstances.resize(glyphsPerMesh.size());
	for (int i = 0; i < meshes.size(); i += 1)
	{
		if (glyphsPerMesh[i] > 0)
		{
			meshes[i] = new MeshBuilder();
			meshes[i]->type = MeshBuilder::QUAD;
			meshes[i]->AddAttribute(MeshBuilder::VERTEX, 3);
			meshes[i]->AddAttribute(MeshBuilder::TEXCOORD, 2);
			meshes[i]->AllocateQuads(glyphsPerMesh[i]);
			materials[i].texture.provider = ifont->GetTextureProvider(i);
			//materials[i].lightMode = MaterialDescriptor::LightFullbright; // TODO: Remove to support lighting on text, or make it an option.
			meshInstances[i].mesh = meshes[i];
			meshInstances[i].orientation = &orientation;
		}
	}

	float x = 0, y = 0;
	for (int code = 0, pos = 0, bytesRead = 0; pos < wrappedText.size() && (bytesRead = String::UTF8ToUnicode(wrappedText, pos, &code)) > 0; pos += bytesRead)
	{
		if ((char)code == '\n')
		{
			y += ifont->LineHeight();
			x = 0;
		}

		IFont::GlyphInfo *glyph = ifont->GetGlyph(code);
		if (glyph == nullptr)
		{
			continue;
		}
		MeshBuilder *mesh = meshes[glyph->imageIndex];

		float w = (float)(glyph->s1 - glyph->s0);
		float h = (float)(glyph->t1 - glyph->t0);
		if (w < 0) w *= -1;
		if (h < 0) h *= -1;

		float v[3] = { 0, 0, 0 };
		float tc[2];

		v[0] = x + (float)glyph->px;
		v[1] = y + (float)glyph->py;
		tc[0] = glyph->fs0;
		tc[1] = glyph->ft0;
		mesh->AddVertex(v, nullptr, nullptr, tc);

		v[1] += h;
		tc[1] = glyph->ft1;
		mesh->AddVertex(v, nullptr, nullptr, tc);

		v[0] += w;
		tc[0] = glyph->fs1;
		mesh->AddVertex(v, nullptr, nullptr, tc);

		v[1] -= h;
		tc[1] = glyph->ft0;
		mesh->AddVertex(v, nullptr, nullptr, tc);

		x += glyph->advance;
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
