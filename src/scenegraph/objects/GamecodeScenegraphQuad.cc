/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "scenegraph/objects/GamecodeScenegraphQuad.hh"
#include "gamecode/ModularEngineTypes.hh"
#include "scenegraph/ScenegraphAdapter.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// TODO: Something nice would be to allow us to pass the valid parameter types
// to the engine object descriptor, and be guaranteed them when they're called
// here.

void GamecodeScenegraphQuad::SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode)
{
	if (!key.compare("orientation"))
	{
		throw gamecode_exception("Property cannot be written, but it can be acted upon");
	}
	else
	{
		throw gamecode_exception("No such property");
	}
}

GamecodeValue GamecodeScenegraphQuad::GetProperty(std::string key, IGamecode *gamecode)
{
	if (!key.compare("orientation"))
	{
		if (positionWrapper == nullptr)
		{
			positionWrapper = new GamecodeOrientation(&meshProvider.orientation);
			AddChildObject(positionWrapper);
		}
		return GamecodeValue(positionWrapper);
	}
	throw gamecode_exception("No such property, or read-only");
}

GamecodeValue GamecodeScenegraphQuad::SetColour(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(4);
	float r = parameters->RequireType(0, GamecodeValue::T_Number).GetFloat();
	float g = parameters->RequireType(1, GamecodeValue::T_Number).GetFloat();
	float b = parameters->RequireType(2, GamecodeValue::T_Number).GetFloat();
	float a = parameters->RequireType(3, GamecodeValue::T_Number).GetFloat();
	meshProvider.SetColour(Color(r, g, b, a));
	return GamecodeValue();
}

GamecodeValue GamecodeScenegraphQuad::SetWidth(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	meshProvider.SetWidth(parameters->RequireType(0, GamecodeValue::T_Number).GetFloat());
	return GamecodeValue();
}

GamecodeValue GamecodeScenegraphQuad::SetHeight(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	meshProvider.SetHeight(parameters->RequireType(0, GamecodeValue::T_Number).GetFloat());
	return GamecodeValue();
}

GamecodeValue GamecodeScenegraphQuad::GetWidth(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(0);
	return GamecodeValue(meshProvider.GetWidth());
}

GamecodeValue GamecodeScenegraphQuad::GetHeight(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(0);
	return GamecodeValue(meshProvider.GetWidth());
}

//-----------------------------------------------------------------------------
// Modular Stuff
//-----------------------------------------------------------------------------

IGamecodeEngineObject* GamecodeScenegraphQuad::Construct(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	// TODO: We could accept colour, width and height.
	return new GamecodeScenegraphQuad();
}

void RegisterGamecodeScenegraphQuad(IGamecode *gamecode, ResourceManager *resources)
{
	gamecode->RegisterEngineType("ScenegraphQuad", gamecode->CreateEngineType<GamecodeScenegraphQuad>()
		->Constructor(&GamecodeScenegraphQuad::Construct)
		->AddMethod("setColour", &GamecodeScenegraphQuad::SetColour)
		->AddMethod("setWidth", &GamecodeScenegraphQuad::SetWidth)
		->AddMethod("getWidth", &GamecodeScenegraphQuad::GetWidth)
		->AddMethod("setHeight", &GamecodeScenegraphQuad::SetHeight)
		->AddMethod("getHeight", &GamecodeScenegraphQuad::GetHeight)
		->AddProperty("orientation")
	);
}

MODULAR_ENGINE_TYPE(ScenegraphAdapter, RegisterGamecodeScenegraphQuad)

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
