/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "scenegraph/objects/GamecodeScenegraphText.hh"
#include "gamecode/ModularEngineTypes.hh"
#include "scenegraph/ScenegraphAdapter.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

GamecodeScenegraphText::~GamecodeScenegraphText()
{
}

//void GamecodeScenegraphText::GetMeshes(const SceneInfo *scene, std::vector<MeshInstance*>* meshes)
//{
//	meshProvider.GetMeshes(scene, meshes);
//}

void GamecodeScenegraphText::SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode)
{
	if (!key.compare("font"))
	{
		resourceName = val.GetString();
		ResourceManager::ResourcePointer<FontResource> font = resources->GetResource<FontResource>(resourceName);
		SetFont(font);
	}
	else if (!key.compare("text"))
	{
		SetText(val.GetString());
	}
	else if (!key.compare("wrapWidth"))
	{
		SetWrapWidth(val.GetFloat());
	}
	else if (!key.compare("width"))
	{
		// Do nothing - it's read only.
	}
	else if (!key.compare("height"))
	{
		// Do nothing - it's read-only.
	}
}

GamecodeValue GamecodeScenegraphText::GetProperty(std::string key, IGamecode *gamecode)
{
	if (!key.compare("font"))
	{
		return GamecodeValue(gamecode->CreateString(resourceName.c_str()));
	}
	else if (!key.compare("text"))
	{
		return GamecodeValue(gamecode->CreateString(meshProvider.GetText().c_str()));
	}
	else if (!key.compare("wrapWidth"))
	{
		return GamecodeValue(GetWrapWidth());
	}
	else if (!key.compare("width"))
	{
		return GamecodeValue(GetWidth());
	}
	else if (!key.compare("height"))
	{
		return GamecodeValue(GetHeight());
	}
	return GamecodeValue();
}

void GamecodeScenegraphText::Destruct()
{
	delete this;
}

void RegisterGamecodeScenegraphText(IGamecode *gamecode, ResourceManager *resources)
{
	gamecode->RegisterEngineType("ScenegraphText", gamecode->CreateEngineType<GamecodeScenegraphText>()
		->Constructor([resources](GamecodeParameterList *parameters, IGamecode *gamecode) {
			return new GamecodeScenegraphText(resources);
		})
		->AddObject("orientation", (OrientationF GamecodeScenegraphText::*)&TextRenderMeshProvider::orientation, &GamecodeScenegraphText::orientationWrapper)
		->AddProperty("font")
		->AddProperty("text")
		->AddProperty("wrapWidth")
		->AddProperty("width")
		->AddProperty("height")
	);
}

MODULAR_ENGINE_TYPE(ScenegraphAdapter, RegisterGamecodeScenegraphText)

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
