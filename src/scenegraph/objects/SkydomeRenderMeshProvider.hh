/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "library/geometry/Vector3.hh"
#include "library/geometry/Camera.hh"
#include "library/graphics/Color.hh"
#include "scenegraph/IRenderMeshProvider.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// TODO: Reorganize this so that we can build the meshes and then just
// manipulate their colours.

class SkydomeRenderMeshProvider : public IRenderMeshProvider {
public:
	SkydomeRenderMeshProvider();
	~SkydomeRenderMeshProvider();

	// IRenderMeshProvider
	void SetResources(const SceneInfo *scene, IRenderResources *resources);
	void GetMeshes(const SceneInfo *scene, std::vector<MeshInstance*>* meshes);

	// Own Methods
	void SetSunPosition(Vector3F position);
	void AddLightFrame(std::string s);

private:
	Vector3F sunDirection;
	Color skyColour;

	struct LightFrameData {
		static const int NUM_BANDS = 5;

		// The top band doesn't really have a size, it's the sky colour.
		struct SkyBand {
			float size;
			Color colour;
		};
		SkyBand bands[NUM_BANDS];

		void Blend(const LightFrameData &a, float fracA, const LightFrameData &b)
		{
			float iFrac = 1.0f - fracA;
			for (int i = 0; i < NUM_BANDS; ++i)
			{
				bands[i].colour.Blend(a.bands[i].colour, fracA, b.bands[i].colour);
				bands[i].size = a.bands[i].size * fracA + b.bands[i].size * iFrac;
			}
		}
	};

	struct LightFrame {
		// sunHeight ranges from 1.0 to -1.0, and represents the cosine of the
		// angle from directly overhead, or the z component of the normalized
		// direction to the sun.
		float sunHeight;
		LightFrameData data;
	};

	void ParseLightFrame(LightFrame *frame, std::string s);

	LightFrameData current;
	std::vector<LightFrame> frames;

	OrientationF orientation;
	MaterialDescriptor skyMaterialDescriptor, waterMaterialDescriptor;
	// TODO: Make these MeshInstances and pre-build them.
	MeshInstance skyMeshes[LightFrameData::NUM_BANDS];
	MeshInstance waterMeshes[2];
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
