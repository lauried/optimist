/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "scenegraph/objects/GamecodeScenegraphPlanet.hh"
#include "library/graphics/Color.hh"
#include "gamecode/ModularEngineTypes.hh"
#include "scenegraph/ScenegraphAdapter.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// IGamecodeEngineObject
//-----------------------------------------------------------------------------

void GamecodeScenegraphPlanet::SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode)
{
	if (!key.compare("orientation"))
	{
		throw gamecode_exception("Property cannot be written, but it can be acted upon");
	}
	else
	{
		throw gamecode_exception("No such property");
	}
}

GamecodeValue GamecodeScenegraphPlanet::GetProperty(std::string key, IGamecode *gamecode)
{
	if (!key.compare("orientation"))
	{
		if (positionWrapper == nullptr)
		{
			positionWrapper = new GamecodeOrientation(&planet.orientation);
			AddChildObject(positionWrapper);
		}
		return GamecodeValue(positionWrapper);
	}
	throw gamecode_exception("No such property, or read-only");
}

void GamecodeScenegraphPlanet::SetIndex(int i, GamecodeValue val, IGamecode *gamecode)
{
	throw gamecode_exception("No Such Index");
}

GamecodeValue GamecodeScenegraphPlanet::GetIndex(int i, IGamecode *gamecode)
{
	throw gamecode_exception("No Such Index");
}

void GamecodeScenegraphPlanet::Destruct()
{
	delete this;
}

//-----------------------------------------------------------------------------
// ScenegraphObjectType requirements
//-----------------------------------------------------------------------------

IGamecodeEngineObject* GamecodeScenegraphPlanet::Construct(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	return new GamecodeScenegraphPlanet();
}

void RegisterGamecodeScenegraphPlanet(IGamecode *gamecode, ResourceManager *resources)
{
	gamecode->RegisterEngineType("ScenegraphPlanet", gamecode->CreateEngineType<GamecodeScenegraphPlanet>()
		->Constructor(&GamecodeScenegraphPlanet::Construct)
		->AddMethod("setPlanet", &GamecodeScenegraphPlanet::SetPlanet)
		->AddMethod("addRing", &GamecodeScenegraphPlanet::AddRing)
		->AddMethod("setFullbright", &GamecodeScenegraphPlanet::SetFullbright)
		->AddProperty("orientation")
	);
}

MODULAR_ENGINE_TYPE(ScenegraphAdapter, RegisterGamecodeScenegraphPlanet)

//-----------------------------------------------------------------------------
// Engine Gamecode Methods
//-----------------------------------------------------------------------------

GamecodeValue GamecodeScenegraphPlanet::SetPlanet(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(2);

	planet.SetPlanet(
		parameters->RequireType(0, GamecodeValue::T_Number).GetFloat(),
		Color(parameters->RequireType(1, GamecodeValue::T_String).GetString())
	);

	return GamecodeValue();
}

GamecodeValue GamecodeScenegraphPlanet::AddRing(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(3);

	planet.AddRing(
		parameters->RequireType(0, GamecodeValue::T_Number).GetFloat(),
		parameters->RequireType(1, GamecodeValue::T_Number).GetFloat(),
		Color(parameters->RequireType(2, GamecodeValue::T_String).GetString())
	);

	return GamecodeValue();
}

GamecodeValue GamecodeScenegraphPlanet::SetFullbright(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);

	planet.SetFullbright(
		parameters->RequireType(0, GamecodeValue::T_Number).GetInt() != 0
	);

	return GamecodeValue();
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
