/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "scenegraph/IRenderMeshProvider.hh"
#include "terrain/GamecodeTerrain.hh"
#include "terrain/TerrainClipmap.hh"
#include "terrain/TerrainChunkModel.hh"
#include "library/containers/PagedArray.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class TerrainRenderMeshProvider : public IRenderMeshProvider, public ITerrainDataProvider<TerrainChunkModel> {
public:
	TerrainRenderMeshProvider() : terrain(nullptr), clipmap(nullptr) {}
	~TerrainRenderMeshProvider();

	// IRenderMeshProvider
	void SetResources(const SceneInfo *scene, IRenderResources *resources);
	void GetMeshes(const SceneInfo *scene, std::vector<MeshInstance*>* meshes);

	// ITerrainDataProvider<TerrainChunkModel>
	TerrainChunkModel* GetData(int x, int y);
	void FreeData(TerrainChunkModel *data);

	void SetTerrain(GamecodeTerrain *terrain);
	void SetTerrainColour(int type, uint32_t colour1, uint32_t colour2);

private:
	GamecodeTerrain *terrain;
	TerrainClipmap<TerrainChunkModel> *clipmap;

	std::map<int, TerrainColour> terrainColours;

	PagedArray<MeshInstance> meshInstances;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
