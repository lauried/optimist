/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "scenegraph/objects/GamecodeScenegraphTerrain.hh"
#include "gamecode/GamecodeException.hh"
#include "library/graphics/Color.hh"
#include "gamecode/ModularEngineTypes.hh"
#include "scenegraph/ScenegraphAdapter.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

GamecodeScenegraphTerrain::GamecodeScenegraphTerrain()
{
}

GamecodeScenegraphTerrain::~GamecodeScenegraphTerrain()
{
}

//-----------------------------------------------------------------------------
// IRenderMeshProvider
//-----------------------------------------------------------------------------

void GamecodeScenegraphTerrain::SetResources(const SceneInfo *scene, IRenderResources *resources)
{
	meshProvider.SetResources(scene, resources);
}

void GamecodeScenegraphTerrain::GetMeshes(const SceneInfo *scene, std::vector<MeshInstance*>* meshes)
{
	meshProvider.GetMeshes(scene, meshes);
}

//-----------------------------------------------------------------------------
// IGamecodeEngineObject
//-----------------------------------------------------------------------------

void GamecodeScenegraphTerrain::SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode)
{
	// TODO: things like LOD
}

GamecodeValue GamecodeScenegraphTerrain::GetProperty(std::string key, IGamecode *gamecode)
{
	return GamecodeValue();
}

void GamecodeScenegraphTerrain::SetIndex(int i, GamecodeValue val, IGamecode *gamecode)
{
}

GamecodeValue GamecodeScenegraphTerrain::GetIndex(int i, IGamecode *gamecode)
{
	return GamecodeValue();
}

void GamecodeScenegraphTerrain::Destruct()
{
	delete this;
}

//-----------------------------------------------------------------------------
// Engine Methods
//-----------------------------------------------------------------------------

GamecodeValue GamecodeScenegraphTerrain::SetTerrain(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1);
	GamecodeTerrain *engineObject = parameters->RequireEngineType<GamecodeTerrain>(0, "Terrain");
	meshProvider.SetTerrain(engineObject);
	return GamecodeValue();
}

GamecodeValue GamecodeScenegraphTerrain::SetTileTypeColour(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(3);

	// TODO: Convert from vectors, ints, if we really need to.

	Color c;
	c.Set(parameters->RequireType(1, GamecodeValue::T_String).GetString());
	uint32_t colour1 = c.GetArgb();
	c.Set(parameters->RequireType(2, GamecodeValue::T_String).GetString());
	uint32_t colour2 = c.GetArgb();

	meshProvider.SetTerrainColour(parameters->RequireType(0, GamecodeValue::T_Number).GetInt(), colour1, colour2);
	return GamecodeValue();
}

//-----------------------------------------------------------------------------
// ScenegraphObjectType requirements
//-----------------------------------------------------------------------------

IGamecodeEngineObject* GamecodeScenegraphTerrain::Construct(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	return new GamecodeScenegraphTerrain();
}

void RegisterGamecodeScenegraphTerrain(IGamecode *gamecode, ResourceManager *resources)
{
	gamecode->RegisterEngineType("ScenegraphTerrain", gamecode->CreateEngineType<GamecodeScenegraphTerrain>()
		->Constructor(&GamecodeScenegraphTerrain::Construct)
		->AddMethod("setTerrain", &GamecodeScenegraphTerrain::SetTerrain)
		->AddMethod("setTileTypeColour", &GamecodeScenegraphTerrain::SetTileTypeColour)
	);
}

MODULAR_ENGINE_TYPE(ScenegraphAdapter, RegisterGamecodeScenegraphTerrain)

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
