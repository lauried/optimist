/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "scenegraph/objects/TerrainRenderMeshProvider.hh"
#include <cmath>
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

TerrainRenderMeshProvider::~TerrainRenderMeshProvider()
{
	delete clipmap;
}

//-----------------------------------------------------------------------------
// IRenderMeshProvider
//-----------------------------------------------------------------------------

void TerrainRenderMeshProvider::SetResources(const SceneInfo *scene, IRenderResources *resources)
{
}

void TerrainRenderMeshProvider::GetMeshes(const SceneInfo *scene, std::vector<MeshInstance*>* meshes)
{
	if (terrain == nullptr)
	{
		return;
	}

	Vector2F centre = terrain->WorldCoordsToChunk(Vector2F(scene->camera->orientation.origin[0], scene->camera->orientation.origin[1]));

	clipmap->SetCentrePosition(Vector2I(std::floor(centre[0]), std::floor(centre[1])));

	bool underwater = scene->camera->orientation.origin[2] < 0;

	Vector2I mins = clipmap->GetMins();
	Vector2I maxs = clipmap->GetMaxs();

	meshInstances.clear(true);

	for (int y = mins[1]; y <= maxs[1]; y += 1)
	{
		for (int x = mins[0]; x <= maxs[0]; x += 1)
		{
			TerrainChunkModel *tm = clipmap->GetChunkAbsolute(x, y);

			if (tm == nullptr)
			{
				continue;
			}

			if (scene->frustum != nullptr && !scene->frustum->Contains(tm->bounds))
			{
				continue;
			}

			Model* models[2];
			models[0] = tm->GetWaterSurface();
			models[1] = (underwater) ? (tm->GetUnderwater()) : (tm->GetLand());

			for (int m = 0; m < 2; m += 1)
			{
				Model *mdl = models[m];
				for (int i = 0; i < mdl->NumMeshes(); i += 1)
				{
					MeshInstance mi;
					mi.mesh = mdl->GetMesh(i);
					mi.orientation = &(tm->orientation);
//std::cout << "tc orig: " << mi.orientation->origin[0] << "," << mi.orientation->origin[1] << std::endl;
					meshes->push_back(meshInstances.push_back(mi));
				}
			}
		}
	}
}

//-----------------------------------------------------------------------------
// ITerrainDataProvider<TerrainChunkModel>
//-----------------------------------------------------------------------------

TerrainChunkModel* TerrainRenderMeshProvider::GetData(int x, int y)
{
	if (terrain == nullptr)
	{
		return nullptr;
	}
	TerrainChunk *data = terrain->GetChunk(x, y);
	TerrainChunkModel *model = new TerrainChunkModel(data, &terrainColours);
	terrain->FreeChunk(data);
	return model;
}

void TerrainRenderMeshProvider::FreeData(TerrainChunkModel *data)
{
	delete data;
}

//-----------------------------------------------------------------------------
// No interface
//-----------------------------------------------------------------------------

void TerrainRenderMeshProvider::SetTerrain(GamecodeTerrain *terrain)
{
	delete clipmap;

	this->terrain = terrain;
	this->clipmap = new TerrainClipmap<TerrainChunkModel>(4, this);
}

void TerrainRenderMeshProvider::SetTerrainColour(int type, uint32_t colour1, uint32_t colour2)
{
	terrainColours.insert(std::make_pair(type, TerrainColour(colour1, colour2)));
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
