/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <vector>
#include "scenegraph/IRenderMeshProvider.hh"
#include "renderer/ParticleMeshBuilder.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// TODO: We could support setting the orientation, but there's no big need.

class StarfieldRenderMeshProvider : public IRenderMeshProvider {
public:
	StarfieldRenderMeshProvider();

	// IRenderMeshProvider
	void SetResources(const SceneInfo *scene, IRenderResources *resources);
	void GetMeshes(const SceneInfo *scene, std::vector<MeshInstance*>* meshes);

	void SetBrightness(float brightness);
	float GetBrightness() { return brightness; }

private:
	struct Star {
		Vector3F origin;
		float brightness;
	};
	std::vector<Star> stars;
	float brightness;

	OrientationF orientation;
	MeshInstance meshInstance;
	MaterialDescriptor material;
	ParticleMeshBuilder particles;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
