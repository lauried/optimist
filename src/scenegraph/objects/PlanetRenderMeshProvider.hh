/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "scenegraph/IRenderMeshProvider.hh"
#include "library/graphics/Color.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class PlanetRenderMeshProvider : public IRenderMeshProvider {
public:
	PlanetRenderMeshProvider();
	~PlanetRenderMeshProvider();

	// IRenderMeshProvider
	void SetResources(const SceneInfo *scene, IRenderResources *resources);
	void GetMeshes(const SceneInfo *scene, std::vector<MeshInstance*>* meshes);

	void SetPlanet(float size, Color colour);
	void AddRing(float min, float max, Color colour);

	void SetFullbright(bool fullbright);

	OrientationF orientation;
private:
	MeshInstance planet;
	MaterialDescriptor planetMaterial;
	std::vector<MeshInstance> rings;
	std::vector<MaterialDescriptor> ringMaterials;

	bool fullbright;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
