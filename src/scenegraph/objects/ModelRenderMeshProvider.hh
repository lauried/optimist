/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <vector>
#include "scenegraph/IRenderMeshProvider.hh"
#include "resources/ResourceManager.hh"
#include "resources/ModelResource.hh"
#include "resources/BvhResource.hh"
#include "renderer/ModelPartAnimation.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class ModelRenderMeshProvider : public IRenderMeshProvider {
public:
	OrientationF position;

	// TODO: We could make the MRMP construct from a model resource, letting
	// us take advantage of the guarantee that a model will always exist.
	// The ScenegraphObject can be responsible for deleting the provider.
	ModelRenderMeshProvider() : model(nullptr), currentAnimation(-1), animationStart(-1) {}

	~ModelRenderMeshProvider();

	/**
	 * This will cause existing animations to be removed.
	 * It's the responsibility of either the ScenegraphObject or the Scenegraph
	 * itself to make sure this only gets called when the  data has been
	 * updated.
	 */
	void SetModel(ResourceManager::ResourcePointer<ModelResource> model);

	/**
	 * Clears all the existing animations.
	 */
	void ClearAnimations();

	/**
	 * Appends an animation to the list of animations.
	 */
	void AddAnimation(ResourceManager::ResourcePointer<BvhResource> anim);

	void SetCurrentAnimation(int index);

	void SetResources(const SceneInfo *scene, IRenderResources *resources);
	void GetMeshes(const SceneInfo *scene, std::vector<MeshInstance*>* meshes);

private:
	ResourceManager::ResourcePointer<ModelResource> model;

	std::vector<ResourceManager::ResourcePointer<BvhResource>> animations;
	int currentAnimation;

	std::vector<ModelPartAnimation*> modelAnimations;
	double animationStart;

	std::vector<MeshInstance> meshInstances;
	std::vector<OrientationF> meshOrientations;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
