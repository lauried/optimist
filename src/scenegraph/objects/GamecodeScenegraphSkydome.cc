/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "scenegraph/objects/GamecodeScenegraphSkydome.hh"
#include "gamecode/GamecodeException.hh"
#include "gamecode/library/GamecodeVector.hh"
#include "gamecode/ModularEngineTypes.hh"
#include "scenegraph/ScenegraphAdapter.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// IRenderMeshprovider
//-----------------------------------------------------------------------------

void GamecodeScenegraphSkydome::SetResources(const SceneInfo *scene, IRenderResources *resources)
{
	skydome.SetResources(scene, resources);
}

void GamecodeScenegraphSkydome::GetMeshes(const SceneInfo *scene, std::vector<MeshInstance*>* meshes)
{
	skydome.GetMeshes(scene, meshes);
}

//-----------------------------------------------------------------------------
// IGamecodeEngineObject
//-----------------------------------------------------------------------------

void GamecodeScenegraphSkydome::SetProperty(std::string key, GamecodeValue val, IGamecode *gamecode)
{
	throw gamecode_exception("No Such Property");
}

GamecodeValue GamecodeScenegraphSkydome::GetProperty(std::string key, IGamecode *gamecode)
{
	throw gamecode_exception("No Such Property");
}

void GamecodeScenegraphSkydome::SetIndex(int i, GamecodeValue val, IGamecode *gamecode)
{
	throw gamecode_exception("No Such Index");
}

GamecodeValue GamecodeScenegraphSkydome::GetIndex(int i, IGamecode *gamecode)
{
	throw gamecode_exception("No Such Index");
}

void GamecodeScenegraphSkydome::Destruct()
{
	delete this;
}

//-----------------------------------------------------------------------------
// Engine methods
//-----------------------------------------------------------------------------

GamecodeValue GamecodeScenegraphSkydome::SetSunPosition(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	parameters->RequireParmCount(1, 3);
	parameters->RequireNotParmCount(2);

	Vector3F position;
	if (parameters->size() == 1)
	{
		position.Set(*(parameters->RequireEngineType<GamecodeVector>(0, "Vector")->GetObject()));
	}
	else
	{
		parameters->RequireType(0, GamecodeValue::T_Number);
		parameters->RequireType(1, GamecodeValue::T_Number);
		parameters->RequireType(2, GamecodeValue::T_Number);
		position.Set(parameters->at(0).GetFloat(), parameters->at(1).GetFloat(), parameters->at(2).GetFloat());
	}

	skydome.SetSunPosition(position);

	return GamecodeValue();
}

//-----------------------------------------------------------------------------
// ScenegraphObject stuff
//-----------------------------------------------------------------------------

IGamecodeEngineObject* GamecodeScenegraphSkydome::Construct(GamecodeParameterList *parameters, IGamecode *gamecode)
{
	return new GamecodeScenegraphSkydome();
}

void RegisterGamecodeScenegraphSkydome(IGamecode *gamecode, ResourceManager *resources)
{
	gamecode->RegisterEngineType("ScenegraphSkydome", gamecode->CreateEngineType<GamecodeScenegraphSkydome>()
		->Constructor(&GamecodeScenegraphSkydome::Construct)
		->AddMethod("setSunPosition", &GamecodeScenegraphSkydome::SetSunPosition)
	);
}

MODULAR_ENGINE_TYPE(ScenegraphAdapter, RegisterGamecodeScenegraphSkydome)

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
