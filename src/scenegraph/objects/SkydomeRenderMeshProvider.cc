/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "scenegraph/objects/SkydomeRenderMeshProvider.hh"
#include "library/text/String.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

static MeshBuilder* MakeBand(float pitch0, float pitch1);
static MeshBuilder* MakeCap(bool top, float pitch);
static void ColourMesh(MeshBuilder *mesh, Color *colour);

SkydomeRenderMeshProvider::SkydomeRenderMeshProvider()
{
	// frames must be ordered lowest to highest in terms of sunHeight
	// "sunHeight    #bandcolour bandsize ..."
	frames.reserve(2);
	AddLightFrame("-0.1   #678 3  #558 3  #336 3  #124 3  #123");
	AddLightFrame(" 0.05  #68e 3  #47e 3  #25d 3  #14c 3  #03a");

	// Make makeband and MakeCap return meshes, and set the pointers to the meshinstances.
	{
		float bandSize = 3;
		float low = 0, high = bandSize;
		int i;
		for (i = 0; i < LightFrameData::NUM_BANDS - 1; ++i, low += bandSize, high += bandSize)
		{
			skyMeshes[i].mesh = MakeBand(low, high);
			skyMeshes[i].mesh->fullbright = true;
			skyMeshes[i].orientation = &orientation;
		}
		skyMeshes[i].mesh = MakeCap(true, low);
		skyMeshes[i].mesh->fullbright = true;
		skyMeshes[i].orientation = &orientation;
	}

	waterMeshes[0].mesh = MakeBand(0, -45);
	waterMeshes[0].orientation = &orientation;
	waterMeshes[1].mesh = MakeCap(false, -45);
	waterMeshes[1].orientation = &orientation;
}

SkydomeRenderMeshProvider::~SkydomeRenderMeshProvider()
{
	// Destruct the meshes in the meshinstances.
	for (int i = 0; i < LightFrameData::NUM_BANDS; i += 1)
	{
		delete skyMeshes[i].mesh;
	}
	delete waterMeshes[0].mesh;
	delete waterMeshes[1].mesh;
}

//-----------------------------------------------------------------------------
// IRenderMeshProvider
//-----------------------------------------------------------------------------

void SkydomeRenderMeshProvider::SetResources(const SceneInfo *scene, IRenderResources *resources)
{
	const Material *skyMaterial = nullptr;
	const Material *waterMaterial = nullptr;
	for (int i = 0; i < LightFrameData::NUM_BANDS; i += 1)
	{
		if (skyMeshes[i].material == nullptr)
		{
			if (skyMaterial == nullptr)
			{
				skyMaterial = resources->GetMaterial(skyMaterialDescriptor);
			}
			skyMeshes[i].material = skyMaterial;
		}
	}
	for (int i = 0; i < 2; i += 1)
	{
		if (waterMeshes[i].material == nullptr)
		{
			if (waterMaterial == nullptr)
			{
				waterMaterial = resources->GetMaterial(waterMaterialDescriptor);
			}
			waterMeshes[i].material = waterMaterial;
		}
	}
}

void SkydomeRenderMeshProvider::GetMeshes(const SceneInfo *scene, std::vector<MeshInstance*>* meshes)
{
	// Set origin to the camera position.
	orientation.origin = scene->camera->orientation.origin;

	// Colour and add the band meshes.
	for (int i = 0; i < LightFrameData::NUM_BANDS; i += 1)
	{
		ColourMesh(skyMeshes[i].mesh, &current.bands[i].colour);
		meshes->push_back(&skyMeshes[i]);
	}

	for (int i = 0; i < 2; i += 1)
	{
		meshes->push_back(&waterMeshes[i]);
	}
}


//-----------------------------------------------------------------------------
// Other parts
//-----------------------------------------------------------------------------

void SkydomeRenderMeshProvider::SetSunPosition(Vector3F position)
{
	sunDirection = position;
	sunDirection.Normalize();

	// Blend the appropriate light frames:
	int lower = -1;
	for (unsigned int i = 0; i < frames.size(); ++i)
	{
		if (frames[i].sunHeight > sunDirection[2])
			break;

		if (frames[i].sunHeight <= sunDirection[2])
		{
			lower = i;
		}
	}
	if (lower == -1)
	{
		if (frames.size() > 0)
			current = frames[0].data;
	}
	else if ((unsigned)lower + 1 < frames.size())
	{
		float frac = (sunDirection[2] - frames[lower].sunHeight) / (frames[lower+1].sunHeight - frames[lower].sunHeight);
		current.Blend(frames[lower].data, 1 - frac, frames[lower+1].data);
	}
	else
	{
		current = frames[lower].data;
	}
	if (sunDirection[2] < 0)
	{
		sunDirection[2] *= -1;
	}

	// Set direction to the direction of light rather than direction to sun.
	sunDirection *= -1.0f;
}

const float skyScale = 1024.0f;
const int DOME_DIVISIONS = 20;

static MeshBuilder* MakeBand(float pitch0, float pitch1)
{
	MeshBuilder *mesh = new MeshBuilder();

	Color waterColour;
	waterColour.Set(0x0002286d);

	pitch0 *= M_PI / 180;
	pitch1 *= M_PI / 180;

	if (pitch1 > pitch0)
	{
		float tmp = pitch0;
		pitch0 = pitch1;
		pitch1 = tmp;
	}

	mesh->AddAttribute(MeshBuilder::VERTEX, 3);
	mesh->AddAttribute(MeshBuilder::NORMAL, 3);
	mesh->AddAttribute(MeshBuilder::COLOUR, 3);
	mesh->type = MeshBuilder::TRIANGLE_STRIP;
	mesh->AllocateVerts((DOME_DIVISIONS + 1) * 2);

	float normal[3] = {0, 0, 1};
	float vertex[3];
	for (int ang = 0; ang <= DOME_DIVISIONS; ++ang)
	{
		float yaw = ((float)ang/(float)DOME_DIVISIONS) * M_PI * 2;

		vertex[0] = cos(yaw) * cos(pitch0) * skyScale;
		vertex[1] = sin(yaw) * cos(pitch0) * skyScale;
		vertex[2] = sin(pitch0) * skyScale;
		mesh->AddVertex(vertex, normal, waterColour.rgba, 0);

		vertex[0] = cos(yaw) * cos(pitch1) * skyScale;
		vertex[1] = sin(yaw) * cos(pitch1) * skyScale;
		vertex[2] = sin(pitch1) * skyScale;
		mesh->AddVertex(vertex, normal, waterColour.rgba, 0);
	}

	return mesh;
}

static MeshBuilder* MakeCap(bool top, float pitch)
{
	MeshBuilder *mesh = new MeshBuilder();

	Color waterColour;
	waterColour.Set(0x0002286d);

	pitch *= M_PI / 180;

	mesh->AddAttribute(MeshBuilder::VERTEX, 3);
	mesh->AddAttribute(MeshBuilder::NORMAL, 3);
	mesh->AddAttribute(MeshBuilder::COLOUR, 3);
	mesh->type = MeshBuilder::TRIANGLE_FAN;
	mesh->AllocateVerts(DOME_DIVISIONS + 2);

	float normal[3] = { 0, 0, 1 };
	float vertex[3];

	int start = 0, end = DOME_DIVISIONS + 1, dir = 1;
	if (top)
	{
		vertex[0] = 0;
		vertex[1] = 0;
		vertex[2] = skyScale;
		mesh->AddVertex(vertex, normal, waterColour.rgba, 0);
	}
	else
	{
		vertex[0] = 0;
		vertex[1] = 0;
		vertex[2] = -skyScale;
		mesh->AddVertex(vertex, normal, waterColour.rgba, 0);
	}

	for (int ang = start; ang != end; ang += dir)
	{
		float yaw = ((float)ang/(float)DOME_DIVISIONS) * M_PI * 2;

		vertex[0] = cos(yaw) * cos(pitch) * skyScale;
		vertex[1] = sin(yaw) * cos(pitch) * skyScale;
		vertex[2] = sin(pitch) * skyScale;
		mesh->AddVertex(vertex, normal, waterColour.rgba, 0);
	}

	return mesh;
}

static void ColourMesh(MeshBuilder *mesh, Color *colour)
{
	MeshBuilder::VertexAttribute *colours = &mesh->attributes[mesh->FindAttribute(MeshBuilder::COLOUR, 3)];
	for (int i = 0; i < mesh->numVerts; ++i)
	{
		float *dest = colours->Element(i);
		dest[0] = colour->rgba[0];
		dest[1] = colour->rgba[1];
		dest[2] = colour->rgba[2];
	}
}

void SkydomeRenderMeshProvider::ParseLightFrame(LightFrame *frame, std::string s)
{
	// "sunHeight #bandcolour bandSize ..."
	std::vector<std::string> tokens;
	String::Tokenize(s, &tokens);

	frame->sunHeight = (tokens.size() > 0) ? atof(tokens[0].c_str()) : 1.0f;

	unsigned token = 1;
	for (int i = 0; i < LightFrameData::NUM_BANDS; ++i)
	{
		frame->data.bands[i].colour.Set((tokens.size() > token) ? tokens[token].c_str() : "#000");
		++token;
		frame->data.bands[i].size = (tokens.size() > token) ? atof(tokens[token].c_str()) : 3.0f;
		++token;
	}
}

void SkydomeRenderMeshProvider::AddLightFrame(std::string s)
{
	LightFrame frame;
	frames.push_back(frame);
	ParseLightFrame(&frames[frames.size() - 1], s);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
