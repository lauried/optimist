/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "scenegraph/objects/PlanetRenderMeshProvider.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Building a ring mesh
//-----------------------------------------------------------------------------

// Builds a ring mesh in the xy plane.
static void BuildRingMesh(MeshBuilder *mesh, float inner, float outer, int segments)
{
	mesh->AllocateTris(segments * 2 * 2); // 2 triangles per segment, 2 sides
	float angularStep = (M_PI * 2) / (float)segments;
	Vector3F p1, p2, p3, p4, normal1(0, 0, 1), normal2(0, 0, -1);
	for (int i = 0; i < segments; ++i)
	{
		float angle1 = (float)i * angularStep;
		float angle2 = angle1 + angularStep;
		float s1 = sin(angle1);
		float c1 = cos(angle1);
		float s2 = sin(angle2);
		float c2 = cos(angle2);

		p1.Set(s1 * inner, c1 * inner, 0);
		p2.Set(s1 * outer, c1 * outer, 0);
		p3.Set(s2 * inner, c2 * inner, 0);
		p4.Set(s2 * outer, c2 * outer, 0);

		// one side
		mesh->AddVertex(p1.v, normal2.v, nullptr, nullptr);
		mesh->AddVertex(p4.v, normal2.v, nullptr, nullptr);
		mesh->AddVertex(p2.v, normal2.v, nullptr, nullptr);

		mesh->AddVertex(p1.v, normal2.v, nullptr, nullptr);
		mesh->AddVertex(p3.v, normal2.v, nullptr, nullptr);
		mesh->AddVertex(p4.v, normal2.v, nullptr, nullptr);

		// other side
		mesh->AddVertex(p2.v, normal1.v, nullptr, nullptr);
		mesh->AddVertex(p4.v, normal1.v, nullptr, nullptr);
		mesh->AddVertex(p1.v, normal1.v, nullptr, nullptr);

		mesh->AddVertex(p4.v, normal1.v, nullptr, nullptr);
		mesh->AddVertex(p3.v, normal1.v, nullptr, nullptr);
		mesh->AddVertex(p1.v, normal1.v, nullptr, nullptr);
	}
}

//-----------------------------------------------------------------------------
// Building a sphere mesh
//-----------------------------------------------------------------------------

static void SubdivideSphere(const float *source, float *dest, int numSourceTris, int vertStep)
{
	// For each triangle input, output 4 output triangles.
	//     /\            /\
	//    /  \          /__\
	//   /    \        /\  /\
	//  /______\      /__\/__\
	// Corners a, b, c clockwise, and midpoint edgees opposite the corner
	// ma, mb, mc.
	// New trianles are:
	// a, mc, mb
	// b, ma, mc
	// c, mb, ma
	// ma, mb, mc

	Vector3F a, b, c, ma, mb, mc;

	const float *sourcePtr = source;
	float *destPtr = dest;
	for (int i = 0; i < numSourceTris; ++i)
	{
		Vector3F::Copy(sourcePtr, a.v); sourcePtr += vertStep;
		Vector3F::Copy(sourcePtr, b.v); sourcePtr += vertStep;
		Vector3F::Copy(sourcePtr, c.v); sourcePtr += vertStep;

		ma = b + c; ma.Normalize();
		mb = a + c; mb.Normalize();
		mc = a + b; mc.Normalize();

		Vector3F::Copy(a.v,  destPtr); destPtr += vertStep;
		Vector3F::Copy(mc.v, destPtr); destPtr += vertStep;
		Vector3F::Copy(mb.v, destPtr); destPtr += vertStep;

		Vector3F::Copy(b.v,  destPtr); destPtr += vertStep;
		Vector3F::Copy(ma.v, destPtr); destPtr += vertStep;
		Vector3F::Copy(mc.v, destPtr); destPtr += vertStep;

		Vector3F::Copy(c.v,  destPtr); destPtr += vertStep;
		Vector3F::Copy(mb.v, destPtr); destPtr += vertStep;
		Vector3F::Copy(ma.v, destPtr); destPtr += vertStep;

		Vector3F::Copy(ma.v, destPtr); destPtr += vertStep;
		Vector3F::Copy(mb.v, destPtr); destPtr += vertStep;
		Vector3F::Copy(mc.v, destPtr); destPtr += vertStep;
	}
}

static void CopyTriangleVerts(const float *source, float *dest, int v1, int v2, int v3) {
	dest[0] = source[v1*3  ];
	dest[1] = source[v1*3+1];
	dest[2] = source[v1*3+2];
	dest[3] = source[v2*3  ];
	dest[4] = source[v2*3+1];
	dest[5] = source[v2*3+2];
	dest[6] = source[v3*3  ];
	dest[7] = source[v3*3+1];
	dest[8] = source[v3*3+2];
}

static void BuildSphereMesh(MeshBuilder *mesh, float scale)
{
	static float gIcosahedronVerts[12*3] = {
		 0.000,  0.000,  1.000,
		 0.894,  0.000,  0.447,
		 0.276,  0.851,  0.447,
		-0.724,  0.526,  0.447,
		-0.724, -0.526,  0.447,
		 0.276, -0.851,  0.447,
		 0.724,  0.526, -0.447,
		-0.276,  0.851, -0.447,
		-0.894,  0.000, -0.447,
		-0.276, -0.851, -0.447,
		 0.724, -0.526, -0.447,
		 0.000,  0.000, -1.000
	};
	static float buffer[80*9];
	static bool bufferBuilt = false;

	if (!bufferBuilt)
	{
		// Build the triangles at the end of the buffer so we can subdivide
		// in place.
		float *icosahedron = &buffer[80*9];
		icosahedron -= 9; CopyTriangleVerts(gIcosahedronVerts, icosahedron, 0,  1,  2);
		icosahedron -= 9; CopyTriangleVerts(gIcosahedronVerts, icosahedron, 0,  2,  3);
		icosahedron -= 9; CopyTriangleVerts(gIcosahedronVerts, icosahedron, 0,  3,  4);
		icosahedron -= 9; CopyTriangleVerts(gIcosahedronVerts, icosahedron, 0,  4,  5);
		icosahedron -= 9; CopyTriangleVerts(gIcosahedronVerts, icosahedron, 0,  5,  1);
		icosahedron -= 9; CopyTriangleVerts(gIcosahedronVerts, icosahedron, 11, 7,  6);
		icosahedron -= 9; CopyTriangleVerts(gIcosahedronVerts, icosahedron, 11, 8,  7);
		icosahedron -= 9; CopyTriangleVerts(gIcosahedronVerts, icosahedron, 11, 9,  8);
		icosahedron -= 9; CopyTriangleVerts(gIcosahedronVerts, icosahedron, 11, 10, 9);
		icosahedron -= 9; CopyTriangleVerts(gIcosahedronVerts, icosahedron, 11, 6,  10);
		icosahedron -= 9; CopyTriangleVerts(gIcosahedronVerts, icosahedron, 1,  6,  2);
		icosahedron -= 9; CopyTriangleVerts(gIcosahedronVerts, icosahedron, 2,  7,  3);
		icosahedron -= 9; CopyTriangleVerts(gIcosahedronVerts, icosahedron, 3,  8,  4);
		icosahedron -= 9; CopyTriangleVerts(gIcosahedronVerts, icosahedron, 4,  9,  5);
		icosahedron -= 9; CopyTriangleVerts(gIcosahedronVerts, icosahedron, 5,  10, 1);
		icosahedron -= 9; CopyTriangleVerts(gIcosahedronVerts, icosahedron, 6,  7,  2);
		icosahedron -= 9; CopyTriangleVerts(gIcosahedronVerts, icosahedron, 7,  8,  3);
		icosahedron -= 9; CopyTriangleVerts(gIcosahedronVerts, icosahedron, 8,  9,  4);
		icosahedron -= 9; CopyTriangleVerts(gIcosahedronVerts, icosahedron, 9,  10, 5);
		icosahedron -= 9; CopyTriangleVerts(gIcosahedronVerts, icosahedron, 10, 6,  1);
		SubdivideSphere(icosahedron, buffer, 20, 3);
		bufferBuilt = true;
	}

	mesh->AllocateTris(80);
	Vector3F p1, p2, p3, normal;
	float *finalPtr = buffer;
	for (int i = 0; i < 80; ++i)
	{
		Vector3F::Copy(finalPtr, p1.v); finalPtr += 3;
		Vector3F::Copy(finalPtr, p2.v); finalPtr += 3;
		Vector3F::Copy(finalPtr, p3.v); finalPtr += 3;
		normal = p1 + p2 + p3;
		normal.Normalize();
		p1 *= scale;
		p2 *= scale;
		p3 *= scale;
		mesh->AddVertex(p3.v, normal.v, nullptr, nullptr);
		mesh->AddVertex(p2.v, normal.v, nullptr, nullptr);
		mesh->AddVertex(p1.v, normal.v, nullptr, nullptr);
	}
}

//-----------------------------------------------------------------------------
// PlanetRenderMeshProvider
//-----------------------------------------------------------------------------

PlanetRenderMeshProvider::PlanetRenderMeshProvider()
{
	fullbright = false;
	planet.orientation = &orientation;
}

PlanetRenderMeshProvider::~PlanetRenderMeshProvider()
{
	delete planet.mesh;
	for (int i= 0; i < rings.size(); i += 1)
	{
		delete rings[i].mesh;
	}
}

void PlanetRenderMeshProvider::SetPlanet(float size, Color colour)
{
	delete planet.mesh;
	planet.mesh = new MeshBuilder();
	planet.mesh->AddAttribute(MeshBuilder::VERTEX, 3);
	planet.mesh->AddAttribute(MeshBuilder::NORMAL, 3);
	BuildSphereMesh(planet.mesh, size);
	planet.mesh->fullbright = fullbright;
	planetMaterial.color = colour;
	planetMaterial.color.rgba[3] = 1.0;
	planetMaterial.lightMode = fullbright ? MaterialDescriptor::LightFullbright : MaterialDescriptor::LightNormal;
}

void PlanetRenderMeshProvider::AddRing(float min, float max, Color colour)
{
	const int RING_SEGMENTS = 16;

	MeshInstance mi;
	MaterialDescriptor md;
	mi.orientation = &orientation;
	mi.mesh = new MeshBuilder();
	mi.mesh->AddAttribute(MeshBuilder::VERTEX, 3);
	mi.mesh->AddAttribute(MeshBuilder::NORMAL, 3);
	BuildRingMesh(mi.mesh, min, max, RING_SEGMENTS);
	mi.mesh->fullbright = true;
	md.color = colour;
	md.color.rgba[3] = 1.0;
	md.lightMode = MaterialDescriptor::LightFullbright;
	rings.push_back(mi);
	ringMaterials.push_back(md);
}

void PlanetRenderMeshProvider::SetFullbright(bool fullbright)
{
	this->fullbright = fullbright;
	if (planet.mesh != nullptr)
	{
		planet.mesh->fullbright = fullbright;
	}
}

//-----------------------------------------------------------------------------
// IRenderMeshProvider
//-----------------------------------------------------------------------------

void PlanetRenderMeshProvider::SetResources(const SceneInfo *scene, IRenderResources *resources)
{
	if (!planet.material)
	{
		planet.material = resources->GetMaterial(planetMaterial);
	}
	for (int i = 0; i < rings.size(); i += 1)
	{
		if (!rings[i].material)
		{
			rings[i].material = resources->GetMaterial(ringMaterials[i]);
		}
	}
}

void PlanetRenderMeshProvider::GetMeshes(const SceneInfo *scene, std::vector<MeshInstance*>* meshes)
{
	// TODO: If the planet is too far away, render it as a point particle.

	if (planet.mesh)
	{
		planet.mesh->fullbright = fullbright;
		meshes->push_back(&planet);
	}
	for (int i = 0; i < rings.size(); i += 1)
	{
		meshes->push_back(&rings[i]);
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
