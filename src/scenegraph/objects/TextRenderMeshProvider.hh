/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "scenegraph/IRenderMeshProvider.hh"
#include "library/geometry/Vector3.hh"
#include "resources/ResourceManager.hh"
#include "resources/FontResource.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class TextRenderMeshProvider : public IRenderMeshProvider {
public:
	TextRenderMeshProvider() : font(nullptr), width(0), height(0) {}

	void SetFont(ResourceManager::ResourcePointer<FontResource> font);
	void SetText(std::string text);
	void SetWrapWidth(float width);
	float GetWrapWidth() { return wrapWidth; }
	float GetHeight();
	float GetWidth();

	std::string GetText() { return originalText; }

	// IRenderMeshProvider
	~TextRenderMeshProvider();
	void SetResources(const SceneInfo *scene, IRenderResources *resources);
	void GetMeshes(const SceneInfo *scene, std::vector<MeshInstance*>* meshes);

	OrientationF orientation;

private:
	ResourceManager::ResourcePointer<FontResource> font;
	std::string originalText;
	std::string wrappedText;
	float wrapWidth;
	float width;
	float height;

	std::vector<MeshBuilder*> meshes; // a different mesh for each glyph page
	std::vector<MeshInstance> meshInstances;
	std::vector<MaterialDescriptor> materials;

	// Wraps the text, calculates number of lines and height.
	// Requires that the font resource is loaded.
	void Update();
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
