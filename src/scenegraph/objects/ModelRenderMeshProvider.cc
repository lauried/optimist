/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "scenegraph/objects/ModelRenderMeshProvider.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

ModelRenderMeshProvider::~ModelRenderMeshProvider()
{
	ClearAnimations();
}

void ModelRenderMeshProvider::SetModel(ResourceManager::ResourcePointer<ModelResource> model)
{
	ClearAnimations();
	this->model = model;
}

void ModelRenderMeshProvider::ClearAnimations()
{
	animations.clear();
	for (auto it = modelAnimations.begin(); it != modelAnimations.end(); ++it)
	{
		delete *it;
	}
	modelAnimations.clear();
	currentAnimation = -1;
}

void ModelRenderMeshProvider::AddAnimation(ResourceManager::ResourcePointer<BvhResource> anim)
{
	animations.push_back(anim);
	// modelAnimations are lazy-loaded to accommodate asynchronously loaded
	// resources.
	modelAnimations.push_back(nullptr);
}

void ModelRenderMeshProvider::SetCurrentAnimation(int index)
{
    currentAnimation = index;
    animationStart = -1;
}

void ModelRenderMeshProvider::SetResources(const SceneInfo *scene, IRenderResources *resources)
{
	// TODO
}

void ModelRenderMeshProvider::GetMeshes(const SceneInfo *scene, std::vector<MeshInstance*>* meshes)
{
	if (model.GetState() != ResourceManager::LOADED)
	{
		std::cout << "no model loaded" << std::endl;
		return;
	}
	Model *mdl = model.GetResource()->GetModel();
	if (meshInstances.size() < mdl->NumMeshes())
	{
		meshInstances.resize(mdl->NumMeshes());
		meshOrientations.resize(mdl->NumMeshes());
		for (int i = 0; i < mdl->NumMeshes(); i += 1)
		{
			meshInstances[i].mesh = mdl->GetMesh(i);
		}
	}

	// If there's a current animation and it's ready,
	//   Lazy load its mapping.
	//   If it's just been started, set up the current timings, else update them.
	//   Calculate joint positions.
	//   Translate calculated joint positions into scene space.
	//   Set meshinstances orientations to the calculated positions.
	// Else
	//   Set all the orientations to the main orientation.
	// TODO: There's a valgrind error here: Conditional jump or move depends on uninitialised value(s). No models currently have animations so something is wrong with that.
	if (currentAnimation >= 0 && currentAnimation < animations.size() && animations[currentAnimation].GetState() == ResourceManager::LOADED)
	{
		BvhFile *bvh = animations[currentAnimation].GetResource()->GetBvh();
		if (modelAnimations[currentAnimation] == nullptr)
		{
			modelAnimations[currentAnimation] = new ModelPartAnimation(mdl, bvh);
		}
		ModelPartAnimation *mpa = modelAnimations[currentAnimation];
		if (animationStart < 0)
		{
			animationStart = scene->gameTime;
		}
		mpa->SetFrame(scene->gameTime - animationStart);
		for (int i = 0; i < meshOrientations.size(); i += 1)
		{
			// TODO: Have one orientation per part?
			meshOrientations[i] = mpa->GetPartOrientation(mdl->GetMeshData(i)->part);
			meshOrientations[i].ToExternalSpaceOf(position);
			meshInstances[i].orientation = &meshOrientations[i];
		}
	}
	else
	{
		for (int i = 0; i < meshInstances.size(); i += 1)
		{
			meshInstances[i].orientation = &position;
		}
	}

	// Append all the meshinstances to the supplied vector.
	for (int i = 0; i < meshInstances.size(); i += 1)
	{
		meshes->push_back(&meshInstances[i]);
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
