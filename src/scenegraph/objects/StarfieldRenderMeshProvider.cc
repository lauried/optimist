/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "scenegraph/objects/StarfieldRenderMeshProvider.hh"
#include "library/maths/RandomSequence.hh"
#include "library/graphics/Color.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// TODO: It'd be nice to load stars from some source, with a greater brightness
// range. Then when we zoom in we view the dimmest stars.

const int numStars = 2048;
const int maxParticles = 1024;
const float starDistance = 1024.0f;
const float particleApparentSize = 2.0f;

StarfieldRenderMeshProvider::StarfieldRenderMeshProvider()
{
	brightness = 1.0f;
	stars.reserve(numStars);

	RandomSequence rs;

	for (int i = 0; i < numStars; i += 1)
	{
		Star star;

		float r = rs.GetFloat();
		star.brightness = rs.GetFloat();//0.6 + pow(r, 10) * 0.4;
		star.origin = rs.GetVector3F();
		star.origin.Normalize();
		star.origin *= starDistance;

		stars.push_back(star);
	}

	particles.AllocateParticles(maxParticles);
	material.lightMode = MaterialDescriptor::LightFullbright;
}

void StarfieldRenderMeshProvider::SetBrightness(float brightness)
{
	this->brightness = brightness;
}

//-----------------------------------------------------------------------------
// IRenderMeshProvider
//-----------------------------------------------------------------------------

inline float StarBrightness(float globalBrightness, float starBrightness)
{
	// globalBrightness is the fraction of stars visible.
	// starBrightness is the star's real brightness roughly linearly distributed between 0.0 and 1.0.
	// We still want 1.0 to be the star's real brightness.

	// Also we apply a scale so that stars brighten up quickly to a visible brightness.

	float f = starBrightness - (1.0f - globalBrightness);
	if (f < 0)
	{
		f = 0;
	}
	return f;
}

void StarfieldRenderMeshProvider::SetResources(const SceneInfo *scene, IRenderResources *resources)
{
	meshInstance.material = resources->GetMaterial(material);
}

void StarfieldRenderMeshProvider::GetMeshes(const SceneInfo *scene, std::vector<MeshInstance*>* meshes)
{
	float pixelsPerUnitLengthProjected = (float)(scene->viewportSize[0]) / scene->camera->fovX;
	float particleSize = (particleApparentSize / pixelsPerUnitLengthProjected) * starDistance * 2;

	particles.ClearParticles();
	particles.SetCameraOrientation(&(scene->camera->orientation));
	for (int i = 0, parts = 0; i < stars.size() && parts < maxParticles; i += 1)
	{
		if (!scene->frustum->Contains(stars[i].origin, particleSize))
		{
			continue;
		}
		float lum = StarBrightness(brightness, stars[i].brightness);
		Color colour(lum, lum, lum);
		//Color colour(0.1, 0.1, 0.1);
		particles.AddParticle(ParticleMeshBuilder::FACE_DEPTH, stars[i].origin, particleSize, colour);
		parts += 1;
	}
	meshInstance.orientation = &orientation;
	meshInstance.mesh = particles.GetMesh();
	meshes->push_back(&meshInstance);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
