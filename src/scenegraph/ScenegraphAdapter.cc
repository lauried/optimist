/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "scenegraph/ScenegraphAdapter.hh"
#include "scenegraph/Scene.hh"
#include "scenegraph/GamecodeScenegraphRenderer.hh"
#include "scenegraph/GamecodeScenegraphLight.hh"
#include "scenegraph/GamecodeScene.hh"
#include "gamecode/library/GamecodeOrientation.hh"
#include "gamecode/GamecodeException.hh"
#include "gamecode/ModularEngineTypes.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Constructor and destructor
//-----------------------------------------------------------------------------

ScenegraphAdapter::~ScenegraphAdapter()
{
}

//-----------------------------------------------------------------------------
// IGamecodeAdapter
//-----------------------------------------------------------------------------

const std::string& ScenegraphAdapter::GetName()
{
	static std::string adapterName("scenegraph");
	return adapterName;
}

std::map<std::string, IGamecodeAdapter::AdapterMethod> ScenegraphAdapter::GetGlobalFunctions()
{
	return {};
}

void ScenegraphAdapter::OnRegister(IGamecode *gamecode)
{
	IPlatform *platformVar = platform;
	Renderer *rendererVar = renderer;

	gamecode->RegisterEngineType("ScenegraphRenderer", gamecode->CreateEngineType<GamecodeScenegraphRenderer>()
		->Constructor([platformVar, rendererVar](GamecodeParameterList *parameters, IGamecode *gamecode) {
			return new GamecodeScenegraphRenderer(platformVar, rendererVar);
		})
		->AddMethod("setCamera", &GamecodeScenegraphRenderer::SetCamera)
		->AddMethod("renderScene", &GamecodeScenegraphRenderer::RenderScene)
		->AddProperty("globalSaturation")
	);

	gamecode->RegisterEngineType("ScenegraphScene", gamecode->CreateEngineType<GamecodeScene>()
		->Constructor(&GamecodeScene::Construct)
		->AddMethod("addObject", &GamecodeScene::AddObject)
		->AddMethod("removeObject", &GamecodeScene::RemoveObject)
		->AddMethod("insertSceneBefore", &GamecodeScene::InsertSceneBefore)
		->AddMethod("insertSceneAfter", &GamecodeScene::InsertSceneAfter)
		->AddProperty("clearDepth")
		->AddProperty("clearColour")
		->AddProperty("additiveBlend")
		->AddProperty("position")
		->AddProperty("originFollowsCamera")
		->AddProperty("rearShading")
		->AddProperty("saturation")
	);

	gamecode->RegisterEngineType("ScenegraphLight", gamecode->CreateEngineType<GamecodeScenegraphLight>()
		->Constructor(&GamecodeScenegraphLight::Construct)
		->AddProperty("type")
		->AddProperty("position")
		->AddProperty("colour")
		->AddProperty("attenuation")
		->AddProperty("brightness")
	);

	ModularEngineTypes::Instance()->Register<ScenegraphAdapter>(gamecode, resources);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
