/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <list>
#include "renderer/Renderer.hh"
#include "platform/IPlatform.hh"
#include "scenegraph/Scene.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class SimpleScenegraphRenderer {
public:
	SimpleScenegraphRenderer(IPlatform *platform, Renderer *renderer);

	// IScenegraphRenderer
	void DrawScene(Scene *scene);
	void SetCamera(OrientationF& orientation, float fovX, float fovY, CameraF::ProjectionType projection);
	void SetCamera(OrientationF& orientation, float left, float top, float right, float bottom, CameraF::ProjectionType projection);
	// TODO: Set viewport. At the moment we're always fullscreen.

	// This is basically a hack for the current rendering code.
	float globalSaturation;

private:
	CameraF camera;
	std::stack<CameraF> cameraStack;
	IPlatform *platform;
	Renderer *renderer;
	ResourceManager *resources;

	void RecursiveDrawScene(Scene *scene, std::list<Scene*> *renderedScenes);
	void InnerDrawScene(Scene *scene);
	void DrawMesh(Scene *scene, MeshBuilder *mesh, const Material *material, const OrientationF &orientation, float meshScale = 1.0f);
	void SetProjection(const CameraF &cam);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
