/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "platform/ncurses/NcursesPlatform.hh"
//## add_libraries ncurses
#include <curses.h>
#ifdef _WIN32
#include <windows.h>
#else
#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>
#endif
#include <chrono>
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

WINDOW *gCursesWindow = nullptr;
int gInstances = 0;

const int NCURSES_MOUSE_BUTTON_1 = BUTTON1_PRESSED | BUTTON1_RELEASED;
const int NCURSES_MOUSE_BUTTON_2 = BUTTON2_PRESSED | BUTTON2_RELEASED;
const int NCURSES_MOUSE_BUTTON_3 = BUTTON3_PRESSED | BUTTON3_RELEASED;
const int NCURSES_MOUSE_BUTTON_PRESS = BUTTON1_PRESSED | BUTTON2_PRESSED | BUTTON3_PRESSED;
const int NCURSES_MOUSE_BUTTON_RELEASE = BUTTON1_RELEASED | BUTTON2_RELEASED | BUTTON3_RELEASED;
const int NCURSES_MOUSE_BUTTON = NCURSES_MOUSE_BUTTON_1 | NCURSES_MOUSE_BUTTON_2 | NCURSES_MOUSE_BUTTON_3 | BUTTON3_RELEASED;

static inline int PairForColour(int fg, int bg)
{
	return ((fg << 3) + bg) + 1;
}

static void InitColourPairs()
{
	for (int fg = 0; fg < 8; fg += 1)
	{
		for (int bg = 0; bg < 8; bg += 1)
		{
			int pair = PairForColour(fg, bg);
			init_pair(pair, fg, bg);
		}
	}
}

NcursesPlatform::NcursesPlatform()
{
	programStart = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	if (gInstances == 0)
	{
		gCursesWindow = initscr();
		start_color();
		InitColourPairs();
		lastColour = -1;
		cbreak();
		keypad(gCursesWindow, true);
		noecho();
		mousemask(NCURSES_MOUSE_BUTTON || REPORT_MOUSE_POSITION, nullptr);
	}
	gInstances += 1;
}

NcursesPlatform::~NcursesPlatform()
{
	gInstances -= 1;
	if (gInstances < 1)
	{
		attroff(0xffffffff);
		mousemask(0, nullptr);
		nocbreak();
		echo();
		keypad(gCursesWindow, false);
		curs_set(1);
		endwin();
	}
}

// input
void NcursesPlatform::ProcessInput()
{
	refresh();
}

void NcursesPlatform::GrabMouse(bool grabbed)
{
	// Do nothing.
}

bool NcursesPlatform::GrabMouse()
{
	return false;
}

bool NcursesPlatform::CanGrabMouse(bool grabbed)
{
	// We can only not have the mouse grabbed.
	return !grabbed;
}

void NcursesPlatform::TypingMode(bool typing)
{
	// Do nothing.
}

bool NcursesPlatform::TypingMode()
{
	return true;
}

bool NcursesPlatform::CanTypingMode(bool typing)
{
	// We can only be in typing mode.
	return typing;
}

// time
void NcursesPlatform::Delay(int ticks)
{
	timeout(ticks);
	while (ticks > 0)
	{
		uint64_t start = GetTime();
		int c = wgetch(gCursesWindow);
		if (c == ERR)
		{
			return;
		}
		if (c == KEY_MOUSE)
		{
			MEVENT event;
			if (getmouse(&event) == OK)
			{
				if (event.bstate & (NCURSES_MOUSE_BUTTON))
				{
					PlatformEvent platEvent;
					platEvent.type = PlatformEvent::PE_MouseButton;
					platEvent.event.pointerPress.position.Set(event.x, event.y);
					platEvent.event.pointerPress.press = event.bstate & NCURSES_MOUSE_BUTTON_PRESS;
					platEvent.event.pointerPress.button = 0;
					if (event.bstate & NCURSES_MOUSE_BUTTON_2)
					{
						platEvent.event.pointerPress.button = 1;
					}
					if (event.bstate & NCURSES_MOUSE_BUTTON_3)
					{
						platEvent.event.pointerPress.button = 2;
					}
					TriggerEvent(platEvent);
				}
				else
				{
					PlatformEvent platEvent;
					platEvent.type = PlatformEvent::PE_MouseMove;
					platEvent.event.pointerMove.relative = false;
					platEvent.event.pointerMove.endPosition.Set(event.x, event.y);
				}
			}
		}
		else
		{
			PlatformEvent platEvent;
			platEvent.type = PlatformEvent::PE_Key;
			platEvent.event.key.code = c;
			platEvent.event.key.unicode = c;
			platEvent.event.key.press = true;
			TriggerEvent(platEvent);
			platEvent.event.key.press = false;
			TriggerEvent(platEvent);
		}
		uint64_t end = GetTime();
		ticks -= (start - end);
	}
}

uint64_t NcursesPlatform::GetTime()
{
	uint64_t currentTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	return currentTime - programStart;
}

// terminal
static void GetConsoleDimensions(int *w, int *h)
{
#ifdef _WIN32
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
	*w = csbi.srWindow.Right - csbi.srWindow.Left + 1;
	*h = csbi.srWindow.Bottom - csbi.srWindow.Top + 1;
#else
	struct winsize winSize;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &winSize);
	*w = winSize.ws_col;
	*h = winSize.ws_row;
#endif
}

Vector2I NcursesPlatform::GetTerminalSize()
{
	int w, h;
	GetConsoleDimensions(&w, &h);
	return Vector2I(w, h);
}

Vector2I NcursesPlatform::GetCursorPos()
{
	int x, y;
	getyx(gCursesWindow, y, x);
	return Vector2I(x, y);
}

void NcursesPlatform::SetCursorPos(int x, int y)
{
	move(y, x);
}

void NcursesPlatform::PrintAt(int x, int y, std::string text)
{
	mvaddstr(y, x, text.c_str());
}

void NcursesPlatform::Print(std::string text)
{
	addstr(text.c_str());
}

void NcursesPlatform::SetBold(bool bold)
{
	if (bold)
	{
		attron(A_BOLD);
	}
	else
	{
		attroff(A_BOLD);
	}
}

void NcursesPlatform::SetDim(bool dim)
{
	if (dim)
	{
		attron(A_DIM);
	}
	else
	{
		attroff(A_DIM);
	}
}

void NcursesPlatform::SetUnderline(bool underline)
{
	if (underline)
	{
		attron(A_UNDERLINE);
	}
	else
	{
		attroff(A_UNDERLINE);
	}
}

void NcursesPlatform::SetBlink(bool blink)
{
	if (blink)
	{
		attron(A_BLINK);
	}
	else
	{
		attroff(A_BLINK);
	}
}

void NcursesPlatform::SetColour(int foreground, int background)
{
	if (lastColour >= 0)
	{
		attroff(COLOR_PAIR(lastColour));
	}
	lastColour = PairForColour(foreground, background);
	attron(COLOR_PAIR(lastColour));
}

void NcursesPlatform::Clear()
{
	bkgd(COLOR_PAIR(lastColour));
	//clear();
}

void NcursesPlatform::ShowCursor(bool show)
{
	curs_set(show ? 1 : 0);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
