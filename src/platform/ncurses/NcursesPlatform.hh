/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "platform/IPlatformVideo.hh"
#include "platform/IPlatformInput.hh"
#include "platform/IPlatformTime.hh"
#include "platform/AbstractPlatformEventGenerator.hh"
#include "terminal/ITerminalDisplay.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Wraps ncurses to provide input events and other things.
 * Not compatible with the ncurses adapter.
 * The video dimensions are in characters.
 */
class NcursesPlatform
	: public IPlatformInput
	, public IPlatformTime
	, public ITerminalDisplay
	, public AbstractPlatformEventGenerator
	{
public:
	NcursesPlatform();
	~NcursesPlatform();

	// input
	virtual void ProcessInput();
	virtual void GrabMouse(bool grabbed);
	virtual bool GrabMouse();
	virtual bool CanGrabMouse(bool grabbed);
	virtual void TypingMode(bool typing);
	virtual bool TypingMode();
	virtual bool CanTypingMode(bool typing);

	// time
	virtual void Delay(int ticks);
	virtual uint64_t GetTime();

	// terminal
	virtual Vector2I GetTerminalSize();
	virtual Vector2I GetCursorPos();
	virtual void SetCursorPos(int x, int y);
	virtual void PrintAt(int x, int y, std::string text);
	virtual void Print(std::string text);
	virtual void SetBold(bool bold);
	virtual void SetDim(bool dim);
	virtual void SetUnderline(bool underline);
	virtual void SetBlink(bool blink);
	virtual void SetColour(int foreground, int background);
	virtual void Clear();
	virtual void ShowCursor(bool show);

private:
	uint64_t programStart;
	int lastColour;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

