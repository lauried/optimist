/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "platform/TimeAdapter.hh"
#include <chrono>
#include <ctime>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

TimeAdapter::~TimeAdapter()
{
}

//-----------------------------------------------------------------------------
// IGamecodeAdapter
//-----------------------------------------------------------------------------

const std::string& TimeAdapter::GetName()
{
	static std::string adapterName("time");
	return adapterName;
}

std::map<std::string, IGamecodeAdapter::AdapterMethod> TimeAdapter::GetGlobalFunctions()
{
	return {
		// table GetTime()
		// returns table of sec, min, hour, weekday(0 = sunday), monthDay, yearDay, month(0 = jan), year, dst(daylight saving - bool)
		{ "getTime", std::bind(&TimeAdapter::GetTime, this, std::placeholders::_1, std::placeholders::_2) },

		// number TotalTime()
		// Returns the total time elapsed since the program start in milliseconds.
		{ "getProgramTime", std::bind(&TimeAdapter::GetProgramTime, this, std::placeholders::_1, std::placeholders::_2) },
	};
}

void TimeAdapter::OnRegister(IGamecode *gamecode)
{
}


//-----------------------------------------------------------------------------
// Callbacks
//-----------------------------------------------------------------------------

// returns a table of sec, min, hour, weekDay(0 = sunday), monthDay, yearDay, month(0=jan), year, dst(bool)
GamecodeValue TimeAdapter::GetTime(GamecodeParameterList *values, IGamecode* gamecode)
{
	GamecodeValue timeResult = gamecode->CreateTable();
	std::time_t rawTime;
	std::time(&rawTime);
	std::tm *tm = std::localtime(&rawTime);
	timeResult.GetTable()->Set(gamecode->CreateString("sec"), tm->tm_sec);
	timeResult.GetTable()->Set(gamecode->CreateString("min"), tm->tm_min);
	timeResult.GetTable()->Set(gamecode->CreateString("hour"), tm->tm_hour);
	timeResult.GetTable()->Set(gamecode->CreateString("dayOfWeek"), tm->tm_wday);
	timeResult.GetTable()->Set(gamecode->CreateString("dayOfMonth"), tm->tm_mday);
	timeResult.GetTable()->Set(gamecode->CreateString("dayOfYear"), tm->tm_yday);
	timeResult.GetTable()->Set(gamecode->CreateString("month"), tm->tm_mon);
	timeResult.GetTable()->Set(gamecode->CreateString("year"), (tm->tm_year + 1900));
	return timeResult;
}

GamecodeValue TimeAdapter::GetProgramTime(GamecodeParameterList *values, IGamecode* gamecode)
{
	return GamecodeValue((double)platform->GetTime());
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
