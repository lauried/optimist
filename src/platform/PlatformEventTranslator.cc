/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "platform/PlatformEventTranslator.hh"
#include "gamecode/library/GamecodeVector.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

IGamecodeTable *PlatformEventTranslator::GetGamecodeTable(IGamecode *gamecode)
{
	GamecodeValue tableValue = gamecode->CreateTable();
	IGamecodeTable *table = tableValue.GetTable();
	switch(event.type)
	{
	case PlatformEvent::PE_TouchMove:
	case PlatformEvent::PE_MouseMove:
		{
			Vector3F end(event.event.pointerMove.endPosition[0], event.event.pointerMove.endPosition[1], 0);
			Vector3F motion(event.event.pointerMove.motion[0], event.event.pointerMove.motion[1], 0);
			table->Set(gamecode->CreateString("type"), gamecode->CreateString(event.type == PlatformEvent::PE_MouseMove ? "MouseMove" : "TouchMove"));
			table->Set(gamecode->CreateString("relative"), GamecodeValue((int)(event.event.pointerMove.relative ? 1 : 0)));
			table->Set(gamecode->CreateString("end"), GamecodeValue(GamecodeVector::Create(end)));
			table->Set(gamecode->CreateString("endX"), GamecodeValue(end[0]));
			table->Set(gamecode->CreateString("endY"), GamecodeValue(end[1]));
			table->Set(gamecode->CreateString("motion"), GamecodeValue(GamecodeVector::Create(motion)));
			table->Set(gamecode->CreateString("motionX"), GamecodeValue(motion[0]));
			table->Set(gamecode->CreateString("motionY"), GamecodeValue(motion[1]));
		}
		break;
	case PlatformEvent::PE_Touch:
	case PlatformEvent::PE_MouseButton:
		{
			Vector3F position(event.event.pointerPress.position[0], event.event.pointerPress.position[1], 1);
			table->Set(gamecode->CreateString("type"), gamecode->CreateString(event.type == PlatformEvent::PE_MouseButton ? "MouseButton" : "Touch"));
			table->Set(gamecode->CreateString("press"), GamecodeValue((int)(event.event.pointerPress.press ? 1 : 0)));
			table->Set(gamecode->CreateString("position"), GamecodeValue(GamecodeVector::Create(position)));
			table->Set(gamecode->CreateString("positionX"), GamecodeValue(position[0]));
			table->Set(gamecode->CreateString("positionY"), GamecodeValue(position[1]));
			table->Set(gamecode->CreateString("button"), GamecodeValue(event.event.pointerPress.button));
		}
		break;
	case PlatformEvent::PE_Key:
		{
			table->Set(gamecode->CreateString("type"), gamecode->CreateString("Key"));
			table->Set(gamecode->CreateString("press"), GamecodeValue((int)(event.event.key.press ? 1 : 0)));
			table->Set(gamecode->CreateString("code"), GamecodeValue(event.event.key.code));
			table->Set(gamecode->CreateString("unicode"), GamecodeValue(event.event.key.unicode));
		}
		break;
	case PlatformEvent::PE_WindowClose:
		{
			table->Set(gamecode->CreateString("type"), gamecode->CreateString("WindowClose"));
		}
		break;
	case PlatformEvent::PE_WindowResize:
		{
			table->Set(gamecode->CreateString("type"), gamecode->CreateString("WindowResize"));
		}
		break;
	case PlatformEvent::PE_WindowExpose:
		{
			table->Set(gamecode->CreateString("type"), gamecode->CreateString("WindowExpose"));
		}
		break;
	}

	return table;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
