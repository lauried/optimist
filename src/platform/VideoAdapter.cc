/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "platform/VideoAdapter.hh"
#include "gamecode/library/GamecodeVector.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

VideoAdapter::~VideoAdapter()
{
}

//-----------------------------------------------------------------------------
// IGamecodeAdapter
//-----------------------------------------------------------------------------

const std::string& VideoAdapter::GetName()
{
	static std::string adapterName("video");
	return adapterName;
}

std::map<std::string, IGamecodeAdapter::AdapterMethod> VideoAdapter::GetGlobalFunctions()
{
	return {
		// void SetVideoMode(w, h, fullscreen, caption) [TODO: Make a member function for it.]
		{ "setVideoMode", std::bind(&VideoAdapter::SetVideoMode, this, std::placeholders::_1, std::placeholders::_2) },

		// table GetWindowSize()
		// Returns a GamecodeVector representing the size of the program window.
		{ "getWindowSize", std::bind(&VideoAdapter::GetWindowSize, this, std::placeholders::_1, std::placeholders::_2) },
	};
}

void VideoAdapter::OnRegister(IGamecode *gamecode)
{
}

//-----------------------------------------------------------------------------
// Callbacks
//-----------------------------------------------------------------------------

// w, h, fullscreen, caption
GamecodeValue VideoAdapter::SetVideoMode(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(2, 4);

	int w = 0;
	int h = 0;
	bool fullScreen = false;
	std::string caption;

	switch (values->size())
	{
	case 4:
		caption = (*values)[3].GetString();
	case 3:
		fullScreen = (*values)[2].GetInt();
	case 2:
		h = (*values)[1].GetInt();
		w = (*values)[0].GetInt();
	}
	platform->SetVideoMode(w, h, fullScreen, caption);

	// TODO: Return true if we were able to change mode, false if we reverted
	// and throw an engine exception if we were unable to change back.

	return GamecodeValue(1);
}

GamecodeValue VideoAdapter::GetWindowSize(GamecodeParameterList *values, IGamecode* gamecode)
{
	Vector2I screenSize = platform->GetScreenSize();
	return GamecodeValue(GamecodeVector::Create(screenSize[0], screenSize[1], 0));
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
