/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "library/geometry/Vector2.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// TODO: Let this be one struct per event type.

struct PlatformEvent {
	enum EventTypeFlags {
		PE_None = 0, // no event represented (could've been cancelled by something up the chain)
		PE_MouseMove = 1, // pointerMove
		PE_MouseButton = 2, // pointerPress
		PE_Key = 4, // key
		PE_Touch = 8, // pointerPress
		PE_TouchMove = 16, // pointerMove
		// TODO: Joystick input
		// TODO: Changes to available devices
		PE_WindowClose = 1024, // When the X was pressed on the OS window. No data for this event.
		PE_WindowResize = 2048, // No data - this can be read from the platform object by listeners.
		PE_WindowExpose = 4096,
		PE_All = 65535,
	};

	struct PointerMoveData {
		bool relative;
		Vector2I endPosition;
		Vector2I motion;
	};

	struct PointerPressData {
		bool press; // versus release
		Vector2I position;
		int button; // 0=left/default, 1=right, 2=middle, 3=wheelup, 4=wheeldown
	};

	struct KeyData {
		bool press;
		int code;
		int unicode; // unicodes are only provided on the downstroke
	};

	struct EventData {
		PointerMoveData pointerMove;
		PointerPressData pointerPress;
		KeyData key;
	};

	EventTypeFlags type; // This is only one type.
	EventData event;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

