/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "platform/IPlatform.hh"
#include "platform/IPlatformVideo.hh"
#include "platform/IPlatformInput.hh"
#include "platform/IPlatformInfo.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class SdlPlatform : public IPlatform {
public:
	SdlPlatform(IPlatformInfo *info);
	~SdlPlatform();

	// common
	void Exit();

	// folders
	std::string GetUserLocalAppDataFolder(std::string organisation, std::string game);
	std::string GetUserRoamingAppDataFolder(std::string organisation, std::string game);
	std::string GetUserDocumentsFolder(std::string organisation, std::string game);

	// video
	void SetVideoMode(int w, int h, bool fullscreen, std::string caption);
	Vector2I GetScreenSize();
	Vector2I RecommendWindowSize();
	void SwapBuffers();

	// time
	uint64_t GetTime();
	void Delay(int ticks);

	// input
	void ProcessInput();
	void GrabMouse(bool grabbed);
	bool GrabMouse();
	bool CanGrabMouse(bool grabbed);
	void TypingMode(bool typing);
	bool TypingMode();
	bool CanTypingMode(bool typing);

private:
	IPlatformInfo *platformInfo;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
