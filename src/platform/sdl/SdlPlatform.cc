/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "platform/sdl/SdlPlatform.hh"
#include <map>
#include <iostream>
#include <SDL/SDL.h>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//## add_libraries SDL
#ifdef _WIN32
//## add_libraries opengl32
#else
//## add_libraries GL
#endif

static Vector2I gScreenSize;

static Vector2I gCursorPosition;

static bool gMouseGrabbed;
static Vector2I gMouseGrabStart;

//-----------------------------------------------------------------------------
// Constructor/Destructor
//-----------------------------------------------------------------------------

SdlPlatform::SdlPlatform(IPlatformInfo *info)
{
	platformInfo = info;
}

SdlPlatform::~SdlPlatform()
{
}

//-----------------------------------------------------------------------------
// Common
//-----------------------------------------------------------------------------

void SdlPlatform::Exit()
{
	GrabMouse(false);
	SDL_Quit();
}

//-----------------------------------------------------------------------------
// Folders
//-----------------------------------------------------------------------------

std::string SdlPlatform::GetUserLocalAppDataFolder(std::string organisation, std::string game)
{
	return platformInfo->GetUserLocalAppDataFolder(organisation, game);
}

std::string SdlPlatform::GetUserRoamingAppDataFolder(std::string organisation, std::string game)
{
	return platformInfo->GetUserRoamingAppDataFolder(organisation, game);
}

std::string SdlPlatform::GetUserDocumentsFolder(std::string organisation, std::string game)
{
	return platformInfo->GetUserDocumentsFolder(organisation, game);
}

//-----------------------------------------------------------------------------
// Video
//-----------------------------------------------------------------------------

static bool CheckInitSystems(Uint32 systems);

void SdlPlatform::SetVideoMode(int w, int h, bool fullscreen, std::string caption)
{
	int flags = SDL_OPENGL | SDL_HWSURFACE;
	if (fullscreen) flags |= SDL_FULLSCREEN;
	SDL_SetVideoMode(w, h, 0, flags);
	gScreenSize.Set(w, h);
	SDL_WM_SetCaption(caption.c_str(), "");
}

Vector2I SdlPlatform::GetScreenSize()
{
	return gScreenSize;
}

Vector2I SdlPlatform::RecommendWindowSize()
{
	Vector2I desktopSize;
	if (CheckInitSystems(SDL_INIT_VIDEO))
	{
		const SDL_VideoInfo *videoInfo = SDL_GetVideoInfo();
		desktopSize.Set(videoInfo->current_w, videoInfo->current_h);
	}

	if (desktopSize[0] - 16 > 1024 && desktopSize[1] - 96 > 768)
	{
		return Vector2I(1024, 768);
	}
	else if (desktopSize[0] - 16 > 800 && desktopSize[1] - 96 > 600)
	{
		return Vector2I(800, 600);
	}
	return Vector2I(640, 480);
}

void SdlPlatform::SwapBuffers()
{
	SDL_GL_SwapBuffers();
}

//-----------------------------------------------------------------------------
// Time
//-----------------------------------------------------------------------------

uint64_t SdlPlatform::GetTime()
{
	return (uint64_t)SDL_GetTicks();
}

void SdlPlatform::Delay(int ticks)
{
	SDL_Delay(ticks);
}

//-----------------------------------------------------------------------------
// Events
//-----------------------------------------------------------------------------

void SdlPlatform::ProcessInput()
{
	PlatformEvent platEvent;
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
		case SDL_QUIT:
			platEvent.type = PlatformEvent::PE_WindowClose;
			TriggerEvent(platEvent);
			break;

		case SDL_VIDEORESIZE:
			platEvent.type = PlatformEvent::PE_WindowResize;
			TriggerEvent(platEvent);
			break;

		case SDL_VIDEOEXPOSE:
			platEvent.type = PlatformEvent::PE_WindowExpose;
			TriggerEvent(platEvent);
			break;

		case SDL_KEYDOWN:
		case SDL_KEYUP:
			platEvent.type = PlatformEvent::PE_Key;
			platEvent.event.key.press = (event.type == SDL_KEYDOWN);
			platEvent.event.key.code = event.key.keysym.sym;
			platEvent.event.key.unicode = event.key.keysym.unicode;
			TriggerEvent(platEvent);
			break;

		case SDL_MOUSEMOTION:
			gCursorPosition.Set(event.motion.x, event.motion.y);
			if (gMouseGrabbed)
			{
				if (event.motion.x == gScreenSize[0] / 2 && event.motion.y == gScreenSize[1] / 2)
				{
					break;
				}
				platEvent.type = PlatformEvent::PE_MouseMove;
				platEvent.event.pointerMove.relative = true;
				platEvent.event.pointerMove.endPosition = gMouseGrabStart;
				platEvent.event.pointerMove.motion.Set(event.motion.xrel, event.motion.yrel);
				TriggerEvent(platEvent);
				SDL_WarpMouse(gScreenSize[0] / 2, gScreenSize[1] / 2);
			}
			else
			{
				platEvent.type = PlatformEvent::PE_MouseMove;
				platEvent.event.pointerMove.relative = false;
				platEvent.event.pointerMove.endPosition = gCursorPosition;
				platEvent.event.pointerMove.motion.Set(event.motion.xrel, event.motion.yrel);
				TriggerEvent(platEvent);
			}
			break;

		case SDL_MOUSEBUTTONDOWN:
		case SDL_MOUSEBUTTONUP:
			int buttonNum = 0;
			switch (event.button.button)
			{
				case SDL_BUTTON_LEFT:      buttonNum = 0; break;
				case SDL_BUTTON_RIGHT:     buttonNum = 1; break;
				case SDL_BUTTON_MIDDLE:    buttonNum = 2; break;
				case SDL_BUTTON_WHEELUP:   buttonNum = 3; break;
				case SDL_BUTTON_WHEELDOWN: buttonNum = 4; break;
			}
			platEvent.type = PlatformEvent::PE_MouseButton;
			platEvent.event.pointerPress.press = (event.type == SDL_MOUSEBUTTONDOWN);
			platEvent.event.pointerPress.position = gCursorPosition;
			platEvent.event.pointerPress.button = buttonNum;
			TriggerEvent(platEvent);
			break;
		}
	}
}

void SdlPlatform::GrabMouse(bool grabbed)
{
	if (grabbed == gMouseGrabbed)
		return;

	if (grabbed)
	{
		gMouseGrabStart = gCursorPosition;
		SDL_ShowCursor(SDL_DISABLE);
		//SDL_WM_GrabInput(SDL_GRAB_ON);
	}
	else
	{
		SDL_WarpMouse(gMouseGrabStart[0], gMouseGrabStart[1]);
		SDL_ShowCursor(SDL_ENABLE);
		//SDL_WM_GrabInput(SDL_GRAB_OFF);
	}

	gMouseGrabbed = grabbed;
}

bool SdlPlatform::GrabMouse()
{
	return gMouseGrabbed;
}

bool SdlPlatform::CanGrabMouse(bool grabbed)
{
	return true;
}

void SdlPlatform::TypingMode(bool typing)
{
}

bool SdlPlatform::TypingMode()
{
	return false;
}

bool SdlPlatform::CanTypingMode(bool typing)
{
	return !typing;
}

//-----------------------------------------------------------------------------
// Internal Helpers
//-----------------------------------------------------------------------------

static bool CheckInitSystems(Uint32 systems)
{
	Uint32 initSystems = SDL_WasInit(SDL_INIT_EVERYTHING);
	if (initSystems == 0)
	{
		SDL_Init(systems | SDL_INIT_NOPARACHUTE);
		SDL_EnableUNICODE(SDL_ENABLE);
		return true;
	}
	systems = systems - (initSystems & systems);
	if (systems == 0)
	{
		return false;
	}
	SDL_InitSubSystem(systems);
	return true;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
