/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "platform/WindowsPlatformInfo.hh"
#include <shlobj.h>
#include <winerror.h>
#include <atlbase.h>
#include <sstream>
#include "library/text/Path.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

static std::string GetWindowsFolder(REFKNOWNFOLDERID rfid)
{
	wchar_t* localAppData = 0;
	SHGetKnownFolderPath(rfid, 0, NULL, &localAppData);
	std::stringstream ss;
	if (localAppData != nullptr)
	{
		ss << localAppData;
		CoTaskMemFree(static_cast<void*>(localAppData));
	}
	return ss.str();
}

std::string WindowsPlatformInfo::GetUserLocalAppDataFolder(std::string organisation, std::string game)
{
	return Path::Join({ GetWindowsFolder(FOLDERID_LocalAppData), organisation, game });
}

std::string WindowsPlatformInfo::GetUserRoamingAppDataFolder(std::string organisation, std::string game)
{
	return Path::Join({ GetWindowsFolder(FOLDERID_RoamingAppData), organisation, game });
}

std::string WindowsPlatformInfo::GetUserDocumentsFolder(std::string organisation, std::string game)
{
	return Path::Join({ GetWindowsFolder(FOLDERID_Documents), organisation, game });
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
