/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class IPlatformInput {
public:
	/**
	 * Polls for input events and triggers their events in this thread.
	 */
	virtual void ProcessInput() = 0;

	/**
	 * If grabbed is true, attempts to set the mouse mode to relative motion,
	 * confining the mouse pointer to the window and hiding the cursor.
	 * If grabbed is false, sets the mouse mode to absolute motion, the default
	 * mode for a windowing system.
	 */
	virtual void GrabMouse(bool grabbed) = 0;

	/**
	 * Returns the current mode for GrabMouse.
	 */
	virtual bool GrabMouse() = 0;
	/**
	 * Returns true if the platform accepts the given parameter for GrabMouse,
	 * else false.
	 */
	virtual bool CanGrabMouse(bool grabbed) = 0;

	/**
	 * If typing is true, then key presses will act like a keyboard and
	 * automatically repeat. If false then they won't, allowing presses and
	 * releases to be detected.
	 */
	virtual void TypingMode(bool typing) = 0;

	/**
	 * Returns the current typing mode.
	 */
	virtual bool TypingMode() = 0;

	/**
	 * Returns true if the platform accepts the given typing mode, else false.
	 */
	virtual bool CanTypingMode(bool typing) = 0;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

