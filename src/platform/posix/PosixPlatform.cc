/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "platform/posix/PosixPlatform.hh"
#include <chrono>
#include <thread>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

PosixPlatform::PosixPlatform()
{
	programStart = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

void PosixPlatform::Delay(int ticks)
{
	std::this_thread::sleep_for(std::chrono::milliseconds{ticks});
}

uint64_t PosixPlatform::GetTime()
{
	uint64_t currentTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	return currentTime - programStart;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
