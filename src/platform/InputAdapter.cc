/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "platform/InputAdapter.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

InputAdapter::~InputAdapter()
{
}

//-----------------------------------------------------------------------------
// IGamecodeAdapter
//-----------------------------------------------------------------------------

const std::string& InputAdapter::GetName()
{
	static std::string adapterName("input");
	return adapterName;
}

std::map<std::string, IGamecodeAdapter::AdapterMethod> InputAdapter::GetGlobalFunctions()
{
	return {
		// void GrabMouse(bool)
		// bool GrabMouse()
		// [true = grabbed]
		{ "grabMouse", std::bind(&InputAdapter::GrabMouse, this, std::placeholders::_1, std::placeholders::_2) },

		// void TypingMode(bool)
		// bool TypingMode()
		// [true = typing]
		{ "typingMode", std::bind(&InputAdapter::TypingMode, this, std::placeholders::_1, std::placeholders::_2) },
	};
}

void InputAdapter::OnRegister(IGamecode *gamecode)
{
}

//-----------------------------------------------------------------------------
// Callbacks
//-----------------------------------------------------------------------------

// bool, true=grabbed
GamecodeValue InputAdapter::GrabMouse(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(0, 1);
	if (values->size() == 1)
	{
		bool grabbed = values->RequireType(0, GamecodeValue::T_Bool).GetBool();
		platform->GrabMouse(grabbed);
		return GamecodeValue();
	}
	bool grabbed = platform->GrabMouse();
	return GamecodeValue(grabbed);
}

// bool, true=typing
GamecodeValue InputAdapter::TypingMode(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(0, 1);
	if (values->size() == 1)
	{
		bool typing = values->RequireType(0, GamecodeValue::T_Bool).GetBool();
		platform->GrabMouse(typing);
		return GamecodeValue();
	}
	bool typing = platform->GrabMouse();
	return GamecodeValue(typing);
}


//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
