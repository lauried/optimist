/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "platform/IPlatformInfo.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class LinuxPlatformInfo : public IPlatformInfo {
public:
	/**
	 * Returns the folder within which it is acceptable to store data which
	 * may be too large to carry around in the user's roaming profile.
	 *
	 * The user code must create a subfolder within this.
	 *
	 * The other platform info methods may return the same folder as this.
	 */
	virtual std::string GetUserLocalAppDataFolder(std::string organisation, std::string game);

	/**
	 * Returns the folder within which it is acceptable to store data which is
	 * carried around in the user's roaming profile.
	 *
	 * The user code must create a subfolder within this.
	 *
	 * The other platform info methods may return the same folder as this.
	 */
	virtual std::string GetUserRoamingAppDataFolder(std::string organisation, std::string game);

	/**
	 * Returns the folder within which it is acceptable to store documents
	 * saved by the user which they would want to access through their file
	 * manager.
	 *
	 * The other platform info methods may return the same folder as this.
	 */
	virtual std::string GetUserDocumentsFolder(std::string organisation, std::string game);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

