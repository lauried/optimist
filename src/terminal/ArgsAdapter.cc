/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "terminal/ArgsAdapter.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

ArgsAdapter::ArgsAdapter(int argc, char *argv[])
{
	for (int i = 0; i < argc; i += 1)
	{
		args.push_back(std::string(argv[i]));
	}
}

ArgsAdapter::~ArgsAdapter()
{
}

const std::string& ArgsAdapter::GetName()
{
	static std::string name("args");
	return name;
}

std::map<std::string, IGamecodeAdapter::AdapterMethod> ArgsAdapter::GetGlobalFunctions()
{
	return {
		// initscr()
		{ "numArgs", std::bind(&ArgsAdapter::NumArgs, this, std::placeholders::_1, std::placeholders::_2) },
		// endwin()
		{ "getArg", std::bind(&ArgsAdapter::GetArg, this, std::placeholders::_1, std::placeholders::_2) },
	};
}

void ArgsAdapter::OnRegister(IGamecode *gamecode)
{
}

GamecodeValue ArgsAdapter::NumArgs(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(0);
	return GamecodeValue((int)args.size());
}

GamecodeValue ArgsAdapter::GetArg(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(0);
	int index = values->RequireType(0, GamecodeValue::T_Number).GetInt();
	if (index < 0 || index >= args.size())
	{
		return GamecodeValue();
	}
	return GamecodeValue(gamecode->CreateString(args[index].c_str()));
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
