/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "terminal/NcursesAdapter.hh"
#include <cstring>
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//## add_libraries ncurses

NcursesAdapter::NcursesAdapter()
{
	cursesWindow = nullptr;
	colourStarted = false;
	nextPair = 1;
}

NcursesAdapter::~NcursesAdapter()
{
	if (cursesWindow != nullptr)
	{
		endwin();
	}

	attroff(0xffffffff);
}

const std::string& NcursesAdapter::GetName()
{
	static std::string name("ncurses");
	return name;
}

std::map<std::string, IGamecodeAdapter::AdapterMethod> NcursesAdapter::GetGlobalFunctions()
{
	return {
		// initscr()
		{ "initscr", std::bind(&NcursesAdapter::InitScr, this, std::placeholders::_1, std::placeholders::_2) },
		// endwin()
		{ "endwin", std::bind(&NcursesAdapter::EndWin, this, std::placeholders::_1, std::placeholders::_2) },

		// setCbreak(bool enable)
		{ "setCbreak", std::bind(&NcursesAdapter::SetCbreak, this, std::placeholders::_1, std::placeholders::_2) },
		// setKeypad(bool enable)
		{ "setKeypad", std::bind(&NcursesAdapter::SetKeypad, this, std::placeholders::_1, std::placeholders::_2) },
		// setEcho(bool enable)
		{ "setEcho", std::bind(&NcursesAdapter::SetEcho, this, std::placeholders::_1, std::placeholders::_2) },
		// setTimeout(number milliseconds)
		{ "setTimeout", std::bind(&NcursesAdapter::SetTimeout, this, std::placeholders::_1, std::placeholders::_2) },

		// attrOn(attr1, attr2, attr3) etc.
		{ "attrOn", std::bind(&NcursesAdapter::AttrOn, this, std::placeholders::_1, std::placeholders::_2) },
		// attrOff(attr1, attr2, attr3) etc.
		{ "attrOff", std::bind(&NcursesAdapter::AttrOff, this, std::placeholders::_1, std::placeholders::_2) },
		// setAttr(attr1, attr2, attr3) etc.
		{ "setAttr", std::bind(&NcursesAdapter::SetAttr, this, std::placeholders::_1, std::placeholders::_2) },
		// bool isAttr(attr)
		{ "isAttr", std::bind(&NcursesAdapter::IsAttr, this, std::placeholders::_1, std::placeholders::_2) },

		// setColour(fg, bg) - automatically starts colour and automatically makes a pair
		{ "setColour", std::bind(&NcursesAdapter::SetColour, this, std::placeholders::_1, std::placeholders::_2) },

		// print([x, y], string)
		{ "print", std::bind(&NcursesAdapter::Print, this, std::placeholders::_1, std::placeholders::_2) },

		// string getch()
		{ "getch", std::bind(&NcursesAdapter::Getch, this, std::placeholders::_1, std::placeholders::_2) },

		// int getRows()
		{ "getRows", std::bind(&NcursesAdapter::GetRows, this, std::placeholders::_1, std::placeholders::_2) },
		// int getCols()
		{ "getCols", std::bind(&NcursesAdapter::GetCols, this, std::placeholders::_1, std::placeholders::_2) },
		// int getRow()
		{ "getRow", std::bind(&NcursesAdapter::GetRow, this, std::placeholders::_1, std::placeholders::_2) },
		// int getCol()
		{ "getCol", std::bind(&NcursesAdapter::GetCol, this, std::placeholders::_1, std::placeholders::_2) },

		// refresh()
		{ "refresh", std::bind(&NcursesAdapter::Refresh, this, std::placeholders::_1, std::placeholders::_2) },
	};
}

void NcursesAdapter::OnRegister(IGamecode *gamecode)
{
}

//-----------------------------------------------------------------------------
// Callbacks
//-----------------------------------------------------------------------------

GamecodeValue NcursesAdapter::InitScr(GamecodeParameterList *values, IGamecode* gamecode)
{
	if (cursesWindow != nullptr)
	{
		return GamecodeValue();
	}

	values->RequireParmCount(0);
	cursesWindow = initscr();

	return GamecodeValue();
}

GamecodeValue NcursesAdapter::EndWin(GamecodeParameterList *values, IGamecode* gamecode)
{
	if (cursesWindow == nullptr)
	{
		return GamecodeValue();
	}

	values->RequireParmCount(0);
	endwin();
	cursesWindow = nullptr;

	return GamecodeValue();
}

GamecodeValue NcursesAdapter::SetCbreak(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(1);
	bool state = values->RequireType(0, GamecodeValue::T_Bool | GamecodeValue::T_Number).GetBool();

	if (cursesWindow == nullptr)
	{
		return GamecodeValue();
	}

	if (state)
	{
		cbreak();
	}
	else
	{
		nocbreak();
	}

	return GamecodeValue();
}

GamecodeValue NcursesAdapter::SetKeypad(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(1);
	bool state = values->RequireType(0, GamecodeValue::T_Bool | GamecodeValue::T_Number).GetBool();

	if (cursesWindow == nullptr)
	{
		return GamecodeValue();
	}

	keypad(cursesWindow, state);

	return GamecodeValue();
}

GamecodeValue NcursesAdapter::SetEcho(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(1);
	bool state = values->RequireType(0, GamecodeValue::T_Bool | GamecodeValue::T_Number).GetBool();

	if (cursesWindow == nullptr)
	{
		return GamecodeValue();
	}

	if (state)
	{
		echo();
	}
	else
	{
		noecho();
	}

	return GamecodeValue();
}

GamecodeValue NcursesAdapter::SetTimeout(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(1);
	GamecodeValue value = values->RequireType(0, GamecodeValue::T_Number);

	if (cursesWindow == nullptr)
	{
		return GamecodeValue();
	}

	timeout(value.GetInt());

	return GamecodeValue();
}

static int AttrForString(const char *str)
{
	if (!std::strcmp(str, "normal"))
	{
		return A_NORMAL;
	}
	else if (!std::strcmp(str, "standout"))
	{
		return A_STANDOUT;
	}
	else if (!std::strcmp(str, "underline"))
	{
		return A_UNDERLINE;
	}
	else if (!std::strcmp(str, "reverse"))
	{
		return A_REVERSE;
	}
	else if (!std::strcmp(str, "blink"))
	{
		return A_BLINK;
	}
	else if (!std::strcmp(str, "dim"))
	{
		return A_DIM;
	} else if (!std::strcmp(str, "bold"))
	{
		return A_BOLD;
	}
	else if (!std::strcmp(str, "protect"))
	{
		return A_PROTECT;
	}
	else if (!std::strcmp(str, "invis"))
	{
		return A_INVIS;
	}
	else if (!std::strcmp(str, "altcharset"))
	{
		return A_ALTCHARSET;
	}
	else if (!std::strcmp(str, "italic"))
	{
		return A_ITALIC;
	}
	else if (!std::strcmp(str, "chartext"))
	{
		return A_CHARTEXT;
	}
	return 0;
}

GamecodeValue NcursesAdapter::AttrOn(GamecodeParameterList *values, IGamecode* gamecode)
{
	int attrs = 0;
	for (int i = 0; i < values->size(); i += 1)
	{
		const char *str = values->RequireType(i, GamecodeValue::T_String).GetString();
		attrs |= AttrForString(str);
	}

	if (cursesWindow == nullptr)
	{
		return GamecodeValue();
	}

	attron(attrs);

	return GamecodeValue();
}

GamecodeValue NcursesAdapter::AttrOff(GamecodeParameterList *values, IGamecode* gamecode)
{
	int attrs = 0;
	for (int i = 0; i < values->size(); i += 1)
	{
		const char *str = values->RequireType(i, GamecodeValue::T_String).GetString();
		attrs |= AttrForString(str);
	}

	if (cursesWindow == nullptr)
	{
		return GamecodeValue();
	}

	attroff(attrs);

	return GamecodeValue();
}

GamecodeValue NcursesAdapter::SetAttr(GamecodeParameterList *values, IGamecode* gamecode)
{
	int attrs = 0;
	for (int i = 0; i < values->size(); i += 1)
	{
		const char *str = values->RequireType(i, GamecodeValue::T_String).GetString();
		attrs |= AttrForString(str);
	}

	if (cursesWindow == nullptr)
	{
		return GamecodeValue();
	}

	attrset(attrs);

	return GamecodeValue();
}

GamecodeValue NcursesAdapter::IsAttr(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(1);
	const char *str = values->RequireType(0, GamecodeValue::T_String).GetString();

	if (cursesWindow == nullptr)
	{
		return GamecodeValue();
	}

	return GamecodeValue(AttrForString(str) != 0);
}

static int ColourForString(const char *str)
{
	if (!std::strcmp(str, "black"))
	{
		return COLOR_BLACK;
	}
	else if (!std::strcmp(str, "red"))
	{
		return COLOR_RED;
	}
	else if (!std::strcmp(str, "green"))
	{
		return COLOR_GREEN;
	}
	else if (!std::strcmp(str, "yellow"))
	{
		return COLOR_YELLOW;
	}
	else if (!std::strcmp(str, "blue"))
	{
		return COLOR_BLUE;
	}
	else if (!std::strcmp(str, "magenta"))
	{
		return COLOR_MAGENTA;
	}
	else if (!std::strcmp(str, "cyan"))
	{
		return COLOR_CYAN;
	}
	else if (!std::strcmp(str, "white"))
	{
		return COLOR_WHITE;
	}
	return 0;
}

GamecodeValue NcursesAdapter::SetColour(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(2);
	GamecodeValue fg = values->RequireType(0, GamecodeValue::T_String | GamecodeValue::T_Number);
	GamecodeValue bg = values->RequireType(1, GamecodeValue::T_String | GamecodeValue::T_Number);

	if (! colourStarted)
	{
		start_color();
	}

	int ifg = fg.IsNumber() ? fg.GetInt() : ColourForString(fg.GetString());
	int ibg = bg.IsNumber() ? bg.GetInt() : ColourForString(bg.GetString());

	int combined = ifg << 16 + ibg;

	auto it = colourPairs.find(combined);
	if (it == colourPairs.end())
	{
		init_pair(nextPair, ifg, ibg);
		colourPairs.insert(std::make_pair(combined, nextPair));
		attron(COLOR_PAIR(nextPair));
		nextPair += 1;
		return GamecodeValue();
	}

	attron(COLOR_PAIR(it->second));

	return GamecodeValue();
}

GamecodeValue NcursesAdapter::Print(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(1, 3);
	values->RequireNotParmCount(2);

	if (values->size() == 1)
	{
		const char *str = values->RequireType(0, GamecodeValue::T_String).GetString();

		if (cursesWindow == nullptr)
		{
			return GamecodeValue();
		}

		addstr(str);

		return GamecodeValue();
	}

	int y = values->RequireType(0, GamecodeValue::T_Number).GetInt();
	int x = values->RequireType(1, GamecodeValue::T_Number).GetInt();
	const char *str = values->RequireType(2, GamecodeValue::T_String).GetString();
	mvaddstr(y, x, str);

	return GamecodeValue();
}

GamecodeValue NcursesAdapter::Refresh(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(0);

	if (cursesWindow == nullptr)
	{
		return GamecodeValue();
	}

	refresh();

	return GamecodeValue();
}

GamecodeValue NcursesAdapter::GetRows(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(0);

	if (cursesWindow == nullptr)
	{
		return GamecodeValue();
	}

	int x, y;
	getmaxyx(cursesWindow, y, x);

	return GamecodeValue(y);
}

GamecodeValue NcursesAdapter::GetCols(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(0);

	if (cursesWindow == nullptr)
	{
		return GamecodeValue();
	}

	int x, y;
	getmaxyx(cursesWindow, y, x);

	return GamecodeValue(x);
}

GamecodeValue NcursesAdapter::GetRow(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(0);

	if (cursesWindow == nullptr)
	{
		return GamecodeValue();
	}

	int x, y;
	getyx(cursesWindow, y, x);

	return GamecodeValue(y);
}

GamecodeValue NcursesAdapter::GetCol(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(0);

	if (cursesWindow == nullptr)
	{
		return GamecodeValue();
	}

	int x, y;
	getyx(cursesWindow, y, x);

	return GamecodeValue(x);
}

GamecodeValue NcursesAdapter::Getch(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(0);

	if (cursesWindow == nullptr)
	{
		return GamecodeValue();
	}

	//char c[2];
	//c[0] = getch();
	//c[1] = '\0';

	//return gamecode->CreateString(c);
	return GamecodeValue((int)getch());
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
