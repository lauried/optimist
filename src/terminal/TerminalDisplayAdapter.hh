/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gamecode/IGamecode.hh"
#include "terminal/ITerminalDisplay.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class TerminalDisplayAdapter : public IGamecodeAdapter {
public:
	TerminalDisplayAdapter(ITerminalDisplay *display)
		: display(display)
		{}

	// IGamecodeAdapter
	virtual const std::string& GetName();
	virtual std::map<std::string, IGamecodeAdapter::AdapterMethod> GetGlobalFunctions();
	virtual void OnRegister(IGamecode *gamecode);

	// Callbacks
	GamecodeValue GetTerminalSize(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue GetCursorPos(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue SetCursorPos(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue PrintAt(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue Print(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue SetBold(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue SetDim(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue SetUnderline(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue SetBlink(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue SetColour(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue Clear(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue ShowCursor(GamecodeParameterList *values, IGamecode* gamecode);

private:
	ITerminalDisplay *display;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

