/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "gamecode/IGamecode.hh"
#include <curses.h>
#include <unordered_map>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * A simplified API that wraps around Ncurses.
 * Ncurses documentation is a bit vague, and trying to do things in it don't
 * seem to actually work. This adapter will provide an interface that enables
 * the use of ncurses but reduces the number of ways in which trying to do
 * anything will fail.
 *
 * The interface can be expanded by letting us create objects that encapsulate
 * ncurses windows.
 */
class NcursesAdapter : public IGamecodeAdapter {
public:
	NcursesAdapter();
	~NcursesAdapter();

	// IGamecodeAdapter
	virtual const std::string& GetName();
	virtual std::map<std::string, IGamecodeAdapter::AdapterMethod> GetGlobalFunctions();
	virtual void OnRegister(IGamecode *gamecode);

	// Callbacks

	// curs_initscr
	GamecodeValue InitScr(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue EndWin(GamecodeParameterList *values, IGamecode* gamecode);

	// curs_inops - these are condensed from separate on/off functions
	GamecodeValue SetCbreak(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue SetKeypad(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue SetEcho(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue SetTimeout(GamecodeParameterList *values, IGamecode* gamecode);

	// curs_attr
	GamecodeValue AttrOn(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue AttrOff(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue SetAttr(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue IsAttr(GamecodeParameterList *values, IGamecode* gamecode);

	// curs_color
	GamecodeValue SetColour(GamecodeParameterList *values, IGamecode* gamecode);

	// curs_addstr
	GamecodeValue Print(GamecodeParameterList *values, IGamecode* gamecode);

	// curs_getch
	GamecodeValue Getch(GamecodeParameterList *values, IGamecode* gamecode);

	// curs_getyx
	GamecodeValue GetRows(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue GetCols(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue GetRow(GamecodeParameterList *values, IGamecode* gamecode);
	GamecodeValue GetCol(GamecodeParameterList *values, IGamecode* gamecode);

	// curs_refresh
	GamecodeValue Refresh(GamecodeParameterList *values, IGamecode* gamecode);

private:
	WINDOW *cursesWindow;
	bool colourStarted;
	std::unordered_map<int, int> colourPairs;
	int nextPair;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

