/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "terminal/AnsiTerminalAdapter.hh"

#include <cstring>

#ifdef _WIN32
#include <windows.h>
#else
#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>
#endif

//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

const std::string& AnsiTerminalAdapter::GetName()
{
	static std::string name("terminal");
	return name;
}

std::map<std::string, IGamecodeAdapter::AdapterMethod> AnsiTerminalAdapter::GetGlobalFunctions()
{
	return {
		// number getWidth()
		{ "getWidth", std::bind(&AnsiTerminalAdapter::GetTerminalWidth, this, std::placeholders::_1, std::placeholders::_2) },
		// number getHeight()
		{ "getHeight", std::bind(&AnsiTerminalAdapter::GetTerminalHeight, this, std::placeholders::_1, std::placeholders::_2) },
		// setCursorPosition(number, number)
		{ "setCursorPosition", std::bind(&AnsiTerminalAdapter::SetCursorPosition, this, std::placeholders::_1, std::placeholders::_2) },
		// clearDisplay()
		{ "clearDisplay", std::bind(&AnsiTerminalAdapter::ClearDisplay, this, std::placeholders::_1, std::placeholders::_2) },
		// print(string)
		{ "print", std::bind(&AnsiTerminalAdapter::Print, this, std::placeholders::_1, std::placeholders::_2) },
		// setColour() - default colours
		// setColour(string) - foreground
		// setColour(string, string, boolean) - foreground, background, bold
		{ "setColour", std::bind(&AnsiTerminalAdapter::SetColour, this, std::placeholders::_1, std::placeholders::_2) },
	};
}

void AnsiTerminalAdapter::OnRegister(IGamecode *gamecode)
{
}

static void GetConsoleDimensions(int *w, int *h)
{
#ifdef _WIN32
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
	*w = csbi.srWindow.Right - csbi.srWindow.Left + 1;
	*h = csbi.srWindow.Bottom - csbi.srWindow.Top + 1;
#else
	struct winsize winSize;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &winSize);
	*w = winSize.ws_col;
	*h = winSize.ws_row;
#endif
}

GamecodeValue AnsiTerminalAdapter::GetTerminalWidth(GamecodeParameterList *values, IGamecode* gamecode)
{
	int w, h;
	GetConsoleDimensions(&w, &h);
	return GamecodeValue(w);
}

GamecodeValue AnsiTerminalAdapter::GetTerminalHeight(GamecodeParameterList *values, IGamecode* gamecode)
{
	int w, h;
	GetConsoleDimensions(&w, &h);
	return GamecodeValue(h);
}

GamecodeValue AnsiTerminalAdapter::SetCursorPosition(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(2);
	int column = values->RequireType(0, GamecodeValue::T_Number).GetInt();
	int row = values->RequireType(1, GamecodeValue::T_Number).GetInt();

	printf("\x1b[%i;%iH", row, column);

	return GamecodeValue();
}

GamecodeValue AnsiTerminalAdapter::ClearDisplay(GamecodeParameterList *values, IGamecode* gamecode)
{
	printf("\x1b[2J");

	return GamecodeValue();
}

GamecodeValue AnsiTerminalAdapter::Print(GamecodeParameterList *values, IGamecode* gamecode)
{
	for (int i = 0; i < values->size(); i += 1)
	{
		// This works for any value type, but we get blank strings for non-strings.
		printf("%s", (*values)[i].GetString());
	}

	return GamecodeValue();
}

/**
 * String is one character, one of:
 * r, g, b, c, m, y, k, w
 * For red, green, blue, cyan, magenta, yellow, black, white, respectively.
 */
static int ColourForString(const char *string, bool foreground)
{
	// Default to white on black.
	int colour = foreground ? 7 : 0;

	if (strlen(string) != 1) {
		return colour;
	}

	switch (string[0]) {
	case 'k':
		colour = 0;
		break;
	case 'r':
		colour = 1;
		break;
	case 'g':
		colour = 2;
		break;
	case 'y':
		colour = 3;
		break;
	case 'b':
		colour = 4;
		break;
	case 'm':
		colour = 5;
		break;
	case 'c':
		colour = 6;
		break;
	case 'w':
		colour = 7;
		break;
	}

	return colour;
}

GamecodeValue AnsiTerminalAdapter::SetColour(GamecodeParameterList *values, IGamecode* gamecode)
{
	if (values->size() == 0)
	{
		printf("\x1b[0m");
		return GamecodeValue();
	}

	if (values->size() == 1)
	{
		int foreground = ColourForString((*values)[0].GetString(), true);
		printf("\x1b[3%im", foreground);
		return GamecodeValue();
	}

	bool bold = false;
	if (values->size() == 3)
	{
		bold = (*values)[2].GetBool();
	}

	values->RequireParmCount(1, 3);
	int foreground = ColourForString((*values)[0].GetString(), true);
	int background = ColourForString((*values)[1].GetString(), false);

	const char *format = "\x1b[3%i;4%im";
	if (bold)
	{
		format = "\x1b[3%i;4%i;1m";
	}
	printf(format, foreground, background);

	return GamecodeValue();
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
