/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "terminal/TerminalDisplayAdapter.hh"
#include "gamecode/library/GamecodeVector.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

const std::string& TerminalDisplayAdapter::GetName()
{
	static std::string name("TerminalDisplay");
	return name;
}

std::map<std::string, IGamecodeAdapter::AdapterMethod> TerminalDisplayAdapter::GetGlobalFunctions()
{
	return {
		// Vector getTerminalSize()
		{ "getTerminalSize", std::bind(&TerminalDisplayAdapter::GetTerminalSize, this, std::placeholders::_1, std::placeholders::_2) },
		// Vector getCursorPos()
		{ "getCursorPos", std::bind(&TerminalDisplayAdapter::GetCursorPos, this, std::placeholders::_1, std::placeholders::_2) },
		// setCursorPos(number, number)
		{ "setCursorPos", std::bind(&TerminalDisplayAdapter::SetCursorPos, this, std::placeholders::_1, std::placeholders::_2) },
		// printAt(number, number, string)
		{ "printAt", std::bind(&TerminalDisplayAdapter::PrintAt, this, std::placeholders::_1, std::placeholders::_2) },
		// print(string)
		{ "print", std::bind(&TerminalDisplayAdapter::Print, this, std::placeholders::_1, std::placeholders::_2) },

		// setBold(bool)
		{ "setBold", std::bind(&TerminalDisplayAdapter::SetBold, this, std::placeholders::_1, std::placeholders::_2) },
		// setDim(bool)
		{ "setDim", std::bind(&TerminalDisplayAdapter::SetDim, this, std::placeholders::_1, std::placeholders::_2) },
		// setUnderline(bool)
		{ "setUnderline", std::bind(&TerminalDisplayAdapter::SetUnderline, this, std::placeholders::_1, std::placeholders::_2) },
		// setBlink(bool)
		{ "setBlink", std::bind(&TerminalDisplayAdapter::SetBlink, this, std::placeholders::_1, std::placeholders::_2) },
		// setColour(string, string)
		{ "setColour", std::bind(&TerminalDisplayAdapter::SetColour, this, std::placeholders::_1, std::placeholders::_2) },
		// clear()
		{ "clear", std::bind(&TerminalDisplayAdapter::Clear, this, std::placeholders::_1, std::placeholders::_2) },
		// showCursor(bool)
		{ "showCursor", std::bind(&TerminalDisplayAdapter::ShowCursor, this, std::placeholders::_1, std::placeholders::_2) },
	};
}

void TerminalDisplayAdapter::OnRegister(IGamecode *gamecode)
{
}

GamecodeValue TerminalDisplayAdapter::GetTerminalSize(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(0);
	Vector2I termSize = display->GetTerminalSize();
	return GamecodeValue(GamecodeVector::Create(termSize[0], termSize[1], 0));
}

GamecodeValue TerminalDisplayAdapter::GetCursorPos(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(0);
	Vector2I cursorPos = display->GetCursorPos();
	return GamecodeValue(GamecodeVector::Create(cursorPos[0], cursorPos[1], 0));
}


GamecodeValue TerminalDisplayAdapter::SetCursorPos(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(2);
	int column = values->RequireType(0, GamecodeValue::T_Number).GetInt();
	int row = values->RequireType(1, GamecodeValue::T_Number).GetInt();
	display->SetCursorPos(column, row);
	return GamecodeValue();
}

GamecodeValue TerminalDisplayAdapter::PrintAt(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(3);
	int column = values->RequireType(0, GamecodeValue::T_Number).GetInt();
	int row = values->RequireType(1, GamecodeValue::T_Number).GetInt();
	std::string text = values->RequireType(2, GamecodeValue::T_String).GetString();
	display->PrintAt(column, row, text);
	return GamecodeValue();
}

GamecodeValue TerminalDisplayAdapter::Print(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(1);
	std::string text = values->RequireType(0, GamecodeValue::T_String).GetString();
	display->Print(text);
	return GamecodeValue();
}

GamecodeValue TerminalDisplayAdapter::SetBold(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(1);
	display->SetBold(values->RequireType(0, GamecodeValue::T_Bool).GetBool());
	return GamecodeValue();
}

GamecodeValue TerminalDisplayAdapter::SetDim(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(1);
	display->SetDim(values->RequireType(0, GamecodeValue::T_Bool).GetBool());
	return GamecodeValue();
}

GamecodeValue TerminalDisplayAdapter::SetUnderline(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(1);
	display->SetUnderline(values->RequireType(0, GamecodeValue::T_Bool).GetBool());
	return GamecodeValue();
}

GamecodeValue TerminalDisplayAdapter::SetBlink(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(1);
	display->SetBlink(values->RequireType(0, GamecodeValue::T_Bool).GetBool());
	return GamecodeValue();
}

GamecodeValue TerminalDisplayAdapter::SetColour(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(2);
	display->SetColour(
		values->RequireType(0, GamecodeValue::T_Number).GetInt(),
		values->RequireType(1, GamecodeValue::T_Number).GetInt()
	);
	return GamecodeValue();
}

GamecodeValue TerminalDisplayAdapter::Clear(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(0);
	display->Clear();
	return GamecodeValue();
}

GamecodeValue TerminalDisplayAdapter::ShowCursor(GamecodeParameterList *values, IGamecode* gamecode)
{
	values->RequireParmCount(1);
	display->ShowCursor(values->RequireType(0, GamecodeValue::T_Bool).GetBool());
	return GamecodeValue();
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
