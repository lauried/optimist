/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <string>
#include "library/geometry/Vector2.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class ITerminalDisplay {
public:
	virtual Vector2I GetTerminalSize() = 0;
	virtual Vector2I GetCursorPos() = 0;
	virtual void SetCursorPos(int x, int y) = 0;
	virtual void PrintAt(int x, int y, std::string text) = 0;
	virtual void Print(std::string text) = 0;
	virtual void SetBold(bool bold) = 0;
	virtual void SetDim(bool dim) = 0;
	virtual void SetUnderline(bool underline) = 0;
	virtual void SetBlink(bool blink) = 0;
	virtual void SetColour(int foreground, int background) = 0;
	virtual void Clear() = 0;
	virtual void ShowCursor(bool show) = 0;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

