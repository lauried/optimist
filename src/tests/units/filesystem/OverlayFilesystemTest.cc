/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "filesystem/IFilesystem.hh"
#include "filesystem/OverlayFilesystem.hh"
#include <iostream>
#include <stdexcept>
#include "tests/Tests.hh"
#include "tests/units/filesystem/MockFilesystem.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

struct MockSetup {
	MockFilesystem firstRead;
	MockFilesystem secondRead;
	MockFilesystem firstWrite;
	MockFilesystem secondWrite;
	OverlayFilesystem overlay;

	MockSetup()
	{
		// The 'first' ones have higher priority but are nested deeper in the
		// virtual folder structure.
		overlay.AddFilesystem(&firstRead, 0, "test/firstread", false);
		overlay.AddFilesystem(&secondRead, 1, "test", false);
		overlay.AddFilesystem(&firstWrite, 2, "test/firstwrite", true);
		overlay.AddFilesystem(&secondWrite, 3, "test", true);
	}
};

TEST_CASE("OverlayFilesystem::FileExists")
{
	MockSetup mocks;
	std::string filename = "test/firstread/a/file.here";
	bool result = mocks.overlay.FileExists(filename);

	REQUIRE(mocks.firstRead.fileExistsCount == 1);
	REQUIRE(mocks.secondRead.fileExistsCount == 1);
	REQUIRE(mocks.firstWrite.fileExistsCount == 0);
	REQUIRE(mocks.secondWrite.fileExistsCount == 1);

	REQUIRE(mocks.firstRead.filename == "a/file.here");
	REQUIRE(mocks.secondRead.filename == "firstread/a/file.here");
	REQUIRE(mocks.secondWrite.filename == "firstread/a/file.here");
}

TEST_CASE("OverlayFilesystem::Open (read)")
{
	MockSetup mocks;
	MockFile mockFile;
	IFile *iFile = &mockFile;

	mocks.secondRead.fileExists = true;
	mocks.secondRead.openFile = iFile;

	std::string filename = "test/firstread/todo.txt";
	IFile *result = mocks.overlay.Open(filename, IFilesystem::T_Read);

	REQUIRE(mocks.firstRead.fileExistsCount == 1);
	REQUIRE(mocks.secondRead.fileExistsCount == 1);

	REQUIRE(mocks.firstRead.openCount == 0);
	REQUIRE(mocks.secondRead.openCount == 1);
	REQUIRE(mocks.firstWrite.openCount == 0);
	REQUIRE(mocks.secondWrite.openCount == 0);

	REQUIRE(mocks.firstRead.filename == "todo.txt");
	REQUIRE(mocks.secondRead.filename == "firstread/todo.txt");

	REQUIRE(result == iFile);
}

TEST_CASE("OverlayFilesystem::Open (write)")
{
	MockSetup mocks;
	MockFile mockFile;
	IFile *iFile = &mockFile;

	mocks.secondWrite.fileExists = true;
	mocks.secondWrite.openFile = iFile;

	std::string filename = "test/secondwrite/todo.txt";
	IFile *result = mocks.overlay.Open(filename, IFilesystem::T_Write);

	// We don't check if the file already exists when opening for write.
	REQUIRE(mocks.firstRead.fileExistsCount == 0);
	REQUIRE(mocks.secondRead.fileExistsCount == 0);
	REQUIRE(mocks.firstWrite.fileExistsCount == 0);
	REQUIRE(mocks.secondWrite.fileExistsCount == 0);

	REQUIRE(mocks.firstRead.openCount == 0);
	REQUIRE(mocks.secondRead.openCount == 0);
	REQUIRE(mocks.firstWrite.openCount == 0);
	REQUIRE(mocks.secondWrite.openCount == 1);

	REQUIRE(mocks.secondWrite.filename == "secondwrite/todo.txt");

	REQUIRE(result == iFile);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
