/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/



#include "tests/units/filesystem/MockFilesystem.hh"

//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

MockFile::~MockFile()
{
}

MockFilesystem::~MockFilesystem()
{
}

bool MockFilesystem::FileExists(const std::string &filename)
{
	this->filename = filename;
	fileExistsCount += 1;
	return fileExists;
}

bool MockFilesystem::DirectoryExists(const std::string &path)
{
	this->path = path;
	directoryExistsCount += 1;
	return directoryExists;
}

bool MockFilesystem::CreateDirectory(const std::string &path)
{
	this->path = path;
	createDirectoryCount += 1;
	return createDirectory;
}

bool MockFilesystem::GetFilesInDirectory(const std::string &path, std::vector<std::string> *files)
{
	this->path = path;
	getFilesInDirectoryCount += 1;
	for (auto it = getFilesInDirectoryFiles.begin(); it != getFilesInDirectoryFiles.end(); ++it)
	{
		files->push_back(*it);
	}
	return getFilesInDirectory;
}

IFile* MockFilesystem::Open(const std::string &filename, IFilesystem::OpenType type)
{
	this->filename = filename;
	openType = type;
	openCount += 1;
	return openFile;
}

void MockFilesystem::Close(IFile *file)
{
	this->file = file;
	closeCount += 1;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
