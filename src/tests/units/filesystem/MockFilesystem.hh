/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "filesystem/IFilesystem.hh"

//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Mock file, just for passing around
//-----------------------------------------------------------------------------

class MockFile : public IFile {
public:
	~MockFile();

	void Seek(size_t pos, IFile::SeekType type = T_Start) {}
	size_t Tell(IFile::SeekType type = T_Start) { return 0; }
	size_t Size() { return 0; }
	size_t ReadBytes(char* buffer, size_t maxBytes) { return 0; }
	void ReadString(char* buffer, size_t maxBytes) {}
	std::string ReadString(size_t maxLength = 0) { return std::string(); }
	size_t WriteBytes(void *buffer, size_t bufferSize) { return 0; }
	size_t WriteString(std::string &str) { return 0; }
	void Flush() {}
	bool Eof() { return true; }
	bool IsOpen() { return false; }
	void Close() {}
};

//-----------------------------------------------------------------------------
// Mock filesystem for testing that things were called with the correct result
//-----------------------------------------------------------------------------

class MockFilesystem : public IFilesystem {
public:
	// Methods called
	int fileExistsCount;
	int directoryExistsCount;
	int createDirectoryCount;
	int getFilesInDirectoryCount;
	int openCount;
	int closeCount;

	// Method return parameters
	bool fileExists;
	bool directoryExists;
	bool createDirectory;
	bool getFilesInDirectory;
	std::vector<std::string> getFilesInDirectoryFiles;
	IFile *openFile;

	// Method input parameters
	std::string filename, path;
	IFilesystem::OpenType openType;
	IFile *file;

	MockFilesystem() :
		fileExistsCount(0),
		directoryExistsCount(0),
		createDirectoryCount(0),
		getFilesInDirectoryCount(0),
		openCount(0),
		closeCount(0),
		fileExists(false),
		directoryExists(false),
		getFilesInDirectory(false),
		openFile(nullptr)
		{}

	~MockFilesystem();

	// IFilesystem
	bool FileExists(const std::string &filename);
	bool DirectoryExists(const std::string &path);
	bool CreateDirectory(const std::string &path);
	bool GetFilesInDirectory(const std::string &path, std::vector<std::string> *files);
	IFile* Open(const std::string &filename, IFilesystem::OpenType type = T_Read);
	void Close(IFile *file);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
