/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "filesystem/IFilesystem.hh"
#include "filesystem/ChrootFilesystem.hh"
#include <iostream>
#include <stdexcept>
#include "tests/Tests.hh"
#include "tests/units/filesystem/MockFilesystem.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

static void TestFileExists(std::string baseDir, std::string testFile, bool result, std::string expectedPath)
{
	MockFilesystem mock;
	mock.fileExists = result;

	ChrootFilesystem chroot(&mock, baseDir);
	bool exists = chroot.FileExists(testFile);

	REQUIRE(mock.fileExistsCount == 1);
	REQUIRE(exists == result);
	REQUIRE(mock.filename == expectedPath);
}

TEST_CASE("ChrootFilesystem::FileExists")
{
	TestFileExists("my/base/dir", "path/to/file.name", true, "my/base/dir/path/to/file.name");
	TestFileExists("my/base/dir", "try/relative/../../a.file", false, "my/base/dir/a.file");
	TestFileExists("my/base/dir", "try/relative/./a.file", false, "my/base/dir/try/relative/a.file");
	TestFileExists("my/base/dir", "not/relative/.../a.file", false, "my/base/dir/not/relative/.../a.file");
	// Invalid directory traversal becomes invalid path.
	TestFileExists("my/base/dir", "a/b/../../../a.file", false, "");
	TestFileExists("my/base/dir", "../a.file", false, "");
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
