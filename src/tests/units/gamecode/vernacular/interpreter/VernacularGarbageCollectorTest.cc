/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/vernacular/interpreter/VernacularGarbageCollector.hh"
#include "tests/Tests.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

#if 0
#define DBG(x) std::cout << x << std::endl;
#else
#define DBG(x)
#endif

class MockCollectable : public IVernacularCollectable
{
public:
	bool deleted;
	std::vector<MockCollectable*> children;

	MockCollectable()
	{
		DBG("created " << this)
		deleted = false;
	}

	void GetChildren(std::vector<IVernacularCollectable*> *objects)
	{
		DBG("GetChildren on " << this)
		for (auto it = children.begin(); it != children.end(); ++it)
		{
			DBG("collectable get child " << (*it))
			objects->push_back(*it);
		}
	}
};

class MockListener : public IVernacularGCListener
{
public:
	std::vector<MockCollectable*> collectables;

	int CountDeleted()
	{
		int count = 0;
		for (auto it = collectables.begin(); it != collectables.end(); ++it)
		{
			if ((*it)->deleted)
			{
				count += 1;
			}
		}
		return count;
	}

	~MockListener()
	{
		for (auto it = collectables.begin(); it != collectables.end(); ++it)
		{
			delete *it;
		}
	}

	virtual void OnCollect(IVernacularCollectable* object) {}

	virtual void Collect(IVernacularCollectable* object)
	{
		auto collectable = dynamic_cast<MockCollectable*>(object);
		DBG("collect " << collectable)
		collectable->deleted = true;
	}
};

class MockRoot : public IVernacularGCRoot
{
public:
	std::vector<MockCollectable*> children;

	void GetChildren(std::vector<IVernacularCollectable*> *objects)
	{
		for (auto it = children.begin(); it != children.end(); ++it)
		{
			DBG("root get child " << (*it))
			objects->push_back(*it);
		}
	}
};

TEST_CASE("Collection")
{
	VernacularGarbageCollector gc;

	MockListener listener;

	for (int i = 0; i < 12; i += 1)
	{
		MockCollectable *collectable = new MockCollectable();
		listener.collectables.push_back(collectable);
		gc.AddObject(collectable, &listener);
	}

	gc.StartCollection();
	while (!gc.IsReady())
	{
		gc.CollectSome(12);
	}

	REQUIRE(listener.CountDeleted() == 12);
}

TEST_CASE("Non Collection Of Root Objects")
{
	VernacularGarbageCollector gc;

	MockListener listener;
	MockRoot root;
	gc.AddRootObject(&root);

	for (int i = 0; i < 32; i += 1)
	{
		MockCollectable *collectable = new MockCollectable();
		listener.collectables.push_back(collectable);
		gc.AddObject(collectable, &listener);
		if (i & 1)
		{
			root.children.push_back(collectable);
		}
	}

	gc.StartCollection();
	while (!gc.IsReady())
	{
		gc.CollectSome(12);
	}

	REQUIRE(listener.CountDeleted() == 16);
}

TEST_CASE("Non Collection Of Child Objects")
{
	DBG("NCCO")
	VernacularGarbageCollector gc;

	MockListener listener;
	MockRoot root;
	gc.AddRootObject(&root);

	MockCollectable *parent = nullptr;
	for (int i = 0; i < 32; i += 1)
	{
		MockCollectable *collectable = new MockCollectable();
		if (i == 0)
		{
			parent = collectable;
			root.children.push_back(parent);
		}
		listener.collectables.push_back(collectable);
		gc.AddObject(collectable, &listener);
		if (i & 1)
		{
			parent->children.push_back(collectable);
		}
	}

	gc.StartCollection();
	while (!gc.IsReady())
	{
		gc.CollectSome(12);
	}

	REQUIRE(listener.CountDeleted() == 15); // 32 - 1 in root - 16 children
}

TEST_CASE("Deep Nesting")
{
	VernacularGarbageCollector gc;

	MockListener listener;
	MockRoot root;
	gc.AddRootObject(&root);

	MockCollectable *previous = nullptr;
	for (int i = 0; i < 32; i += 1)
	{
		MockCollectable *collectable = new MockCollectable();
		if (previous != nullptr)
		{
			collectable->children.push_back(previous);
		}
		listener.collectables.push_back(collectable);
		gc.AddObject(collectable, &listener);
		previous = collectable;
	}
	root.children.push_back(previous);

	gc.StartCollection();
	while (!gc.IsReady())
	{
		gc.CollectSome(12);
	}

	REQUIRE(listener.CountDeleted() == 0);

	root.children.resize(0);

	gc.StartCollection();
	while (!gc.IsReady())
	{
		gc.CollectSome(12);
	}

	REQUIRE(listener.CountDeleted() == 32);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
