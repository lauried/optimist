/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/vernacular/types/VernacularObject.hh"
#include "gamecode/vernacular/types/VernacularString.hh"
#include <iostream>
#include <stdexcept>
#include "tests/Tests.hh"
#include "gamecode/vernacular/interpreter/IVernacularEnvironment.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

// Although methods can be given nullptr for the environment, this will let us
// test that exceptions are thrown.
class MockEnvironment : public IVernacularEnvironment {
public:
	bool exception;
	bool warning;
	bool print;

	MockEnvironment() : exception(false), warning(false), print(false) {}

	IGamecode* GetGamecode() { return nullptr; }
	VernacularGlobals* GetGlobals() { return nullptr; }
	VernacularFunctionFactory* GetFunctionFactory() { return nullptr; }
	VernacularObjectFactory* GetObjectFactory() { return nullptr; }
	VernacularStringFactory* GetStringFactory() { return nullptr; }
	VernacularUserObjectFactory* GetUserObjectFactory() { return nullptr; }

	bool IncludeFile(std::string filename) { return false; }

	void ThrowException(std::string message) { exception = true; }
	void ThrowWarning(std::string message) { warning = true; }
	void Print(std::string message) { print = true; }

	ILogTarget *GetLog() { return nullptr; }
};

TEST_CASE("Value Churn")
{
	VernacularObject::IndexPoolType pool;
	VernacularObject object(&pool);
	MockEnvironment environment;

	for (int i = 0; i < 100; i += 1)
	{
		object.Set(VernacularValue(i), VernacularValue(i), &environment, false);
		REQUIRE(environment.exception == false);
	}
	REQUIRE(object.Count() == 100);

	for (int i = 0; i < 100; i += 2)
	{
		object.Set(VernacularValue(i), VernacularValue(), &environment, false);
		REQUIRE(environment.exception == false);
	}
	REQUIRE(object.Count() == 50);

	for (int i = 100; i < 200; i += 1)
	{
		object.Set(VernacularValue(i), VernacularValue(i), &environment, false);
		REQUIRE(environment.exception == false);
	}
	REQUIRE(object.Count() == 150);
}

TEST_CASE("Garbage Collection Behaviour")
{
	VernacularObject::IndexPoolType pool;
	VernacularObject object(&pool);
	MockEnvironment environment;

	// Make some strings.
	std::vector<VernacularString*> testStrings;
	const int numStrings = 200;
	for (int i = 0; i < numStrings; i += 1)
	{
		testStrings.push_back(new VernacularString(std::string(2, 'a' + i)));
	}

	// Assign the strings alternately as keys and values.
	for (int i = 0; i < numStrings; i += 2)
	{
		object.Set(VernacularValue(testStrings[i]), VernacularValue(testStrings[i + 1]), &environment, false);
	}

	// Get the children of the object.
	std::vector<IVernacularCollectable*> objects;
	object.GetChildren(&objects);

	// All the strings should have been added to the objects list.
	REQUIRE(objects.size() == numStrings);

	// Clean up.
	for (int i = 0; i < testStrings.size(); i += 1)
	{
		delete testStrings[i];
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
