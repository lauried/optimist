/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/vernacular/types/VernacularString.hh"
#include <iostream>
#include <stdexcept>
#include "tests/Tests.hh"
#include "gamecode/vernacular/interpreter/IVernacularEnvironment.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

TEST_CASE("Garbage Collector Behaviour")
{
	VernacularString a(std::string(VernacularString::key_size, 'x'));
	VernacularString b(std::string("y"));
	VernacularString c(&a, &b);

	std::vector<IVernacularCollectable*> objects;
	c.GetChildren(&objects);

	REQUIRE(objects.size() == 2);

	for (int i = 0; i < objects.size(); i += 1)
	{
		VernacularString *s = dynamic_cast<VernacularString*>(objects[i]);
		REQUIRE(s != nullptr);
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
