/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "gamecode/vernacular/VernacularGamecode.hh"
#include <iostream>
#include <stdexcept>
#include "tests/Tests.hh"
#include "log/ILogTarget.hh"
#include "log/DummyLog.hh"
#include "filesystem/DummyFilesystem.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class MockLogTarget : public ILogTarget {
public:
	MockLogTarget() : hadError(false), lastCode(0) {}
	~MockLogTarget() {}

	ILogTarget* Start(MessageType type)
	{
		if (type == ILogTarget::ERROR)
		{
			hadError = true;
		}
		return this;
	}
	ILogTarget* Code(int code) { lastCode = code; return this; }
	ILogTarget* Write(std::string message) { messages.append(message); return this; }
	ILogTarget* End() { messages.append("\n"); return this; }

	std::string Messages() { return messages; }
	bool HadError() { return hadError; }
private:
	bool hadError;
	int lastCode;
	std::string messages;
};

static bool Compiles(std::string code, bool quiet = false)
{
	DummyFilesystem filesystem;
	MockLogTarget logTarget;
	VernacularGamecode gamecode(&filesystem, &logTarget);
	gamecode.SetInstructionLimit(1024);
	try
	{
		bool result = gamecode.Eval(code);
		if (logTarget.HadError())
		{
			std::cout << logTarget.Messages() << std::endl;
		}
		return result;
	}
	catch (std::exception &e)
	{
		return true;
	}
}

static bool DoesntCompile(std::string code)
{
	DummyFilesystem filesystem;
	MockLogTarget logTarget;
	VernacularGamecode gamecode(&filesystem, &logTarget);
	gamecode.SetInstructionLimit(1024);
	try
	{
		return !gamecode.Eval(code);
	}
	catch (std::exception &e)
	{
		return false;
	}
}

static VernacularValue FunctionResultNoWrap(std::string code)
{
	DummyFilesystem filesystem;
	MockLogTarget logTarget;
	VernacularGamecode gamecode(&filesystem, &logTarget);
	gamecode.SetInstructionLimit(80);
	std::string function = std::string(code);
	gamecode.Eval(function);
	if (logTarget.HadError())
	{
		std::cout << logTarget.Messages() << std::endl;
	}
	std::string funcName("testFunc");
	std::vector<GamecodeValue> parameters;
	return gamecode.CallFunction(funcName, parameters);
}

static VernacularValue FunctionResult(std::string code)
{
	 return FunctionResultNoWrap(std::string("testFunc := function(){") + code + std::string("};"));
}

// Returns the number of times the opcode is found in the function.
static int FunctionContainsOpcode(std::string code, VernacularBytecode::Opcode opcode)
{
	DummyFilesystem filesystem;
	MockLogTarget logTarget;
	VernacularGamecode gamecode(&filesystem, &logTarget);
	gamecode.SetInstructionLimit(80);
	std::string functionCode = std::string("testFunc := function(){") + code + std::string("};");
	gamecode.Eval(functionCode);
	VernacularValue functionValue = gamecode.GetGlobal("testFunc");
	if (!functionValue.IsFunction())
	{
		throw std::logic_error("function did not compile");
	}
	VernacularFunction *function = functionValue.GetVernacularFunction();
	VernacularBytecode bytecode = VernacularBytecode(opcode);
	int count = 0;
	for (int i = 0; i < function->bytecode.size(); i += 1)
	{
		if (function->bytecode[i] == bytecode)
		{
			count += 1;
		}
	}
	return count;
}

// Note: These methods don't perform any type casting if the result wasn't the
// required type.

static double FunctionResultNumberNoWrap(std::string code)
{
	return FunctionResultNoWrap(code).GetDouble();
}

static double FunctionResultNumber(std::string code)
{
	return FunctionResult(code).GetDouble();
}

static bool FunctionResultBool(std::string code)
{
	return FunctionResult(code).GetBool();
}

static std::string FunctionResultString(std::string code)
{
	return FunctionResult(code).ToString();
}

static bool FunctionResultException(std::string code)
{
	try
	{
		FunctionResult(code);
		return false;
	}
	catch (std::exception &e)
	{
		return true;
	}
}

TEST_CASE("Two Function Calls Inside An Expression")
{
	REQUIRE(FunctionResultNumber(
		"var MyClass := function() {\n"
		"	this.values := { a := 1, b := 0 };\n"
		"	this.getVal := function(x) {\n"
		"		return this.values[x];\n"
		"	};\n"
		"	this.getDval := function(x, y) {\n"
		"		return this.getVal(x) - this.getVal(y);\n"
		"	};\n"
		"};\n"
		"var instance := new MyClass();\n"
		"return instance.getDval(\"a\", \"b\");\n"
	) == 1);
}

TEST_CASE("Syntax Tree Test")
{
	// if
	REQUIRE(Compiles("if (1) {}"));
	REQUIRE(Compiles("if (\"hi\") {}"));
	REQUIRE(Compiles("if (true) {}"));
	REQUIRE(Compiles("if (x < y) {}"));
	REQUIRE(Compiles("if (funcCall()) {}"));
	REQUIRE(DoesntCompile("if (x := 12) {}")); // expression performs an action rather than evaluating

	// for
	REQUIRE(Compiles("for (i := 2; i < 12; i := i + 1) {}"));
	REQUIRE(Compiles("for (;;) {}"));
	REQUIRE(Compiles("for (;funcCall();) {}"));

	REQUIRE(DoesntCompile("for (i < 2;;) {}")); // first part must perform an action
	REQUIRE(DoesntCompile("for (;i := 12;) {}")); // second part must evaluate to a value
	REQUIRE(DoesntCompile("for (;;i < 2) {}")); // third part must perform an action

	// foreach
	REQUIRE(Compiles("foreach (key x, value y in a.b.c) {}"));
	REQUIRE(Compiles("foreach (key f in funcCall()) {}"));
	REQUIRE(Compiles("foreach (value a in {}) {}"));

	REQUIRE(DoesntCompile("foreach (value x in x := 12) {}")); // expression must evaluate

	// return
	REQUIRE(Compiles("return;"));
	REQUIRE(Compiles("return x;"));
	REQUIRE(Compiles("return x + y;"));
	REQUIRE(Compiles("return (a + b) * (c + d);"));
	REQUIRE(Compiles("return x = 12;"));

	REQUIRE(DoesntCompile("return x := 12;")); // expression must evaluate

	// var
	REQUIRE(Compiles("var x;"));
	REQUIRE(Compiles("var x, y;"));
	REQUIRE(Compiles("var x := 1, y := 2;"));

	// expressions
	REQUIRE(Compiles("x := 12;"));
	REQUIRE(Compiles("x := \"Hello World\";"));
	REQUIRE(Compiles("x := function(x) { return x + 1; };"));
	REQUIRE(Compiles("x := { a := 12, b := \"Hello World\" };"));
	REQUIRE(Compiles("x := funcCall();"));

	REQUIRE(DoesntCompile("x < 2;")); // statement must perform action
	REQUIRE(DoesntCompile("\"hello\";")); // statement must perform action

	// function calls
	REQUIRE(Compiles("funcCall(a + b + c, d + e + f);"));
	REQUIRE(Compiles("funcCall({1, 2, 12});"));

	REQUIRE(DoesntCompile("funcCall(a := 12);")); // expression must evaluate

	// constructor syntax
	REQUIRE(Compiles("result := new Constructor();"));
	REQUIRE(DoesntCompile("result := new object.method();")); // new not allowed on method call
	REQUIRE(DoesntCompile("result := new object[\"method\"]();")); // new not allowed on method call

	// test extra syntax
	REQUIRE(Compiles("if (a = b) { a := c; } else if (a = c) { a := d; } else { a := e; }"));

	REQUIRE(Compiles("for (var i := 0; i < 4; i := i + 1) {\n\
		if (i = 2) {\n\
			break;\n\
		}\n\
	}"));

	// var
	REQUIRE(Compiles("var a_b;"));
	REQUIRE(Compiles("var i := 0;"));
	REQUIRE(Compiles("var x, y, z;"));
	REQUIRE(Compiles("var o := 1, p := 2, q := 3;"));

	// null literals
	REQUIRE(Compiles("z := null;"));

	// boolean literals
	REQUIRE(Compiles("a := true;"));
	REQUIRE(Compiles("b := false;"));

	// number literals
	REQUIRE(Compiles("c := 0;"));
	REQUIRE(Compiles("d := 123456789;"));
	REQUIRE(Compiles("e := 123.456789;"));
	REQUIRE(Compiles("f := -0.5;"));
	REQUIRE(Compiles("f := .5;"));
	REQUIRE(Compiles("f := -.5;"));
	REQUIRE(Compiles("g := 0x012abcd;"));
	REQUIRE(Compiles("h := 0xfaded;"));
	REQUIRE(Compiles("i := 0xCAFEBEEF;"));
	REQUIRE(Compiles("j := 0b10001111;"));
	REQUIRE(Compiles("k := 0b01010111;"));

	// string literals
	REQUIRE(Compiles("l := \"Hello world\";"));
	REQUIRE(Compiles("m := \"Hello\\nWorld\";"));
	REQUIRE(Compiles("n := \"She said \\\"Hi!\\\"\";"));
	REQUIRE(Compiles("o := \"a\\tbb\\tc\";"));

	// object literals
	REQUIRE(Compiles("p := {};"));
	REQUIRE(Compiles("q := { a := 1, b := 2 };"));
	REQUIRE(Compiles("r := { a := 1, 2, 3, b := 12 };"));

	// function literals, with and without parameters
	REQUIRE(Compiles("f1 := function() { return 12;};"));
	REQUIRE(Compiles("f2 := function(a, b, c) { return a + b + c; };"));

	// Bug where this doesn't compile, probably due to operator precedence.
	// Weirdly, it currently is interpreted as (this.(positive))-(this.(negative))
	// Where the inner braces are function calls.
	REQUIRE(Compiles("return this.controlValue(positive) - this.controlValue(negative);"));
}

TEST_CASE("Literal Values")
{
	REQUIRE(FunctionResultNumber("return 0x20;") == 32);
	REQUIRE(FunctionResultNumber("return 0b1010;") == 10);
}

TEST_CASE("Flow Control")
{
	REQUIRE(FunctionResultNumber("var a := 0; for (var i := 1; i <= 4; i := i + 1) { a := a + i; } return a;") == 10);

	// TODO: Breaking out of loops, conditional statements.
}

TEST_CASE("Functions")
{
	REQUIRE(FunctionResultNumber("var a := function(x, y) { return x + y; }; return a(1, 3);") == 4);
	REQUIRE(FunctionResultNumber("var a := function(x, y, z) { return x + y + z; }; return a(1, 3, 5);") == 9);
	// Return parameters, locals and call-out parameters.
	REQUIRE(FunctionResultNumber("var a := function(a, b) { return a + b; }; var f := function(a, b, c) { var d := a * b; return c(a, b) + d; }; return f(2, 3, a);") == 11);
	// Calling two functions with different numbers of parameters.
	REQUIRE(FunctionResultNumber("var f1 := function(a, b) { return a * b; }; var f2 := function(x, y, z) { return x + y + z; }; return f1(2, f2(1, 2, 3));") == 12);
	// Nested function calls.
	REQUIRE(FunctionResultString("var a := 1, b := \"2\", output := function(x) { return x; }; return output(a $ \"+\" $ toString(b));") == std::string("1+2"));
}

TEST_CASE("Global Mapping")
{
	REQUIRE(FunctionResultNumberNoWrap("a := 1; b := function() { a := a + 2; }; testFunc := function() { b(); return a; };") == 3);
}

TEST_CASE("Value Equality")
{
	// Same types with different format literals are equal
	REQUIRE(FunctionResultBool("return 0xff = 255;") == true);
	REQUIRE(FunctionResultBool("return 0b1010 = 10;") == true);
	REQUIRE(FunctionResultBool("return 10.00 = 10;") == true);

	// Different types don't cast
	REQUIRE(FunctionResultBool("return 1 = true;") == false);
}

TEST_CASE("Operator Precedence")
{
	REQUIRE(FunctionResultNumber("return 1 + 2 * 3;") == 7);
	REQUIRE(FunctionResultNumber("return 2 * 10 ** 3;") == 2000);

	REQUIRE(FunctionResultBool("return 3 > 4 = false;") == true);
	REQUIRE(FunctionResultBool("return true && false = true;") == false);

	// Better (non-C) precedence for bitwise operators.
	// Either "0xff00 = 0xff00" or "0xff00 & false".
	REQUIRE(FunctionResultBool("return 0xff0f & 0xff00 = 0xff00;") == true);
	REQUIRE(FunctionResultBool("return 0xf000 | 0x0f00 = 0xff00;") == true);
}

TEST_CASE("Object Behaviour")
{
	// TODO: Ordering, adding, removing, etc.
	REQUIRE(FunctionResultBool("var o := { \"a\", \"b\", \"c\", \"d\", \"e\", \"f\" }; var b := \"\"; foreach (value v in o) { b := b $ v; } return b = \"abcdef\";") == true);
}

TEST_CASE("Strings")
{
	// TODO: Check that long concatenation still works with equality.
}

TEST_CASE("Bound Functions")
{
	REQUIRE(FunctionResultNumber(
		"var obj := {\n"
		"	property := 3,\n"
		"	method := function(a) {\n"
		"		return a + 3;\n"
		"	}\n"
		"};\n"
		"var callback := bind(obj, obj.method);\n"
		"return callback(2);\n"
	) == 5);
}

TEST_CASE("Assignment Modifiers")
{
	// Private is null externally.
	REQUIRE(FunctionResultBool("var obj := { prop := @private 1 }; return (obj.prop = null);") == true);
	REQUIRE(FunctionResultBool("var obj := new (function(){ this.prop := @private 1; })(); return obj.prop = null;") == true);

	// Private does not show up in foreach.
	REQUIRE(FunctionResultNumber(
		"var obj := { priv := @private 5, pub := 8 };\n"
		"var count := 0;\n"
		"foreach (value v in obj) {\n"
		"	count := count + v;\n"
		"}\n"
		"return count;\n"
	) == 8);

	// Readonly cannot be rewritten.
	// TODO: These throw exceptions when trying to write.
	// Need to assert that an exception occurs.
	REQUIRE(FunctionResultException("var obj := { prop := @readonly 4 }; obj.prop := 12; return obj.prop;") == true);
	REQUIRE(FunctionResultException("var obj := new (function(){ this.prop := @readonly 4; })(); obj.prop := 12; return obj.prop;") == true);

	// Readonly does show up in foreach.
	REQUIRE(FunctionResultNumber(
		"var obj := { priv := @readonly 5, pub := 8 };\n"
		"var count := 0;\n"
		"foreach (value v in obj) {\n"
		"	count := count + v;\n"
		"}\n"
		"return count;\n"
	) == 13);
}

// Note: This feature has been removed.
#if 0
TEST_CASE("Metatable Class")
{
	// The 'class' function creates a function bound to a table such that the
	// function is a constructor, and all values in the bound table are copied
	// to the constructed table.

	REQUIRE(FunctionResultNumber(
		"var MyClass := class(\n"
		"	{ a := 1, b := 2, c := 3, d := 4 },\n"
		"	function() {\n"
		"		this.e := 5;\n"
		"	}\n"
		");\n"
		"var obj := new MyClass();\n"
		"var count := 0;\n"
		"foreach (value v in obj) {\n"
		"	count := count + v;\n"
		"}\n"
		"return count;\n"
	) == 15);
}
#endif

TEST_CASE("Fancy Assignment Operators")
{
	// On individual values
	REQUIRE(FunctionResultNumber("var myVal := 4; myVal :+= 6; return myVal;") == 10);
	// On dot access
	REQUIRE(FunctionResultNumber("var myTable := { myValue := 4 }; myTable.myValue :+= 6; return myTable.myValue;") == 10);
	// On index access
	REQUIRE(FunctionResultNumber("var myTable := { myValue := 4 }; myTable[\"myValue\"] :+= 6; return myTable.myValue;") == 10);

	// Some usages:
	REQUIRE(FunctionResultNumber("var count; for (var x := 0; x < 6; x :+= 1) { count :+= x; } return count;") == 15);
}

TEST_CASE("Copy On Write Behaves Itself")
{
	// Running vernacular can't check that a copy-on-write was created
	// internally, but we can check that writing to the copy doesn't crash
	// and doesn't affect the original literal.
	REQUIRE(FunctionResultNumber(
		"var myFunc := function() {\n"
		"	var t := { v := 12 };\n"
		"	var v := t.v;\n"
		"   t.v := 1314;\n"
		"	return v;\n"
		"};\n"
		"var a := myFunc();\n"
		"var b := myFunc();\n"
		"return b;\n"
	) == 12);

	// Compile some code and check that the bytecode contains OP_COPY_ON_WRITE.
	REQUIRE(FunctionContainsOpcode("x := { a := 1, b := 2, c := 3 };", VernacularBytecode::OP_COPY_ON_WRITE) == 1);
}

TEST_CASE("Address Reuse Optimisation On Dot-Gets")
{
	// The nicest solution is to make every single local assignment create a
	// new address and then update the address of the identifier.
	// We'd need to make sure loops are handled properly.

	// The simplest is to use a temporary address for every assignment.
	// A compromise would be to check for self-referential local assignments.

	REQUIRE(FunctionResultNumber(
		"var t := { v := 12 };\n"
		"var v := 3;\n"
		"v := t.v + v;\n"
		"return v;\n"
	) == 15);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
