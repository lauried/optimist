/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "library/text/Path.hh"
#include <iostream>
#include <stdexcept>
#include "tests/Tests.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

static void TestPath(
	std::string path,
	int flags,
	std::string expectedPath,
	std::string expectedFilename,
	std::string expectedBasename,
	std::string expectedExtension,
	int expectedPathPartCount
) {
	Path pathInfo(path, flags);
	REQUIRE(pathInfo.path == expectedPath);
	REQUIRE(pathInfo.filename == expectedFilename);
	REQUIRE(pathInfo.basename == expectedBasename);
	REQUIRE(pathInfo.extension == expectedExtension);
	REQUIRE(pathInfo.pathParts.size() == expectedPathPartCount);
}

TEST_CASE("Path")
{
	int allButNormalize = Path::F_Path + Path::F_PathParts + Path::F_Filename + Path::F_Basename + Path::F_Extension;
	int all = allButNormalize | Path::F_NormalizePath;

	TestPath("path/to/file.name", allButNormalize, "path/to", "file.name", "file", "name", 2);

	// If there are no slashes, then there's no path and it's all the filename
	// regardless of whether it represents a directory or a file.
	TestPath("test", allButNormalize, "", "test", "test", "", 0);

	TestPath("test", all, "", "test", "test", "", 0);
}

TEST_CASE("Join")
{
	REQUIRE(Path::Join({ "start", "end" }) == std::string("start/end"));
	REQUIRE(Path::Join({ "/start", "end" }) == std::string("/start/end"));
	REQUIRE(Path::Join({ "a", "b", "c" }) == std::string("a/b/c"));
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
