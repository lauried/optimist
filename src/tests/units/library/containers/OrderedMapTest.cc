/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "library/containers/OrderedMap.hh"
#include <iostream>
#include <stdexcept>
#include "tests/Tests.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

TEST_CASE("Push Front Pop Back")
{
	std::vector<int> values = { 4, 12, 8, 6, 32, 36, 24, 38 };

	OrderedMap<int, int> map;

	for (int i = 0; i < values.size(); i += 1)
	{
		map.push_front(std::make_pair(i, values[i]));
	}

	for (int i = 0; i < values.size(); i += 1)
	{
		REQUIRE(i == map.back().first);
		REQUIRE(values[i] == map.back().second);
		map.pop_back();
	}
}

TEST_CASE("Push Front Pop Front")
{
	std::vector<int> values = { 4, 12, 8, 6, 32, 36, 24, 38 };

	OrderedMap<int, int> map;

	for (int i = 0; i < values.size(); i += 1)
	{
		map.push_front(std::make_pair(i, values[i]));
	}

	for (int i = values.size() - 1; i >= 0; i -= 1)
	{
		REQUIRE(i == map.front().first);
		REQUIRE(values[i] == map.front().second);
		map.pop_front();
	}
}

TEST_CASE("Push Back Pop Front")
{
	std::vector<int> values({ 4, 12, 8, 6, 32, 36, 24, 38 });

	OrderedMap<int, int> map;

	for (int i = 0; i < values.size(); i += 1)
	{
		map.push_back(std::make_pair(i, values[i]));
	}

	for (int i = 0; i < values.size(); i += 1)
	{
		REQUIRE(i == map.front().first);
		REQUIRE(values[i] == map.front().second);
		map.pop_front();
	}
}

TEST_CASE("Push Back Pop Back")
{
	std::vector<int> values({ 4, 12, 8, 6, 32, 36, 24, 38 });

	OrderedMap<int, int> map;

	for (int i = 0; i < values.size(); i += 1)
	{
		map.push_back(std::make_pair(i, values[i]));
	}

	for (int i = values.size() - 1; i >= 0; i -= 1)
	{
		REQUIRE(i == map.back().first);
		REQUIRE(values[i] == map.back().second);
		map.pop_back();
	}
}

TEST_CASE("Iterator Order")
{
	std::vector<int> values({ 4, 12, 8, 6, 32, 36, 24, 38 });

	OrderedMap<int, int> map;

	for (int i = 0; i < values.size(); i += 1)
	{
		map.push_back(std::make_pair(i, values[i]));
	}

	auto it = map.begin();

	for (int i = 0; i < values.size(); i += 1)
	{
		REQUIRE(i == it->first);
		REQUIRE(values[i] == it->second);
		++it;
	}
}

TEST_CASE("Begin When Empty")
{
	OrderedMap<int, int> map;
	REQUIRE(map.begin() == map.end());
}

TEST_CASE("Iterate From End Harmless")
{
	OrderedMap<int, int> map;
	auto it = map.begin();
	it++;
	REQUIRE(it == map.end());
	++it;
	REQUIRE(it == map.end());
}

TEST_CASE("Erase End Harmless")
{
	OrderedMap<int, int> map;
	auto it = map.end();
	it = map.erase(it);
	REQUIRE(it == map.end());
}

TEST_CASE("Erase")
{
	std::vector<int> values({ 4, 12, 8, 6, 32, 36, 24, 38 });

	OrderedMap<int, int> map;

	for (int i = 0; i < values.size(); i += 1)
	{
		map.push_back(std::make_pair(i, values[i]));
	}

	auto it = map.begin();

	for (int i = 0; i < values.size(); i += 1)
	{
		REQUIRE(i == it->first);
		REQUIRE(values[i] == it->second);
		it = map.erase(it);
	}

	REQUIRE(map.size() == 0);
}

TEST_CASE("Find")
{
	std::vector<int> values({ 4, 12, 8, 6, 32, 36, 24, 38 });

	OrderedMap<int, int> map;

	for (int i = 0; i < values.size(); i += 1)
	{
		map.push_back(std::make_pair(i, values[i]));
	}

	auto it = map.begin();

	for (int i = 0; i < values.size(); i += 1)
	{
		auto it = map.find(i);
		REQUIRE(it->first == i);
		REQUIRE(it->second == values[i]);
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
