/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/


#include "tests/Tests.hh"
#include "library/maths/GridSplit.hh"
#include "library/maths/AbstractSplit.hh"
#include <vector>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

TEST_CASE("GridSplit")
{
	GridSplit split;
	std::vector<GridSplit::BoundInfo> bounds;

	split.GenerateBounds(4, 4, 66, 28, 32, &bounds);

	REQUIRE(bounds.size() == 3);

	REQUIRE(bounds[0].coordMin == Vector2I(4, 4));
	REQUIRE(bounds[0].coordMax == Vector2I(32, 28));
	REQUIRE(bounds[1].coordMin == Vector2I(32, 4));
	REQUIRE(bounds[1].coordMax == Vector2I(64, 28));
	REQUIRE(bounds[2].coordMin == Vector2I(64, 4));
	REQUIRE(bounds[2].coordMax == Vector2I(66, 28));
}

TEST_CASE("GridSplit: Main Test")
{
	GridSplit split;
	std::vector<GridSplit::BoundInfo> bounds;

	split.GenerateBounds(16, 16, 80, 80, 32, &bounds);

	REQUIRE(bounds.size() == 9);

	REQUIRE(bounds[0].coordMin == Vector2I(16, 16));
	REQUIRE(bounds[0].coordMax == Vector2I(32, 32));
	REQUIRE(bounds[0].fracMin == Vector2F(0.5, 0.5));
	REQUIRE(bounds[0].fracMax == Vector2F(1.0, 1.0));

	REQUIRE(bounds[1].coordMin == Vector2I(32, 16));
	REQUIRE(bounds[1].coordMax == Vector2I(64, 32));
	REQUIRE(bounds[1].fracMin == Vector2F(0.0, 0.5));
	REQUIRE(bounds[1].fracMax == Vector2F(1.0, 1.0));

	REQUIRE(bounds[2].coordMin == Vector2I(64, 16));
	REQUIRE(bounds[2].coordMax == Vector2I(80, 32));
	REQUIRE(bounds[2].fracMin == Vector2F(0.0, 0.5));
	REQUIRE(bounds[2].fracMax == Vector2F(0.5, 1.0));

	REQUIRE(bounds[3].coordMin == Vector2I(16, 32));
	REQUIRE(bounds[3].coordMax == Vector2I(32, 64));
	REQUIRE(bounds[3].fracMin == Vector2F(0.5, 0.0));
	REQUIRE(bounds[3].fracMax == Vector2F(1.0, 1.0));

	REQUIRE(bounds[4].coordMin == Vector2I(32, 32));
	REQUIRE(bounds[4].coordMax == Vector2I(64, 64));
	REQUIRE(bounds[4].fracMin == Vector2F(0.0, 0.0));
	REQUIRE(bounds[4].fracMax == Vector2F(1.0, 1.0));

	REQUIRE(bounds[5].coordMin == Vector2I(64, 32));
	REQUIRE(bounds[5].coordMax == Vector2I(80, 64));
	REQUIRE(bounds[5].fracMin == Vector2F(0.0, 0.0));
	REQUIRE(bounds[5].fracMax == Vector2F(0.5, 1.0));

	REQUIRE(bounds[6].coordMin == Vector2I(16, 64));
	REQUIRE(bounds[6].coordMax == Vector2I(32, 80));
	REQUIRE(bounds[6].fracMin == Vector2F(0.5, 0.0));
	REQUIRE(bounds[6].fracMax == Vector2F(1.0, 0.5));

	REQUIRE(bounds[7].coordMin == Vector2I(32, 64));
	REQUIRE(bounds[7].coordMax == Vector2I(64, 80));
	REQUIRE(bounds[7].fracMin == Vector2F(0.0, 0.0));
	REQUIRE(bounds[7].fracMax == Vector2F(1.0, 0.5));

	REQUIRE(bounds[8].coordMin == Vector2I(64, 64));
	REQUIRE(bounds[8].coordMax == Vector2I(80, 80));
	REQUIRE(bounds[8].fracMin == Vector2F(0.0, 0.0));
	REQUIRE(bounds[8].fracMax == Vector2F(0.5, 0.5));
}

TEST_CASE("GridSplit: Fractions work OK")
{
	GridSplit split;
	std::vector<GridSplit::BoundInfo> bounds;

	split.GenerateBounds(8, 8, 24, 24, 32, &bounds);

	REQUIRE(bounds.size() == 1);

	REQUIRE(bounds[0].coordMin == Vector2I(8, 8));
	REQUIRE(bounds[0].coordMax == Vector2I(24, 24));
	REQUIRE(bounds[0].fracMin[0] == 0.25);
	REQUIRE(bounds[0].fracMin[1] == 0.25);
	REQUIRE(bounds[0].fracMax[0] == 0.75);
	REQUIRE(bounds[0].fracMax[1] == 0.75);
}

class MockAbstract : public AbstractSplit {
public:
	void TestNextPosAndFraction(int current, int end, std::vector<int> *output)
	{
		while (current != end)
		{
			output->push_back(current);
			int next;
			float frac;
			NextPosAndFraction(current, 32, end, &next, &frac);
			current = next;
		}
		output->push_back(current);
	}
};

TEST_CASE("GridSplit: Test Abstract Next Method For Negative Numbers")
{
	MockAbstract ma;

	std::vector<int> result;

	ma.TestNextPosAndFraction(-66, 66, &result);

	REQUIRE(result.size() == 7);

	REQUIRE(result[0] == -66);
	REQUIRE(result[1] == -64);
	REQUIRE(result[2] == -32);
	REQUIRE(result[3] == 0);
	REQUIRE(result[4] == 32);
	REQUIRE(result[5] == 64);
	REQUIRE(result[6] == 66);
}

TEST_CASE("GridSplit: Ensure these specific values don't infinite loop")
{
	GridSplit split;
	std::vector<GridSplit::BoundInfo> bounds;
	split.GenerateBounds(-1, -1, 19, 19, 1 << 5, &bounds);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
