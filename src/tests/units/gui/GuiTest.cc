/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "gui/Gui.hh"
#include "tests/Tests.hh"
#include <string>
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

TEST_CASE("Layouts")
{
	// TODO: A document containing the layout elements.
	// TODO: Pass XML string as function call parameter.
	std::string xml(
		"<?xml version=\"1.0\"?>"
		"<document>"
		"  <nocontents attr1=\"value1\"/>"
		"  <empty></empty>"
		"  <parent attr2=\"value2\">"
		"    <!-- comment -->"
		"    <child></child>"
		"    <child></child>"
		"    <child></child>"
		"  </parent>"
		"</document>"
	);

	Gui gui;

	gui.LoadDocument(xml.c_str(), xml.length());
	gui.SetSize(Vector2F(640, 480));

	// TODO: A way to find a particular object.
	// TODO: Find the laid-out objects and check that their positions are
	// correct.
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
