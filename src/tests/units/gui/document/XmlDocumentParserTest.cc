/*
===============================================================================
Optimist Game Engine
Software intended to help people create computer games.
Copyright © 2010-2017 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "gui/document/IDocumentNodeAllocator.hh"
#include "gui/document/XmlDocumentParser.hh"
#include "tests/Tests.hh"
#include <string>
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class Allocator : public IDocumentNodeAllocator {
public:
	GuiDocumentNode *CreateNode() { return new GuiDocumentNode(); }
	void DeleteNode(GuiDocumentNode *node) { delete node; }
};

TEST_CASE("Correct document")
{
	std::string xml(
		"<?xml version=\"1.0\"?>"
		"<document>"
		"  <nocontents attr1=\"value1\"/>"
		"  <empty></empty>"
		"  <parent attr2=\"value2\">"
		"    <!-- comment -->"
		"    <child></child>"
		"    <child></child>"
		"    <child></child>"
		"  </parent>"
		"</document>"
	);

	Allocator allocator;
	GuiDocument document(&allocator);
	XmlDocumentParser parser;

	parser.ParseDocument(&document, xml.c_str(), xml.length());

	// Inspect the document and require that the nodes match.
	GuiDocumentNode* root = document.GetRoot();
	REQUIRE(root != nullptr);
	REQUIRE(root->GetChildren().size() == 3);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
