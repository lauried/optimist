-------------------------------------------------------------------------------
Running Optimist Game Engine
-------------------------------------------------------------------------------

The executable is in Build/Release or Build/Debug. The base directory can be
specified on the command line, e.g.

	Build/Release/Optimist_Game_Engine --baseDir=game/test

Later, the engine ought to handle command line parameters to set up its
filesystem.

