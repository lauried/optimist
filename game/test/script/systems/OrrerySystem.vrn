/**
 * Orrery is a renderer sub-system.
 */
OrrerySystem := function() {
	BaseSystem("Orrery", { RendererInterface });

    this.mainScene := @readonly ScenegraphScene();
    this.mainScene.clearDepth := true;
    this.mainScene.originFollowsCamera := true;

    this.speedScale := @readonly 60 * 24;

	this.viewpoint := @private Orientation();
	this.viewPlanet := @private null;
	this.viewLat := @private 0;
	this.viewLong := @private 0;
	this.lightDirection := @private Vector(0, 0, 1);

	this.starfieldOrientation := @readonly Orientation();

	/**
	 * Called in the frame method of the renderer we're added to.
	 */
	this.rendererFrame := @readonly function(interval) {
		// Constants would be the number of seconds in an orrery day.
		interval := interval / this.speedScale;

		foreach (value component in this.components) {
			component.update(interval);
		}

		if (this.viewPlanet != null) {
			this.viewPlanet.setSurfaceOrientation(this.viewLat, this.viewLong, this.viewpoint);
			var star := this.viewPlanet.getClosestStar();
			this.lightDirection := this.viewpoint.pointToInternalSpace(star.planetaryCoords.origin);
			this.lightDirection.normalize();
		}

		this.mainScene.position.set(this.viewpoint);
		this.starfieldOrientation.set(this.viewpoint);
		this.starfieldOrientation.origin.set(0, 0, 0);
	};

	/**
	 * Sets the body we're viewing from.
	 */
	this.setViewpoint := @readonly function(planet, lat, long) {
		this.viewPlanet := @private planet;
		this.viewLat := @private lat;
		this.viewLong := @private long;
	};

	/**
	 * Gets the light direction at our viewpoint.
	 */
	this.getLightDirectionInWorldSpace := @readonly function() {
		return this.lightDirection;
	};
};
