/**
 * Base class for a system.
 */
BaseSystem := function(name, interfaces) {
	/**
	 * Contains all this system's components.
	 */
	this.components := @private {};

	/**
	 * The interface name that all components are required to implement in
	 * order to support this system.
	 */
	this.requiredInterfaces := @private interfaces;

	/**
	 * Adds a component to the system. Throws an exception if it doesn't
	 * implement our required interface.
	 */
	this.addComponent := @readonly function(component) {
		foreach (value interface in this.requiredInterfaces) {
			interface.assertImplementedBy(component);
		}
		this.components[component] := component;
		this.onAddComponent(component);
		component.onAddToSystem(this);
	};

	/**
	 * Removes the component from this system.
	 * Returns true if a component was removed, else false.
	 */
	this.removeComponent := @readonly function(component) {
		if (!this.components[component]) {
			return false;
		}
		this.components[component] := null;
		this.onRemoveComponent(component);
		component.onRemoveFromSystem(this);
		return true;
	};

	/**
	 * Event called when a component is added to this system.
	 */
	this.onAddComponent := @private function(component) {};

	/**
	 * Event called when a component is removed from this system.
	 */
	this.onRemoveComponent := @private function(component) {};

	/**
	 * Returns true if our required interface is in the component's list of
	 * interfaces, else false.
	 */
	this.componentHasInterface := @private function(component, requiredInterface) {
		if (requiredInterface = null) {
			requiredInterface := this.requiredInterface;
		}
		foreach (value componentInterface in component.interfaces) {
			if (componentInterface = requiredInterface) {
				return true;
			}
		}
		return false;
	};

	/**
	 * Return true if the component implements all of the methods passed in the
	 * array, otherwise false.
	 */
	this.componentHasMethods := @private function(component, methods) {
		foreach (value method in methods) {
			if (getType(component[method]) != "function") {
				return false;
			}
		}
		return true;
	};
};
