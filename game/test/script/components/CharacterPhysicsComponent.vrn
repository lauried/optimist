/**
 * Character Physics
 */
CharacterPhysicsComponent := function(params) {
	this.interfaces := @readonly { "Physics", "CharacterPhysics", "Orientation" };

	BaseComponent("CharacterPhysics");
	this.dynamicsOrientation := @private Orientation();
	this.orientation := @private Orientation();
	this.size := @private Vector(0.25, 0.25, 1.0);
	this.movement := @private Vector();
	this.eulerAngles := @private Vector();
	this.maxWalkSpeed := @private 4;
	this.maxWalkForce := @private 2;
	this.maxFlySpeed := @private 2000;
	this.maxFlyForce := @private 80;
	this.flyMode := @private false;

	if (params.origin != null) {
		this.dynamicsOrientation.origin.set(params.origin);
		this.orientation.origin.set(params.origin);
	}
	if (params.size != null) {
		this.size.set(params.size);
	}

	this.onAddToSystem := function(system) {
		this.dynamicsObject := system.dynamics.addObject(this.dynamicsOrientation, "character", this.size, 1.0);
	};

	this.onRemoveFromSystem := function() {
		this.physics.dynamics.removeObject(this.dyamicsObject);
	};

	this.getOrientation := function() {
		return this.orientation;
	};

	this.getOrigin := function() {
		return this.orientation.origin;
	};

	this.setOrigin := function(vec) {
		this.orientation.origin.set(vec);
		this.dynamicsOrientation.origin.set(vec);
		if (this.dynamicsObject) {
			this.dynamicsObject.setOrientation(this.dynamicsOrientation);
		}
	};

	// Takes motion vectors in model space.
	this.setMovement := function(data) {
		this.movement.x := data.x;
		this.movement.y := data.y;
	};

	// Sets or unsets fly mode.
	// Note that flymode is essentially for debug.
	this.toggleFlyMode := function(data) {
		if (this.flyMode) {
			print("flymode off");
			this.flyMode := 0;
			this.dynamicsObject.setGravity(1);
		} else {
			print("flymode on");
			this.flyMode := 1;
			this.dynamicsObject.setGravity(0);
		}
	};

	// Takes relative euler angles.
	this.turnEuler := function(data) {
		this.eulerAngles.z := this.eulerAngles.z + data.x;
		this.eulerAngles.y := this.eulerAngles.y + data.y;
		if (this.eulerAngles.z > PI * 2) {
			this.eulerAngles.z := this.eulerAngles.z - PI * 2;
		}
		if (this.eulerAngles.z < 0) {
			this.eulerAngles.z := this.eulerAngles.z + PI * 2;
		}
		if (this.eulerAngles.y > PI * 0.5) {
			this.eulerAngles.y := PI * 0.5;
		}
		if (this.eulerAngles.y < PI * -0.5) {
			this.eulerAngles.y := PI * -0.5;
		}
		this.orientation.forward.set(1, 0, 0);
		this.orientation.right.set(0, 1, 0);
		this.orientation.up.set(0, 0, 1);
		this.orientation.rotate(this.eulerAngles);
	};

	this.prePhysics := function(frametime) {
		if (this.dynamicsObject = null) {
			return;
		}
		if (this.flyMode) {
			var desiredVelocity := this.orientation.directionToExternalSpace(this.movement);
			desiredVelocity.normalize();
			desiredVelocity._OpMulBy(this.maxFlySpeed);
			var maxForce := this.maxFlyForce * frametime * 1024;
			var velocity := Vector();
			this.dynamicsObject.getVelocity(velocity);
			var requiredForce := desiredVelocity._OpSub(velocity)._OpMul(maxForce);
			if (requiredForce.length() > maxForce) {
				requiredForce._OpMulBy(maxForce / requiredForce.length());
			}
			this.dynamicsObject.applyForce(this.dynamicsOrientation.origin, requiredForce);
			this.movement.set(0, 0, 0);
			return;
		}

		if (this.dynamicsObject.onGround) {
			var normal := Vector();
			this.dynamicsObject.getGroundNormal(normal);

			var terrainForward := this.orientation.right.crossProduct(normal);
			terrainForward.normalize();
			var terrainRight := this.orientation.forward.crossProduct(normal);
			terrainRight.scale(-1);
			terrainRight.normalize();

			var velocity := Vector();
			this.dynamicsObject.getVelocity(velocity);

			terrainForward.scale(this.movement.x);
			terrainRight.scale(this.movement.y);
			var desiredVelocity := terrainForward._OpAdd(terrainRight);
			desiredVelocity.normalize();
			desiredVelocity := desiredVelocity._OpMul(this.maxWalkSpeed);

			var maxForce := this.maxWalkForce * frametime * 1024;

			var requiredForce := desiredVelocity._OpSub(velocity)._OpMul(maxForce);
			if (requiredForce.length() > maxForce) {
				requiredForce := requiredForce._OpMul(maxForce / requiredForce.length());
			}

			this.dynamicsObject.applyForce(this.dynamicsOrientation.origin, requiredForce);
		}

		this.movement.set(0, 0, 0);
	};

	this.postPhysics := function(frametime) {
		if (this.dynamicsObject = null) {
			return;
		}
		this.dynamicsObject.getOrientation(this.dynamicsOrientation);
		this.orientation.origin.set(this.dynamicsOrientation.origin);
	};
};
