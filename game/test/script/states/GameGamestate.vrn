/**
 * Gamestate which contains the game being played.
 */
GameGamestate := function(controls) {
	this.controls := @private controls;
	controls.bindInput("Key_w", "game.forward");
	controls.bindInput("Key_s", "game.backward");
	controls.bindInput("Key_a", "game.left");
	controls.bindInput("Key_d", "game.right");
	controls.bindInput("Key_e", "game.rollleft");
	controls.bindInput("Key_q", "game.rollright");
	controls.bindInput("Key_r", "game.up");
	controls.bindInput("Key_f", "game.down");
	controls.bindInput("Key_276", "game.yawleft");
	controls.bindInput("Key_275", "game.yawright");
	controls.bindInput("Key_274", "game.pitchup");
	controls.bindInput("Key_273", "game.pitchdown");
	controls.bindInput("Key_l", "game.toggleflymode");
	controls.bindInput("Key_p", "game.toggleprojection");

	this.projection := @private 0;

	this.config := @private {
		mouseSensitivity := 1 / 256
	};

	// TODO: Systems should be encapsulated in a World object, which can
	// perform this logic. There's no reason not to have multiple worlds.
	this.addToSystems := @private function(entity) {
		foreach (key name, value system in this.systems) {
			if (entity.interfaces[name] != null) {
				system.addComponent(entity.interfaces[name]);
			}
		}
	};

	this.removeFromSystems := @private function(entity) {
		foreach (key name, value system in this.systems) {
			if (entity.interfaces[name] != null) {
				system.removeComponent(entity.interfaces[name]);
			}
		}
	};

	this.start := @readonly function() {
		// Set up world stuff.
		var physics := new PhysicsSystem();
		var orrery := new OrrerySystem();
		var renderer := new RendererSystem(orrery);

		this.systems := @private {
			Physics := physics,
			Renderer := renderer,
			Orrery := orrery
		};

		// Create orrery.
		var sun := new Entity();
		sun.addComponent(OrreryPlanetComponent, {
			name := "sun",
			ascendingNodeLongitude := 270,
			inclination := 0,
			orbitalPhase := 0,
			orbitalRadius := 0,
			orbitalPeriod := 1,
			siderealPeriod := 10,
			axialTilt := 0,
			size := 1.09,
			isStar := true,
			colour := "#fe4",
			rings := {}
		});
		this.addToSystems(sun);

		var saturn := new Entity();
		saturn.addComponent(OrreryPlanetComponent, {
			name := "saturn",
			ascendingNodeLongitude := 270,
			inclination := 0,
			orbitalPhase := 0,
			orbitalRadius := 40,
			orbitalPeriod := 360,
			siderealPeriod := 8,
			axialTilt := 32,
			size := 0.2,
			isStar := false,
			colour := "#953",
			rings := {
				{ colour := "#221", innerRadius := 0.3, outerRadius := 0.33 },
				{ colour := "#222", innerRadius := 0.34, outerRadius := 0.40 }
			}
		});
		sun.interfaces.Orrery.addChild(saturn.interfaces.Orrery);
		this.addToSystems(saturn);

		var earth := new Entity();
		earth.addComponent(OrreryPlanetComponent, {
			name := "earth",
			ascendingNodeLongitude := 270,
			inclination := 45,
			orbitalPhase := 0.25,
			orbitalRadius := 1,
			orbitalPeriod := 10,
			// orbital period (for light) = 360 (saturn)
			// there'll be 361 sidereal days because there's an extra one absorbed by orbiting around
			// this value sets a sidereal day so that a synodic day (i.e. as experienced on the surface of a planet) is 1.
			siderealPeriod := 361/360,
			axialTilt := 24,
			size := 0.01,
			isStar := false,
			colour := "#593",
			rings := {}
		});
		saturn.interfaces.Orrery.addChild(earth.interfaces.Orrery);
		this.addToSystems(earth);

		var moon := new Entity();
		moon.addComponent(OrreryPlanetComponent, {
			name := "moon",
			ascendingNodeLongitude := 270,
			inclination := 36,
			orbitalPhase := 0.4,
			orbitalRadius := 0.5,
			orbitalPeriod := 6,
			siderealPeriod := 0.3,
			axialTilt := 5,
			size := 0.01,
			isStar := false,
			colour := "#543",
			rings := {}
		});
		saturn.interfaces.Orrery.addChild(moon.interfaces.Orrery);
		this.addToSystems(moon);

		this.systems.Orrery.setViewpoint(earth.interfaces.Orrery, 0.0, 0.5);

		// Create terrain.
		var terrain := new Entity();
		//terrain.addComponent(TerrainDataComponent, { type := "image", source := "terrain/greece512.png" });
		terrain.addComponent(TerrainDataComponent, { type := "procedural", source := "terrain/greece512.png" });
		terrain.addComponent(TerrainPhysicsComponent, {});
		terrain.addComponent(TerrainModelComponent, {});
		this.addToSystems(terrain);

		// Create player.
		var player := new Entity();
		var playerOrigin := Vector(0, 0, 0);
		playerOrigin.z := terrain.getInterface("TerrainData").getHeightAtPoint(playerOrigin) + 1;
		player.addComponent(CharacterPhysicsComponent, { origin := playerOrigin });
		player.addComponent(CharacterControlsComponent, {});
		player.addComponent(CharacterCameraComponent, {});
		this.addToSystems(player);

		this.playerEntity := player;
		this.systems.Renderer.setCamera(player.interfaces.Camera);

		// Jump to sunset to test day/night
		//orrery.rendererFrame(0.3 * orrery.speedScale);
	};

	this.stop := @readonly function() {
		this.systems := null;
	};

	this.event := @readonly function(event) {
    	var controlEvents := this.controls.handleEvent(event, {"game"});
		if (controlEvents["game.toggleprojection"] = 1) {
			this.projection := !this.projection;
		}
		controlEvents["game.toggleprojection"] := null;

		if (tableCount(controlEvents) > 0) {
			this.playerEntity.interfaces.Controls.onInputEvent(controlEvents);
		}

		if (event.type = "MouseMove" && event.relative) {
			this.playerEntity.interfaces.Controls.mouseInput({
				x := event.motion.x * this.config.mouseSensitivity * -1,
				y := event.motion.y * this.config.mouseSensitivity * -1
			});
		}
	};

	this.frame := @readonly function(interval) {
		this.playerEntity.interfaces.CharacterControls.updateControls(this.controls, interval);
		this.systems.Physics.frame(interval);
		this.systems.Renderer.frame(interval);
	};

	this.render := @readonly function() {
		this.systems.Renderer.render();
	};
};
