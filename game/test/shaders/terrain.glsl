//------------------------------------------------------------------------------
// Varying
//------------------------------------------------------------------------------

// ATI cards only support 8 varyings, regardless of their size, so these defines
// keep the code readable.
varying vec4 blend;
#define skyBlend    blend[0]
#define fogBlend    blend[1]
#define slopeBlend  blend[2]
#define detailBlend blend[3]

varying vec4 coord;
#define texCoord    coord.xy
#define screenCoord coord.zw

varying float light;

//------------------------------------------------------------------------------
// Vertex 
//------------------------------------------------------------------------------
#ifdef VERTEX

uniform vec3 camOrg;
uniform float viewDist;
uniform float fogDist;
uniform vec3 sunDir;
uniform vec2 screenSize;
uniform vec2 textureSize;
uniform vec4 fogColour;

void main (void)
{	
	// Planar texture mapping
	texCoord.x = gl_Vertex.x;
	texCoord.y = gl_Vertex.y;
	
	// for blending the sky-fade
	float clear = viewDist * 0.8;
	float fade = viewDist * 0.2;
	float dist = length( gl_Vertex.xyz - camOrg );
	float f = max(dist - clear, 0.0);
	skyBlend = min(f / fade, 1.0);
	
	// for blending light-scatter fog
	fogBlend = min(dist/(fogDist), 1.0) * fogColour[3];
	
	// for blending detail texture
	detailBlend = clamp(dist / 2048.0, 0.0, 1.0);
	
	// for blending slope type
	slopeBlend = clamp(gl_Normal.z, 0.0, 1.0);
	
	// calculate light level given sun angle
	light = clamp(dot(gl_Normal.xyz, sunDir), 0.0, 1.0);
	
	// set the fragment position
	vec4 v = vec4( gl_Vertex[0], gl_Vertex[1], gl_Vertex[2], 1.0 );
	gl_Position = gl_ModelViewProjectionMatrix * v;
	
	// ATI cards do not support the gl builtin gl_FragCoord, but we can lerp
	// screen coordinates anyway, putting the transformation per-vertex.
	screenCoord = (gl_Position.xy / gl_Position.w + 1.0) * 0.5;
	screenCoord[0] *= screenSize.x / textureSize.x;
	screenCoord[1] *= screenSize.y / textureSize.y;
}

#endif // VERTEX
//------------------------------------------------------------------------------
// Fragment
//------------------------------------------------------------------------------
#ifdef FRAGMENT

uniform sampler2D texture0;
uniform sampler2D texture1;
uniform sampler2D texture2;
uniform vec3 sunlightColour;
uniform vec3 ambientColour;
uniform vec4 fogColour;

void main (void)
{
    // terrain with detail level
    vec2 tertc1 = vec2( texCoord.x / 2048.0, texCoord.y / 2048.0);
    vec2 tertc2 = vec2( tertc1.x * 16.0, tertc1.y * 16.0);
                                 
    vec4 detailedTerrainColour = (texture2D(texture0, tertc2) * (1.0-detailBlend) +
                                  texture2D(texture0, tertc1) * detailBlend) * slopeBlend;
                                 
    detailedTerrainColour += (texture2D(texture1, tertc2) * (1.0-detailBlend) +
                              texture2D(texture1, tertc1) * detailBlend) * (1.0-slopeBlend);
                              
    // light the terrain
    detailedTerrainColour.xyz = min(detailedTerrainColour.xyz * (sunlightColour*light + ambientColour), 1.0);

    // fog with the average sky colour
    vec4 foggedTerrainColour = fogColour * fogBlend +
                               detailedTerrainColour * (1.0-fogBlend);

    // fade to the skydome colour at the very edge of everything
    gl_FragColor = foggedTerrainColour * (1.0-skyBlend) +
                   texture2D( texture2, screenCoord ) * skyBlend;
                   
    gl_FragColor[3] = 1.0;
}

#endif // FRAGMENT
//------------------------------------------------------------------------------

