
//------------------------------------------------------------------------------
// Varying
//------------------------------------------------------------------------------

varying vec4 v_Modulation;
varying vec2 v_TexCoord;

//------------------------------------------------------------------------------
// Vertex
//------------------------------------------------------------------------------
#ifdef VERTEX

uniform vec3 lightNormal;
uniform vec3 lightColour;
uniform float lightMultiply;
uniform float lightAdd;

void main()
{
	float dotProduct = dot(gl_Normal, lightNormal) * lightMultiply + lightAdd;
	gl_Position = gl_ModelViewProjectionMatrix * vec4(gl_Vertex.xyz, 1.0);
	v_Modulation = gl_Color * vec4(lightColour * clamp(dotProduct, 0.0, 1.0), 1.0);
	v_TexCoord = gl_MultiTexCoord0.xy;
}

#endif // VERTEX
//------------------------------------------------------------------------------
// Fragment
//------------------------------------------------------------------------------
#ifdef FRAGMENT

uniform sampler2D texture0;
uniform vec2 saturation; // x is amount, y is inverse
uniform vec2 gamma; // x is amount, y is inverse

vec4 MakeGreyVector(in vec4 colour)
{
	vec3 colourGamma = pow(colour.xyz, vec3(gamma.x, gamma.x, gamma.x));
	float grey = pow((colourGamma.x + colourGamma.y + colourGamma.z) * 0.3333, gamma.y);
	return vec4(grey, grey, grey, colour.w);
}

void main (void)
{
	vec4 colour = texture2D(texture0, v_TexCoord) * v_Modulation;
	vec4 grey = MakeGreyVector(colour);
	gl_FragColor = colour * saturation.x + grey * saturation.y;
}

#endif // FRAGMENT
//------------------------------------------------------------------------------
