/**
 * Post processing from an old retro style game.
 * Uses the fragment to upscale a scene rendered to texture.
 */

//------------------------------------------------------------------------------
// Varying
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Vertex
//------------------------------------------------------------------------------
#ifdef VERTEX

void main (void)
{
	/*
		gl_Vertex is the input vertex.
		gl_MultiTexCoord0 and 1 are input texture coordinatex.

		gl_Position is the output vertex coordinates in screen space.
		gl_TexCoord[] are a built-in varying - they are output per-vertex here
		and the fragment shader receives the interpolated versions.
	*/

	// set the fragment position
	vec4 v = vec4( gl_Vertex[0], gl_Vertex[1], gl_Vertex[2], 1.0 );
	gl_Position = gl_ModelViewProjectionMatrix * v;
	gl_TexCoord[0] = gl_MultiTexCoord0;
	gl_FrontColor = gl_Color;
}

#endif // VERTEX
//------------------------------------------------------------------------------
// Fragment
//------------------------------------------------------------------------------
#ifdef FRAGMENT

uniform sampler2D texInput;
uniform vec2      pixelSize;

#define SHARPNESS 0.35
#define GHOSTING  0.25
#define SCANLINES 0.45
#define COLOUR    0.90

void main (void)
{
	// sharpening
	vec2 sharpnessCoord = gl_TexCoord[0].xy;
	sharpnessCoord.x += pixelSize.x;
	vec4 sharpen1 = texture2D(texInput, gl_TexCoord[0].xy);
	vec4 sharpen2 = texture2D(texInput, sharpnessCoord);
	vec4 sharpenDiff = (sharpen1 - sharpen2) * SHARPNESS;
	gl_FragColor = sharpen1 + sharpenDiff;

	// ghosting
	vec2 ghostingCoord1 = gl_TexCoord[0].xy;
	vec2 ghostingCoord2 = gl_TexCoord[0].xy;
	ghostingCoord1.x += pixelSize.x * -1.0;
	ghostingCoord2.x += pixelSize.x * -2.0;
	vec4 ghost = (texture2D(texInput, ghostingCoord1) + texture2D(texInput, ghostingCoord2)) * GHOSTING;
	gl_FragColor = gl_FragColor + ghost;

	// scanlines
	float scanline = gl_FragCoord.y*0.5 - floor(gl_FragCoord.y*0.5);
	scanline = (scanline * SCANLINES) + (1.0 - SCANLINES);
	gl_FragColor.xyz = gl_FragColor.xyz * scanline;

	// desaturation
	float average = (gl_FragColor[0] + gl_FragColor[1] + gl_FragColor[2]) * 0.3333;
	vec3  blendVec;
	blendVec.x = average;
	blendVec.y = average;
	blendVec.z = average;
	gl_FragColor.xyz = gl_FragColor.xyz*COLOUR + blendVec*(1.0 - COLOUR);
}

#endif // FRAGMENT
//------------------------------------------------------------------------------

