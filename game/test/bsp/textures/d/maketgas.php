<?php

$data = array(
	0, // identsize
	0, // colourmap none
	2, // file type rgb
	0, 0, // colourmapstart
	0, 0, // colourmaplength
	0, // colourmapbits
	0, 0, // x origin
	0, 0, // y origin
	1, 0, // width (LE?)
	1, 0, // height (LE?)
	24, // bits per pixel
	0, // image descriptor bits (VH flip)
);

function MakeFile($r, $g, $b)
{
	global $data;

	if ($r == 0) $r = "0";
	if ($g == 0) $g = "0";
	if ($b == 0) $b = "0";
	$filename = $r . $g . $b . ".tga";

	$fp = fopen($filename, 'wb');
	foreach ($data as $d) {
		fwrite($fp, chr($d));
	}
	fwrite($fp, chr($b * 25.5));
	fwrite($fp, chr($g * 25.5));
	fwrite($fp, chr($r * 25.5));
	fclose($fp);
}

for ($r = 0; $r <= 9; ++$r)
{
	for ($g = 0; $g <= 9; ++$g)
	{
		for ($b = 0; $b <= 9; ++$b)
		{
			MakeFile($r, $g, $b);
		}
	}
}

?>
