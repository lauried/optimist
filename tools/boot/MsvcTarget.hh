
//-----------------------------------------------------------------------------
// Build Target Type: GCC
//-----------------------------------------------------------------------------

class MsvcTarget {
public:
	void Usage();
	int HandleCommand(std::deque<std::string> &arguments);

private:
	/**
	 * Add defines that we were compiled with to our platform.
	 *
	 * The intention of this is so that we traverse the same code that the compiler
	 * will try to traverse, and thus find all required object files.
	 * The user can also use directives to add extra files in cases where external
	 * source code somehow evades being traversed.
	 *
	 * What we should instead do is determine the platform being compiled and the
	 * compiler being used, and set defines for that.
	 */
	void SetPlatformDefines(std::set<std::string> *defines);
};

void MsvcTarget::Usage()
{
	std::cout << "msvc <startFile> [ <define> ... ]" << std::endl;
}

void MsvcTarget::SetPlatformDefines(std::set<std::string> *defines)
{
// Detect platform:
// http://nadeausoftware.com/articles/2012/01/c_c_tip_how_use_compiler_predefined_macros_detect_operating_system
#ifdef _WIN32
	defines->insert(std::string("_WIN32"));
#endif

#ifdef _WIN64
	defines->insert(std::string("_WIN64"));
#endif

// Compiler specific.
	defines->insert(std::string("_MSC_VER"));
}

int MsvcTarget::HandleCommand(std::deque<std::string> &arguments)
{
	if (arguments.size() < 1)
	{
		Usage();
		return -1;
	}

	std::string startFile = arguments.front();
	arguments.pop_front();

	// Default build name comes from the file that we called.
	std::string buildName = StripFilePath(StripFileExtension(startFile));

	// Gather build information
	Build build;
	SetPlatformDefines(&build.initialDefines);

	for (auto it = arguments.begin(); it != arguments.end(); ++it)
	{
		build.initialDefines.insert(*it);
	}

	// Scan build
	build.FindBuildFiles(startFile);

	// Perform build
	MsvcBuilder builder;
	builder.defaultTarget = "build/" + buildName;
	builder.defaultObjectDir = "obj/" + buildName;
	return builder.Compile(&build);
}
