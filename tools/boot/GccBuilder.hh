
//-----------------------------------------------------------------------------
// Compilation Invoker: GCC
//-----------------------------------------------------------------------------

/**
 * Performs the build. It's hard-coded to GCC for now, because there's no point
 * prematurely designing a way to build multiple systems.
 */
class GccBuilder : public GenericBuilder {
public:
	GccBuilder()
	{
		defaultTarget = "build/UnknownTarget";
		defaultObjectDir = "obj/UnknownTarget";
		targetName = "gcc";
	}

protected:
	virtual void SetDefaultBuildOptions(BuildOptions *options);
	virtual std::string BuildCompileCommand(std::string filename, std::string objectFilename, BuildOptions *options);
	virtual std::string BuildLinkCommand(std::set<std::string> *objectFiles, BuildOptions *options);
};

void GccBuilder::SetDefaultBuildOptions(GccBuilder::BuildOptions *options)
{
	options->cCompiler = "gcc";
	options->cppCompiler = "g++";
	options->linker = "g++";
}

std::string GccBuilder::BuildCompileCommand(std::string filename, std::string objectFilename, BuildOptions *buildOptions)
{
	std::string compiler = buildOptions->cppCompiler;
	std::set<std::string> *options = &(buildOptions->cppOptions);
	std::string extension = GetFileExtension(filename);
	if (!extension.compare("c"))
	{
		compiler = buildOptions->cCompiler;
		options = &(buildOptions->cOptions);
	}

	std::stringstream command;

	command << compiler << " -c";
	for (auto it = options->begin(); it != options->end(); ++it)
	{
		command << " " << *it;
	}
	for (auto it = buildOptions->sourceDirs.begin(); it != buildOptions->sourceDirs.end(); ++it)
	{
		command << " -I" << *it;
	}

	command << " " << filename;

	command << " -o " << objectFilename << ".o";

	return command.str();
}

std::string GccBuilder::BuildLinkCommand(std::set<std::string> *objectFiles, BuildOptions *options)
{
	std::stringstream command;

	command << options->linker;
	command << " -o " << options->target;

	for (auto it = objectFiles->begin(); it != objectFiles->end(); ++it)
	{
		command << " " << *it << ".o";
	}

	for (auto it = options->libraries.begin(); it != options->libraries.end(); ++it)
	{
		command << " -l" << *it;
	}

	return command.str();
}
