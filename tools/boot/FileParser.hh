
//-----------------------------------------------------------------------------
// Scanned File
//-----------------------------------------------------------------------------

/**
 * Parses a source or header file.
 */
class FileParser {
public:
	std::string filename;
	std::vector<std::string> includes;
	std::vector<Directive> directives;
	uint64_t modifiedTime;

	FileParser(std::string filename)
	{
		(*this).filename = filename;
		modifiedTime = ModifiedTime(filename);
	}

	bool Parse(std::set<std::string> *defines);

private:
	struct Ifdef {
		bool first, second, current;
		Ifdef() {}
		Ifdef(bool first, bool second) : first(first), second(second), current(first) {}
	};
};

bool FileParser::Parse(std::set<std::string> *defines)
{
	std::stack<Ifdef> ifdefs;

	// Open the file and check it exists.
	std::ifstream file(filename);
	if (!file.is_open())
	{
		std::cout << "Error: could not open " << filename << std::endl;
		return false;
	}

	// Process line by line.
	std::string line;
	while (std::getline(file, line))
	{
		if (line.length() < 1 || (line[0] != '#' && line.substr(0, 4) != "//##"))
		{
			continue;
		}

		// Tokenize.
		std::stringstream tokenStream(line);
		std::vector<std::string> tokens { std::istream_iterator<std::string>{ tokenStream }, std::istream_iterator<std::string>{} };
		if (tokens.size() < 1)
		{
			continue;
		}

		if (!tokens[0].compare("#else"))
		{
			if (ifdefs.size() < 1)
			{
				throw std::logic_error("#else without corresponding #if*");
			}
			ifdefs.top().current = ifdefs.top().second;
		}
		else if (!tokens[0].compare("#endif"))
		{
			if (ifdefs.size() < 1)
			{
				throw std::logic_error("#endif without corresponding #if*");
			}
			ifdefs.pop();
		}
		else if (!tokens[0].compare("#undef") && tokens.size() > 1)
		{
			auto it = defines->find(tokens[1]);
			if (it != defines->end())
			{
				defines->erase(it);
			}
		}
		else if (!tokens[0].compare("#ifdef") && tokens.size() > 1)
		{
			bool defined = (defines->find(tokens[1]) != defines->end());
			ifdefs.push(Ifdef(defined, !defined));
		}
		else if (!tokens[0].compare("#ifndef") && tokens.size() > 1)
		{
			bool defined = (defines->find(tokens[1]) != defines->end());
			ifdefs.push(Ifdef(!defined, defined));
		}
		else if (!tokens[0].compare("#if") && tokens.size() > 1)
		{
			ifdefs.push(Ifdef(true, true));
		}
		else if ((!tokens[0].compare("#elseif") || !tokens[0].compare("#elif")) && tokens.size() > 1)
		{
			if (!ifdefs.top().current)
			{
				bool defined = (defines->find(tokens[1]) != defines->end());
				ifdefs.top().current = defined;
				ifdefs.top().second = !defined;
			}
		}

		// If the current ifdef prevents us from processing:
		if (ifdefs.size() && !ifdefs.top().current)
		{
			continue;
		}

		if (!tokens[0].compare("#include") && tokens.size() > 1)
		{
			std::string incFile = tokens[1];
			if (incFile[0] == '"' && incFile[incFile.size() - 1] == '"')
			{
				includes.push_back(incFile.substr(1, incFile.size() - 2));
			}
		}
		else if (!tokens[0].compare("#define") && tokens.size() > 1)
		{
			defines->insert(tokens[1]);
		}

		if (tokens[0].compare("//##") || tokens.size() < 3)
		{
			continue;
		}

		Directive directive;
		directive.name = tokens[1];
		for (int i = 2; i < tokens.size(); i += 1)
		{
			directive.values.push_back(tokens[i]);
		}
		directives.push_back(directive);
	}

	if (ifdefs.size() > 0)
	{
		throw std::logic_error("Found eof when expecting #else or #endif in " + filename);
	}

	return true;
}
