
//-----------------------------------------------------------------------------
// Source File
//-----------------------------------------------------------------------------

/**
 * Recursively parses all files referred to by a source (non-header) file.
 */
class SourceFile {
public:
	std::string filename;
	std::vector<std::string> foundSourceFiles;
	std::set<std::string> defines;
	std::vector<Directive> directives;
	uint64_t sourceModifiedTime;
	uint64_t dependModifiedTime;

	SourceFile(std::string filename)
	{
		(*this).filename = filename;
		sourceModifiedTime = ModifiedTime(filename);
		dependModifiedTime = 0;
	}

	/**
	 * Sets our initial defines.
	 */
	void SetInitialDefines(std::set<std::string> *defines);

	/**
	 * Parse, traversing all included files.
	 */
	void Parse(std::set<std::string> *searchDirs);
};

void SourceFile::SetInitialDefines(std::set<std::string> *defines)
{
	for (auto it = defines->begin(); it != defines->end(); ++it)
	{
		this->defines.insert(*it);
	}
}

void SourceFile::Parse(std::set<std::string> *searchDirs)
{
	std::set<std::string> visitedFiles;
	std::queue<std::string> filesToVisit;

	filesToVisit.push(filename);

	while (filesToVisit.size() > 0)
	{
		std::string filename = FindFile(filesToVisit.front(), searchDirs);
		filesToVisit.pop();

		FileParser parser(filename);
		if (!parser.Parse(&defines))
		{
			continue;
		}
		if (parser.modifiedTime > dependModifiedTime)
		{
			dependModifiedTime = parser.modifiedTime;
		}

		// Add all header files to the list of things to traverse.
		for(auto it = parser.includes.begin(); it != parser.includes.end(); ++it)
		{
			if (visitedFiles.find(*it) == visitedFiles.end()) {
				visitedFiles.insert(*it);
				filesToVisit.push(*it);
			}

			std::string extension = GetFileExtension(*it);
			std::replace(extension.begin(), extension.end(), 'h', 'c');
			std::string sourceFile = StripFileExtension(*it) + "." + extension;
			foundSourceFiles.push_back(sourceFile);
		}

		// Intercept some search dirs here which affect our traversal.
		for (auto it = parser.directives.begin(); it != parser.directives.end(); ++it)
		{
			// TODO: Abstract directives so that we get rid of this duplicate name.
			if (!it->name.compare("add_source_dirs"))
			{
				for (auto it2 = it->values.begin(); it2 != it->values.end(); ++it2)
				{
					searchDirs->insert(*it2);
				}
			}

			// Other directives don't have any effect on where to search.
			directives.push_back(*it);
		}
	}
}
