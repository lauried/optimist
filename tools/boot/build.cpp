/*
This is the source for the project builder.
It takes as parameter the start file, and processes accordingly.
This source file can provide everything needed to do the actual build.
*/
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <deque>
#include <iostream>
#include <fstream>
#include <iterator>
#include <map>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <stdexcept>
#include <string>
#include <vector>

#include <sys/stat.h>

#ifdef MSC_VER
#include <direct.h>
#endif

#include "tinydir.h"
#include "ThreadPool.h"

// The program is broken up into these parts, but to keep compiling it simple,
// they aren't separate compilation units.
#include "helpers.hh"
#include "Directive.hh"
#include "FileParser.hh"
#include "SourceFile.hh"
#include "Build.hh"
#include "GenericBuilder.hh"
#include "GccBuilder.hh"
#include "GccTarget.hh"
#include "MsvcBuilder.hh"
#include "MsvcTarget.hh"
#include "CodeBlocksBuilder.hh"
#include "CodeBlocksTarget.hh"

//-----------------------------------------------------------------------------
// Init
//-----------------------------------------------------------------------------

void Usage(std::string program)
{
	std::cout << "Usage:" << std::endl
		<< "  " << program << " <target>" << std::endl
		<< "  " << program << " help <target>" << std::endl
		<< std::endl
		<< "Available targets: gcc, msvc, codeblocks" << std::endl;
}

/**
 * Program entry point.
 * Validate input, initialize state and kick off processing.
 * State is the set of defines we expect for this platform.
 */
int main(int argc, char *argv[])
{
#if 1
	// Insufficient arguments at all.
	if (argc < 2)
	{
		Usage(argv[0]);
		return 0;
	}

	std::deque<std::string> arguments;
	for (int i = 1; i < argc; i += 1)
	{
		arguments.push_back(std::string(argv[i]));
	}
#else
	std::deque<std::string> arguments;
	arguments.push_back("codeblocks");
	arguments.push_back("project.txt");
	arguments.push_back("Build/CodeBlocksTest.cbp");
#endif

	// TODO: Unused - would trigger an individual target's help section.
	bool showHelp = false;
	if (!arguments.front().compare("help"))
	{
		arguments.pop_front();
		showHelp = true;
	}

	// Insufficient arguments for a help command.
	if (arguments.size() < 1)
	{
		Usage(argv[0]);
		return 0;
	}

	std::string target = arguments.front();
	arguments.pop_front();

	if (!target.compare("gcc"))
	{
		GccTarget target;
		return target.HandleCommand(arguments);
	}
	else if (!target.compare("msvc"))
	{
		MsvcTarget target;
		return target.HandleCommand(arguments);
	}
	else if (!target.compare("codeblocks"))
	{
		CodeBlocksTarget target;
		return target.HandleCommand(arguments);
	}
	else
	{
		std::cout << "Invalid target " << target << std::endl;
		return -1;
	}
}
