
//-----------------------------------------------------------------------------
// Compilation Invoker: Code::Blocks (renders a project file)
//-----------------------------------------------------------------------------

class CodeBlocksBuilder : public GenericBuilder {
public:
	void SetTitle(std::string pTitle) { title = pTitle; }
	void AddTarget(Build *build, std::string name);
	void AddUnit(std::string filename);

	// Overrides GenericBuilder's compile method because we actually output
	void Compile(std::string filename);

protected:
	// These are needed so that the class is complete.
	void SetDefaultBuildOptions(BuildOptions *options) {}
	std::string BuildCompileCommand(std::string filename, std::string objectFilename, BuildOptions *options) {}
	std::string BuildLinkCommand(std::set<std::string> *objectFiles, BuildOptions *options) {}

private:
	std::string title;
	std::string basePath;
	std::map<std::string, Build*> targets;
	std::map<std::string, std::vector<std::string>> units;

	void AddUnit(std::string filename, std::string target);
	void WriteProject(std::ofstream &file);
	void WriteTarget(std::ofstream &file, std::string name, Build *build);
	void WriteUnits(std::ofstream &file);
};

void CodeBlocksBuilder::AddUnit(std::string filename)
{
	AddUnit(filename, "");
}

void CodeBlocksBuilder::AddUnit(std::string filename, std::string target)
{
	auto itUnit = units.find(filename);
	if (itUnit == units.end())
	{
		// insert returns pair(iterator, bool[true if inserted, false if already existed])
		itUnit = units.insert(std::make_pair(filename, std::vector<std::string>())).first;
	}
	if (target.length() > 0)
	{
		itUnit->second.push_back(target);
	}
}

void CodeBlocksBuilder::AddTarget(Build *build, std::string name)
{
	targets.insert(std::make_pair(name, build));
	for (auto itObject = build->objects.begin(); itObject != build->objects.end(); ++itObject)
	{
		AddUnit(itObject->filename, name);
	}
}

void CodeBlocksBuilder::Compile(std::string filename)
{
	basePath = GetFilePath(filename);

	std::ofstream project;
	project.open(filename);

	project << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>" << std::endl;

	WriteProject(project);
}

void CodeBlocksBuilder::WriteProject(std::ofstream &file)
{
	file << "<CodeBlocks_project_file>" << std::endl;
	file << "\t<FileVersion major=\"1\" minor=\"6\" />" << std::endl;
	file << "\t<Project>" << std::endl;
	file << "\t\t<Option title=\"" << title << "\" />" << std::endl;
	file << "\t\t<Option pch_mode=\"2\" />" << std::endl;
	file << "\t\t<Option compiler=\"gcc\" />" << std::endl;

	file << "\t\t<Build>" << std::endl;
	for (auto itTarget = targets.begin(); itTarget != targets.end(); ++itTarget)
	{
		WriteTarget(file, itTarget->first, itTarget->second);
	}
	file << "\t\t</Build>" << std::endl;

	WriteUnits(file);

	file << "\t</Project>" << std::endl;

	file << "</CodeBlocks_project_file>" << std::endl;
}

void CodeBlocksBuilder::WriteTarget(std::ofstream &file, std::string name, Build *build)
{
	if (build->objects.size() < 1)
	{
		std::cout << "No units found for build " << name << std::endl;
		return;
	}

	// Generate buildoptions for the build.
	BuildOptions options;
	options.cCompiler = "gcc";
	options.cppCompiler = "g++";
	options.linker = "g++";
	options.SetDefaults("gcc", "build/UnknownTarget");
	for (int i = 0; i < build->objects.size(); i += 1)
	{
		const Build::Object *firstObject = &(build->objects[i]);
		options.ApplyDirectives(&(firstObject->directives), "gcc");
	}

	file << "\t\t\t<Target title=\"" << name << "\">" << std::endl;

	// Options
	file << "\t\t\t\t<Option output=\"" << GetRelativePath(basePath, options.target) << "\" prefix_auto=\"0\" extension_auto=\"0\" />" << std::endl;
	// TODO: Scan object directives for some codeblocks options like working directory.
	file << "\t\t\t\t<Option working_dir=\"../game/test/\" />" << std::endl;
	file << "\t\t\t\t<Option object_output=\"" << GetRelativePath(basePath, options.objectDir) << "\" />" << std::endl;
	file << "\t\t\t\t<Option type=\"1\" />" << std::endl;
	file << "\t\t\t\t<Option compiler=\"gcc\" />" << std::endl;

	// Compiler
	file << "\t\t\t\t<Compiler>" << std::endl;
	for (auto itOption = options.cppOptions.begin(); itOption != options.cppOptions.end(); ++itOption)
	{
		file << "\t\t\t\t\t<Add option=\"" << (*itOption) << "\" />" << std::endl;
	}
	for (auto itDir = options.sourceDirs.begin(); itDir != options.sourceDirs.end(); ++itDir)
	{
		file << "\t\t\t\t\t<Add directory=\"" << GetRelativePath(basePath, *itDir) << "\" />" << std::endl;
	}
	file << "\t\t\t\t</Compiler>" << std::endl;

	// Linker
	file << "\t\t\t\t<Linker>" << std::endl;
	for (auto itLibrary = options.libraries.begin(); itLibrary != options.libraries.end(); ++itLibrary)
	{
		file << "\t\t\t\t\t<Add library=\"" << (*itLibrary) << "\" />" << std::endl;
	}
	for (auto itDir = options.libDirs.begin(); itDir != options.libDirs.end(); ++itDir)
	{
		file << "\t\t\t\t\t<Add directory=\"" << GetRelativePath(basePath, *itDir) << "\" />" << std::endl;
	}
	file << "\t\t\t\t</Linker>" << std::endl;

	file << "\t\t\t</Target>" << std::endl;
}

void CodeBlocksBuilder::WriteUnits(std::ofstream &file)
{
	for (auto itUnit = units.begin(); itUnit != units.end(); ++itUnit)
	{
		std::string filePath = GetRelativePath(basePath, itUnit->first);

		file << "\t\t\t<Unit filename=\"" << filePath << "\">" << std::endl;
		file << "\t\t\t\t<Option target=\"&lt;{~None~}&gt;\"/>" << std::endl;
		for (auto itTarget = itUnit->second.begin(); itTarget != itUnit->second.end(); ++itTarget)
		{
			std::string filePath = GetRelativePath(basePath, *itTarget);
			file << "\t\t\t\t<Option target=\"" << (*itTarget) << "\" />" << std::endl;
		}
		file << "\t\t\t</Unit>" << std::endl;
	}
}
