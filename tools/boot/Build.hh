
//-----------------------------------------------------------------------------
// Files To Build
//-----------------------------------------------------------------------------

/**
 * Represents the information about a particular build.
 */
class Build {
public:
	struct Object {
		std::string filename;
		std::vector<Directive> directives;
		uint64_t sourceModifiedTime;
	};

	std::set<std::string> libraries;
	std::set<std::string> initialDefines;
	std::set<std::string> searchDirs;
	std::vector<Object> objects;

	/**
	 * Find all source files to build.
	 */
	void FindBuildFiles(std::string firstFile);

private:
	std::set<std::string> scannedSourceFiles;
};

void Build::FindBuildFiles(std::string firstFile)
{
	std::queue<std::string> buildQueue;

	buildQueue.push(firstFile);
	bool isFirst = true;

	while (buildQueue.size() > 0)
	{
		std::string buildFilename = FindFile(buildQueue.front(), &searchDirs);
		buildQueue.pop();

		if (scannedSourceFiles.find(buildFilename) != scannedSourceFiles.end() || !FileExists(buildFilename))
		{
			continue;
		}

		scannedSourceFiles.insert(buildFilename);

		std::cout << "Processing " << buildFilename << "..." << std::endl;

		std::vector<std::string> sourceFiles;
		SourceFile sourceFile(buildFilename);
		sourceFile.SetInitialDefines(&initialDefines);
		sourceFile.Parse(&searchDirs);

		// Add all source files that this file discovered to the build queue.
		for (int i = 0; i < sourceFile.foundSourceFiles.size(); i += 1)
		{
			if (FileExists(FindFile(sourceFile.foundSourceFiles[i], &searchDirs)))
			{
				buildQueue.push(sourceFile.foundSourceFiles[i]);
			}
		}

		// Intercept some directives here which affect our traversal.
		for (auto it = sourceFile.directives.begin(); it != sourceFile.directives.end(); ++it)
		{
			if (!it->name.compare("add_sources"))
			{
				for (auto it2 = it->values.begin(); it2 != it->values.end(); ++it2)
				{
					if (FileExists(FindFile(*it2, &searchDirs)))
					{
						buildQueue.push(*it2);
					}
				}
			}

			if (!it->name.compare("add_sources_in"))
			{
				for (auto it2 = it->values.begin(); it2 != it->values.end(); ++it2)
				{
					std::string dir = FindDirectory(*it2, &searchDirs);
					if (!DirectoryExists(dir))
					{
						continue;
					}
					std::vector<std::string> files;
					GetFilesInDirectory(dir, &files);
					for (auto it3 = files.begin(); it3 != files.end(); ++it3)
					{
						std::string extension = GetFileExtension(*it3);
						if (extension.find_first_of("h") != std::string::npos)
						{
							continue;
						}
						std::string file = PathJoin(*it2, *it3);
						if (FileExists(FindFile(file, &searchDirs)))
						{
							buildQueue.push(file);
						}
					}
				}
			}
		}

		// TODO: add_dir - use tinydir but we'll also have to find the real directory

		// Add the file.
		Object object;
		object.filename = buildFilename;
		object.directives = sourceFile.directives;
		object.sourceModifiedTime = sourceFile.dependModifiedTime;
		objects.push_back(object);

		isFirst = false;
	}
}
