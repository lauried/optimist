
//-----------------------------------------------------------------------------
// Build Target Type: GCC
//-----------------------------------------------------------------------------

class GccTarget {
public:
	void Usage();
	int HandleCommand(std::deque<std::string> &arguments);
};

void GccTarget::Usage()
{
	std::cout << "gcc <startFile> [ <define> ... ]" << std::endl;
}

int GccTarget::HandleCommand(std::deque<std::string> &arguments)
{
	if (arguments.size() < 1)
	{
		Usage();
		return -1;
	}

	std::string startFile = arguments.front();
	arguments.pop_front();

	// Default build name comes from the file that we called.
	std::string buildName = StripFilePath(StripFileExtension(startFile));

	// Gather build information
	Build build;
	SetPlatformDefines(&build.initialDefines);

	for (auto it = arguments.begin(); it != arguments.end(); ++it)
	{
		build.initialDefines.insert(*it);
	}

	// Scan build
	build.FindBuildFiles(startFile);

	// Perform build
	GccBuilder builder;
	builder.defaultTarget = "build/" + buildName;
	builder.defaultObjectDir = "obj/" + buildName;
	return builder.Compile(&build);
}
