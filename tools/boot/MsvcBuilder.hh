
//-----------------------------------------------------------------------------
// Compilation Invoker: MSVC
//-----------------------------------------------------------------------------

/**
 * Performs the build. It's hard-coded to GCC for now, because there's no point
 * prematurely designing a way to build multiple systems.
 */
class MsvcBuilder : public GenericBuilder {
public:
	MsvcBuilder()
	{
		defaultTarget = "build/UnknownTarget";
		defaultObjectDir = "obj/UnknownTarget";
		targetName = "msvc";
	}

protected:
	virtual void SetDefaultBuildOptions(BuildOptions *options);
	virtual std::string BuildCompileCommand(std::string filename, std::string objectFilename, BuildOptions *options);
	virtual std::string BuildLinkCommand(std::set<std::string> *objectFiles, BuildOptions *options);

private:
	void BuildResponseFile(std::string filename, std::set<std::string> *objectFiles);
};

void MsvcBuilder::SetDefaultBuildOptions(MsvcBuilder::BuildOptions *options)
{
	options->cCompiler = "cl";
	options->cppCompiler = "cl";
	options->linker = "link";
}

std::string MsvcBuilder::BuildCompileCommand(std::string filename, std::string objectFilename, BuildOptions *buildOptions)
{
	std::string compiler = buildOptions->cppCompiler;
	std::set<std::string> *options = &(buildOptions->cppOptions);
	std::string extension = GetFileExtension(filename);
	if (!extension.compare("c"))
	{
		compiler = buildOptions->cCompiler;
		options = &(buildOptions->cOptions);
	}

	std::stringstream command;

	command << compiler << " /c";
	for (auto it = options->begin(); it != options->end(); ++it)
	{
		command << " " << *it;
	}
	for (auto it = buildOptions->sourceDirs.begin(); it != buildOptions->sourceDirs.end(); ++it)
	{
		command << " /I" << *it;
	}

	command << " " << filename;

	command << " /Fo" << objectFilename << ".obj";

	return command.str();
}

std::string MsvcBuilder::BuildLinkCommand(std::set<std::string> *objectFiles, BuildOptions *options)
{
	std::string responseFilename = PathJoin(options->objectDir, "link.txt");
	BuildResponseFile(responseFilename, objectFiles);

	std::stringstream command;

	command << options->linker;
	command << " @" << responseFilename;

	for (auto it = options->libDirs.begin(); it != options->libDirs.end(); ++it)
	{
		command << " /LIBPATH:" << *it;
	}

	for (auto it = options->libraries.begin(); it != options->libraries.end(); ++it)
	{
		command << " " << *it << ".lib";
	}

	command << " /OUT:" << options->target;

	return command.str();
}

void MsvcBuilder::BuildResponseFile(std::string filename, std::set<std::string> *objectFiles)
{
	std::ofstream responseFile;
	responseFile.open(filename, std::ios::trunc);

	for (auto it = objectFiles->begin(); it != objectFiles->end(); ++it)
	{
		responseFile << *it << ".obj" << std::endl;
	}

	responseFile.close();
}
