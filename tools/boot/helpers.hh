
//-----------------------------------------------------------------------------
// Helpers
//-----------------------------------------------------------------------------

void Tokenize(std::string str, char delimiter, std::vector<std::string> *target)
{
	std::istringstream ss(str);
	std::string token;
	while(std::getline(ss, token, delimiter))
	{
		target->push_back(token);
	}
}

int RunCommand(std::string command)
{
	std::cout << command << std::endl;
#ifdef DRY_RUN
	return 0;
#else
	return system(command.c_str());
#endif
}

std::string GetFileExtension(std::string filename)
{
	return filename.substr(filename.find_last_of(".") + 1);
}

std::string StripFileExtension(std::string filename)
{
	return filename.substr(0, filename.find_last_of("."));
}

std::string StripFilePath(std::string filename)
{
	return filename.substr(filename.find_last_of("/") + 1);
}

std::string GetFilePath(std::string filename)
{
	size_t pos = filename.find_last_of("/");
	if (pos == std::string::npos)
	{
		return "";
	}
	return filename.substr(0, pos);
}

/**
 * Counts the number of parts to a path, dealing with leading, trailing and
 * repeated slashes.
 */
int CountPathParts(std::string path)
{
	// Count every time we go from a path separator to some path.
	// We start in an imaginary separator, so if the string doesn't start with
	// a separator we successfully count the first path part.
	int parts = 0;
	bool inSeparator = true;
	for (int i = 0; i < path.length(); i += 1)
	{
		if (path[i] == '/')
		{
			inSeparator = true;
		}
		else if (inSeparator)
		{
			parts += 1;
			inSeparator = false;
		}
	}
	return parts;
}

/**
 * Joins paths together ensuring that only a single slash exists between
 * them. The second part ought never to start with a slash, because that would
 * be an absolute path anyway.
 */
std::string PathJoin(std::string first, std::string second)
{
	if (first[first.length() - 1] != '/')
	{
		return first + "/" + second;
	}
	return first + second;
}

/**
 * Returns the number of characters that match at the start of the given
 * strings. Also the index of the first non-matching characters.
 */
int MatchingLength(std::string s1, std::string s2)
{
	int len = s1.length() < s2.length() ? s1.length() : s2.length();
	for (int i = 0; i < len; i += 1)
	{
		if (s1[i] != s2[i])
		{
			return i;
		}
	}
	return len;
}

/**
 * Returns the relative path of 'to' from the directory given in 'from'.
 * e.g. a/b/c, a/b/d/hi.html -> ../d/hi.html
 */
std::string GetRelativePath(std::string from, std::string to)
{
	if (from[from.length() - 1] != '/')
	{
		from = from + "/";
	}

	int matchLen = MatchingLength(from, to);
	from = from.substr(matchLen);
	to = to.substr(matchLen);

	int numFromParts = CountPathParts(from);
	for (int i = 0; i < numFromParts; i += 1)
	{
		to = PathJoin("..", to);
	}

	return to;
}

/**
 * Return true if file exists and is a file.
 */
bool FileExists(std::string filename)
{
	if (FILE *file = fopen(filename.c_str(), "r"))
	{
		fclose(file);
		return true;
	}
	return false;
}

/**
 * Find a file given all the search paths.
 */
std::string FindFile(std::string filename, std::set<std::string> *searchDirs)
{
	for (auto it = searchDirs->begin(); it != searchDirs->end(); ++it)
	{
		std::string path = *it;
		path = PathJoin(path, filename);
		if (FileExists(path))
		{
			return path;
		}
	}
	return filename;
}

/**
 * Check if a directory exists.
 */
bool DirectoryExists(std::string path)
{
	bool exists = true;
	tinydir_dir dir;
	if (tinydir_open(&dir, path.c_str()) == -1)
	{
		exists = false;
	}
	tinydir_close(&dir);
	return exists;
}

/**
 * Find all the files in a given directory.
 */
void GetFilesInDirectory(std::string path, std::vector<std::string> *files)
{
	tinydir_dir dir;
	if (tinydir_open(&dir, path.c_str()) == -1)
	{
		tinydir_close(&dir);
		return;
	}

	while (dir.has_next)
	{
		tinydir_file file;
		if (tinydir_readfile(&dir, &file) != -1)
		{
			std::string filename(file.name);
			if (filename.compare(".") && filename.compare(".."))
			{
				files->push_back(std::string(file.name));
			}
		}
		tinydir_next(&dir);
	}

	tinydir_close(&dir);
}

/**
 * Find the first directory that exists within search dirs.
 */
std::string FindDirectory(std::string directory, std::set<std::string> *searchDirs)
{
	for (auto it = searchDirs->begin(); it != searchDirs->end(); ++it)
	{
		std::string path = *it;
		path = PathJoin(path, directory);
		if (DirectoryExists(path))
		{
			return path;
		}
	}
	return directory;
}

/**
 * Find files in a directory and all its subdirectories, and all theirs, etc.
 */
void RecursiveFindFiles(std::string directory, std::vector<std::string> *target)
{
	std::vector<std::string> files;
	GetFilesInDirectory(directory, &files);

	for (auto itFile = files.begin(); itFile != files.end(); ++itFile)
	{
		std::string filename = PathJoin(directory, *itFile);
		if (DirectoryExists(filename))
		{
			RecursiveFindFiles(filename, target);
		}
		else
		{
			target->push_back(filename);
		}
	}
}

//-----------------------------------------------------------------------------
// Some Slightly Platform Specific Stuff
//-----------------------------------------------------------------------------

// TODO: If we can't figure out the platform's modified time, then just use
// something that'll build everything.
#ifdef _WIN32
#define stat _stat
#endif

/**
 * Return the modified timestamp of the file, or zero if it doesn't exist.
 */
uint64_t ModifiedTime(std::string filename)
{
	if (!FileExists(filename))
	{
		return 0;
	}

	struct stat buf;
	stat(filename.c_str(), &buf);
	return buf.st_mtime;
}

#ifdef _MSC_VER
#include <direct.h>
#define CREATE_DIR(x) _mkdir(x);
#else
#define CREATE_DIR(x) mkdir(x, S_IRWXU | S_IRWXG | S_IROTH);
#endif

/**
 * Ensures all parts of a path have been created.
 */
void EnsureDirectoryExists(std::string path)
{
	if (DirectoryExists(path))
	{
		return;
	}

	if (path.find_first_of('/') != std::string::npos)
	{
		std::string base = GetFilePath(path);
		EnsureDirectoryExists(base);
	}
	CREATE_DIR(path.c_str())
}

//-----------------------------------------------------------------------------
// Globally Useful Purpose-Specific Stuff
//-----------------------------------------------------------------------------

/**
 * Add defines that we were compiled with to our platform.
 *
 * The intention of this is so that we traverse the same code that the compiler
 * will try to traverse, and thus find all required object files.
 * The user can also use directives to add extra files in cases where external
 * source code somehow evades being traversed.
 *
 * What we should instead do is determine the platform being compiled and the
 * compiler being used, and set defines for that.
 */
void SetPlatformDefines(std::set<std::string> *defines)
{
// Detect platform:
// http://nadeausoftware.com/articles/2012/01/c_c_tip_how_use_compiler_predefined_macros_detect_operating_system
#ifdef __unix__
	defines->insert(std::string("__unix__"));
#endif

#ifdef __linux__
	defines->insert(std::string("__linux__"));
#endif

#ifdef __APPLE__
	defines->insert(std::string("__APPLE__"));
#endif

#ifdef __MACH__
	defines->insert(std::string("__APPLE__"));
#endif

#ifdef _WIN32
	defines->insert(std::string("_WIN32"));
#endif

#ifdef _WIN64
	defines->insert(std::string("_WIN64"));
#endif

#ifdef __CYGWIN__
	defines->insert(std::string("__CYGWIN__"));
#endif

#ifdef __CYGWIN32__
	defines->insert(std::string("__CYGWIN32__"));
#endif

#ifdef __MINGW32__
	defines->insert(std::string("__MINGW32__"));
#endif

#ifdef __MINGW64__
	defines->insert(std::string("__MINGW64__"));
#endif

#ifdef _AIX
	defines->insert(std::string("_AIX"));
#endif

#ifdef __hpux
	defines->insert(std::string("__hpux"));
#endif

#ifdef __sun
	defines->insert(std::string("__sun"));
#endif

#ifdef __SVR4
	defines->insert(std::string("__SVR4"));
#endif

// Compiler specific. We know it's definitely gcc here.
	defines->insert(std::string("__GNUC__"));
	// TODO: Set this only if c++.
	defines->insert(std::string("__GNUG__"));
}
