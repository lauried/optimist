
//-----------------------------------------------------------------------------
// Compilation Invoker: (Base class)
//-----------------------------------------------------------------------------

class GenericBuilder {
public:
	std::string defaultTarget;
	std::string defaultObjectDir;
	std::string targetName;

	GenericBuilder()
	{
		defaultTarget = "build/UnknownTarget";
		defaultObjectDir = "obj/UnknownTarget";
		targetName = "unknown";
	}

	/**
	 * Performs the build.
	 */
	int Compile(Build *buildFiles);

protected:
	/**
	 * A set of options. They get processed for each file.
	 * The root file's options will be used for linking.
	 */
	struct BuildOptions {
		std::string target;

		std::string cCompiler;
		std::string cppCompiler;
		std::string linker;
		std::string objectDir;

		std::set<std::string> cOptions;
		std::set<std::string> cppOptions;
		std::set<std::string> sourceDirs;
		std::set<std::string> libDirs;
		std::set<std::string> libraries;

		/**
		 * Set defaults.
		 */
		void SetDefaults(std::string defaultTarget, std::string defaultObjectDir);

		/**
		 * Apply the given directions on top of the existing options.
		 * Target lets us choose which set of compiler options and which compiler to use.
		 */
		void ApplyDirectives(const std::vector<Directive> *directives, std::string targetName);
	};

	virtual void SetDefaultBuildOptions(BuildOptions *options) = 0;
	virtual std::string BuildCompileCommand(std::string filename, std::string objectFilename, BuildOptions *options) = 0;
	virtual std::string BuildLinkCommand(std::set<std::string> *objectFiles, BuildOptions *options) = 0;
};

void GenericBuilder::BuildOptions::SetDefaults(std::string defaultTarget, std::string defaultObjectDir)
{
	target = defaultTarget;
	objectDir = defaultObjectDir;
	cOptions.clear();
	cppOptions.clear();
	sourceDirs.clear();
	libDirs.clear();
	libraries.clear();
}

void GenericBuilder::BuildOptions::ApplyDirectives(const std::vector<Directive> *directives, std::string targetName)
{
	for (auto it = directives->begin(); it != directives->end(); ++it)
	{
		if (!it->name.compare("target") && it->values.size() > 0)
		{
			target = it->values[0];
		}
		if (!it->name.compare("target_append") && it->values.size() > 0)
		{
			target += it->values[0];
		}
		if (!it->name.compare("object_dir") && it->values.size() > 0)
		{
			objectDir = it->values[0];
		}
		if (!it->name.compare("add_source_dirs"))
		{
			for (auto it2 = it->values.begin(); it2 != it->values.end(); ++it2)
			{
				sourceDirs.insert(*it2);
			}
		}
		if (!it->name.compare("add_libraries"))
		{
			for (auto it2 = it->values.begin(); it2 != it->values.end(); ++it2)
			{
				libraries.insert(*it2);
			}
		}
		if (!it->name.compare("add_lib_dirs"))
		{
			for (auto it2 = it->values.begin(); it2 != it->values.end(); ++it2)
			{
				libDirs.insert(*it2);
			}
		}
		if (!it->name.compare("c_compiler") && it->values.size() > 1 && !targetName.compare(it->values[0]))
		{
			cCompiler = it->values[1];
		}
		if (!it->name.compare("cpp_compiler") && it->values.size() > 1 && !targetName.compare(it->values[0]))
		{
			cppCompiler = it->values[1];
		}
		if (!it->name.compare("linker") && it->values.size() > 1 && !targetName.compare(it->values[0]))
		{
			linker = it->values[1];
		}
		if (!it->name.compare("c_options") && it->values.size() > 1 && !targetName.compare(it->values[0]))
		{
			for (int i = 1; i < it->values.size(); i += 1)
			{
				cOptions.insert(it->values[i]);
			}
		}
		if (!it->name.compare("cpp_options") && it->values.size() > 1 && !targetName.compare(it->values[0]))
		{
			for (int i = 1; i < it->values.size(); i += 1)
			{
				cppOptions.insert(it->values[i]);
			}
		}
		if (!it->name.compare("remove_c_options") && it->values.size() > 1 && !targetName.compare(it->values[0]))
		{
			for (int i = 1; i < it->values.size(); i += 1)
			{
				auto find = cOptions.find(it->values[i]);
				if (find != cOptions.end())
				{
					cOptions.erase(find);
				}
			}
		}
		if (!it->name.compare("remove_cpp_options") && it->values.size() > 1 && !targetName.compare(it->values[0]))
		{
			for (int i = 1; i < it->values.size(); i += 1)
			{
				auto find = cppOptions.find(it->values[i]);
				if (find != cppOptions.end())
				{
					cppOptions.erase(find);
				}
			}
		}
	}
}

int GenericBuilder::Compile(Build *build)
{
	if (build->objects.size() < 1)
	{
		std::cout << "No files to build" << std::endl;
		return -1;
	}

	std::set<std::string> objectFiles;

	BuildOptions globalOptions;
	globalOptions.SetDefaults(defaultTarget, defaultObjectDir);
	SetDefaultBuildOptions(&globalOptions);

	const Build::Object *firstObject = &(*(build->objects.begin()));

	int compileStatus = 0;
	size_t numThreads = std::thread::hardware_concurrency();
	std::cout << "Compiling with " << numThreads << " threads." << std::endl;

	// Put the ThreadPool in its own block so it destructs before we try to link.
	{
		ThreadPool threadPool(numThreads);
		for (auto it = build->objects.begin(); it != build->objects.end(); ++it)
		{
			const Build::Object *buildObject = &(*it);

			globalOptions.ApplyDirectives(&(buildObject->directives), targetName);

			// Get the first file's options as the base,
			// then apply the current file's options over the top of it.
			BuildOptions options;
			options.SetDefaults(defaultTarget, defaultObjectDir);
			SetDefaultBuildOptions(&options);
			options.ApplyDirectives(&(firstObject->directives), targetName);
			options.ApplyDirectives(&(buildObject->directives), targetName);

			// Get the object file name.
			std::string baseName = StripFileExtension(buildObject->filename);
			std::string objectFilename = PathJoin(options.objectDir, baseName) + ".o";
			objectFiles.insert(objectFilename);

			// If the object is older than the source, then recompile it.
			// ModifiedTime returns zero for non-existent files, so this calculation here works.
			// We also use <= instead of < because then if ModifiedTime doesn't work the whole thing will still be recompiled each time.
			uint64_t objectModified = ModifiedTime(objectFilename);
			if (objectModified <= buildObject->sourceModifiedTime)
			{
				EnsureDirectoryExists(GetFilePath(objectFilename));

				std::string compileCommand = BuildCompileCommand(buildObject->filename, objectFilename, &options);
				threadPool.enqueue(
					[&compileStatus, compileCommand]()
					{
						int result = RunCommand(compileCommand);
						if (result != 0)
						{
							compileStatus = result;
						}
					}
				);
			}
		}
	}

	// If any compilation threw an error, don't try linking.
	if (compileStatus != 0)
	{
		return compileStatus;
	}

	BuildOptions options;
	options.SetDefaults(defaultTarget, defaultObjectDir);
	SetDefaultBuildOptions(&options);
	options.ApplyDirectives(&(firstObject->directives), targetName);
	std::string linkCommand = BuildLinkCommand(&objectFiles, &globalOptions);
	return RunCommand(linkCommand);
}
