
//-----------------------------------------------------------------------------
// Build Target Type: Code::Blocks
//-----------------------------------------------------------------------------

class CodeBlocksTarget {
public:
	void Usage();
	int HandleCommand(std::deque<std::string> &arguments);

private:

};

void CodeBlocksTarget::Usage()
{
	//std::cout << "codeblocks <dir> <target> [ <define> ... ]" << std::endl;
	std::cout << "codeblocks <inputfile> <projectfile>" << std::endl;
}

int CodeBlocksTarget::HandleCommand(std::deque<std::string> &arguments)
{
	if (arguments.size() < 2)
	{
		Usage();
		return -1;
	}

	// TODO: "codeblocks <file> <output>"
	// The file describes what we do to build the project.
	// The output is the file we write.

	// Reading line by line, we either process a build and add it as a target
	// to the builder, or we recursively find files and add them as units to
	// the builder.
	std::string inputFilename = arguments.front();
	arguments.pop_front();

	std::string projectFilename = arguments.front();
	arguments.pop_front();

	std::ifstream file(inputFilename);
	if (!file.is_open())
	{
		std::cout << "Error: could not open " << inputFilename << std::endl;
		return -1;
	}

	std::vector<Build*> builds;
	CodeBlocksBuilder builder;

	std::string line;
	while (std::getline(file, line))
	{
		std::vector<std::string> tokens;
		Tokenize(line, ' ', &tokens);

		if (tokens.size() < 1)
		{
			continue;
		}

		if (!tokens[0].compare("target"))
		{
			if (tokens.size() < 3)
			{
				std::cout << "Error: Expected 'target <targetName> <startFile> [ <define> ... ]' got '" << line << "'" << std::endl;
				return -1;
			}
			Build *build = new Build();
			SetPlatformDefines(&(build->initialDefines));
			builds.push_back(build);
			for (int iToken = 3; iToken < tokens.size(); iToken += 1)
			{
				build->initialDefines.insert(tokens[iToken]);
			}
			build->FindBuildFiles(tokens[2]);
			builder.AddTarget(build, tokens[1]);
		}

		if (!tokens[0].compare("files"))
		{
			if (tokens.size() != 4 || tokens[2].compare("in"))
			{
				std::cout << "Error: Expected 'files <extensionList> in <searchDir>' got '" << line << "'" << std::endl;
				return -1;
			}

			std::set<std::string> extensionSet;
			std::vector<std::string> extensions;
			Tokenize(tokens[1], ',', &extensions);
			for (auto itExtension = extensions.begin(); itExtension != extensions.end(); ++itExtension)
			{
				extensionSet.insert(*itExtension);
			}
			extensions.resize(0);

			std::vector<std::string> filesInDirectory;
			RecursiveFindFiles(tokens[3], &filesInDirectory);
			for (auto itFile = filesInDirectory.begin(); itFile != filesInDirectory.end(); ++itFile)
			{
				std::string extension = GetFileExtension(*itFile);
				if (extensionSet.find(extension) != extensionSet.end())
				{
					builder.AddUnit(*itFile);
				}
			}
		}
	}

	builder.SetTitle(StripFilePath(StripFileExtension(projectFilename)));
	builder.Compile(projectFilename);

	for (auto itBuild = builds.begin(); itBuild != builds.end(); ++itBuild)
	{
		delete (*itBuild);
	}

	return 0;
}
