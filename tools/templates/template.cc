/*
Templating Utility

1. Build new source files from templates.
2. Apply and update header comments on existing files.

Here we might as well include parts of the framework that we need, because it
can be compiled by the automatic build system.

Templating is performed by loading files, performing substring replacement on
them and then concatenating them together.

Header replacement is loading files, determining the header and replacing it.

A header is a C style comment right at the start of the file, before any other
characters.
We can also look for some other indicators, like a run of ==== or ----.

Some text replacements:
{HEADER_FILE} - the name of the corresponding header file for the source file.
{CLASS} - the name of the class
{NAMESPACES_START} - namespace statements and opening braces
{NAMESPACES_END} - closing braces for namespaces
{DATE} - the current date
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <string>

#include "library/text/StringReplacer.hh"
#include "library/text/Path.hh"
#include "filesystem/PlatformFilesystem.hh"

//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

bool gDryRun = false;

PlatformFilesystem gFilesystem;

//-----------------------------------------------------------------------------
// Helpers
//-----------------------------------------------------------------------------

/**
 * Returns the length of the file's header comment, if it could be found.
 */
size_t GetHeaderLength(const std::string &file)
{
	if (file.length() < 4)
	{
		return 0;
	}
	if (file[0] != '/')
	{
		return 0;
	}
	if (file[1] != '*')
	{
		return 0;
	}
	size_t commentEndPos = file.find("*/");
	if (commentEndPos == std::string::npos)
	{
		return 0;
	}
	size_t dividerPos = file.find("====");
	if (dividerPos == std::string::npos)
	{
		dividerPos = file.find("----");
	}
	if (dividerPos > commentEndPos || dividerPos == std::string::npos)
	{
		return 0;
	}
	return commentEndPos + 2;
}

/**
 * Returns a blank string if the file could not be opened, otherwise returns
 * the text content of the file.
 */
std::string GetFileContents(const std::string &filename)
{
	std::ifstream in(filename, std::ios::in | std::ios::binary);
	std::string result((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());
	return result;
}

/**
 * Writes a string to the contents of a file.
 */
void PutFileContents(const std::string &filename, const std::string &contents)
{
	std::ofstream out(filename, std::ios::out | std::ios::binary | std::ios::trunc);
	out << contents;
}

/**
 * Writes a string to the contents of a file.
 */
void PutFileContentsSafe(const std::string &filename, const std::string &contents)
{
	if (gFilesystem.FileExists(filename))
	{
		std::cout << "File already exists, not rewriting " << filename << std::endl;
		return;
	}
	PutFileContents(filename, contents);
}

//-----------------------------------------------------------------------------
// Scan all files and replace their headers
//-----------------------------------------------------------------------------

void ReplaceHeader(const std::string &filename, const std::string &newHeader)
{
	Path pathInfo(filename, Path::F_Extension);
	if (pathInfo.extension.compare("hh") && pathInfo.extension.compare("cc"))
	{
		return;
	}

	std::cout << "Replacing " << filename << std::endl;

	std::string fileContents = GetFileContents(filename);
	int oldHeaderLength = GetHeaderLength(fileContents);
	fileContents = newHeader + fileContents.substr(oldHeaderLength);

	if (!gDryRun)
	{
		PutFileContents(filename, fileContents);
	}
}

void RecursiveReplaceHeaders(const std::string &searchDir, const std::string &newHeader, int maxDepth, int depth = 0)
{
	std::cout << "Replacing in " << searchDir << "..." << std::endl;

	if (depth > maxDepth)
	{
		return;
	}

	std::vector<std::string> files;
	gFilesystem.GetFilesInDirectory(searchDir, &files);

	for (int i = 0; i < files.size(); i += 1)
	{
		std::string filename = Path::Join({ searchDir, files[i] });
		if (gFilesystem.DirectoryExists(filename))
		{
			RecursiveReplaceHeaders(filename, newHeader, depth + 1);
		}
		else if (gFilesystem.FileExists(filename))
		{
			ReplaceHeader(filename, newHeader);
		}
	}
}

void ReplaceHeaders(const std::string &searchDir, const std::string &newHeaderFilename)
{
	std::string newHeader = GetFileContents(newHeaderFilename);
	int foundHeaderLength = GetHeaderLength(newHeader);

	for (int i = foundHeaderLength; i < newHeader.length(); i += 1)
	{
		if (newHeader[i] != '\n' && newHeader[i] != '\n')
		{
			std::cout << "New header file is not detected as a header - it won't be replaceable later by this tool" << std::endl;
			std::cout << "Aborted" << std::endl;
			return;
		}
	}
	RecursiveReplaceHeaders(searchDir, newHeader, 8);
}

//-----------------------------------------------------------------------------
// Create a source/header pair, or just the header
//-----------------------------------------------------------------------------

void BuildFiles(std::string className, bool headerOnly)
{
	std::string baseDir("src");

	Path pathInfo(className, Path::F_Path | Path::F_Filename);
	std::string headerFile = className + ".hh";

	StringReplacer replacements;
	replacements.AddReplacement("{HEADER_FILE}", headerFile);
	replacements.AddReplacement("{CLASS}", pathInfo.filename);
	replacements.AddReplacement("{NAMESPACES_START}", "namespace opti {");
	replacements.AddReplacement("{NAMESPACES_END}", "} // namespace opti");

	std::string commonTemplate = replacements.Replace(GetFileContents("tools/templates/common_header.txt"));
	std::string classTemplate = replacements.Replace(GetFileContents("tools/templates/class.txt"));
	std::string headerTemplate = replacements.Replace(GetFileContents("tools/templates/class_header.txt"));

	std::string path = Path::Join({ baseDir, pathInfo.path });
	gFilesystem.CreateDirectory(path);

	if (!headerOnly)
	{
		std::string sourceName = Path::Join({ baseDir, className }) + ".cc";

		if (gDryRun)
		{
			std::cout << sourceName << std::endl << std::endl;
			std::cout << commonTemplate << classTemplate << std::endl << std::endl;
		}
		else
		{
			std::string contents = commonTemplate + classTemplate;
			PutFileContentsSafe(sourceName, contents);
		}
	}

	std::string headerName = Path::Join({ baseDir, className }) + ".hh";

	if (gDryRun)
	{
		std::cout << headerName << std::endl << std::endl;
		std::cout << commonTemplate << headerTemplate << std::endl << std::endl;
	}
	else
	{
		std::string contents = commonTemplate + headerTemplate;
		PutFileContentsSafe(headerName, contents);
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

int main(int argc, char *argv[])
{
	// Check for parameters to do header replacement
	std::string applyHeader("--apply-header");
	std::string add("--add");
	std::string addHeader("--add-header");
	if (argc == 3 && !applyHeader.compare(argv[1]))
	{
		opti::PlatformFilesystem filesystem;
		std::string filename(argv[2]);
		if (!filesystem.FileExists(filename))
		{
			std::cout << "File does not exist" << std::endl;
			return 1;
		}

		std::cout << "Are you sure you want to replace all files headers with " << filename << " (y/n)?" << std::endl;
		std::string areYouSure;
		std::getline(std::cin, areYouSure);
		if (areYouSure.compare("y"))
		{
			std::cout << "Cancelled" << std::endl;
			return 0;
		}
		opti::ReplaceHeaders("src", filename);
		std::cout << "Done" << std::endl;
		return 0;
	}
	if (argc == 3 && !add.compare(argv[1]))
	{
		opti::BuildFiles(argv[2], false);
		return 0;
	}
	if (argc == 3 && !addHeader.compare(argv[1]))
	{
		opti::BuildFiles(argv[2], true);
		return 0;
	}

	std::cout << "Available parameters:" << std::endl;
	std::cout << "  --apply-header <file>" << std::endl;
	std::cout << "Applies the header comment in the specified file to all source and header files," << std::endl;
	std::cout << "replacing an existing header if found." << std::endl;
	std::cout << "An existing header is detected if the file starts with a multiline comment" << std::endl;
	std::cout << "containing --- or ===." << std::endl;
	std::cout << "  --add <path>" << std::endl;
	std::cout << "  --add-header <path>" << std::endl;
	std::cout << "Creates a source and header, or header file only, respectively, within src/," << std::endl;
	std::cout << "with the given path, using the templates in templates/." << std::endl;

	return 1;

	// Create a class
	std::cout << "Enter path of class to create: ";
	std::string path;
	std::getline(std::cin, path);
	std::cout << "Creating " << path << "..." << std::endl;
	std::string headerOnly;
	while (headerOnly.compare("y") && headerOnly.compare("n"))
	{
		std::cout << "Header only? (y/n)";
		std::getline(std::cin, headerOnly);
	}
	opti::BuildFiles(path, !headerOnly.compare("y"));
	std::cout << "Done" << std::endl;
	return 0;
}

//-----------------------------------------------------------------------------
